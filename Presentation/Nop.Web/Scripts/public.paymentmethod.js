﻿var PaymentMethod = {
    stripeMethodName: "Payments.Stripe",
    paypalMethodName: "Payments.PayPalStandard",
    formData: "#formPaymentMethod",
    saveUrl: false,
    successUrl: false,

    init: function (saveUrl, successUrl) {
        this.saveUrl = saveUrl;
        this.successUrl = successUrl;
    },
    selectedMethod: function () {
        return $("input[name='paymentmethod']").val();
    },
    currentSubmitButton: function () {
        var submitBtn = "";
        var selectedMethod = PaymentMethod.selectedMethod();
        if (selectedMethod == PaymentMethod.stripeMethodName)
            submitBtn = "#checkout-stripe";
        if (selectedMethod == PaymentMethod.paypalMethodName)
            submitBtn = "#checkout-paypal";
        return submitBtn;
    },
    validatePaymentMethodForm: function () {
        var _paymentForm = $(PaymentMethod.formData);
        _paymentForm.submit(function (event) {
            if (_paymentForm.valid()) {
                PaymentMethod.onSubmitLoading();
                if (PaymentMethod.selectedMethod() == PaymentMethod.stripeMethodName) {
                    event.preventDefault();
                    PaymentMethod.save();
                }
                else {
                    setTimeout(PaymentMethod.onRemoveLoading(), 5000);
                }
            }
        });
    },
    save: function () {
        // get form data value
        var values = {};
        $.each($(PaymentMethod.formData).serializeArray(), function (i, field) {
            values[field.name] = field.value;
        });

        this.submitForm();

        return false;
    },
    submitForm: function () {
        $.ajax({
            cache: false,
            url: this.saveUrl,
            data: $(PaymentMethod.formData).serialize(),
            type: 'post',
            success: function (response) {
                if (response.update_section) {
                    $("#payment-errors-container").html(response.update_section.html);
                }
                if (response.allow_sections) {
                    response.allow_sections.each(function (e) {
                        $('#opc-' + e).addClass('allow');
                    });
                }

                //TODO move it to a new method
                if (response.redirect) {
                    location.href = response.redirect;
                    return true;
                }

                if (response.success) {
                    window.location = PaymentMethod.successUrl;
                }

                PaymentMethod.onRemoveLoading();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert('an error occurred while processing your request');
                PaymentMethod.onRemoveLoading();
            }
        });
    },
    onSubmitLoading: function () {
        var currentBtn = PaymentMethod.currentSubmitButton();
        if (currentBtn != "") {
            var _submitBtn = $(currentBtn);
            _submitBtn.addClass("btn-loading");
        }
    },
    onRemoveLoading: function () {
        var currentBtn = PaymentMethod.currentSubmitButton();
        if (currentBtn != "") {
            var _submitBtn = $(currentBtn);
            _submitBtn.removeClass("btn-loading");
        }
    }
};