﻿var BillingAddress = {
	newAddress: function (isNew) {
		if (isNew) {
			this.resetSelectedAddress();
			$('#billing-new-address-form').show();
		} else {
			$('#billing-new-address-form').hide();
		}
	},

	resetSelectedAddress: function () {
		var selectElement = $('#billing-address-select');
		if (selectElement) {
			selectElement.val('');
		}
	},

    submitBillingAddress: function (btnElm) {
		var _coBillingForm = $("#co-billing-form");
        if (_coBillingForm.valid()) {
            if (btnElm) {
                btnElm.classList.add("btn-loading");
            }
			_coBillingForm.submit();
        }
	}
};