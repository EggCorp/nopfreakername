﻿
var EggCheckout = {
    loadWaiting: false,
    failureUrl: false,
    formData: false,
    saveUrl: false,
    successUrl: false,

    init: function (saveUrl, formData, failureUrl, successUrl) {
        this.loadWaiting = false;
        this.failureUrl = failureUrl;
        this.formData = formData;
        this.saveUrl = saveUrl;
        this.successUrl = successUrl;
        Accordion.disallowAccessToNextSections = true;
    },

    ajaxFailure: function () {
        removeCheckoutLoading();
        location.href = Checkout.failureUrl;
    },

    _disableEnableAll: function (element, isDisabled) {
        var descendants = element.find('*');
        $(descendants).each(function () {
            if (isDisabled) {
                $(this).attr('disabled', 'disabled');
            } else {
                $(this).removeAttr('disabled');
            }
        });

        if (isDisabled) {
            element.attr('disabled', 'disabled');
        } else {
            element.removeAttr('disabled');
        }
    },

    setLoadWaiting: function (step, keepDisabled) {
        if (step) {
            if (this.loadWaiting) {
                this.setLoadWaiting(false);
            }
            var container = $('#' + step + '-buttons-container');
            container.addClass('disabled');
            container.css('opacity', '.5');
            this._disableEnableAll(container, true);
            $('#' + step + '-please-wait').show();
        } else {
            if (this.loadWaiting) {
                var container = $('#' + this.loadWaiting + '-buttons-container');
                var isDisabled = (keepDisabled ? true : false);
                if (!isDisabled) {
                    container.removeClass('disabled');
                    container.css('opacity', '1');
                }
                this._disableEnableAll(container, isDisabled);
                $('#' + this.loadWaiting + '-please-wait').hide();
            }
        }
        this.loadWaiting = step;
    },


    setStepResponse: function (response) {
        if (response.update_section) {
            $("#payment-errors-container").html(response.update_section.html);
        }
        if (response.allow_sections) {
            response.allow_sections.each(function (e) {
                $('#opc-' + e).addClass('allow');
            });
        }

        //TODO move it to a new method
        if (response.redirect) {
            location.href = response.redirect;
            return true;
        }

        if (response.success) {
            window.location = EggCheckout.successUrl;
        }
        //removeCheckoutLoading();
        return false;
    },

    save: function () {
        checkoutLoading();
        resetError();
        // get form data value
        var values = {};
        $.each($(this.formData).serializeArray(), function (i, field) {
            values[field.name] = field.value;
        });
        
        this.submitForm();
        
        return false;
    },

    nextStep: function (response) {
        if (response.error) {
            if ((typeof response.message) == 'string') {
                alert(response.message);
            } else {
                alert(response.message.join("\n"));
            }
            removeCheckoutLoading();
            return false;
        }
        EggCheckout.setStepResponse(response);
        if (response.wrong_billing_address) {
            removeCheckoutLoading();
        }
    },

    resetLoadWaiting: function () {
        removeCheckoutLoading();
        //EggCheckout.setLoadWaiting(false);
    },
    stripeValidateCard: function () {
        if (false == Stripe.card.validateCardNumber($('#CardNumber').val())) {
            $("#payment-errors-container").text('Invalid Card');
            removeCheckoutLoading();
            return false;
        }

        if (false == Stripe.card.validateExpiry($('#ExpireMonth').val(), $('#ExpireYear').val())) {
            $("#payment-errors-container").text('Expired Card');
            removeCheckoutLoading();
            return false;
        }

        this.submitForm();

        //var selectedCoundtry = $("#BillingNewAddress_CountryId option:selected").text();
        //var selectedState = $("#BillingNewAddress_StateProvinceId option:selected").text();

        //var countryIso2LetterCode = getCountryCode(selectedCoundtry);

        //Stripe.card.createToken({
        //    number: $('#CardNumber').val(),
        //    cvc: $('#CardCode').val(),
        //    exp_month: $('#ExpireMonth').val(),
        //    exp_year: $('#ExpireYear').val(),
        //    name: $('#CardholderName').val(),
        //    address_line1: $('#BillingNewAddress_Address1').val(),
        //    address_line2: '',
        //    address_city: $('#BillingNewAddress_City').val(),
        //    address_state: selectedState,
        //    address_zip: $('#BillingNewAddress_ZipPostalCode').val(),
        //    address_country: countryIso2LetterCode
        //}, this.stripeResponseHandler);
    },
    stripeResponseHandler: function (status, response) {
        if (response.error) {
            // Show the errors on the form
            $("#payment-errors-container").text(response.error.message);
            removeCheckoutLoading();
        } else {
            // response contains id and card, which contains additional card details
            var token = response.id;
            $("#payment-errors-container").text("");
            var form = $(".payment-info-next-step-button").closest('form');

            $("#paymenttoken").val(token);

            EggCheckout.submitForm();
        }

        $(".payment-info-next-step-button").removeAttr("disabled");
    },
    submitForm: function () {
        $.ajax({
            cache: false,
            url: this.saveUrl,
            data: $(this.formData).serialize(),
            type: 'post',
            success: this.nextStep,
            complete: this.resetLoadWaiting,
            error: this.ajaxFailure
        });
    },

    //some function triggering submit checkout form
    validateCOAddressForm: function () {
        var _coBillingForm = $("#co-billing-form");
        _coBillingForm.submit(function (event) {
            event.preventDefault();
            if (_coBillingForm.valid()) {
                EggCheckout.save();
            }
        });
    },
    onSubmitCheckoutForm: function () {
        $("#submit-co-billing-form").click();
    }
};

var EggOrderSummary = {
    saveUrl: false,


    int: function (saveUrl) {
        this.saveUrl = saveUrl;
    },

    save: function (countryId) {
        resetError();
        $.ajax({
            cache: false,
            url: this.saveUrl,
            data: { "countryId": countryId },
            type: 'post',
            success: this.nextStep,
            complete: this.resetLoadWaiting,
            error: this.ajaxFailure
        });
    },

    ajaxFailure: function () {
        removeCheckoutLoading();
        location.href = Checkout.failureUrl;
    },

    nextStep: function (response) {
        if (response.error) {
            if ((typeof response.message) == 'string') {
                alert(response.message);
            } else {
                alert(response.message.join("\n"));
            }
            removeCheckoutLoading();
            return false;
        }
        EggOrderSummary.setStepResponse(response);
    },

    setStepResponse: function (response) {
        if (response.update_section) {
            $("#order-summary-shipping").html(response.update_section.shipping);
            $("#order-summary-total").html(response.update_section.total);
            removeCheckoutLoading();
        }

        //TODO move it to a new method
        if (response.redirect) {
            location.href = response.redirect;
            removeCheckoutLoading();
            return true;
        }

        if (response.success) {
            window.location = EggCheckout.successUrl;
            removeCheckoutLoading();
        }
        removeCheckoutLoading();
        return false;
    },

    resetLoadWaiting: function () {
        removeCheckoutLoading();
        EggCheckout.setLoadWaiting(false);
    },
};

var EggShippingMethod = {
    form: false,
    saveUrl: false,

    init: function (form, saveUrl) {
        this.form = form;
        this.saveUrl = saveUrl;
    },

    validate: function () {
        var methods = document.getElementsByName('shippingoption');
        if (methods.length == 0) {
            alert('Your order cannot be completed at this time as there is no shipping methods available for it. Please make necessary changes in your shipping address.');
            return false;
        }

        for (var i = 0; i < methods.length; i++) {
            if (methods[i].checked) {
                return true;
            }
        }
        alert('Please specify shipping method.');
        return false;
    },

    save: function () {
        $.ajax({
            cache: false,
            url: this.saveUrl,
            data: $(this.form).serialize(),
            type: 'post',
            success: this.nextStep,
            complete: this.resetLoadWaiting,
            error: EggShippingMethod.ajaxFailure
        });
    },

    resetLoadWaiting: function () {
        EggShippingMethod.setLoadWaiting(false);
    },

    nextStep: function (response) {
        if (response.error) {
            if ((typeof response.message) == 'string') {
                alert(response.message);
            } else {
                alert(response.message.join("\n"));
            }

            return false;
        }

        EggShippingMethod.setStepResponse(response);
    },
    setStepResponse: function (response) {
        if (response.update_section) {
            $("#order-summary-shipping").html(response.update_section.shipping);
            $("#order-summary-total").html(response.update_section.total);
            removeCheckoutLoading();
        }

        //TODO move it to a new method
        if (response.redirect) {
            location.href = response.redirect;
            removeCheckoutLoading();
            return true;
        }

        if (response.success) {
            window.location = EggCheckout.successUrl;
            removeCheckoutLoading();
        }
        removeCheckoutLoading();
        return false;
    },

    resetLoadWaiting: function () {
        removeCheckoutLoading();
        EggCheckout.setLoadWaiting(false);
    },
};

function checkoutLoading() {
    $('#checkout-paypal').fadeOut('fast');
    $('#checkout-stripe').fadeOut('fast');
    $('#second-checkout :button').fadeOut('fast');
    $('.checkout-waiting').fadeIn('slow');
}
function removeCheckoutLoading() {
    $('.checkout-waiting').fadeOut('slow');
    $('#checkout-paypal').fadeIn('fast');
    $('#checkout-stripe').fadeIn('fast');
    $('#second-checkout :button').fadeIn('fast');
}
function resetError() {
    var paymentError = $('div#payment-errors-container');
    var paymentValidate = $('.field-validation-error');
    paymentError.html("");
    paymentValidate.html("");
}


function getCountryCode(countryName) {
    if (isoCountries.hasOwnProperty(countryName)) {
        return isoCountries[countryName];
    } else {
        return countryName;
    }
}
function checkoutApplyCoupon(){
    var _discountForm = $("#shopping-cart-form");
    var couponValidate = $("#cp-validate");
    couponValidate.hide();
    // get form data value
    var values = {};
    $.each(_discountForm.serializeArray(), function (i, field) {
        values[field.name] = field.value;
    });
    if (values.discountcouponcodecheckout != '') {
        var _btnApply = $("button[name='applydiscountcouponcodecheckout']");
        _btnApply.addClass("btn-loading");
        $.ajax({
            cache: false,
            url: "/ShoppingCart/ApplyDiscountCouponAjax",
            data: values,
            type: 'post',
            success: function (response) {
                $("#checkout-order-summary").html(response);
                _btnApply.addClass("btn-loading");
                couponValidate.hide();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert('an error occurred while processing your request');
                _btnApply.addClass("btn-loading");
                couponValidate.hide();
            }
        });
    }
    else {
        couponValidate.show();
    }
}

function checkoutRemoveCoupon() {
    var _discountForm = $("#shopping-cart-form");
    displayAjaxLoading(true);
    // get form data value
    var values = {};
    $.each(_discountForm.serializeArray(), function (i, field) {
        values[field.name] = field.value;
    });
    $.ajax({
        cache: false,
        url: "/ShoppingCart/RemoveDiscountCouponAjax",
        data: values,
        type: 'post',
        success: function (response) {
            $("#checkout-order-summary").html(response);
            displayAjaxLoading(false);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert('an error occurred while processing your request');
            displayAjaxLoading(false);
        }
    });
}

function submitShippingMethod(btnElm) {
    var _coBillingForm = $("#shipping-method-form");
    if (_coBillingForm.valid()) {
        if (btnElm) {
            btnElm.classList.add("btn-loading");
        }
        _coBillingForm.submit();
    }
}

var isoCountries = {
    'Afghanistan': 'AF',
    'Aland Islands': 'AX',
    'Albania': 'AL',
    'Algeria': 'DZ',
    'American Samoa': 'AS',
    'Andorra': 'AD',
    'Angola': 'AO',
    'Anguilla': 'AI',
    'Antarctica': 'AQ',
    'Antigua And Barbuda': 'AG',
    'Argentina': 'AR',
    'Armenia': 'AM',
    'Aruba': 'AW',
    'Australia': 'AU',
    'Austria': 'AT',
    'Azerbaijan': 'AZ',
    'Bahamas': 'BS',
    'Bahrain': 'BH',
    'Bangladesh': 'BD',
    'Barbados': 'BB',
    'Belarus': 'BY',
    'Belgium': 'BE',
    'Belize': 'BZ',
    'Benin': 'BJ',
    'Bermuda': 'BM',
    'Bhutan': 'BT',
    'Bolivia': 'BO',
    'Bosnia And Herzegovina': 'BA',
    'Botswana': 'BW',
    'Bouvet Island': 'BV',
    'Brazil': 'BR',
    'British Indian Ocean Territory': 'IO',
    'Brunei Darussalam': 'BN',
    'Bulgaria': 'BG',
    'Burkina Faso': 'BF',
    'Burundi': 'BI',
    'Cambodia': 'KH',
    'Cameroon': 'CM',
    'Canada': 'CA',
    'Cape Verde': 'CV',
    'Cayman Islands': 'KY',
    'Central African Republic': 'CF',
    'Chad': 'TD',
    'Chile': 'CL',
    'China': 'CN',
    'Christmas Island': 'CX',
    'Cocos (Keeling) Islands': 'CC',
    'Colombia': 'CO',
    'Comoros': 'KM',
    'Congo': 'CG',
    'Congo, Democratic Republic': 'CD',
    'Cook Islands': 'CK',
    'Costa Rica': 'CR',
    'Cote D\'Ivoire': 'CI',
    'Croatia': 'HR',
    'Cuba': 'CU',
    'Cyprus': 'CY',
    'Czech Republic': 'CZ',
    'Denmark': 'DK',
    'Djibouti': 'DJ',
    'Dominica': 'DM',
    'Dominican Republic': 'DO',
    'Ecuador': 'EC',
    'Egypt': 'EG',
    'El Salvador': 'SV',
    'Equatorial Guinea': 'GQ',
    'Eritrea': 'ER',
    'Estonia': 'EE',
    'Ethiopia': 'ET',
    'Falkland Islands': 'FK',
    'Faroe Islands': 'FO',
    'Fiji': 'FJ',
    'Finland': 'FI',
    'France': 'FR',
    'French Guiana': 'GF',
    'French Polynesia': 'PF',
    'French Southern Territories': 'TF',
    'Gabon': 'GA',
    'Gambia': 'GM',
    'Georgia': 'GE',
    'Germany': 'DE',
    'Ghana': 'GH',
    'Gibraltar': 'GI',
    'Greece': 'GR',
    'Greenland': 'GL',
    'Grenada': 'GD',
    'Guadeloupe': 'GP',
    'Guam': 'GU',
    'Guatemala': 'GT',
    'Guernsey': 'GG',
    'Guinea': 'GN',
    'Guinea-Bissau': 'GW',
    'Guyana': 'GY',
    'Haiti': 'HT',
    'Heard Island & Mcdonald Islands': 'HM',
    'Holy See (Vatican City State)': 'VA',
    'Honduras': 'HN',
    'Hong Kong': 'HK',
    'Hungary': 'HU',
    'Iceland': 'IS',
    'India': 'IN',
    'Indonesia': 'ID',
    'Iran, Islamic Republic Of': 'IR',
    'Iraq': 'IQ',
    'Ireland': 'IE',
    'Isle Of Man': 'IM',
    'Israel': 'IL',
    'Italy': 'IT',
    'Jamaica': 'JM',
    'Japan': 'JP',
    'Jersey': 'JE',
    'Jordan': 'JO',
    'Kazakhstan': 'KZ',
    'Kenya': 'KE',
    'Kiribati': 'KI',
    'Korea': 'KR',
    'Kuwait': 'KW',
    'Kyrgyzstan': 'KG',
    'Lao People\'s Democratic Republic': 'LA',
    'Latvia': 'LV',
    'Lebanon': 'LB',
    'Lesotho': 'LS',
    'Liberia': 'LR',
    'Libyan Arab Jamahiriya': 'LY',
    'Liechtenstein': 'LI',
    'Lithuania': 'LT',
    'Luxembourg': 'LU',
    'Macao': 'MO',
    'Macedonia': 'MK',
    'Madagascar': 'MG',
    'Malawi': 'MW',
    'Malaysia': 'MY',
    'Maldives': 'MV',
    'Mali': 'ML',
    'Malta': 'MT',
    'Marshall Islands': 'MH',
    'Martinique': 'MQ',
    'Mauritania': 'MR',
    'Mauritius': 'MU',
    'Mayotte': 'YT',
    'Mexico': 'MX',
    'Micronesia, Federated States Of': 'FM',
    'Moldova': 'MD',
    'Monaco': 'MC',
    'Mongolia': 'MN',
    'Montenegro': 'ME',
    'Montserrat': 'MS',
    'Morocco': 'MA',
    'Mozambique': 'MZ',
    'Myanmar': 'MM',
    'Namibia': 'NA',
    'Nauru': 'NR',
    'Nepal': 'NP',
    'Netherlands': 'NL',
    'Netherlands Antilles': 'AN',
    'New Caledonia': 'NC',
    'New Zealand': 'NZ',
    'Nicaragua': 'NI',
    'Niger': 'NE',
    'Nigeria': 'NG',
    'Niue': 'NU',
    'Norfolk Island': 'NF',
    'Northern Mariana Islands': 'MP',
    'Norway': 'NO',
    'Oman': 'OM',
    'Pakistan': 'PK',
    'Palau': 'PW',
    'Palestinian Territory, Occupied': 'PS',
    'Panama': 'PA',
    'Papua New Guinea': 'PG',
    'Paraguay': 'PY',
    'Peru': 'PE',
    'Philippines': 'PH',
    'Pitcairn': 'PN',
    'Poland': 'PL',
    'Portugal': 'PT',
    'Puerto Rico': 'PR',
    'Qatar': 'QA',
    'Reunion': 'RE',
    'Romania': 'RO',
    'Russian Federation': 'RU',
    'Rwanda': 'RW',
    'Saint Barthelemy': 'BL',
    'Saint Helena': 'SH',
    'Saint Kitts And Nevis': 'KN',
    'Saint Lucia': 'LC',
    'Saint Martin': 'MF',
    'Saint Pierre And Miquelon': 'PM',
    'Saint Vincent And Grenadines': 'VC',
    'Samoa': 'WS',
    'San Marino': 'SM',
    'Sao Tome And Principe': 'ST',
    'Saudi Arabia': 'SA',
    'Senegal': 'SN',
    'Serbia': 'RS',
    'Seychelles': 'SC',
    'Sierra Leone': 'SL',
    'Singapore': 'SG',
    'Slovakia': 'SK',
    'Slovenia': 'SI',
    'Solomon Islands': 'SB',
    'Somalia': 'SO',
    'South Africa': 'ZA',
    'South Georgia And Sandwich Isl.': 'GS',
    'Spain': 'ES',
    'Sri Lanka': 'LK',
    'Sudan': 'SD',
    'Suriname': 'SR',
    'Svalbard And Jan Mayen': 'SJ',
    'Swaziland': 'SZ',
    'Sweden': 'SE',
    'Switzerland': 'CH',
    'Syrian Arab Republic': 'SY',
    'Taiwan': 'TW',
    'Tajikistan': 'TJ',
    'Tanzania': 'TZ',
    'Thailand': 'TH',
    'Timor-Leste': 'TL',
    'Togo': 'TG',
    'Tokelau': 'TK',
    'Tonga': 'TO',
    'Trinidad And Tobago': 'TT',
    'Tunisia': 'TN',
    'Turkey': 'TR',
    'Turkmenistan': 'TM',
    'Turks And Caicos Islands': 'TC',
    'Tuvalu': 'TV',
    'Uganda': 'UG',
    'Ukraine': 'UA',
    'United Arab Emirates': 'AE',
    'United Kingdom': 'GB',
    'United States': 'US',
    'United States Outlying Islands': 'UM',
    'Uruguay': 'UY',
    'Uzbekistan': 'UZ',
    'Vanuatu': 'VU',
    'Venezuela': 'VE',
    'Vietnam': 'VN',
    'Virgin Islands, British': 'VG',
    'Virgin Islands, U.S.': 'VI',
    'Wallis And Futuna': 'WF',
    'Western Sahara': 'EH',
    'Yemen': 'YE',
    'Zambia': 'ZM',
    'Zimbabwe': 'ZW'
};

