﻿FortuneWheel = function (config) {
    this.config = config;
    this.container = this.config.container;
    this.setupSlices();
};

FortuneWheel.prototype = {
    setupSlices: function () {
        this.slices = this.container.find('.fortunewheel-slice');
        var degrees = 0;
        for (var x = 0; x < this.slices.length; x++) {
            var slice = $(this.slices[x]);
            slice.css('transform', 'rotate(' + degrees + 'deg) translate(0px, -50%)');
            degrees -= 30;
        }
    },

    setWinningSlice: function (slice) {
        this.winning_slice = slice;
        this.container.css('-webkit-animation-name', 'spin-slice' + slice);
        this.container.css('animation-name', 'spin-slice' + slice);
    },

    spin: function (callback) {
        this.reset();
        this.container.addClass('fortunewheel-spinning');

        /* We need to do callback when spinning is done */
        var duration = this.getDuration();

        setTimeout(callback, duration);
    },

    getDuration: function () {
        var duration = this.config.container.css('animation-duration');

        /* See for second split */
        if (duration.split('s').length == 2) {
            return duration.split('s')[0] * 1000;
        } else {
            return duration.split('ms')[0];
        }

        return duration;
    },

    reset: function () {
        this.container.removeClass('fortunewheel-spinning');
    },
};
