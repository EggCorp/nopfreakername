﻿window.eggConfig = { 'token': 'f4fff965-8707-4a4f-a201-8ade219a2516', 'base_media': '/Scripts/listagram/api/images/default', 'base_static': '', 'base_api': '/Scripts/listagram/js', };
var WheelManager = function (config, listagram) {
    this.config = config;
    this.listagram = listagram;
    this.shown = false;
};

WheelManager.prototype = {

    setUp: function () {
        this.setUpIntent();
        this.setUpTimeout();
        this.setUpLinkClick();
    },

    display: function() {
        var conf = this.config;
        /* First check if there is a cookie for this already being displayed */
        var cookie = this.listagram.getCookie(conf.cookie_name);
        if(cookie != null && cookie != '') {
            return false;
        }

        if(!this.checkUrls()) {
            return false;
        }

        /* Check according to rules */
        if(this.isDesktop()) {
            /* Desktop */
            if(conf.display_desktop) {
                return true;
            } else {
                return false;
            }
        } else {
            /* Mobile */
            if(conf.display_mobile) {
                return true;
            } else {
                return false;
            }
        }

        return false;
    },

    checkUrls: function() {
        /* Read the config URLs for include and exclude URLs and decide accordingly */
        var exclude = this.config.exclude_urls;
        var include = this.config.include_urls;
        var page_url = window.location.href;

        /* Just ignore if URL config is not found */
        if(typeof exclude == 'undefined' || typeof include == 'undefined') {
            return true;
        }

        /* Return true if there are no exclude or include urls */
        if(exclude.length == 0 && include.length == 0) {
            return true;
        }

        var found_matching_include = this.checkMatchingUrls(include, page_url);
        var found_matching_exclude = this.checkMatchingUrls(exclude, page_url);

        /* If an exclude match is found, lets return false */
        if(found_matching_exclude) {
            return false;
        }

        /* At this point, if we can't find a matching include url, we should also return false */
        if(include.length > 0 && !found_matching_include) {
            return false;
        }

        /* Seems to pass all tests, lets display */
        return true;
    },

    checkMatchingUrls: function(urls, page_url) {
        var found = false;

        for(var x=0; x < urls.length; x++) {
            if(this.checkMatchingUrl(urls[x], page_url)) {
                found = true;
                break;
            }
        }
        return found;
    },

    checkMatchingUrl: function(url, page_url) {
        /* Check one included or excluded URL for a match with current URL */
        if(url.type == 'startswith') {
            return (page_url.indexOf(url.url) == 0)
        } else if(url.type == 'exact') {
            return (page_url == url.url);
        } else if(url.type == 'contains') {
            return (page_url.indexOf(url.url) > -1);
        } else if(url.type == 'notcontains') {
            return (page_url.indexOf(url.url) == -1);
        } else if(url.type == 'regex') {
            var regex = new RegExp(url.url);
            return (regex.test(page_url));
        }
    },

    intentMouseLeave: function() {
        if(this.shown) { return; }

        var timeout_delay = 1;
        this.intent_timeout = setTimeout(
            this.show.bind(this), timeout_delay*1000);
    },

    intentMouseEnter: function() {
        if(this.shown) { return; }

        if(this.intent_timeout) {
            clearTimeout(this.intent_timeout);
        }
    },

    saveImpressionStatistic: function() {
        var wheel = this.config.id;
        var token = this.listagram.config.token;
        var script = document.createElement('script');
        script.async = true;
        script.src = this.listagram.config.base_api + 'save-impression/?wheel=' + wheel + '&token=' + token;
        document.body.appendChild(script);
    },

    setUpLinkClick: function() {
        /* Lets search for links to activate this wheel */
        var matches = document.querySelectorAll('.' + this.config.cookie_name);
        for(var x=0; x < matches.length; x++) {
            matches[x].addEventListener('click', function(event) {
                event.stopPropagation();
                event.preventDefault();

                // Set shown to false to allow multiple showings on link clicks
                this.shown = false;
                this.show();
            }.bind(this));
        }
    },

    setUpIntent: function() {
        if(this.isDesktop()) {
            var enabled = this.config.display_leave_intent_desktop;
        } else {
            var enabled = this.config.display_leave_intent_mobile;
        }

        if(enabled) {
            if(this.isDesktop()) {
                var elm = document.documentElement;
                elm.addEventListener('mouseleave', this.intentMouseLeave.bind(this));
                elm.addEventListener('mouseenter', this.intentMouseEnter.bind(this));
            } else {
                console.log('leave intent MOBILE not implemented yet!');
            }
        } else {
        }
    },

    show: function () {
        /* Cancel any waiting timeouts */
        if(this.intent_timeout) {
            clearTimeout(this.intent_timeout);
        }

        if(this.timeout) {
            clearTimeout(this.timeout);
        }

        if(this.shown) { return; }

        //this.saveImpressionStatistic();
        this.listagram.show();
        this.shown = true;
        //debugger;
        //var iframeElm = $('iframe');
        //var test = iframeElm.children();
        //if (iframeElm.length > 0)
        //{
        //    iframeElm.each(function () {
        //        var dom = this.contentDocument;
        //        console.log(dom);
        //    });
        //}

    },

    setUpTimeout: function() {
        if(this.isDesktop()) {
            var enabled = this.config.display_timer_desktop;
            var seconds = this.config.display_timer_desktop_seconds;
        } else {
            var enabled = this.config.display_timer_mobile;
            var seconds = this.config.display_timer_mobile_seconds;
        }

        if(enabled) {
            this.timeout = setTimeout(this.show.bind(this), seconds*1000);
        } else {
        }
    },

    isDesktop: function() {
        return !this.isMobile();    
    },

    isMobile: function() {
        var check = false;
        (function(a){if(/Mobile|iP(hone|od|ad)|Android|BlackBerry|IEMobile|Kindle|NetFront|Silk-Accelerated|(hpw|web)OS|Fennec|Minimo|Opera M(obi|ini)|Blazer|Dolfin|Dolphin|Skyfire|Zune/i.test(a)) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
        return check;
    }
};


var RouletteManager = function() {
    this.url = window.location.href;
    this.wheels = [];
    this.loadConfig();
};


RouletteManager.prototype.initializeWheel = function(wheel) {
    this.setupOverlay();
    this.setupContainer();
    this.setupIframe();
    this.active_wheel.setUp();
    this.active_wheel.setupIntent();
    this.active_wheel.setupTimeout();
};

RouletteManager.prototype.initializeWheels = function() {
    /* Find first eligeble wheel to initialize */
    for(var x=0; x < this.wheels.length; x++) {
        if(this.wheels[x].display()) {
            this.active_wheel = this.wheels[x];
            this.initializeWheel(this.active_wheel);
            return;
        } else {
            //console.log('Skipping wheel \'' + this.wheels[x].config.name + '\' because shouldn\'t be displayed');
        }
    }
};

RouletteManager.prototype.setupWheels = function() {
    for(var x=0; x < this.config.wheels.length; x++) {
        this.wheels.push(new WheelManager(this.config.wheels[x], this));
    }

    this.initializeWheels();
};



//window.roulette = new RouletteManager();

/* Listagram main class */
var Listagram = function(config) {
    this.config = config;
    this.user_config = false;
    this.fortune_wheels = [];
    this.active_wheel = false;
    this.container = false;
    this.overlay = false;
    this.url = window.location.href;
    this.loadUserConfig();
}

Listagram.prototype = {
    loadUserConfig: function() {
        //var config_url = this.config.base_media + 'user_configs/' + this.config.token + '.json';
        var config_url = '/Scripts/listagram/api/user_configs/f4fff965-8707-4a4f-a201-8ade219a2516.json';
        /* Inject config <script> */
        var script = document.createElement('script');
        script.src = config_url;
        document.body.appendChild(script);
        /* Inject CSS */
        var link = document.createElement('link');
        //link.href = this.config.base_static + 'api/css/listagram.css';
        link.href = '/Scripts/listagram/api/css/listagram.css';
        link.type = 'text/css';
        link.rel = 'stylesheet';
        document.body.appendChild(link);
    },
    
    parseUserConfig: function(json) {
        var config = JSON.parse(json);

        if(!config.success) {
            return;
        }

        this.user_config = config;
        this.setUpWheels();
        this.setUpBinds();
    },

    setUpBinds: function() {
        window.addEventListener("message", function(event) {
            if(event.data.action == 'close') {
                this.hide();
            }
            if(event.data.action == 'setCookie') {
                this.setCookie(event.data.name, event.data.value, event.data.days);
            }
            if(event.data.action == 'redirect') {
                window.location = event.data.url;
            }
        }.bind(this));
    },

    setUpWheels: function () {
        /* If user config has any wheels, lets set them up */
        for(var x=0; x < this.user_config.wheels.length; x++) {
            var wheel = new WheelManager(this.user_config.wheels[x], this);
            if(wheel.display()) {
                this.activateWheel(wheel);
            }
            this.fortune_wheels.push(wheel);
        }
    },

    activateWheel: function(wheel) {
        this.setupOverlay();
        this.setupContainer();

        //var iframe_url = this.config.base_api + 
        //    'iframe/?token=' + this.config.token + 
        //    '&wheel=' + wheel.config.id +
        //    '&url=' + encodeURI(this.url);
        var iframe_url = '../Scripts/listagram/coupon-wheel.html';

        this.setupIframe(iframe_url);

        /* Call setUp on the wheel itself */
        wheel.setUp();
    },

    appendBody: function(elm) {
        document.body.appendChild(elm);
    },

    setupContainer: function() {
        if(this.container) {
            return;
        }

        this.container = document.createElement('div');
        this.container.className = 'listagram-container listagram-container-animation';
        this.appendBody(this.container);
    },

    setupOverlay: function() {
        if(this.overlay) {
            return;
        }

        this.overlay = document.createElement('div');
        this.overlay.className = 'listagram-overlay listagram-overlay-animation';
        this.appendBody(this.overlay);
    },

    show: function () {
        this.container.style.display = 'block';
        this.overlay.style.display = 'block';

        document.body.style.overflow = 'hidden';
        //document.body.style.position = 'relative';
    },

    hide: function() {
        this.container.style.display = 'none';
        this.overlay.style.display = 'none';
        document.body.style.overflow = 'visible';
        //document.body.style.position = 'static';
    },

    setupIframe: function (url) {
        this.iframe = document.createElement('iframe');
        this.iframe.src = url;
        this.iframe.frameBorder = 0;
        this.iframe.style.cssText = 'width: 100%; height: 100%;';
        this.container.appendChild(this.iframe);
    },

    setCookie: function(name, value, days) {
        /* Skip cookie if lifetime is set to 0 days */
        if(days === 0) {
            return;
        }

        if (days) {
            var date = new Date();
            date.setTime(date.getTime()+(days*24*60*60*1000));
            var expires = "; expires="+date.toGMTString();
        }
        else var expires = "";
        document.cookie = name+"="+value+expires+"; path=/";
    },

    getCookie: function(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    },

    delCookie: function(name) {
        this.setCookie(name, '', -1);
    },
};
window.LISTAGRAM = new Listagram(window.eggConfig);
