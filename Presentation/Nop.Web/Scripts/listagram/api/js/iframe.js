﻿

var IframeRoulette = function (config) {
    this.config = config;
    this.winning_slice = false;
    this.setUp();
};

IframeRoulette.prototype.setUp = function () {

    this.config.form.submit(
        this.buttonClick.bind(this));

    this.config.played_button.click(function () {
        this.closeWheel();
        this.setPlayedCookie();

        if (this.winning_slice.type == 'product') {
            this.postMessage({
                'action': 'redirect',
                'url': this.winning_slice.value,
            });
        }

    }.bind(this));

    this.config.close.click(function () {
        //this.saveCloseStatistic();
        this.closeWheel();
        this.setCloseCookie();
    }.bind(this));

    /* Hide prize area */
    this.config.prize_container.hide();

    /* Setup tooltipster for error messages */
    $('#id_email').tooltipster({
        'trigger': 'custom',
        'theme': ['tooltipster-default', 'tooltipster-listagram']
    });

    /* For dev */
    /* 
    //var winning_slice = {"type": "noprize", "number": 1, "value": "GRATIØSFRAKT", "label": "Gratis produkt!"};
    //var winning_slice = {"type": "product", "number": 1, "value": "GRATIØSFRAKT", "label": "https://foobar.com"};
    //var winning_slice = {"type": "coupon", "number": 1, "value": "GRATIØSFRAKT", "label": "Gratis produkt!"};
    this.setupPrizeContainer(winning_slice);
    this.setWinningSlice(winning_slice);
    this.switchSides();
    */

};

IframeRoulette.prototype.saveCloseStatistic = function () {

    if (this.winning_slice) {
        var stat_type = 'rejection_after_spin';
    } else {
        var stat_type = 'rejection_before_spin';
    }

    var data = {
        'token': this.config.token,
        'wheel': this.config.wheel,
        'csrfmiddlewaretoken': this.config.csrfmiddlewaretoken,
        'stat_type': stat_type,
    };

    this.setLoading();

    var xhr = $.post({
        'data': data,
        'dataType': 'json',
        'url': this.config.stats_url,
    });
};

IframeRoulette.prototype.setWinningSlice = function (slice) {
    this.winning_slice = slice;
};


IframeRoulette.prototype.closeWheel = function () {
    this.postMessage({
        'action': 'close',
    });
};

IframeRoulette.prototype.setPlayedCookie = function () {
    this.postMessage({
        'action': 'setCookie',
        'name': this.config.cookie_name,
        'value': 'played',
        'days': this.config.cookie_lifetime,
    });
};

IframeRoulette.prototype.setCloseCookie = function () {
    this.postMessage({
        'action': 'setCookie',
        'name': this.config.cookie_name,
        'value': 'closed',
        'days': this.config.cookie_lifetime,
    });
};

IframeRoulette.prototype.postMessage = function (data) {
    window.parent.postMessage(data, '*');
};

IframeRoulette.prototype.switchSides = function () {
    this.config.form_container.fadeOut(300, function () {
        this.config.prize_container.fadeIn();
    }.bind(this));
};

IframeRoulette.prototype.buttonClick = function (e) {
    e.preventDefault();

    // We need to blur the email field to make keyboards go away
    this.config.email.blur();

    if (this.config.email.val() == '') {
        this.setError(this.config.translation.email_invalid);
    } else {
        if (!this.validateEmail(this.config.email.val())) {
            this.setError(this.config.translation.email_invalid);
        } else {
            var data = {
                'token': this.config.token,
                'wheel': this.config.wheel,
                'email': this.config.email.val(),
                'csrfmiddlewaretoken': this.config.csrfmiddlewaretoken,
                'url': window.location.href,
            };
            this.setLoading();
            var xhr = $.post({
                'data': data,
                'dataType': 'json',
                'url': '/EggDiscount/CouponWheel',
            });

            xhr.done(function (response) {
                if (response.success) {
                    this.stopLoading();
                    this.config.fortunewheel.setWinningSlice(response.winning_slice.number);
                    this.setWinningSlice(response.winning_slice);
                    this.config.fortunewheel.spin(function () {
                        this.setupPrizeContainer(response.winning_slice);
                        this.switchSides();
                        this.setPlayedCookie();
                    }.bind(this));
                } else {
                    this.setError(response.error);
                }
            }.bind(this));
        }
    }
};

IframeRoulette.prototype.setupPrizeContainer = function (winning_slice) {
    var title = this.config.prize_container.find('.roulette-iframe-right-prize-title');
    var desc = this.config.prize_container.find('.roulette-iframe-right-prize-description');
    var btn_div = this.config.prize_container.find('.roulette-iframe-right-prize-button');
    var btn = this.config.prize_container.find('.roulette-iframe-right-prize-button button');
    var coupon_div = this.config.prize_container.find('.roulette-iframe-right-prize-coupon');
    var coupon_input = this.config.prize_container.find('.roulette-iframe-right-prize-coupon input');


    //this.config.prize_container.show();

    if (winning_slice.type == 'coupon') {
        title.html(this.config.translation.prize_title.replace('%s', winning_slice.label));
        desc.html(this.config.translation.prize_description_coupon);
        coupon_div.show();
        coupon_input.val(winning_slice.value);
        btn.html(this.config.translation.button_coupon);
    } else if (winning_slice.type == 'noprize') {
        title.html(this.config.translation.noprize_title);
        desc.html(this.config.translation.noprize_description);
        btn.html(this.config.translation.button_noprize);
    } else if (winning_slice.type == 'product') {
        title.html(this.config.translation.prize_title.replace('%s', winning_slice.label));
        desc.html(this.config.translation.prize_description_freeproduct);
        btn.html(this.config.translation.button_freeproduct);
    }
};

IframeRoulette.prototype.validateEmail = function validateEmail(email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test(email);
};

IframeRoulette.prototype.setLoading = function () {

};

IframeRoulette.prototype.stopLoading = function () {

};

IframeRoulette.prototype.setError = function (error) {
    $('#id_email').tooltipster('content', error);
    $('#id_email').tooltipster('show');

    setTimeout(function () {
        $('#id_email').tooltipster('hide');
    }, 2000);
    //this.config.message.html(error);
    //this.config.message.addClass('roulette-iframe-form-error');
};

IframeRoulette.prototype.resetMessage = function () {
    this.config.message.removeClass('roulette-iframe-form-error');
    this.config.message.html('');
}
