﻿using System.Web.Mvc;
using Nop.Web.Framework.Security;
using Nop.Core;
using Nop.Web.Extensions;

namespace Nop.Web.Controllers
{
    public partial class HomeController : BasePublicController
    {
        private readonly IStoreContext _storeContext;
        public HomeController(IStoreContext storeContext)
        {
            this._storeContext = storeContext;
        }

        [NopHttpsRequirement(SslRequirement.No)]
        public ActionResult Index()
        {
            _storeContext.CurrentStore.FacebookPixelAffiliateIds = EggCommon.StringWithDelimiterToList(_storeContext.CurrentStore.FacebookPixelAffiliateId);
            ViewBag.CurrentStore = _storeContext.CurrentStore;
            return View();
        }
    }
}
