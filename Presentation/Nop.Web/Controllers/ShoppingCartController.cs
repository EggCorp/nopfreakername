﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Discounts;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Shipping;
using Nop.Core.Domain.Tax;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Shipping;
using Nop.Services.Tax;
using Nop.Web.Extensions;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc;
using Nop.Web.Framework.Security;
using Nop.Web.Framework.Security.Captcha;
using Nop.Web.Infrastructure.Cache;
using Nop.Web.Models.Common;
using Nop.Web.Models.Media;
using Nop.Web.Models.ShoppingCart;
using Nop.Web.Models.Catalog;
using System.Web.Script.Serialization;
using static Nop.Web.Models.ShoppingCart.ShoppingCartModel;
using Newtonsoft.Json;
using System.Collections.Specialized;
using Nop.Services.SubscribeMessenger;
using System.Text;

namespace Nop.Web.Controllers
{
    public partial class ShoppingCartController : BasePublicController
    {
        #region Fields

        private readonly IProductService _productService;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly IPictureService _pictureService;
        private readonly ILocalizationService _localizationService;
        private readonly IProductAttributeService _productAttributeService;
        private readonly IProductAttributeFormatter _productAttributeFormatter;
        private readonly IProductAttributeParser _productAttributeParser;
        private readonly ITaxService _taxService;
        private readonly ICurrencyService _currencyService;
        private readonly IPriceCalculationService _priceCalculationService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly ICheckoutAttributeParser _checkoutAttributeParser;
        private readonly ICheckoutAttributeFormatter _checkoutAttributeFormatter;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly IDiscountService _discountService;
        private readonly ICustomerService _customerService;
        private readonly IGiftCardService _giftCardService;
        private readonly ICountryService _countryService;
        private readonly IStateProvinceService _stateProvinceService;
        private readonly IShippingService _shippingService;
        private readonly IOrderTotalCalculationService _orderTotalCalculationService;
        private readonly ICheckoutAttributeService _checkoutAttributeService;
        private readonly IPaymentService _paymentService;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly IPermissionService _permissionService;
        private readonly IDownloadService _downloadService;
        private readonly ICacheManager _cacheManager;
        private readonly IWebHelper _webHelper;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IAddressAttributeFormatter _addressAttributeFormatter;
        private readonly HttpContextBase _httpContext;
        private readonly ICategoryService _categoryService;
        private readonly ISpecificationAttributeService _specificationAttributeService;
        private readonly IMeasureService _measureService;

        private readonly MediaSettings _mediaSettings;
        private readonly ShoppingCartSettings _shoppingCartSettings;
        private readonly CatalogSettings _catalogSettings;
        private readonly OrderSettings _orderSettings;
        private readonly ShippingSettings _shippingSettings;
        private readonly TaxSettings _taxSettings;
        private readonly CaptchaSettings _captchaSettings;
        private readonly AddressSettings _addressSettings;
        private readonly RewardPointsSettings _rewardPointsSettings;
        private readonly CustomerSettings _customerSettings;
        private readonly ILogger _logger;

        private readonly IProductCustomFieldService _productCustomFieldService;
        private readonly IRenderPictureService _renderPictureService;
        private readonly ISubscribeCustomerService _subscribeCustomerService;
        private readonly ISubscribeActivityLogService _subscribeActivityLogService;
        private readonly ISubscribeMessageService _subscribeMessageService;
        #endregion

        #region Constructors

        public ShoppingCartController(IProductService productService,
            IStoreContext storeContext,
            IWorkContext workContext,
            IShoppingCartService shoppingCartService,
            IPictureService pictureService,
            ILocalizationService localizationService,
            IProductAttributeService productAttributeService,
            IProductAttributeFormatter productAttributeFormatter,
            IProductAttributeParser productAttributeParser,
            ITaxService taxService, ICurrencyService currencyService,
            IPriceCalculationService priceCalculationService,
            IPriceFormatter priceFormatter,
            ICheckoutAttributeParser checkoutAttributeParser,
            ICheckoutAttributeFormatter checkoutAttributeFormatter,
            IOrderProcessingService orderProcessingService,
            IDiscountService discountService,
            ICustomerService customerService,
            IGiftCardService giftCardService,
            ICountryService countryService,
            IStateProvinceService stateProvinceService,
            IShippingService shippingService,
            IOrderTotalCalculationService orderTotalCalculationService,
            ICheckoutAttributeService checkoutAttributeService,
            IPaymentService paymentService,
            IWorkflowMessageService workflowMessageService,
            IPermissionService permissionService,
            IDownloadService downloadService,
            ICacheManager cacheManager,
            IWebHelper webHelper,
            ICustomerActivityService customerActivityService,
            IGenericAttributeService genericAttributeService,
            IAddressAttributeFormatter addressAttributeFormatter,
            HttpContextBase httpContext,
            MediaSettings mediaSettings,
            ShoppingCartSettings shoppingCartSettings,
            CatalogSettings catalogSettings,
            OrderSettings orderSettings,
            ShippingSettings shippingSettings,
            TaxSettings taxSettings,
            CaptchaSettings captchaSettings,
            AddressSettings addressSettings,
            RewardPointsSettings rewardPointsSettings,
            CustomerSettings customerSettings,
            ICategoryService categoryService,
            ISpecificationAttributeService specificationAttributeService,
            IMeasureService measureService,
            ILogger logger,
            IProductCustomFieldService productCustomFieldService,
            IRenderPictureService renderPictureService,
            ISubscribeCustomerService subscribeCustomerService,
            ISubscribeActivityLogService subscribeActivityLogService,
            ISubscribeMessageService subscribeMessageService)
        {
            this._productService = productService;
            this._workContext = workContext;
            this._storeContext = storeContext;
            this._shoppingCartService = shoppingCartService;
            this._pictureService = pictureService;
            this._localizationService = localizationService;
            this._productAttributeService = productAttributeService;
            this._productAttributeFormatter = productAttributeFormatter;
            this._productAttributeParser = productAttributeParser;
            this._taxService = taxService;
            this._currencyService = currencyService;
            this._priceCalculationService = priceCalculationService;
            this._priceFormatter = priceFormatter;
            this._checkoutAttributeParser = checkoutAttributeParser;
            this._checkoutAttributeFormatter = checkoutAttributeFormatter;
            this._orderProcessingService = orderProcessingService;
            this._discountService = discountService;
            this._customerService = customerService;
            this._giftCardService = giftCardService;
            this._countryService = countryService;
            this._stateProvinceService = stateProvinceService;
            this._shippingService = shippingService;
            this._orderTotalCalculationService = orderTotalCalculationService;
            this._checkoutAttributeService = checkoutAttributeService;
            this._paymentService = paymentService;
            this._workflowMessageService = workflowMessageService;
            this._permissionService = permissionService;
            this._downloadService = downloadService;
            this._cacheManager = cacheManager;
            this._webHelper = webHelper;
            this._customerActivityService = customerActivityService;
            this._genericAttributeService = genericAttributeService;
            this._addressAttributeFormatter = addressAttributeFormatter;
            this._httpContext = httpContext;
            this._categoryService = categoryService;
            this._specificationAttributeService = specificationAttributeService;
            this._measureService = measureService;

            this._mediaSettings = mediaSettings;
            this._shoppingCartSettings = shoppingCartSettings;
            this._catalogSettings = catalogSettings;
            this._orderSettings = orderSettings;
            this._shippingSettings = shippingSettings;
            this._taxSettings = taxSettings;
            this._captchaSettings = captchaSettings;
            this._addressSettings = addressSettings;
            this._rewardPointsSettings = rewardPointsSettings;
            this._customerSettings = customerSettings;
            this._logger = logger;

            this._productCustomFieldService = productCustomFieldService;
            this._renderPictureService = renderPictureService;
            this._subscribeCustomerService = subscribeCustomerService;
            this._subscribeActivityLogService = subscribeActivityLogService;
            this._subscribeMessageService = subscribeMessageService;
        }

        #endregion

        #region Utilities

        [NonAction]
        protected virtual PictureModel PrepareCartItemPictureModel(ShoppingCartItem sci,
            int pictureSize, bool showDefaultPicture, string productName, RenderPictureRequest render)
        {
            string imageUrl = "";
            if (render != null)
                imageUrl = this.GetPictureRenderUrl(Url.RouteUrl("RenderProductImage"), render);
            else
            {
                var sciPicture = sci.Product.GetProductPicture(sci.AttributesXml, _pictureService, _productAttributeParser);
                imageUrl = _pictureService.GetPictureUrl(sciPicture, pictureSize, showDefaultPicture);
            }
            return new PictureModel
            {
                ImageUrl = imageUrl,
                Title = string.Format(_localizationService.GetResource("Media.Product.ImageLinkTitleFormat"), productName),
                AlternateText = string.Format(_localizationService.GetResource("Media.Product.ImageAlternateTextFormat"), productName),
            };
        }

        /// <summary>
        /// Prepare shopping cart model
        /// </summary>
        /// <param name="model">Model instance</param>
        /// <param name="cart">Shopping cart</param>
        /// <param name="isEditable">A value indicating whether cart is editable</param>
        /// <param name="validateCheckoutAttributes">A value indicating whether we should validate checkout attributes when preparing the model</param>
        /// <param name="prepareEstimateShippingIfEnabled">A value indicating whether we should prepare "Estimate shipping" model</param>
        /// <param name="setEstimateShippingDefaultAddress">A value indicating whether we should prefill "Estimate shipping" model with the default customer address</param>
        /// <param name="prepareAndDisplayOrderReviewData">A value indicating whether we should prepare review data (such as billing/shipping address, payment or shipping data entered during checkout)</param>
        /// <returns>Model</returns>
        [NonAction]
        protected virtual void PrepareShoppingCartModel(ShoppingCartModel model,
            IList<ShoppingCartItem> cart, bool isEditable = true,
            bool validateCheckoutAttributes = false,
            bool prepareEstimateShippingIfEnabled = true, bool setEstimateShippingDefaultAddress = true,
            bool prepareAndDisplayOrderReviewData = false)
        {
            if (cart == null)
                throw new ArgumentNullException("cart");

            if (model == null)
                throw new ArgumentNullException("model");

            model.OnePageCheckoutEnabled = _orderSettings.OnePageCheckoutEnabled;

            // set standar shipping method
            _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedShippingMethod, 0);


            if (!cart.Any())
                return;

            #region Simple properties

            model.IsEditable = isEditable;
            model.ShowProductImages = _shoppingCartSettings.ShowProductImagesOnShoppingCart;
            model.ShowSku = _catalogSettings.ShowProductSku;
            var checkoutAttributesXml = _workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.CheckoutAttributes, _genericAttributeService, _storeContext.CurrentStore.Id);
            model.CheckoutAttributeInfo = _checkoutAttributeFormatter.FormatAttributes(checkoutAttributesXml, _workContext.CurrentCustomer);
            bool minOrderSubtotalAmountOk = _orderProcessingService.ValidateMinOrderSubtotalAmount(cart);
            if (!minOrderSubtotalAmountOk)
            {
                decimal minOrderSubtotalAmount = _currencyService.ConvertFromPrimaryStoreCurrency(_orderSettings.MinOrderSubtotalAmount, _workContext.WorkingCurrency);
                model.MinOrderSubtotalWarning = string.Format(_localizationService.GetResource("Checkout.MinOrderSubtotalAmount"), _priceFormatter.FormatPrice(minOrderSubtotalAmount, true, false));
            }
            model.TermsOfServiceOnShoppingCartPage = _orderSettings.TermsOfServiceOnShoppingCartPage;
            model.TermsOfServiceOnOrderConfirmPage = _orderSettings.TermsOfServiceOnOrderConfirmPage;
            model.DisplayTaxShippingInfo = _catalogSettings.DisplayTaxShippingInfoShoppingCart;

            //gift card and gift card boxes
            model.DiscountBox.Display = _shoppingCartSettings.ShowDiscountBox;
            var discountCouponCode = _workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.DiscountCouponCode);
            var discount = _discountService.GetDiscountByCouponCode(discountCouponCode);
            if (discount != null &&
                discount.RequiresCouponCode &&
                _discountService.ValidateDiscount(discount, _workContext.CurrentCustomer).IsValid)
                model.DiscountBox.CurrentCode = discount.CouponCode;
            model.GiftCardBox.Display = _shoppingCartSettings.ShowGiftCardBox;

            //cart warnings
            var cartWarnings = _shoppingCartService.GetShoppingCartWarnings(cart, checkoutAttributesXml, validateCheckoutAttributes);
            foreach (var warning in cartWarnings)
                model.Warnings.Add(warning);

            #endregion

            #region Checkout attributes

            var checkoutAttributes = _checkoutAttributeService.GetAllCheckoutAttributes(_storeContext.CurrentStore.Id, !cart.RequiresShipping());
            foreach (var attribute in checkoutAttributes)
            {
                var attributeModel = new ShoppingCartModel.CheckoutAttributeModel
                {
                    Id = attribute.Id,
                    Name = attribute.GetLocalized(x => x.Name),
                    TextPrompt = attribute.GetLocalized(x => x.TextPrompt),
                    IsRequired = attribute.IsRequired,
                    AttributeControlType = attribute.AttributeControlType,
                    DefaultValue = attribute.DefaultValue
                };
                if (!String.IsNullOrEmpty(attribute.ValidationFileAllowedExtensions))
                {
                    attributeModel.AllowedFileExtensions = attribute.ValidationFileAllowedExtensions
                        .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                        .ToList();
                }

                if (attribute.ShouldHaveValues())
                {
                    //values
                    var attributeValues = _checkoutAttributeService.GetCheckoutAttributeValues(attribute.Id);
                    foreach (var attributeValue in attributeValues)
                    {
                        var attributeValueModel = new ShoppingCartModel.CheckoutAttributeValueModel
                        {
                            Id = attributeValue.Id,
                            Name = attributeValue.GetLocalized(x => x.Name),
                            ColorSquaresRgb = attributeValue.ColorSquaresRgb,
                            IsPreSelected = attributeValue.IsPreSelected,
                        };
                        attributeModel.Values.Add(attributeValueModel);

                        //display price if allowed
                        if (_permissionService.Authorize(StandardPermissionProvider.DisplayPrices))
                        {
                            decimal priceAdjustmentBase = _taxService.GetCheckoutAttributePrice(attributeValue);
                            decimal priceAdjustment = _currencyService.ConvertFromPrimaryStoreCurrency(priceAdjustmentBase, _workContext.WorkingCurrency);
                            if (priceAdjustmentBase > decimal.Zero)
                                attributeValueModel.PriceAdjustment = "+" + _priceFormatter.FormatPrice(priceAdjustment);
                            else if (priceAdjustmentBase < decimal.Zero)
                                attributeValueModel.PriceAdjustment = "-" + _priceFormatter.FormatPrice(-priceAdjustment);
                        }
                    }
                }



                //set already selected attributes
                var selectedCheckoutAttributes = _workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.CheckoutAttributes, _genericAttributeService, _storeContext.CurrentStore.Id);
                switch (attribute.AttributeControlType)
                {
                    case AttributeControlType.DropdownList:
                    case AttributeControlType.RadioList:
                    case AttributeControlType.Checkboxes:
                    case AttributeControlType.ColorSquares:
                    case AttributeControlType.ImageSquares:
                        {
                            if (!String.IsNullOrEmpty(selectedCheckoutAttributes))
                            {
                                //clear default selection
                                foreach (var item in attributeModel.Values)
                                    item.IsPreSelected = false;

                                //select new values
                                var selectedValues = _checkoutAttributeParser.ParseCheckoutAttributeValues(selectedCheckoutAttributes);
                                foreach (var attributeValue in selectedValues)
                                    foreach (var item in attributeModel.Values)
                                        if (attributeValue.Id == item.Id)
                                            item.IsPreSelected = true;
                            }
                        }
                        break;
                    case AttributeControlType.ReadonlyCheckboxes:
                        {
                            //do nothing
                            //values are already pre-set
                        }
                        break;
                    case AttributeControlType.TextBox:
                    case AttributeControlType.MultilineTextbox:
                        {
                            if (!String.IsNullOrEmpty(selectedCheckoutAttributes))
                            {
                                var enteredText = _checkoutAttributeParser.ParseValues(selectedCheckoutAttributes, attribute.Id);
                                if (enteredText.Any())
                                    attributeModel.DefaultValue = enteredText[0];
                            }
                        }
                        break;
                    case AttributeControlType.Datepicker:
                        {
                            //keep in mind my that the code below works only in the current culture
                            var selectedDateStr = _checkoutAttributeParser.ParseValues(selectedCheckoutAttributes, attribute.Id);
                            if (selectedDateStr.Any())
                            {
                                DateTime selectedDate;
                                if (DateTime.TryParseExact(selectedDateStr[0], "D", CultureInfo.CurrentCulture,
                                                       DateTimeStyles.None, out selectedDate))
                                {
                                    //successfully parsed
                                    attributeModel.SelectedDay = selectedDate.Day;
                                    attributeModel.SelectedMonth = selectedDate.Month;
                                    attributeModel.SelectedYear = selectedDate.Year;
                                }
                            }

                        }
                        break;
                    case AttributeControlType.FileUpload:
                        {
                            if (!String.IsNullOrEmpty(selectedCheckoutAttributes))
                            {
                                var downloadGuidStr = _checkoutAttributeParser.ParseValues(selectedCheckoutAttributes, attribute.Id).FirstOrDefault();
                                Guid downloadGuid;
                                Guid.TryParse(downloadGuidStr, out downloadGuid);
                                var download = _downloadService.GetDownloadByGuid(downloadGuid);
                                if (download != null)
                                    attributeModel.DefaultValue = download.DownloadGuid.ToString();
                            }
                        }
                        break;
                    default:
                        break;
                }

                model.CheckoutAttributes.Add(attributeModel);
            }

            #endregion 

            #region Estimate shipping

            if (prepareEstimateShippingIfEnabled)
            {
                model.EstimateShipping.Enabled = cart.Any() && cart.RequiresShipping() && _shippingSettings.EstimateShippingEnabled;
                if (model.EstimateShipping.Enabled)
                {
                    //countries
                    int? defaultEstimateCountryId = (setEstimateShippingDefaultAddress && _workContext.CurrentCustomer.ShippingAddress != null) ? _workContext.CurrentCustomer.ShippingAddress.CountryId : model.EstimateShipping.CountryId;
                    model.EstimateShipping.AvailableCountries.Add(new SelectListItem { Text = _localizationService.GetResource("Address.SelectCountry"), Value = "0" });
                    foreach (var c in _countryService.GetAllCountriesForShipping(_workContext.WorkingLanguage.Id))
                        model.EstimateShipping.AvailableCountries.Add(new SelectListItem
                        {
                            Text = c.GetLocalized(x => x.Name),
                            Value = c.Id.ToString(),
                            Selected = c.Id == defaultEstimateCountryId
                        });
                    //states
                    int? defaultEstimateStateId = (setEstimateShippingDefaultAddress && _workContext.CurrentCustomer.ShippingAddress != null) ? _workContext.CurrentCustomer.ShippingAddress.StateProvinceId : model.EstimateShipping.StateProvinceId;
                    var states = defaultEstimateCountryId.HasValue ? _stateProvinceService.GetStateProvincesByCountryId(defaultEstimateCountryId.Value, _workContext.WorkingLanguage.Id).ToList() : new List<StateProvince>();
                    if (states.Any())
                        foreach (var s in states)
                            model.EstimateShipping.AvailableStates.Add(new SelectListItem
                            {
                                Text = s.GetLocalized(x => x.Name),
                                Value = s.Id.ToString(),
                                Selected = s.Id == defaultEstimateStateId
                            });
                    else
                        model.EstimateShipping.AvailableStates.Add(new SelectListItem { Text = _localizationService.GetResource("Address.OtherNonUS"), Value = "0" });

                    if (setEstimateShippingDefaultAddress && _workContext.CurrentCustomer.ShippingAddress != null)
                        model.EstimateShipping.ZipPostalCode = _workContext.CurrentCustomer.ShippingAddress.ZipPostalCode;
                }
            }

            #endregion

            #region Cart items
            var validatedCartItems = new List<ShoppingCartItem>();
            var errorCartItems = new List<ShoppingCartItem>();
            Exception errorCart = null;
            List<SelectListItem> _remarketingCartItems = new List<SelectListItem>();
            foreach (var sci in cart)
            {
                try
                {
                    var cartItemModel = new ShoppingCartModel.ShoppingCartItemModel
                    {
                        Id = sci.Id,
                        Sku = sci.Product.FormatSku(sci.AttributesXml, _productAttributeParser),
                        ProductId = sci.Product.Id,
                        ProductName = sci.Product.GetLocalized(x => x.Name),
                        ProductSeName = sci.Product.GetSeName(),
                        Quantity = sci.Quantity,
                        AttributeInfo = _productAttributeFormatter.FormatAttributes(sci.Product, sci.AttributesXml),
                        AttributeXml = sci.AttributesXml
                    };

                    var itemAttributeMappings = sci.Product.ProductAttributeMappings;
                    var itemAttributeValues = _productAttributeParser.ParseProductAttributeValues(sci.AttributesXml);
                    var itemSelectedAttributeMappings = _productAttributeParser.ParseProductAttributeMappings(sci.AttributesXml);

                    ProductAttributeValue selectedSize = null;
                    ProductAttributeValue selectedColor = null;
                    ProductAttributeValue selectedStyle = null;

                    //Parse Cart item style
                    var allStyle = itemAttributeMappings.Where(p => p.ProductAttribute.Name.Equals(EggCommon.ProductTypeAttributeValue))
                       .Select(iam => iam.ProductAttributeValues).FirstOrDefault();
                    selectedStyle = itemAttributeValues.Where(p => p.ProductAttributeMappingId == allStyle.FirstOrDefault().ProductAttributeMappingId).FirstOrDefault();

                    //parse cart item sizing
                    var selectedSizingAttrMapping = itemSelectedAttributeMappings.Where(p => p.ProductAttribute.Name.Substring(0, 5).Equals("size_")).FirstOrDefault();
                    if (selectedSizingAttrMapping != null)
                    {
                        foreach (var cartAttrValue in itemAttributeValues)
                        {
                            foreach (var itemSizeAttrValue in selectedSizingAttrMapping.ProductAttributeValues)
                            {
                                if (cartAttrValue.Id == itemSizeAttrValue.Id)
                                {
                                    selectedSize = cartAttrValue;
                                    break;
                                }
                            }
                        }
                    }

                    //parse cart item color

                    var allColor = itemAttributeMappings.Where(p => p.AttributeControlType == AttributeControlType.ColorSquares).ToList();
                    selectedColor = itemAttributeValues.Where(p => p.ProductAttributeMapping.AttributeControlType==AttributeControlType.ColorSquares).FirstOrDefault();


                    //prepare dropdown sizing change
                    if (selectedSizingAttrMapping != null)
                    {
                        var productAllSizing = itemAttributeMappings.Where(p => p.ProductAttribute.Name.Substring(0, 5).Equals("size_"))
                     .Select(iam => iam.ProductAttributeValues).ToList();
                        foreach (var size in selectedSizingAttrMapping.ProductAttributeValues.OrderBy(p => p.Id).ToList())
                        {
                            if (selectedSize != null)
                            {
                                var tempSize = new ShoppingCartModel.ShoppingCartAttributeValueModel
                                {
                                    Id = size.Id,
                                    ColorSquaresRgb = size.ColorSquaresRgb,
                                    IsPreSelected = size.Id == selectedSize.Id,
                                    Name = size.Name,
                                    PriceAdjustment = size.PriceAdjustment == decimal.Zero ? "" : _priceFormatter.FormatPrice(_currencyService.ConvertFromPrimaryStoreCurrency(size.PriceAdjustment, _workContext.WorkingCurrency))
                                };
                                cartItemModel.SizingAttributeValues.Add(tempSize);
                            }
                        }
                    }
                    

                    //prepare dropdown style change
                    foreach (var styleAttrValue in allStyle)
                    {
                        foreach (var colorAttrValue in allColor)
                        {
                            var valuesThatShouldBeSelected = _productAttributeParser.ParseConditionValue(colorAttrValue.ConditionAttributeXml, styleAttrValue.ProductAttributeMappingId, styleAttrValue.Id).ToList();
                            if (valuesThatShouldBeSelected.Any())
                            {
                                foreach (var item in colorAttrValue.ProductAttributeValues)
                                {
                                    var tempStyle = new ShoppingCartModel.ShoppingCartAttributeValueModel
                                    {
                                        Id = item.Id,
                                        ProductTypeId = styleAttrValue.Id,
                                        ColorSquaresRgb = item.ColorSquaresRgb,
                                        IsPreSelected = item.Id == selectedColor.Id,
                                        Name = string.Format("{0} - {1}", styleAttrValue.Name, item.Name)
                                    };
                                    cartItemModel.StyleAttributeValues.Add(tempStyle);
                                }
                            }
                        }
                    }


                    //parse cart item short information
                    StringBuilder shorInfo = new StringBuilder();
                    var customData = _productAttributeParser.ParserCartItemCustomData(sci.AttributesXml);

                    shorInfo.Append("<p class=\"egg-item-content-des\">");
                    var customText = "";
                    if (customData != null)
                    {
                        foreach (var data in customData)
                        {
                            var customField = _productCustomFieldService.GetProductCustomFieldById(data.Id);
                            if (customField != null)
                                customText += string.Format("{0}: {1} ", customField.Name, data.Text);
                        }
                    }
                    shorInfo.Append(customText);
                    shorInfo.Append("</p>");
                    shorInfo.Append("<p class=\"egg-item-content-des\">");
                    var productInfo = "";
                    if (selectedStyle != null)
                        productInfo += selectedStyle.Name;
                    if (selectedSize != null)
                        productInfo += string.Format(" - {0}", selectedSize.Name);
                    if (selectedColor != null)
                        productInfo += string.Format(" - {0}", selectedColor.Name);
                    shorInfo.Append("<p class=\"egg-item-content-des\">");
                    shorInfo.Append(productInfo);
                    shorInfo.Append("</p>");

                    cartItemModel.AttributeParsed = shorInfo.ToString();

                    //marketing ids
                    var remarketingId = EggCommon.GetProductRemarketingId(sci.ProductId, selectedStyle != null ? selectedStyle.Id : 0, selectedColor != null ? selectedColor.Id : 0);
                    _remarketingCartItems.Add(new SelectListItem {
                        Text= sci.Id.ToString(),
                        Value = remarketingId,
                    });

                    //picture
                    if (_shoppingCartSettings.ShowProductImagesOnShoppingCart)
                    {

                        var renderPictureRequest = new RenderPictureRequest();
                        renderPictureRequest.Data = _productAttributeParser.ParserCartItemCustomData(sci.AttributesXml).ToList();
                        renderPictureRequest.Scale = _mediaSettings.CartThumbPictureSize;
                        if (renderPictureRequest.Data.Any())
                        {
                            renderPictureRequest.AttrValueId = selectedColor.Id;
                            //var  pictureRenderedUrl = this.GetPictureRenderUrl(Url.RouteUrl("RenderProductImage"), renderPictureRequest);
                            cartItemModel.Picture = PrepareCartItemPictureModel(sci,
                                _mediaSettings.CartThumbPictureSize, true, cartItemModel.ProductName, renderPictureRequest);
                        }
                        else
                        {
                            cartItemModel.Picture = PrepareCartItemPictureModel(sci,
                           _mediaSettings.CartThumbPictureSize, true, cartItemModel.ProductName, null);
                        }

                    }


                    //allow editing?
                    //1. setting enabled?
                    //2. simple product?
                    //3. has attribute or gift card?
                    //4. visible individually?
                    cartItemModel.AllowItemEditing = _shoppingCartSettings.AllowCartItemEditing &&
                        sci.Product.ProductType == ProductType.SimpleProduct &&
                        (!String.IsNullOrEmpty(cartItemModel.AttributeInfo) || sci.Product.IsGiftCard) &&
                        sci.Product.VisibleIndividually;

                    //allowed quantities
                    var allowedQuantities = sci.Product.ParseAllowedQuantities();
                    foreach (var qty in allowedQuantities)
                    {
                        cartItemModel.AllowedQuantities.Add(new SelectListItem
                        {
                            Text = qty.ToString(),
                            Value = qty.ToString(),
                            Selected = sci.Quantity == qty
                        });
                    }

                    //recurring info
                    if (sci.Product.IsRecurring)
                        cartItemModel.RecurringInfo = string.Format(_localizationService.GetResource("ShoppingCart.RecurringPeriod"), sci.Product.RecurringCycleLength, sci.Product.RecurringCyclePeriod.GetLocalizedEnum(_localizationService, _workContext));

                    //rental info
                    if (sci.Product.IsRental)
                    {
                        var rentalStartDate = sci.RentalStartDateUtc.HasValue ? sci.Product.FormatRentalDate(sci.RentalStartDateUtc.Value) : "";
                        var rentalEndDate = sci.RentalEndDateUtc.HasValue ? sci.Product.FormatRentalDate(sci.RentalEndDateUtc.Value) : "";
                        cartItemModel.RentalInfo = string.Format(_localizationService.GetResource("ShoppingCart.Rental.FormattedDate"),
                            rentalStartDate, rentalEndDate);
                    }

                    //unit prices
                    if (sci.Product.CallForPrice)
                    {
                        cartItemModel.UnitPrice = _localizationService.GetResource("Products.CallForPrice");
                    }
                    else
                    {
                        decimal taxRate;
                        decimal shoppingCartUnitPriceWithDiscountBase = _taxService.GetProductPrice(sci.Product, _priceCalculationService.GetUnitPrice(sci), out taxRate);
                        decimal shoppingCartUnitPriceWithDiscount = _currencyService.ConvertFromPrimaryStoreCurrency(shoppingCartUnitPriceWithDiscountBase, _workContext.WorkingCurrency);
                        cartItemModel.StoreCurrencyPrice = _priceCalculationService.GetUnitPrice(sci);
                        cartItemModel.UnitPrice = _priceFormatter.FormatPrice(shoppingCartUnitPriceWithDiscount);
                    }
                    //subtotal, discount
                    if (sci.Product.CallForPrice)
                    {
                        cartItemModel.SubTotal = _localizationService.GetResource("Products.CallForPrice");
                    }
                    else
                    {
                        //sub total
                        List<Discount> scDiscounts;
                        decimal shoppingCartItemDiscountBase;
                        decimal taxRate;
                        decimal shoppingCartItemSubTotalWithDiscountBase = _taxService.GetProductPrice(sci.Product, _priceCalculationService.GetSubTotal(sci, true, out shoppingCartItemDiscountBase, out scDiscounts), out taxRate);
                        decimal shoppingCartItemSubTotalWithDiscount = _currencyService.ConvertFromPrimaryStoreCurrency(shoppingCartItemSubTotalWithDiscountBase, _workContext.WorkingCurrency);
                        cartItemModel.SubTotal = _priceFormatter.FormatPrice(shoppingCartItemSubTotalWithDiscount);

                        //display an applied discount amount
                        if (shoppingCartItemDiscountBase > decimal.Zero)
                        {
                            shoppingCartItemDiscountBase = _taxService.GetProductPrice(sci.Product, shoppingCartItemDiscountBase, out taxRate);
                            if (shoppingCartItemDiscountBase > decimal.Zero)
                            {
                                decimal shoppingCartItemDiscount = _currencyService.ConvertFromPrimaryStoreCurrency(shoppingCartItemDiscountBase, _workContext.WorkingCurrency);
                                cartItemModel.Discount = _priceFormatter.FormatPrice(shoppingCartItemDiscount);
                            }
                        }
                    }

                    //item warnings
                    var itemWarnings = _shoppingCartService.GetShoppingCartItemWarnings(
                        _workContext.CurrentCustomer,
                        sci.ShoppingCartType,
                        sci.Product,
                        sci.StoreId,
                        sci.AttributesXml,
                        sci.CustomerEnteredPrice,
                        sci.RentalStartDateUtc,
                        sci.RentalEndDateUtc,
                        sci.Quantity,
                        false);
                    foreach (var warning in itemWarnings)
                        cartItemModel.Warnings.Add(warning);

                    model.Items.Add(cartItemModel);
                    validatedCartItems.Add(sci);
                }
                catch(Exception ex)
                {
                    errorCartItems.Add(sci);
                    errorCart = ex;
                }
            }


            //apply validated cart item
            if(validatedCartItems.Count != cart.Count)
            {
                cart = validatedCartItems;

                //delete error cart item
                if(errorCartItems.Any())
                {
                    foreach(var erItem in errorCartItems)
                    {
                        _shoppingCartService.DeleteShoppingCartItem(erItem, ensureOnlyActiveCheckoutAttributes :true);
                        _logger.Error(string.Format("Error cart item- productid: {0}", erItem.ProductId), errorCart,_workContext.CurrentCustomer);
                    }
                }
            }
            #endregion

            #region Button payment methods

            var paymentMethods = _paymentService
                .LoadActivePaymentMethods(_workContext.CurrentCustomer.Id, _storeContext.CurrentStore.Id)
                .Where(pm => pm.PaymentMethodType == PaymentMethodType.Button)
                .Where(pm => !pm.HidePaymentMethod(cart))
                .ToList();
            foreach (var pm in paymentMethods)
            {
                if (cart.IsRecurring() && pm.RecurringPaymentType == RecurringPaymentType.NotSupported)
                    continue;

                string actionName;
                string controllerName;
                RouteValueDictionary routeValues;
                pm.GetPaymentInfoRoute(out actionName, out controllerName, out routeValues);

                model.ButtonPaymentMethodActionNames.Add(actionName);
                model.ButtonPaymentMethodControllerNames.Add(controllerName);
                model.ButtonPaymentMethodRouteValues.Add(routeValues);
            }

            #endregion

            #region Order review data

            if (prepareAndDisplayOrderReviewData)
            {
                model.OrderReviewData.Display = true;

                //billing info
                var billingAddress = _workContext.CurrentCustomer.BillingAddress;
                if (billingAddress != null)
                    model.OrderReviewData.BillingAddress.PrepareModel(
                        address: billingAddress,
                        excludeProperties: false,
                        addressSettings: _addressSettings,
                        addressAttributeFormatter: _addressAttributeFormatter);

                //shipping info
                if (cart.RequiresShipping())
                {
                    model.OrderReviewData.IsShippable = true;

                    var pickupPoint = _workContext.CurrentCustomer
                        .GetAttribute<PickupPoint>(SystemCustomerAttributeNames.SelectedPickupPoint, _storeContext.CurrentStore.Id);
                    model.OrderReviewData.SelectedPickUpInStore = _shippingSettings.AllowPickUpInStore && pickupPoint != null;
                    if (!model.OrderReviewData.SelectedPickUpInStore)
                    {
                        if (_workContext.CurrentCustomer.ShippingAddress != null)
                        {
                            model.OrderReviewData.ShippingAddress.PrepareModel(
                                address: _workContext.CurrentCustomer.ShippingAddress,
                                excludeProperties: false,
                                addressSettings: _addressSettings,
                                addressAttributeFormatter: _addressAttributeFormatter);
                        }
                    }
                    else
                    {
                        var country = _countryService.GetCountryByTwoLetterIsoCode(pickupPoint.CountryCode);
                        model.OrderReviewData.PickupAddress = new AddressModel
                        {
                            Address1 = pickupPoint.Address,
                            City = pickupPoint.City,
                            CountryName = country != null ? country.Name : string.Empty,
                            ZipPostalCode = pickupPoint.ZipPostalCode
                        };
                    }

                    //selected shipping method
                    var shippingOption = _workContext.CurrentCustomer.GetAttribute<ShippingOption>(SystemCustomerAttributeNames.SelectedShippingOption, _storeContext.CurrentStore.Id);
                    if (shippingOption != null)
                        model.OrderReviewData.ShippingMethod = shippingOption.Name;
                }
                //payment info
                var selectedPaymentMethodSystemName = _workContext.CurrentCustomer.GetAttribute<string>(
                    SystemCustomerAttributeNames.SelectedPaymentMethod, _storeContext.CurrentStore.Id);
                var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(selectedPaymentMethodSystemName);
                model.OrderReviewData.PaymentMethod = paymentMethod != null ? paymentMethod.GetLocalizedFriendlyName(_localizationService, _workContext.WorkingLanguage.Id) : "";

                //custom values
                var processPaymentRequest = _httpContext.Session["OrderPaymentInfo"] as ProcessPaymentRequest;
                if (processPaymentRequest != null)
                {
                    model.OrderReviewData.CustomValues = processPaymentRequest.CustomValues;
                }
            }
            #endregion

            #region Marketing Data
            model.RelatedProducts = new List<ProductOverviewModel>();
            // Egg code
            var productInCardIds = new List<string>();
            var productInCardIdsForGA = new List<string>();
            var trackingItems = new List<GATrackingItemModel>();
            foreach (var item in model.Items)
            {
                var remarketingItem = _remarketingCartItems.FirstOrDefault(p=>p.Text.Equals(item.Id.ToString()));
                string tempRemarketingProductId = remarketingItem != null ? remarketingItem.Value : "";
                //GA ID
                string googleRemarketingId = string.Format("{0}{1}", tempRemarketingProductId, _storeContext.CurrentStore.RemarketingForGoogleId);
                productInCardIdsForGA.Add(googleRemarketingId);

                //FB ID
                string facbookRemarketingId = string.Format("{0}{1}", tempRemarketingProductId, _storeContext.CurrentStore.RemarketingForFacebookId);
                productInCardIds.Add(facbookRemarketingId);

                item.AttributeInfo = EggCommon.PrepareEggProductAttribute(item.AttributeInfo);
                var gaTrackingItem = new GATrackingItemModel();
                gaTrackingItem.Id = googleRemarketingId;
                gaTrackingItem.Name = item.ProductName.Trim();
                gaTrackingItem.Category = _categoryService.GetProductCategoriesByProductId(item.ProductId).Select(p => p.Category.Name).FirstOrDefault();
                gaTrackingItem.Quantity = item.Quantity;
                gaTrackingItem.Price = item.StoreCurrencyPrice;
                trackingItems.Add(gaTrackingItem);
            }

            var totalOrderModel = PrepareOrderTotalsModel(cart, true);
            decimal totalOrderDecimal = totalOrderModel.OrderTotalDecimal;
            var relatedProducts = GetRelatedProductWithHightestQuantityInCart(cart);
            model.RelatedProducts = PrepareProductOverviewModels(relatedProducts, true, true).ToList();
            model.FreeShippingOverXEnabled = _shippingSettings.FreeShippingOverXEnabled;
            if (model.FreeShippingOverXEnabled)
                model.FreeShippingOverXValue = _shippingSettings.FreeShippingOverXValue;

            model.RemarketingData = this.PrepareCustomerRemarketingDataModel(_workContext, _storeContext, _categoryService, _productService, _genericAttributeService, null, trackingItems, totalOrderDecimal);
            model.RemarketingData.DynamicFacebookData = productInCardIds;
            model.RemarketingData.GoogleAnalyticsData = productInCardIdsForGA;
            #endregion
        }

        [NonAction]
        protected virtual void PrepareWishlistModel(WishlistModel model,
            IList<ShoppingCartItem> cart, bool isEditable = true)
        {
            if (cart == null)
                throw new ArgumentNullException("cart");

            if (model == null)
                throw new ArgumentNullException("model");

            model.EmailWishlistEnabled = _shoppingCartSettings.EmailWishlistEnabled;
            model.IsEditable = isEditable;
            model.DisplayAddToCart = _permissionService.Authorize(StandardPermissionProvider.EnableShoppingCart);
            model.DisplayTaxShippingInfo = _catalogSettings.DisplayTaxShippingInfoWishlist;

            if (!cart.Any())
                return;

            #region Simple properties

            var customer = cart.GetCustomer();
            model.CustomerGuid = customer.CustomerGuid;
            model.CustomerFullname = customer.GetFullName();
            model.ShowProductImages = _shoppingCartSettings.ShowProductImagesOnShoppingCart;
            model.ShowSku = _catalogSettings.ShowProductSku;

            //cart warnings
            var cartWarnings = _shoppingCartService.GetShoppingCartWarnings(cart, "", false);
            foreach (var warning in cartWarnings)
                model.Warnings.Add(warning);

            #endregion

            #region Cart items

            foreach (var sci in cart)
            {
                var cartItemModel = new WishlistModel.ShoppingCartItemModel
                {
                    Id = sci.Id,
                    Sku = sci.Product.FormatSku(sci.AttributesXml, _productAttributeParser),
                    ProductId = sci.Product.Id,
                    ProductName = sci.Product.GetLocalized(x => x.Name),
                    ProductSeName = sci.Product.GetSeName(),
                    Quantity = sci.Quantity,
                    AttributeInfo = _productAttributeFormatter.FormatAttributes(sci.Product, sci.AttributesXml),

                };

                //allow editing?
                //1. setting enabled?
                //2. simple product?
                //3. has attribute or gift card?
                //4. visible individually?
                cartItemModel.AllowItemEditing = _shoppingCartSettings.AllowCartItemEditing &&
                    sci.Product.ProductType == ProductType.SimpleProduct &&
                    (!String.IsNullOrEmpty(cartItemModel.AttributeInfo) || sci.Product.IsGiftCard) &&
                    sci.Product.VisibleIndividually;

                //allowed quantities
                var allowedQuantities = sci.Product.ParseAllowedQuantities();
                foreach (var qty in allowedQuantities)
                {
                    cartItemModel.AllowedQuantities.Add(new SelectListItem
                    {
                        Text = qty.ToString(),
                        Value = qty.ToString(),
                        Selected = sci.Quantity == qty
                    });
                }


                //recurring info
                if (sci.Product.IsRecurring)
                    cartItemModel.RecurringInfo = string.Format(_localizationService.GetResource("ShoppingCart.RecurringPeriod"), sci.Product.RecurringCycleLength, sci.Product.RecurringCyclePeriod.GetLocalizedEnum(_localizationService, _workContext));

                //rental info
                if (sci.Product.IsRental)
                {
                    var rentalStartDate = sci.RentalStartDateUtc.HasValue ? sci.Product.FormatRentalDate(sci.RentalStartDateUtc.Value) : "";
                    var rentalEndDate = sci.RentalEndDateUtc.HasValue ? sci.Product.FormatRentalDate(sci.RentalEndDateUtc.Value) : "";
                    cartItemModel.RentalInfo = string.Format(_localizationService.GetResource("ShoppingCart.Rental.FormattedDate"),
                        rentalStartDate, rentalEndDate);
                }

                //unit prices
                if (sci.Product.CallForPrice)
                {
                    cartItemModel.UnitPrice = _localizationService.GetResource("Products.CallForPrice");
                }
                else
                {
                    decimal taxRate;
                    decimal shoppingCartUnitPriceWithDiscountBase = _taxService.GetProductPrice(sci.Product, _priceCalculationService.GetUnitPrice(sci), out taxRate);
                    decimal shoppingCartUnitPriceWithDiscount = _currencyService.ConvertFromPrimaryStoreCurrency(shoppingCartUnitPriceWithDiscountBase, _workContext.WorkingCurrency);
                    cartItemModel.UnitPrice = _priceFormatter.FormatPrice(shoppingCartUnitPriceWithDiscount);
                }
                //subtotal, discount
                if (sci.Product.CallForPrice)
                {
                    cartItemModel.SubTotal = _localizationService.GetResource("Products.CallForPrice");
                }
                else
                {
                    //sub total
                    List<Discount> scDiscounts;
                    decimal shoppingCartItemDiscountBase;
                    decimal taxRate;
                    decimal shoppingCartItemSubTotalWithDiscountBase = _taxService.GetProductPrice(sci.Product, _priceCalculationService.GetSubTotal(sci, true, out shoppingCartItemDiscountBase, out scDiscounts), out taxRate);
                    decimal shoppingCartItemSubTotalWithDiscount = _currencyService.ConvertFromPrimaryStoreCurrency(shoppingCartItemSubTotalWithDiscountBase, _workContext.WorkingCurrency);
                    cartItemModel.SubTotal = _priceFormatter.FormatPrice(shoppingCartItemSubTotalWithDiscount);

                    //display an applied discount amount
                    if (shoppingCartItemDiscountBase > decimal.Zero)
                    {
                        shoppingCartItemDiscountBase = _taxService.GetProductPrice(sci.Product, shoppingCartItemDiscountBase, out taxRate);
                        if (shoppingCartItemDiscountBase > decimal.Zero)
                        {
                            decimal shoppingCartItemDiscount = _currencyService.ConvertFromPrimaryStoreCurrency(shoppingCartItemDiscountBase, _workContext.WorkingCurrency);
                            cartItemModel.Discount = _priceFormatter.FormatPrice(shoppingCartItemDiscount);
                        }
                    }
                }
                var itemAttributeValues = _productAttributeParser.ParseProductAttributeValues(sci.AttributesXml);
                var selectedColor = itemAttributeValues.Where(p => p.PictureId > 0).FirstOrDefault();

                //picture
                if (_shoppingCartSettings.ShowProductImagesOnShoppingCart)
                {
                    var renderPictureRequest = new RenderPictureRequest();
                    renderPictureRequest.Data = _productAttributeParser.ParserCartItemCustomData(sci.AttributesXml).ToList();
                    renderPictureRequest.AttrValueId = selectedColor.Id;
                    renderPictureRequest.Scale = _mediaSettings.CartThumbPictureSize;
                    if (renderPictureRequest.Data.Any())
                    {
                        //var  pictureRenderedUrl = this.GetPictureRenderUrl(Url.RouteUrl("RenderProductImage"), renderPictureRequest);
                        cartItemModel.Picture = PrepareCartItemPictureModel(sci,
                            _mediaSettings.CartThumbPictureSize, true, cartItemModel.ProductName, renderPictureRequest);
                    }
                    else
                    {
                        cartItemModel.Picture = PrepareCartItemPictureModel(sci,
                       _mediaSettings.CartThumbPictureSize, true, cartItemModel.ProductName, null);
                    }
                }

                //item warnings
                var itemWarnings = _shoppingCartService.GetShoppingCartItemWarnings(
                    _workContext.CurrentCustomer,
                    sci.ShoppingCartType,
                    sci.Product,
                    sci.StoreId,
                    sci.AttributesXml,
                    sci.CustomerEnteredPrice,
                    sci.RentalStartDateUtc,
                    sci.RentalEndDateUtc,
                    sci.Quantity,
                    false);
                foreach (var warning in itemWarnings)
                    cartItemModel.Warnings.Add(warning);

                model.Items.Add(cartItemModel);
            }

            #endregion
        }

        [NonAction]
        protected virtual MiniShoppingCartModel PrepareMiniShoppingCartModel()
        {
            var model = new MiniShoppingCartModel
            {
                ShowProductImages = _shoppingCartSettings.ShowProductImagesInMiniShoppingCart,
                //let's always display it
                DisplayShoppingCartButton = true,
                CurrentCustomerIsGuest = _workContext.CurrentCustomer.IsGuest(),
                AnonymousCheckoutAllowed = _orderSettings.AnonymousCheckoutAllowed,
            };


            //performance optimization (use "HasShoppingCartItems" property)
            if (_workContext.CurrentCustomer.HasShoppingCartItems)
            {
                var cart = _workContext.CurrentCustomer.ShoppingCartItems
                    .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                    .LimitPerStore(_storeContext.CurrentStore.Id)
                    .ToList();
                model.TotalProducts = cart.GetTotalProducts();
                if (cart.Any())
                {
                    //subtotal
                    decimal orderSubTotalDiscountAmountBase;
                    List<Discount> orderSubTotalAppliedDiscounts;
                    decimal subTotalWithoutDiscountBase;
                    decimal subTotalWithDiscountBase;
                    var subTotalIncludingTax = _workContext.TaxDisplayType == TaxDisplayType.IncludingTax && !_taxSettings.ForceTaxExclusionFromOrderSubtotal;
                    _orderTotalCalculationService.GetShoppingCartSubTotal(cart, subTotalIncludingTax,
                        out orderSubTotalDiscountAmountBase, out orderSubTotalAppliedDiscounts,
                        out subTotalWithoutDiscountBase, out subTotalWithDiscountBase);
                    decimal subtotalBase = subTotalWithoutDiscountBase;
                    decimal subtotal = _currencyService.ConvertFromPrimaryStoreCurrency(subtotalBase, _workContext.WorkingCurrency);
                    model.SubTotal = _priceFormatter.FormatPrice(subtotal, false, _workContext.WorkingCurrency, _workContext.WorkingLanguage, subTotalIncludingTax);

                    var requiresShipping = cart.RequiresShipping();
                    //a customer should visit the shopping cart page (hide checkout button) before going to checkout if:
                    //1. "terms of service" are enabled
                    //2. min order sub-total is OK
                    //3. we have at least one checkout attribute
                    var checkoutAttributesExistCacheKey = string.Format(ModelCacheEventConsumer.CHECKOUTATTRIBUTES_EXIST_KEY,
                        _storeContext.CurrentStore.Id, requiresShipping);
                    var checkoutAttributesExist = _cacheManager.Get(checkoutAttributesExistCacheKey,
                        () =>
                        {
                            var checkoutAttributes = _checkoutAttributeService.GetAllCheckoutAttributes(_storeContext.CurrentStore.Id, !requiresShipping);
                            return checkoutAttributes.Any();
                        });

                    bool minOrderSubtotalAmountOk = _orderProcessingService.ValidateMinOrderSubtotalAmount(cart);
                    bool downloadableProductsRequireRegistration =
                        _customerSettings.RequireRegistrationForDownloadableProducts && cart.Any(sci => sci.Product.IsDownload);

                    model.DisplayCheckoutButton = !_orderSettings.TermsOfServiceOnShoppingCartPage &&
                        minOrderSubtotalAmountOk &&
                        !checkoutAttributesExist &&
                        !(downloadableProductsRequireRegistration
                            && _workContext.CurrentCustomer.IsGuest());

                    //products. sort descending (recently added products)
                    foreach (var sci in cart
                        .OrderByDescending(x => x.Id)
                        .Take(_shoppingCartSettings.MiniShoppingCartProductNumber)
                        .ToList())
                    {
                        var cartItemModel = new MiniShoppingCartModel.ShoppingCartItemModel
                        {
                            Id = sci.Id,
                            ProductId = sci.Product.Id,
                            ProductName = sci.Product.GetLocalized(x => x.Name),
                            ProductSeName = sci.Product.GetSeName(),
                            Quantity = sci.Quantity,
                            AttributeInfo = _productAttributeFormatter.FormatAttributes(sci.Product, sci.AttributesXml)
                        };

                        //unit prices
                        if (sci.Product.CallForPrice)
                        {
                            cartItemModel.UnitPrice = _localizationService.GetResource("Products.CallForPrice");
                        }
                        else
                        {
                            decimal taxRate;
                            decimal shoppingCartUnitPriceWithDiscountBase = _taxService.GetProductPrice(sci.Product, _priceCalculationService.GetUnitPrice(sci), out taxRate);
                            decimal shoppingCartUnitPriceWithDiscount = _currencyService.ConvertFromPrimaryStoreCurrency(shoppingCartUnitPriceWithDiscountBase, _workContext.WorkingCurrency);
                            cartItemModel.UnitPrice = _priceFormatter.FormatPrice(shoppingCartUnitPriceWithDiscount);
                        }
                        var itemAttributeValues = _productAttributeParser.ParseProductAttributeValues(sci.AttributesXml);
                        var selectedColor = itemAttributeValues.Where(p => p.PictureId > 0).FirstOrDefault();

                        //picture
                        if (_shoppingCartSettings.ShowProductImagesInMiniShoppingCart)
                        {
                            var renderPictureRequest = new RenderPictureRequest();
                            renderPictureRequest.Data = _productAttributeParser.ParserCartItemCustomData(sci.AttributesXml).ToList();
                            renderPictureRequest.AttrValueId = selectedColor.Id;
                            renderPictureRequest.Scale = _mediaSettings.CartThumbPictureSize;
                            if (renderPictureRequest.Data.Any())
                            {
                                //var  pictureRenderedUrl = this.GetPictureRenderUrl(Url.RouteUrl("RenderProductImage"), renderPictureRequest);
                                cartItemModel.Picture = PrepareCartItemPictureModel(sci,
                                    _mediaSettings.CartThumbPictureSize, true, cartItemModel.ProductName, renderPictureRequest);
                            }
                            else
                            {
                                cartItemModel.Picture = PrepareCartItemPictureModel(sci,
                               _mediaSettings.CartThumbPictureSize, true, cartItemModel.ProductName, null);
                            }
                        }

                        model.Items.Add(cartItemModel);
                    }
                }
            }

            return model;
        }

        [NonAction]
        protected virtual OrderTotalsModel PrepareOrderTotalsModel(IList<ShoppingCartItem> cart, bool isEditable)
        {
            var model = new OrderTotalsModel();
            model.IsEditable = isEditable;

            if (cart.Any())
            {
                //subtotal
                decimal orderSubTotalDiscountAmountBase;
                List<Discount> orderSubTotalAppliedDiscounts;
                decimal subTotalWithoutDiscountBase;
                decimal subTotalWithDiscountBase;
                var subTotalIncludingTax = _workContext.TaxDisplayType == TaxDisplayType.IncludingTax && !_taxSettings.ForceTaxExclusionFromOrderSubtotal;
                _orderTotalCalculationService.GetShoppingCartSubTotal(cart, subTotalIncludingTax,
                    out orderSubTotalDiscountAmountBase, out orderSubTotalAppliedDiscounts,
                    out subTotalWithoutDiscountBase, out subTotalWithDiscountBase);
                decimal subtotalBase = subTotalWithoutDiscountBase;
                decimal subtotal = _currencyService.ConvertFromPrimaryStoreCurrency(subtotalBase, _workContext.WorkingCurrency);
                model.SubTotal = _priceFormatter.FormatPrice(subtotal, true, _workContext.WorkingCurrency, _workContext.WorkingLanguage, subTotalIncludingTax);

                if (orderSubTotalDiscountAmountBase > decimal.Zero)
                {
                    decimal orderSubTotalDiscountAmount = _currencyService.ConvertFromPrimaryStoreCurrency(orderSubTotalDiscountAmountBase, _workContext.WorkingCurrency);
                    model.SubTotalDiscount = _priceFormatter.FormatPrice(-orderSubTotalDiscountAmount, true, _workContext.WorkingCurrency, _workContext.WorkingLanguage, subTotalIncludingTax);
                    model.AllowRemovingSubTotalDiscount = model.IsEditable &&
                        orderSubTotalAppliedDiscounts.Any(d => d.RequiresCouponCode && !String.IsNullOrEmpty(d.CouponCode));
                }


                //shipping info
                decimal eggShoppingCartShippingBase;
                model.RequiresShipping = cart.RequiresShipping();
                if (model.RequiresShipping)
                {
                    decimal? shoppingCartShippingBase = _orderTotalCalculationService.GetShoppingCartShippingTotal(cart);
                    model.IsFreeShipping = false;
                    if (shoppingCartShippingBase.HasValue)
                    {
                        eggShoppingCartShippingBase = shoppingCartShippingBase ?? decimal.Zero;
                        model.IsFreeShipping = shoppingCartShippingBase == decimal.Zero;
                        decimal shoppingCartShipping = _currencyService.ConvertFromPrimaryStoreCurrency(shoppingCartShippingBase.Value, _workContext.WorkingCurrency);
                        model.Shipping = _priceFormatter.FormatShippingPrice(shoppingCartShipping, true);

                        //selected shipping method
                        var shippingOption = _workContext.CurrentCustomer.GetAttribute<ShippingOption>(SystemCustomerAttributeNames.SelectedShippingOption, _storeContext.CurrentStore.Id);
                        if (shippingOption != null)
                            model.SelectedShippingMethod = shippingOption.Name;
                    }
                }

                //payment method fee
                var paymentMethodSystemName = _workContext.CurrentCustomer.GetAttribute<string>(
                    SystemCustomerAttributeNames.SelectedPaymentMethod, _storeContext.CurrentStore.Id);
                decimal paymentMethodAdditionalFee = _paymentService.GetAdditionalHandlingFee(cart, paymentMethodSystemName);
                decimal paymentMethodAdditionalFeeWithTaxBase = _taxService.GetPaymentMethodAdditionalFee(paymentMethodAdditionalFee, _workContext.CurrentCustomer);
                if (paymentMethodAdditionalFeeWithTaxBase > decimal.Zero)
                {
                    decimal paymentMethodAdditionalFeeWithTax = _currencyService.ConvertFromPrimaryStoreCurrency(paymentMethodAdditionalFeeWithTaxBase, _workContext.WorkingCurrency);
                    model.PaymentMethodAdditionalFee = _priceFormatter.FormatPaymentMethodAdditionalFee(paymentMethodAdditionalFeeWithTax, true);
                }

                //tax
                bool displayTax = true;
                bool displayTaxRates = true;
                if (_taxSettings.HideTaxInOrderSummary && _workContext.TaxDisplayType == TaxDisplayType.IncludingTax)
                {
                    displayTax = false;
                    displayTaxRates = false;
                }
                else
                {
                    SortedDictionary<decimal, decimal> taxRates;
                    decimal shoppingCartTaxBase = _orderTotalCalculationService.GetTaxTotal(cart, out taxRates);
                    decimal shoppingCartTax = _currencyService.ConvertFromPrimaryStoreCurrency(shoppingCartTaxBase, _workContext.WorkingCurrency);

                    if (shoppingCartTaxBase == 0 && _taxSettings.HideZeroTax)
                    {
                        displayTax = false;
                        displayTaxRates = false;
                    }
                    else
                    {
                        displayTaxRates = _taxSettings.DisplayTaxRates && taxRates.Any();
                        displayTax = !displayTaxRates;

                        model.Tax = _priceFormatter.FormatPrice(shoppingCartTax, true, false);
                        foreach (var tr in taxRates)
                        {
                            model.TaxRates.Add(new OrderTotalsModel.TaxRate
                            {
                                Rate = _priceFormatter.FormatTaxRate(tr.Key),
                                Value = _priceFormatter.FormatPrice(_currencyService.ConvertFromPrimaryStoreCurrency(tr.Value, _workContext.WorkingCurrency), true, false),
                            });
                        }
                    }
                }
                model.DisplayTaxRates = displayTaxRates;
                model.DisplayTax = displayTax;

                //total
                decimal orderTotalDiscountAmountBase;
                List<Discount> orderTotalAppliedDiscounts;
                List<AppliedGiftCard> appliedGiftCards;
                int redeemedRewardPoints;
                decimal redeemedRewardPointsAmount;
                decimal? shoppingCartTotalBase = _orderTotalCalculationService.GetShoppingCartTotal(cart,
                    out orderTotalDiscountAmountBase, out orderTotalAppliedDiscounts,
                    out appliedGiftCards, out redeemedRewardPoints, out redeemedRewardPointsAmount);
                if (shoppingCartTotalBase.HasValue)
                {
                    decimal shoppingCartTotal = _currencyService.ConvertFromPrimaryStoreCurrency(shoppingCartTotalBase.Value, _workContext.WorkingCurrency);
                    model.OrderTotal = _priceFormatter.FormatPrice(shoppingCartTotal, true, false);
                    model.OrderTotalDecimal = shoppingCartTotalBase.Value;
                }


                //discount
                if (orderTotalDiscountAmountBase > decimal.Zero)
                {
                    decimal orderTotalDiscountAmount = _currencyService.ConvertFromPrimaryStoreCurrency(orderTotalDiscountAmountBase, _workContext.WorkingCurrency);
                    model.OrderTotalDiscount = _priceFormatter.FormatPrice(-orderTotalDiscountAmount, true, false);
                    model.AllowRemovingOrderTotalDiscount = model.IsEditable &&
                        orderTotalAppliedDiscounts.Any(d => d.RequiresCouponCode && !String.IsNullOrEmpty(d.CouponCode));
                }

                //gift cards
                if (appliedGiftCards != null && appliedGiftCards.Any())
                {
                    foreach (var appliedGiftCard in appliedGiftCards)
                    {
                        var gcModel = new OrderTotalsModel.GiftCard
                        {
                            Id = appliedGiftCard.GiftCard.Id,
                            CouponCode = appliedGiftCard.GiftCard.GiftCardCouponCode,
                        };
                        decimal amountCanBeUsed = _currencyService.ConvertFromPrimaryStoreCurrency(appliedGiftCard.AmountCanBeUsed, _workContext.WorkingCurrency);
                        gcModel.Amount = _priceFormatter.FormatPrice(-amountCanBeUsed, true, false);

                        decimal remainingAmountBase = appliedGiftCard.GiftCard.GetGiftCardRemainingAmount() - appliedGiftCard.AmountCanBeUsed;
                        decimal remainingAmount = _currencyService.ConvertFromPrimaryStoreCurrency(remainingAmountBase, _workContext.WorkingCurrency);
                        gcModel.Remaining = _priceFormatter.FormatPrice(remainingAmount, true, false);

                        model.GiftCards.Add(gcModel);
                    }
                }

                //reward points to be spent (redeemed)
                if (redeemedRewardPointsAmount > decimal.Zero)
                {
                    decimal redeemedRewardPointsAmountInCustomerCurrency = _currencyService.ConvertFromPrimaryStoreCurrency(redeemedRewardPointsAmount, _workContext.WorkingCurrency);
                    model.RedeemedRewardPoints = redeemedRewardPoints;
                    model.RedeemedRewardPointsAmount = _priceFormatter.FormatPrice(-redeemedRewardPointsAmountInCustomerCurrency, true, false);
                }

                //reward points to be earned
                if (_rewardPointsSettings.Enabled &&
                    _rewardPointsSettings.DisplayHowMuchWillBeEarned &&
                    shoppingCartTotalBase.HasValue)
                {
                    model.WillEarnRewardPoints = _orderTotalCalculationService
                        .CalculateRewardPoints(_workContext.CurrentCustomer, shoppingCartTotalBase.Value);
                }

            }

            return model;
        }


        [NonAction]
        protected virtual void ParseAndSaveCheckoutAttributes(List<ShoppingCartItem> cart, FormCollection form)
        {
            if (cart == null)
                throw new ArgumentNullException("cart");

            if (form == null)
                throw new ArgumentNullException("form");

            string attributesXml = "";
            var checkoutAttributes = _checkoutAttributeService.GetAllCheckoutAttributes(_storeContext.CurrentStore.Id, !cart.RequiresShipping());
            foreach (var attribute in checkoutAttributes)
            {
                string controlId = string.Format("checkout_attribute_{0}", attribute.Id);
                switch (attribute.AttributeControlType)
                {
                    case AttributeControlType.DropdownList:
                    case AttributeControlType.RadioList:
                    case AttributeControlType.ColorSquares:
                    case AttributeControlType.ImageSquares:
                        {
                            var ctrlAttributes = form[controlId];
                            if (!String.IsNullOrEmpty(ctrlAttributes))
                            {
                                int selectedAttributeId = int.Parse(ctrlAttributes);
                                if (selectedAttributeId > 0)
                                    attributesXml = _checkoutAttributeParser.AddCheckoutAttribute(attributesXml,
                                        attribute, selectedAttributeId.ToString());
                            }
                        }
                        break;
                    case AttributeControlType.Checkboxes:
                        {
                            var cblAttributes = form[controlId];
                            if (!String.IsNullOrEmpty(cblAttributes))
                            {
                                foreach (var item in cblAttributes.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                                {
                                    int selectedAttributeId = int.Parse(item);
                                    if (selectedAttributeId > 0)
                                        attributesXml = _checkoutAttributeParser.AddCheckoutAttribute(attributesXml,
                                            attribute, selectedAttributeId.ToString());
                                }
                            }
                        }
                        break;
                    case AttributeControlType.ReadonlyCheckboxes:
                        {
                            //load read-only (already server-side selected) values
                            var attributeValues = _checkoutAttributeService.GetCheckoutAttributeValues(attribute.Id);
                            foreach (var selectedAttributeId in attributeValues
                                .Where(v => v.IsPreSelected)
                                .Select(v => v.Id)
                                .ToList())
                            {
                                attributesXml = _checkoutAttributeParser.AddCheckoutAttribute(attributesXml,
                                            attribute, selectedAttributeId.ToString());
                            }
                        }
                        break;
                    case AttributeControlType.TextBox:
                    case AttributeControlType.MultilineTextbox:
                        {
                            var ctrlAttributes = form[controlId];
                            if (!String.IsNullOrEmpty(ctrlAttributes))
                            {
                                string enteredText = ctrlAttributes.Trim();
                                attributesXml = _checkoutAttributeParser.AddCheckoutAttribute(attributesXml,
                                    attribute, enteredText);
                            }
                        }
                        break;
                    case AttributeControlType.Datepicker:
                        {
                            var date = form[controlId + "_day"];
                            var month = form[controlId + "_month"];
                            var year = form[controlId + "_year"];
                            DateTime? selectedDate = null;
                            try
                            {
                                selectedDate = new DateTime(Int32.Parse(year), Int32.Parse(month), Int32.Parse(date));
                            }
                            catch { }
                            if (selectedDate.HasValue)
                            {
                                attributesXml = _checkoutAttributeParser.AddCheckoutAttribute(attributesXml,
                                    attribute, selectedDate.Value.ToString("D"));
                            }
                        }
                        break;
                    case AttributeControlType.FileUpload:
                        {
                            Guid downloadGuid;
                            Guid.TryParse(form[controlId], out downloadGuid);
                            var download = _downloadService.GetDownloadByGuid(downloadGuid);
                            if (download != null)
                            {
                                attributesXml = _checkoutAttributeParser.AddCheckoutAttribute(attributesXml,
                                           attribute, download.DownloadGuid.ToString());
                            }
                        }
                        break;
                    default:
                        break;
                }
            }

            //validate conditional attributes (if specified)
            foreach (var attribute in checkoutAttributes)
            {
                var conditionMet = _checkoutAttributeParser.IsConditionMet(attribute, attributesXml);
                if (conditionMet.HasValue && !conditionMet.Value)
                    attributesXml = _checkoutAttributeParser.RemoveCheckoutAttribute(attributesXml, attribute);
            }

            //save checkout attributes
            _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, SystemCustomerAttributeNames.CheckoutAttributes, attributesXml, _storeContext.CurrentStore.Id);
        }

        /// <summary>
        /// Parse product attributes on the product details page
        /// </summary>
        /// <param name="product">Product</param>
        /// <param name="form">Form</param>
        /// <returns>Parsed attributes</returns>
        [NonAction]
        protected virtual string ParseProductAttributes(Product product, FormCollection form)
        {
            string attributesXml = "";

            try
            {
                #region Product attributes

                var productAttributes = _productAttributeService.GetProductAttributeMappingsByProductId(product.Id);
                foreach (var attribute in productAttributes)
                {
                    string controlId = string.Format("product_attribute_{0}", attribute.Id);
                    switch (attribute.AttributeControlType)
                    {
                        case AttributeControlType.DropdownList:
                        case AttributeControlType.RadioList:
                        case AttributeControlType.ColorSquares:
                        case AttributeControlType.ImageSquares:
                            {
                                var ctrlAttributes = form[controlId];
                                if (!String.IsNullOrEmpty(ctrlAttributes))
                                {
                                    int selectedAttributeId;
                                    if (int.TryParse(ctrlAttributes, out selectedAttributeId))
                                    {
                                        if (selectedAttributeId > 0)
                                            attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                                                attribute, selectedAttributeId.ToString());
                                    }
                                    else { }
                                }
                            }
                            break;
                        case AttributeControlType.Checkboxes:
                            {
                                var ctrlAttributes = form[controlId];
                                if (!String.IsNullOrEmpty(ctrlAttributes))
                                {
                                    foreach (var item in ctrlAttributes.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                                    {
                                        int selectedAttributeId = int.Parse(item);
                                        if (selectedAttributeId > 0)
                                            attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                                                attribute, selectedAttributeId.ToString());
                                    }
                                }
                            }
                            break;
                        case AttributeControlType.ReadonlyCheckboxes:
                            {
                                //load read-only (already server-side selected) values
                                var attributeValues = _productAttributeService.GetProductAttributeValues(attribute.Id);
                                foreach (var selectedAttributeId in attributeValues
                                    .Where(v => v.IsPreSelected)
                                    .Select(v => v.Id)
                                    .ToList())
                                {
                                    attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                                        attribute, selectedAttributeId.ToString());
                                }
                            }
                            break;
                        case AttributeControlType.TextBox:
                        case AttributeControlType.MultilineTextbox:
                            {
                                var ctrlAttributes = form[controlId];
                                if (!String.IsNullOrEmpty(ctrlAttributes))
                                {
                                    string enteredText = ctrlAttributes.Trim();
                                    attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                                        attribute, enteredText);
                                }
                            }
                            break;
                        case AttributeControlType.Datepicker:
                            {
                                var day = form[controlId + "_day"];
                                var month = form[controlId + "_month"];
                                var year = form[controlId + "_year"];
                                DateTime? selectedDate = null;
                                try
                                {
                                    selectedDate = new DateTime(Int32.Parse(year), Int32.Parse(month), Int32.Parse(day));
                                }
                                catch { }
                                if (selectedDate.HasValue)
                                {
                                    attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                                        attribute, selectedDate.Value.ToString("D"));
                                }
                            }
                            break;
                        case AttributeControlType.FileUpload:
                            {
                                Guid downloadGuid;
                                Guid.TryParse(form[controlId], out downloadGuid);
                                var download = _downloadService.GetDownloadByGuid(downloadGuid);
                                if (download != null)
                                {
                                    attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                                            attribute, download.DownloadGuid.ToString());
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
                //validate conditional attributes (if specified)
                foreach (var attribute in productAttributes)
                {
                    var conditionMet = _productAttributeParser.IsConditionMet(attribute, attributesXml);
                    if (conditionMet.HasValue && !conditionMet.Value)
                    {
                        attributesXml = _productAttributeParser.RemoveProductAttribute(attributesXml, attribute);
                    }
                }

                #endregion

                #region Gift cards

                if (product.IsGiftCard)
                {
                    string recipientName = "";
                    string recipientEmail = "";
                    string senderName = "";
                    string senderEmail = "";
                    string giftCardMessage = "";
                    foreach (string formKey in form.AllKeys)
                    {
                        if (formKey.Equals(string.Format("giftcard_{0}.RecipientName", product.Id), StringComparison.InvariantCultureIgnoreCase))
                        {
                            recipientName = form[formKey];
                            continue;
                        }
                        if (formKey.Equals(string.Format("giftcard_{0}.RecipientEmail", product.Id), StringComparison.InvariantCultureIgnoreCase))
                        {
                            recipientEmail = form[formKey];
                            continue;
                        }
                        if (formKey.Equals(string.Format("giftcard_{0}.SenderName", product.Id), StringComparison.InvariantCultureIgnoreCase))
                        {
                            senderName = form[formKey];
                            continue;
                        }
                        if (formKey.Equals(string.Format("giftcard_{0}.SenderEmail", product.Id), StringComparison.InvariantCultureIgnoreCase))
                        {
                            senderEmail = form[formKey];
                            continue;
                        }
                        if (formKey.Equals(string.Format("giftcard_{0}.Message", product.Id), StringComparison.InvariantCultureIgnoreCase))
                        {
                            giftCardMessage = form[formKey];
                            continue;
                        }
                    }

                    attributesXml = _productAttributeParser.AddGiftCardAttribute(attributesXml,
                        recipientName, recipientEmail, senderName, senderEmail, giftCardMessage);
                }

                #endregion

            }
            catch (Exception ex)
            {
                _logger.Error(string.Format("Change attribute error: {0}", ex.Message), ex);
            }
            return attributesXml;
        }

        [NonAction]
        protected virtual string ParseEggCustomProductAttributes(Product product, EggCartItem cartItem)
        {
            string attributesXml = "";
            try
            {
                // CustomFieldValue
                var pamCustomFieldValue = product.ProductAttributeMappings.Where(ppam => ppam.ProductAttributeId == EggCommon.CustomFieldValueProductAttributeId).FirstOrDefault();

                attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                                        pamCustomFieldValue, JsonConvert.SerializeObject(cartItem.CustomFieldsResult));

                // FullFillSKU
                var pamFullFillSKU = product.ProductAttributeMappings.Where(ppam => ppam.ProductAttributeId == EggCommon.FullFillSKUProductAttributeId).FirstOrDefault();
                attributesXml = _productAttributeParser.AddProductAttribute(attributesXml, pamFullFillSKU, string.Empty);

                // Product Type
                var pamProductType = product.ProductAttributeMappings.Where(ppam => ppam.ProductAttributeId == EggCommon.ProductTypeProductAttributeId).FirstOrDefault();
                attributesXml = _productAttributeParser.AddProductAttribute(attributesXml, pamProductType, cartItem.StyleProductAttribuiteValueId.ToString());

                // Color
                ProductAttributeMapping ppamColor = null;
                foreach (var ppam in product.ProductAttributeMappings.Where(ppam => !string.IsNullOrEmpty(ppam.ConditionAttributeXml) && ppam.ProductAttribute.Name.Contains(EggCommon.PrefixColorAttributeValue)).Select(ppam => ppam))
                {
                    var tmp = _productAttributeParser.ParseConditionValue(ppam.ConditionAttributeXml, pamProductType.Id, cartItem.StyleProductAttribuiteValueId);
                    if (tmp.Count > 0)
                    {
                        ppamColor = ppam;
                        break;
                    }
                }
                attributesXml = _productAttributeParser.AddProductAttribute(attributesXml, ppamColor, cartItem.ColorProductAttribuiteValueId.ToString());

                // Size
                ProductAttributeMapping ppamSize = null;
                foreach (var ppam in product.ProductAttributeMappings.Where(ppam => !string.IsNullOrEmpty(ppam.ConditionAttributeXml) && ppam.ProductAttribute.Name.Contains("size")).Select(ppam => ppam))
                {
                    var tmp = _productAttributeParser.ParseConditionValue(ppam.ConditionAttributeXml, pamProductType.Id, cartItem.StyleProductAttribuiteValueId);
                    if (tmp.Count > 0)
                    {
                        ppamSize = ppam;
                        break;
                    }
                }
                var ppavSize = ppamSize.ProductAttributeValues.Where(ppav => ppav.Name.ToLower().Equals(cartItem.Size.ToLower())).FirstOrDefault();
                attributesXml = _productAttributeParser.AddProductAttribute(attributesXml, ppamSize, ppavSize.Id.ToString());


            }
            catch (Exception ex)
            {
                _logger.Error(string.Format("ParseEggCustomProductAttributes error: {0}", ex.Message), ex);
            }

            return attributesXml;
        }

        [NonAction]
        protected virtual string UpdateAttributeXml(string oldAttributesXml, UpdateCartItemModel updateModel)
        {
            try
            {
                var product = _productService.GetProductById(updateModel.ProductId);
                if (product == null)
                    return null;

                var productAttributeMappings = product.ProductAttributeMappings;
                var itemAttributeValues = _productAttributeParser.ParseProductAttributeValues(oldAttributesXml);
                var itemSelectedAttributeMappings = _productAttributeParser.ParseProductAttributeMappings(oldAttributesXml);
                var allColor = productAttributeMappings.Where(p => p.AttributeControlTypeId == 40).ToList();

                var allType = productAttributeMappings.Where(p => p.ProductAttribute.Name.Equals("Available Products"))
                    .Select(iam => iam.ProductAttributeValues).FirstOrDefault();
                var currentAllSizing = itemSelectedAttributeMappings.Where(p => p.ProductAttribute.Name.Substring(0, 5).Equals("size_"))
                    .Select(iam => iam.ProductAttributeValues).FirstOrDefault();

                var selectedType = _productAttributeService.GetProductAttributeValueById(updateModel.TypeId);

                var currentSize = itemAttributeValues.Where(p => p.ProductAttributeMappingId == currentAllSizing.FirstOrDefault().ProductAttributeMappingId).FirstOrDefault();

                var selectedColor = _productAttributeService.GetProductAttributeValueById(updateModel.ColorId);

                var productMappingAllSizing = productAttributeMappings.Where(p => p.ProductAttribute.Name.Substring(0, 5).Equals("size_")).ToList();
                var allSizing = new List<ProductAttributeValue>();
                var selectedSize = new ProductAttributeValue();

                foreach (var attr in productMappingAllSizing)
                {
                    var valuesThatShouldBeSelected = _productAttributeParser.ParseConditionValue(attr.ConditionAttributeXml, selectedType.ProductAttributeMappingId, selectedType.Id).ToList();
                    if (valuesThatShouldBeSelected.Any())
                    {
                        allSizing = attr.ProductAttributeValues.ToList();
                        selectedSize = allSizing.Where(s => s.Name.Equals(currentSize.Name)).FirstOrDefault();
                        break;
                    }
                }

                string attributesXml = "";
                foreach (var attr in itemSelectedAttributeMappings)
                {
                    if (attr.ProductAttribute.Name.Equals("Available Products"))
                    {
                        attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                                                 selectedType.ProductAttributeMapping, selectedType.Id.ToString());
                    }
                    else if (attr.ProductAttribute.Name.Substring(0, 5).Equals("size_"))
                    {
                        attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                                                 selectedSize.ProductAttributeMapping, selectedSize.Id.ToString());
                    }
                    else if (attr.ProductAttribute.Name.Equals("CustomFieldValue"))
                    {
                        var parseData = _productAttributeParser.ParserCartItemCustomData(oldAttributesXml).ToList();
                        if (parseData.Any())
                        {
                            foreach (var data in parseData)
                            {
                                var customFieldValue = product.ProductCustomFieldMappings.Select(p => p.ProductCustomFieldValues.FirstOrDefault(i => i.ProductAttributeValueId == updateModel.TypeId)).FirstOrDefault();
                                data.ValueId = customFieldValue.Id;
                            }
                        }
                        var jsonSerialiser = new JavaScriptSerializer();
                        var jsonValue = jsonSerialiser.Serialize(parseData);
                        //var value = _productAttributeParser.ParseValues(oldAttributesXml, attr.Id);
                        attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                                                  attr, jsonValue);

                    }
                    else if (attr.AttributeControlType == AttributeControlType.ColorSquares)
                    {
                        attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                                                  selectedColor.ProductAttributeMapping, selectedColor.Id.ToString());
                    }
                }
                return attributesXml;
            }
            catch (Exception ex)
            {
                _logger.Error(string.Format("update xml attribute error: {0}", ex.Message), ex);
            }
            return null;
        }



        [NonAction]
        protected virtual List<RenderPictureRequest.CustomData> ParseProductCustomFieldData(Product product, FormCollection form, int selectedAttributeValueId)
        {
            try
            {
                var result = new List<RenderPictureRequest.CustomData>();
                #region Product custom field
                var productCustomfields = _productCustomFieldService.GetProductCustomFieldMappingsByProductId(product.Id);
                foreach (var pCustomField in productCustomfields)
                {
                    var customField = pCustomField.CustomField;
                    var tempCustomData = new RenderPictureRequest.CustomData();
                    tempCustomData.Id = customField.Id;
                    var controlId = string.Format("customfield_{0}", customField.Id);
                    tempCustomData.Text = form[controlId];
                    var activeCustomFieldValueId = pCustomField.ProductCustomFieldValues.Where(p => p.ProductAttributeValueId == selectedAttributeValueId).Select(p => p.Id).FirstOrDefault();
                    tempCustomData.ValueId = activeCustomFieldValueId;
                    if (!string.IsNullOrEmpty(tempCustomData.Text))
                        result.Add(tempCustomData);
                }
                #endregion
                return result;
            }
            catch (Exception ex)
            { }
            return null;
        }

        /// <summary>
        /// Parse product rental dates on the product details page
        /// </summary>
        /// <param name="product">Product</param>
        /// <param name="form">Form</param>
        /// <param name="startDate">Start date</param>
        /// <param name="endDate">End date</param>
        [NonAction]
        protected virtual void ParseRentalDates(Product product, FormCollection form,
            out DateTime? startDate, out DateTime? endDate)
        {
            startDate = null;
            endDate = null;

            string startControlId = string.Format("rental_start_date_{0}", product.Id);
            string endControlId = string.Format("rental_end_date_{0}", product.Id);
            var ctrlStartDate = form[startControlId];
            var ctrlEndDate = form[endControlId];
            try
            {
                //currenly we support only this format (as in the \Views\Product\_RentalInfo.cshtml file)
                const string datePickerFormat = "MM/dd/yyyy";
                startDate = DateTime.ParseExact(ctrlStartDate, datePickerFormat, CultureInfo.InvariantCulture);
                endDate = DateTime.ParseExact(ctrlEndDate, datePickerFormat, CultureInfo.InvariantCulture);
            }
            catch
            {
            }
        }
        #endregion

        #region Shopping cart

        //add product to cart using AJAX
        //currently we use this method on catalog pages (category/manufacturer/etc)
        [HttpPost]
        public ActionResult AddProductToCart_Catalog(int productId, int shoppingCartTypeId,
            int quantity, bool forceredirection = false)
        {
            var cartType = (ShoppingCartType)shoppingCartTypeId;

            var product = _productService.GetProductById(productId);
            if (product == null)
                //no product found
                return Json(new
                {
                    success = false,
                    message = "No product found with the specified ID"
                });

            //we can add only simple products
            if (product.ProductType != ProductType.SimpleProduct)
            {
                return Json(new
                {
                    redirect = Url.RouteUrl("Product", new { SeName = product.GetSeName() }),
                });
            }

            //products with "minimum order quantity" more than a specified qty
            if (product.OrderMinimumQuantity > quantity)
            {
                //we cannot add to the cart such products from category pages
                //it can confuse customers. That's why we redirect customers to the product details page
                return Json(new
                {
                    redirect = Url.RouteUrl("Product", new { SeName = product.GetSeName() }),
                });
            }

            if (product.CustomerEntersPrice)
            {
                //cannot be added to the cart (requires a customer to enter price)
                return Json(new
                {
                    redirect = Url.RouteUrl("Product", new { SeName = product.GetSeName() }),
                });
            }

            if (product.IsRental)
            {
                //rental products require start/end dates to be entered
                return Json(new
                {
                    redirect = Url.RouteUrl("Product", new { SeName = product.GetSeName() }),
                });
            }

            var allowedQuantities = product.ParseAllowedQuantities();
            if (allowedQuantities.Length > 0)
            {
                //cannot be added to the cart (requires a customer to select a quantity from dropdownlist)
                return Json(new
                {
                    redirect = Url.RouteUrl("Product", new { SeName = product.GetSeName() }),
                });
            }

            if (product.ProductAttributeMappings.Any())
            {
                //product has some attributes. let a customer see them
                return Json(new
                {
                    redirect = Url.RouteUrl("Product", new { SeName = product.GetSeName() }),
                });
            }

            //get standard warnings without attribute validations
            //first, try to find existing shopping cart item
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == cartType)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();
            var shoppingCartItem = _shoppingCartService.FindShoppingCartItemInTheCart(cart, cartType, product);
            //if we already have the same product in the cart, then use the total quantity to validate
            var quantityToValidate = shoppingCartItem != null ? shoppingCartItem.Quantity + quantity : quantity;
            var addToCartWarnings = _shoppingCartService
                .GetShoppingCartItemWarnings(_workContext.CurrentCustomer, cartType,
                product, _storeContext.CurrentStore.Id, string.Empty,
                decimal.Zero, null, null, quantityToValidate, false, true, false, false, false);
            if (addToCartWarnings.Any())
            {
                //cannot be added to the cart
                //let's display standard warnings
                return Json(new
                {
                    success = false,
                    message = addToCartWarnings.ToArray()
                });
            }

            //now let's try adding product to the cart (now including product attribute validation, etc)
            addToCartWarnings = _shoppingCartService.AddToCart(customer: _workContext.CurrentCustomer,
                product: product,
                shoppingCartType: cartType,
                storeId: _storeContext.CurrentStore.Id,
                quantity: quantity);
            if (addToCartWarnings.Any())
            {
                //cannot be added to the cart
                //but we do not display attribute and gift card warnings here. let's do it on the product details page
                return Json(new
                {
                    redirect = Url.RouteUrl("Product", new { SeName = product.GetSeName() }),
                });
            }

            //added to the cart/wishlist
            switch (cartType)
            {
                case ShoppingCartType.Wishlist:
                    {
                        //activity log
                        _customerActivityService.InsertActivity("PublicStore.AddToWishlist", _localizationService.GetResource("ActivityLog.PublicStore.AddToWishlist"), product.Name);

                        if (_shoppingCartSettings.DisplayWishlistAfterAddingProduct || forceredirection)
                        {
                            //redirect to the wishlist page
                            return Json(new
                            {
                                redirect = Url.RouteUrl("Wishlist"),
                            });
                        }

                        //display notification message and update appropriate blocks
                        var updatetopwishlistsectionhtml = string.Format(_localizationService.GetResource("Wishlist.HeaderQuantity"),
                        _workContext.CurrentCustomer.ShoppingCartItems
                        .Where(sci => sci.ShoppingCartType == ShoppingCartType.Wishlist)
                        .LimitPerStore(_storeContext.CurrentStore.Id)
                        .ToList()
                        .GetTotalProducts());
                        return Json(new
                        {
                            success = true,
                            message = string.Format(_localizationService.GetResource("Products.ProductHasBeenAddedToTheWishlist.Link"), Url.RouteUrl("Wishlist")),
                            updatetopwishlistsectionhtml = updatetopwishlistsectionhtml,
                        });
                    }
                case ShoppingCartType.ShoppingCart:
                default:
                    {
                        //activity log
                        _customerActivityService.InsertActivity("PublicStore.AddToShoppingCart", _localizationService.GetResource("ActivityLog.PublicStore.AddToShoppingCart"), product.Name);

                        if (_shoppingCartSettings.DisplayCartAfterAddingProduct || forceredirection)
                        {
                            //redirect to the shopping cart page
                            return Json(new
                            {
                                redirect = Url.RouteUrl("ShoppingCart"),
                            });
                        }

                        //display notification message and update appropriate blocks
                        var updatetopcartsectionhtml = string.Format(_localizationService.GetResource("ShoppingCart.HeaderQuantity"),
                        _workContext.CurrentCustomer.ShoppingCartItems
                        .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                        .LimitPerStore(_storeContext.CurrentStore.Id)
                        .ToList()
                        .GetTotalProducts());

                        var updateflyoutcartsectionhtml = _shoppingCartSettings.MiniShoppingCartEnabled
                            ? this.RenderPartialViewToString("FlyoutShoppingCart", PrepareMiniShoppingCartModel())
                            : "";

                        return Json(new
                        {
                            success = true,
                            message = string.Format(_localizationService.GetResource("Products.ProductHasBeenAddedToTheCart.Link"), Url.RouteUrl("ShoppingCart")),
                            updatetopcartsectionhtml = updatetopcartsectionhtml,
                            updateflyoutcartsectionhtml = updateflyoutcartsectionhtml
                        });
                    }
            }
        }

        //add product to cart using AJAX
        //currently we use this method on the product details pages
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddProductToCart_Details(int productId, int shoppingCartTypeId, FormCollection form)
        {
            var product = _productService.GetProductById(productId);
            if (product == null)
            {
                return Json(new
                {
                    redirect = Url.RouteUrl("HomePage"),
                });
            }

            //we can add only simple products
            if (product.ProductType != ProductType.EggProduct)
            {
                return Json(new
                {
                    success = false,
                    message = "Only Egg products could be added to the cart"
                });
            }

            #region Update existing shopping cart item?

            int updatecartitemid = 0;
            foreach (string formKey in form.AllKeys)
                if (formKey.Equals(string.Format("addtocart_{0}.UpdatedShoppingCartItemId", productId), StringComparison.InvariantCultureIgnoreCase))
                {
                    int.TryParse(form[formKey], out updatecartitemid);
                    break;
                }
            ShoppingCartItem updatecartitem = null;
            if (_shoppingCartSettings.AllowCartItemEditing && updatecartitemid > 0)
            {
                //search with the same cart type as specified
                var cart = _workContext.CurrentCustomer.ShoppingCartItems
                    .Where(x => x.ShoppingCartTypeId == shoppingCartTypeId)
                    .LimitPerStore(_storeContext.CurrentStore.Id)
                    .ToList();
                updatecartitem = cart.FirstOrDefault(x => x.Id == updatecartitemid);
                //not found? let's ignore it. in this case we'll add a new item
                //if (updatecartitem == null)
                //{
                //    return Json(new
                //    {
                //        success = false,
                //        message = "No shopping cart item found to update"
                //    });
                //}
                //is it this product?
                if (updatecartitem != null && product.Id != updatecartitem.ProductId)
                {
                    return Json(new
                    {
                        success = false,
                        message = "This product does not match a passed shopping cart item identifier"
                    });
                }
            }

            #endregion

            #region Customer entered price
            decimal customerEnteredPriceConverted = decimal.Zero;
            if (product.CustomerEntersPrice)
            {
                foreach (string formKey in form.AllKeys)
                {
                    if (formKey.Equals(string.Format("addtocart_{0}.CustomerEnteredPrice", productId), StringComparison.InvariantCultureIgnoreCase))
                    {
                        decimal customerEnteredPrice;
                        if (decimal.TryParse(form[formKey], out customerEnteredPrice))
                            customerEnteredPriceConverted = _currencyService.ConvertToPrimaryStoreCurrency(customerEnteredPrice, _workContext.WorkingCurrency);
                        break;
                    }
                }
            }
            #endregion

            #region Quantity

            int quantity = 1;
            foreach (string formKey in form.AllKeys)
                if (formKey.Equals("AddToCart.EnteredQuantity", StringComparison.InvariantCultureIgnoreCase))
                {
                    int.TryParse(form[formKey], out quantity);
                    break;
                }

            #endregion

            //product and gift card attributes
            string attributes = ParseProductAttributes(product, form);

            //rental attributes
            DateTime? rentalStartDate = null;
            DateTime? rentalEndDate = null;
            if (product.IsRental)
            {
                ParseRentalDates(product, form, out rentalStartDate, out rentalEndDate);
            }

            var cartType = updatecartitem == null ? (ShoppingCartType)shoppingCartTypeId :
                //if the item to update is found, then we ignore the specified "shoppingCartTypeId" parameter
                updatecartitem.ShoppingCartType;

            //save item
            var addToCartWarnings = new List<string>();
            if (updatecartitem == null)
            {
                //add to the cart
                addToCartWarnings.AddRange(_shoppingCartService.AddToCart(_workContext.CurrentCustomer,
                    product, cartType, _storeContext.CurrentStore.Id,
                    attributes, customerEnteredPriceConverted,
                    rentalStartDate, rentalEndDate, quantity, true));
            }
            else
            {
                var cart = _workContext.CurrentCustomer.ShoppingCartItems
                    .Where(x => x.ShoppingCartType == updatecartitem.ShoppingCartType)
                    .LimitPerStore(_storeContext.CurrentStore.Id)
                    .ToList();
                var otherCartItemWithSameParameters = _shoppingCartService.FindShoppingCartItemInTheCart(
                    cart, updatecartitem.ShoppingCartType, product, attributes, customerEnteredPriceConverted,
                    rentalStartDate, rentalEndDate);
                if (otherCartItemWithSameParameters != null &&
                    otherCartItemWithSameParameters.Id == updatecartitem.Id)
                {
                    //ensure it's some other shopping cart item
                    otherCartItemWithSameParameters = null;
                }
                //update existing item
                addToCartWarnings.AddRange(_shoppingCartService.UpdateShoppingCartItem(_workContext.CurrentCustomer,
                    updatecartitem.Id, attributes, customerEnteredPriceConverted,
                    rentalStartDate, rentalEndDate, quantity, true));
                if (otherCartItemWithSameParameters != null && !addToCartWarnings.Any())
                {
                    //delete the same shopping cart item (the other one)
                    _shoppingCartService.DeleteShoppingCartItem(otherCartItemWithSameParameters);
                }
            }

            #region Return result

            if (addToCartWarnings.Any())
            {
                //cannot be added to the cart/wishlist
                //let's display warnings
                return Json(new
                {
                    success = false,
                    message = addToCartWarnings.ToArray()
                });
            }

            //added to the cart/wishlist
            switch (cartType)
            {
                case ShoppingCartType.Wishlist:
                    {
                        //activity log
                        _customerActivityService.InsertActivity("PublicStore.AddToWishlist", _localizationService.GetResource("ActivityLog.PublicStore.AddToWishlist"), product.Name);

                        if (_shoppingCartSettings.DisplayWishlistAfterAddingProduct)
                        {
                            //redirect to the wishlist page
                            return Json(new
                            {
                                redirect = Url.RouteUrl("Wishlist"),
                            });
                        }

                        //display notification message and update appropriate blocks
                        var updatetopwishlistsectionhtml = string.Format(_localizationService.GetResource("Wishlist.HeaderQuantity"),
                        _workContext.CurrentCustomer.ShoppingCartItems
                        .Where(sci => sci.ShoppingCartType == ShoppingCartType.Wishlist)
                        .LimitPerStore(_storeContext.CurrentStore.Id)
                        .ToList()
                        .GetTotalProducts());

                        return Json(new
                        {
                            success = true,
                            message = string.Format(_localizationService.GetResource("Products.ProductHasBeenAddedToTheWishlist.Link"), Url.RouteUrl("Wishlist")),
                            updatetopwishlistsectionhtml = updatetopwishlistsectionhtml,
                        });
                    }
                case ShoppingCartType.ShoppingCart:
                default:
                    {
                        //activity log
                        _customerActivityService.InsertActivity("PublicStore.AddToShoppingCart", _localizationService.GetResource("ActivityLog.PublicStore.AddToShoppingCart"), product.Name);

                        if (_shoppingCartSettings.DisplayCartAfterAddingProduct)
                        {
                            //redirect to the shopping cart page
                            return Json(new
                            {
                                redirect = Url.RouteUrl("ShoppingCart"),
                            });
                        }

                        //display notification message and update appropriate blocks
                        var updatetopcartsectionhtml = string.Format(_localizationService.GetResource("ShoppingCart.HeaderQuantity"),
                        _workContext.CurrentCustomer.ShoppingCartItems
                        .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                        .LimitPerStore(_storeContext.CurrentStore.Id)
                        .ToList()
                        .GetTotalProducts());

                        var updateflyoutcartsectionhtml = _shoppingCartSettings.MiniShoppingCartEnabled
                            ? this.RenderPartialViewToString("FlyoutShoppingCart", PrepareMiniShoppingCartModel())
                            : "";

                        return Json(new
                        {
                            success = true,
                            message = string.Format(_localizationService.GetResource("Products.ProductHasBeenAddedToTheCart.Link"), Url.RouteUrl("ShoppingCart")),
                            updatetopcartsectionhtml = updatetopcartsectionhtml,
                            updateflyoutcartsectionhtml = updateflyoutcartsectionhtml
                        });
                    }
            }


            #endregion
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddProductToCart_DetailsPopup(int productId, int shoppingCartTypeId, FormCollection form)
        {
            var product = _productService.GetProductById(productId);
            if (product == null)
            {
                return Json(new
                {
                    redirect = Url.RouteUrl("HomePage"),
                });
            }

            //we can add only simple products
            if (product.ProductType != ProductType.EggProduct)
            {
                return Json(new
                {
                    success = false,
                    message = "Only Egg products could be added to the cart"
                });
            }

            #region Update existing shopping cart item?
            var cart = new List<ShoppingCartItem>();
            int updatecartitemid = 0;
            foreach (string formKey in form.AllKeys)
                if (formKey.Equals(string.Format("addtocart_{0}.UpdatedShoppingCartItemId", productId), StringComparison.InvariantCultureIgnoreCase))
                {
                    int.TryParse(form[formKey], out updatecartitemid);
                    break;
                }
            ShoppingCartItem updatecartitem = null;
            if (_shoppingCartSettings.AllowCartItemEditing && updatecartitemid > 0)
            {
                //search with the same cart type as specified
                cart = _workContext.CurrentCustomer.ShoppingCartItems
                    .Where(x => x.ShoppingCartTypeId == shoppingCartTypeId)
                    .LimitPerStore(_storeContext.CurrentStore.Id)
                    .ToList();
                updatecartitem = cart.FirstOrDefault(x => x.Id == updatecartitemid);
                if (updatecartitem != null && product.Id != updatecartitem.ProductId)
                {
                    return Json(new
                    {
                        success = false,
                        message = "This product does not match a passed shopping cart item identifier"
                    });
                }
            }

            #endregion

            #region Customer entered price
            decimal customerEnteredPriceConverted = decimal.Zero;
            if (product.CustomerEntersPrice)
            {
                foreach (string formKey in form.AllKeys)
                {
                    if (formKey.Equals(string.Format("addtocart_{0}.CustomerEnteredPrice", productId), StringComparison.InvariantCultureIgnoreCase))
                    {
                        decimal customerEnteredPrice;
                        if (decimal.TryParse(form[formKey], out customerEnteredPrice))
                            customerEnteredPriceConverted = _currencyService.ConvertToPrimaryStoreCurrency(customerEnteredPrice, _workContext.WorkingCurrency);
                        break;
                    }
                }
            }
            #endregion

            #region Quantity

            int quantity = 1;
            foreach (string formKey in form.AllKeys)
                if (formKey.Equals(string.Format("addtocart_{0}.EnteredQuantity", productId), StringComparison.InvariantCultureIgnoreCase))
                {
                    int.TryParse(form[formKey], out quantity);
                    break;
                }

            #endregion

            //product and gift card attributes
            string attributes = ParseProductAttributes(product, form);

            //rental attributes
            DateTime? rentalStartDate = null;
            DateTime? rentalEndDate = null;
            if (product.IsRental)
            {
                ParseRentalDates(product, form, out rentalStartDate, out rentalEndDate);
            }

            var cartType = updatecartitem == null ? (ShoppingCartType)shoppingCartTypeId :
                //if the item to update is found, then we ignore the specified "shoppingCartTypeId" parameter
                updatecartitem.ShoppingCartType;

            //save item
            var addToCartWarnings = new List<string>();
            if (updatecartitem == null)
            {
                //add to the cart
                addToCartWarnings.AddRange(_shoppingCartService.AddToCart(_workContext.CurrentCustomer,
                    product, cartType, _storeContext.CurrentStore.Id,
                    attributes, customerEnteredPriceConverted,
                    rentalStartDate, rentalEndDate, quantity, true));
            }
            else
            {
                cart = _workContext.CurrentCustomer.ShoppingCartItems
                    .Where(x => x.ShoppingCartType == updatecartitem.ShoppingCartType)
                    .LimitPerStore(_storeContext.CurrentStore.Id)
                    .ToList();
                var otherCartItemWithSameParameters = _shoppingCartService.FindShoppingCartItemInTheCart(
                    cart, updatecartitem.ShoppingCartType, product, attributes, customerEnteredPriceConverted,
                    rentalStartDate, rentalEndDate);
                if (otherCartItemWithSameParameters != null &&
                    otherCartItemWithSameParameters.Id == updatecartitem.Id)
                {
                    //ensure it's some other shopping cart item
                    otherCartItemWithSameParameters = null;
                }
                //update existing item
                addToCartWarnings.AddRange(_shoppingCartService.UpdateShoppingCartItem(_workContext.CurrentCustomer,
                    updatecartitem.Id, attributes, customerEnteredPriceConverted,
                    rentalStartDate, rentalEndDate, quantity, true));
                if (otherCartItemWithSameParameters != null && !addToCartWarnings.Any())
                {
                    //delete the same shopping cart item (the other one)
                    _shoppingCartService.DeleteShoppingCartItem(otherCartItemWithSameParameters);
                }
            }

            #region Return result
            if (addToCartWarnings.Any())
            {
                //cannot be added to the cart/wishlist
                //let's display warnings
                return Json(new
                {
                    success = false,
                    message = addToCartWarnings.ToArray()
                });
            }

            //added to the cart/wishlist
            var model = new ShoppingCartModel();
            cart = _workContext.CurrentCustomer.ShoppingCartItems
              .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
              .LimitPerStore(_storeContext.CurrentStore.Id)
              .ToList();
            PrepareShoppingCartModel(model, cart);
            return View(model);

            #endregion
        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult UpSellProductDetailPopup(int productId, FormCollection form)
        {
            var product = _productService.GetProductById(productId);
            if (product == null)
            {
                return Json(new
                {
                    redirect = Url.RouteUrl("HomePage"),
                });
            }

            ShoppingCartModel model = new ShoppingCartModel();
            PrepareUpsellProductModel(model, product, form);

            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult UpSellAdd2Cart(int productId, FormCollection form, List<ItemUpSellRequest> cartItems)
        {
            Product product = _productService.GetProductById(productId);
            
            NameValueCollection formCollection = HttpUtility.ParseQueryString(form["form"]);

            // get custom field value
            List<RenderPictureRequest.CustomData> lstCustomData = new List<RenderPictureRequest.CustomData>();
            var productAttrMappingWithCustomFieldValue = product.ProductAttributeMappings.Where(p => p.ProductAttributeId==EggCommon.CustomFieldValueProductAttributeId).FirstOrDefault();
            if(productAttrMappingWithCustomFieldValue!=null)
            {
                var customValueKey = string.Format("{0}{1}", EggCommon.PrefixAttribute, productAttrMappingWithCustomFieldValue.Id);
                if (!string.IsNullOrEmpty(formCollection[customValueKey]))
                {
                    JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                    lstCustomData = jsonSerializer.Deserialize<List<RenderPictureRequest.CustomData>>(formCollection[customValueKey]);
                }
            }
            // get all Items
            foreach (ItemUpSellRequest itemRequest in cartItems)
            {
                EggCartItem cartItem = new EggCartItem();
                cartItem.CustomFieldsResult = lstCustomData;
                cartItem.Size = itemRequest.Size.IndexOf("+") > 0 ? itemRequest.Size.Substring(0, itemRequest.Size.IndexOf("+")).Trim() : itemRequest.Size;
                StyleColorJsonResult styleColorResult = JsonConvert.DeserializeObject<StyleColorJsonResult>(itemRequest.ItemStyleColor);
                cartItem.ColorProductAttribuiteValueId = styleColorResult.ColorId;
                cartItem.StyleProductAttribuiteValueId = styleColorResult.TypeId;
                cartItem.Quantity = itemRequest.Quantity;

                string attributes = ParseEggCustomProductAttributes(product, cartItem);

                // add to cart item
                _shoppingCartService.AddToCart(_workContext.CurrentCustomer, product, ShoppingCartType.ShoppingCart, _storeContext.CurrentStore.Id, attributes, 0, null, null, cartItem.Quantity);
            }
            
            
            return null;
        }

        [NonAction]
        public void PrepareUpsellProductModel(ShoppingCartModel model, Product product, FormCollection form)
        {
            //product and gift card attributes
            string attributes = ParseProductAttributes(product, form);

            ShoppingCartItemModel cartItemModel = new ShoppingCartItemModel();
            cartItemModel.Id = 1;
            cartItemModel.ProductId = product.Id;
            model.Items.Add(cartItemModel);

            #region Quantity

            int quantity = 1;
            foreach (string formKey in form.AllKeys)
                if (formKey.Equals(string.Format("AddToCart.EnteredQuantity"), StringComparison.InvariantCultureIgnoreCase))
                {
                    int.TryParse(form[formKey], out quantity);
                    break;
                }
            cartItemModel.Quantity = quantity;
            #endregion

            #region Size and Style
            var itemAttributeMappings = product.ProductAttributeMappings;
            var itemAttributeValues = _productAttributeParser.ParseProductAttributeValues(attributes);
            var itemSelectedAttributeMappings = _productAttributeParser.ParseProductAttributeMappings(attributes);
            var allColor = itemAttributeMappings.Where(p => p.AttributeControlTypeId == 40).ToList();

            var allStyle = itemAttributeMappings.Where(p => p.ProductAttribute.Name.Equals("Available Products"))
                .Select(iam => iam.ProductAttributeValues).FirstOrDefault();
            var selectedStyle = itemAttributeValues.Where(p => p.ProductAttributeMappingId == allStyle.FirstOrDefault().ProductAttributeMappingId).FirstOrDefault();

            var allSizing = itemSelectedAttributeMappings.Where(p => p.ProductAttribute.Name.Substring(0, 5).Equals("size_"))
                .Select(iam => iam.ProductAttributeValues.OrderBy(pav=> pav.Id)).FirstOrDefault();
            if (allSizing != null)
            {
                var selectedSize = itemAttributeValues.Where(p => p.ProductAttributeMappingId == allSizing.FirstOrDefault().ProductAttributeMappingId).FirstOrDefault();
                var productAllSizing = itemAttributeMappings.Where(p => p.ProductAttribute.Name.Substring(0, 5).Equals("size_"))
                 .Select(iam => iam.ProductAttributeValues).ToList();
                foreach (var size in allSizing)
                {
                    var tempSize = new ShoppingCartModel.ShoppingCartAttributeValueModel
                    {
                        Id = size.Id,
                        ColorSquaresRgb = size.ColorSquaresRgb,
                        IsPreSelected = size.Id == selectedSize.Id,
                        Name = size.Name,
                        PriceAdjustment = size.PriceAdjustment == decimal.Zero ? "" : _priceFormatter.FormatPrice(_currencyService.ConvertFromPrimaryStoreCurrency(size.PriceAdjustment, _workContext.WorkingCurrency))
                    };
                    cartItemModel.SizingAttributeValues.Add(tempSize);
                }
            }
            

            var selectedColor = itemAttributeValues.Where(p => p.PictureId > 0).FirstOrDefault();

            

            foreach (var styleAttrValue in allStyle)
            {
                foreach (var colorAttrValue in allColor)
                {
                    var valuesThatShouldBeSelected = _productAttributeParser.ParseConditionValue(colorAttrValue.ConditionAttributeXml, styleAttrValue.ProductAttributeMappingId, styleAttrValue.Id).ToList();
                    if (valuesThatShouldBeSelected.Any())
                    {
                        foreach (var item in colorAttrValue.ProductAttributeValues)
                        {
                            var tempStyle = new ShoppingCartModel.ShoppingCartAttributeValueModel
                            {
                                Id = item.Id,
                                ProductTypeId = styleAttrValue.Id,
                                ColorSquaresRgb = item.ColorSquaresRgb,
                                IsPreSelected = item.Id == selectedColor.Id,
                                Name = string.Format("{0} - {1}", styleAttrValue.Name, item.Name)
                            };
                            cartItemModel.StyleAttributeValues.Add(tempStyle);
                        }
                    }
                }
            }
            #endregion

            // need tmp ShoppingCartItem to reuse some function
            ShoppingCartItem tmpShoppingCartItem = new ShoppingCartItem();
            tmpShoppingCartItem.ShoppingCartType = ShoppingCartType.ShoppingCart;
            tmpShoppingCartItem.Product = product;
            tmpShoppingCartItem.AttributesXml = attributes;
            tmpShoppingCartItem.Quantity = quantity;
            tmpShoppingCartItem.StoreId = _storeContext.CurrentStore.Id;
            Customer tmpCustomer = _workContext.CurrentCustomer;
            tmpCustomer.ShoppingCartItems.Add(tmpShoppingCartItem);
            
            tmpShoppingCartItem.Customer = tmpCustomer;

            //picture
            if (_shoppingCartSettings.ShowProductImagesOnShoppingCart)
            {

                var renderPictureRequest = new RenderPictureRequest();
                renderPictureRequest.Data = _productAttributeParser.ParserCartItemCustomData(attributes).ToList();
                renderPictureRequest.AttrValueId = selectedColor.Id;
                renderPictureRequest.Scale = _mediaSettings.CartThumbPictureSize;

                if (renderPictureRequest.Data.Any())
                {
                    //var  pictureRenderedUrl = this.GetPictureRenderUrl(Url.RouteUrl("RenderProductImage"), renderPictureRequest);
                    cartItemModel.Picture = PrepareCartItemPictureModel(tmpShoppingCartItem,
                        _mediaSettings.CartThumbPictureSize, true, cartItemModel.ProductName, renderPictureRequest);
                }
                else
                {
                    cartItemModel.Picture = PrepareCartItemPictureModel(tmpShoppingCartItem,
                   _mediaSettings.CartThumbPictureSize, true, cartItemModel.ProductName, null);
                }

            }


            //allowed quantities
            var allowedQuantities = product.ParseAllowedQuantities();
            foreach (var qty in allowedQuantities)
            {
                cartItemModel.AllowedQuantities.Add(new SelectListItem
                {
                    Text = qty.ToString(),
                    Value = qty.ToString(),
                    Selected = quantity == qty
                });
            }

            //unit prices
            if (product.CallForPrice)
            {
                cartItemModel.UnitPrice = _localizationService.GetResource("Products.CallForPrice");
            }
            else
            {
                decimal taxRate;
                decimal shoppingCartUnitPriceWithDiscountBase = _taxService.GetProductPrice(product, _priceCalculationService.GetUnitPrice(tmpShoppingCartItem), out taxRate);
                decimal shoppingCartUnitPriceWithDiscount = _currencyService.ConvertFromPrimaryStoreCurrency(shoppingCartUnitPriceWithDiscountBase, _workContext.WorkingCurrency);

                List<Discount> scDiscounts;
                decimal shoppingCartItemDiscountBase;
                decimal shoppingCartItemSubTotalWithDiscountBase = _taxService.GetProductPrice(product, _priceCalculationService.GetSubTotal(tmpShoppingCartItem, true, out shoppingCartItemDiscountBase, out scDiscounts),tmpCustomer, out taxRate);
                decimal shoppingCartItemSubTotalWithDiscount = _currencyService.ConvertFromPrimaryStoreCurrency(shoppingCartItemSubTotalWithDiscountBase, _workContext.WorkingCurrency);

                cartItemModel.SubTotal = _priceFormatter.FormatPrice(shoppingCartItemSubTotalWithDiscount);
                cartItemModel.UnitPrice = _priceFormatter.FormatPrice(shoppingCartUnitPriceWithDiscount);
            }
        }


        //handle product attribute selection event. this way we return new price, overridden gtin/sku/mpn
        //currently we use this method on the product details pages
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ProductDetails_AttributeChange(int productId, bool validateAttributeConditions,
            bool loadPicture, FormCollection form)
        {
            var product = _productService.GetProductById(productId);
            if (product == null)
                return new NullJsonResult();

            string attributeXml = ParseProductAttributes(product, form);

            //rental attributes
            DateTime? rentalStartDate = null;
            DateTime? rentalEndDate = null;
            if (product.IsRental)
            {
                ParseRentalDates(product, form, out rentalStartDate, out rentalEndDate);
            }

            //sku, mpn, gtin
            string sku = product.FormatSku(attributeXml, _productAttributeParser);
            string mpn = product.FormatMpn(attributeXml, _productAttributeParser);
            string gtin = product.FormatGtin(attributeXml, _productAttributeParser);

            //price
            string price = "";
            string oldPrice = "";
            if (_permissionService.Authorize(StandardPermissionProvider.DisplayPrices) && !product.CustomerEntersPrice)
            {
                //we do not calculate price of "customer enters price" option is enabled
                List<Discount> scDiscounts;
                decimal discountAmount;
                decimal finalPrice = _priceCalculationService.GetUnitPrice(product,
                    _workContext.CurrentCustomer,
                    ShoppingCartType.ShoppingCart,
                    1, attributeXml, 0,
                    rentalStartDate, rentalEndDate,
                    true, out discountAmount, out scDiscounts);
                decimal taxRate;
                decimal finalPriceWithDiscountBase = _taxService.GetProductPrice(product, finalPrice, out taxRate);
                decimal finalPriceWithDiscount = _currencyService.ConvertFromPrimaryStoreCurrency(finalPriceWithDiscountBase, _workContext.WorkingCurrency);
                decimal finalOldPrice = _currencyService.ConvertFromPrimaryStoreCurrency(finalPriceWithDiscountBase+_catalogSettings.SaveAmount, _workContext.WorkingCurrency);
                price = _priceFormatter.FormatPrice(finalPriceWithDiscount);
                oldPrice = _priceFormatter.FormatPrice(finalOldPrice);
            }

            //stock
            var stockAvailability = product.FormatStockMessage(attributeXml, _localizationService, _productAttributeParser);

            //conditional attributes
            var enabledAttributeMappingIds = new List<int>();
            var disabledAttributeMappingIds = new List<int>();
            var renderPictureRequest = new RenderPictureRequest();
            if (validateAttributeConditions)
            {
                var attributes = _productAttributeService.GetProductAttributeMappingsByProductId(product.Id);
                foreach (var attribute in attributes)
                {
                    var conditionMet = _productAttributeParser.IsConditionMet(attribute, attributeXml);
                    if (conditionMet.HasValue)
                    {
                        if (conditionMet.Value)
                            enabledAttributeMappingIds.Add(attribute.Id);
                        else
                            disabledAttributeMappingIds.Add(attribute.Id);
                    }
                }
            }

            //picture. used when we want to override a default product picture when some attribute is selected
            var pictureFullSizeUrl = "";
            var pictureDefaultSizeUrl = "";
            var pictureRenderedUrl = "";
            int selectedAttributeValueTypeId = 0;
            int selectedAttributeValueColorId = 0;
            if (loadPicture)
            {
                //just load (return) the first found picture (in case if we have several distinct attributes with associated pictures)
                //actually we're going to support pictures associated to attribute combinations (not attribute values) soon. it'll more flexible approach
                var attributeValues = _productAttributeParser.ParseProductAttributeValues(attributeXml);
                var attributeValueWithPicture = attributeValues.FirstOrDefault(x => x.ProductAttributeMapping.AttributeControlType == AttributeControlType.ColorSquares);
                if(attributeValueWithPicture==null)
                {
                    attributeValueWithPicture = attributeValues.FirstOrDefault(x => x.PictureId > 0);
                }
                if (attributeValueWithPicture != null)
                {
                    var attributeValueWithType = attributeValues.FirstOrDefault(x => x.ProductAttributeMapping.ProductAttribute.Name.Equals("Available Products"));
                    renderPictureRequest.Data = this.ParseProductCustomFieldData(product, form, attributeValueWithType.Id);
                    renderPictureRequest.AttrValueId = attributeValueWithPicture.Id;
                    renderPictureRequest.Scale = 768;

                    selectedAttributeValueTypeId = attributeValueWithType.Id;
                    selectedAttributeValueColorId = attributeValueWithPicture.Id;
                    if (renderPictureRequest.Data.Any())
                    {
                        pictureRenderedUrl = this.GetPictureRenderUrl(Url.RouteUrl("RenderProductImage"), renderPictureRequest);
                        //pictureBase64Data = string.Format("data:image/png;base64,{0}", _renderPictureService.RendingCustomProductPicture(renderPictureRequest));
                    }
                    else
                    {
                        if (attributeValueWithPicture != null)
                        {
                            var productAttributePictureCacheKey = string.Format(ModelCacheEventConsumer.PRODUCTATTRIBUTE_PICTURE_MODEL_KEY,
                                            attributeValueWithPicture.PictureId,
                                            _webHelper.IsCurrentConnectionSecured(),
                                            _storeContext.CurrentStore.Id);
                            var pictureModel = _cacheManager.Get(productAttributePictureCacheKey, () =>
                            {
                                var valuePicture = _pictureService.GetPictureById(attributeValueWithPicture.PictureId);
                                if (valuePicture != null)
                                {
                                    return new PictureModel
                                    {
                                        FullSizeImageUrl = _pictureService.GetPictureUrl(valuePicture),
                                        ImageUrl = _pictureService.GetPictureUrl(valuePicture, _mediaSettings.ProductDetailsPictureSize)
                                    };
                                }
                                return new PictureModel();
                            });
                            pictureFullSizeUrl = pictureModel.FullSizeImageUrl;
                            pictureDefaultSizeUrl = pictureModel.ImageUrl;
                            pictureRenderedUrl = pictureModel.FullSizeImageUrl;
                        }
                    }
                }

            }
            var jsonResult = Json(
                new
                {
                    gtin = gtin,
                    mpn = mpn,
                    sku = sku,
                    price = price,
                    oldPrice = oldPrice,
                    stockAvailability = stockAvailability,
                    enabledattributemappingids = enabledAttributeMappingIds.ToArray(),
                    disabledattributemappingids = disabledAttributeMappingIds.ToArray(),
                    pictureFullSizeUrl = pictureFullSizeUrl,
                    pictureDefaultSizeUrl = pictureDefaultSizeUrl,
                    pictureRenderedUrl = pictureRenderedUrl,
                    selectedAttributeValueTypeId = selectedAttributeValueTypeId,
                    selectedAttributeValueColorId = selectedAttributeValueColorId,
                    renderPictureRequest = renderPictureRequest
                }
                , JsonRequestBehavior.AllowGet);
            return jsonResult;
        }
        protected virtual string GetPictureRenderUrl(string route, RenderPictureRequest request)
        {
            var url = string.Empty;
            try
            {
                url = route + "?";
                url += "AttrValueId=" + request.AttrValueId + '&';
                for (int index = 0; index < request.Data.Count; index++)
                {
                    url += string.Format("Data[{0}].Text={1}&", index, request.Data[index].Text);
                    url += string.Format("Data[{0}].ValueId={1}&", index, request.Data[index].ValueId);
                }
                url += "Scale=" + request.Scale;
            }
            catch (Exception ex)
            {
            }
            return url;
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CheckoutAttributeChange(FormCollection form)
        {
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();

            ParseAndSaveCheckoutAttributes(cart, form);
            var attributeXml = _workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.CheckoutAttributes,
                _genericAttributeService, _storeContext.CurrentStore.Id);

            var enabledAttributeIds = new List<int>();
            var disabledAttributeIds = new List<int>();
            var attributes = _checkoutAttributeService.GetAllCheckoutAttributes(_storeContext.CurrentStore.Id, !cart.RequiresShipping());
            foreach (var attribute in attributes)
            {
                var conditionMet = _checkoutAttributeParser.IsConditionMet(attribute, attributeXml);
                if (conditionMet.HasValue)
                {
                    if (conditionMet.Value)
                        enabledAttributeIds.Add(attribute.Id);
                    else
                        disabledAttributeIds.Add(attribute.Id);
                }
            }

            return Json(new
            {
                enabledattributeids = enabledAttributeIds.ToArray(),
                disabledattributeids = disabledAttributeIds.ToArray()
            });
        }

        [HttpPost]
        public ActionResult UploadFileProductAttribute(int attributeId)
        {
            var attribute = _productAttributeService.GetProductAttributeMappingById(attributeId);
            if (attribute == null || attribute.AttributeControlType != AttributeControlType.FileUpload)
            {
                return Json(new
                {
                    success = false,
                    downloadGuid = Guid.Empty,
                }, MimeTypes.TextPlain);
            }

            //we process it distinct ways based on a browser
            //find more info here http://stackoverflow.com/questions/4884920/mvc3-valums-ajax-file-upload
            Stream stream = null;
            var fileName = "";
            var contentType = "";
            if (String.IsNullOrEmpty(Request["qqfile"]))
            {
                // IE
                HttpPostedFileBase httpPostedFile = Request.Files[0];
                if (httpPostedFile == null)
                    throw new ArgumentException("No file uploaded");
                stream = httpPostedFile.InputStream;
                fileName = Path.GetFileName(httpPostedFile.FileName);
                contentType = httpPostedFile.ContentType;
            }
            else
            {
                //Webkit, Mozilla
                stream = Request.InputStream;
                fileName = Request["qqfile"];
            }

            var fileBinary = new byte[stream.Length];
            stream.Read(fileBinary, 0, fileBinary.Length);

            var fileExtension = Path.GetExtension(fileName);
            if (!String.IsNullOrEmpty(fileExtension))
                fileExtension = fileExtension.ToLowerInvariant();

            if (attribute.ValidationFileMaximumSize.HasValue)
            {
                //compare in bytes
                var maxFileSizeBytes = attribute.ValidationFileMaximumSize.Value * 1024;
                if (fileBinary.Length > maxFileSizeBytes)
                {
                    //when returning JSON the mime-type must be set to text/plain
                    //otherwise some browsers will pop-up a "Save As" dialog.
                    return Json(new
                    {
                        success = false,
                        message = string.Format(_localizationService.GetResource("ShoppingCart.MaximumUploadedFileSize"), attribute.ValidationFileMaximumSize.Value),
                        downloadGuid = Guid.Empty,
                    }, MimeTypes.TextPlain);
                }
            }

            var download = new Download
            {
                DownloadGuid = Guid.NewGuid(),
                UseDownloadUrl = false,
                DownloadUrl = "",
                DownloadBinary = fileBinary,
                ContentType = contentType,
                //we store filename without extension for downloads
                Filename = Path.GetFileNameWithoutExtension(fileName),
                Extension = fileExtension,
                IsNew = true
            };
            _downloadService.InsertDownload(download);

            //when returning JSON the mime-type must be set to text/plain
            //otherwise some browsers will pop-up a "Save As" dialog.
            return Json(new
            {
                success = true,
                message = _localizationService.GetResource("ShoppingCart.FileUploaded"),
                downloadUrl = Url.Action("GetFileUpload", "Download", new { downloadId = download.DownloadGuid }),
                downloadGuid = download.DownloadGuid,
            }, MimeTypes.TextPlain);
        }

        [HttpPost]
        public ActionResult UploadFileCheckoutAttribute(int attributeId)
        {
            var attribute = _checkoutAttributeService.GetCheckoutAttributeById(attributeId);
            if (attribute == null || attribute.AttributeControlType != AttributeControlType.FileUpload)
            {
                return Json(new
                {
                    success = false,
                    downloadGuid = Guid.Empty,
                }, MimeTypes.TextPlain);
            }

            //we process it distinct ways based on a browser
            //find more info here http://stackoverflow.com/questions/4884920/mvc3-valums-ajax-file-upload
            Stream stream = null;
            var fileName = "";
            var contentType = "";
            if (String.IsNullOrEmpty(Request["qqfile"]))
            {
                // IE
                HttpPostedFileBase httpPostedFile = Request.Files[0];
                if (httpPostedFile == null)
                    throw new ArgumentException("No file uploaded");
                stream = httpPostedFile.InputStream;
                fileName = Path.GetFileName(httpPostedFile.FileName);
                contentType = httpPostedFile.ContentType;
            }
            else
            {
                //Webkit, Mozilla
                stream = Request.InputStream;
                fileName = Request["qqfile"];
            }

            var fileBinary = new byte[stream.Length];
            stream.Read(fileBinary, 0, fileBinary.Length);

            var fileExtension = Path.GetExtension(fileName);
            if (!String.IsNullOrEmpty(fileExtension))
                fileExtension = fileExtension.ToLowerInvariant();

            if (attribute.ValidationFileMaximumSize.HasValue)
            {
                //compare in bytes
                var maxFileSizeBytes = attribute.ValidationFileMaximumSize.Value * 1024;
                if (fileBinary.Length > maxFileSizeBytes)
                {
                    //when returning JSON the mime-type must be set to text/plain
                    //otherwise some browsers will pop-up a "Save As" dialog.
                    return Json(new
                    {
                        success = false,
                        message = string.Format(_localizationService.GetResource("ShoppingCart.MaximumUploadedFileSize"), attribute.ValidationFileMaximumSize.Value),
                        downloadGuid = Guid.Empty,
                    }, MimeTypes.TextPlain);
                }
            }

            var download = new Download
            {
                DownloadGuid = Guid.NewGuid(),
                UseDownloadUrl = false,
                DownloadUrl = "",
                DownloadBinary = fileBinary,
                ContentType = contentType,
                //we store filename without extension for downloads
                Filename = Path.GetFileNameWithoutExtension(fileName),
                Extension = fileExtension,
                IsNew = true
            };
            _downloadService.InsertDownload(download);

            //when returning JSON the mime-type must be set to text/plain
            //otherwise some browsers will pop-up a "Save As" dialog.
            return Json(new
            {
                success = true,
                message = _localizationService.GetResource("ShoppingCart.FileUploaded"),
                downloadUrl = Url.Action("GetFileUpload", "Download", new { downloadId = download.DownloadGuid }),
                downloadGuid = download.DownloadGuid,
            }, MimeTypes.TextPlain);
        }


        [NopHttpsRequirement(SslRequirement.Yes)]
        public ActionResult Cart()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.EnableShoppingCart))
                return RedirectToRoute("HomePage");

            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();
            var model = new ShoppingCartModel();
            PrepareShoppingCartModel(model, cart);
            return View(model);
        }

        [ChildActionOnly]
        public ActionResult OrderSummary(bool? prepareAndDisplayOrderReviewData)
        {
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();
            var model = new ShoppingCartModel();
            PrepareShoppingCartModel(model, cart,
                isEditable: false,
                prepareEstimateShippingIfEnabled: false,
                prepareAndDisplayOrderReviewData: prepareAndDisplayOrderReviewData.GetValueOrDefault());
            return PartialView(model);
        }


        [HttpPost, ActionName("Cart")]
        [FormValueRequired("updatecart")]
        public ActionResult UpdateCart(FormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.EnableShoppingCart))
                return RedirectToRoute("HomePage");

            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();

            var allIdsToRemove = form["removefromcart"] != null ? form["removefromcart"].Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(x => int.Parse(x)).ToList() : new List<int>();

            //current warnings <cart item identifier, warnings>
            var innerWarnings = new Dictionary<int, IList<string>>();
            foreach (var sci in cart)
            {
                bool remove = allIdsToRemove.Contains(sci.Id);
                if (remove)
                    _shoppingCartService.DeleteShoppingCartItem(sci, ensureOnlyActiveCheckoutAttributes: true);
                else
                {
                    foreach (string formKey in form.AllKeys)
                        if (formKey.Equals(string.Format("itemquantity{0}", sci.Id), StringComparison.InvariantCultureIgnoreCase))
                        {
                            int newQuantity;
                            if (int.TryParse(form[formKey], out newQuantity))
                            {
                                var currSciWarnings = _shoppingCartService.UpdateShoppingCartItem(_workContext.CurrentCustomer,
                                    sci.Id, sci.AttributesXml, sci.CustomerEnteredPrice,
                                    sci.RentalStartDateUtc, sci.RentalEndDateUtc,
                                    newQuantity, true);
                                innerWarnings.Add(sci.Id, currSciWarnings);
                            }
                            break;
                        }
                }
            }

            //parse and save checkout attributes
            ParseAndSaveCheckoutAttributes(cart, form);

            //updated cart
            cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();
            var model = new ShoppingCartModel();
            PrepareShoppingCartModel(model, cart);
            //update current warnings
            foreach (var kvp in innerWarnings)
            {
                //kvp = <cart item identifier, warnings>
                var sciId = kvp.Key;
                var warnings = kvp.Value;
                //find model
                var sciModel = model.Items.FirstOrDefault(x => x.Id == sciId);
                if (sciModel != null)
                    foreach (var w in warnings)
                        if (!sciModel.Warnings.Contains(w))
                            sciModel.Warnings.Add(w);
            }
            return View(model);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult UpdateCartItemByAjax(int Id, int Quantity, int SizeId, bool IsRemove = false)
        {
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id).ToList();
            var innerWarnings = new Dictionary<int, IList<string>>();
            var selectedCart = cart.FirstOrDefault(c => c.Id == Id);
            if (selectedCart != null)
            {
                if (IsRemove)
                    _shoppingCartService.DeleteShoppingCartItem(selectedCart, ensureOnlyActiveCheckoutAttributes: true);
                else
                {

                    if (selectedCart.Quantity != Quantity)
                    {
                        var currSciWarnings = _shoppingCartService.UpdateShoppingCartItem(_workContext.CurrentCustomer,
                       selectedCart.Id, selectedCart.AttributesXml, selectedCart.CustomerEnteredPrice,
                       selectedCart.RentalStartDateUtc, selectedCart.RentalEndDateUtc,
                       Quantity, true);
                        innerWarnings.Add(selectedCart.Id, currSciWarnings);
                    }
                    else
                    {
                        var itemAttributeMappings = selectedCart.Product.ProductAttributeMappings;
                        var itemAttributeValues = _productAttributeParser.ParseProductAttributeValues(selectedCart.AttributesXml);
                        var itemSelectedAttributeMappings = _productAttributeParser.ParseProductAttributeMappings(selectedCart.AttributesXml);

                        ProductAttributeValue selectedSize = null;
                        ProductAttributeValue selectedColor = null;
                        ProductAttributeValue selectedStyle = null;

                        //Parse Cart item style
                        var allStyle = itemAttributeMappings.Where(p => p.ProductAttribute.Name.Equals(EggCommon.ProductTypeAttributeValue))
                           .Select(iam => iam.ProductAttributeValues).FirstOrDefault();
                        selectedStyle = itemAttributeValues.Where(p => p.ProductAttributeMappingId == allStyle.FirstOrDefault().ProductAttributeMappingId).FirstOrDefault();

                        //parse cart item sizing
                        var selectedSizingAttrMapping = itemSelectedAttributeMappings.Where(p => p.ProductAttribute.Name.Substring(0, 5).Equals("size_")).FirstOrDefault();
                        if (selectedSizingAttrMapping != null)
                        {
                            foreach (var cartAttrValue in itemAttributeValues)
                            {
                                foreach (var itemSizeAttrValue in selectedSizingAttrMapping.ProductAttributeValues)
                                {
                                    if (cartAttrValue.Id == itemSizeAttrValue.Id)
                                    {
                                        selectedSize = cartAttrValue;
                                        break;
                                    }
                                }
                            }
                        }

                        //parse cart item color

                        var allColor = itemAttributeMappings.Where(p => p.AttributeControlType == AttributeControlType.ColorSquares).ToList();
                        selectedColor = itemAttributeValues.Where(p => p.ProductAttributeMapping.AttributeControlType == AttributeControlType.ColorSquares).FirstOrDefault();
                        var allSizing = itemSelectedAttributeMappings.Where(p => p.ProductAttribute.Name.Substring(0, 5).Equals("size_"))
                            .Select(iam => iam.ProductAttributeValues).FirstOrDefault();
                        if (selectedSize.Id != SizeId)
                        {
                            var newSize = allSizing.FirstOrDefault(p => p.Id == SizeId);
                            if (newSize != null)
                            {
                                string attributesXml = "";
                                foreach (var attr in itemSelectedAttributeMappings)
                                {
                                    if (attr.ProductAttribute.Name.Equals("Available Products"))
                                    {
                                        if (selectedStyle != null)
                                        {
                                            attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                                                                 selectedStyle.ProductAttributeMapping, selectedStyle.Id.ToString());
                                        }
                                    }
                                    else if (attr.ProductAttribute.Name.Substring(0, 5).Equals("size_"))
                                    {
                                        attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                                                                 newSize.ProductAttributeMapping, newSize.Id.ToString());
                                    }
                                    else if (attr.ProductAttribute.Name.Equals("CustomFieldValue"))
                                    {
                                        var parseData = _productAttributeParser.ParserCartItemCustomData(selectedCart.AttributesXml).ToList();
                                        var jsonSerialiser = new JavaScriptSerializer();
                                        var jsonValue = jsonSerialiser.Serialize(parseData);
                                        attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                                                                  attr, jsonValue);

                                    }
                                    else if (attr.AttributeControlType == AttributeControlType.ColorSquares)
                                    {
                                       if(selectedColor!=null)
                                        {
                                            attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                                                                 selectedColor.ProductAttributeMapping, selectedColor.Id.ToString());
                                        }
                                    }
                                }
                                var currSciWarnings = _shoppingCartService.UpdateShoppingCartItem(_workContext.CurrentCustomer,
                                 selectedCart.Id, attributesXml, selectedCart.CustomerEnteredPrice,
                                 selectedCart.RentalStartDateUtc, selectedCart.RentalEndDateUtc,
                                  Quantity, true);
                                innerWarnings.Add(selectedCart.Id, currSciWarnings);
                            }
                        }
                    }
                }
            }
            //parse and save checkout attributes
            //ParseAndSaveCheckoutAttributes(cart, null);

            //updated cart
            cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();
            var model = new ShoppingCartModel();
            PrepareShoppingCartModel(model, cart);
            // Egg code
            foreach (var item in model.Items)
            {
                item.AttributeInfo = EggCommon.PrepareEggProductAttribute(item.AttributeInfo);
            }
            //update current warnings
            foreach (var kvp in innerWarnings)
            {
                //kvp = <cart item identifier, warnings>
                var sciId = kvp.Key;
                var warnings = kvp.Value;
                //find model
                var sciModel = model.Items.FirstOrDefault(x => x.Id == sciId);
                if (sciModel != null)
                    foreach (var w in warnings)
                        if (!sciModel.Warnings.Contains(w))
                            sciModel.Warnings.Add(w);
            }
            return PartialView(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult UpdateCheckoutOrderSummary(int Id, int Quantity, int SizeId, bool IsRemove = false)
        {
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id).ToList();
            var innerWarnings = new Dictionary<int, IList<string>>();
            var selectedCart = cart.FirstOrDefault(c => c.Id == Id);
            if (selectedCart != null)
            {
                if (IsRemove)
                    _shoppingCartService.DeleteShoppingCartItem(selectedCart, ensureOnlyActiveCheckoutAttributes: true);
                else
                {

                    if (selectedCart.Quantity != Quantity)
                    {
                        var currSciWarnings = _shoppingCartService.UpdateShoppingCartItem(_workContext.CurrentCustomer,
                       selectedCart.Id, selectedCart.AttributesXml, selectedCart.CustomerEnteredPrice,
                       selectedCart.RentalStartDateUtc, selectedCart.RentalEndDateUtc,
                       Quantity, true);
                        innerWarnings.Add(selectedCart.Id, currSciWarnings);
                    }
                    else
                    {
                        var itemAttributeMappings = selectedCart.Product.ProductAttributeMappings;
                        var itemAttributeValues = _productAttributeParser.ParseProductAttributeValues(selectedCart.AttributesXml);
                        var itemSelectedAttributeMappings = _productAttributeParser.ParseProductAttributeMappings(selectedCart.AttributesXml);
                        var allSizing = itemSelectedAttributeMappings.Where(p => p.ProductAttribute.Name.Substring(0, 5).Equals("size_"))
                            .Select(iam => iam.ProductAttributeValues).FirstOrDefault();
                        var selectedSize = itemAttributeValues.Where(p => p.ProductAttributeMappingId == allSizing.FirstOrDefault().ProductAttributeMappingId).FirstOrDefault();
                        if (selectedSize.Id != SizeId)
                        {
                            var selectedColor = itemAttributeValues.FirstOrDefault(p => p.PictureId > 0);
                            var selectedType = itemAttributeValues.Where(p => p.ProductAttributeMapping.ProductAttribute.Name.Equals("Available Products")).FirstOrDefault();
                            var newSize = allSizing.FirstOrDefault(p => p.Id == SizeId);
                            if (selectedColor != null && selectedType != null && newSize != null)
                            {
                                string attributesXml = "";
                                foreach (var attr in itemSelectedAttributeMappings)
                                {
                                    if (attr.ProductAttribute.Name.Equals("Available Products"))
                                    {
                                        attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                                                                 selectedType.ProductAttributeMapping, selectedType.Id.ToString());
                                    }
                                    else if (attr.ProductAttribute.Name.Substring(0, 5).Equals("size_"))
                                    {
                                        attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                                                                 newSize.ProductAttributeMapping, newSize.Id.ToString());
                                    }
                                    else if (attr.ProductAttribute.Name.Equals("CustomFieldValue"))
                                    {
                                        var parseData = _productAttributeParser.ParserCartItemCustomData(selectedCart.AttributesXml).ToList();
                                        var jsonSerialiser = new JavaScriptSerializer();
                                        var jsonValue = jsonSerialiser.Serialize(parseData);
                                        attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                                                                  attr, jsonValue);

                                    }
                                    else if (attr.AttributeControlType == AttributeControlType.ColorSquares)
                                    {
                                        attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                                                                  selectedColor.ProductAttributeMapping, selectedColor.Id.ToString());
                                    }
                                }
                                var currSciWarnings = _shoppingCartService.UpdateShoppingCartItem(_workContext.CurrentCustomer,
                                 selectedCart.Id, attributesXml, selectedCart.CustomerEnteredPrice,
                                 selectedCart.RentalStartDateUtc, selectedCart.RentalEndDateUtc,
                                  Quantity, true);
                                innerWarnings.Add(selectedCart.Id, currSciWarnings);
                            }
                        }
                    }
                }
            }
            //parse and save checkout attributes
            //ParseAndSaveCheckoutAttributes(cart, null);

            //updated cart
            cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();
            var model = new EggOrderSummaryModel();

            // prepair shopping cart model
            var shoppingCartModel = new ShoppingCartModel();
            PrepareShoppingCartModel(shoppingCartModel, cart);
            model.ShoppingCartModel = shoppingCartModel;

            //default address and shipping option
            Address address = _workContext.CurrentCustomer.Addresses.FirstOrDefault();
            if (address == null)
            {
                address = new Address
                {
                    CountryId = 1,
                    Country = _countryService.GetCountryById(1),
                    StateProvinceId = null,
                    StateProvince = null,
                    ZipPostalCode = null,
                    CreatedOnUtc = DateTime.UtcNow
                };
            }

            _workContext.CurrentCustomer.ShippingAddress = address;
            _workContext.CurrentCustomer.BillingAddress = address;
            _customerService.UpdateCustomer(_workContext.CurrentCustomer);

            GetShippingOptionResponse getShippingOptionResponse = _shippingService
                .GetShippingOptions(cart, address, "", _storeContext.CurrentStore.Id);
            if (getShippingOptionResponse.ShippingOptions.Count > 0)
                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedShippingOption, getShippingOptionResponse.ShippingOptions[0], _storeContext.CurrentStore.Id);

            // prepair total order
            var orderTotalsModel = new OrderTotalsModel();
            orderTotalsModel = PrepareOrderTotalsModel(cart, false);
            model.OrderTotalModel = orderTotalsModel;

            return PartialView(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddAnOtherStyle(UpdateCartItemModel updateModel, int maxId = 0)
        {
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id).ToList();
            var model = new ShoppingCartModel.ShoppingCartItemModel();
            if (cart.Any())
            {
                var selectedCart = cart.Where(p => p.Id == updateModel.Id).FirstOrDefault();
                if (selectedCart != null)
                {
                    updateModel.ProductId = selectedCart.ProductId;
                    var newAttributesXml = this.UpdateAttributeXml(selectedCart.AttributesXml, updateModel);
                    var cartItemModel = new ShoppingCartModel.ShoppingCartItemModel
                    {
                        Id = maxId + 1,
                        TemporaryId = selectedCart.Id,
                        Sku = selectedCart.Product.FormatSku(newAttributesXml, _productAttributeParser),
                        ProductId = selectedCart.Product.Id,
                        ProductName = selectedCart.Product.GetLocalized(x => x.Name),
                        ProductSeName = selectedCart.Product.GetSeName(),
                        Quantity = 1,
                        AttributeInfo = _productAttributeFormatter.FormatAttributes(selectedCart.Product, newAttributesXml),
                        AttributeParsed = _productAttributeFormatter.FormatShoppingCartAttributes(newAttributesXml),
                        AttributeXml = newAttributesXml
                    };
                    var itemAttributeMappings = selectedCart.Product.ProductAttributeMappings;
                    var itemAttributeValues = _productAttributeParser.ParseProductAttributeValues(newAttributesXml);
                    var itemSelectedAttributeMappings = _productAttributeParser.ParseProductAttributeMappings(newAttributesXml);
                    var allColor = itemAttributeMappings.Where(p => p.AttributeControlTypeId == 40).ToList();

                    var allType = itemAttributeMappings.Where(p => p.ProductAttribute.Name.Equals("Available Products"))
                        .Select(iam => iam.ProductAttributeValues).FirstOrDefault();
                    var allSizing = itemSelectedAttributeMappings.Where(p => p.ProductAttribute.Name.Substring(0, 5).Equals("size_"))
                        .Select(iam => iam.ProductAttributeValues).FirstOrDefault();

                    var selectedType = _productAttributeService.GetProductAttributeValueById(updateModel.TypeId);

                    var selectedColor = _productAttributeService.GetProductAttributeValueById(updateModel.ColorId);
                    var defaultSize = new ShoppingCartModel.ShoppingCartAttributeValueModel
                    {
                        Id = 0,
                        ColorSquaresRgb = null,
                        IsPreSelected = true,
                        Name = "---"
                    };
                    cartItemModel.SizingAttributeValues.Add(defaultSize);
                    foreach (var size in allSizing)
                    {
                        var tempSize = new ShoppingCartModel.ShoppingCartAttributeValueModel
                        {
                            Id = size.Id,
                            ColorSquaresRgb = size.ColorSquaresRgb,
                            IsPreSelected = false,
                            Name = size.Name
                        };
                        cartItemModel.SizingAttributeValues.Add(tempSize);
                    }

                    foreach (var styleAttrValue in allType)
                    {
                        foreach (var colorAttrValue in allColor)
                        {
                            var valuesThatShouldBeSelected = _productAttributeParser.ParseConditionValue(colorAttrValue.ConditionAttributeXml, styleAttrValue.ProductAttributeMappingId, styleAttrValue.Id).ToList();
                            if (valuesThatShouldBeSelected.Any())
                            {
                                foreach (var item in colorAttrValue.ProductAttributeValues)
                                {
                                    var tempStyle = new ShoppingCartModel.ShoppingCartAttributeValueModel
                                    {
                                        Id = item.Id,
                                        ProductTypeId = styleAttrValue.Id,
                                        ColorSquaresRgb = item.ColorSquaresRgb,
                                        IsPreSelected = item.Id == selectedColor.Id,
                                        Name = string.Format("{0} - {1}", styleAttrValue.Name, item.Name)
                                    };
                                    cartItemModel.StyleAttributeValues.Add(tempStyle);
                                }
                            }
                        }
                    }

                    //picture
                    if (_shoppingCartSettings.ShowProductImagesOnShoppingCart)
                    {
                        var renderPictureRequest = new RenderPictureRequest();
                        renderPictureRequest.Data = _productAttributeParser.ParserCartItemCustomData(newAttributesXml).ToList();
                        renderPictureRequest.AttrValueId = selectedColor.Id;
                        renderPictureRequest.Scale = _mediaSettings.CartThumbPictureSize;
                        if (renderPictureRequest.Data.Any())
                        {
                            cartItemModel.Picture = PrepareCartItemPictureModel(selectedCart,
                                _mediaSettings.CartThumbPictureSize, true, cartItemModel.ProductName, renderPictureRequest);
                        }
                        else
                        {
                            cartItemModel.Picture = PrepareCartItemPictureModel(selectedCart,
                           _mediaSettings.CartThumbPictureSize, true, cartItemModel.ProductName, null);
                        }

                    }
                    //allow editing?
                    //1. setting enabled?
                    //2. simple product?
                    //3. has attribute or gift card?
                    //4. visible individually?
                    cartItemModel.AllowItemEditing = _shoppingCartSettings.AllowCartItemEditing &&
                        selectedCart.Product.ProductType == ProductType.SimpleProduct &&
                        (!String.IsNullOrEmpty(cartItemModel.AttributeInfo) || selectedCart.Product.IsGiftCard) &&
                        selectedCart.Product.VisibleIndividually;

                    //allowed quantities
                    var allowedQuantities = selectedCart.Product.ParseAllowedQuantities();
                    foreach (var qty in allowedQuantities)
                    {
                        cartItemModel.AllowedQuantities.Add(new SelectListItem
                        {
                            Text = qty.ToString(),
                            Value = qty.ToString(),
                            Selected = updateModel.Quantity == qty
                        });
                    }

                    //recurring info
                    if (selectedCart.Product.IsRecurring)
                        cartItemModel.RecurringInfo = string.Format(_localizationService.GetResource("ShoppingCart.RecurringPeriod"), selectedCart.Product.RecurringCycleLength, selectedCart.Product.RecurringCyclePeriod.GetLocalizedEnum(_localizationService, _workContext));

                    //rental info
                    if (selectedCart.Product.IsRental)
                    {
                        var rentalStartDate = selectedCart.RentalStartDateUtc.HasValue ? selectedCart.Product.FormatRentalDate(selectedCart.RentalStartDateUtc.Value) : "";
                        var rentalEndDate = selectedCart.RentalEndDateUtc.HasValue ? selectedCart.Product.FormatRentalDate(selectedCart.RentalEndDateUtc.Value) : "";
                        cartItemModel.RentalInfo = string.Format(_localizationService.GetResource("ShoppingCart.Rental.FormattedDate"),
                            rentalStartDate, rentalEndDate);
                    }

                    //unit prices
                    if (selectedCart.Product.CallForPrice)
                    {
                        cartItemModel.UnitPrice = _localizationService.GetResource("Products.CallForPrice");
                    }
                    else
                    {

                        cartItemModel.UnitPrice = _priceFormatter.FormatPrice(updateModel.Quantity * selectedType.PriceAdjustment);
                    }
                    //subtotal, discount
                    if (selectedCart.Product.CallForPrice)
                    {
                        cartItemModel.SubTotal = _localizationService.GetResource("Products.CallForPrice");
                    }
                    else
                    {
                        cartItemModel.SubTotal = _priceFormatter.FormatPrice(updateModel.Quantity * selectedType.PriceAdjustment);
                    }

                    //item warnings
                    var itemWarnings = _shoppingCartService.GetShoppingCartItemWarnings(
                        _workContext.CurrentCustomer,
                        selectedCart.ShoppingCartType,
                        selectedCart.Product,
                        selectedCart.StoreId,
                        newAttributesXml,
                        selectedCart.CustomerEnteredPrice,
                        selectedCart.RentalStartDateUtc,
                        selectedCart.RentalEndDateUtc,
                        updateModel.Quantity,
                        false);
                    foreach (var warning in itemWarnings)
                        cartItemModel.Warnings.Add(warning);

                    model = cartItemModel;
                }
            }
            return PartialView(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult OnAddToCart_Redirection(IList<UpdateCartItemModel> cartItems, bool gotoCheckout)
        {
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
               .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
               .LimitPerStore(_storeContext.CurrentStore.Id).ToList();
            foreach (var item in cartItems)
            {
                if (!item.IsNew)
                {
                    var selectedCart = cart.Where(p => p.Id == item.Id).FirstOrDefault();
                    if (selectedCart != null)
                    {
                        if (item.IsRemove)
                            _shoppingCartService.DeleteShoppingCartItem(selectedCart, ensureOnlyActiveCheckoutAttributes: true);
                        else
                        {

                            var currSciWarnings = _shoppingCartService.UpdateShoppingCartItem(_workContext.CurrentCustomer,
                              selectedCart.Id, item.AttributeXml, selectedCart.CustomerEnteredPrice,
                              selectedCart.RentalStartDateUtc, selectedCart.RentalEndDateUtc,
                              item.Quantity, true);
                        }
                    }
                }
                else
                {
                    var product = _productService.GetProductById(item.ProductId);
                    if (product == null)
                        break;
                    var addToCartWarnings = new List<string>();
                    var newAttributeXml = this.UpdateAttributeXml(item.AttributeXml, item);
                    //add to the cart
                    addToCartWarnings.AddRange(_shoppingCartService.AddToCart(_workContext.CurrentCustomer,
                        product, ShoppingCartType.ShoppingCart, _storeContext.CurrentStore.Id, newAttributeXml));
                }
            }
            string targetUrl = "";
            if (gotoCheckout)
                targetUrl = "/checkout";
            else
                targetUrl = "/cart";

            return Json(targetUrl);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ShoppingCartItem_AttributeChange(UpdateCartItemModel updateModel)
        {
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id).ToList();
            var model = new ShoppingCartModel.ShoppingCartItemModel();
            if (cart.Any())
            {
                ShoppingCartItem selectedCart = null;
                var exacltyCartId = updateModel.IsNew ? updateModel.TemporaryId : updateModel.Id;
                selectedCart = cart.Where(p => p.Id == exacltyCartId).FirstOrDefault();
                if (selectedCart != null)
                {
                    updateModel.ProductId = selectedCart.ProductId;
                    var oldAttributeXml = updateModel.IsNew ? updateModel.AttributeXml : selectedCart.AttributesXml;
                    var newAttributesXml = this.UpdateAttributeXml(oldAttributeXml, updateModel);
                    var cartItemModel = new ShoppingCartModel.ShoppingCartItemModel
                    {
                        Id = updateModel.Id,
                        TemporaryId = updateModel.TemporaryId,
                        Sku = selectedCart.Product.FormatSku(newAttributesXml, _productAttributeParser),
                        ProductId = selectedCart.Product.Id,
                        ProductName = selectedCart.Product.GetLocalized(x => x.Name),
                        ProductSeName = selectedCart.Product.GetSeName(),
                        Quantity = updateModel.Quantity,
                        AttributeInfo = _productAttributeFormatter.FormatAttributes(selectedCart.Product, newAttributesXml),
                        AttributeParsed = _productAttributeFormatter.FormatShoppingCartAttributes(newAttributesXml),
                        AttributeXml = newAttributesXml
                    };
                    var itemAttributeMappings = selectedCart.Product.ProductAttributeMappings;
                    var itemAttributeValues = _productAttributeParser.ParseProductAttributeValues(newAttributesXml);
                    var itemSelectedAttributeMappings = _productAttributeParser.ParseProductAttributeMappings(newAttributesXml);
                    var allColor = itemAttributeMappings.Where(p => p.AttributeControlTypeId == 40).ToList();

                    var allType = itemAttributeMappings.Where(p => p.ProductAttribute.Name.Equals("Available Products"))
                        .Select(iam => iam.ProductAttributeValues).FirstOrDefault();
                    var allSizing = itemSelectedAttributeMappings.Where(p => p.ProductAttribute.Name.Substring(0, 5).Equals("size_"))
                        .Select(iam => iam.ProductAttributeValues).FirstOrDefault();

                    var selectedType = _productAttributeService.GetProductAttributeValueById(updateModel.TypeId);

                    var selectedSize = itemAttributeValues.Where(p => p.ProductAttributeMappingId == allSizing.FirstOrDefault().ProductAttributeMappingId).FirstOrDefault();
                    selectedSize = selectedSize ?? new ProductAttributeValue { Id = 0, IsPreSelected = true, Name = "---" };
                    var selectedColor = _productAttributeService.GetProductAttributeValueById(updateModel.ColorId);

                    if (updateModel.IsNew)
                    {
                        var defaultSize = new ShoppingCartModel.ShoppingCartAttributeValueModel { Id = 0, IsPreSelected = true, Name = "---" };
                        cartItemModel.SizingAttributeValues.Add(defaultSize);
                        foreach (var size in allSizing)
                        {
                            var tempSize = new ShoppingCartModel.ShoppingCartAttributeValueModel
                            {
                                Id = size.Id,
                                ColorSquaresRgb = size.ColorSquaresRgb,
                                IsPreSelected = false,
                                Name = size.Name
                            };
                            cartItemModel.SizingAttributeValues.Add(tempSize);
                        }
                    }
                    else
                    {
                        foreach (var size in allSizing)
                        {
                            var tempSize = new ShoppingCartModel.ShoppingCartAttributeValueModel
                            {
                                Id = size.Id,
                                ColorSquaresRgb = size.ColorSquaresRgb,
                                IsPreSelected = size.Id == selectedSize.Id,
                                Name = size.Name
                            };
                            cartItemModel.SizingAttributeValues.Add(tempSize);
                        }
                    }

                    foreach (var styleAttrValue in allType)
                    {
                        foreach (var colorAttrValue in allColor)
                        {
                            var valuesThatShouldBeSelected = _productAttributeParser.ParseConditionValue(colorAttrValue.ConditionAttributeXml, styleAttrValue.ProductAttributeMappingId, styleAttrValue.Id).ToList();
                            if (valuesThatShouldBeSelected.Any())
                            {
                                foreach (var item in colorAttrValue.ProductAttributeValues)
                                {
                                    var tempStyle = new ShoppingCartModel.ShoppingCartAttributeValueModel
                                    {
                                        Id = item.Id,
                                        ProductTypeId = styleAttrValue.Id,
                                        ColorSquaresRgb = item.ColorSquaresRgb,
                                        IsPreSelected = item.Id == selectedColor.Id,
                                        Name = string.Format("{0} - {1}", styleAttrValue.Name, item.Name)
                                    };
                                    cartItemModel.StyleAttributeValues.Add(tempStyle);
                                }
                            }
                        }
                    }

                    //picture
                    if (_shoppingCartSettings.ShowProductImagesOnShoppingCart)
                    {
                        var renderPictureRequest = new RenderPictureRequest();
                        renderPictureRequest.Data = _productAttributeParser.ParserCartItemCustomData(newAttributesXml).ToList();
                        renderPictureRequest.AttrValueId = selectedColor.Id;
                        renderPictureRequest.Scale = _mediaSettings.CartThumbPictureSize;
                        if (renderPictureRequest.Data.Any())
                        {
                            cartItemModel.Picture = PrepareCartItemPictureModel(selectedCart,
                                _mediaSettings.CartThumbPictureSize, true, cartItemModel.ProductName, renderPictureRequest);
                        }
                        else
                        {
                            cartItemModel.Picture = PrepareCartItemPictureModel(selectedCart,
                           _mediaSettings.CartThumbPictureSize, true, cartItemModel.ProductName, null);
                        }

                    }
                    //allow editing?
                    //1. setting enabled?
                    //2. simple product?
                    //3. has attribute or gift card?
                    //4. visible individually?
                    cartItemModel.AllowItemEditing = _shoppingCartSettings.AllowCartItemEditing &&
                        selectedCart.Product.ProductType == ProductType.SimpleProduct &&
                        (!String.IsNullOrEmpty(cartItemModel.AttributeInfo) || selectedCart.Product.IsGiftCard) &&
                        selectedCart.Product.VisibleIndividually;

                    //allowed quantities
                    var allowedQuantities = selectedCart.Product.ParseAllowedQuantities();
                    foreach (var qty in allowedQuantities)
                    {
                        cartItemModel.AllowedQuantities.Add(new SelectListItem
                        {
                            Text = qty.ToString(),
                            Value = qty.ToString(),
                            Selected = updateModel.Quantity == qty
                        });
                    }

                    //recurring info
                    if (selectedCart.Product.IsRecurring)
                        cartItemModel.RecurringInfo = string.Format(_localizationService.GetResource("ShoppingCart.RecurringPeriod"), selectedCart.Product.RecurringCycleLength, selectedCart.Product.RecurringCyclePeriod.GetLocalizedEnum(_localizationService, _workContext));

                    //rental info
                    if (selectedCart.Product.IsRental)
                    {
                        var rentalStartDate = selectedCart.RentalStartDateUtc.HasValue ? selectedCart.Product.FormatRentalDate(selectedCart.RentalStartDateUtc.Value) : "";
                        var rentalEndDate = selectedCart.RentalEndDateUtc.HasValue ? selectedCart.Product.FormatRentalDate(selectedCart.RentalEndDateUtc.Value) : "";
                        cartItemModel.RentalInfo = string.Format(_localizationService.GetResource("ShoppingCart.Rental.FormattedDate"),
                            rentalStartDate, rentalEndDate);
                    }

                    //unit prices
                    if (selectedCart.Product.CallForPrice)
                    {
                        cartItemModel.UnitPrice = _localizationService.GetResource("Products.CallForPrice");
                    }
                    else
                    {

                        cartItemModel.UnitPrice = _priceFormatter.FormatPrice(updateModel.Quantity * selectedType.PriceAdjustment);
                    }
                    //subtotal, discount
                    if (selectedCart.Product.CallForPrice)
                    {
                        cartItemModel.SubTotal = _localizationService.GetResource("Products.CallForPrice");
                    }
                    else
                    {
                        cartItemModel.SubTotal = _priceFormatter.FormatPrice(updateModel.Quantity * selectedType.PriceAdjustment);
                    }

                    //item warnings
                    var itemWarnings = _shoppingCartService.GetShoppingCartItemWarnings(
                        _workContext.CurrentCustomer,
                        selectedCart.ShoppingCartType,
                        selectedCart.Product,
                        selectedCart.StoreId,
                        newAttributesXml,
                        selectedCart.CustomerEnteredPrice,
                        selectedCart.RentalStartDateUtc,
                        selectedCart.RentalEndDateUtc,
                        updateModel.Quantity,
                        false);
                    foreach (var warning in itemWarnings)
                        cartItemModel.Warnings.Add(warning);

                    model = cartItemModel;
                }
            }
            return PartialView(model);
        }
        [ValidateInput(false)]
        [HttpPost, ActionName("Cart")]
        [FormValueRequired("continueshopping")]
        public ActionResult ContinueShopping()
        {
            var returnUrl = _workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.LastContinueShoppingPage, _storeContext.CurrentStore.Id);
            if (!String.IsNullOrEmpty(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToRoute("HomePage");
            }
        }

        [ValidateInput(false)]
        [HttpPost, ActionName("Cart")]
        [FormValueRequired("checkout")]
        public ActionResult StartCheckout(FormCollection form)
        {
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();

            //parse and save checkout attributes
            ParseAndSaveCheckoutAttributes(cart, form);

            //validate attributes
            var checkoutAttributes = _workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.CheckoutAttributes, _genericAttributeService, _storeContext.CurrentStore.Id);
            var checkoutAttributeWarnings = _shoppingCartService.GetShoppingCartWarnings(cart, checkoutAttributes, true);
            if (checkoutAttributeWarnings.Any())
            {
                //something wrong, redisplay the page with warnings
                var model = new ShoppingCartModel();
                PrepareShoppingCartModel(model, cart, validateCheckoutAttributes: true);
                return View(model);
            }

            //everything is OK
            if (_workContext.CurrentCustomer.IsGuest())
            {
                bool downloadableProductsRequireRegistration =
                    _customerSettings.RequireRegistrationForDownloadableProducts && cart.Any(sci => sci.Product.IsDownload);

                if (!_orderSettings.AnonymousCheckoutAllowed
                    || downloadableProductsRequireRegistration)
                    return new HttpUnauthorizedResult();

                return RedirectToRoute("LoginCheckoutAsGuest", new { returnUrl = Url.RouteUrl("ShoppingCart") });
            }

            return RedirectToRoute("Checkout");
        }

        [ValidateInput(false)]
        [HttpPost, ActionName("Cart")]
        [FormValueRequired("applydiscountcouponcode")]
        public ActionResult ApplyDiscountCouponForCart(string discountcouponcode, FormCollection form)
        {

            ShoppingCartModel model = OnApplyDiscountCoupon(discountcouponcode, form);

            var cart = _workContext.CurrentCustomer.ShoppingCartItems
               .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
               .LimitPerStore(_storeContext.CurrentStore.Id)
               .ToList();
            PrepareShoppingCartModel(model, cart);

            return View(model);
        }


        [ValidateInput(false)]
        [HttpPost, ActionName("Cart")]
        [FormValueRequired("applydiscountcouponcodecheckout")]
        public ActionResult ApplyDiscountCouponForCheckout(string discountcouponcodecheckout, FormCollection form)
        {
            try
            {
                ShoppingCartModel model = OnApplyDiscountCoupon(discountcouponcodecheckout, form);

                //cart
                var cart = _workContext.CurrentCustomer.ShoppingCartItems
              .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
              .LimitPerStore(_storeContext.CurrentStore.Id)
              .ToList();
                PrepareShoppingCartModel(model, cart);
                Response.Redirect("/checkout");
            }
            catch(Exception ex)
            {
            }
            return null;
        }

        protected virtual ShoppingCartModel OnApplyDiscountCoupon(string discountcouponcode, FormCollection form)
        {
            //trim
            if (discountcouponcode != null)
                discountcouponcode = discountcouponcode.Trim();

            //cart
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();

            //parse and save checkout attributes
            ParseAndSaveCheckoutAttributes(cart, form);

            var model = new ShoppingCartModel();
            if (!String.IsNullOrWhiteSpace(discountcouponcode))
            {
                //we find even hidden records here. this way we can display a user-friendly message if it's expired
                var discount = _discountService.GetDiscountByCouponCode(discountcouponcode, true);
                if (discount != null && discount.RequiresCouponCode)
                {
                    var validationResult = _discountService.ValidateDiscount(discount, _workContext.CurrentCustomer, discountcouponcode);
                    if (validationResult.IsValid)
                    {
                        //valid
                        _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, SystemCustomerAttributeNames.DiscountCouponCode, discountcouponcode);
                        model.DiscountBox.Message = _localizationService.GetResource("ShoppingCart.DiscountCouponCode.Applied");
                        model.DiscountBox.IsApplied = true;
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(validationResult.UserError))
                        {
                            //some user error
                            model.DiscountBox.Message = validationResult.UserError;
                            model.DiscountBox.IsApplied = false;
                        }
                        else
                        {
                            //general error text
                            _logger.InsertLog(Core.Domain.Logging.LogLevel.Error, string.Format("Egg Discount: general error text {0}", discountcouponcode), string.Format("Egg Discount: discount cannot be found {0}", discountcouponcode), _workContext.CurrentCustomer);

                            model.DiscountBox.Message = _localizationService.GetResource("ShoppingCart.DiscountCouponCode.WrongDiscount");
                            model.DiscountBox.IsApplied = false;
                        }
                    }
                }
                else
                {

                    //discount cannot be found
                    _logger.InsertLog(Core.Domain.Logging.LogLevel.Error, string.Format("Egg Discount: discount cannot be found {0}", discountcouponcode), string.Format("Egg Discount: discount cannot be found {0}", discountcouponcode), _workContext.CurrentCustomer);
                    model.DiscountBox.Message = _localizationService.GetResource("ShoppingCart.DiscountCouponCode.WrongDiscount");
                    model.DiscountBox.IsApplied = false;
                }
            }
            else
            {
                //empty coupon code
                _logger.InsertLog(Core.Domain.Logging.LogLevel.Error, string.Format("Egg Discount: empty coupon code {0}", discountcouponcode), string.Format("Egg Discount: discount cannot be found {0}", discountcouponcode), _workContext.CurrentCustomer);
                model.DiscountBox.Message = _localizationService.GetResource("ShoppingCart.DiscountCouponCode.WrongDiscount");
                model.DiscountBox.IsApplied = false;
            }

            PrepareShoppingCartModel(model, cart);

            return model;
        }

        [ValidateInput(false)]
        [HttpPost, ActionName("Cart")]
        [FormValueRequired("applygiftcardcouponcode")]
        public ActionResult ApplyGiftCard(string giftcardcouponcode, FormCollection form)
        {
            //trim
            if (giftcardcouponcode != null)
                giftcardcouponcode = giftcardcouponcode.Trim();

            //cart
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();

            //parse and save checkout attributes
            ParseAndSaveCheckoutAttributes(cart, form);

            var model = new ShoppingCartModel();
            if (!cart.IsRecurring())
            {
                if (!String.IsNullOrWhiteSpace(giftcardcouponcode))
                {
                    var giftCard = _giftCardService.GetAllGiftCards(giftCardCouponCode: giftcardcouponcode).FirstOrDefault();
                    bool isGiftCardValid = giftCard != null && giftCard.IsGiftCardValid();
                    if (isGiftCardValid)
                    {
                        _workContext.CurrentCustomer.ApplyGiftCardCouponCode(giftcardcouponcode);
                        _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                        model.GiftCardBox.Message = _localizationService.GetResource("ShoppingCart.GiftCardCouponCode.Applied");
                        model.GiftCardBox.IsApplied = true;
                    }
                    else
                    {
                        model.GiftCardBox.Message = _localizationService.GetResource("ShoppingCart.GiftCardCouponCode.WrongGiftCard");
                        model.GiftCardBox.IsApplied = false;
                    }
                }
                else
                {
                    model.GiftCardBox.Message = _localizationService.GetResource("ShoppingCart.GiftCardCouponCode.WrongGiftCard");
                    model.GiftCardBox.IsApplied = false;
                }
            }
            else
            {
                model.GiftCardBox.Message = _localizationService.GetResource("ShoppingCart.GiftCardCouponCode.DontWorkWithAutoshipProducts");
                model.GiftCardBox.IsApplied = false;
            }

            PrepareShoppingCartModel(model, cart);
            // Egg code
            foreach (var item in model.Items)
            {
                item.AttributeInfo = EggCommon.PrepareEggProductAttribute(item.AttributeInfo);
            }
            return View(model);
        }

        [ValidateInput(false)]
        [PublicAntiForgery]
        [HttpPost]
        public ActionResult GetEstimateShipping(int? countryId, int? stateProvinceId, string zipPostalCode, FormCollection form)
        {
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();

            //parse and save checkout attributes
            ParseAndSaveCheckoutAttributes(cart, form);

            var model = new EstimateShippingResultModel();

            if (cart.RequiresShipping())
            {
                var address = new Address
                {
                    CountryId = countryId,
                    Country = countryId.HasValue ? _countryService.GetCountryById(countryId.Value) : null,
                    StateProvinceId = stateProvinceId,
                    StateProvince = stateProvinceId.HasValue ? _stateProvinceService.GetStateProvinceById(stateProvinceId.Value) : null,
                    ZipPostalCode = zipPostalCode,
                };
                GetShippingOptionResponse getShippingOptionResponse = _shippingService
                    .GetShippingOptions(cart, address, "", _storeContext.CurrentStore.Id);
                if (getShippingOptionResponse.Success)
                {
                    if (getShippingOptionResponse.ShippingOptions.Any())
                    {
                        foreach (var shippingOption in getShippingOptionResponse.ShippingOptions)
                        {
                            var soModel = new EstimateShippingResultModel.ShippingOptionModel
                            {
                                Name = shippingOption.Name,
                                Description = shippingOption.Description,

                            };
                            //calculate discounted and taxed rate
                            List<Discount> appliedDiscounts = null;
                            decimal shippingTotal = _orderTotalCalculationService.AdjustShippingRate(shippingOption.Rate,
                                cart, out appliedDiscounts);

                            decimal rateBase = _taxService.GetShippingPrice(shippingTotal, _workContext.CurrentCustomer);
                            decimal rate = _currencyService.ConvertFromPrimaryStoreCurrency(rateBase, _workContext.WorkingCurrency);
                            soModel.Price = _priceFormatter.FormatShippingPrice(rate, true);
                            model.ShippingOptions.Add(soModel);
                        }
                    }
                }
                else
                    foreach (var error in getShippingOptionResponse.Errors)
                        model.Warnings.Add(error);

                if (_shippingSettings.AllowPickUpInStore)
                {
                    var pickupPointsResponse = _shippingService.GetPickupPoints(address, null, _storeContext.CurrentStore.Id);
                    if (pickupPointsResponse.Success)
                    {
                        if (pickupPointsResponse.PickupPoints.Any())
                        {
                            var soModel = new EstimateShippingResultModel.ShippingOptionModel
                            {
                                Name = _localizationService.GetResource("Checkout.PickupPoints"),
                                Description = _localizationService.GetResource("Checkout.PickupPoints.Description"),
                            };
                            var pickupFee = pickupPointsResponse.PickupPoints.Min(x => x.PickupFee);
                            if (pickupFee > 0)
                            {
                                pickupFee = _taxService.GetShippingPrice(pickupFee, _workContext.CurrentCustomer);
                                pickupFee = _currencyService.ConvertFromPrimaryStoreCurrency(pickupFee, _workContext.WorkingCurrency);
                            }
                            soModel.Price = _priceFormatter.FormatShippingPrice(pickupFee, true);
                            model.ShippingOptions.Add(soModel);
                        }
                    }
                    else
                        foreach (var error in pickupPointsResponse.Errors)
                            model.Warnings.Add(error);
                }

            }

            return PartialView("_EstimateShippingResult", model);
        }

        [ChildActionOnly]
        public ActionResult OrderTotals(bool isEditable)
        {
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();

            #region EggTeam Customize default address for shipping
            Address address = _workContext.CurrentCustomer.Addresses.FirstOrDefault();
            if (address == null)
            {
                address = new Address
                {
                    CountryId = 1,
                    Country = _countryService.GetCountryById(1),
                    StateProvinceId = null,
                    StateProvince = null,
                    ZipPostalCode = null,
                    CreatedOnUtc = DateTime.UtcNow
                };
            }

            _workContext.CurrentCustomer.ShippingAddress = address;
            _workContext.CurrentCustomer.BillingAddress = address;
            _customerService.UpdateCustomer(_workContext.CurrentCustomer);

            GetShippingOptionResponse getShippingOptionResponse = _shippingService
                .GetShippingOptions(cart, address, "", _storeContext.CurrentStore.Id);
            if (getShippingOptionResponse.ShippingOptions.Count > 0)
                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedShippingOption, getShippingOptionResponse.ShippingOptions[0], _storeContext.CurrentStore.Id);
            #endregion

            var model = PrepareOrderTotalsModel(cart, isEditable);
            return PartialView(model);
        }

        [ValidateInput(false)]
        [HttpPost, ActionName("Cart")]
        [FormValueRequired("removesubtotaldiscount", "removeordertotaldiscount", "removediscountcouponcode")]
        public ActionResult RemoveDiscountCoupon()
        {
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();
            var model = new ShoppingCartModel();

            _genericAttributeService.SaveAttribute<string>(_workContext.CurrentCustomer,
                SystemCustomerAttributeNames.DiscountCouponCode, null);

            PrepareShoppingCartModel(model, cart);
            return View(model);
        }

        [ValidateInput(false)]
        [HttpPost, ActionName("Cart")]
        [FormValueRequired("removediscountcouponcodecheckout")]
        public ActionResult RemoveDiscountCouponCheckout()
        {
            try
            {
                var cart = _workContext.CurrentCustomer.ShoppingCartItems
               .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
               .LimitPerStore(_storeContext.CurrentStore.Id)
               .ToList();
                var model = new ShoppingCartModel();

                _genericAttributeService.SaveAttribute<string>(_workContext.CurrentCustomer,
                    SystemCustomerAttributeNames.DiscountCouponCode, null);

                PrepareShoppingCartModel(model, cart);
                Response.Redirect("/checkout");
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        [ValidateInput(false)]
        [HttpPost, ActionName("Cart")]
        [FormValueRequired(FormValueRequirement.StartsWith, "removegiftcard-")]
        public ActionResult RemoveGiftCardCode(FormCollection form)
        {
            var model = new ShoppingCartModel();

            //get gift card identifier
            int giftCardId = 0;
            foreach (var formValue in form.AllKeys)
                if (formValue.StartsWith("removegiftcard-", StringComparison.InvariantCultureIgnoreCase))
                    giftCardId = Convert.ToInt32(formValue.Substring("removegiftcard-".Length));
            var gc = _giftCardService.GetGiftCardById(giftCardId);
            if (gc != null)
            {
                _workContext.CurrentCustomer.RemoveGiftCardCouponCode(gc.GiftCardCouponCode);
                _customerService.UpdateCustomer(_workContext.CurrentCustomer);
            }

            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();
            PrepareShoppingCartModel(model, cart);
            return View(model);
        }

        [ChildActionOnly]
        public ActionResult FlyoutShoppingCart()
        {
            if (!_shoppingCartSettings.MiniShoppingCartEnabled)
                return Content("");

            if (!_permissionService.Authorize(StandardPermissionProvider.EnableShoppingCart))
                return Content("");

            var model = PrepareMiniShoppingCartModel();
            return PartialView(model);
        }

        #endregion

        #region Wishlist

        [NopHttpsRequirement(SslRequirement.Yes)]
        public ActionResult Wishlist(Guid? customerGuid)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.EnableWishlist))
                return RedirectToRoute("HomePage");

            Customer customer = customerGuid.HasValue ?
                _customerService.GetCustomerByGuid(customerGuid.Value)
                : _workContext.CurrentCustomer;
            if (customer == null)
                return RedirectToRoute("HomePage");
            var cart = customer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.Wishlist)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();
            var model = new WishlistModel();
            PrepareWishlistModel(model, cart, !customerGuid.HasValue);
            return View(model);
        }

        [ValidateInput(false)]
        [HttpPost, ActionName("Wishlist")]
        [FormValueRequired("updatecart")]
        public ActionResult UpdateWishlist(FormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.EnableWishlist))
                return RedirectToRoute("HomePage");

            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.Wishlist)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();

            var allIdsToRemove = form["removefromcart"] != null
                ? form["removefromcart"].Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                .Select(int.Parse)
                .ToList()
                : new List<int>();

            //current warnings <cart item identifier, warnings>
            var innerWarnings = new Dictionary<int, IList<string>>();
            foreach (var sci in cart)
            {
                bool remove = allIdsToRemove.Contains(sci.Id);
                if (remove)
                    _shoppingCartService.DeleteShoppingCartItem(sci);
                else
                {
                    foreach (string formKey in form.AllKeys)
                        if (formKey.Equals(string.Format("itemquantity{0}", sci.Id), StringComparison.InvariantCultureIgnoreCase))
                        {
                            int newQuantity;
                            if (int.TryParse(form[formKey], out newQuantity))
                            {
                                var currSciWarnings = _shoppingCartService.UpdateShoppingCartItem(_workContext.CurrentCustomer,
                                    sci.Id, sci.AttributesXml, sci.CustomerEnteredPrice,
                                    sci.RentalStartDateUtc, sci.RentalEndDateUtc,
                                    newQuantity, true);
                                innerWarnings.Add(sci.Id, currSciWarnings);
                            }
                            break;
                        }
                }
            }

            //updated wishlist
            cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.Wishlist)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();
            var model = new WishlistModel();
            PrepareWishlistModel(model, cart);
            //update current warnings
            foreach (var kvp in innerWarnings)
            {
                //kvp = <cart item identifier, warnings>
                var sciId = kvp.Key;
                var warnings = kvp.Value;
                //find model
                var sciModel = model.Items.FirstOrDefault(x => x.Id == sciId);
                if (sciModel != null)
                    foreach (var w in warnings)
                        if (!sciModel.Warnings.Contains(w))
                            sciModel.Warnings.Add(w);
            }
            return View(model);
        }

        [ValidateInput(false)]
        [HttpPost, ActionName("Wishlist")]
        [FormValueRequired("addtocartbutton")]
        public ActionResult AddItemsToCartFromWishlist(Guid? customerGuid, FormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.EnableShoppingCart))
                return RedirectToRoute("HomePage");

            if (!_permissionService.Authorize(StandardPermissionProvider.EnableWishlist))
                return RedirectToRoute("HomePage");

            var pageCustomer = customerGuid.HasValue
                ? _customerService.GetCustomerByGuid(customerGuid.Value)
                : _workContext.CurrentCustomer;
            if (pageCustomer == null)
                return RedirectToRoute("HomePage");

            var pageCart = pageCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.Wishlist)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();

            var allWarnings = new List<string>();
            var numberOfAddedItems = 0;
            var allIdsToAdd = form["addtocart"] != null
                ? form["addtocart"].Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                .Select(int.Parse)
                .ToList()
                : new List<int>();
            foreach (var sci in pageCart)
            {
                if (allIdsToAdd.Contains(sci.Id))
                {
                    var warnings = _shoppingCartService.AddToCart(_workContext.CurrentCustomer,
                        sci.Product, ShoppingCartType.ShoppingCart,
                        _storeContext.CurrentStore.Id,
                        sci.AttributesXml, sci.CustomerEnteredPrice,
                        sci.RentalStartDateUtc, sci.RentalEndDateUtc, sci.Quantity, true);
                    if (!warnings.Any())
                        numberOfAddedItems++;
                    if (_shoppingCartSettings.MoveItemsFromWishlistToCart && //settings enabled
                        !customerGuid.HasValue && //own wishlist
                        !warnings.Any()) //no warnings ( already in the cart)
                    {
                        //let's remove the item from wishlist
                        _shoppingCartService.DeleteShoppingCartItem(sci);
                    }
                    allWarnings.AddRange(warnings);
                }
            }

            if (numberOfAddedItems > 0)
            {
                //redirect to the shopping cart page

                if (allWarnings.Any())
                {
                    ErrorNotification(_localizationService.GetResource("Wishlist.AddToCart.Error"), true);
                }

                return RedirectToRoute("ShoppingCart");
            }
            else
            {
                //no items added. redisplay the wishlist page

                if (allWarnings.Any())
                {
                    ErrorNotification(_localizationService.GetResource("Wishlist.AddToCart.Error"), false);
                }

                var cart = pageCustomer.ShoppingCartItems
                    .Where(sci => sci.ShoppingCartType == ShoppingCartType.Wishlist)
                    .LimitPerStore(_storeContext.CurrentStore.Id)
                    .ToList();
                var model = new WishlistModel();
                PrepareWishlistModel(model, cart, !customerGuid.HasValue);
                return View(model);
            }
        }

        [NopHttpsRequirement(SslRequirement.Yes)]
        public ActionResult EmailWishlist()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.EnableWishlist) || !_shoppingCartSettings.EmailWishlistEnabled)
                return RedirectToRoute("HomePage");

            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.Wishlist)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();

            if (!cart.Any())
                return RedirectToRoute("HomePage");

            var model = new WishlistEmailAFriendModel
            {
                YourEmailAddress = _workContext.CurrentCustomer.Email,
                DisplayCaptcha = _captchaSettings.Enabled && _captchaSettings.ShowOnEmailWishlistToFriendPage
            };
            return View(model);
        }

        [HttpPost, ActionName("EmailWishlist")]
        [PublicAntiForgery]
        [FormValueRequired("send-email")]
        [CaptchaValidator]
        public ActionResult EmailWishlistSend(WishlistEmailAFriendModel model, bool captchaValid)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.EnableWishlist) || !_shoppingCartSettings.EmailWishlistEnabled)
                return RedirectToRoute("HomePage");

            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.Wishlist)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();
            if (!cart.Any())
                return RedirectToRoute("HomePage");

            //validate CAPTCHA
            if (_captchaSettings.Enabled && _captchaSettings.ShowOnEmailWishlistToFriendPage && !captchaValid)
            {
                ModelState.AddModelError("", _captchaSettings.GetWrongCaptchaMessage(_localizationService));
            }

            //check whether the current customer is guest and ia allowed to email wishlist
            if (_workContext.CurrentCustomer.IsGuest() && !_shoppingCartSettings.AllowAnonymousUsersToEmailWishlist)
            {
                ModelState.AddModelError("", _localizationService.GetResource("Wishlist.EmailAFriend.OnlyRegisteredUsers"));
            }

            if (ModelState.IsValid)
            {
                //email
                _workflowMessageService.SendWishlistEmailAFriendMessage(_workContext.CurrentCustomer,
                        _workContext.WorkingLanguage.Id, model.YourEmailAddress,
                        model.FriendEmail, Core.Html.HtmlHelper.FormatText(model.PersonalMessage, false, true, false, false, false, false));

                model.SuccessfullySent = true;
                model.Result = _localizationService.GetResource("Wishlist.EmailAFriend.SuccessfullySent");

                return View(model);
            }

            //If we got this far, something failed, redisplay form
            model.DisplayCaptcha = _captchaSettings.Enabled && _captchaSettings.ShowOnEmailWishlistToFriendPage;
            return View(model);
        }

        #endregion

        #region Egg Shopping Cart
        public ActionResult EggOrderSummary(bool? prepareAndDisplayOrderReviewData)
        {
            var model = new EggOrderSummaryModel();

            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();

            // prepair shopping cart model
            var shoppingCartModel = new ShoppingCartModel();
            PrepareShoppingCartModel(shoppingCartModel, cart,
                isEditable: false,
                prepareEstimateShippingIfEnabled: false,
                prepareAndDisplayOrderReviewData: prepareAndDisplayOrderReviewData.GetValueOrDefault());
            model.ShoppingCartModel = shoppingCartModel;
            model.ShoppingCartModel.DiscountBox.IsIncludedTrackingCode = true;
            //default address and shipping option
            Address address = _workContext.CurrentCustomer.Addresses.FirstOrDefault();
            if (address == null)
            {
                address = new Address
                {
                    CountryId = 1,
                    Country = _countryService.GetCountryById(1),
                    StateProvinceId = null,
                    StateProvince = null,
                    ZipPostalCode = null,
                    CreatedOnUtc = DateTime.UtcNow
                };
            }

            _workContext.CurrentCustomer.ShippingAddress = address;
            _workContext.CurrentCustomer.BillingAddress = address;
            _customerService.UpdateCustomer(_workContext.CurrentCustomer);

            GetShippingOptionResponse getShippingOptionResponse = _shippingService
                .GetShippingOptions(cart, address, "", _storeContext.CurrentStore.Id);
            if (getShippingOptionResponse.ShippingOptions.Count > 0)
                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedShippingOption, getShippingOptionResponse.ShippingOptions[0], _storeContext.CurrentStore.Id);


            // prepair total order
            var orderTotalsModel = new OrderTotalsModel();
            orderTotalsModel = PrepareOrderTotalsModel(cart, false);
            model.OrderTotalModel = orderTotalsModel;
            return PartialView(model);
        }
        public ActionResult EggOrderSummaryWithoutTrackingData(bool? prepareAndDisplayOrderReviewData)
        {
            var model = new EggOrderSummaryModel();

            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();

            // prepair shopping cart model
            var shoppingCartModel = new ShoppingCartModel();
            PrepareShoppingCartModel(shoppingCartModel, cart,
                isEditable: false,
                prepareEstimateShippingIfEnabled: false,
                prepareAndDisplayOrderReviewData: prepareAndDisplayOrderReviewData.GetValueOrDefault());
            model.ShoppingCartModel = shoppingCartModel;
            model.ShoppingCartModel.DiscountBox.IsIncludedTrackingCode = false;
            //default address and shipping option
            Address address = _workContext.CurrentCustomer.Addresses.FirstOrDefault();
            if (address == null)
            {
                address = new Address
                {
                    CountryId = 1,
                    Country = _countryService.GetCountryById(1),
                    StateProvinceId = null,
                    StateProvince = null,
                    ZipPostalCode = null,
                    CreatedOnUtc = DateTime.UtcNow
                };
            }

            _workContext.CurrentCustomer.ShippingAddress = address;
            _workContext.CurrentCustomer.BillingAddress = address;
            _customerService.UpdateCustomer(_workContext.CurrentCustomer);

            GetShippingOptionResponse getShippingOptionResponse = _shippingService
                .GetShippingOptions(cart, address, "", _storeContext.CurrentStore.Id);
            if (getShippingOptionResponse.ShippingOptions.Count > 0)
                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedShippingOption, getShippingOptionResponse.ShippingOptions[0], _storeContext.CurrentStore.Id);


            // prepair total order
            var orderTotalsModel = new OrderTotalsModel();
            orderTotalsModel = PrepareOrderTotalsModel(cart, false);
            model.OrderTotalModel = orderTotalsModel;

            return PartialView(model);
        }

        #region Egg section
        [ValidateInput(false)]
        public ActionResult EggChangeCountry(FormCollection form)
        {
            try
            {
                int countryId = Int32.Parse(form["countryId"]);
                var country = _countryService.GetCountryById(countryId);
                if (country != null)
                {
                    var address = new Address
                    {
                        CountryId = countryId,
                        Country = country,
                        StateProvinceId = null,
                        StateProvince = null,
                        ZipPostalCode = null,
                        CreatedOnUtc = DateTime.UtcNow
                    };
                    _workContext.CurrentCustomer.ShippingAddress = address;
                    _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                }

                var cart = _workContext.CurrentCustomer.ShoppingCartItems
               .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
               .LimitPerStore(_storeContext.CurrentStore.Id)
               .ToList();

                GetShippingOptionResponse getShippingOptionResponse = _shippingService
                .GetShippingOptions(cart, _workContext.CurrentCustomer.ShippingAddress, "", _storeContext.CurrentStore.Id);
                if (getShippingOptionResponse.ShippingOptions.Count > 0)
                    _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedShippingOption, getShippingOptionResponse.ShippingOptions[0], _storeContext.CurrentStore.Id);


                // prepair total order
                var orderTotalsModel = new OrderTotalsModel();
                orderTotalsModel = PrepareOrderTotalsModel(cart, false);

                return Json(new
                {
                    update_section = new
                    {
                        shipping = orderTotalsModel.Shipping,
                        total = orderTotalsModel.OrderTotal
                    }
                });
            }
            catch (Exception exc)
            {
                return Json(new { error = 1, message = exc.Message });
            }
        }

        [ValidateInput(false)]
        public ActionResult EggSaveShippingMethod(FormCollection form)
        {
            try
            {
                string shippingMethodId = form["shippingmethod"];

                // save shipping method to customer attribuite
                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedShippingMethod, shippingMethodId);

                var cart = _workContext.CurrentCustomer.ShoppingCartItems
               .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
               .LimitPerStore(_storeContext.CurrentStore.Id)
               .ToList();

                GetShippingOptionResponse getShippingOptionResponse = _shippingService
                .GetShippingOptions(cart, _workContext.CurrentCustomer.ShippingAddress, "", _storeContext.CurrentStore.Id);
                if (getShippingOptionResponse.ShippingOptions.Count > 0)
                    _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedShippingOption, getShippingOptionResponse.ShippingOptions[0], _storeContext.CurrentStore.Id);


                // prepair total order
                var orderTotalsModel = new OrderTotalsModel();
                orderTotalsModel = PrepareOrderTotalsModel(cart, false);

                return Json(new
                {
                    update_section = new
                    {
                        shipping = orderTotalsModel.Shipping,
                        total = orderTotalsModel.OrderTotal
                    }
                });
            }
            catch(Exception exc)
            {
                return Json(new { error = 1, message = exc.Message });
            }


        }

        [ValidateInput(false)]
        public ActionResult YourCart(string cartcode)
        {
            try
            {
                // check and apply coupon
                foreach (String key in Request.QueryString.AllKeys)
                {
                    if (key.Equals(EggCommon.ApplyCouponQueryStringKey))
                    {
                        bool isAppliedCoupon = false;
                        Discount appliedDiscount = null;

                        var couponCode = Request.QueryString[key];
                        this.OnApplyDiscountCouponByQueryString(couponCode, out isAppliedCoupon, out appliedDiscount);
                    }
                }

                var oldCart = _shoppingCartService.GetCustomerShoppingCart(cartcode);

                if (oldCart != null)
                {
                    var cart = _shoppingCartService.RiviveCart(cart: oldCart.ToList(), customer: _workContext.CurrentCustomer, storeId: _storeContext.CurrentStore.Id);
                    Response.Redirect("/checkout");
                }
                else
                    return RedirectToAction("Cart");
            }
            catch (Exception ex)
            {
                return RedirectToAction("Cart");
            }
            return null;
        }

        [ValidateInput(false)]
        public ActionResult RestoreCustomerShoppingCart(int customerId)
        {
            try
            {
                if (customerId != _workContext.CurrentCustomer.Id)
                {
                    var oldCart = _shoppingCartService.GetShoppingCartItemsByCustomerId(customerId);

                    if (oldCart != null)
                    {
                        var cart = _shoppingCartService.RiviveCart(cart: oldCart.ToList(), customer: _workContext.CurrentCustomer, storeId: _storeContext.CurrentStore.Id);
                    }
                }

                // check and apply coupon
                foreach (String key in Request.QueryString.AllKeys)
                {
                    if (key.Equals(EggCommon.ApplyCouponQueryStringKey))
                    {
                        bool isAppliedCoupon = false;
                        Discount appliedDiscount = null;

                        var couponCode = Request.QueryString[key];
                        this.OnApplyDiscountCouponByQueryString(couponCode, out isAppliedCoupon, out appliedDiscount);
                    }
                }

                return RedirectToAction("Cart");

            }
            catch (Exception ex)
            {
                _logger.Error("RestoreCustomerCart Error", ex, _workContext.CurrentCustomer);

                return RedirectToAction("Cart");
            }
        }

        protected virtual void OnApplyDiscountCouponByQueryString(string discountcouponcode, out bool isApplied, out Discount appliedDiscount)
        {
            isApplied = false;
            appliedDiscount = null;
            //trim
            if (discountcouponcode != null)
                discountcouponcode = discountcouponcode.Trim();
            if (!String.IsNullOrWhiteSpace(discountcouponcode))
            {
                //we find even hidden records here. this way we can display a user-friendly message if it's expired
                var discount = _discountService.GetDiscountByCouponCode(discountcouponcode, true);
                if (discount != null && discount.RequiresCouponCode)
                {
                    var validationResult = _discountService.ValidateDiscount(discount, _workContext.CurrentCustomer, discountcouponcode);
                    if (validationResult.IsValid)
                    {
                        var couponCodeToValidate = _workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.DiscountCouponCode, _genericAttributeService);
                        if (!string.IsNullOrEmpty(couponCodeToValidate))
                        {
                            if (!couponCodeToValidate.Equals(discountcouponcode))
                            {
                                //valid
                                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, SystemCustomerAttributeNames.DiscountCouponCode, discountcouponcode);
                            }
                        }
                        else
                            //valid
                            _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, SystemCustomerAttributeNames.DiscountCouponCode, discountcouponcode);

                        isApplied = true;
                    }
                    appliedDiscount = discount;
                }
            }
        }
        #endregion


        protected virtual IList<Product> GetRelatedProductWithHightestQuantityInCart(IList<ShoppingCartItem> cart)
        {
            try
            {
                var cartItemsGroupbyName = cart.GroupBy(p => p.Product.ProductCategories.Select(i => i.Category.Id).FirstOrDefault())
                    .Select(p => new CartGroupbyCategoryModel
                    {
                        CategoryId = p.Key,
                        ProductIds = p.Select(item => item.ProductId).ToList(),
                        Quantity = p.Select(item => item.Quantity).FirstOrDefault()
                    }).ToList();
                int hightestQuantity = cartItemsGroupbyName.Max(p => p.Quantity);
                var itemWithHightestQuantity = cartItemsGroupbyName.Where(p => p.Quantity == hightestQuantity).FirstOrDefault();
                var relatedProducts = _productService.GetRelatedProductsForCart(itemWithHightestQuantity.CategoryId, itemWithHightestQuantity.ProductIds);
                return relatedProducts;
            } catch
            {
                return new List<Product>();
            }
        }
        [NonAction]
        protected virtual IEnumerable<ProductOverviewModel> PrepareProductOverviewModels(IEnumerable<Product> products,
          bool preparePriceModel = true, bool preparePictureModel = true,
          int? productThumbPictureSize = null, bool prepareSpecificationAttributes = false,
          bool forceRedirectionAfterAddingToCart = false)
        {
            return this.PrepareProductOverviewModels(_workContext,
                _storeContext, _categoryService, _productService, _specificationAttributeService,
                _priceCalculationService, _priceFormatter, _permissionService,
                _localizationService, _taxService, _currencyService,
                _pictureService, _measureService, _webHelper, _cacheManager,
                _catalogSettings, _mediaSettings, products,
                preparePriceModel, preparePictureModel,
                productThumbPictureSize, prepareSpecificationAttributes,
                forceRedirectionAfterAddingToCart);

        }

        [ValidateInput(false)]
        public JsonResult RequestChangeItemUpSell(List<ItemUpSellRequest> request)
        {
            try
            {
                List<ItemUpSellJsonResult> result = new List<ItemUpSellJsonResult>();
                Customer tmpCustomer = _workContext.CurrentCustomer;

                //generate temp shopping cart item to calculate discount 
                foreach (var item in request)
                {
                    Product product = _productService.GetProductById(item.ProductId);

                    if (product != null)
                    {
                        var tempUpsellItem = new ItemUpSellJsonResult();
                        StyleColorJsonResult styleColorResult = JsonConvert.DeserializeObject<StyleColorJsonResult>(item.ItemStyleColor);
                        var ppamAvailbleProduct = product.ProductAttributeMappings.Where(ppam => ppam.ProductAttributeId == 1).FirstOrDefault();

                        // find ppam Size of Type
                        ProductAttributeMapping ppamSize = null;
                        foreach (var ppam in product.ProductAttributeMappings.Where(ppam => !string.IsNullOrEmpty(ppam.ConditionAttributeXml) && ppam.ProductAttribute.Name.Contains("size")).Select(ppam => ppam))
                        {
                            var tmp = _productAttributeParser.ParseConditionValue(ppam.ConditionAttributeXml, ppamAvailbleProduct.Id, styleColorResult.TypeId);
                            if (tmp.Count > 0)
                            {
                                ppamSize = ppam;
                                break;
                            }
                        }
                        // calculate price after request
                        var ppavStyle = ppamAvailbleProduct.ProductAttributeValues.Where(ppav => ppav.Id == styleColorResult.TypeId).FirstOrDefault();

                        ProductAttributeValue ppavSize = null;
                        if (!string.IsNullOrEmpty(item.Size))
                        {
                            var tempsize = item.Size.IndexOf("+") > 0 ? item.Size.Substring(0, item.Size.IndexOf("+")).Trim() : item.Size;
                            ppavSize = ppamSize.ProductAttributeValues.Where(ppav => ppav.Name.ToLower().Equals(tempsize.ToLower())).FirstOrDefault();

                            //priceSubTotal = priceSubTotal * request.Quantity;
                            // need tmp ShoppingCartItem to reuse some function
                            ShoppingCartItem tmpShoppingCartItem = new ShoppingCartItem();
                            tmpShoppingCartItem.ShoppingCartType = ShoppingCartType.ShoppingCart;
                            tmpShoppingCartItem.Product = product;
                            tmpShoppingCartItem.AttributesXml = this.GenerateTempAttributeXml(item.ProductId, styleColorResult.ColorId, ppavSize != null ? ppavSize.Id : 0, styleColorResult.TypeId);
                            tmpShoppingCartItem.Quantity = item.Quantity;
                            tmpShoppingCartItem.StoreId = _storeContext.CurrentStore.Id;
                            tmpShoppingCartItem.Customer = tmpCustomer;
                            int maxId = tmpCustomer.ShoppingCartItems.Any() ? tmpCustomer.ShoppingCartItems.Max(p => p.Id) : 0;
                            tmpShoppingCartItem.Id = maxId + 1;
                            tmpCustomer.ShoppingCartItems.Add(tmpShoppingCartItem);
                            item.TempCartItemId = tmpShoppingCartItem.Id;
                        }
                    }
                }


                //select new item
                var newShoppingCartItems = tmpCustomer.ShoppingCartItems.Where(p => request.Select(r=>r.TempCartItemId).Contains(p.Id)).ToList();

                foreach(var item in newShoppingCartItems)
                {
                    foreach (var data in request)
                    {
                        if(item.Id== data.TempCartItemId)
                        {
                            var tempUpsellItem = new ItemUpSellJsonResult();
                            StyleColorJsonResult styleColorResult = JsonConvert.DeserializeObject<StyleColorJsonResult>(data.ItemStyleColor);
                            var ppamAvailbleProduct = item.Product.ProductAttributeMappings.Where(ppam => ppam.ProductAttributeId == 1).FirstOrDefault();

                            // find ppam Size of Type
                            ProductAttributeMapping ppamSize = null;
                            foreach (var ppam in item.Product.ProductAttributeMappings.Where(ppam => !string.IsNullOrEmpty(ppam.ConditionAttributeXml) && ppam.ProductAttribute.Name.Contains("size")).Select(ppam => ppam))
                            {
                                var tmp = _productAttributeParser.ParseConditionValue(ppam.ConditionAttributeXml, ppamAvailbleProduct.Id, styleColorResult.TypeId);
                                if (tmp.Count > 0)
                                {
                                    ppamSize = ppam;
                                    break;
                                }
                            }
                            // get all sizes
                            tempUpsellItem.Sizes = new List<string>();
                            foreach(var size in ppamSize.ProductAttributeValues)
                            {
                                var sizeWithPrice = size.Name;
                                if(size.PriceAdjustment>decimal.Zero)
                                    sizeWithPrice= string.Format("{0} + {1}", size.Name, _priceFormatter.FormatPrice(_currencyService.ConvertFromPrimaryStoreCurrency(size.PriceAdjustment, _workContext.WorkingCurrency)));
                                tempUpsellItem.Sizes.Add(sizeWithPrice);
                            }
                            // calculate price after request
                            var ppavStyle = ppamAvailbleProduct.ProductAttributeValues.Where(ppav => ppav.Id == styleColorResult.TypeId).FirstOrDefault();

                            ProductAttributeValue ppavSize = null;
                            if (!string.IsNullOrEmpty(data.Size))
                            {
                                var tempsize = data.Size.IndexOf("+") > 0 ? data.Size.Substring(0, data.Size.IndexOf("+")).Trim() : data.Size;

                                ppavSize = ppamSize.ProductAttributeValues.Where(ppav => ppav.Name.ToLower().Equals(tempsize.ToLower())).FirstOrDefault();
                                if (ppavSize != null)
                                    tempUpsellItem.NewSize = ppavSize.PriceAdjustment > decimal.Zero ? string.Format("{0} + {1}", ppavSize.Name, _priceFormatter.FormatPrice(_currencyService.ConvertFromPrimaryStoreCurrency(ppavSize.PriceAdjustment, _workContext.WorkingCurrency))) : ppavSize.Name;
                                else
                                    tempUpsellItem.NewSize = string.Empty;
                            }
                            else
                                tempUpsellItem.NewSize = string.Empty;

                            string subTotal = "";
                            //subtotal, discount
                            List<Discount> scDiscounts;
                            decimal shoppingCartItemDiscountBase;
                            decimal taxRate;
                            decimal shoppingCartItemSubTotalWithDiscountBase = _taxService.GetProductPrice(item.Product, _priceCalculationService.GetSubTotal(item, true, out shoppingCartItemDiscountBase, out scDiscounts), tmpCustomer, out taxRate);
                            decimal shoppingCartItemSubTotalWithDiscount = _currencyService.ConvertFromPrimaryStoreCurrency(shoppingCartItemSubTotalWithDiscountBase, _workContext.WorkingCurrency);
                            subTotal = _priceFormatter.FormatPrice(shoppingCartItemSubTotalWithDiscount);

                            tempUpsellItem.SubTotalPrice = subTotal;

                            // result.SubTotalPrice = _priceFormatter.FormatPrice(priceSubTotal);

                            // find color picture
                            ProductAttributeMapping ppamColor = null;
                            foreach (var ppam in item.Product.ProductAttributeMappings.Where(ppam => !string.IsNullOrEmpty(ppam.ConditionAttributeXml) && ppam.ProductAttribute.Name.Contains(EggCommon.PrefixColorAttributeValue)).Select(ppam => ppam))
                            {
                                var tmp = _productAttributeParser.ParseConditionValue(ppam.ConditionAttributeXml, ppamAvailbleProduct.Id, styleColorResult.TypeId);
                                if (tmp.Count > 0)
                                {
                                    ppamColor = ppam;
                                    break;
                                }
                            }
                            var ppavColor = ppamColor.ProductAttributeValues.Where(ppav => ppav.Id == styleColorResult.ColorId).FirstOrDefault();
                            var picture = _pictureService.GetPictureById(ppavColor.PictureId);
                            if (picture != null)
                            {
                                picture.IsNew = false;
                                tempUpsellItem.PictureUrl = _pictureService.GetPictureUrl(picture, 40, true);
                            }
                            tempUpsellItem.Id = data.Id;
                            result.Add(tempUpsellItem);

                        }
                    }

                }
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex, JsonRequestBehavior.AllowGet);
            }
            
        }

        [NonAction]
        protected virtual string GenerateTempAttributeXml(int productId,int colorId, int sizeId, int typeId)
        {
            try
            {
                var product = _productService.GetProductById(productId);
                if (product == null)
                    return null;

                var productAttributeMappings = product.ProductAttributeMappings;
                var color = _productAttributeService.GetProductAttributeValueById(colorId);
                var type = _productAttributeService.GetProductAttributeValueById(typeId);
                var size = _productAttributeService.GetProductAttributeValueById(sizeId);
                string attributesXml = "";
                if (type!=null)
                    attributesXml = _productAttributeParser.AddProductAttribute(attributesXml, type.ProductAttributeMapping, type.Id.ToString());

                if (size!=null)
                    attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,size.ProductAttributeMapping, size.Id.ToString());

                if (color != null)
                    attributesXml = _productAttributeParser.AddProductAttribute(attributesXml, color.ProductAttributeMapping, color.Id.ToString());

                return attributesXml;
            }
            catch (Exception ex)
            {
                _logger.Error(string.Format("generate xml attribute error: {0}", ex.Message), ex);
            }
            return null;
        }

        public ActionResult ApplyDiscountCouponAjax(string discountcouponcodecheckout, FormCollection form, bool isincludedtrackingcode = false)
        {
            try
            {
                var model = new EggOrderSummaryModel();

                var cart = _workContext.CurrentCustomer.ShoppingCartItems
                    .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                    .LimitPerStore(_storeContext.CurrentStore.Id)
                    .ToList();

                // prepair shopping cart model
                model.ShoppingCartModel = OnApplyDiscountCoupon(discountcouponcodecheckout, form);
                model.ShoppingCartModel.DiscountBox.IsIncludedTrackingCode = isincludedtrackingcode;
                //default address and shipping option
                Address address = _workContext.CurrentCustomer.Addresses.FirstOrDefault();
                if (address == null)
                {
                    address = new Address
                    {
                        CountryId = 1,
                        Country = _countryService.GetCountryById(1),
                        StateProvinceId = null,
                        StateProvince = null,
                        ZipPostalCode = null,
                        CreatedOnUtc = DateTime.UtcNow
                    };
                }

                _workContext.CurrentCustomer.ShippingAddress = address;
                _workContext.CurrentCustomer.BillingAddress = address;
                _customerService.UpdateCustomer(_workContext.CurrentCustomer);

                GetShippingOptionResponse getShippingOptionResponse = _shippingService
                    .GetShippingOptions(cart, address, "", _storeContext.CurrentStore.Id);
                if (getShippingOptionResponse.ShippingOptions.Count > 0)
                    _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedShippingOption, getShippingOptionResponse.ShippingOptions[0], _storeContext.CurrentStore.Id);


                // prepair total order
                var orderTotalsModel = new OrderTotalsModel();
                orderTotalsModel = PrepareOrderTotalsModel(cart, false);
                model.OrderTotalModel = orderTotalsModel;

                if (isincludedtrackingcode)
                    return View("~/Themes/EggStore/Views/ShoppingCart/EggOrderSummary.cshtml", model);
                else
                    return View("~/Themes/EggStore/Views/ShoppingCart/EggOrderSummaryWithoutTrackingData.cshtml", model);

            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public ActionResult RemoveDiscountCouponAjax(string discountcouponcodecheckout, FormCollection form, bool isincludedtrackingcode = false)
        {
            try
            {
                _genericAttributeService.SaveAttribute<string>(_workContext.CurrentCustomer,
                    SystemCustomerAttributeNames.DiscountCouponCode, null);

                if (isincludedtrackingcode)
                    return RedirectToAction("EggOrderSummary");
                else
                    return RedirectToAction("EggOrderSummaryWithoutTrackingData");

            }
            catch (Exception ex)
            {
            }
            return null;
        }
        #endregion
        #region Egg Subscribe Customer
        public ActionResult SubscribeCustomer(FormCollection form)
        {
            try
            {
                SubscribeCustomer scCustomer = new SubscribeCustomer();
                scCustomer.CustomerId = _workContext.CurrentCustomer.Id;
                scCustomer.ProductId = Int32.Parse(form["productId"]);
                scCustomer.UserRef = form["userRef"];
                scCustomer.Token = "EAAHmXmN5NBABADw0wnHC8uwilSzbHWuykhQ7ZBlBc0fmGiQGIdFxjhRi5hMyFyMZCyVZB4akaBfAYcsWKgUGzmd6H98NUpvdM0wZBhxdZA0zxnhrw8MtvZBlyAoBjsq8L7ZCIjSvcZA5kNrGA4HLb1YUIiZCLpHmNW56wugKcJ1RWE3zyfYqQLmJQ";

                var exitSubscribeCustomer = _subscribeCustomerService.GetByCustomer(_workContext.CurrentCustomer.Id);
                if (exitSubscribeCustomer != null)
                {
                    exitSubscribeCustomer.ProductId = scCustomer.ProductId;
                    exitSubscribeCustomer.UserRef = scCustomer.UserRef;
                    exitSubscribeCustomer.Token = scCustomer.Token;
                    _subscribeCustomerService.UpdateSubscribeCustomer(exitSubscribeCustomer);
                }
                else
                {
                    scCustomer = _subscribeCustomerService.InsertSubscribeCustomer(scCustomer);

                    // add first message subscribe
                    _subscribeMessageService.RemindViewProductFirstMessage(scCustomer);

                    _subscribeActivityLogService.InsertSubscribeActivityLogViewProduct(scCustomer.ProductId.Value, string.Empty);
                }
                _logger.Information("Subscribe Customer Success", null, _workContext.CurrentCustomer);
            }
            catch (Exception ex)
            {
                _logger.Error("Subscribe Customer Error", ex, _workContext.CurrentCustomer);
            }

            return null;
        }
        #endregion
    }
}
