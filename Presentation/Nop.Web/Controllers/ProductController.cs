﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Seo;
using Nop.Core.Domain.Vendors;
using Nop.Services.Catalog;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Events;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Shipping;
using Nop.Services.Stores;
using Nop.Services.Tax;
using Nop.Services.Vendors;
using Nop.Web.Extensions;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Security;
using Nop.Web.Framework.Security.Captcha;
using Nop.Web.Infrastructure.Cache;
using Nop.Web.Models.Catalog;
using Nop.Web.Models.Common;
using Nop.Web.Models.Media;
using Nop.Services.Common;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Shipping;
using System.Web;
using Nop.Core.Domain.Customs;
using static Nop.Web.Models.Catalog.ProductDetailsModel;
using Nop.Web.Framework.Mvc;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Nop.Services.Discounts;
using Nop.Core.Domain.Discounts;

namespace Nop.Web.Controllers
{
    public partial class ProductController : BasePublicController
    {
        #region Fields

        private readonly ICategoryService _categoryService;
        private readonly IManufacturerService _manufacturerService;
        private readonly IProductService _productService;
        private readonly IVendorService _vendorService;
        private readonly IProductTemplateService _productTemplateService;
        private readonly IProductAttributeService _productAttributeService;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly ITaxService _taxService;
        private readonly ICurrencyService _currencyService;
        private readonly IPictureService _pictureService;
        private readonly ILocalizationService _localizationService;
        private readonly IMeasureService _measureService;
        private readonly IPriceCalculationService _priceCalculationService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IWebHelper _webHelper;
        private readonly ISpecificationAttributeService _specificationAttributeService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IRecentlyViewedProductsService _recentlyViewedProductsService;
        private readonly ICompareProductsService _compareProductsService;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly IProductTagService _productTagService;
        private readonly IOrderReportService _orderReportService;
        private readonly IAclService _aclService;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IPermissionService _permissionService;
        private readonly IDownloadService _downloadService;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly IProductAttributeParser _productAttributeParser;
        private readonly IShippingService _shippingService;
        private readonly IEventPublisher _eventPublisher;
        private readonly MediaSettings _mediaSettings;
        private readonly CatalogSettings _catalogSettings;
        private readonly VendorSettings _vendorSettings;
        private readonly ShoppingCartSettings _shoppingCartSettings;
        private readonly LocalizationSettings _localizationSettings;
        private readonly CustomerSettings _customerSettings;
        private readonly CaptchaSettings _captchaSettings;
        private readonly SeoSettings _seoSettings;
        private readonly ICacheManager _cacheManager;
        private readonly IReviewService _reviewService;
        private readonly ShippingSettings _shippingSettings;
        private readonly IRenderPictureService _renderPictureService;
        private readonly IAdvancedDrawingPictureService _advancedDrawingPictureService;
        private readonly IProductCustomFieldService _productCustomFieldService;
        private readonly IDiscountService _discountService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ISubscribeActivityLogService _subscribeActivityLogService;
        private readonly IProductTypeService _productTypeService;
        #endregion

        #region Constructors

        public ProductController(ICategoryService categoryService,
            IManufacturerService manufacturerService,
            IProductService productService,
            IVendorService vendorService,
            IProductTemplateService productTemplateService,
            IProductAttributeService productAttributeService,
            IWorkContext workContext,
            IStoreContext storeContext,
            ITaxService taxService,
            ICurrencyService currencyService,
            IPictureService pictureService,
            ILocalizationService localizationService,
            IMeasureService measureService,
            IPriceCalculationService priceCalculationService,
            IPriceFormatter priceFormatter,
            IWebHelper webHelper,
            ISpecificationAttributeService specificationAttributeService,
            IDateTimeHelper dateTimeHelper,
            IRecentlyViewedProductsService recentlyViewedProductsService,
            ICompareProductsService compareProductsService,
            IWorkflowMessageService workflowMessageService,
            IProductTagService productTagService,
            IOrderReportService orderReportService,
            IAclService aclService,
            IStoreMappingService storeMappingService,
            IPermissionService permissionService,
            IDownloadService downloadService,
            ICustomerActivityService customerActivityService,
            IProductAttributeParser productAttributeParser,
            IShippingService shippingService,
            IEventPublisher eventPublisher,
            MediaSettings mediaSettings,
            CatalogSettings catalogSettings,
            VendorSettings vendorSettings,
            ShoppingCartSettings shoppingCartSettings,
            LocalizationSettings localizationSettings,
            CustomerSettings customerSettings,
            CaptchaSettings captchaSettings,
            ShippingSettings shippingSettings,
            SeoSettings seoSettings,
            ICacheManager cacheManager,
            IReviewService reviewService,
            IRenderPictureService renderPictureService,
            IAdvancedDrawingPictureService advancedDrawingPictureService,
            IProductCustomFieldService productCustomFieldService,
            IDiscountService discountService,
            IGenericAttributeService genericAttributeService,
            ISubscribeActivityLogService subscribeActivityLogService, 
            IProductTypeService productTypeService)
        {
            this._categoryService = categoryService;
            this._manufacturerService = manufacturerService;
            this._productService = productService;
            this._vendorService = vendorService;
            this._productTemplateService = productTemplateService;
            this._productAttributeService = productAttributeService;
            this._workContext = workContext;
            this._storeContext = storeContext;
            this._taxService = taxService;
            this._currencyService = currencyService;
            this._pictureService = pictureService;
            this._localizationService = localizationService;
            this._measureService = measureService;
            this._priceCalculationService = priceCalculationService;
            this._priceFormatter = priceFormatter;
            this._webHelper = webHelper;
            this._specificationAttributeService = specificationAttributeService;
            this._dateTimeHelper = dateTimeHelper;
            this._recentlyViewedProductsService = recentlyViewedProductsService;
            this._compareProductsService = compareProductsService;
            this._workflowMessageService = workflowMessageService;
            this._productTagService = productTagService;
            this._orderReportService = orderReportService;
            this._aclService = aclService;
            this._storeMappingService = storeMappingService;
            this._permissionService = permissionService;
            this._downloadService = downloadService;
            this._customerActivityService = customerActivityService;
            this._productAttributeParser = productAttributeParser;
            this._shippingService = shippingService;
            this._eventPublisher = eventPublisher;
            this._mediaSettings = mediaSettings;
            this._catalogSettings = catalogSettings;
            this._vendorSettings = vendorSettings;
            this._shoppingCartSettings = shoppingCartSettings;
            this._localizationSettings = localizationSettings;
            this._customerSettings = customerSettings;
            this._shippingSettings = shippingSettings;
            this._captchaSettings = captchaSettings;
            this._seoSettings = seoSettings;
            this._cacheManager = cacheManager;
            this._reviewService = reviewService;
            this._renderPictureService = renderPictureService;
            this._advancedDrawingPictureService = advancedDrawingPictureService;
            this._productCustomFieldService = productCustomFieldService;
            this._discountService = discountService;
            this._genericAttributeService = genericAttributeService;
            this._subscribeActivityLogService = subscribeActivityLogService;
            this._productTypeService = productTypeService;
        }

        #endregion

        #region Utilities

        [NonAction]
        protected virtual IEnumerable<ProductOverviewModel> PrepareProductOverviewModels(IEnumerable<Product> products,
            bool preparePriceModel = true, bool preparePictureModel = true,
            int? productThumbPictureSize = null, bool prepareSpecificationAttributes = false,
            bool forceRedirectionAfterAddingToCart = false)
        {
            return this.PrepareProductOverviewModels(_workContext,
                _storeContext, _categoryService, _productService, _specificationAttributeService,
                _priceCalculationService, _priceFormatter, _permissionService,
                _localizationService, _taxService, _currencyService,
                _pictureService, _measureService, _webHelper, _cacheManager,
                _catalogSettings, _mediaSettings, products,
                preparePriceModel, preparePictureModel,
                productThumbPictureSize, prepareSpecificationAttributes,
                forceRedirectionAfterAddingToCart);
        }

        protected virtual string GetSellOffExpiresDay()
        {
            try
            {
                HttpCookie cookie = Request.Cookies["SellOffExpiresDayCookie"];
                if (cookie != null)
                {
                    string sellOffExpiresDayCookie = Server.HtmlEncode(cookie.Value);
                    if (string.IsNullOrEmpty(sellOffExpiresDayCookie))
                    {
                        Response.Cookies["SellOffExpiresDayCookie"].Value = DateTime.UtcNow.AddDays(1).ToString("yyyy/MM/dd HH:mm:ss");
                    }
                    else
                    {
                        DateTime sellOffExpiresDay = DateTime.Parse(sellOffExpiresDayCookie, new CultureInfo("en-CA"));
                        if (sellOffExpiresDay < DateTime.Now)
                        {
                            Response.Cookies["SellOffExpiresDayCookie"].Value = DateTime.UtcNow.AddDays(1).ToString("yyyy/MM/dd HH:mm:ss");
                        }
                        else
                        {
                        }
                    }
                }
                else
                {
                    cookie = new HttpCookie("SellOffExpiresDayCookie");
                    Response.Cookies["SellOffExpiresDayCookie"].Value = DateTime.UtcNow.AddDays(1).ToString("yyyy/MM/dd HH:mm:ss");
                }
                string result = Server.HtmlEncode(Request.Cookies["SellOffExpiresDayCookie"].Value);
                return result;
            }
            catch(Exception ex)
            { }
            return string.Empty;
        }

        [NonAction]
        protected virtual ProductDetailsModel PrepareProductDetailsPageModel(Product product,
            ShoppingCartItem updatecartitem = null, bool isAssociatedProduct = false)
        {
            if (product == null)
                throw new ArgumentNullException("product");

            #region Standard properties

            var model = new ProductDetailsModel
            {
                Id = product.Id,
                Name = product.GetLocalized(x => x.Name),
                ShortDescription = product.GetLocalized(x => x.ShortDescription),
                FullDescription = product.GetLocalized(x => x.FullDescription),
                MetaKeywords = product.GetLocalized(x => x.MetaKeywords),
                MetaDescription = product.GetLocalized(x => x.MetaDescription),
                MetaTitle = product.GetLocalized(x => x.MetaTitle),
                SeName = product.GetSeName(),
                ProductType = product.ProductType,
                ShowSku = _catalogSettings.ShowProductSku,
                Sku = product.Sku,
                ShowManufacturerPartNumber = _catalogSettings.ShowManufacturerPartNumber,
                FreeShippingNotificationEnabled = _catalogSettings.ShowFreeShippingNotification,
                ManufacturerPartNumber = product.ManufacturerPartNumber,
                ShowGtin = _catalogSettings.ShowGtin,
                Gtin = product.Gtin,
                StockAvailability = product.FormatStockMessage("", _localizationService, _productAttributeParser),
                HasSampleDownload = product.IsDownload && product.HasSampleDownload,
                DisplayDiscontinuedMessage = !product.Published && _catalogSettings.DisplayDiscontinuedMessageForUnpublishedProducts
            };

            //automatically generate product description?
            if (_seoSettings.GenerateProductMetaDescription && String.IsNullOrEmpty(model.MetaDescription))
            {
                //based on short description
                model.MetaDescription = model.ShortDescription;
            }

            //shipping info
            model.IsShipEnabled = product.IsShipEnabled;
            if (product.IsShipEnabled)
            {
                model.IsFreeShipping = product.IsFreeShipping;
                //delivery date
                var deliveryDate = _shippingService.GetDeliveryDateById(product.DeliveryDateId);
                if (deliveryDate != null)
                {
                    model.DeliveryDate = deliveryDate.GetLocalized(dd => dd.Name);
                }
            }

            //email a friend
            model.EmailAFriendEnabled = _catalogSettings.EmailAFriendEnabled;
            //compare products
            model.CompareProductsEnabled = _catalogSettings.CompareProductsEnabled;
            //store name
            model.CurrentStoreName = _storeContext.CurrentStore.GetLocalized(x => x.Name);

            //limited in stock quantity
            model.IsLimitedInStockEnable = product.IsLimitedInStockEnable;
           
            if (model.IsLimitedInStockEnable)
            {
                int _totalInStockQuantity = product.LimitedInStockQuantity.HasValue ? product.LimitedInStockQuantity.Value : 0;
                var _limitedStartDate = product.LimitedInStockStartDate;
                var _soldQuantity = product.SoldQuantity.HasValue ? product.SoldQuantity.Value : 0;
                var _inStockQuantity = _totalInStockQuantity - _soldQuantity;
                int _subtractedCount = product.SubtractedSoldTimeCount.HasValue ? product.SubtractedSoldTimeCount.Value : 0;
                ///
                if (_limitedStartDate.HasValue)
                {
                    //get diff start limit date with current date to subtract fibonacci sold item
                    var subtractDate = DateTime.UtcNow - _limitedStartDate.Value;
                    var _differenceDays = (int)Math.Round(subtractDate.TotalDays);
                    if (_differenceDays >= 1)
                    {
                        if (_subtractedCount != _differenceDays)
                        {
                            //get subtract quantity from 3
                            int _startFibonacciIndex = 4;

                            _soldQuantity += CommonHelper.CalculateNthFibonacciNumber(_startFibonacciIndex + _differenceDays);
                            _inStockQuantity = _totalInStockQuantity - _soldQuantity;

                            //get limited stock quantity more than min limit value,else refresh data 
                            if (_inStockQuantity > EggCommon.MinimumLimitedInStockQuantity)
                            {
                                product.SoldQuantity = _soldQuantity;
                                product.SubtractedSoldTimeCount = _differenceDays;
                            }
                            else
                            {
                                int subtractedQuantity = 0;
                                ////the loop to get in stock product
                                for (int day = 1; day <= _differenceDays; day++)
                                {
                                    //increase fibonacci index 
                                    subtractedQuantity += CommonHelper.CalculateNthFibonacciNumber(_startFibonacciIndex + day);
                                }
                                //get old sold quantity data
                                product.SoldQuantity = (_soldQuantity - subtractedQuantity) > 0 ? (_soldQuantity - subtractedQuantity) : 0;
                                product.LimitedInStockStartDate = DateTime.UtcNow;
                                product.SubtractedSoldTimeCount = 0;
                                _soldQuantity = product.SoldQuantity.Value;
                            }
                            _productService.UpdateProduct(product);
                        }
                        
                    }
                }
                else
                {
                    product.LimitedInStockStartDate = DateTime.UtcNow;
                    product.SubtractedSoldTimeCount = 0;
                    _productService.UpdateProduct(product);
                }
                model.ProductInStockQuantity = _totalInStockQuantity - _soldQuantity;
                model.ProductSoldQuantity = _soldQuantity;
                //get solder product percent 
                decimal instockPercent = decimal.Divide(model.ProductInStockQuantity, _totalInStockQuantity);
                model.SoldPercent = 100 - (int)Math.Round(instockPercent * 100);
            }


            #endregion

            #region Page sharing

            if (_catalogSettings.ShowShareButton && !String.IsNullOrEmpty(_catalogSettings.PageShareCode))
            {
                var shareCode = _catalogSettings.PageShareCode;
                if (_webHelper.IsCurrentConnectionSecured())
                {
                    //need to change the addthis link to be https linked when the page is, so that the page doesnt ask about mixed mode when viewed in https...
                    shareCode = shareCode.Replace("http://", "https://");
                }
                model.PageShareCode = shareCode;
            }

            #endregion

            #region Breadcrumb

            //do not prepare this model for the associated products. anyway it's not used
            var productCategories = _categoryService.GetProductCategoriesByProductId(product.Id);
            if (_catalogSettings.CategoryBreadcrumbEnabled && !isAssociatedProduct)
            {
                var breadcrumbCacheKey = string.Format(ModelCacheEventConsumer.PRODUCT_BREADCRUMB_MODEL_KEY,
                    product.Id,
                    _workContext.WorkingLanguage.Id,
                    string.Join(",", _workContext.CurrentCustomer.GetCustomerRoleIds()),
                    _storeContext.CurrentStore.Id);
                model.Breadcrumb = _cacheManager.Get(breadcrumbCacheKey, () =>
                {
                    var breadcrumbModel = new ProductDetailsModel.ProductBreadcrumbModel
                    {
                        Enabled = _catalogSettings.CategoryBreadcrumbEnabled,
                        ProductId = product.Id,
                        ProductName = product.GetLocalized(x => x.Name),
                        ProductSeName = product.GetSeName()
                    };
                    if (productCategories.Any())
                    {
                        var category = productCategories[0].Category;
                        if (category != null)
                        {
                            foreach (var catBr in category.GetCategoryBreadCrumb(_categoryService, _aclService, _storeMappingService))
                            {
                                breadcrumbModel.CategoryBreadcrumb.Add(new CategorySimpleModel
                                {
                                    Id = catBr.Id,
                                    Name = catBr.GetLocalized(x => x.Name),
                                    SeName = catBr.GetSeName(),
                                    IncludeInTopMenu = catBr.IncludeInTopMenu
                                });
                            }
                        }
                    }
                    return breadcrumbModel;
                });
            }

            #endregion

            #region Templates

            var templateCacheKey = string.Format(ModelCacheEventConsumer.PRODUCT_TEMPLATE_MODEL_KEY, product.ProductTemplateId);
            model.ProductTemplateViewPath = _cacheManager.Get(templateCacheKey, () =>
            {
                var template = _productTemplateService.GetProductTemplateById(product.ProductTemplateId);
                if (template == null)
                    template = _productTemplateService.GetAllProductTemplates().FirstOrDefault();
                if (template == null)
                    throw new Exception("No default template could be loaded");
                return template.ViewPath;
            });

            #endregion

            #region  Common Condition Value
            int queryStringCount = Request.QueryString.Count;
            ProductAttributeValueModel productAttrColorValueByQueryStringModel = null;
            ProductAttributeValueModel productAttrTypeValueByQueryStringModel = null;
            ProductAttributeValueModel productPreselectedAttrTypeValueForPriceModel = null;
            List<ProductRenderRequest> renderRequest = new List<ProductRenderRequest>();
            bool isExistAttributeQueryString = false;
            bool isExistRenderQueryString = false;
            string renderKey = EggCommon.RenderQueryStringKey;
            bool isAppliedCoupon = false;
            Discount appliedDiscount =null;
            #endregion

            #region Pictures

            model.DefaultPictureZoomEnabled = _mediaSettings.DefaultPictureZoomEnabled;
            //default picture
            var defaultPictureSize = _mediaSettings.ProductDetailsPictureSize;
            //prepare picture models
            var productPicturesCacheKey = string.Format(ModelCacheEventConsumer.PRODUCT_DETAILS_PICTURES_MODEL_KEY, product.Id, defaultPictureSize, isAssociatedProduct, _workContext.WorkingLanguage.Id, _webHelper.IsCurrentConnectionSecured(), _storeContext.CurrentStore.Id);
            var cachedPictures = _cacheManager.Get(productPicturesCacheKey, () =>
            {
                var pictures = _pictureService.GetPicturesByProductId(product.Id);
                var defaultPicture = pictures.FirstOrDefault();
                var defaultPictureModel = new PictureModel
                {
                    ImageUrl = _pictureService.GetPictureUrl(defaultPicture, defaultPictureSize, !isAssociatedProduct),
                    FullSizeImageUrl = _pictureService.GetPictureUrl(defaultPicture, 0, !isAssociatedProduct),
                    Title = string.Format(_localizationService.GetResource("Media.Product.ImageLinkTitleFormat.Details"), model.Name),
                    AlternateText = string.Format(_localizationService.GetResource("Media.Product.ImageAlternateTextFormat.Details"), model.Name),
                };
                //"title" attribute
                defaultPictureModel.Title = (defaultPicture != null && !string.IsNullOrEmpty(defaultPicture.TitleAttribute)) ?
                    defaultPicture.TitleAttribute :
                    string.Format(_localizationService.GetResource("Media.Product.ImageLinkTitleFormat.Details"), model.Name);
                //"alt" attribute
                defaultPictureModel.AlternateText = (defaultPicture != null && !string.IsNullOrEmpty(defaultPicture.AltAttribute)) ?
                    defaultPicture.AltAttribute :
                    string.Format(_localizationService.GetResource("Media.Product.ImageAlternateTextFormat.Details"), model.Name);

                //all pictures
                var pictureModels = new List<PictureModel>();
                foreach (var picture in pictures)
                {

                    var pictureModel = new PictureModel
                    {
                        ImageUrl = _pictureService.GetPictureUrl(picture, _mediaSettings.ProductThumbPictureSizeOnProductDetailsPage),
                        FullSizeImageUrl = _pictureService.GetPictureUrl(picture,1024),
                        Title = string.Format(_localizationService.GetResource("Media.Product.ImageLinkTitleFormat.Details"), model.Name),
                        AlternateText = string.Format(_localizationService.GetResource("Media.Product.ImageAlternateTextFormat.Details"), model.Name),
                        IsPreselected = pictures.IndexOf(picture)==0
                    };
                    //"title" attribute
                    pictureModel.Title = !string.IsNullOrEmpty(picture.TitleAttribute) ?
                        picture.TitleAttribute :
                        string.Format(_localizationService.GetResource("Media.Product.ImageLinkTitleFormat.Details"), model.Name);
                    //"alt" attribute
                    pictureModel.AlternateText = !string.IsNullOrEmpty(picture.AltAttribute) ?
                        picture.AltAttribute :
                        string.Format(_localizationService.GetResource("Media.Product.ImageAlternateTextFormat.Details"), model.Name);

                    pictureModels.Add(pictureModel);
                }

                return new { DefaultPictureModel = defaultPictureModel, PictureModels = pictureModels };
            });
           

            if (queryStringCount > 0)
            {
                //Change image
                foreach (String key in Request.QueryString.AllKeys)
                {
                    int productAttrValueId;
                    bool validQueryString = int.TryParse(Request.QueryString[key], out productAttrValueId);
                    if (validQueryString)
                    {
                        ProductAttributeValueModel productAttrValueModel = _productAttributeService.GetProductAttributeValueById(productAttrValueId).ToModel();

                        ProductAttributeMappingModel productAttrMappingOfValue = _productAttributeService.GetProductAttributeMappingById(productAttrValueModel.ProductAttributeMappingId).ToModel();

                        if (productAttrValueModel != null)
                        {
                            isExistAttributeQueryString = true;
                            //set selected type for condition

                            ProductAttributeModel productAttributeOfValueModel = _productAttributeService.GetProductAttributeById(productAttrMappingOfValue.ProductAttributeId).ToModel();

                            if (productAttributeOfValueModel.Name.Equals(EggCommon.ProductTypeAttributeValue))
                            {
                                //set preselected style
                                productAttrTypeValueByQueryStringModel = productAttrValueModel;
                                model.SelectedTypeId = productAttrValueModel.Id;

                                //check preselected color
                                if (productAttrColorValueByQueryStringModel == null)
                                {
                                    var attributes = _productAttributeService.GetProductAttributeMappingsByProductId(product.Id);
                                    var colorAttrs = attributes.Where(p => p.AttributeControlTypeId == 40);
                                    foreach (var attr in colorAttrs)
                                    {
                                        var valuesThatShouldBeSelected = _productAttributeParser.ParseConditionValue(attr.ConditionAttributeXml, int.Parse(key), productAttrValueId).ToList();
                                        if (valuesThatShouldBeSelected.Any())
                                        {
                                            productAttrColorValueByQueryStringModel = attr.ProductAttributeValues.FirstOrDefault(p => p.IsPreSelected).ToModel();
                                            if (productAttrColorValueByQueryStringModel == null)
                                                productAttrColorValueByQueryStringModel = attr.ProductAttributeValues.FirstOrDefault().ToModel();
                                        }
                                    }
                                }
                            }
                            else if (productAttrMappingOfValue.AttributeControlType == AttributeControlType.ColorSquares)
                            {
                                //set selected color for image rending
                                productAttrColorValueByQueryStringModel = productAttrValueModel;
                            }
                        }
                    }
                    else
                    {
                        //check render query string
                        if (key.Equals(renderKey))
                        {
                            var value = Request.QueryString[key];
                            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                            renderRequest = JsonConvert.DeserializeObject<List<ProductRenderRequest>>(value);
                            isExistRenderQueryString = renderRequest.Any();
                        }
                        //check apply coupon query string
                        if (key.Equals(EggCommon.ApplyCouponQueryStringKey))
                        {
                            var couponCode = Request.QueryString[key];
                            this.OnApplyDiscountCouponByQueryString(couponCode, out isAppliedCoupon, out appliedDiscount);
                        }
                    }
                }

                //get preselected style of query string color if not set
                if (productAttrColorValueByQueryStringModel!=null && productAttrTypeValueByQueryStringModel==null)
                {
                    var productAttMappingOfColorModel = _productAttributeService.GetProductAttributeMappingById(productAttrColorValueByQueryStringModel.ProductAttributeMappingId).ToModel();

                    var conditionParsed = _productAttributeParser.ParseProductAttributeValues(productAttMappingOfColorModel.ConditionAttributeXml);

                    if (conditionParsed.Any())
                    {
                        var conditionType = conditionParsed.FirstOrDefault(p => p.ProductAttributeMapping.ProductAttribute.Name.Equals(EggCommon.ProductTypeAttributeValue)).ToModel();
                        if(conditionType!=null)
                        {
                            productAttrTypeValueByQueryStringModel = conditionType;
                        }
                        else
                        {
                            //restore default
                            productAttrColorValueByQueryStringModel = null;
                            queryStringCount = 0;
                        }
                    }
                }
                if (isExistAttributeQueryString)
                {
                    //set image source
                    string fullSizeImageUrl = this.GetProductWithQueryStringImage(productAttrTypeValueByQueryStringModel, productAttrColorValueByQueryStringModel, renderRequest);
                    model.DefaultPictureModel.ImageUrl = fullSizeImageUrl;
                    model.DefaultPictureModel.FullSizeImageUrl = fullSizeImageUrl;
                    model.PictureModels = cachedPictures.PictureModels;
                    model.PictureModels.FirstOrDefault(p => p.IsPreselected).FullSizeImageUrl = fullSizeImageUrl;
                }
            }
            else
            {
                //Default image
                model.DefaultPictureModel = cachedPictures.DefaultPictureModel;
                model.PictureModels = cachedPictures.PictureModels;
                model.PictureModels.FirstOrDefault(p => p.IsPreselected).FullSizeImageUrl = model.DefaultPictureModel.FullSizeImageUrl;
            }
            #endregion

            #region Product attributes

            //performance optimization
            //We cache a value indicating whether a product has attributes
            IList<ProductAttributeMapping> productAttributeMapping = null;
            string cacheKey = string.Format(ModelCacheEventConsumer.PRODUCT_HAS_PRODUCT_ATTRIBUTES_KEY, product.Id);
            var hasProductAttributesCache = _cacheManager.Get<bool?>(cacheKey);
            if (!hasProductAttributesCache.HasValue)
            {
                //no value in the cache yet
                //let's load attributes and cache the result (true/false)
                productAttributeMapping = _productAttributeService.GetProductAttributeMappingsByProductId(product.Id);
                hasProductAttributesCache = productAttributeMapping.Any();
                _cacheManager.Set(cacheKey, hasProductAttributesCache, 60);
            }
            if (hasProductAttributesCache.Value && productAttributeMapping == null)
            {
                //cache indicates that the product has attributes
                //let's load them
                productAttributeMapping = _productAttributeService.GetProductAttributeMappingsByProductId(product.Id);
            }
            if (productAttributeMapping == null)
            {
                productAttributeMapping = new List<ProductAttributeMapping>();
            }
            var tempProductAttrConditionChecks = new List<ProductDetailConditionCheckModel>();
            foreach (var attribute in productAttributeMapping)
            {
                var attributeModel = new ProductDetailsModel.ProductAttributeModel
                {
                    Id = attribute.Id,
                    ProductId = product.Id,
                    ProductAttributeId = attribute.ProductAttributeId,
                    Name = attribute.ProductAttribute.GetLocalized(x => x.Name),
                    Description = attribute.ProductAttribute.GetLocalized(x => x.Description),
                    TextPrompt = attribute.TextPrompt,
                    IsRequired = attribute.IsRequired,
                    AttributeControlType = attribute.AttributeControlType,
                    DefaultValue = updatecartitem != null ? null : attribute.DefaultValue,
                };
                var conditionCheckModel = new ProductDetailConditionCheckModel();
                //Check attr condition show
                if (String.IsNullOrEmpty(attribute.ConditionAttributeXml))
                    attributeModel.HasCondition = false;
                else
                {
                   var attrConditionValue = _productService.GetAttributeCondititonValue(attribute).ToModel();
                   
                   if(attrConditionValue!=null)
                    {
                        if (isExistAttributeQueryString)
                        {
                            if (productAttrTypeValueByQueryStringModel != null)
                            {
                                if (attrConditionValue.ProductAttributeMappingId == productAttrTypeValueByQueryStringModel.ProductAttributeMappingId && attrConditionValue.Id == productAttrTypeValueByQueryStringModel.Id)
                                {
                                    attributeModel.HasCondition = false;
                                    productPreselectedAttrTypeValueForPriceModel = attrConditionValue;
                                    attrConditionValue.IsPreSelected = true;
                                    if (attribute.AttributeControlType == AttributeControlType.ColorSquares)
                                    {
                                        var selectedColor = productAttrColorValueByQueryStringModel != null ? productAttrColorValueByQueryStringModel : attribute.ProductAttributeValues.FirstOrDefault(p => p.IsPreSelected).ToModel();
                                        model.SelectedColorId = selectedColor != null ? selectedColor.Id : attribute.ProductAttributeValues.FirstOrDefault().Id;
                                        foreach (var attrValue in attribute.ProductAttributeValues)
                                            attrValue.IsPreSelected = attrValue.Id == model.SelectedColorId;
                                    }
                                }
                                else
                                {
                                    attrConditionValue.IsPreSelected = false;
                                    attributeModel.HasCondition = true;
                                }
                            }
                            else
                                attributeModel.HasCondition = true;
                        }
                        else
                        {
                            if (attrConditionValue != null)
                            {
                                attributeModel.HasCondition = !attrConditionValue.IsPreSelected;
                                if (attrConditionValue.IsPreSelected)
                                {
                                    productPreselectedAttrTypeValueForPriceModel = attrConditionValue;
                                    if(attribute.AttributeControlType == AttributeControlType.ColorSquares)
                                    {
                                        var selectedColor = attribute.ProductAttributeValues.FirstOrDefault(p => p.IsPreSelected);
                                        model.SelectedColorId = selectedColor != null ? selectedColor.Id : attribute.ProductAttributeValues.FirstOrDefault().Id;
                                    }
                                }
                            }
                            else
                                attributeModel.HasCondition = true;
                        }
                        conditionCheckModel.ProductAttributeConditionValue = attrConditionValue;
                    }
                    else
                    {
                        //product is not available condition
                    }
                }

                if (!String.IsNullOrEmpty(attribute.ValidationFileAllowedExtensions))
                {
                    attributeModel.AllowedFileExtensions = attribute.ValidationFileAllowedExtensions
                        .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                        .ToList();
                }

                if (attribute.ShouldHaveValues())
                {
                    //values
                    var attributeValues = _productAttributeService.GetProductAttributeValues(attribute.Id);
                    foreach (var attributeValue in attributeValues)
                    {
                        var valueModel = new ProductDetailsModel.ProductAttributeValueModel
                        {
                            Id = attributeValue.Id,
                            Name = attributeValue.GetLocalized(x => x.Name),
                            ColorSquaresRgb = attributeValue.ColorSquaresRgb, //used with "Color squares" attribute type
                            IsPreSelected = attributeValue.IsPreSelected
                        };
                        attributeModel.Values.Add(valueModel);

                        //display price if allowed
                        if (_permissionService.Authorize(StandardPermissionProvider.DisplayPrices))
                        {
                            decimal taxRate;
                            decimal attributeValuePriceAdjustment = _priceCalculationService.GetProductAttributeValuePriceAdjustment(attributeValue);
                            decimal priceAdjustmentBase = _taxService.GetProductPrice(product, attributeValuePriceAdjustment, out taxRate);
                            decimal priceAdjustment = _currencyService.ConvertFromPrimaryStoreCurrency(priceAdjustmentBase, _workContext.WorkingCurrency);
                            if (priceAdjustmentBase > decimal.Zero)
                                valueModel.PriceAdjustment = "+" + _priceFormatter.FormatPrice(priceAdjustment, false, false);
                            else if (priceAdjustmentBase < decimal.Zero)
                                valueModel.PriceAdjustment = "-" + _priceFormatter.FormatPrice(-priceAdjustment, false, false);
                            
                            valueModel.PriceAdjustmentValue = priceAdjustment;
                        }

                        //"image square" picture (with with "image squares" attribute type only)
                        if (attributeValue.ImageSquaresPictureId > 0)
                        {
                            var productAttributeImageSquarePictureCacheKey = string.Format(ModelCacheEventConsumer.PRODUCTATTRIBUTE_IMAGESQUARE_PICTURE_MODEL_KEY,
                                   attributeValue.ImageSquaresPictureId,
                                   _webHelper.IsCurrentConnectionSecured(),
                                   _storeContext.CurrentStore.Id);
                            valueModel.ImageSquaresPictureModel = _cacheManager.Get(productAttributeImageSquarePictureCacheKey, () =>
                            {
                                var imageSquaresPicture = _pictureService.GetPictureById(attributeValue.ImageSquaresPictureId);
                                if (imageSquaresPicture != null)
                                {
                                    return new PictureModel
                                    {
                                        FullSizeImageUrl = _pictureService.GetPictureUrl(imageSquaresPicture),
                                        ImageUrl = _pictureService.GetPictureUrl(imageSquaresPicture, _mediaSettings.ImageSquarePictureSize)
                                    };
                                }
                                return new PictureModel();
                            });
                        }

                        //picture of a product attribute value
                        valueModel.PictureId = attributeValue.PictureId;
                    }

                }

                //set already selected attributes (if we're going to update the existing shopping cart item)
                if (updatecartitem != null)
                {
                    switch (attribute.AttributeControlType)
                    {
                        case AttributeControlType.DropdownList:
                        case AttributeControlType.RadioList:
                        case AttributeControlType.Checkboxes:
                        case AttributeControlType.ColorSquares:
                        case AttributeControlType.ImageSquares:
                            {
                                if (!String.IsNullOrEmpty(updatecartitem.AttributesXml))
                                {
                                    //clear default selection
                                    foreach (var item in attributeModel.Values)
                                        item.IsPreSelected = false;

                                    //select new values
                                    var selectedValues = _productAttributeParser.ParseProductAttributeValues(updatecartitem.AttributesXml);
                                    foreach (var attributeValue in selectedValues)
                                        foreach (var item in attributeModel.Values)
                                            if (attributeValue.Id == item.Id)
                                                item.IsPreSelected = true;
                                }
                            }
                            break;
                        case AttributeControlType.ReadonlyCheckboxes:
                            {
                                //do nothing
                                //values are already pre-set
                            }
                            break;
                        case AttributeControlType.TextBox:
                        case AttributeControlType.MultilineTextbox:
                            {
                                if (!String.IsNullOrEmpty(updatecartitem.AttributesXml))
                                {
                                    var enteredText = _productAttributeParser.ParseValues(updatecartitem.AttributesXml, attribute.Id);
                                    if (enteredText.Any())
                                        attributeModel.DefaultValue = enteredText[0];
                                }
                            }
                            break;
                        case AttributeControlType.Datepicker:
                            {
                                //keep in mind my that the code below works only in the current culture
                                var selectedDateStr = _productAttributeParser.ParseValues(updatecartitem.AttributesXml, attribute.Id);
                                if (selectedDateStr.Any())
                                {
                                    DateTime selectedDate;
                                    if (DateTime.TryParseExact(selectedDateStr[0], "D", CultureInfo.CurrentCulture,
                                                           DateTimeStyles.None, out selectedDate))
                                    {
                                        //successfully parsed
                                        attributeModel.SelectedDay = selectedDate.Day;
                                        attributeModel.SelectedMonth = selectedDate.Month;
                                        attributeModel.SelectedYear = selectedDate.Year;
                                    }
                                }

                            }
                            break;
                        case AttributeControlType.FileUpload:
                            {
                                if (!String.IsNullOrEmpty(updatecartitem.AttributesXml))
                                {
                                    var downloadGuidStr = _productAttributeParser.ParseValues(updatecartitem.AttributesXml, attribute.Id).FirstOrDefault();
                                    Guid downloadGuid;
                                    Guid.TryParse(downloadGuidStr, out downloadGuid);
                                    var download = _downloadService.GetDownloadByGuid(downloadGuid);
                                    if (download != null)
                                        attributeModel.DefaultValue = download.DownloadGuid.ToString();
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
                conditionCheckModel.ProductAttributeModel = attributeModel;
                tempProductAttrConditionChecks.Add(conditionCheckModel);
                model.ProductAttributes.Add(attributeModel);
            }
            //ensure product attribute value for calculating price not null
            if (productPreselectedAttrTypeValueForPriceModel == null)
            {
                var productAttrMappingStyles = product.ProductAttributeMappings.Where(p => p.ProductAttribute.Name.Equals(EggCommon.ProductTypeAttributeValue)).FirstOrDefault();
                if (productAttrMappingStyles != null && productAttrMappingStyles.ShouldHaveValues())
                    productPreselectedAttrTypeValueForPriceModel = productAttrMappingStyles.ProductAttributeValues.FirstOrDefault().ToModel();
            }
            if (productPreselectedAttrTypeValueForPriceModel == null)
                throw new ArgumentNullException("Product not set any attribute with price. return error.");

            model.SelectedTypeId = productPreselectedAttrTypeValueForPriceModel == null ? 0 : productPreselectedAttrTypeValueForPriceModel.Id;

            //Type infor
            model.ProductContentForAttributeValueModels = _productTypeService.GetProductContentForAttributeValues(product.Id, model.SelectedTypeId);
            
            // Shipping Fee From Table ShippingByCountry
            if (productPreselectedAttrTypeValueForPriceModel != null)
            {
                var preSelectShippingByCountry = _productTypeService.GetShippingByCountryByProductTypeName(productPreselectedAttrTypeValueForPriceModel.Name);
                if(preSelectShippingByCountry != null)
                {
                    model.IsFreeShipping = preSelectShippingByCountry.USShippingPrice == 0;
                }
            }


            //Product image square
            model.ProductTypeCarouselImage = _productService.PrepareProductImageForCarousel(productAttributeMapping, model.SelectedTypeId);

            //set default product show attribute condition if not set
            var isAllowedToContinueCalculatingPrice = tempProductAttrConditionChecks.Select(p => p.ProductAttributeConditionValue).Where(con => con.IsPreSelected).FirstOrDefault()!=null;
            if(!isAllowedToContinueCalculatingPrice)
            {
                var productNotSetContions = tempProductAttrConditionChecks.Where(p=> !string.IsNullOrEmpty(p.ProductAttributeConditionValue.Name))
                    .Where(p => p.ProductAttributeConditionValue.Name.Equals(productPreselectedAttrTypeValueForPriceModel.Name)).ToList();
                if (productNotSetContions.Any())
                {
                    foreach (var checkCondition in productNotSetContions)
                    {
                        var updateConditionItem = model.ProductAttributes.Where(p => p.Id == checkCondition.ProductAttributeModel.Id).FirstOrDefault();
                        if (updateConditionItem != null)
                            updateConditionItem.HasCondition = false;
                    }
                    productPreselectedAttrTypeValueForPriceModel = productNotSetContions.Select(p=>p.ProductAttributeConditionValue).Where(p => p.PriceAdjustmentValue > 0).FirstOrDefault();
                }
            }
            if(!isExistAttributeQueryString && isExistRenderQueryString)
            {
                string fullSizeImageUrl = this.GetProductWithQueryStringImage(productPreselectedAttrTypeValueForPriceModel, null, renderRequest);
                model.DefaultPictureModel.ImageUrl = fullSizeImageUrl;
                model.DefaultPictureModel.FullSizeImageUrl = fullSizeImageUrl;
                model.PictureModels = cachedPictures.PictureModels;
            }
            else if(!isExistAttributeQueryString && !isExistRenderQueryString)
            {
                //Default image
                model.DefaultPictureModel = cachedPictures.DefaultPictureModel;
                model.PictureModels = cachedPictures.PictureModels;
            }
            #endregion

            #region Product custom fields

            //performance optimization
            //We cache a value indicating whether a product has attributes
            List<ProductCustomFieldMapping> productCustomFieldMappings = product.ProductCustomFieldMappings.ToList();
            if(isExistRenderQueryString)
            {
                var parsedData = this.ParseRenderRequest(productPreselectedAttrTypeValueForPriceModel, productAttrColorValueByQueryStringModel, renderRequest);
                if(parsedData!=null)
                {
                    model.RenderRequest = parsedData;
                    string jsonData = JsonConvert.SerializeObject(parsedData.Data);
                    var customFieldValueAttr = model.ProductAttributes.Where(p => p.Name.Equals("CustomFieldValue")).FirstOrDefault();
                    if (customFieldValueAttr != null)
                        customFieldValueAttr.DefaultValue = jsonData;
                }
               
                foreach (var fieldMap in productCustomFieldMappings)
                {
                    foreach(var req in renderRequest)
                    {
                        if(req.Id == fieldMap.CustomFieldId)
                        {
                            fieldMap.CustomField.TypedValue = req.Text;
                        }
                    }
                }
            }

            var groupProductCustomFields = productCustomFieldMappings.GroupBy(pct => new { pct.Group }).Select(p => new ProductDetailsModel.GroupedProductCustomFieldModel
            {
                Group = p.Select(item => item.Group).FirstOrDefault(),
                ProductCustomFieldModels = p.Select(item => new ProductCustomFieldModel {
                    Id = item.Id,
                    ProductId = product.Id,
                    CustomFieldId = item.CustomFieldId,
                    CustomFieldModel = item.CustomField,
                    TypedText = item.CustomField.TypedValue,
                    Values = item.ProductCustomFieldValues.Select(ctv => new ProductDetailsModel.ProductCustomFieldValueModel
                    {
                        Id = ctv.Id,
                        PositionX = ctv.PositionX,
                        PositionY = ctv.PositionY,
                        ProductAttributeValueId = ctv.ProductAttributeValueId,
                        ProductCustomFieldMappingId = ctv.ProductCustomFieldMappingId,
                        IsPreselected = ctv.ProductAttributeValueId == model.SelectedTypeId
                    }).ToList()
        }).ToList()
            }).ToList();

            model.GroupedProductCustomFieldModels = groupProductCustomFields;
            #endregion

            #region Product price

            model.ProductPrice.ProductId = product.Id;
            if (_permissionService.Authorize(StandardPermissionProvider.DisplayPrices))
            {
               
                model.ProductPrice.HidePrices = false;
                decimal taxRate;

                decimal productPrice = decimal.Zero;
                decimal oldPrice = decimal.Zero;
                if (productPreselectedAttrTypeValueForPriceModel!=null && productPreselectedAttrTypeValueForPriceModel.PriceAdjustmentValue > 0)
                {
                    // Product price with presected Product;
                    ProductAttributeValue productPreselectedAttrTypeValueForPrice = _productAttributeService.GetProductAttributeValueById(productPreselectedAttrTypeValueForPriceModel.Id);
                    decimal attributeValuePriceAdjustment = _priceCalculationService.GetProductAttributeValuePriceAdjustment(productPreselectedAttrTypeValueForPrice);
                    decimal priceAdjustmentFromCustomerCurrency = _currencyService.ConvertFromPrimaryStoreCurrency(attributeValuePriceAdjustment, _workContext.WorkingCurrency);
                    productPrice = priceAdjustmentFromCustomerCurrency;

                    oldPrice = _currencyService.ConvertFromPrimaryStoreCurrency(attributeValuePriceAdjustment + _catalogSettings.SaveAmount, _workContext.WorkingCurrency); ;
                }
                else
                {
                    if (product.CustomerEntersPrice)
                    {
                        model.ProductPrice.CustomerEntersPrice = true;
                    }
                    else
                    {
                        if (product.CallForPrice)
                        {
                            model.ProductPrice.CallForPrice = true;
                        }
                        else
                        {
                            decimal productCalculatedPrice = _taxService.GetProductPrice(product, _priceCalculationService.GetFinalPrice(product, _workContext.CurrentCustomer, includeDiscounts: true), out taxRate);
                            productPrice = _currencyService.ConvertFromPrimaryStoreCurrency(productCalculatedPrice, _workContext.WorkingCurrency);
                            oldPrice = _currencyService.ConvertFromPrimaryStoreCurrency(productCalculatedPrice + _catalogSettings.SaveAmount, _workContext.WorkingCurrency);
                        }
                    }
                }

                if (isAppliedCoupon && appliedDiscount != null)
                {
                    if(appliedDiscount.AppliedToProducts.Any())
                    {
                        //only change price for discount applied to product
                        if (appliedDiscount.AppliedToProducts.Select(p => p.Id).Contains(product.Id))
                        {
                            //update attribute priceadjustment
                            var variantAttribute = model.ProductAttributes.FirstOrDefault(p => p.Name.Equals(EggCommon.ProductTypeAttributeValue));
                            if (appliedDiscount.UsePercentage)
                            {
                                productPrice = productPrice - (productPrice * appliedDiscount.DiscountPercentage)/100;
                                if (variantAttribute != null)
                                    foreach (var item in variantAttribute.Values)
                                        item.PriceAdjustment = string.Format("+{0}", _priceFormatter.FormatPrice(item.PriceAdjustmentValue - (item.PriceAdjustmentValue * appliedDiscount.DiscountPercentage) / 100));
                            }
                            else if (appliedDiscount.DiscountAmount != decimal.Zero)
                            {
                                productPrice = productPrice - appliedDiscount.DiscountAmount;
                                if (variantAttribute != null)
                                    foreach (var item in variantAttribute.Values)
                                        item.PriceAdjustment =string.Format("+{0}",_priceFormatter.FormatPrice(item.PriceAdjustmentValue - appliedDiscount.DiscountAmount));
                            }
                        } 
                    }
                    
                }

                model.ProductPrice.Price = _priceFormatter.FormatPrice(productPrice);
                model.ProductPrice.PriceValue = productPrice;
                if (_catalogSettings.SaveAmount>0)
                    model.ProductPrice.OldPrice = _priceFormatter.FormatPrice(oldPrice);
                //property for German market
                //we display tax/shipping info only with "shipping enabled" for this product
                //we also ensure this it's not free shipping
                model.ProductPrice.DisplayTaxShippingInfo = _catalogSettings.DisplayTaxShippingInfoProductDetailsPage
                    && product.IsShipEnabled &&
                    !product.IsFreeShipping;

                //PAngV baseprice (used in Germany)
                model.ProductPrice.BasePricePAngV = product.FormatBasePrice(productPrice,
                    _localizationService, _measureService, _currencyService, _workContext, _priceFormatter);

                //currency code
                model.ProductPrice.CurrencyCode = _workContext.WorkingCurrency.CurrencyCode;

                //rental
                if (product.IsRental)
                {
                    model.ProductPrice.IsRental = true;
                    var priceStr = _priceFormatter.FormatPrice(productPrice);
                    model.ProductPrice.RentalPrice = _priceFormatter.FormatRentalProductPeriod(product, priceStr);
                }
            }
            else
            {
                model.ProductPrice.HidePrices = true;
                model.ProductPrice.OldPrice = null;
                model.ProductPrice.Price = null;
            }

            //time out ExpiresDay
            //model.ProductPrice.SellOffExpiresDay = GetSellOffExpiresDay();
            #endregion

            #region 'Add to cart' model

            model.AddToCart.ProductId = product.Id;
            if (updatecartitem != null)
            {
                model.AddToCart.UpdatedShoppingCartItemId = updatecartitem.Id;
                model.AddToCart.UpdateShoppingCartItemType = updatecartitem.ShoppingCartType;
            }

            //quantity
            model.AddToCart.EnteredQuantity = updatecartitem != null ? updatecartitem.Quantity : product.OrderMinimumQuantity;
            //allowed quantities
            var allowedQuantities = product.ParseAllowedQuantities();
            foreach (var qty in allowedQuantities)
            {
                model.AddToCart.AllowedQuantities.Add(new SelectListItem
                {
                    Text = qty.ToString(),
                    Value = qty.ToString(),
                    Selected = updatecartitem != null && updatecartitem.Quantity == qty
                });
            }
            //minimum quantity notification
            if (product.OrderMinimumQuantity > 1)
            {
                model.AddToCart.MinimumQuantityNotification = string.Format(_localizationService.GetResource("Products.MinimumQuantityNotification"), product.OrderMinimumQuantity);
            }

            //'add to cart', 'add to wishlist' buttons
            model.AddToCart.DisableBuyButton = product.DisableBuyButton;
            model.AddToCart.DisableWishlistButton = product.DisableWishlistButton || !_permissionService.Authorize(StandardPermissionProvider.EnableWishlist);
            //pre-order
            if (product.AvailableForPreOrder)
            {
                model.AddToCart.AvailableForPreOrder = !product.PreOrderAvailabilityStartDateTimeUtc.HasValue ||
                    product.PreOrderAvailabilityStartDateTimeUtc.Value >= DateTime.UtcNow;
                model.AddToCart.PreOrderAvailabilityStartDateTimeUtc = product.PreOrderAvailabilityStartDateTimeUtc;
            }
            //rental
            model.AddToCart.IsRental = product.IsRental;

            //customer entered price
            model.AddToCart.CustomerEntersPrice = product.CustomerEntersPrice;
            if (model.AddToCart.CustomerEntersPrice)
            {
                decimal minimumCustomerEnteredPrice = _currencyService.ConvertFromPrimaryStoreCurrency(product.MinimumCustomerEnteredPrice, _workContext.WorkingCurrency);
                decimal maximumCustomerEnteredPrice = _currencyService.ConvertFromPrimaryStoreCurrency(product.MaximumCustomerEnteredPrice, _workContext.WorkingCurrency);

                model.AddToCart.CustomerEnteredPrice = updatecartitem != null ? updatecartitem.CustomerEnteredPrice : minimumCustomerEnteredPrice;
                model.AddToCart.CustomerEnteredPriceRange = string.Format(_localizationService.GetResource("Products.EnterProductPrice.Range"),
                    _priceFormatter.FormatPrice(minimumCustomerEnteredPrice, false, false),
                    _priceFormatter.FormatPrice(maximumCustomerEnteredPrice, false, false));
            }

            #endregion


            #region Remarketting Data
            Category currentCategory = productCategories.FirstOrDefault().Category;
            var rootCategory = currentCategory.GetRootCategory(_categoryService, _aclService, _storeMappingService);
            model.RemarketingData = this.PrepareCustomerRemarketingDataModel(_workContext,_storeContext,_categoryService,_productService,_genericAttributeService, rootCategory, null);

            var tempRemarketingProductId = EggCommon.GetProductRemarketingId(model.Id, model.SelectedTypeId,model.SelectedColorId);
            //GA ID
            string googleRemarketingId = string.Format("{0}{1}", tempRemarketingProductId, _storeContext.CurrentStore.RemarketingForGoogleId);

            //FB ID
            string facbookRemarketingId = string.Format("{0}{1}", tempRemarketingProductId, _storeContext.CurrentStore.RemarketingForFacebookId);
            model.RemarketingProductId = facbookRemarketingId;

            //prepare remarketing data 
            var gaTrackingItem = new GATrackingItemModel();
            gaTrackingItem.Id = googleRemarketingId;
            gaTrackingItem.Category = product.ProductCategories.Select(p => p.Category.Name).FirstOrDefault();
            var availableAttr = model.ProductAttributes.Where(p => p.Name.Equals("Available Products")).FirstOrDefault();
            gaTrackingItem.Price = productPreselectedAttrTypeValueForPriceModel.PriceAdjustmentValue;
            gaTrackingItem.Name = model.Name.Trim();
            model.GATrackingItem = gaTrackingItem;

            #endregion

            #region Product review overview
            this.PrepareProductReviewsModel(model.ProductReviewsModel,product);
            model.ProductReviewOverview = this.PrepareProductReviewOverviewModel(_storeContext, _catalogSettings,
                _cacheManager, product);

            #endregion

            return model;
        }

        [NonAction]
        protected virtual void PrepareProductReviewsModel(ProductReviewsModel model, Product product)
        {
            if (product == null)
                throw new ArgumentNullException("product");

            if (model == null)
                throw new ArgumentNullException("model");

            model.ProductId = product.Id;
            model.ProductName = product.GetLocalized(x => x.Name);
            model.ProductSeName = product.GetSeName();

            if (_catalogSettings.AllowAnonymousUsersToReviewProduct)
            {
                var productReviews = _productService.GetProductReviews(product.Id);
                foreach (var pr in productReviews)
                {
                    var customer = pr.Customer;
                    var reviewModel = new ProductReviewModel
                    {
                        Id = pr.Id,
                        CustomerId = pr.CustomerId,
                        CustomerName = customer.FormatUserName(),
                        AllowViewingProfiles = _customerSettings.AllowViewingProfiles && customer != null && !customer.IsGuest(),
                        Title = pr.Title,
                        ReviewText = pr.ReviewText,
                        Rating = pr.Rating,
                        OrderInfo = pr.OrderInfo,
                        CustomerDisplayName = pr.CustomerDisplayName,
                        Helpfulness = new ProductReviewHelpfulnessModel
                        {
                            ProductReviewId = pr.Id,
                            HelpfulYesTotal = pr.HelpfulYesTotal,
                            HelpfulNoTotal = pr.HelpfulNoTotal,
                        },
                        WrittenOnStr = _dateTimeHelper.ConvertToUserTime(pr.CreatedOnUtc, DateTimeKind.Utc).ToString("g"),
                    };
                    foreach (var reviewPicture in pr.ProductReviewPictures.OrderBy(p=>p.DisplayOrder))
                    {
                        var picture = reviewPicture.Picture == null ? this._pictureService.GetPictureById(reviewPicture.PictureId) : reviewPicture.Picture;
                        string pictureUrl = this._pictureService.GetPictureUrl(picture);
                        var pictureModel = new ProductReviewPictureModel
                        {
                            PictureUrl= pictureUrl,
                            PictureId = reviewPicture.PictureId,
                            ProductReviewId= reviewPicture.ProductReviewId,
                            DisplayOrder = reviewPicture.DisplayOrder
                        };
                        reviewModel.ProductReviewPictures.Add(pictureModel);
                    }
                    model.Items.Add(reviewModel);
                }

                model.AddProductReview.CanCurrentCustomerLeaveReview = _catalogSettings.AllowAnonymousUsersToReviewProduct || !_workContext.CurrentCustomer.IsGuest();
                model.AddProductReview.DisplayCaptcha = _captchaSettings.Enabled && _captchaSettings.ShowOnProductReviewPage;
                model.AddProductReview.Email = _workContext.CurrentCustomer.Email;
                model.AddProductReview.Name = _workContext.CurrentCustomer.GetFullName();
                model.AddProductReview.ProductId = product.Id;

            }
        }

        #endregion

        #region Product details page

        [NopHttpsRequirement(SslRequirement.No)]
        public ActionResult ProductDetails(int productId, int updatecartitemid = 0)
        {
            var product = _productService.GetProductById(productId);
            if (product == null || product.Deleted)
                return InvokeHttp404();

            //published?
            if (!_catalogSettings.AllowViewUnpublishedProductPage)
            {
                //Check whether the current user has a "Manage catalog" permission
                //It allows him to preview a product before publishing
                if (!product.Published && !_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                    return InvokeHttp404();
            }

            //ACL (access control list)
            if (!_aclService.Authorize(product))
                return InvokeHttp404();

            //Store mapping
            if (!_storeMappingService.Authorize(product))
                return InvokeHttp404();

            //availability dates
            if (!product.IsAvailable())
                return InvokeHttp404();

            //visible individually?
            //if (!product.VisibleIndividually)
            //{
            //    //is this one an associated products?
            //    var parentGroupedProduct = _productService.GetProductById(product.ParentGroupedProductId);
            //    if (parentGroupedProduct == null)
            //        return RedirectToRoute("HomePage");

            //    return RedirectToRoute("Product", new { SeName = parentGroupedProduct.GetSeName() });
            //}

            //update existing shopping cart or wishlist  item?
            ShoppingCartItem updatecartitem = null;
            if (_shoppingCartSettings.AllowCartItemEditing && updatecartitemid > 0)
            {
                var cart = _workContext.CurrentCustomer.ShoppingCartItems
                    .LimitPerStore(_storeContext.CurrentStore.Id)
                    .ToList();
                updatecartitem = cart.FirstOrDefault(x => x.Id == updatecartitemid);
                //not found?
                if (updatecartitem == null)
                {
                    return RedirectToRoute("Product", new { SeName = product.GetSeName() });
                }
                //is it this product?
                if (product.Id != updatecartitem.ProductId)
                {
                    return RedirectToRoute("Product", new { SeName = product.GetSeName() });
                }
            }

            //prepare the model
            var model = PrepareProductDetailsPageModel(product, updatecartitem, false);
            model.FreeShippingOverXValue = _shippingSettings.FreeShippingOverXEnabled ?  (int)_shippingSettings.FreeShippingOverXValue :50;
            //save as recently viewed

            _recentlyViewedProductsService.AddProductToRecentlyViewedList(product.Id);

            //display "edit" (manage) link
            if (_permissionService.Authorize(StandardPermissionProvider.AccessAdminPanel) && _permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                DisplayEditLink(Url.Action("Edit", "Product", new { id = product.Id, area = "Admin" }));

            //activity log
            _customerActivityService.InsertActivity("PublicStore.ViewProduct", _localizationService.GetResource("ActivityLog.PublicStore.ViewProduct"), product.Name);

            //log subscribe activity
            _subscribeActivityLogService.InsertSubscribeActivityLogViewProduct(product.Id, string.Empty);

            return View(model.ProductTemplateViewPath, model);
        }

        [ChildActionOnly]
        public ActionResult RelatedProducts(int productId, int? productThumbPictureSize)
        {
            ////load and cache report
            //var productIds = _cacheManager.Get(string.Format(ModelCacheEventConsumer.PRODUCTS_RELATED_IDS_KEY, productId, _storeContext.CurrentStore.Id),
            //    () =>
            //        _productService.GetRelatedProductsByProductId1(productId).Select(x => x.ProductId2).ToArray()
            //        );

            //load products
            var products = _productService.GetRelatedProducts(productId);
            //ACL and store mapping
            products = products.Where(p => _aclService.Authorize(p) && _storeMappingService.Authorize(p)).ToList();
            //availability dates
            products = products.Where(p => p.IsAvailable()).ToList();

            if (!products.Any())
                return Content("");

            var model = PrepareProductOverviewModels(products, true, true, 300).ToList();
            return PartialView(model);
        }

        [ChildActionOnly]
        public ActionResult ProductsAlsoPurchased(int productId, int? productThumbPictureSize)
        {
            if (!_catalogSettings.ProductsAlsoPurchasedEnabled)
                return Content("");

            //load and cache report
            var productIds = _cacheManager.Get(string.Format(ModelCacheEventConsumer.PRODUCTS_ALSO_PURCHASED_IDS_KEY, productId, _storeContext.CurrentStore.Id),
                () =>
                    _orderReportService
                    .GetAlsoPurchasedProductsIds(_storeContext.CurrentStore.Id, productId, _catalogSettings.ProductsAlsoPurchasedNumber)
                    );

            //load products
            var products = _productService.GetProductsByIds(productIds);
            //ACL and store mapping
            products = products.Where(p => _aclService.Authorize(p) && _storeMappingService.Authorize(p)).ToList();
            //availability dates
            products = products.Where(p => p.IsAvailable()).ToList();

            if (!products.Any())
                return Content("");

            //prepare model
            var model = PrepareProductOverviewModels(products, true, true, productThumbPictureSize).ToList();

            return PartialView(model);
        }

        [ChildActionOnly]
        public ActionResult CrossSellProducts(int? productThumbPictureSize)
        {
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();

            var products = _productService.GetCrosssellProductsByShoppingCart(cart, _shoppingCartSettings.CrossSellsNumber);
            //ACL and store mapping
            products = products.Where(p => _aclService.Authorize(p) && _storeMappingService.Authorize(p)).ToList();
            //availability dates
            products = products.Where(p => p.IsAvailable()).ToList();

            if (!products.Any())
                return Content("");


            //Cross-sell products are dispalyed on the shopping cart page.
            //We know that the entire shopping cart page is not refresh
            //even if "ShoppingCartSettings.DisplayCartAfterAddingProduct" setting  is enabled.
            //That's why we force page refresh (redirect) in this case
            var model = PrepareProductOverviewModels(products,
                productThumbPictureSize: productThumbPictureSize, forceRedirectionAfterAddingToCart: true)
                .ToList();

            return PartialView(model);
        }

        [HttpGet]
        public ActionResult SizeGuide(string typeProduct)
        {
            return PartialView("SizeGuide", typeProduct);
        }

        #endregion

        #region Recently viewed products

        [NopHttpsRequirement(SslRequirement.No)]
        public ActionResult RecentlyViewedProducts()
        {
            if (!_catalogSettings.RecentlyViewedProductsEnabled)
                return Content("");

            var products = _recentlyViewedProductsService.GetRecentlyViewedProducts(_catalogSettings.RecentlyViewedProductsNumber);

            var model = new List<ProductOverviewModel>();
            model.AddRange(PrepareProductOverviewModels(products));

            return View(model);
        }

        [ChildActionOnly]
        public ActionResult RecentlyViewedProductsBlock(int? productThumbPictureSize, bool? preparePriceModel)
        {
            if (!_catalogSettings.RecentlyViewedProductsEnabled)
                return Content("");

            var preparePictureModel = productThumbPictureSize.HasValue;
            var products = _recentlyViewedProductsService.GetRecentlyViewedProducts(_catalogSettings.RecentlyViewedProductsNumber);

            //ACL and store mapping
            products = products.Where(p => _aclService.Authorize(p) && _storeMappingService.Authorize(p)).ToList();
            //availability dates
            products = products.Where(p => p.IsAvailable()).ToList();

            if (!products.Any())
                return Content("");

            //prepare model
            var model = new List<ProductOverviewModel>();
            model.AddRange(PrepareProductOverviewModels(products,
                preparePriceModel.GetValueOrDefault(),
                preparePictureModel,
                productThumbPictureSize));

            return PartialView(model);
        }

        #endregion

        #region New (recently added) products page

        [NopHttpsRequirement(SslRequirement.No)]
        public ActionResult NewProducts()
        {
            if (!_catalogSettings.NewProductsEnabled)
                return Content("");

            var products = _productService.SearchProducts(
                storeId: _storeContext.CurrentStore.Id,
                visibleIndividuallyOnly: true,
                markedAsNewOnly: true,
                orderBy: ProductSortingEnum.CreatedOn,
                pageSize: _catalogSettings.NewProductsNumber);

            var model = new List<ProductOverviewModel>();
            model.AddRange(PrepareProductOverviewModels(products));

            return View(model);
        }

        public ActionResult NewProductsRss()
        {
            var feed = new SyndicationFeed(
                                    string.Format("{0}: New products", _storeContext.CurrentStore.GetLocalized(x => x.Name)),
                                    "Information about products",
                                    new Uri(_webHelper.GetStoreLocation(false)),
                                    string.Format("urn:store:{0}:newProducts", _storeContext.CurrentStore.Id),
                                    DateTime.UtcNow);

            if (!_catalogSettings.NewProductsEnabled)
                return new RssActionResult { Feed = feed };

            var items = new List<SyndicationItem>();

            var products = _productService.SearchProducts(
                storeId: _storeContext.CurrentStore.Id,
                visibleIndividuallyOnly: true,
                markedAsNewOnly: true,
                orderBy: ProductSortingEnum.CreatedOn,
                pageSize: _catalogSettings.NewProductsNumber);
            foreach (var product in products)
            {
                string productUrl = Url.RouteUrl("Product", new { SeName = product.GetSeName() }, _webHelper.IsCurrentConnectionSecured() ? "https" : "http");
                string productName = product.GetLocalized(x => x.Name);
                string productDescription = product.GetLocalized(x => x.ShortDescription);
                var item = new SyndicationItem(productName, productDescription, new Uri(productUrl), String.Format("urn:store:{0}:newProducts:product:{1}", _storeContext.CurrentStore.Id, product.Id), product.CreatedOnUtc);
                items.Add(item);
                //uncomment below if you want to add RSS enclosure for pictures
                //var picture = _pictureService.GetPicturesByProductId(product.Id, 1).FirstOrDefault();
                //if (picture != null)
                //{
                //    var imageUrl = _pictureService.GetPictureUrl(picture, _mediaSettings.ProductDetailsPictureSize);
                //    item.ElementExtensions.Add(new XElement("enclosure", new XAttribute("type", "image/jpeg"), new XAttribute("url", imageUrl)).CreateReader());
                //}

            }
            feed.Items = items;
            return new RssActionResult { Feed = feed };
        }

        #endregion

        #region Home page bestsellers and products

        [ChildActionOnly]
        public ActionResult HomepageBestSellers(int? productThumbPictureSize)
        {
            if (!_catalogSettings.ShowBestsellersOnHomepage || _catalogSettings.NumberOfBestsellersOnHomepage == 0)
                return Content("");

            //load and cache report
            var report = _cacheManager.Get(string.Format(ModelCacheEventConsumer.HOMEPAGE_BESTSELLERS_IDS_KEY, _storeContext.CurrentStore.Id),
                () => _orderReportService.BestSellersReport(
                    storeId: _storeContext.CurrentStore.Id,
                    pageSize: _catalogSettings.NumberOfBestsellersOnHomepage)
                    .ToList());


            //load products
            var products = _productService.GetProductsByIds(report.Select(x => x.ProductId).ToArray());
            //ACL and store mapping
            products = products.Where(p => _aclService.Authorize(p) && _storeMappingService.Authorize(p)).ToList();
            //availability dates
            products = products.Where(p => p.IsAvailable()).ToList();

            if (!products.Any())
                return Content("");

            //prepare model
            var model = PrepareProductOverviewModels(products, true, true, productThumbPictureSize).ToList();
            return PartialView(model);
        }

        [ChildActionOnly]
        public ActionResult HomepageProducts(int? productThumbPictureSize)
        {
            var products = _productService.GetAllProductsDisplayedOnHomePage();
            //ACL and store mapping
            products = products.Where(p => _aclService.Authorize(p) && _storeMappingService.Authorize(p)).ToList();
            //availability dates
            products = products.Where(p => p.IsAvailable()).ToList();

            if (!products.Any())
                return Content("");

            var model = PrepareProductOverviewModels(products, true, true, productThumbPictureSize).ToList();
            return PartialView(model);
        }

        #endregion

        #region Product reviews
     
        [NopHttpsRequirement(SslRequirement.No)]
        public ActionResult ProductReviews(int productId)
        {
            var product = _productService.GetProductById(productId);
            if (product == null || product.Deleted || !product.Published || !product.AllowCustomerReviews)
                return RedirectToRoute("HomePage");

            var model = new ProductReviewsModel();
            PrepareProductReviewsModel(model, product);
            //only registered users can leave reviews
            if (_workContext.CurrentCustomer.IsGuest() && !_catalogSettings.AllowAnonymousUsersToReviewProduct)
                ModelState.AddModelError("", _localizationService.GetResource("Reviews.OnlyRegisteredUsersCanWriteReviews"));
            //default value
            model.AddProductReview.Rating = _catalogSettings.DefaultProductRatingValue;
            return View(model);
        }

        [HttpPost, ActionName("ProductReviews")]
        [PublicAntiForgery]
        [FormValueRequired("add-review")]
        [CaptchaValidator]
        public ActionResult ProductReviewsAdd(int productId, ProductReviewsModel model, bool captchaValid)
        {
            var product = _productService.GetProductById(productId);
            if (product == null || product.Deleted || !product.Published || !product.AllowCustomerReviews)
                return RedirectToRoute("HomePage");

            //validate CAPTCHA
            if (_captchaSettings.Enabled && _captchaSettings.ShowOnProductReviewPage && !captchaValid)
            {
                ModelState.AddModelError("", _captchaSettings.GetWrongCaptchaMessage(_localizationService));
            }

            if (_workContext.CurrentCustomer.IsGuest() && !_catalogSettings.AllowAnonymousUsersToReviewProduct)
            {
                ModelState.AddModelError("", _localizationService.GetResource("Reviews.OnlyRegisteredUsersCanWriteReviews"));
            }

            if (ModelState.IsValid)
            {
                //save review
                int rating = model.AddProductReview.Rating;
                if (rating < 1 || rating > 5)
                    rating = _catalogSettings.DefaultProductRatingValue;
                bool isApproved = !_catalogSettings.ProductReviewsMustBeApproved;

                var productReview = new ProductReview
                {
                    ProductId = product.Id,
                    CustomerId = _workContext.CurrentCustomer.Id,
                    Title = model.AddProductReview.Title,
                    ReviewText = model.AddProductReview.ReviewText,
                    Rating = rating,
                    HelpfulYesTotal = 0,
                    HelpfulNoTotal = 0,
                    IsApproved = isApproved,
                    CreatedOnUtc = DateTime.UtcNow,
                    StoreId = _storeContext.CurrentStore.Id,
                };
                product.ProductReviews.Add(productReview);
                _productService.UpdateProduct(product);

                //update product totals
                _productService.UpdateProductReviewTotals(product);

                //notify store owner
                if (_catalogSettings.NotifyStoreOwnerAboutNewProductReviews)
                    _workflowMessageService.SendProductReviewNotificationMessage(productReview, _localizationSettings.DefaultAdminLanguageId);

                //activity log
                _customerActivityService.InsertActivity("PublicStore.AddProductReview", _localizationService.GetResource("ActivityLog.PublicStore.AddProductReview"), product.Name);

                //raise event
                if (productReview.IsApproved)
                    _eventPublisher.Publish(new ProductReviewApprovedEvent(productReview));

                PrepareProductReviewsModel(model, product);
                model.AddProductReview.Title = null;
                model.AddProductReview.ReviewText = null;

                model.AddProductReview.SuccessfullyAdded = true;
                if (!isApproved)
                    model.AddProductReview.Result = _localizationService.GetResource("Reviews.SeeAfterApproving");
                else
                    model.AddProductReview.Result = _localizationService.GetResource("Reviews.SuccessfullyAdded");

                return View(model);
            }

            //If we got this far, something failed, redisplay form
            PrepareProductReviewsModel(model, product);
            return View(model);
        }

        [HttpPost]
        public ActionResult SetProductReviewHelpfulness(int productReviewId, bool washelpful)
        {
            var productReview = _productService.GetProductReviewById(productReviewId);
            if (productReview == null)
                throw new ArgumentException("No product review found with the specified id");

            if (_workContext.CurrentCustomer.IsGuest() && !_catalogSettings.AllowAnonymousUsersToReviewProduct)
            {
                return Json(new
                {
                    Result = _localizationService.GetResource("Reviews.Helpfulness.OnlyRegistered"),
                    TotalYes = productReview.HelpfulYesTotal,
                    TotalNo = productReview.HelpfulNoTotal
                });
            }

            //customers aren't allowed to vote for their own reviews
            if (productReview.CustomerId == _workContext.CurrentCustomer.Id)
            {
                return Json(new
                {
                    Result = _localizationService.GetResource("Reviews.Helpfulness.YourOwnReview"),
                    TotalYes = productReview.HelpfulYesTotal,
                    TotalNo = productReview.HelpfulNoTotal
                });
            }

            //delete previous helpfulness
            var prh = productReview.ProductReviewHelpfulnessEntries
                .FirstOrDefault(x => x.CustomerId == _workContext.CurrentCustomer.Id);
            if (prh != null)
            {
                //existing one
                prh.WasHelpful = washelpful;
            }
            else
            {
                //insert new helpfulness
                prh = new ProductReviewHelpfulness
                {
                    ProductReviewId = productReview.Id,
                    CustomerId = _workContext.CurrentCustomer.Id,
                    WasHelpful = washelpful,
                };
                productReview.ProductReviewHelpfulnessEntries.Add(prh);
            }
            _productService.UpdateProduct(productReview.Product);

            //new totals
            productReview.HelpfulYesTotal = productReview.ProductReviewHelpfulnessEntries.Count(x => x.WasHelpful);
            productReview.HelpfulNoTotal = productReview.ProductReviewHelpfulnessEntries.Count(x => !x.WasHelpful);
            _productService.UpdateProduct(productReview.Product);

            return Json(new
            {
                Result = _localizationService.GetResource("Reviews.Helpfulness.SuccessfullyVoted"),
                TotalYes = productReview.HelpfulYesTotal,
                TotalNo = productReview.HelpfulNoTotal
            });
        }

        public ActionResult CustomerProductReviews(int? page)
        {
            if (_workContext.CurrentCustomer.IsGuest())
                return new HttpUnauthorizedResult();

            if (!_catalogSettings.ShowProductReviewsTabOnAccountPage)
            {
                return RedirectToRoute("CustomerInfo");
            }

            var pageSize = _catalogSettings.ProductReviewsPageSizeOnAccountPage;
            int pageIndex = 0;

            if (page > 0)
            {
                pageIndex = page.Value - 1;
            }

            var list = _productService.GetAllProductReviews(_workContext.CurrentCustomer.Id, null,
                            pageIndex: pageIndex, pageSize: pageSize);

            var productReviews = new List<CustomerProductReviewModel>();

            foreach (var review in list)
            {
                var product = review.Product;
                var productReviewModel = new CustomerProductReviewModel
                {
                    Title = review.Title,
                    ProductId = product.Id,
                    ProductName = product.GetLocalized(p => p.Name),
                    ProductSeName = product.GetSeName(),
                    Rating = review.Rating,
                    ReviewText = review.ReviewText,
                    WrittenOnStr =
                        _dateTimeHelper.ConvertToUserTime(product.CreatedOnUtc, DateTimeKind.Utc).ToString("g")
                };

                if (_catalogSettings.ProductReviewsMustBeApproved)
                {
                    productReviewModel.ApprovalStatus = review.IsApproved
                        ? _localizationService.GetResource("Account.CustomerProductReviews.ApprovalStatus.Approved")
                        : _localizationService.GetResource("Account.CustomerProductReviews.ApprovalStatus.Pending");
                }
                productReviews.Add(productReviewModel);
            }

            var pagerModel = new PagerModel
            {
                PageSize = list.PageSize,
                TotalRecords = list.TotalCount,
                PageIndex = list.PageIndex,
                ShowTotalSummary = false,
                RouteActionName = "CustomerProductReviewsPaged",
                UseRouteLinks = true,
                RouteValues = new CustomerProductReviewsModel.CustomerProductReviewsRouteValues { page = pageIndex }
            };

            var model = new CustomerProductReviewsModel
            {
                ProductReviews = productReviews,
                PagerModel = pagerModel
            };

            return View(model);
        }

        [HttpPost, ActionName("ProductDetails")]
        [PublicAntiForgery]
        [FormValueRequired("add-review")]
        [CaptchaValidator]
        public ActionResult CustomerAddProductReview(AddProductReviewModel addReviewModel)
        {
            var product = _productService.GetProductById(addReviewModel.ProductId);
            if (product == null)
                throw new Exception("Invalid product id");
            bool addReviewSuccess = false;
            string message = "";
            try
            {
                int rating = addReviewModel.Rating;
                if (rating < 1 || rating > 5)
                    rating = _catalogSettings.DefaultProductRatingValue;

                var productReview = new ProductReview
                {
                    ProductId = product.Id,
                    CustomerId = _workContext.CurrentCustomer.Id,
                    Title = addReviewModel.Title,
                    ReviewText = addReviewModel.ReviewText,
                    Rating = rating,
                    HelpfulYesTotal = 0,
                    HelpfulNoTotal = 0,
                    IsApproved = false,
                    IsSystemReview = false,
                    CreatedOnUtc = DateTime.UtcNow,
                    StoreId = _storeContext.CurrentStore.Id,
                    OrderInfo = addReviewModel.Title,
                    CustomerDisplayName = addReviewModel.Name,
                };
                product.ProductReviews.Add(productReview);
                _productService.UpdateProduct(product);

                //update product totals
                _productService.UpdateProductReviewTotals(product);

                //notify store owner
                if (_catalogSettings.NotifyStoreOwnerAboutNewProductReviews)
                    _workflowMessageService.SendProductReviewNotificationMessage(productReview, _localizationSettings.DefaultAdminLanguageId);

                //activity log
                _customerActivityService.InsertActivity("PublicStore.AddProductReview", _localizationService.GetResource("ActivityLog.PublicStore.AddProductReview"), product.Name);

                if (!string.IsNullOrEmpty(addReviewModel.PictureIds))
                {
                    var pictureIds = addReviewModel.PictureIds.Split(',');
                    foreach (var idStr in pictureIds)
                    {
                        int pictureId;
                        if (int.TryParse(idStr, out pictureId))
                        {
                            var reviewPicture = new ProductReviewPicture
                            {
                                PictureId = pictureId,
                                ProductReviewId = productReview.Id,
                                DisplayOrder = 0
                            };
                            _productService.InsertProductReviewPicture(reviewPicture);
                        }

                    }
                }
                addReviewSuccess = true;
                message = _localizationService.GetResource("Reviews.SuccessfullyAdded");
            }
            catch(Exception ex)
            {
                message = string.Format("An error occured while add new review: " + ex.Message);
            }
            //prepare the model
            var model = PrepareProductDetailsPageModel(product);
            model.FreeShippingOverXValue = _shippingSettings.FreeShippingOverXEnabled ? (int)_shippingSettings.FreeShippingOverXValue : 50;
            //display "edit" (manage) link
            if (_permissionService.Authorize(StandardPermissionProvider.AccessAdminPanel) && _permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                DisplayEditLink(Url.Action("Edit", "Product", new { id = product.Id, area = "Admin" }));
            model.ProductReviewsModel.AddProductReview = new AddProductReviewModel();
            model.ProductReviewsModel.AddProductReview.SuccessfullyAdded = addReviewSuccess;
            model.ProductReviewsModel.AddProductReview.Result  = message;
            model.ProductReviewsModel.AddProductReview.ProductId  = product.Id;
            return View(model.ProductTemplateViewPath, model);

        }
        #endregion

        #region Email a friend

        [NopHttpsRequirement(SslRequirement.No)]
        public ActionResult ProductEmailAFriend(int productId)
        {
            var product = _productService.GetProductById(productId);
            if (product == null || product.Deleted || !product.Published || !_catalogSettings.EmailAFriendEnabled)
                return RedirectToRoute("HomePage");

            var model = new ProductEmailAFriendModel();
            model.ProductId = product.Id;
            model.ProductName = product.GetLocalized(x => x.Name);
            model.ProductSeName = product.GetSeName();
            model.YourEmailAddress = _workContext.CurrentCustomer.Email;
            model.DisplayCaptcha = _captchaSettings.Enabled && _captchaSettings.ShowOnEmailProductToFriendPage;
            return View(model);
        }

        [HttpPost, ActionName("ProductEmailAFriend")]
        [PublicAntiForgery]
        [FormValueRequired("send-email")]
        [CaptchaValidator]
        public ActionResult ProductEmailAFriendSend(ProductEmailAFriendModel model, bool captchaValid)
        {
            var product = _productService.GetProductById(model.ProductId);
            if (product == null || product.Deleted || !product.Published || !_catalogSettings.EmailAFriendEnabled)
                return RedirectToRoute("HomePage");

            //validate CAPTCHA
            if (_captchaSettings.Enabled && _captchaSettings.ShowOnEmailProductToFriendPage && !captchaValid)
            {
                ModelState.AddModelError("", _captchaSettings.GetWrongCaptchaMessage(_localizationService));
            }

            //check whether the current customer is guest and ia allowed to email a friend
            if (_workContext.CurrentCustomer.IsGuest() && !_catalogSettings.AllowAnonymousUsersToEmailAFriend)
            {
                ModelState.AddModelError("", _localizationService.GetResource("Products.EmailAFriend.OnlyRegisteredUsers"));
            }

            if (ModelState.IsValid)
            {
                //email
                _workflowMessageService.SendProductEmailAFriendMessage(_workContext.CurrentCustomer,
                        _workContext.WorkingLanguage.Id, product,
                        model.YourEmailAddress, model.FriendEmail,
                        Core.Html.HtmlHelper.FormatText(model.PersonalMessage, false, true, false, false, false, false));

                model.ProductId = product.Id;
                model.ProductName = product.GetLocalized(x => x.Name);
                model.ProductSeName = product.GetSeName();

                model.SuccessfullySent = true;
                model.Result = _localizationService.GetResource("Products.EmailAFriend.SuccessfullySent");

                return View(model);
            }

            //If we got this far, something failed, redisplay form
            model.ProductId = product.Id;
            model.ProductName = product.GetLocalized(x => x.Name);
            model.ProductSeName = product.GetSeName();
            model.DisplayCaptcha = _captchaSettings.Enabled && _captchaSettings.ShowOnEmailProductToFriendPage;
            return View(model);
        }

        #endregion

        #region Comparing products

        [HttpPost]
        public ActionResult AddProductToCompareList(int productId)
        {
            var product = _productService.GetProductById(productId);
            if (product == null || product.Deleted || !product.Published)
                return Json(new
                {
                    success = false,
                    message = "No product found with the specified ID"
                });

            if (!_catalogSettings.CompareProductsEnabled)
                return Json(new
                {
                    success = false,
                    message = "Product comparison is disabled"
                });

            _compareProductsService.AddProductToCompareList(productId);

            //activity log
            _customerActivityService.InsertActivity("PublicStore.AddToCompareList", _localizationService.GetResource("ActivityLog.PublicStore.AddToCompareList"), product.Name);

            return Json(new
            {
                success = true,
                message = string.Format(_localizationService.GetResource("Products.ProductHasBeenAddedToCompareList.Link"), Url.RouteUrl("CompareProducts"))
                //use the code below (commented) if you want a customer to be automatically redirected to the compare products page
                //redirect = Url.RouteUrl("CompareProducts"),
            });
        }

        public ActionResult RemoveProductFromCompareList(int productId)
        {
            var product = _productService.GetProductById(productId);
            if (product == null)
                return RedirectToRoute("HomePage");

            if (!_catalogSettings.CompareProductsEnabled)
                return RedirectToRoute("HomePage");

            _compareProductsService.RemoveProductFromCompareList(productId);

            return RedirectToRoute("CompareProducts");
        }

        [NopHttpsRequirement(SslRequirement.No)]
        public ActionResult CompareProducts()
        {
            if (!_catalogSettings.CompareProductsEnabled)
                return RedirectToRoute("HomePage");

            var model = new CompareProductsModel
            {
                IncludeShortDescriptionInCompareProducts = _catalogSettings.IncludeShortDescriptionInCompareProducts,
                IncludeFullDescriptionInCompareProducts = _catalogSettings.IncludeFullDescriptionInCompareProducts,
            };

            var products = _compareProductsService.GetComparedProducts();

            //ACL and store mapping
            products = products.Where(p => _aclService.Authorize(p) && _storeMappingService.Authorize(p)).ToList();
            //availability dates
            products = products.Where(p => p.IsAvailable()).ToList();

            //prepare model
            PrepareProductOverviewModels(products, prepareSpecificationAttributes: true)
                .ToList()
                .ForEach(model.Products.Add);
            return View(model);
        }

        public ActionResult ClearCompareList()
        {
            if (!_catalogSettings.CompareProductsEnabled)
                return RedirectToRoute("HomePage");

            _compareProductsService.ClearCompareProducts();

            return RedirectToRoute("CompareProducts");
        }

        #endregion

        #region HomepageProductCategories
        [ChildActionOnly]
        public ActionResult HomepageProductsByCategory(int categoryId, int? productThumbPictureSize)
        {
            var productsInCategory = _categoryService.GetProductCategoriesByCategoryId(categoryId, 0, 4).ToList();

            var products = new List<Product>();
            foreach (var product in productsInCategory)
            {
                var id = product.Product.Id;
                var productResult = _productService.GetProductById(id);
                products.Add(productResult);
            }


            var model = PrepareProductOverviewModels(products, true, true, productThumbPictureSize).ToList();
            return PartialView(model);
        }

        #endregion
        [HttpGet]
        public ActionResult RandomProduct()
        {
                  
                var randomProduct = _productService.GetRandomProduct();
                var picture = _pictureService.GetPicturesByProductId(randomProduct.Id).First();
                string imageUrl = _pictureService.GetPictureUrl(picture, _mediaSettings.ProductThumbPictureSizeOnProductDetailsPage);
                //if (imageUrl.IndexOf("m_") > 0)
                //{
                //    //change image size
                //    imageUrl = imageUrl.Replace("m_", "md_");
                //}

                var result = new
                {
                    Id = randomProduct.Id,
                    Name = randomProduct.GetLocalized(x => x.Name),
                    ShortDescription = randomProduct.GetLocalized(x => x.ShortDescription),
                    FullDescription = randomProduct.GetLocalized(x => x.FullDescription),
                    SeName = randomProduct.GetSeName(),
                    ImageUrl = imageUrl
                };
                return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult EggProductReviews(string title, int page=0)
        {
            var model = new EggReviewModel();
            model.Reviews = _reviewService.GetReviews(page, 20);
            model.ProductTitle = title;
            model.AvailableSeeMore = model.Reviews.Count >= 20 ? true : false;
            return View(model);
        }
        [HttpGet]
        public ActionResult LoadMoreReviews()
        {
            var model = _reviewService.GetReviews(1, 15);
            return View(model);
        }

        public ActionResult RendingProductCustomFieldPicture(RenderPictureRequest request)
        {
            try
            {
                //var renderResult = string.Format("data:image/png;base64,{0}", _renderPictureService.RendingCustomProductPicture(request));
                //var jsonResult = Json(renderResult, JsonRequestBehavior.AllowGet);
                //jsonResult.MaxJsonLength = int.MaxValue;
                //return jsonResult;
                request.Scale=request.Scale == 0 ? 768 : request.Scale;

                var imageData = _advancedDrawingPictureService.DrawingCustomProductPicture(request);
                return File(imageData, "image/png");
            }
            catch(Exception ex)
            {
            }
            return null;
        }
        [HttpGet]
        public ActionResult RendingPicture(int id, int width=0)
        {
            try
            {
                var imageData = _pictureService.GetPictureBinaryData(id, width);
                return File(imageData, "image/png");
            }
            catch (Exception ex)
            {
            }
            return Content("Picture does not exist!");
        }

        protected virtual string GetProductWithQueryStringImage(ProductAttributeValueModel type , ProductAttributeValueModel color ,List<ProductRenderRequest> request) {
            string result = "";
            var size = _mediaSettings.ProductDetailsPictureSize;
            if(color==null && type==null)
                return _pictureService.GetPictureUrl(0, size);

            ProductAttributeMappingModel productAttributeMappingTypeMapping = _productAttributeService.GetProductAttributeMappingById(type.ProductAttributeMappingId).ToModel();


            if (color == null && type != null)
            {
                var attributes = _productAttributeService.GetProductAttributeMappingsByProductId(productAttributeMappingTypeMapping.ProductId);
                var colorAttrs = attributes.Where(p => p.AttributeControlType == AttributeControlType.ColorSquares);
                foreach (var attr in colorAttrs)
                {
                    var valuesThatShouldBeSelected = _productAttributeParser.ParseConditionValue(attr.ConditionAttributeXml, type.ProductAttributeMappingId, type.Id).ToList();
                    if (valuesThatShouldBeSelected.Any())
                    {
                        color = attr.ProductAttributeValues.FirstOrDefault(p => p.IsPreSelected).ToModel();
                        if(color==null)
                            color = attr.ProductAttributeValues.FirstOrDefault().ToModel();
                    }
                }
            }
            if(color!=null && type==null)
            {
                ProductAttributeMappingModel productAttributeMappingColorModel = _productAttributeService.GetProductAttributeMappingById(color.ProductAttributeMappingId).ToModel();

                var conditionParsed = _productAttributeParser.ParseProductAttributeValues(productAttributeMappingColorModel.ConditionAttributeXml);
                if (conditionParsed.Any())
                {
                    var conditionType = conditionParsed.FirstOrDefault(p => p.ProductAttributeMapping.ProductAttribute.Name.Equals(EggCommon.ProductTypeAttributeValue)).ToModel();
                    if (conditionType != null) type = conditionType;
                }
            }

                
            if (request.Any())
            {
                var renderPictureRequest = new RenderPictureRequest();
               
                var fieldMappings = _productCustomFieldService.GetProductCustomFieldMappingsByProductId(productAttributeMappingTypeMapping.ProductId);
                foreach (var fieldMap in fieldMappings)
                {
                    foreach (var req in request)
                    {
                        if (fieldMap.CustomFieldId == req.Id)
                        {
                            var renderData = new RenderPictureRequest.CustomData();
                            renderData.Id = req.Id;
                            renderData.Text = req.Text;
                            var customValue = fieldMap.ProductCustomFieldValues.FirstOrDefault(p => p.ProductAttributeValueId == type.Id);
                            if (customValue != null)
                            {
                                renderData.ValueId = customValue.Id;
                                renderPictureRequest.Data.Add(renderData);
                            }
                        }
                    }
                }
                renderPictureRequest.AttrValueId = color.Id;
                renderPictureRequest.Scale = size;
                result = this.GetPictureRenderUrl(Url.RouteUrl("RenderProductImage"), renderPictureRequest);
            }
            else
            {
                //set image
                int pictureId = 0;
                if (color != null)
                    pictureId = color.PictureId;

                if (pictureId == 0)
                    if (type != null)
                        pictureId = type.PictureId;

                result = _pictureService.GetPictureUrl(pictureId, size);
            }
            return result;
        }

        protected virtual string GetPictureRenderUrl(string route, RenderPictureRequest request)
        {
            var url = string.Empty;
            try
            {
                url = route + "?";
                url += "AttrValueId=" + request.AttrValueId + '&';
                for (int index = 0; index < request.Data.Count; index++)
                {
                    url += string.Format("Data[{0}].Text={1}&", index, request.Data[index].Text);
                    url += string.Format("Data[{0}].ValueId={1}&", index, request.Data[index].ValueId);
                }
                url += "Scale=" + request.Scale;
            }
            catch (Exception ex)
            {
            }
            return url;
        }

        protected virtual RenderPictureRequest ParseRenderRequest(ProductAttributeValueModel type, ProductAttributeValueModel color, List<ProductRenderRequest> request)
        {
            try
            {
                if (color == null && type == null)
                    return null;

                ProductAttributeMappingModel productAttributeMappingTypeMapping = _productAttributeService.GetProductAttributeMappingById(type.ProductAttributeMappingId).ToModel();

                if (color == null && type != null)
                {
                    var attributes = _productAttributeService.GetProductAttributeMappingsByProductId(productAttributeMappingTypeMapping.ProductId);
                    var colorAttrs = attributes.Where(p => p.AttributeControlTypeId == 40);
                    foreach (var attr in colorAttrs)
                    {
                        var valuesThatShouldBeSelected = _productAttributeParser.ParseConditionValue(attr.ConditionAttributeXml, type.ProductAttributeMappingId, type.Id).ToList();
                        if (valuesThatShouldBeSelected.Any())
                        {
                            color = attr.ProductAttributeValues.FirstOrDefault(p => p.IsPreSelected).ToModel();
                        }
                    }
                }
                if (color != null && type == null)
                {
                    ProductAttributeMappingModel productAttributeMappingColorModel = _productAttributeService.GetProductAttributeMappingById(color.ProductAttributeMappingId).ToModel();

                    var conditionParsed = _productAttributeParser.ParseProductAttributeValues(productAttributeMappingColorModel.ConditionAttributeXml);
                    if (conditionParsed.Any())
                    {
                        var conditionType = conditionParsed.FirstOrDefault(p => p.ProductAttributeMapping.ProductAttribute.Name.Equals(EggCommon.ProductTypeAttributeValue)).ToModel();
                        if (conditionType != null) type = conditionType;
                    }
                }
                if (request.Any())
                {
                    var renderPictureRequest = new RenderPictureRequest();
                    
                    var fieldMappings = _productCustomFieldService.GetProductCustomFieldMappingsByProductId(productAttributeMappingTypeMapping.ProductId);

                    foreach (var fieldMap in fieldMappings)
                    {
                        foreach (var req in request)
                        {
                            if (fieldMap.CustomFieldId == req.Id)
                            {
                                var renderData = new RenderPictureRequest.CustomData();
                                renderData.Id = req.Id;
                                renderData.Text = req.Text;
                                var customValue = fieldMap.ProductCustomFieldValues.FirstOrDefault(p => p.ProductAttributeValueId == type.Id);
                                if (customValue != null)
                                {
                                    renderData.ValueId = customValue.Id;
                                    renderPictureRequest.Data.Add(renderData);
                                }
                            }
                        }
                    }
                    renderPictureRequest.AttrValueId = color.Id;
                    renderPictureRequest.Scale = _mediaSettings.ProductDetailsPictureSize;
                    return renderPictureRequest;
                }
            }
            catch(Exception ex)
            {
            }
            return null;
        }

        protected virtual void OnApplyDiscountCouponByQueryString(string discountcouponcode, out bool isApplied, out Discount appliedDiscount)
        {
            isApplied = false;
            appliedDiscount = null;
            //trim
            if (discountcouponcode != null)
                discountcouponcode = discountcouponcode.Trim();
            if (!String.IsNullOrWhiteSpace(discountcouponcode))
            {
                //we find even hidden records here. this way we can display a user-friendly message if it's expired
                var discount = _discountService.GetDiscountByCouponCode(discountcouponcode, true);
                if (discount != null && discount.RequiresCouponCode)
                {
                    var validationResult = _discountService.ValidateDiscount(discount, _workContext.CurrentCustomer, discountcouponcode);
                    if (validationResult.IsValid)
                    {
                        var couponCodeToValidate = _workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.DiscountCouponCode, _genericAttributeService);
                        if (!string.IsNullOrEmpty(couponCodeToValidate))
                        {
                            if (!couponCodeToValidate.Equals(discountcouponcode))
                            {
                                //valid
                                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, SystemCustomerAttributeNames.DiscountCouponCode, discountcouponcode);
                            }
                        }
                        else
                            //valid
                            _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, SystemCustomerAttributeNames.DiscountCouponCode, discountcouponcode);

                        isApplied = true;
                    }
                    appliedDiscount = discount;
                }
            }
        }
    }
}
