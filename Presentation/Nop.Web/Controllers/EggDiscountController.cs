﻿using Nop.Services.Logging;
using Nop.Core;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Messages;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.Messages;
using Nop.Services.Tasks;
using Nop.Web.Extensions;
using Nop.Web.Models.Common;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Nop.Web.Controllers
{
    public class EggDiscountController : BasePublicController
    {
        #region Fields
        private readonly IWorkContext _workContext;
        private readonly IDiscountService _discountService;
        private readonly ICustomerService _customerService;
        private readonly IEggReMarketingEmailService _eggReMarketingEmailService;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly IStoreContext _storeContext;
        private readonly IQueuedEmailService _queuedEmailService;
        private readonly IEmailAccountService _emailAccountService;
        private readonly EmailAccountSettings _emailAccountSettings;
        private readonly ILogger _logger;



        #endregion

        #region Constructors
        public EggDiscountController(IDiscountService discountService,
            IWorkContext workcontext,
            ICustomerService customerService,
            IEggReMarketingEmailService eggReMarketingEmailService,
            IMessageTemplateService messageTemplateService,
            IStoreContext storeContext,
            IQueuedEmailService queuedEmailService,
            IEmailAccountService emailAccountService,
            EmailAccountSettings emailAccountSettings,
            ILogger logger)
        {
            _discountService = discountService;
            _workContext = workcontext;
            _customerService = customerService;
            _eggReMarketingEmailService = eggReMarketingEmailService;
            _messageTemplateService = messageTemplateService;
            _storeContext = storeContext;
            _queuedEmailService = queuedEmailService;
            _emailAccountService = emailAccountService;
            _emailAccountSettings = emailAccountSettings;
            this._logger = logger;
        }
        #endregion

        [HttpPost]
        // GET: EggDiscount
        public JsonResult PageDetail()
        {
            var discount = _discountService.GetAllDiscounts(null, string.Empty, EggCommon.DiscountProductDetail).FirstOrDefault();
            //_workContext.CurrentCustomer.Email = Request["Email"];
            //_customerService.UpdateCustomer(_workContext.CurrentCustomer);
            //Subscribe(_workContext.CurrentCustomer.Email, discount.CouponCode);
            var eggRemarketingEmailModel = new EggReMarketingEmail();
            eggRemarketingEmailModel.EmailType = EggRemarketingEmailType.GetCoupon.ToString();
            eggRemarketingEmailModel.CreatedOnUtc = DateTime.UtcNow;
            eggRemarketingEmailModel.InStep = 1;
            eggRemarketingEmailModel.IsCompleted = false;
            eggRemarketingEmailModel.EmailAddress = Request["Email"].Trim();
            eggRemarketingEmailModel.MaxStep = 2;
            //eggRemarketingEmailModel.OrderId = 0;
            //eggRemarketingEmailModel.NameRegister = string.Empty;
            var emailTemplate = _messageTemplateService.GetMessageTemplateForEggEmail(EggRemarketingEmailType.GetCoupon.ToString(), eggRemarketingEmailModel.InStep);
            if(emailTemplate!=null)
            {
                var emailAccount = _emailAccountService.GetEmailAccountById(_emailAccountSettings.DefaultEmailAccountId);
                if (emailAccount == null)
                    emailAccount = _emailAccountService.GetAllEmailAccounts().FirstOrDefault();
                if (emailAccount == null)
                    throw new Exception("No email account could be loaded");

                string from;
                string fromName;
                string subject = emailTemplate.Subject;
                string body = emailTemplate.Body;
                //required for some SMTP servers
                from = emailAccount.Email;
                fromName = emailAccount.DisplayName;
                _queuedEmailService.InsertQueuedEmail(new QueuedEmail
                {
                    From = from,
                    FromName = fromName,
                    To = eggRemarketingEmailModel.EmailAddress,
                    ToName = emailAccount.DisplayName,
                    ReplyTo = from,
                    ReplyToName = fromName,
                    Priority = QueuedEmailPriority.High,
                    Subject = subject,
                    Body = body,
                    CreatedOnUtc = DateTime.UtcNow,
                    EmailAccountId = emailAccount.Id
                });
                eggRemarketingEmailModel.SentOnUtc = DateTime.UtcNow;
                eggRemarketingEmailModel.ResentDate = eggRemarketingEmailModel.SentOnUtc.AddHours(72);
            }
            _eggReMarketingEmailService.InsertEggReMarketingEmail(eggRemarketingEmailModel);
            _eggReMarketingEmailService.NextStepOrChangeStatus(eggRemarketingEmailModel);
            return Json(new { DiscountCode = discount.CouponCode }, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult PageCheckout()
        {
            var discount = _discountService.GetAllDiscounts(null, string.Empty, EggCommon.DiscountCheckout).FirstOrDefault();
            _workContext.CurrentCustomer.Email = Request["Email"];
            //_customerService.UpdateCustomer(_workContext.CurrentCustomer);
            //Subscribe(_workContext.CurrentCustomer.Email, discount.CouponCode);
            return Json(new { DiscountCode = discount.CouponCode }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult PageYourname()
        {
            var discount = _discountService.GetAllDiscounts(null, string.Empty, EggCommon.DiscountYourname).FirstOrDefault();
            //_workContext.CurrentCustomer.Email = Request["Email"];
            //_customerService.UpdateCustomer(_workContext.CurrentCustomer);
            //Subscribe(_workContext.CurrentCustomer.Email, discount.CouponCode);
            return Json(new { DiscountCode = discount.CouponCode }, JsonRequestBehavior.AllowGet);
        }

        private void Subscribe(string email, string coupon)
        {
            using (var client = new WebClient())
            {
                var values = new NameValueCollection();
                values["name"] = email;
                values["email"] = email;
                values["coupon"] = coupon;
                values["list"] = System.Configuration.ConfigurationManager.AppSettings["ListEmailSubscribe"].ToString();

                var response = client.UploadValues(System.Configuration.ConfigurationManager.AppSettings["LinkEmailSubscribe"].ToString(), values);

                var responseString = Encoding.Default.GetString(response);
            }
        }
        [HttpPost]
        public ActionResult DisableCouponPopup()
        {
            try
            {
                HttpContext.Session["DisableCouponPopup"] = true;
            }
            catch
            {

            }
            return Json(HttpContext.Session["DisableCouponPopup"]);
        }

        public ActionResult CouponLuckyWheel()
        {
            return View();
        }
        [HttpPost]
        public async Task<JsonResult> ListagramCouponWheel(CouponRequestData requestData)
        {
            try
            {
                string url = "https://www.listagram.com/api/iframe/save-subscriber/";
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(requestData.token);

                    // HTTP POST
                    HttpResponseMessage response = client.PostAsJsonAsync(url, requestData).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var dataJson = await response.Content.ReadAsStringAsync();
                        //ApiCarparkingResultModel apiCarparkingResultModel = JsonConvert.DeserializeObject<ApiCarparkingResultModel>(dataJson);
                        return Json(dataJson);
                    }
                    return null;
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

            }
            var jsonResult = new { success = true, winning_slice = new { number = 7, type = "coupon", value = "30OFF", label = "Coupon 30%" } };
            //return Json(jsonResult, JsonRequestBehavior.AllowGet);
            return Json(jsonResult);

        }

        [HttpPost]
        public JsonResult CouponWheel(CouponRequestData requestData)
        {
            bool isApprovedToGetCoupon = _eggReMarketingEmailService.CheckValidTimetoRegetCoupon(requestData.email);
            if (isApprovedToGetCoupon)
            {
                try
                {
                    var eggRemarketingEmailModel = new EggReMarketingEmail();
                    eggRemarketingEmailModel.EmailType = EggRemarketingEmailType.GetCoupon.ToString();
                    eggRemarketingEmailModel.CreatedOnUtc = DateTime.UtcNow;
                    eggRemarketingEmailModel.InStep = 1;
                    eggRemarketingEmailModel.IsCompleted = false;
                    eggRemarketingEmailModel.EmailAddress = requestData.email;
                    eggRemarketingEmailModel.MaxStep = 2;
                    var emailTemplate = _messageTemplateService.GetMessageTemplateForEggEmail(EggRemarketingEmailType.GetCoupon.ToString(), eggRemarketingEmailModel.InStep);
                    if (emailTemplate != null)
                    {
                        var currentStore = _storeContext.CurrentStore;
                        var emailAccount = _emailAccountService.GetEmailAccountById(_emailAccountSettings.DefaultEmailAccountId);
                        if (emailAccount == null)
                            emailAccount = _emailAccountService.GetAllEmailAccounts().FirstOrDefault();
                        if (emailAccount == null)
                            throw new Exception("No email account could be loaded");

                        string from;
                        string fromName;
                        string subject = emailTemplate.Subject;
                        subject = _eggReMarketingEmailService.Replace(subject, EggEmailPattern.StoreName, currentStore.Name);
                        string body = emailTemplate.Body;
                        body = _eggReMarketingEmailService.Replace(body, EggEmailPattern.StoreName, currentStore.Name);
                        body = _eggReMarketingEmailService.Replace(body, EggEmailPattern.StoreUrl, currentStore.Url);
                        body = _eggReMarketingEmailService.Replace(body, EggEmailPattern.StoreEmail, currentStore.EmailAccountDisplayed);
                        //required for some SMTP servers
                        from = emailAccount.Email;
                        fromName = emailAccount.DisplayName;
                        _queuedEmailService.InsertQueuedEmail(new QueuedEmail
                        {
                            From = from,
                            FromName = fromName,
                            To = eggRemarketingEmailModel.EmailAddress,
                            ToName = emailAccount.DisplayName,
                            ReplyTo = from,
                            ReplyToName = fromName,
                            Priority = QueuedEmailPriority.High,
                            Subject = subject,
                            Body = body,
                            CreatedOnUtc = DateTime.UtcNow,
                            EmailAccountId = emailAccount.Id
                        });
                        eggRemarketingEmailModel.SentOnUtc = DateTime.UtcNow;
                        eggRemarketingEmailModel.ResentDate = eggRemarketingEmailModel.SentOnUtc.AddHours(72);
                    }
                    _eggReMarketingEmailService.InsertEggReMarketingEmail(eggRemarketingEmailModel);
                    _eggReMarketingEmailService.NextStepOrChangeStatus(eggRemarketingEmailModel);
                    var allDiscounts = _discountService.GetAllDiscounts(null, string.Empty, string.Empty).ToList();
                    var discount30 = allDiscounts.Where(p => p.Name.Equals(EggCommon.Discount30Percent)).FirstOrDefault();
                    var discount15 = allDiscounts.Where(p => p.Name.Equals(EggCommon.Discount15Percent)).FirstOrDefault();
                    var discount10 = allDiscounts.Where(p => p.Name.Equals(EggCommon.Discount10Percent)).FirstOrDefault();
                    var discount5 = allDiscounts.Where(p => p.Name.Equals(EggCommon.Discount5Percent)).FirstOrDefault();
                    var coupon5 = new EggCouponResult
                    {
                        success = true,
                        winning_slice = new WinningSlice
                        {
                            number = 11,
                            type = "coupon",
                            value = discount5.CouponCode,
                            label = "Coupon 5%"
                        }
                    };
                    var coupon10 = new EggCouponResult
                    {
                        success = true,
                        winning_slice = new WinningSlice
                        {
                            number = 7,
                            type = "coupon",
                            value = discount10.CouponCode,
                            label = "Coupon 10%"
                        }
                    };
                    var coupon15 = new EggCouponResult
                    {
                        success = true,
                        winning_slice = new WinningSlice
                        {
                            number = 9,
                            type = "coupon",
                            value = discount15.CouponCode,
                            label = "Coupon 15%"
                        }
                    };
                    var coupon30 = new EggCouponResult
                    {
                        success = true,
                        winning_slice = new WinningSlice
                        {
                            number = 9,
                            type = "coupon",
                            value = discount30.CouponCode,
                            label = "Coupon 30%"
                        }
                    };
                    var noPrize = new EggCouponResult
                    {
                        success = true,
                        winning_slice = new WinningSlice
                        {
                            number = 2,
                            type = "noprize",
                            value = "",
                            label = "No Prize"
                        }
                    };
                    Random rnd = new Random();
                    int displayCouponNumber = rnd.Next(1, 100);
                    //50% for coupon 5%
                    if (displayCouponNumber % 2 == 1)
                    {
                        return Json(coupon5);
                    }
                    //10% for noPrize
                    else if (displayCouponNumber % 10 == 0)
                    {
                        return Json(noPrize);
                    }
                    //1% for coupon 30%
                    else if (displayCouponNumber == 22)
                    {
                        return Json(coupon30);
                    }
                    //39% for coupon 10%
                    else
                    {
                        return Json(coupon10);
                    }
                }
                catch (Exception ex)
                {
                    _logger.InsertLog(Core.Domain.Logging.LogLevel.Error, string.Format("Egg Coupon Wheel: general error text {0}", "Can not spin"), string.Format("Egg Coupon wheel failed to spin: Error: {0}", ex.Message), _workContext.CurrentCustomer);
                    var noPrize = new EggCouponResult
                    {
                        success = true,
                        winning_slice = new WinningSlice
                        {
                            number = 2,
                            type = "noprize",
                            value = "",
                            label = "No Prize"
                        }
                    };
                    return Json(noPrize);
                }
            }
            else
            {
                var noResult = new
                {
                    success = false,
                    winning_slice = "",
                    error = "This email address is already used. Please try agian!",
                };
                return Json(noResult);
            }
            
        }
    }
}