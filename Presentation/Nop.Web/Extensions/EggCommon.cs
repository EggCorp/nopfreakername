﻿using Nop.Web.Models.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nop.Web.Extensions
{
    public class EggCommon
    {
        public const string ProductTypeAttributeValue = "Available Products";

        public const int CustomFieldValueProductAttributeId = 4;

        public const int ProductTypeProductAttributeId = 1;

        public const int FullFillSKUProductAttributeId = 5;

        public const string PrefixCustomField = "customField_";

        public const string PrefixAttribute = "product_attribute_";

        public const string RenderQueryStringKey = "custom";

        public const string ApplyCouponQueryStringKey = "coupon";

        public const int MinimumLimitedInStockQuantity = 5;
        public static string PrepareEggProductAttribute(string originalAttribute)
        {
            string result = originalAttribute;
            string[] attributeDetailArg = originalAttribute.Split(new string[] { "<br />" }, StringSplitOptions.None);
            string typeProduct = "";
            string sizeProduct = "";
            string colorProduct = "";
            foreach (string att in attributeDetailArg)
            {
                if (att.IndexOf(string.Format("{0}:", EggCommon.ProductTypeAttributeValue)) == 0)
                {
                    typeProduct = att.Replace(string.Format("{0}:", EggCommon.ProductTypeAttributeValue), string.Empty);
                    typeProduct = typeProduct.Substring(0, typeProduct.IndexOf("[+")).Trim();
                }
                else if (att.IndexOf("Color:") > 0)
                {
                    int indexOfColor = att.IndexOf("Color:");
                    colorProduct = att.Substring(indexOfColor + 6, att.Length - indexOfColor - 6).Trim();
                }
                else if (att.IndexOf("size_") == 0)
                {
                    string nameSizeAtt = att.Substring(0, att.IndexOf(':') + 1);
                    sizeProduct = att.Replace(nameSizeAtt, string.Empty).Trim();
                }
            }
            return string.Format("<div class=\"order_summary__item\">{0}</div><div class=\"order_summary__item\">{1} - {2}</div>", typeProduct, sizeProduct, colorProduct);
        }

        public static string GetProductGAId(string originalAttribute, int productId)
        {
            string result = "";
            string[] attributeDetailArg = originalAttribute.Split(new string[] { "<br />" }, StringSplitOptions.None);
            string typeProduct = "";
            foreach (string att in attributeDetailArg)
            {
                if (att.IndexOf(string.Format("{0}:", EggCommon.ProductTypeAttributeValue)) == 0)
                {
                    typeProduct = att.Replace(string.Format("{0}:", EggCommon.ProductTypeAttributeValue), string.Empty);
                    typeProduct = typeProduct.Substring(0, typeProduct.IndexOf("[+")).Trim();
                }
            }
            result = string.Format("{0}_{1}", typeProduct, productId.ToString());
            result = result.Replace(' ', '_').Replace('-', '_');
            return result;
        }

        public static string GetProductRemarketingId(int productId,int typeId, int colorId)
        {
            string result = "";
            result = string.Format("{0}_{1}{2}",productId, typeId, (colorId >0 ? string.Format("_{0}",colorId):""));
            return result;
        }

        public static decimal GetPrimaryProductPrice(ProductDetailsModel product)
        {
            decimal result = 0;
            var availableProductAttribute = product.ProductAttributes.Where(pa => pa.ProductAttributeId == 1).FirstOrDefault();
            if (availableProductAttribute != null)
            {
                if (availableProductAttribute.Values.Count > 0)
                    result = availableProductAttribute.Values[0].PriceAdjustmentValue;
            }
            return result;
        }
        public static decimal ConvertStringToDecimalPrice(string strPrice)
        {
            if (!string.IsNullOrEmpty(strPrice))
            {
                strPrice = strPrice.Replace("$", "");
            }
            else
            {
                strPrice = "0.00";
            }
            decimal decimalPrice = System.Convert.ToDecimal(strPrice);

            return decimalPrice;
        }

        public static List<string> StringWithDelimiterToList(string origin)
        {
            var result = new List<string>();
            if(!string.IsNullOrEmpty(origin))
            {
                if (origin.IndexOf(',') > 0)
                    result = origin.Split(',').ToList();
                else result.Add(origin);
            }
            return result;
        }
        public static string ListToStringWithDelimiter(List<string> datas)
        {
            StringBuilder result = new StringBuilder();
            foreach(var str in datas)
            {
                if((datas.IndexOf(str)+1) ==datas.Count)
                    result.Append(str);
                else
                    result.Append(string.Format("{0},", str));
            }

            return result.ToString();
        }
        public const string DiscountProductDetail = "Coupon_Product_Detail";
        public const string DiscountCheckout = "Coupon_Checkout";
        public const string DiscountYourname = "Coupon_Yourname";
        public const string PaymentMethodDefault = "Payments.Stripe";
        public const string Discount5Percent = "Coupon_5_Percent";
        public const string Discount10Percent = "Coupon_10_Percent";
        public const string Discount15Percent = "Coupon_15_Percent";
        public const string Discount30Percent = "Coupon_30_Percent";

        public const string AffiliateParam = "affiliate";
        public const string AffiliateIdParam = "affiliateId";

    
        //Link image size
        public const string SizeLadies = "https://www.sunfrog.com/images/ladies-sizing.jpg";
        public const string SizeMenUnisex = "https://www.sunfrog.com/images/mens-tee-sizing.jpg";
        public const string SizeHoodie = "https://www.sunfrog.com/images/hoodie-sizing.jpg";
        public const string SizeSweatShirt = "https://www.sunfrog.com/images/sweatshirt-sizing.jpg";
        public const string SizeYouthTShirt = "https://www.sunfrog.com/images/youth-sizing.jpg";
        public const string SizeUnisexTank = "https://www.sunfrog.com/images/unisex-tank-sizing.jpg";
        public const string SizeLadiesTank = "https://www.sunfrog.com/images/ladies-tank-sizing.jpg";
        public const string SizeUnisexLongSleeve = "https://www.sunfrog.com/images/unisex-longsleave-sizing.jpg";
        public const string SizeLadiesVNeck = "Themes/EggStore/Content/images/ladies-Vneck.png";
        public const string SizeGuysVNeck = "Themes/EggStore/Content/images/guys-Vneck.png";

        public const string FontFolderPath = "/Content/fonts/";

        public const string PrefixColorAttributeValue = "Color";

        public const string PrefixSizeAttributeValue = "size";

        public static string GetFontFolderPath(string fontName)
        {
            return string.Format("{0}{1}",FontFolderPath,fontName);
        }

        public static string PriceToLocaleDecimal(decimal price)
        {
            try
            {
                return String.Format("{0:00.00}", price).Replace(",", ".");
            }
            catch
            {
                return "00.00";
            }
        }

        //payment method name
        public const string StripePaymentMethodName = "Payments.Stripe";
        public const string PaypalPaymentMethodName = "Payments.PayPalStandard";

        public static double GetPercent(int part, int total)
        {
            if (part <= 0 || total <= 0)
                return 0;
            var percent = decimal.Divide(part, total) * 100;
            return (double)Math.Floor(percent * 10) / 10;
        }
        public static int GetPercentRound(int part, int total)
        {
            if (part <= 0 || total <= 0)
                return 0;
            var percent = (int)Math.Round(decimal.Divide(part, total) * 100);
            return percent;
        }
        public static double DevideRoundDown(int x, int y)
        {
            if (x <= 0 || y <= 0)
                return 0;
            var r = decimal.Divide(x, y);
            return (double)Math.Ceiling(r * 10) / 10;

        }
    }
}