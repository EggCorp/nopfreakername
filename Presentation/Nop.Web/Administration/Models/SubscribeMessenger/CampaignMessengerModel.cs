﻿using Nop.Web.Framework.Mvc;
using System;
using System.ComponentModel.DataAnnotations;

namespace Nop.Admin.Models.SubscribeMessenger
{
    public class CampaignMessengerModel : BaseNopEntityModel
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string AttachmentUrl { get; set; }

        [Required]
        public string AttachmentType { get; set; }

        [Required]
        public string TextMessage { get; set; }

        [Required]
        public string Link { get; set; }

        [Required]
        public string CallToAction { get; set; }

        public int PageView { get; set; }

        [Required]
        public string ActionType { get; set; }

        public DateTime CreatedDateUTC { get; set; }
        public int StatusId { get; set; }
        public int TotalAudience { get; set; }

    }
}