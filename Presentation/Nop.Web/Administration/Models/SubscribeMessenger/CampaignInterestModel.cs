﻿using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Nop.Admin.Models.SubscribeMessenger
{
    public class CampaignInterestModel : BaseNopEntityModel
    {
        public CampaignInterestModel()
        {
            AvailableCategories = new List<SelectListItem>();
        }

        [Required]
        public string Name { get; set; }
        public int? CateogryId { get; set; }
        public string CategoryName { get; set; }
        public string Keyword { get; set; }
        [Required]
        public int CampaignMessengerId { get; set; }
        [Required]
        public string Type { get; set; }

        public DateTime CreatedDateUTC { get; set; }

        public IList<SelectListItem> AvailableCategories { get; set; }
    }

    public enum CampaignInterestType
    {
        Category,
        Keyword
    }
}