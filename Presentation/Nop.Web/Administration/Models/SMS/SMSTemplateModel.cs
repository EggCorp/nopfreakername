﻿using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Admin.Models.SMS
{
    public class SMSTemplateModel : BaseNopEntityModel
    {
        [NopResourceDisplayName("TemplateBody")]
        public string TemplateBody { get; set; }

        [NopResourceDisplayName("SMS Type")]
        public string SMSType { get; set; }

        [NopResourceDisplayName("IsDeleted")]
        public bool IsDeleted { get; set; }
    }
}