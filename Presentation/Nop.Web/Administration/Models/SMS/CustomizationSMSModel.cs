﻿using Nop.Web.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nop.Admin.Models.SMS
{
    public class CustomizationSMSModel
    {
        [NopResourceDisplayName("To")]
        public string To { get; set; }

        [NopResourceDisplayName("SMS Type")]
        public int SearchSMSTypeId { get; set; }
        public IList<SelectListItem> AvailablSMSTypes { get; set; }

        [NopResourceDisplayName("Customer Name")]
        public string Name { get; set; }
    }
}