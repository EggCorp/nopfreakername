﻿using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Admin.Models.SMS
{
    public class SMSConfigurationModel : BaseNopEntityModel
    {
        [NopResourceDisplayName("AccountSid")]
        public string AccountSid { get; set; }

        [NopResourceDisplayName("AuthToken")]
        public string AuthToken { get; set; }

        [NopResourceDisplayName("PhoneNumber")]
        public string PhoneNumber { get; set; }

        [NopResourceDisplayName("Qouta")]
        public int Qouta { get; set; }

        [NopResourceDisplayName("SendCount")]
        public int SendCount { get; set; }

    }
}