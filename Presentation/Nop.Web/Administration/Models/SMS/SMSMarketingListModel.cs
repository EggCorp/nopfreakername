﻿using Nop.Web.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nop.Admin.Models.SMS
{
    public class SMSMarketingListModel
    {
        public SMSMarketingListModel()
        {
            AvailablSMSTypes = new List<SelectListItem>();
        }

        [NopResourceDisplayName("Search Number Phone")]
        [AllowHtml]
        public string SearchNumberPhone { get; set; }

        [NopResourceDisplayName("Show Failed")]
        public bool ShowFailed { get; set; }

        [NopResourceDisplayName("Search SMSType")]
        public int SearchSMSTypeId { get; set; }
        public IList<SelectListItem> AvailablSMSTypes { get; set; }

        [NopResourceDisplayName("Search OrderId")]
        public int OrderId { get; set; }

    }
}