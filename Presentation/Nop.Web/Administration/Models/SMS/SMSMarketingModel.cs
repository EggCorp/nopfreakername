﻿using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Admin.Models.SMS
{
    public class SMSMarketingModel : BaseNopEntityModel
    {
        [NopResourceDisplayName("From")]
        public string From { get; set; }

        [NopResourceDisplayName("To")]
        public string To { get; set; }

        [NopResourceDisplayName("SMS Type")]
        public string SMSType { get; set; }

        [NopResourceDisplayName("IsCompleted")]
        public bool IsCompleted { get; set; }

        [NopResourceDisplayName("Body")]
        public string Body { get; set; }

        [NopResourceDisplayName("SentOnUtc")]
        public DateTime SentOnUtc { get; set; }

        [NopResourceDisplayName("OrderId")]
        public int OrderId { get; set; }

    }
}