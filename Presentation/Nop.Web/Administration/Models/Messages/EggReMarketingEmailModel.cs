﻿using FluentValidation.Attributes;
using Nop.Admin.Validators.Messages;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Admin.Models.Messages
{
    [Validator(typeof(EggReMarketingEmailValidator))]
    public class EggReMarketingEmailModel: BaseNopEntityModel
    {
        [NopResourceDisplayName("Email Address")]
        public string EmailAddress { get; set; }

        [NopResourceDisplayName("Email Type")]
        public string EmailType { get; set; }

        [NopResourceDisplayName("MaxStep")]
        public int MaxStep { get; set; }

        [NopResourceDisplayName("InStep")]
        public int InStep { get; set; }

        [NopResourceDisplayName("IsCompleted")]
        public bool IsCompleted { get; set; }

        [NopResourceDisplayName("CreatedOnUtc")]
        public DateTime CreatedOnUtc { get; set; }

        [NopResourceDisplayName("SentOnUtc")]
        public DateTime SentOnUtc { get; set; }

        [NopResourceDisplayName("ResentDate")]
        public DateTime ResentDate { get; set; }

        [NopResourceDisplayName("NameRegister")]
        public string NameRegister { get; set; }

        [NopResourceDisplayName("OrderId")]
        public int OrderId { get; set; }

        [NopResourceDisplayName("CategoryId")]
        public int CategoryId { get; set; }
    }
}