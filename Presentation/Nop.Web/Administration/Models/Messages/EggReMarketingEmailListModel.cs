﻿using Nop.Web.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nop.Admin.Models.Messages
{
    public class EggReMarketingEmailListModel
    {
        public EggReMarketingEmailListModel()
        {
            AvailableEmailTypes = new List<SelectListItem>();
        }

        [NopResourceDisplayName("Search Email Address")]
        [AllowHtml]
        public string SearchEmailAddress { get; set; }
        [AllowHtml]
        [NopResourceDisplayName("Search Registered Name")]
        public string SearchNameRegister { get; set; }

        [NopResourceDisplayName("Search InStep")]
        public int SearchInStep { get; set; }

        [NopResourceDisplayName("Show Completed")]
        public bool ShowCompleted { get; set; }

        [NopResourceDisplayName("Search EmailType")]
        public int SearchEmailTypeId { get; set; }
        public IList<SelectListItem> AvailableEmailTypes { get; set; }

    }
}