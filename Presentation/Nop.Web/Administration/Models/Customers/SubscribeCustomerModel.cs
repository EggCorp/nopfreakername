﻿using Nop.Web.Framework.Mvc;
using System;

namespace Nop.Admin.Models.Customers
{
    public class SubscribeCustomerModel : BaseNopEntityModel
    {
        public int CustomerId { get; set; }
        public int ProductId { get; set; }
        public string UserRef { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}