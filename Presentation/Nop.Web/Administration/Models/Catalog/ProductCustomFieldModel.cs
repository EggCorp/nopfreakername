﻿using System.Collections.Generic;
using System.Web.Mvc;
using FluentValidation.Attributes;
using Nop.Admin.Validators.Catalog;
using Nop.Web.Framework;
using Nop.Web.Framework.Localization;
using Nop.Web.Framework.Mvc;
using Nop.Core.Domain.Customs;

namespace Nop.Admin.Models.Catalog
{
    public partial class ProductCustomFieldModel : BaseNopEntityModel
    {
        public ProductCustomFieldModel()
        {
        }

        [NopResourceDisplayName("Admin.Catalog.Attributes.ProductAttributes.Fields.Name")]
        [AllowHtml]
        public string Name { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Attributes.ProductAttributes.Fields.Description")]
        [AllowHtml]
        public string Description {get;set;}

        [NopResourceDisplayName("Type")]
        public string Type { get; set; }
        public int TypeId { get; set; }

        public IList<SelectListItem> AvailableCustomFieldTypes { get; set; }
    }
}