﻿using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nop.Admin.Models.Catalog
{
    public class DesignModel : BaseNopEntityModel
    {
        public DesignModel()
        {
            AvailableUploadStatus = new List<SelectListItem>();
        }
        public string Name { get; set; }

        public string Description { get; set; }

        public string FileDesign { get; set; }

        public string TemplateUpload { get; set; }

        public int? ProductId { get; set; }

        public string UploadLink { get; set; }

        public int? UploadStatusId { get; set; }
        public IList<SelectListItem> AvailableUploadStatus { get; set; }

    }
}