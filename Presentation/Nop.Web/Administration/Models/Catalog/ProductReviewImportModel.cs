﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Admin.Models.Catalog
{
    public class ProductReviewImportModel
    {
        public int ProductId { get; set; }
        public string ImportUrl { get; set; }
    }
}