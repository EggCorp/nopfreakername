﻿using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nop.Admin.Models.Catalog
{
    public partial class HotTrendingModel : BaseNopEntityModel
    {
        [NopResourceDisplayName("Name")]
        [AllowHtml]
        public string Name { get; set; }

        [NopResourceDisplayName("RedirectUrl")]
        [AllowHtml]
        public string RedirectUrl { get; set; }

        [NopResourceDisplayName("InnerText")]
        [AllowHtml]
        public string InnerText { get; set; }

        [NopResourceDisplayName("DisplayOrder")]
        [AllowHtml]
        public int DisplayOrder { get; set; }

        [NopResourceDisplayName("IsLargeBanner")]
        public bool IsLargeBanner { get; set; }

        [UIHint("Picture")]
        [NopResourceDisplayName("Admin.Catalog.Categories.Fields.Picture")]
        public int PictureId { get; set; }

        [NopResourceDisplayName("IsSellOffBanner")]
        public bool IsSellOffBanner { get; set; }

    }
}