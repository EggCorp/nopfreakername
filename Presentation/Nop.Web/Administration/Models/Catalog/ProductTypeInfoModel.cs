﻿using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nop.Admin.Models.Catalog
{
    public partial class ProductTypeInfoModel : BaseNopEntityModel
    {
        public ProductTypeInfoModel()
        {
            AvailableProductInfoTemplates = new List<SelectListItem>();
            AvailableProductShippingInfoTemplates = new List<SelectListItem>();
            AvailableProductSizingInfoTemplates = new List<SelectListItem>();
        }
        public string TypeName { get; set; }
        public string TypeCode { get; set; }
        public string Description { get; set; }
        public int? ProductInfoTemplateId { get; set; }
        public int? ProductSizingInfoTemplateId { get; set; }
        public int? ProductShippingInfoTemplateId { get; set; }
        public bool IsActive { get; set; }

        public ProductInfoOverviewModel ProductInfoTemplate { get; set; }
        public ProductInfoOverviewModel ProductSizingInfoTemplate { get; set; }
        public ProductInfoOverviewModel ProductShippingInfoTemplate { get; set; }

        public IList<SelectListItem> AvailableProductInfoTemplates { get; set; }
        public IList<SelectListItem> AvailableProductSizingInfoTemplates { get; set; }
        public IList<SelectListItem> AvailableProductShippingInfoTemplates { get; set; }
    }
    public partial class ProductInfoOverviewModel : BaseNopEntityModel
    {
        public string Name { get; set; }
        [AllowHtml]
        public string Content { get; set; }
    }
}