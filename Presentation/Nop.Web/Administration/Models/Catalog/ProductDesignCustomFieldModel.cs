﻿using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Admin.Models.Catalog
{
    public partial class ProductDesignCustomFieldModel : BaseNopEntityModel
    {
        public int DesignCustomFieldId { get; set; }

        public int ProductCustomFieldMappingId { get; set; }

    }
}