﻿using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nop.Admin.Models.Catalog
{
    public partial class DesignCustomFieldModel : BaseNopEntityModel
    {
        public DesignCustomFieldModel()
        {
            AvailableArtEffects = new List<SelectListItem>();
            AvailableArtTypes = new List<SelectListItem>();
            AvailableArtAlignments = new List<SelectListItem>();
            AvailableCustomFields = new List<SelectListItem>();
            ProductDesignCustomField = new ProductDesignCustomFieldModel();
        }

        public int CustomFieldId { get; set; }

        [UIHint("Font")]
        public string Font { get; set; }

        public string FontColor { get; set; }

        public int FontSize { get; set; }

        public double Width { get; set; }

        public double Height { get; set; }

        public string DefaulProductImageUrl { get; set; }

        public double? Bend { get; set; }

        public double? Rotate { get; set; }

        public string EffectColor { get; set; }

        public double? EffectSize { get; set; }

        public double? LeftScale { get; set; }

        public double? RightScale { get; set; }

        public double? Tilt { get; set; }

        public int ArtTypeId { get; set; }

        public int ArtEffectId { get; set; }

        public int ArtAlignmentId { get; set; }

        public string DefaultText { get; set; }

        public IList<SelectListItem> AvailableArtTypes { get; set; }

        public IList<SelectListItem> AvailableArtEffects { get; set; }

        public IList<SelectListItem> AvailableArtAlignments { get; set; }

        public IList<SelectListItem> AvailableCustomFields { get; set; }

        public ProductDesignCustomFieldModel ProductDesignCustomField { get; set; }

    }
}