﻿using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nop.Admin.Models.Common
{
    public partial class ReviewModel : BaseNopEntityModel
    {
        [NopResourceDisplayName("Name")]
        [AllowHtml]
        public string Title { get; set; }

        [NopResourceDisplayName("Review")]
        [AllowHtml]
        public string ReviewText { get; set; }

        [NopResourceDisplayName("Rating")]
        public int Rating { get; set; }

        [NopResourceDisplayName("Created On")]
        public DateTime CreatedOnUtc { get; set; }

    }
}