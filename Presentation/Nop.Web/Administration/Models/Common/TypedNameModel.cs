﻿using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Admin.Models.Common
{
    public class TypedNameModel: BaseNopEntityModel
    {
        public string Name { get; set; }

        public int Count { get; set; }

        public int ProductId { get; set; }

        public int CustomerId { get; set; }

        public string CustomerEmail { get; set; }

        public string ProductUrl { get; set; }

        public string ProductName { get; set; }

        public string IpAddress { get; set; }

        public DateTime CreatedOnTime { get; set; }
    }
}