﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nop.Admin.Models.Common
{
    public partial class TypedNameListModel
    {
        public TypedNameListModel()
        {
            AvailableOrderBy = new List<SelectListItem>();
        }

        [AllowHtml]
        public string Name { get; set; }
        [AllowHtml]
        public int ProductId { get; set; }

        public int OrderById { get; set; }
        public IList<SelectListItem> AvailableOrderBy { get; set; }
    }
}