﻿using System;
using System.Linq;
using System.Web.Mvc;
using Nop.Admin.Extensions;
using Nop.Admin.Models.Catalog;
using Nop.Core.Domain.Catalog;
using Nop.Services.Catalog;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Security;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Kendoui;
using Nop.Web.Framework.Mvc;
using Nop.Core.Domain.Customs;
using Nop.Core.Domain.Common;
using Nop.Services;

namespace Nop.Admin.Controllers
{
    public class ProductCustomFieldController : BaseAdminController
    {
        #region Fields

        private readonly IProductService _productService;
        private readonly IProductAttributeService _productAttributeService;
        private readonly ILanguageService _languageService;
        private readonly ILocalizedEntityService _localizedEntityService;
        private readonly ILocalizationService _localizationService;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly IPermissionService _permissionService;
        private readonly IProductCustomFieldService _productCustomFieldService;

        #endregion Fields

        #region Constructors

        public ProductCustomFieldController(IProductService productService,
            IProductAttributeService productAttributeService,
            ILanguageService languageService,
            ILocalizedEntityService localizedEntityService,
            ILocalizationService localizationService,
            ICustomerActivityService customerActivityService,
            IPermissionService permissionService,
             IProductCustomFieldService productCustomFieldService)
        {
            this._productService = productService;
            this._productAttributeService = productAttributeService;
            this._languageService = languageService;
            this._localizedEntityService = localizedEntityService;
            this._localizationService = localizationService;
            this._customerActivityService = customerActivityService;
            this._permissionService = permissionService;
            this._productCustomFieldService = productCustomFieldService;
        }

        #endregion

        #region Methods

        #region Attribute list / create / edit / delete

        //list
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageAttributes))
                return AccessDeniedView();

            return View();
        }

        [HttpPost]
        public ActionResult List(DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageAttributes))
                return AccessDeniedView();

            var productCustomFields = _productCustomFieldService.GetAllProductCustomFields(command.Page - 1, command.PageSize);
            var gridModel = new DataSourceResult
            {
                Data = productCustomFields.Select(x => x.ToModel()),
                Total = productCustomFields.TotalCount
            };

            return Json(gridModel);
        }

        //create
        public ActionResult Create()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageAttributes))
                return AccessDeniedView();
            var model = new ProductCustomFieldModel();
            model.AvailableCustomFieldTypes = CustomFieldType.text.ToSelectList(true).ToList();
            //locales
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Create(ProductCustomFieldModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageAttributes))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                var productCustomField = model.ToEntity();
                _productCustomFieldService.InsertProductCustomField(productCustomField);

                //activity log
                _customerActivityService.InsertActivity("AddNewProductAttribute", _localizationService.GetResource("ActivityLog.AddNewProductAttribute"), productCustomField.Name);

                SuccessNotification(_localizationService.GetResource("Admin.Catalog.Attributes.ProductAttributes.Added"));

                if (continueEditing)
                {
                    //selected tab
                    SaveSelectedTabName();

                    return RedirectToAction("Edit", new { id = productCustomField.Id });
                }
                return RedirectToAction("List");

            }
            //If we got this far, something failed, redisplay form
            return View(model);
        }

        //edit
        public ActionResult Edit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageAttributes))
                return AccessDeniedView();

            var productCustomField = _productCustomFieldService.GetProductCustomFieldById(id);
            if (productCustomField == null)
                //No product attribute found with the specified id
                return RedirectToAction("List");

            var model = productCustomField.ToModel();
            model.Type = productCustomField.Type.ToString();
            model.AvailableCustomFieldTypes = productCustomField.Type.ToSelectList(true).ToList();
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Edit(ProductCustomFieldModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageAttributes))
                return AccessDeniedView();

            var productCustomField = _productCustomFieldService.GetProductCustomFieldById(model.Id);
            if (productCustomField == null)
                //No product attribute found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                productCustomField = model.ToEntity(productCustomField);
                _productCustomFieldService.UpdateProductCustomField(productCustomField);

                //activity log
                _customerActivityService.InsertActivity("EditProductAttribute", _localizationService.GetResource("ActivityLog.EditProductAttribute"), productCustomField.Name);

                SuccessNotification(_localizationService.GetResource("Admin.Catalog.Attributes.ProductAttributes.Updated"));
                if (continueEditing)
                {
                    //selected tab
                    SaveSelectedTabName();

                    return RedirectToAction("Edit", new { id = productCustomField.Id });
                }
                return RedirectToAction("List");
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        //delete
        [HttpPost]
        public ActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageAttributes))
                return AccessDeniedView();

            var productCustomField = _productCustomFieldService.GetProductCustomFieldById(id);
            if (productCustomField == null)
                //No product attribute found with the specified id
                return RedirectToAction("List");

            _productCustomFieldService.DeleteProductCustomField(productCustomField);

            //activity log
            _customerActivityService.InsertActivity("DeleteProductAttribute", _localizationService.GetResource("ActivityLog.DeleteProductAttribute"), productCustomField.Name);

            SuccessNotification(_localizationService.GetResource("Admin.Catalog.Attributes.ProductAttributes.Deleted"));
            return RedirectToAction("List");
        }

        #endregion

        #endregion
    }
}
