﻿using System;
using System.IO;
using System.Web;
using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Domain.Media;
using Nop.Services.Media;
using Nop.Web.Framework.Security;

namespace Nop.Admin.Controllers
{
    public partial class FontController : BaseAdminController
    {
        private readonly IFontService _fontService;
        public FontController(IFontService fontService)
        {
            this._fontService = fontService;
        }

        [HttpPost]
        //do not validate request token (XSRF)
        [AdminAntiForgery(true)]
        public ActionResult AsyncUpload()
        {
            //we process it distinct ways based on a browser
            //find more info here http://stackoverflow.com/questions/4884920/mvc3-valums-ajax-file-upload
            Stream stream = null;
            var fileName = "";
            var contentType = "";

            if (String.IsNullOrEmpty(Request["qqfile"]))
            {
                // IE
                HttpPostedFileBase httpPostedFile = Request.Files[0];
                if (httpPostedFile == null)
                    throw new ArgumentException("No file uploaded");
                stream = httpPostedFile.InputStream;
                fileName = Path.GetFileName(httpPostedFile.FileName);
                contentType = httpPostedFile.ContentType;
            }
            else
            {
                //Webkit, Mozilla
                stream = Request.InputStream;
                fileName = Request["qqfile"];
            }

            var fileBinary = new byte[stream.Length];
            stream.Read(fileBinary, 0, fileBinary.Length);

            var fileExtension = Path.GetExtension(fileName);
            if (!String.IsNullOrEmpty(fileExtension))
                fileExtension = fileExtension.ToLowerInvariant();
            var uploadResponse = _fontService.UploadFont(fileBinary, fileName, fileExtension);

            //when returning JSON the mime-type must be set to text/plain
            //otherwise some browsers will pop-up a "Save As" dialog.
            return Json(new
            {
                success = uploadResponse.Success,
                message = uploadResponse.Message,
                fontname = uploadResponse.FontName
            },
            MimeTypes.TextPlain);
        }
    }
}
