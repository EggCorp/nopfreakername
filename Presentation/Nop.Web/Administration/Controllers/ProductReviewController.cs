﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Nop.Admin.Models.Catalog;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Services.Catalog;
using Nop.Services.Events;
using Nop.Services.ExportImport;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Kendoui;
using Nop.Web.Framework.Mvc;

namespace Nop.Admin.Controllers
{
    public partial class ProductReviewController : BaseAdminController
    {
        #region Fields

        private readonly IProductService _productService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly ILocalizationService _localizationService;
        private readonly IPermissionService _permissionService;
        private readonly IEventPublisher _eventPublisher;
        private readonly IStoreService _storeService;
        private readonly IStoreContext _storeContext;
        private readonly IWorkContext _workContext;
        private readonly IPictureService _pictureService;
        private readonly IImportManager _importManager;



        #endregion Fields

        #region Constructors

        public ProductReviewController(IProductService productService, 
            IDateTimeHelper dateTimeHelper,
            ILocalizationService localizationService, 
            IPermissionService permissionService,
            IEventPublisher eventPublisher,
            IStoreService storeService,
            IStoreContext storeContext, IWorkContext workContext,
            IPictureService pictureService,
            IImportManager importManager)
        {
            this._productService = productService;
            this._dateTimeHelper = dateTimeHelper;
            this._localizationService = localizationService;
            this._permissionService = permissionService;
            this._eventPublisher = eventPublisher;
            _storeService = storeService;
            _storeContext = storeContext;
            _workContext = workContext;
            _pictureService = pictureService;
            _importManager = importManager;
        }

        #endregion

        #region Utilities

        [NonAction]
        protected virtual void PrepareProductReviewModel(ProductReviewModel model,
            ProductReview productReview, bool excludeProperties, bool formatReviewText)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            if (productReview == null)
                throw new ArgumentNullException("productReview");

            model.Id = productReview.Id;
            model.StoreName = productReview.Store.Name;
            model.ProductId = productReview.ProductId;
            model.ProductName = productReview.Product.Name;
            model.CustomerId = productReview.CustomerId;
            var customer = productReview.Customer;
            model.CustomerInfo = customer.IsRegistered() ? customer.Email : _localizationService.GetResource("Admin.Customers.Guest");
            model.Rating = productReview.Rating;
            model.CreatedOn = _dateTimeHelper.ConvertToUserTime(productReview.CreatedOnUtc, DateTimeKind.Utc);
            model.CustomerDisplayName = productReview.CustomerDisplayName;
            model.OrderInfo = productReview.OrderInfo;
            foreach(var reviewPicture in productReview.ProductReviewPictures)
            {
                var picModel = new ProductReviewModel.ProductReviewPictureModel
                {
                    Id = reviewPicture.Id,
                    PictureId = reviewPicture.PictureId,
                    PictureUrl = reviewPicture.Picture.ImageUrl,
                    DisplayOrder = reviewPicture.DisplayOrder,
                    ProductReviewId = reviewPicture.ProductReviewId,
                };
                model.ProductReviewPictureModels.Add(picModel);
            }
      
            if (!excludeProperties)
            {
                model.Title = productReview.Title;
                if (formatReviewText)
                    model.ReviewText = Core.Html.HtmlHelper.FormatText(productReview.ReviewText, false, true, false, false, false, false);
                else
                    model.ReviewText = productReview.ReviewText;
                model.IsApproved = productReview.IsApproved;
            }
        }

        #endregion

        #region Methods

        //list
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProductReviews))
                return AccessDeniedView();

            var model = new ProductReviewListModel();
            model.AvailableStores.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            var stores = _storeService.GetAllStores().Select(st => new SelectListItem() { Text = st.Name, Value = st.Id.ToString() });
            foreach (var selectListItem in stores)
                model.AvailableStores.Add(selectListItem);
            return View(model);
        }

        [HttpPost]
        public ActionResult List(DataSourceRequest command, ProductReviewListModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProductReviews))
                return AccessDeniedView();

            DateTime? createdOnFromValue = (model.CreatedOnFrom == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.CreatedOnFrom.Value, _dateTimeHelper.CurrentTimeZone);

            DateTime? createdToFromValue = (model.CreatedOnTo == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.CreatedOnTo.Value, _dateTimeHelper.CurrentTimeZone).AddDays(1);

            var productReviews = _productService.GetAllProductReviews(0, null, 
                createdOnFromValue, createdToFromValue, model.SearchText, model.SearchStoreId, model.SearchProductId, command.Page - 1, command.PageSize);
            var gridModel = new DataSourceResult
            {
                Data = productReviews.Select(x =>
                {
                    var m = new ProductReviewModel();
                    PrepareProductReviewModel(m, x, false, true);
                    return m;
                }),
                Total = productReviews.TotalCount
            };

            return Json(gridModel);
        }

        public ActionResult Create()
        {
            var model = new ProductReviewModel();
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Create(ProductReviewModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProductReviews))
                return AccessDeniedView();
            try
            {
                var product = _productService.GetProductById(model.ProductId);
                if (product == null)
                    throw new Exception("Review proudtc could not be loaded");
                ProductReview productReview = new ProductReview();
                productReview.Title = model.Title;
                productReview.ReviewText = model.ReviewText;
                productReview.IsApproved = true;
                productReview.ProductId = product.Id;
                productReview.OrderInfo = model.OrderInfo;
                productReview.Rating = model.Rating;
                productReview.CustomerDisplayName = model.CustomerDisplayName;
                productReview.CreatedOnUtc = DateTime.UtcNow;
                productReview.StoreId = _storeContext.CurrentStore.Id;
                productReview.CustomerId = _workContext.CurrentCustomer.Id;
                productReview.HelpfulNoTotal = 0;
                productReview.HelpfulYesTotal = 0;
                productReview.IsSystemReview = true;

                _productService.InsertProductReview(productReview);

                SuccessNotification("Product review was created successfully");
                return continueEditing ? RedirectToAction("Edit", new { id = productReview.Id }) : RedirectToAction("List");
            }
            catch(Exception ex)
            {
                ErrorNotification("Error create review :"+ ex.Message);
            }
            return View(model);
        }
        //edit
        public ActionResult Edit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProductReviews))
                return AccessDeniedView();

            var productReview = _productService.GetProductReviewById(id);
            if (productReview == null)
                //No product review found with the specified id
                return RedirectToAction("List");

            var model = new ProductReviewModel();
            PrepareProductReviewModel(model, productReview, false, false);
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Edit(ProductReviewModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProductReviews))
                return AccessDeniedView();

            var productReview = _productService.GetProductReviewById(model.Id);
            if (productReview == null)
                //No product review found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                productReview.Title = model.Title;
                productReview.ReviewText = model.ReviewText;
                productReview.IsApproved = model.IsApproved;
                productReview.CustomerDisplayName = model.CustomerDisplayName;
                productReview.OrderInfo = model.OrderInfo;
                TimeZoneInfo timeZone = _dateTimeHelper.CurrentTimeZone;
                productReview.CreatedOnUtc = _dateTimeHelper.ConvertToUtcTime(model.CreatedOn,timeZone);
                _productService.UpdateProduct(productReview.Product);
                
                //update product totals
                _productService.UpdateProductReviewTotals(productReview.Product);

                SuccessNotification(_localizationService.GetResource("Admin.Catalog.ProductReviews.Updated"));
                return continueEditing ? RedirectToAction("Edit", new { id = productReview.Id}) : RedirectToAction("List");
            }


            //If we got this far, something failed, redisplay form
            PrepareProductReviewModel(model, productReview, true, false);
            return View(model);
        }
        

        //import
        public ActionResult Import()
        {
            ProductReviewImportModel model = new ProductReviewImportModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult Import(ProductReviewImportModel model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.ImportUrl))
                    throw new Exception("Import url must be required");
                this._productService.ImportProductReviewFromAliExpress(model.ProductId, model.ImportUrl);
                SuccessNotification(string.Format("Imported review from\"{0}\"",model.ImportUrl));
                return RedirectToAction("List");
            }
            catch (Exception ex)
            {
                ErrorNotification("Error import review: " + ex.Message);
            }
            return View(model);
        }
        //delete
        [HttpPost]
        public ActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProductReviews))
                return AccessDeniedView();

            var productReview = _productService.GetProductReviewById(id);
            if (productReview == null)
                //No product review found with the specified id
                return RedirectToAction("List");

            var product = productReview.Product;
            _productService.DeleteProductReview(productReview);
            //update product totals
            _productService.UpdateProductReviewTotals(product);

            SuccessNotification(_localizationService.GetResource("Admin.Catalog.ProductReviews.Deleted"));
            return RedirectToAction("List");
        }

        [HttpPost]
        public ActionResult ApproveSelected(ICollection<int> selectedIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProductReviews))
                return AccessDeniedView();

            if (selectedIds != null)
            {
                var productReviews = _productService.GetProducReviewsByIds(selectedIds.ToArray());
                foreach (var productReview in productReviews)
                {
                    var previousIsApproved = productReview.IsApproved;
                    productReview.IsApproved = true;
                    _productService.UpdateProduct(productReview.Product);
                    //update product totals
                    _productService.UpdateProductReviewTotals(productReview.Product);


                    //raise event (only if it wasn't approved before)
                    if (!previousIsApproved)
                        _eventPublisher.Publish(new ProductReviewApprovedEvent(productReview));
                }
            }

            return Json(new { Result = true });
        }

        [HttpPost]
        public ActionResult DisapproveSelected(ICollection<int> selectedIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProductReviews))
                return AccessDeniedView();



            if (selectedIds != null)
            {
                var productReviews = _productService.GetProducReviewsByIds(selectedIds.ToArray());
                foreach (var productReview in productReviews)
                {
                    productReview.IsApproved = false;
                    _productService.UpdateProduct(productReview.Product);
                    //update product totals
                    _productService.UpdateProductReviewTotals(productReview.Product);
                }
            }

            return Json(new { Result = true });
        }

        [HttpPost]
        public ActionResult DeleteSelected(ICollection<int> selectedIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProductReviews))
                return AccessDeniedView();

            if (selectedIds != null)
            {
                var productReviews = _productService.GetProducReviewsByIds(selectedIds.ToArray());
                var products = _productService.GetProductsByIds(productReviews.Select(p => p.ProductId).Distinct().ToArray());

                _productService.DeleteProductReviews(productReviews);

                //update product totals
                foreach (var product in products)
                {
                    _productService.UpdateProductReviewTotals(product);
                }
            }

            return Json(new { Result = true });
        }

        public ActionResult ProductSearchAutoComplete(string term)
        {
            const int searchTermMinimumLength = 3;
            if (String.IsNullOrWhiteSpace(term) || term.Length < searchTermMinimumLength)
                return Content("");

            //products
            const int productNumber = 15;
            var products = _productService.SearchProducts(
                keywords: term,
                pageSize: productNumber,
                showHidden: true);

            var result = (from p in products
                select new
                {
                    label = p.Name,
                    productid = p.Id
                })
                .ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public ActionResult ProductReviewPictureAdd(int pictureId, int displayOrder, int productReviewId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReviews))
                return AccessDeniedView();

            if (pictureId == 0)
                throw new ArgumentException();

            var productReview = _productService.GetProductReviewById(productReviewId);
            if (productReview == null)
                throw new ArgumentException("No product Review found with the specified id");


            var picture = _pictureService.GetPictureById(pictureId);
            if (picture == null)
                throw new ArgumentException("No picture found with the specified id");

            _productService.InsertProductReviewPicture(new ProductReviewPicture
            {
                PictureId = pictureId,
                ProductReviewId = productReviewId,
                DisplayOrder = displayOrder,
            });

            return Json(new { Result = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ProductReviewPictureList(DataSourceRequest command, int productReviewId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var productReviewPictures = _productService.GetProductReviewPicturesByProductId(productReviewId);
            var productReviewPicturesModel = productReviewPictures
                .Select(x =>
                {
                    var picture = _pictureService.GetPictureById(x.PictureId);
                    if (picture == null)
                        throw new Exception("Picture cannot be loaded");
                    var m = new ProductReviewModel.ProductReviewPictureModel
                    {
                        Id = x.Id,
                        ProductReviewId = x.ProductReviewId,
                        PictureId = x.PictureId,
                        PictureUrl = _pictureService.GetPictureUrl(picture),
                        DisplayOrder = x.DisplayOrder
                    };
                    return m;
                })
                .ToList();

            var gridModel = new DataSourceResult
            {
                Data = productReviewPicturesModel,
                Total = productReviewPicturesModel.Count
            };

            return Json(gridModel);
        }

        [HttpPost]
        public ActionResult ProductPictureUpdate(ProductReviewModel.ProductReviewPictureModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var productReviewPicture = _productService.GetProductReviewPictureById(model.Id);
            if (productReviewPicture == null)
                throw new ArgumentException("No product picture found with the specified id");

            productReviewPicture.DisplayOrder = model.DisplayOrder;
            _productService.UpdateProductReviewPicture(productReviewPicture);

            return new NullJsonResult();
        }

        [HttpPost]
        public ActionResult ProductReviewPictureDelete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var productPicture = _productService.GetProductReviewPictureById(id);
            if (productPicture == null)
                throw new ArgumentException("No product picture found with the specified id");

            var pictureId = productPicture.PictureId;
            _productService.DeleteProductReviewPicture(productPicture);

            var picture = _pictureService.GetPictureById(pictureId);
            if (picture == null)
                throw new ArgumentException("No picture found with the specified id");
            _pictureService.DeletePicture(picture);

            return new NullJsonResult();
        }
        [HttpPost]
        public ActionResult ImportProductReviews(FormCollection form)
        {
            var productId = int.Parse(form["productId"]);
            try
            {
                var product = _productService.GetProductById(productId);
                if (product == null)
                    throw new Exception("Invalid product id");
                //set page timeout to 5 minutes
                this.Server.ScriptTimeout = 300;
                var file = Request.Files["importexcelfile"];
                if (file != null && file.ContentLength > 0)
                {
                    _importManager.ImportProductReviewsFromXlsx(product.Id,file.InputStream);
                }
                else
                {
                   throw new Exception(_localizationService.GetResource("Admin.Common.UploadFile"));
                }

                SuccessNotification("Product reviews were imported successfully");
            }
            catch (Exception ex)
            {
                ErrorNotification("Error import review: " + ex.Message);
            }
            return RedirectToAction("Edit", "Product", new { id = productId });
        }
        #endregion
    }
}
