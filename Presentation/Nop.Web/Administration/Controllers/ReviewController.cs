﻿using Nop.Admin.Extensions;
using Nop.Admin.Models.Common;
using Nop.Services.Common;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Stores;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Kendoui;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nop.Admin.Controllers
{
    public partial class ReviewController : BaseAdminController
	{
		#region Fields

        private readonly IReviewService _reviewService;
        private readonly ILanguageService _languageService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly ILocalizationService _localizationService;
        private readonly IPermissionService _permissionService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly IStoreService _storeService;
        private readonly IStoreMappingService _storeMappingService;
        
		#endregion

		#region Constructors

        public ReviewController(IReviewService reviewService, 
            ILanguageService languageService,
            IDateTimeHelper dateTimeHelper,
            ILocalizationService localizationService,
            IPermissionService permissionService,
            IUrlRecordService urlRecordService,
            IStoreService storeService, 
            IStoreMappingService storeMappingService)
        {
            this._reviewService = reviewService;
            this._languageService = languageService;
            this._dateTimeHelper = dateTimeHelper;
            this._localizationService = localizationService;
            this._permissionService = permissionService;
            this._urlRecordService = urlRecordService;
            this._storeService = storeService;
            this._storeMappingService = storeMappingService;
		}

        #endregion

        #region Review items

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReviews))
                return AccessDeniedView();

            return View();
        }

        [HttpPost]
        public ActionResult List(DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReviews))
                return AccessDeniedView();

            var reviews = _reviewService.GetReviews(0,1000).ToList();
            var reviewItems = reviews.PagedForCommand(command)
               .Select(x => x.ToModel())
               .ToList();

            var gridModel = new DataSourceResult
            {
                Data = reviewItems,
                Total = reviews.Count()
            };

            return Json(gridModel);
        }

        public ActionResult Create()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReviews))
                return AccessDeniedView();

            var model = new ReviewModel();
         
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Create(ReviewModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReviews))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                var review = model.ToEntity();
                _reviewService.InsertReview(review);
                //locales
                SuccessNotification(_localizationService.GetResource("Admin.Configuration.Stores.Added"));
                return continueEditing ? RedirectToAction("Edit", new { id = review.Id }) : RedirectToAction("List");
            }
            return View(model);
        }

        public ActionResult Edit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReviews))
                return AccessDeniedView();

            var review = _reviewService.GetReviewById(id);
            if (review == null)
                //No review found with the specified id
                return RedirectToAction("List");

            var model = review.ToModel();
          
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        [FormValueRequired("save", "save-continue")]
        public ActionResult Edit(ReviewModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReviews))
                return AccessDeniedView();

            var review = _reviewService.GetReviewById(model.Id);
            if (review == null)
                //No review found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                review.Rating = model.Rating;
                review.ReviewText = model.ReviewText;
                review.Title = model.Title;
                review.CreatedOnUtc = model.CreatedOnUtc;

                _reviewService.UpdateReview(review);

                SuccessNotification(_localizationService.GetResource("Admin.Configuration.Stores.Updated"));
                return continueEditing ? RedirectToAction("Edit", new { id = review.Id }) : RedirectToAction("List");
            }

            //If we got this far, something failed, redisplay form

            //languages
            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReviews))
                return AccessDeniedView();

            var review = _reviewService.GetReviewById(id);
            if (review != null)
                _reviewService.DeleteReview(review);

            return RedirectToAction("List");
        }
        public ActionResult RefreshData()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReviews))
                return AccessDeniedView();
            _reviewService.RefreshReviewData();
            SuccessNotification("All reviews were generated new date and rating!");
            return RedirectToAction("List");
        }
        #endregion

        #region Comments

        #endregion
    }
}