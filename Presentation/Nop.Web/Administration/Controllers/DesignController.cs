﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Nop.Admin.Extensions;
using Nop.Admin.Helpers;
using Nop.Admin.Models.Discounts;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Messages;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Discounts;
using Nop.Services;
using Nop.Services.Catalog;
using Nop.Services.Directory;
using Nop.Services.Messages;
using Nop.Services.Discounts;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Services.Vendors;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Kendoui;
using Nop.Web.Framework.Mvc;
using Nop.Admin.Models.Catalog;
using Nop.Core.Domain.Designs;

namespace Nop.Admin.Controllers
{
    public class DesignController : BaseAdminController
    {
        #region Fields

        private readonly IDiscountService _discountService;
        private readonly ILocalizationService _localizationService;
        private readonly IWebHelper _webHelper;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly ICurrencyService _currencyService;
        private readonly ICategoryService _categoryService;
        private readonly IProductService _productService;
        private readonly CurrencySettings _currencySettings;
        private readonly IPermissionService _permissionService;
        private readonly IWorkContext _workContext;
        private readonly IManufacturerService _manufacturerService;
        private readonly IStoreService _storeService;
        private readonly IVendorService _vendorService;
        private readonly IOrderService _orderService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly ICacheManager _cacheManager;
        private readonly IProductDesignService _productDesignService;

        #endregion

        #region Constructors

        public DesignController(IDiscountService discountService,
            ILocalizationService localizationService,
            ICurrencyService currencyService,
            ICategoryService categoryService,
            IProductService productService,
            IWebHelper webHelper,
            IDateTimeHelper dateTimeHelper,
            ICustomerActivityService customerActivityService,
            CurrencySettings currencySettings,
            IPermissionService permissionService,
            IWorkContext workContext,
            IManufacturerService manufacturerService,
            IStoreService storeService,
            IVendorService vendorService,
            IOrderService orderService,
            IPriceFormatter priceFormatter,
            ICacheManager cacheManager,
            IProductDesignService productDesignService)
        {
            this._discountService = discountService;
            this._localizationService = localizationService;
            this._currencyService = currencyService;
            this._categoryService = categoryService;
            this._productService = productService;
            this._webHelper = webHelper;
            this._dateTimeHelper = dateTimeHelper;
            this._customerActivityService = customerActivityService;
            this._currencySettings = currencySettings;
            this._permissionService = permissionService;
            this._workContext = workContext;
            this._manufacturerService = manufacturerService;
            this._storeService = storeService;
            this._vendorService = vendorService;
            this._orderService = orderService;
            this._priceFormatter = priceFormatter;
            this._cacheManager = cacheManager;
            this._productDesignService = productDesignService;
        }

        #endregion
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }
        public ActionResult List()
        {
            return View();
        }

        public ActionResult GetList(DataSourceRequest command, string name="")
        {
            var items = _productDesignService.SearchDesign(name);
            var gridModel = new DataSourceResult
            {
                Data = items.PagedForCommand(command).Select(x =>
                {
                    DesignModel model = new DesignModel
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Description = x.Description,
                        ProductId = x.ProductId,
                        UploadStatusId = x.UploadStatusId,
                        FileDesign = x.FileDesign,
                        UploadLink = x.UploadLink,
                        TemplateUpload = x.TemplateUpload

                    };
                    return model;
                }),
                Total = items.Count
            };

            return Json(gridModel);
        }
        public ActionResult Edit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var design = _productDesignService.GetDesignById(id);
            if (design == null)
                //No country found with the specified id
                return RedirectToAction("List");

            var model = new DesignModel
            {
                Name= design.Name,
                Description = design.Description,
                Id= design.Id,
                ProductId= design.ProductId==null? 0 : design.ProductId,
                UploadStatusId = design.UploadStatusId== null ? 0 : design.UploadStatusId,
                FileDesign = design.FileDesign,
                UploadLink= design.UploadLink,
                TemplateUpload= design.TemplateUpload
            };
            model.AvailableUploadStatus = UploadStatus.New.ToSelectList(false).ToList();
            model.AvailableUploadStatus.Insert(0, new SelectListItem { Text ="None", Value = "0" });


            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Edit(DesignModel model, bool continueEditing)
        {

            var design = _productDesignService.GetDesignById(model.Id);
            if (design == null)
                //No country found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                design.Name = model.Name;
                design.Description = model.Description;
                design.ProductId = model.ProductId;
                design.UploadStatusId = model.UploadStatusId;
    
                _productDesignService.UpdateDesign(design);
                SuccessNotification(_localizationService.GetResource("This design was Updated succesfully"));

                if (continueEditing)
                    return RedirectToAction("Edit", new { id = design.Id });
                return RedirectToAction("List");
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var design = _productDesignService.GetDesignById(id);
            if (design == null)
                //No country found with the specified id
                return RedirectToAction("List");

            try
            {
                _productDesignService.DeleteDesign(design);

                SuccessNotification("This ReMarketingEmail was Deleted succesfully");
                return RedirectToAction("List");
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("Edit", new { id = design.Id });
            }
        }

    }
}