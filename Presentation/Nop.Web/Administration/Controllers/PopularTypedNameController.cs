﻿using Nop.Admin.Models.Common;
using Nop.Core;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Stores;
using Nop.Web.Framework;
using Nop.Web.Framework.Kendoui;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nop.Admin.Controllers
{
    public class PopularTypedNameController : BaseAdminController
    {
        #region Fields

        private readonly ITypedNameService _typedNameService;
        private readonly ILanguageService _languageService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly ILocalizationService _localizationService;
        private readonly IPermissionService _permissionService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly IStoreService _storeService;
        private readonly IStoreMappingService _storeMappingService;
        private readonly ICustomerService _customerService;
        private readonly IProductService _productService;
        private readonly IStoreContext _storeContext;

        #endregion

        #region Constructors

        public PopularTypedNameController(ITypedNameService typedNameService,
            ILanguageService languageService,
            IDateTimeHelper dateTimeHelper,
            ILocalizationService localizationService,
            IPermissionService permissionService,
            IUrlRecordService urlRecordService,
            IStoreService storeService,
            IStoreMappingService storeMappingService,
            ICustomerService customerService,
            IProductService productService,
                  IStoreContext storeContext)
        {
            this._typedNameService = typedNameService;
            this._languageService = languageService;
            this._dateTimeHelper = dateTimeHelper;
            this._localizationService = localizationService;
            this._permissionService = permissionService;
            this._urlRecordService = urlRecordService;
            this._storeService = storeService;
            this._storeMappingService = storeMappingService;
            this._customerService = customerService;
            this._productService = productService;
            this._storeContext = storeContext;
        }

        #endregion

        #region TypedName items

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();
            var model = new TypedNameListModel();
            model.AvailableOrderBy.Insert(0, new SelectListItem { Text = "Newest", Value = "1" });
            model.AvailableOrderBy.Insert(0, new SelectListItem { Text = "Count", Value = "2" });
            return View(model);
        }

        [HttpPost]
        public ActionResult List(TypedNameListModel model,DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();
            var orderByNewest = model.OrderById == 1;
            var typedNames = _typedNameService.Searchs(!orderByNewest,orderByNewest,model.Name,model.ProductId).ToList();
            var typedNameItems = typedNames.PagedForCommand(command)
               .Select(x => new TypedNameModel {
                   Id = x.Id,
                   Name = x.Name,
                   CustomerId= x.CustomerId,
                   ProductId= x.ProductId,
                   Count =x.Count,
                   CreatedOnTime =x.CreatedOnTime
               })
               .ToList();

            var gridModel = new DataSourceResult
            {
                Data = typedNameItems,
                Total = typedNames.Count()
            };

            return Json(gridModel);
        }

        public ActionResult View(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();
            var typedName = _typedNameService.GetById(id);
            if (typedName == null)
                //No log found with the specified id
                return RedirectToAction("List");
            var customer = _customerService.GetCustomerById(typedName.CustomerId);
            var model = new TypedNameModel
            {
                Id = typedName.Id,
                Name= typedName.Name,
                Count = typedName.Count,
                IpAddress = customer != null ? customer.LastIpAddress : null,
                CustomerId = typedName.CustomerId,
                CustomerEmail = customer != null ? customer.Email : null,
                ProductUrl = null,
                CreatedOnTime=typedName.CreatedOnTime
            };
            var product = _productService.GetProductById(typedName.ProductId);
            if(product!=null)
            {
                model.ProductUrl = string.Format("{0}/{1}", _storeContext.CurrentStore.Url, product.GetSeName());
                model.ProductName = product.Name;
            }
            return View(model);
        }
        #endregion
        #region Comments

        #endregion
    }
}