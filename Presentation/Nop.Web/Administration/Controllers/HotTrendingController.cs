﻿using Nop.Admin.Extensions;
using Nop.Admin.Models.Catalog;
using Nop.Core.Domain.Catalog;
using Nop.Services.Catalog;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Stores;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Kendoui;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nop.Admin.Controllers
{
    public class HotTrendingController : BaseAdminController
    {
        #region Fields

        private readonly IHotTrendingService _hotTrendingService;
        private readonly ILanguageService _languageService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly ILocalizationService _localizationService;
        private readonly IPermissionService _permissionService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly IStoreService _storeService;
        private readonly IStoreMappingService _storeMappingService;

        #endregion

        #region Constructors

        public HotTrendingController(IHotTrendingService hotTrendingService,
            ILanguageService languageService,
            IDateTimeHelper dateTimeHelper,
            ILocalizationService localizationService,
            IPermissionService permissionService,
            IUrlRecordService urlRecordService,
            IStoreService storeService,
            IStoreMappingService storeMappingService)
        {
            this._hotTrendingService = hotTrendingService;
            this._languageService = languageService;
            this._dateTimeHelper = dateTimeHelper;
            this._localizationService = localizationService;
            this._permissionService = permissionService;
            this._urlRecordService = urlRecordService;
            this._storeService = storeService;
            this._storeMappingService = storeMappingService;
        }

        #endregion

        #region HotTrending items

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageHotTrendings))
                return AccessDeniedView();

            return View();
        }

        [HttpPost]
        public ActionResult List(DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageHotTrendings))
                return AccessDeniedView();

            var hotTrendings = _hotTrendingService.GetHotTrendings(0, 1000).ToList();
            var hotTrendingItems = hotTrendings.PagedForCommand(command)
               .Select(x => new HotTrendingModel
               {
                   Id = x.Id,
                   Name = x.Name,
                   RedirectUrl = x.RedirectUrl,
                   DisplayOrder = x.DisplayOrder,
                   InnerText = x.InnerText,
                   IsLargeBanner = x.IsLargeBanner,
                   PictureId = x.PictureId,
                   IsSellOffBanner = x.IsSellOffBanner
               })
               .ToList();

            var gridModel = new DataSourceResult
            {
                Data = hotTrendingItems,
                Total = hotTrendings.Count()
            };

            return Json(gridModel);
        }

        public ActionResult Create()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageHotTrendings))
                return AccessDeniedView();

            var model = new HotTrendingModel();

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Create(HotTrendingModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageHotTrendings))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                var hotTrending = new HotTrending
                {
                    Name = model.Name,
                    InnerText = model.InnerText,
                    RedirectUrl = model.RedirectUrl,
                    DisplayOrder = model.DisplayOrder,
                    PictureId = model.PictureId,
                    IsLargeBanner = model.IsLargeBanner,
                    IsSellOffBanner = model.IsSellOffBanner
                };
                _hotTrendingService.InsertHotTrending(hotTrending);
                //locales
                SuccessNotification(_localizationService.GetResource("Admin.Configuration.Stores.Added"));
                return continueEditing ? RedirectToAction("Edit", new { id = hotTrending.Id }) : RedirectToAction("List");
            }
            return View(model);
        }

        public ActionResult Edit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageHotTrendings))
                return AccessDeniedView();

            var hotTrending = _hotTrendingService.GetHotTrendingById(id);
            if (hotTrending == null)
                //No hotTrending found with the specified id
                return RedirectToAction("List");

            var model = new HotTrendingModel
            {
                Id = hotTrending.Id,
                Name = hotTrending.Name,
                RedirectUrl = hotTrending.RedirectUrl,
                DisplayOrder = hotTrending.DisplayOrder,
                InnerText = hotTrending.InnerText,
                PictureId = hotTrending.PictureId,
                IsLargeBanner = hotTrending.IsLargeBanner,
                IsSellOffBanner = hotTrending.IsSellOffBanner
            };

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        [FormValueRequired("save", "save-continue")]
        public ActionResult Edit(HotTrendingModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageHotTrendings))
                return AccessDeniedView();

            var hotTrending = _hotTrendingService.GetHotTrendingById(model.Id);
            if (hotTrending == null)
                //No hotTrending found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                hotTrending.Name = model.Name;
                hotTrending.InnerText = model.InnerText;
                hotTrending.DisplayOrder = model.DisplayOrder;
                hotTrending.PictureId = model.PictureId;
                hotTrending.RedirectUrl = model.RedirectUrl;
                hotTrending.IsLargeBanner = model.IsLargeBanner;
                hotTrending.IsSellOffBanner = model.IsSellOffBanner;

                _hotTrendingService.UpdateHotTrending(hotTrending);

                SuccessNotification(_localizationService.GetResource("Admin.Configuration.Stores.Updated"));
                return continueEditing ? RedirectToAction("Edit", new { id = hotTrending.Id }) : RedirectToAction("List");
            }

            //If we got this far, something failed, redisplay form

            //languages
            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageHotTrendings))
                return AccessDeniedView();

            var hotTrending = _hotTrendingService.GetHotTrendingById(id);
            if (hotTrending != null)
                _hotTrendingService.DeleteHotTrending(hotTrending);

            return RedirectToAction("List");
        }
        #endregion

    }
}