﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Nop.Admin.Extensions;
using Nop.Admin.Helpers;
using Nop.Admin.Models.Discounts;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Messages;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Discounts;
using Nop.Services;
using Nop.Services.Catalog;
using Nop.Services.Directory;
using Nop.Services.Messages;
using Nop.Services.Discounts;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Services.Vendors;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Kendoui;
using Nop.Web.Framework.Mvc;
using Nop.Admin.Models.Messages;
using Nop.Core.Domain.Common;

namespace Nop.Admin.Controllers
{
    public class EggReMarketingEmailController : BaseAdminController
    {
        #region Fields

        private readonly IDiscountService _discountService;
        private readonly ILocalizationService _localizationService;
        private readonly IWebHelper _webHelper;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly ICurrencyService _currencyService;
        private readonly ICategoryService _categoryService;
        private readonly IProductService _productService;
        private readonly CurrencySettings _currencySettings;
        private readonly IPermissionService _permissionService;
        private readonly IWorkContext _workContext;
        private readonly IManufacturerService _manufacturerService;
        private readonly IStoreService _storeService;
        private readonly IVendorService _vendorService;
        private readonly IOrderService _orderService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly ICacheManager _cacheManager;
        private readonly IEggReMarketingEmailService _eggReMarketingService;

        #endregion

        #region Constructors

        public EggReMarketingEmailController(IDiscountService discountService,
            ILocalizationService localizationService,
            ICurrencyService currencyService,
            ICategoryService categoryService,
            IProductService productService,
            IWebHelper webHelper,
            IDateTimeHelper dateTimeHelper,
            ICustomerActivityService customerActivityService,
            CurrencySettings currencySettings,
            IPermissionService permissionService,
            IWorkContext workContext,
            IManufacturerService manufacturerService,
            IStoreService storeService,
            IVendorService vendorService,
            IOrderService orderService,
            IPriceFormatter priceFormatter,
            ICacheManager cacheManager,
            IEggReMarketingEmailService eggReMarketingService)
        {
            this._discountService = discountService;
            this._localizationService = localizationService;
            this._currencyService = currencyService;
            this._categoryService = categoryService;
            this._productService = productService;
            this._webHelper = webHelper;
            this._dateTimeHelper = dateTimeHelper;
            this._customerActivityService = customerActivityService;
            this._currencySettings = currencySettings;
            this._permissionService = permissionService;
            this._workContext = workContext;
            this._manufacturerService = manufacturerService;
            this._storeService = storeService;
            this._vendorService = vendorService;
            this._orderService = orderService;
            this._priceFormatter = priceFormatter;
            this._cacheManager = cacheManager;
            this._eggReMarketingService = eggReMarketingService;
        }

        #endregion

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }
        public ActionResult List()
        {
            var model = new EggReMarketingEmailListModel();
            model.AvailableEmailTypes = EggRemarketingEmailType.CheckoutSuccess.ToSelectList(false).ToList();
            model.AvailableEmailTypes.Insert(0, new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });

            return View(model);
        }
        [HttpPost]
        public ActionResult EggReMarketingEmailList(EggReMarketingEmailListModel model, DataSourceRequest command)
        {
            
            EggRemarketingEmailType? type = null;
            if (model.SearchEmailTypeId > 0)
                type = (EggRemarketingEmailType)model.SearchEmailTypeId;
            var eggReMarketingEmail = _eggReMarketingService.SearchEggReMarketingEmails(type,
                model.SearchInStep,
                model.SearchNameRegister,
                model.SearchEmailAddress,
                model.ShowCompleted);

            var gridModel = new DataSourceResult
            {
                Data = eggReMarketingEmail.PagedForCommand(command).Select(x =>
                {
                    EggReMarketingEmailModel eggReMarketingEmailModel = x.ToModel();
                    return eggReMarketingEmailModel;
                }),
                Total = eggReMarketingEmail.Count
            };

            return Json(gridModel);
        }

        public ActionResult Edit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCountries))
                return AccessDeniedView();

            var eggReMarketingEmail = _eggReMarketingService.GetEggReMarketingEmailById(id);
            if (eggReMarketingEmail == null)
                //No country found with the specified id
                return RedirectToAction("List");

            var model = eggReMarketingEmail.ToModel();
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Edit(EggReMarketingEmailModel model, bool continueEditing)
        {

            var eggReMarketingEmail = _eggReMarketingService.GetEggReMarketingEmailById(model.Id);
            if (eggReMarketingEmail == null)
                //No country found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                eggReMarketingEmail = model.ToEntity(eggReMarketingEmail);
                _eggReMarketingService.UpdateEggReMarketingEmail(eggReMarketingEmail);
                SuccessNotification(_localizationService.GetResource("This ReMarketingEmail was Updated succesfully"));

                if (continueEditing)
                {
                    //selected tab
                    SaveSelectedTabName();

                    return RedirectToAction("Edit", new { id = eggReMarketingEmail.Id });
                }
                return RedirectToAction("List");
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var eggReMarketingEmail = _eggReMarketingService.GetEggReMarketingEmailById(id);
            if (eggReMarketingEmail == null)
                //No country found with the specified id
                return RedirectToAction("List");

            try
            {
                _eggReMarketingService.DeleteEggReMarketingEmail(eggReMarketingEmail);

                SuccessNotification("This ReMarketingEmail was Deleted succesfully");
                return RedirectToAction("List");
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("Edit", new { id = eggReMarketingEmail.Id });
            }
        }
        [HttpPost]
        public ActionResult DeleteSelected(ICollection<int> selectedIds)
        {

            if (selectedIds != null)
            {
                _eggReMarketingService.DeleteEggReMarketingEmails(_eggReMarketingService.GetEggReMarketingEmailsByIds(selectedIds.ToArray()));
            }
            SuccessNotification("All selected ReMarketingEmail was deleted successfully!");
            return Json(new { Result = true });
        }
        [HttpPost, ActionName("List")]
        [FormValueRequired("delete-completed")]
        public ActionResult DeleteCompleted()
        {
            try
            {
                _eggReMarketingService.DeleteCompletedEggReMarketingEmails();
                SuccessNotification("Completed ReMarketingEmail was deleted successfully!");
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
            }
            return RedirectToAction("List");

        }
    }
}