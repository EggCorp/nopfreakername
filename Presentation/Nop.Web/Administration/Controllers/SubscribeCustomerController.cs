﻿using Nop.Admin.Extensions;
using Nop.Admin.Models.Customers;
using Nop.Services.Customers;
using Nop.Web.Framework;
using Nop.Web.Framework.Kendoui;
using System.Linq;
using System.Web.Mvc;


namespace Nop.Admin.Controllers
{
    public class SubscribeCustomerController : BaseAdminController
    {
        #region Fields
        private readonly ISubscribeCustomerService _subscribeCustomerService;
        #endregion

        #region Constructors
        public SubscribeCustomerController(ISubscribeCustomerService subscribeCustomerService)
        {
            _subscribeCustomerService = subscribeCustomerService;
        }
        #endregion
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SubscribeCustomerList(DataSourceRequest command)
        {
            var subscribeCustomers = _subscribeCustomerService.Search();
            var gridModel = new DataSourceResult
            {
                Data = subscribeCustomers.PagedForCommand(command).Select(x =>
                {
                    SubscribeCustomerModel subscribeCustomerModel = x.ToModel();
                    return subscribeCustomerModel;
                }),
                Total = subscribeCustomers.Count
            };

            return Json(gridModel);
        }
    }
}
