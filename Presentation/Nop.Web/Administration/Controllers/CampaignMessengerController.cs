﻿using Newtonsoft.Json;
using Nop.Admin.Extensions;
using Nop.Admin.Helpers;
using Nop.Admin.Models.SubscribeMessenger;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.SubscribeMessenger;
using Nop.Services.Catalog;
using Nop.Services.Customers;
using Nop.Services.SubscribeMessenger;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Kendoui;
using RestSharp;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Nop.Admin.Controllers
{
    public class CampaignMessengerController : BaseAdminController
    {
        #region Field
        private readonly ICampaignMessengerService _campaignMessengerService;
        private readonly ICategoryService _categoryService;
        private readonly ICacheManager _cacheManager;
        private readonly ISubscribeCustomerService _subscribeCustomerService;
        private readonly IWorkContext _workContext;
        #endregion

        #region Const
        private const string EndPointSendMessage = "/v2.6/me/messages";
        private const string ApiFacebook = "https://graph.facebook.com";
        #endregion

        #region Constructor
        public CampaignMessengerController(ICampaignMessengerService campaignMessengerService, 
                                           ICategoryService categoryService, ICacheManager cacheManager,
                                           ISubscribeCustomerService subscribeCustomerService,
                                           IWorkContext workContext)
        {
            _campaignMessengerService = campaignMessengerService;
            _categoryService = categoryService;
            _cacheManager = cacheManager;
            _subscribeCustomerService = subscribeCustomerService;
            _workContext = workContext;
        }

        #endregion

        #region Campaign Messenger
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CampaignMessengerList(DataSourceRequest command)
        {
            var subscribeCustomers = _campaignMessengerService.Search();
            var gridModel = new DataSourceResult
            {
                Data = subscribeCustomers.PagedForCommand(command).Select(x =>
                {
                    CampaignMessengerModel subscribeCustomerModel = x.ToModel();
                    return subscribeCustomerModel;
                }),
                Total = subscribeCustomers.Count
            };

            return Json(gridModel);
        }

        public ActionResult Create()
        {
            var model = new CampaignMessengerModel();
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        [FormValueRequired("save", "save-continue")]
        [ValidateInput(true)]
        public ActionResult Create(CampaignMessengerModel model, bool continueEditing, FormCollection form)
        {
            if (ModelState.IsValid)
            {
                var campaignMessenger = new CampaignMessenger
                {
                    Name = model.Name,
                    AttachmentType = model.AttachmentType,
                    AttachmentUrl = model.AttachmentUrl,
                    TextMessage = model.TextMessage,
                    Link = model.Link,
                    CallToAction = model.CallToAction,
                    PageView = model.PageView,
                    ActionType = model.ActionType,
                    CreatedDateUTC = DateTime.UtcNow,
                    StatusId = (int)CampaignStatus.New,
                    TotalAudience = 0,
                };

                _campaignMessengerService.Insert(campaignMessenger);

                if (continueEditing)
                {
                    return RedirectToAction("Edit", new { id = campaignMessenger.Id });
                }
                return RedirectToAction("List");
            }

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            CampaignMessenger campaignMessenger = _campaignMessengerService.GetCampaignMessengerById(id);

            CampaignMessengerModel model = new CampaignMessengerModel();
            model.Id = campaignMessenger.Id;
            model.Name = campaignMessenger.Name;
            model.AttachmentType = campaignMessenger.AttachmentType;
            model.AttachmentUrl = campaignMessenger.AttachmentUrl;
            model.TextMessage = campaignMessenger.TextMessage;
            model.Link = campaignMessenger.Link;
            model.CallToAction = campaignMessenger.CallToAction;
            model.PageView = campaignMessenger.PageView;
            model.ActionType = campaignMessenger.ActionType;
            model.StatusId = campaignMessenger.StatusId;
            return View(model);

        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        [FormValueRequired("save", "save-continue")]
        [ValidateInput(false)]
        public ActionResult Edit(CampaignMessengerModel model, bool continueEditing, FormCollection form)
        {
            if (ModelState.IsValid)
            {
                var campaignMessenger = _campaignMessengerService.GetCampaignMessengerById(model.Id);
                campaignMessenger.Name = model.Name;
                campaignMessenger.AttachmentType = model.AttachmentType;
                campaignMessenger.AttachmentUrl = model.AttachmentUrl;
                campaignMessenger.TextMessage = model.TextMessage;
                campaignMessenger.Link = model.Link;
                campaignMessenger.PageView = model.PageView;
                campaignMessenger.ActionType = model.ActionType;
                campaignMessenger.CallToAction = model.CallToAction;
                _campaignMessengerService.UpdateCampaignMessenger(campaignMessenger);
                if (continueEditing)
                {
                    return RedirectToAction("Edit", new { id = campaignMessenger.Id });
                }
                return RedirectToAction("List");
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult CampaignInterestList(DataSourceRequest command, int campaignMessengerId)
        {

            var campaignInterestResult = _campaignMessengerService.GetCampaignInterestByCampaignMessengerId(campaignMessengerId);
            
            var gridModel = new DataSourceResult
            {
                Data = campaignInterestResult.PagedForCommand(command).Select(x =>
                {
                    CampaignInterestModel campaignInterest = x.ToModel();
                    if (campaignInterest.CateogryId.HasValue)
                    {
                        campaignInterest.CategoryName = _categoryService.GetCategoryById(campaignInterest.CateogryId.Value).Name;
                    }
                    return campaignInterest;
                }),
                Total = campaignInterestResult.Count
            };

            return Json(gridModel);
        }

        public ActionResult CreateCampaignInterest(int campaignMessengerId)
        {
            CampaignInterestModel model = new CampaignInterestModel();
            model.CampaignMessengerId = campaignMessengerId;
            model.CateogryId = 0;
            var allCategories = SelectListHelper.GetCategoryList(_categoryService, _cacheManager, true);
            model.AvailableCategories = allCategories;
            model.Type = "Category";
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        [FormValueRequired("save", "save-continue")]
        [ValidateInput(true)]
        public ActionResult CreateCampaignInterest(CampaignInterestModel model, bool continueEditing, FormCollection form)
        {
            if (ModelState.IsValid)
            {
                var campaignInterest = new CampaignInterest
                {
                    Name = model.Name,
                    Type = model.Type,
                    CateogryId = model.Type.Equals(Core.Domain.SubscribeMessenger.CampaignInterestType.Category.ToString()) ? model.CateogryId : null,
                    Keyword = model.Type.Equals(Core.Domain.SubscribeMessenger.CampaignInterestType.Keyword.ToString()) ? model.Keyword : string.Empty,
                    CampaignMessengerId = model.CampaignMessengerId,
                    CreatedDateUTC = DateTime.UtcNow,
                };

                _campaignMessengerService.InsertCampaignInterest(campaignInterest);

                if (continueEditing)
                {
                    return RedirectToAction("EditCampaignInterest", new { id = campaignInterest.Id });
                }
                return RedirectToAction("List");
            }

            return View(model);
        }

        public ActionResult EditCampaignInterest(int id)
        {
            CampaignInterest campaignInterest = _campaignMessengerService.GetCampaignInterestById(id);

            if (campaignInterest != null)
            {
                CampaignInterestModel model = campaignInterest.ToModel();
                var allCategories = SelectListHelper.GetCategoryList(_categoryService, _cacheManager, true);
                model.AvailableCategories = allCategories;

                return View(model);
            }
            else
                return RedirectToAction("List");
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        [FormValueRequired("save", "save-continue")]
        [ValidateInput(false)]
        public ActionResult EditCampaignInterest(CampaignInterestModel model, bool continueEditing, FormCollection form)
        {
            if (ModelState.IsValid)
            {
                CampaignInterest campaignInterest = _campaignMessengerService.GetCampaignInterestById(model.Id);
                campaignInterest.Name = model.Name;
                campaignInterest.Type = model.Type;
                campaignInterest.CateogryId = model.Type.Equals(Core.Domain.SubscribeMessenger.CampaignInterestType.Category.ToString()) ? model.CateogryId : null;
                campaignInterest.Keyword = model.Type.Equals(Core.Domain.SubscribeMessenger.CampaignInterestType.Keyword.ToString()) ? model.Keyword : string.Empty;
                _campaignMessengerService.UpdateCampaignInterest(campaignInterest);

                if (continueEditing)
                {
                    return RedirectToAction("EditCampaignInterest", new { id = campaignInterest.Id });
                }
                return RedirectToAction("Edit", new { id = campaignInterest.CampaignMessengerId });
            }

            return View(model);
        }


        [HttpPost]
        public ActionResult DeleteCampaignInterest(int id)
        {
            try
            {
                CampaignInterest campaignInterest = _campaignMessengerService.GetCampaignInterestById(id);
                int campaignMessengerId = campaignInterest.CampaignMessengerId;

                _campaignMessengerService.DeleteCampaignInterest(campaignInterest);
                SuccessNotification("Success Deleted Campaign Interest");
                return RedirectToAction("Edit", new { Id = campaignMessengerId });
            }
            catch (Exception ex)
            {
                ErrorNotification(ex.Message);
                return RedirectToAction("EditCampaignInterest", new { Id = id });
            }
        }


        [HttpPost, ActionName("Edit")]
        [FormValueRequired("TestCampaignMessenger")]
        public ActionResult TestCampaignMessenger(int id)
        {
            try
            {
                SubscribeCustomer subscribeCustomer = _subscribeCustomerService.GetByCustomer(_workContext.CurrentCustomer.Id);
                if (subscribeCustomer != null)
                {
                    FbSendMessage fbImageMessage = new FbSendMessage();
                    FbSendMessage fbTextMessage = new FbSendMessage();

                    _campaignMessengerService.GenerateFbSendMessageFromCampaignMessengerId(id, out fbImageMessage, out fbTextMessage);
                    fbImageMessage.Recipient.UserRef = subscribeCustomer.UserRef;
                    fbTextMessage.Recipient.UserRef = subscribeCustomer.UserRef;

                    // send Attachment
                    RestClient restClient = new RestClient(ApiFacebook);
                    RestRequest request = new RestRequest(string.Format("{0}?access_token={1}", EndPointSendMessage, subscribeCustomer.Token), Method.POST);
                    request.AddParameter("Application/Json", JsonConvert.SerializeObject(fbImageMessage), ParameterType.RequestBody);

                    request.RequestFormat = DataFormat.Json;

                    var res = restClient.Execute(request);

                    var responResult = JsonConvert.DeserializeObject<FbReponseSendMessage>(res.Content);
                    if (!string.IsNullOrEmpty(responResult.MessageId))
                    {
                        // do nothing                   
                    }
                    else
                    {
                        // sending error
                        throw new Exception(string.Format("Error send Attachment: {0}", JsonConvert.SerializeObject(responResult)));
                    }

                    // send Text Messaage
                    RestRequest requestTextMessage = new RestRequest(string.Format("{0}?access_token={1}", EndPointSendMessage, subscribeCustomer.Token), Method.POST);
                    requestTextMessage.AddParameter("Application/Json", JsonConvert.SerializeObject(fbTextMessage), ParameterType.RequestBody);

                    requestTextMessage.RequestFormat = DataFormat.Json;

                    var resTextMessage = restClient.Execute(requestTextMessage);
                    var responseTextMessageResult = JsonConvert.DeserializeObject<FbReponseSendMessage>(resTextMessage.Content);
                    if (!string.IsNullOrEmpty(responseTextMessageResult.MessageId))
                    {
                        // do nothing                   
                    }
                    else
                    {
                        // sending error
                        throw new Exception(string.Format("Error send Text: {0}", JsonConvert.SerializeObject(responseTextMessageResult)));
                    }
                }
                else
                {
                    throw new Exception("Please subscribe facebook before test campaign!");
                }
                

                SuccessNotification("Success Test Campaign!");
            }
            catch (Exception ex)
            {
                ErrorNotification(ex.Message);
            }
            return RedirectToAction("Edit", new { id = id});
        }

        [HttpPost, ActionName("Edit")]
        [FormValueRequired("runcampaignmessenger")]
        public ActionResult RunCampaignMessenger(int id)
        {
            try
            {
                _campaignMessengerService.UpdateStatusCampaignMessenger(id, CampaignStatus.Processing);
                SuccessNotification("Success Run Campaign!");
            }
            catch (Exception ex)
            {
                ErrorNotification(ex.Message);
            }

            return RedirectToAction("Edit", new { id = id });
        }
        #endregion
    }
}