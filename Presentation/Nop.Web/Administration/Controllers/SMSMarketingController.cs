﻿using Nop.Admin.Models.SMS;
using Nop.Services.SMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Nop.Admin.Extensions;
using Nop.Admin.Helpers;
using Nop.Admin.Models.Discounts;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Messages;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Discounts;
using Nop.Services;
using Nop.Services.Catalog;
using Nop.Services.Directory;
using Nop.Services.Messages;
using Nop.Services.Discounts;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Services.Vendors;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Kendoui;
using Nop.Web.Framework.Mvc;
using Nop.Core.Domain.SMS;
using Nop.Web.Framework.Security;

namespace Nop.Admin.Controllers
{
    public class SMSMarketingController :  BaseAdminController
    {
        #region Fields
        private readonly IDiscountService _discountService;
        private readonly ILocalizationService _localizationService;
        private readonly IWebHelper _webHelper;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly ICurrencyService _currencyService;
        private readonly ICategoryService _categoryService;
        private readonly IProductService _productService;
        private readonly CurrencySettings _currencySettings;
        private readonly IPermissionService _permissionService;
        private readonly IWorkContext _workContext;
        private readonly IManufacturerService _manufacturerService;
        private readonly IStoreService _storeService;
        private readonly IVendorService _vendorService;
        private readonly IOrderService _orderService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly ICacheManager _cacheManager;
        private readonly ISMSMarketingService _sMSMarketingService;
        #endregion

        #region Constructors

        public SMSMarketingController(IDiscountService discountService,
            ILocalizationService localizationService,
            ICurrencyService currencyService,
            ICategoryService categoryService,
            IProductService productService,
            IWebHelper webHelper,
            IDateTimeHelper dateTimeHelper,
            ICustomerActivityService customerActivityService,
            CurrencySettings currencySettings,
            IPermissionService permissionService,
            IWorkContext workContext,
            IManufacturerService manufacturerService,
            IStoreService storeService,
            IVendorService vendorService,
            IOrderService orderService,
            IPriceFormatter priceFormatter,
            ICacheManager cacheManager, 
            ISMSMarketingService sMSMarketingService)
        {
            this._discountService = discountService;
            this._localizationService = localizationService;
            this._currencyService = currencyService;
            this._categoryService = categoryService;
            this._productService = productService;
            this._webHelper = webHelper;
            this._dateTimeHelper = dateTimeHelper;
            this._customerActivityService = customerActivityService;
            this._currencySettings = currencySettings;
            this._permissionService = permissionService;
            this._workContext = workContext;
            this._manufacturerService = manufacturerService;
            this._storeService = storeService;
            this._vendorService = vendorService;
            this._orderService = orderService;
            this._priceFormatter = priceFormatter;
            this._cacheManager = cacheManager;
            this._sMSMarketingService = sMSMarketingService;
        }

        #endregion

        #region Manage SMS
        // GET: SMSMarketing
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            var model = new SMSMarketingListModel();
            model.AvailablSMSTypes = SMSType.Purchase.ToSelectList(false).ToList();
            model.AvailablSMSTypes.Insert(0, new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            return View(model);
        }
        [HttpPost]
        public ActionResult SMSMarketingList(SMSMarketingListModel model, DataSourceRequest command)
        {

            SMSType? type = null;
            if (model.SearchSMSTypeId > 0)
                type = (SMSType)model.SearchSMSTypeId;
            var smsMarketing = _sMSMarketingService.SearchSMSMarketings(type,
                model.SearchNumberPhone,
                model.ShowFailed,
                model.OrderId);

            var gridModel = new DataSourceResult
            {
                Data = smsMarketing.PagedForCommand(command).Select(x =>
                {
                    SMSMarketingModel smsMarketingModel = x.ToModel();
                    return smsMarketingModel;
                }),
                Total = smsMarketing.Count
            };

            return Json(gridModel);
        }

        public ActionResult Edit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCountries))
                return AccessDeniedView();

            var smsMarketing = _sMSMarketingService.GetSMSMarketingById(id);
            if (smsMarketing == null)
                //No country found with the specified id
                return RedirectToAction("List");

            var model = smsMarketing.ToModel();
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Edit(SMSMarketingModel model, bool continueEditing)
        {

            var smsMarketing = _sMSMarketingService.GetSMSMarketingById(model.Id);
            if (smsMarketing == null)
                //No country found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                smsMarketing = model.ToEntity(smsMarketing);
                _sMSMarketingService.UpdateSMSMarketing(smsMarketing);
                SuccessNotification(_localizationService.GetResource("This ReMarketingEmail was Updated succesfully"));

                if (continueEditing)
                {
                    //selected tab
                    SaveSelectedTabName();

                    return RedirectToAction("Edit", new { id = smsMarketing.Id });
                }
                return RedirectToAction("List");
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var smsMarketing = _sMSMarketingService.GetSMSMarketingById(id);
            if (smsMarketing == null)
                //No country found with the specified id
                return RedirectToAction("List");

            try
            {
                _sMSMarketingService.DeleteSMSMarketing(smsMarketing);

                SuccessNotification("This ReMarketingEmail was Deleted succesfully");
                return RedirectToAction("List");
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("Edit", new { id = smsMarketing.Id });
            }
        }
        [HttpPost]
        public ActionResult DeleteSelected(ICollection<int> selectedIds)
        {

            if (selectedIds != null)
            {
                _sMSMarketingService.DeleteSMSMarketings(_sMSMarketingService.GetSMSMarketingsByIds(selectedIds.ToArray()));
            }
            SuccessNotification("All selected ReMarketingEmail was deleted successfully!");
            return Json(new { Result = true });
        }
        [HttpPost, ActionName("List")]
        [FormValueRequired("delete-completed")]
        public ActionResult DeleteCompleted()
        {
            try
            {
                _sMSMarketingService.DeleteCompleted();
                SuccessNotification("Completed SMS was deleted successfully!");
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
            }
            return RedirectToAction("List");

        }
        #endregion
        #region SMS Temp

        public ActionResult SMSTemplate()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SMSTemplateList()
        {

            var smsTemplates = _sMSMarketingService.GetSMSTemplates();

            var gridModel = new DataSourceResult
            {
                Data = smsTemplates.Select(x =>
                {
                    SMSTemplateModel smsTemplateModel = x.ToModel();
                    return smsTemplateModel;
                }),
                Total = smsTemplates.Count
            };
            return Json(gridModel);
        }
        public ActionResult CreateTemplate()
        {
            var model = new SMSTemplateModel();
            return View(model);
        }
        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult CreateTemplate(SMSTemplateModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSMSMarketing))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                var temp = model.ToEntity();
                _sMSMarketingService.CreateSMSTemplate(temp);
                //locales
                SuccessNotification("Created successfully");
                return continueEditing ? RedirectToAction("EditTemplate", new { id = temp.Id }) : RedirectToAction("SMSTemplate");
            }
            return View(model);
        }

        public ActionResult EditTemplate(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSMSMarketing))
                return AccessDeniedView();

            var smsTemplate = _sMSMarketingService.GetSMSTemplateById(id);
            if (smsTemplate == null)
                //No country found with the specified id
                return RedirectToAction("SMSTemplate");

            var model = smsTemplate.ToModel();
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult EditTemplate(SMSTemplateModel model, bool continueEditing)
        {

            var smsMarketing = _sMSMarketingService.GetSMSTemplateById(model.Id);
            if (smsMarketing == null)
                //No country found with the specified id
                return RedirectToAction("SMSTemplate");

            if (ModelState.IsValid)
            {
                smsMarketing = model.ToEntity(smsMarketing);
                _sMSMarketingService.UpdateSMSTemplate(smsMarketing);
                SuccessNotification(_localizationService.GetResource("This SMS Template was Updated succesfully"));

                if (continueEditing)
                {
                    //selected tab
                    SaveSelectedTabName();

                    return RedirectToAction("EditTemplate", new { id = smsMarketing.Id });
                }
                return RedirectToAction("SMSTemplate");
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult DeleteSMSTemplate(int id)
        {
            var smsMarketing = _sMSMarketingService.GetSMSTemplateById(id);
            if (smsMarketing == null)
                //No country found with the specified id
                return RedirectToAction("SMSTemplate");

            try
            {
                _sMSMarketingService.DeleteSMSTemplate(smsMarketing);

                SuccessNotification("This SMS Template was Deleted succesfully");
                return RedirectToAction("SMSTemplate");
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("EditTemplate", new { id = smsMarketing.Id });
            }
        }

        public ActionResult SendCustomizationSMS()
        {
            var model = new CustomizationSMSModel();
            model.AvailablSMSTypes = SMSType.Purchase.ToSelectList(true).ToList();
            return View(model);
        }

        [HttpPost]
        [AdminAntiForgery]
        public ActionResult SendCustomizationSMS(CustomizationSMSModel model)
        {
            SMSType smsType = (SMSType)model.SearchSMSTypeId;
            _sMSMarketingService.SendCustomizationSMS(model.To, model.Name, smsType);
            SuccessNotification("This SMS was send successfully!");
            ViewBag.RefreshPage = true;
            model.AvailablSMSTypes = SMSType.Purchase.ToSelectList(true).ToList();
            return View(model);
        }
        #endregion

        #region SMS config

        public ActionResult SMSConfiguration()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SMSConfigurationList()
        {

            var config = _sMSMarketingService.GetSMSConfigurations();

            var gridModel = new DataSourceResult
            {
                Data = config.Select(x =>
                {
                    SMSConfigurationModel smsTemplateModel = x.ToModel();
                    return smsTemplateModel;
                }),
                Total = config.Count
            };
            return Json(gridModel);
        }
        public ActionResult CreateConfiguration()
        {
            var model = new SMSConfigurationModel();
            return View(model);
        }
        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult CreateConfiguration(SMSConfigurationModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSMSMarketing))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                var temp = model.ToEntity();
                _sMSMarketingService.CreateSMSConfiguration(temp);
                //locales
                SuccessNotification("Created successfully");
                return continueEditing ? RedirectToAction("EditConfiguration", new { id = temp.Id }) : RedirectToAction("SMSConfiguration");
            }
            return View(model);
        }

        public ActionResult EditConfiguration(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSMSMarketing))
                return AccessDeniedView();

            var smsTemplate = _sMSMarketingService.GetSMSConfigurationById(id);
            if (smsTemplate == null)
                //No country found with the specified id
                return RedirectToAction("SMSConfiguration");

            var model = smsTemplate.ToModel();
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult EditConfiguration(SMSConfigurationModel model, bool continueEditing)
        {

            var smsMarketing = _sMSMarketingService.GetSMSConfigurationById(model.Id);
            if (smsMarketing == null)
                //No country found with the specified id
                return RedirectToAction("SMSConfiguration");

            if (ModelState.IsValid)
            {
                smsMarketing = model.ToEntity(smsMarketing);
                _sMSMarketingService.UpdateSMSConfiguration(smsMarketing);
                SuccessNotification(_localizationService.GetResource("This SMS Configuration was Updated succesfully"));

                if (continueEditing)
                {
                    //selected tab
                    SaveSelectedTabName();

                    return RedirectToAction("EditConfiguration", new { id = smsMarketing.Id });
                }
                return RedirectToAction("SMSConfiguration");
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult DeleteConfiguration(int id)
        {
            var smsMarketing = _sMSMarketingService.GetSMSConfigurationById(id);
            if (smsMarketing == null)
                //No country found with the specified id
                return RedirectToAction("SMSConfiguration");

            try
            {
                _sMSMarketingService.DeleteSMSConfiguration(smsMarketing);

                SuccessNotification("This SMS Configuration was Deleted succesfully");
                return RedirectToAction("SMSConfiguration");
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("EditConfiguration", new { id = smsMarketing.Id });
            }
        }
        #endregion
    }
}