﻿using Nop.Admin.Models.Catalog;
using Nop.Core.Domain.Catalog;
using Nop.Services.Catalog;
using Nop.Services.Localization;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Kendoui;
using System.Linq;
using System.Web.Mvc;

namespace Nop.Admin.Controllers
{
    public class SizingShippingInfoController : BaseAdminController
    {
        #region Fields

        private readonly IProductService _productService;
        private readonly IProductAttributeService _productAttributeService;
        private readonly ILanguageService _languageService;
        private readonly ILocalizedEntityService _localizedEntityService;
        private readonly ILocalizationService _localizationService;
        private readonly IPermissionService _permissionService;
        private readonly IProductTypeService _productTypeService;

        #endregion Fields

        #region Constructors

        public SizingShippingInfoController(IProductService productService,
            IProductAttributeService productAttributeService,
            ILanguageService languageService,
            ILocalizedEntityService localizedEntityService,
            ILocalizationService localizationService,
            IPermissionService permissionService,
            IProductTypeService productTypeService)
        {
            this._productService = productService;
            this._productAttributeService = productAttributeService;
            this._languageService = languageService;
            this._localizedEntityService = localizedEntityService;
            this._localizationService = localizationService;
            this._permissionService = permissionService;
            this._productTypeService = productTypeService;
        }

        #endregion

        #region Methods

        #region Attribute list / create / edit / delete

        //list
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            return View();
        }

        [HttpPost]
        public ActionResult List(DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var datas = _productTypeService.GetAllProductTypes(command.Page - 1, command.PageSize);
            var gridModel = new DataSourceResult
            {
                Data = datas.Select(x => new ProductTypeInfoModel {
                    Id = x.Id,
                    TypeName =x.TypeName,
                    TypeCode = x.TypeCode,
                    Description = x.Description,
                    ProductInfoTemplateId = x.ProductInfoTemplateId,
                    ProductShippingInfoTemplateId= x.ProductShippingInfoTemplateId,
                    ProductSizingInfoTemplateId=x.ProductSizingInfoTemplateId,
                    IsActive = x.IsActive
                }),
                Total = datas.TotalCount
            };
            return Json(gridModel);
        }

        //create
        public ActionResult Create()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();
            var model = new ProductTypeInfoModel();
            var productInfoTemplates = _productTypeService.GetAllProductInfoTemplate();
            model.AvailableProductInfoTemplates = productInfoTemplates.Select(p => new SelectListItem
            {
                Text= p.Name,
                Value=p.Id.ToString(),
                Selected = false
            }).ToList();
            var productSizingTemplates = _productTypeService.GetAllProductSizingInfoTemplate();
            model.AvailableProductSizingInfoTemplates = productSizingTemplates.Select(p => new SelectListItem
            {
                Text = p.Name,
                Value = p.Id.ToString(),
                Selected = false
            }).ToList();
            var productShippingTemplates = _productTypeService.GetAllProductShippingInfoTemplate();
            model.AvailableProductShippingInfoTemplates = productShippingTemplates.Select(p => new SelectListItem
            {
                Text = p.Name,
                Value = p.Id.ToString(),
                Selected = false
            }).ToList();
            //locales
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Create(ProductTypeInfoModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                ProductTypeInfo info = new ProductTypeInfo
                {
                    TypeName = model.TypeName,
                    TypeCode = SeoExtensions.GetSeName(model.TypeName),
                    Description = model.Description,
                    ProductSizingInfoTemplateId = model.ProductSizingInfoTemplateId,
                    ProductInfoTemplateId = model.ProductInfoTemplateId,
                    ProductShippingInfoTemplateId = model.ProductShippingInfoTemplateId,
                    IsActive = true
                };
                _productTypeService.InsertProductTypeInfo(info);

            
                SuccessNotification(_localizationService.GetResource("Admin.Catalog.Attributes.ProductAttributes.Added"));

                if (continueEditing)
                {
                    //selected tab
                    SaveSelectedTabName();

                    return RedirectToAction("Edit", new { id = info.Id });
                }
                return RedirectToAction("List");

            }
            //If we got this far, something failed, redisplay form
            return View(model);
        }

        //edit
        public ActionResult Edit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var info = _productTypeService.GetProductTypeById(id);
            if (info == null)
                //No product attribute found with the specified id
                return RedirectToAction("List");

            var model = new ProductTypeInfoModel
            {
                Id = info.Id,
                TypeName = info.TypeName,
                TypeCode = info.TypeCode,
                Description = info.Description,
                ProductInfoTemplateId = info.ProductInfoTemplateId,
                ProductShippingInfoTemplateId = info.ProductShippingInfoTemplateId,
                ProductSizingInfoTemplateId = info.ProductSizingInfoTemplateId,
                IsActive = info.IsActive
            };
            var productInfoTemplates = _productTypeService.GetAllProductInfoTemplate();
            model.AvailableProductInfoTemplates = productInfoTemplates.Select(p => new SelectListItem
            {
                Text = p.Name,
                Value = p.Id.ToString(),
                Selected = false
            }).ToList();
            var currentInfoTemp = productInfoTemplates.FirstOrDefault(p => p.Id == info.ProductInfoTemplateId);

            var productSizingTemplates = _productTypeService.GetAllProductSizingInfoTemplate();
            model.AvailableProductSizingInfoTemplates = productSizingTemplates.Select(p => new SelectListItem
            {
                Text = p.Name,
                Value = p.Id.ToString(),
                Selected = false
            }).ToList();
            var currentSizingTemp = productSizingTemplates.FirstOrDefault(p => p.Id == info.ProductSizingInfoTemplateId);

            var productShippingTemplates = _productTypeService.GetAllProductShippingInfoTemplate();
            model.AvailableProductShippingInfoTemplates = productShippingTemplates.Select(p => new SelectListItem
            {
                Text = p.Name,
                Value = p.Id.ToString(),
                Selected = false
            }).ToList();
            var currentShippingTemp = productShippingTemplates.FirstOrDefault(p => p.Id == info.ProductShippingInfoTemplateId);

            if (currentInfoTemp!=null)
            {
                model.ProductInfoTemplate = new ProductInfoOverviewModel
                {
                    Id = currentInfoTemp.Id,
                    Name = currentInfoTemp.Name,
                    Content = currentInfoTemp.Content,
                };
            }
            if(currentShippingTemp!=null)
            {
                model.ProductShippingInfoTemplate = new ProductInfoOverviewModel
                {
                    Id = currentShippingTemp.Id,
                    Name = currentShippingTemp.Name,
                    Content = currentShippingTemp.Content,
                };
            }
            if(currentSizingTemp!=null)
            {
                model.ProductSizingInfoTemplate = new ProductInfoOverviewModel
                {
                    Id = currentSizingTemp.Id,
                    Name = currentSizingTemp.Name,
                    Content = currentSizingTemp.Content,
                };
            }
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Edit(ProductTypeInfoModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var info = _productTypeService.GetProductTypeById(model.Id);
            if (info == null)
                //No product attribute found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                info.TypeName = model.TypeName;
                info.TypeCode = SeoExtensions.GetSeName(model.TypeName);
                info.Description = model.Description;
                info.ProductSizingInfoTemplateId = model.ProductSizingInfoTemplateId;
                info.ProductInfoTemplateId = model.ProductInfoTemplateId;
                info.ProductShippingInfoTemplateId = model.ProductShippingInfoTemplateId;
                info.IsActive = model.IsActive;
                _productTypeService.UpdateProductTypeInfo(info);
             
                SuccessNotification(_localizationService.GetResource("Admin.Catalog.Attributes.ProductAttributes.Updated"));
                if (continueEditing)
                {
                    //selected tab
                    SaveSelectedTabName();

                    return RedirectToAction("Edit", new { id = info.Id });
                }
                return RedirectToAction("List");
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        //delete
        [HttpPost]
        public ActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var info = _productTypeService.GetProductTypeById(id);
            if (info == null)
                //No product attribute found with the specified id
                return RedirectToAction("List");

            _productTypeService.DeleteProductType(info);

            SuccessNotification(_localizationService.GetResource("Admin.Catalog.Attributes.ProductAttributes.Deleted"));
            return RedirectToAction("List");
        }

        #endregion

        #region sizing info

        public ActionResult ListSizingTemplate()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            return View();
        }

        [HttpPost]
        public ActionResult ListSizingTemplate(DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var datas = _productTypeService.GetAllProductSizingInfoTemplate(command.Page - 1, command.PageSize);
            var gridModel = new DataSourceResult
            {
                Data = datas.Select(x => new ProductInfoOverviewModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    Content = x.Content,
                }),
                Total = datas.TotalCount
            };
            return Json(gridModel);
        }

        //create
        public ActionResult CreateSizingTemplate()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();
            var model = new ProductInfoOverviewModel();
            //locales
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult CreateSizingTemplate(ProductInfoOverviewModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                ProductSizingInfoTemplate info = new ProductSizingInfoTemplate
                {
                    Name = model.Name,
                    Content = model.Content,
                };
                _productTypeService.InsertProductSizingInfoTemplate(info);


                SuccessNotification(_localizationService.GetResource("Admin.Catalog.Attributes.ProductAttributes.Added"));

                if (continueEditing)
                {
                    //selected tab
                    SaveSelectedTabName();

                    return RedirectToAction("EditSizingTemplate", new { id = info.Id });
                }
                return RedirectToAction("ListSizingTemplate");

            }
            //If we got this far, something failed, redisplay form
            return View(model);
        }

        //edit
        public ActionResult EditSizingTemplate(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var info = _productTypeService.GetProductSizingInfoTemplateById(id);
            if (info == null)
                //No product attribute found with the specified id
                return RedirectToAction("ListSizingTemplate");

            var model = new ProductInfoOverviewModel
            {
                Id = info.Id,
                Name = info.Name,
                Content = info.Content,
            };
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult EditSizingTemplate(ProductInfoOverviewModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var info = _productTypeService.GetProductSizingInfoTemplateById(model.Id);
            if (info == null)
                //No product attribute found with the specified id
                return RedirectToAction("ListSizingTemplate");

            if (ModelState.IsValid)
            {
                info.Name = model.Name;
                info.Content = model.Content;
                _productTypeService.UpdateProductSizingInfoTemplate(info);

                SuccessNotification(_localizationService.GetResource("Admin.Catalog.Attributes.ProductAttributes.Updated"));
                if (continueEditing)
                {
                    //selected tab
                    SaveSelectedTabName();

                    return RedirectToAction("EditSizingTemplate", new { id = info.Id });
                }
                return RedirectToAction("ListSizingTemplate");
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        //delete
        [HttpPost]
        public ActionResult DeleteSizingTemplate(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var info = _productTypeService.GetProductSizingInfoTemplateById(id);
            if (info == null)
                //No product attribute found with the specified id
                return RedirectToAction("ListSizingTemplate");

            _productTypeService.DeleteProductSizingInfoTemplate(info);

            SuccessNotification(_localizationService.GetResource("Admin.Catalog.Attributes.ProductAttributes.Deleted"));
            return RedirectToAction("ListSizingTemplate");
        }

        #endregion

        #region Shipping info

        public ActionResult ListShippingTemplate()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            return View();
        }

        [HttpPost]
        public ActionResult ListShippingTemplate(DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var datas = _productTypeService.GetAllProductShippingInfoTemplate(command.Page - 1, command.PageSize);
            var gridModel = new DataSourceResult
            {
                Data = datas.Select(x => new ProductInfoOverviewModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    Content = x.Content,
                }),
                Total = datas.TotalCount
            };
            return Json(gridModel);
        }

        //create
        public ActionResult CreateShippingTemplate()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();
            var model = new ProductInfoOverviewModel();
            //locales
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult CreateShippingTemplate(ProductInfoOverviewModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                ProductShippingInfoTemplate info = new ProductShippingInfoTemplate
                {
                    Name = model.Name,
                    Content = model.Content,
                };
                _productTypeService.InsertProductShippingInfoTemplate(info);


                SuccessNotification(_localizationService.GetResource("Admin.Catalog.Attributes.ProductAttributes.Added"));

                if (continueEditing)
                {
                    //selected tab
                    SaveSelectedTabName();

                    return RedirectToAction("EditShippingTemplate", new { id = info.Id });
                }
                return RedirectToAction("ListShippingTemplate");

            }
            //If we got this far, something failed, redisplay form
            return View(model);
        }

        //edit
        public ActionResult EditShippingTemplate(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var info = _productTypeService.GetProductShippingInfoTemplateById(id);
            if (info == null)
                //No product attribute found with the specified id
                return RedirectToAction("ListShippingTemplate");

            var model = new ProductInfoOverviewModel
            {
                Id = info.Id,
                Name = info.Name,
                Content = info.Content,
            };
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult EditShippingTemplate(ProductInfoOverviewModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var info = _productTypeService.GetProductShippingInfoTemplateById(model.Id);
            if (info == null)
                //No product attribute found with the specified id
                return RedirectToAction("ListShippingTemplate");

            if (ModelState.IsValid)
            {
                info.Name = model.Name;
                info.Content = model.Content;
                _productTypeService.UpdateProductShippingInfoTemplate(info);

                SuccessNotification(_localizationService.GetResource("Admin.Catalog.Attributes.ProductAttributes.Updated"));
                if (continueEditing)
                {
                    //selected tab
                    SaveSelectedTabName();

                    return RedirectToAction("EditShippingTemplate", new { id = info.Id });
                }
                return RedirectToAction("ListShippingTemplate");
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        //delete
        [HttpPost]
        public ActionResult DeleteShippingTemplate(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var info = _productTypeService.GetProductShippingInfoTemplateById(id);
            if (info == null)
                //No product attribute found with the specified id
                return RedirectToAction("ListShippingTemplate");

            _productTypeService.DeleteProductShippingInfoTemplate(info);

            SuccessNotification(_localizationService.GetResource("Admin.Catalog.Attributes.ProductAttributes.Deleted"));
            return RedirectToAction("ListShippingTemplate");
        }

        #endregion

        #region Basic info

        public ActionResult ListBasicInfoTemplate()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            return View();
        }

        [HttpPost]
        public ActionResult ListBasicInfoTemplate(DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var datas = _productTypeService.GetAllProductInfoTemplate(command.Page - 1, command.PageSize);
            var gridModel = new DataSourceResult
            {
                Data = datas.Select(x => new ProductInfoOverviewModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    Content = x.Content,
                }),
                Total = datas.TotalCount
            };
            return Json(gridModel);
        }

        //create
        public ActionResult CreateBasicInfoTemplate()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();
            var model = new ProductInfoOverviewModel();
            //locales
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult CreateBasicInfoTemplate(ProductInfoOverviewModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                ProductInfoTemplate info = new ProductInfoTemplate
                {
                    Name = model.Name,
                    Content = model.Content,
                };
                _productTypeService.InsertProductInfoTemplate(info);


                SuccessNotification(_localizationService.GetResource("Admin.Catalog.Attributes.ProductAttributes.Added"));

                if (continueEditing)
                {
                    //selected tab
                    SaveSelectedTabName();

                    return RedirectToAction("EditBasicInfoTemplate", new { id = info.Id });
                }
                return RedirectToAction("ListBasicInfoTemplate");

            }
            //If we got this far, something failed, redisplay form
            return View(model);
        }

        //edit
        public ActionResult EditBasicInfoTemplate(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var info = _productTypeService.GetProductInfoTemplateById(id);
            if (info == null)
                //No product attribute found with the specified id
                return RedirectToAction("ListBasicInfoTemplate");

            var model = new ProductInfoOverviewModel
            {
                Id = info.Id,
                Name = info.Name,
                Content = info.Content,
            };
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult EditBasicInfoTemplate(ProductInfoOverviewModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var info = _productTypeService.GetProductInfoTemplateById(model.Id);
            if (info == null)
                //No product attribute found with the specified id
                return RedirectToAction("ListBasicInfoTemplate");

            if (ModelState.IsValid)
            {
                info.Name = model.Name;
                info.Content = model.Content;
                _productTypeService.UpdateProductInfoTemplate(info);

                SuccessNotification(_localizationService.GetResource("Admin.Catalog.Attributes.ProductAttributes.Updated"));
                if (continueEditing)
                {
                    //selected tab
                    SaveSelectedTabName();

                    return RedirectToAction("EditBasicInfoTemplate", new { id = info.Id });
                }
                return RedirectToAction("ListBasicInfoTemplate");
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        //delete
        [HttpPost]
        public ActionResult DeleteBasicInfoTemplate(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var info = _productTypeService.GetProductInfoTemplateById(id);
            if (info == null)
                //No product attribute found with the specified id
                return RedirectToAction("ListBasicInfoTemplate");

            _productTypeService.DeleteProductInfoTemplateTemplate(info);

            SuccessNotification(_localizationService.GetResource("Admin.Catalog.Attributes.ProductAttributes.Deleted"));
            return RedirectToAction("ListBasicInfoTemplate");
        }

        public ActionResult GenerateTemplate()
        {
            return View();
        }
        #endregion
        #endregion
    }
}