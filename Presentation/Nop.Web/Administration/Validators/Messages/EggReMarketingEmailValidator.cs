﻿using FluentValidation;
using Nop.Admin.Models.Messages;
using Nop.Core.Domain.Messages;
using Nop.Data;
using Nop.Services.Localization;
using Nop.Web.Framework.Validators;

namespace Nop.Admin.Validators.Messages
{
    public partial class EggReMarketingEmailValidator : BaseNopValidator<EggReMarketingEmailModel>
    {
        public EggReMarketingEmailValidator(ILocalizationService localizationService, IDbContext dbContext)
        {
            RuleFor(x => x.EmailAddress).NotEmpty();
            RuleFor(x => x.EmailAddress).EmailAddress().WithMessage(localizationService.GetResource("Admin.Common.WrongEmail"));
            
            RuleFor(x => x.EmailType).NotEmpty();

            SetStringPropertiesMaxLength<EggReMarketingEmail>(dbContext);
        }
    }
}