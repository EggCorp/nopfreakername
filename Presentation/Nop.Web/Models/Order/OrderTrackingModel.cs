﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Order
{
    public class OrderTrackingModel
    {
        public IList<OrderDetailsModel> ListOrderDetail { get; set; }

        public bool NotOrderToFound { get; set; }

    }
}