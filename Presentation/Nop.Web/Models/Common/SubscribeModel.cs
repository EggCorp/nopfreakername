﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Common
{
    public class SubscribeModel
    {
        public string Email { get; set; }

        public bool IsSubscribed { get; set; }

        public bool IsNotExistedEmail { get; set; }
    }
}