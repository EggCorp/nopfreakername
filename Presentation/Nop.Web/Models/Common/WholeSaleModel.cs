﻿using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Stores;
using Nop.Web.Models.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Common
{
    public class WholeSaleModel
    {
        public WholeSaleModel()
        {
            HotTrendingCategories = new List<CategoryModel>();
        }
        public Store CurrentStore { get; set; }

        public IList<CategoryModel> HotTrendingCategories { get; set; }
    }
}