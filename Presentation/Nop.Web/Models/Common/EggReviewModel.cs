﻿using Nop.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Common
{
    public partial class EggReviewModel
    {
        public IList<Review> Reviews { get; set; }

        public string ProductTitle { get; set; }

        public bool AvailableSeeMore { get; set; }
    }
}