﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Common
{
    public class EggCouponResult
    {
        public bool success { get; set; }
        public WinningSlice winning_slice { get; set; }
    }
    public class WinningSlice
    {
        public int number { get; set; }
        public string type { get; set; }
        public string value { get; set; }
        public string label { get; set; }
    }
}