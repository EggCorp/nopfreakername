﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Common
{
    public class CouponRequestData
    {
        public string token { get; set; }
        public string wheel { get; set; }
        public string email { get; set; }
        public string csrfmiddlewaretoken { get; set; }
        public string url { get; set; }

    }
}