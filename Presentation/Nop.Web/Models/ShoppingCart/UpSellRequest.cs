﻿namespace Nop.Web.Models.ShoppingCart
{
    public class ItemUpSellRequest
    {
        public string Id { get; set; }
        public int ProductId { get; set; }
        public string Size { get; set; }
        public int Quantity { get; set; }
        public string ItemStyleColor { get; set; }
        public int TempCartItemId { get; set; }
    }
}