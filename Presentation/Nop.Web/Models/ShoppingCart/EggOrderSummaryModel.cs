﻿using Nop.Web.Framework.Mvc;
using Nop.Web.Models.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Nop.Web.Models.ShoppingCart.ShoppingCartModel;

namespace Nop.Web.Models.ShoppingCart
{
    public partial class EggOrderSummaryModel : BaseNopModel
    {
        public EggOrderSummaryModel()
        {
            ShoppingCartModel = new ShoppingCartModel();
            OrderTotalModel = new OrderTotalsModel();
        }
        public ShoppingCartModel ShoppingCartModel { get; set; }
        public OrderTotalsModel OrderTotalModel { get; set; }
    }
}