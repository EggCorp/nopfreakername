﻿namespace Nop.Web.Models.ShoppingCart
{
    public class UpdateCartItemModel
    {
        public int Id { get; set; }

        public int TemporaryId { get; set; }

        public int Quantity { get; set; }

        public int SizeId { get; set; }

        public int ColorId { get; set; }

        public int TypeId { get; set; }

        public bool IsRemove { get; set; } = false;

        public bool IsNew { get; set; }

        public int ProductId { get; set; }

        public string AttributeXml { get; set; }
    }
}