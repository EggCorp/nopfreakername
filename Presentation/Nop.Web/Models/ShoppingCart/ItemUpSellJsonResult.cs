﻿using Nop.Core.Domain.Media;
using System.Collections.Generic;

namespace Nop.Web.Models.ShoppingCart
{
    public class ItemUpSellJsonResult
    {
        public string Id { get; set; }
        public string SubTotalPrice { get; set; }
        public string PictureUrl { get; set; }
        public List<string> Sizes { get; set; }
        public string NewSize { get; set; }
    }

    public class StyleColorJsonResult
    {
        public int ColorId { get; set; }
        public int TypeId { get; set; }
    }

    public class CustomFieldJsonResult
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }

    public class EggCartItem
    {
        public List<RenderPictureRequest.CustomData> CustomFieldsResult { get; set; }
        public string Size { get; set; }
        public int ColorProductAttribuiteValueId { get; set; }
        public int StyleProductAttribuiteValueId { get; set; }
        public int Quantity { get; set; }
    }
}