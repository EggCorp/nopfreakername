﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.ShoppingCart
{
    public class CartGroupbyCategoryModel
    {
        public int CategoryId { get; set; }
        public IList<int> ProductIds { get; set; }
        public int Quantity { get; set; }
    }
}