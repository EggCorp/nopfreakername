﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Media
{
    public partial class ProductRenderRequest
    {
        public int Id { get; set; }

        public string Text { get; set; }
    }
}