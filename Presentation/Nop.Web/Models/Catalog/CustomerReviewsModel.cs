﻿using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Catalog
{
    public class CustomerReviewsModel : BaseNopModel
    {
        public CustomerReviewsModel()
        {
            this.Reviews = new List<ProductReviewModel>();
            PagingFilteringContext = new CatalogPagingFilteringModel();
            ReviewOverviewModel = new ProductReviewOverviewModel();

        }
        public ProductReviewOverviewModel ReviewOverviewModel { get; set; }
        public List<ProductReviewModel> Reviews { get; set; }
        public CatalogPagingFilteringModel PagingFilteringContext { get; set; }
        public int TotalReviews { get; set; }
    }
}