﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Catalog
{
    public class GATrackingItemModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Category { get; set; }

        public decimal Price { get; set; }

        public int Quantity { get; set; }

        public int OrderId { get; set; }

        public decimal Revenue { get; set; }

        public decimal Shipping { get; set; }

        public bool IsOrderTotalItem { get; set; } = false;

    }
}