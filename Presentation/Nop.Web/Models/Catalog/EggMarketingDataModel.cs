﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Catalog
{
    public class EggMarketingDataModel
    {
        public EggMarketingDataModel()
        {
            FacebookPixelAffiliateIds = new List<string>();
            DynamicFacebookData = new List<string>();
            GoogleAnalyticsData = new List<string>();
            TrackingItems = new List<GATrackingItemModel>();
        }
        public List<string> FacebookPixelAffiliateIds { get; set; }
        public string FacebookPixelParentId { get; set; }
        public List<string> DynamicFacebookData { get; set; }
        public IList<GATrackingItemModel> TrackingItems { get; set; }
        public long GoogleAnalyticsId { get; set; }
        public List<string> GoogleAnalyticsData { get; set; }
        public string GoogleAnalyticsLabelForCart { get; set; }
        public string GoogleAnalyticsLabelForCheckOut { get; set; }
        public string GoogleAnalyticsLabelForCompleted { get; set; }
        public decimal TotalPrice { get; set; }
    }
}