﻿using Nop.Core.Domain.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Nop.Web.Models.Catalog.ProductDetailsModel;

namespace Nop.Web.Models.Catalog
{
    public class ProductDetailConditionCheckModel
    {
        public ProductDetailConditionCheckModel()
        {
            ProductAttributeConditionValue = new ProductAttributeValueModel();
            ProductAttributeModel = new ProductAttributeModel();
        }

        public ProductAttributeValueModel ProductAttributeConditionValue { get; set; }

        public ProductAttributeModel ProductAttributeModel { get; set; }

    }
}