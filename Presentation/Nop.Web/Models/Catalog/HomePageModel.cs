﻿using Nop.Core;
using Nop.Core.Domain.Stores;
using Nop.Web.Models.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Catalog
{
    public partial class HomePageModel
    {
        public HomePageModel()
        {
            StoreInfo  = new Store();
            HomePageItems = new List<CategoryModel>();
            HotTrendings = new List<HotTrendingModel>();
            BestSellerProducts = new List<ProductOverviewModel>();
            NewestProducts = new List<ProductOverviewModel>();
        }

        public Store StoreInfo { get; set; }

        public IList<CategoryModel> HomePageItems { get; set; }

        public IList<HotTrendingModel> HotTrendings { get; set; }

        public IList<ProductOverviewModel> BestSellerProducts { get; set; }

        public IList<ProductOverviewModel> NewestProducts { get; set; }
        public int TotalReviews { get; set; }

        public partial class HotTrendingModel : BaseEntity
        {
            public HotTrendingModel()
            {
                PictureModel = new PictureModel();

            }
            public string Name { get; set; }

            public string RedirectUrl { get; set; }

            public string InnerText { get; set; }

            public int DisplayOrder { get; set; }

            public bool IsLargeBanner { get; set; }

            public int PictureId { get; set; }

            public PictureModel PictureModel { get; set; }


        }

        public EggMarketingDataModel RemarketingData { get; set; }

        public string SellOffBannerImageUrl { get; set; }
    }
}