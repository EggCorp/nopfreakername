﻿using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.OrderFullFill
{
    public class OrderFullFillModel
    {
        public OrderFullFillModel()
        {
            OrderItems = new List<OrderItemFullFillModel>();
            Shipments = new List<ShipmentModel>();
        }
        public int Id { get; set; }
        public int OrderId { get; set; }
        public string ShippingName { get; set; }
        public string ShippingPhone { get; set; }
        public string ShippingEmail { get; set; }
        public string ShippingAddress { get; set; }
        public int OrderStatusId { get; set; }
        public int PaymentStatusId { get; set; }
        public int FullFilltStatusId { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ShippingStatusId { get; set; }
        public long ShopifyOrderId { get; set; }
        public string Gateway { get; set; }
        public decimal OrderTotal { get; set; }
        public decimal OrderShipTotal { get; set; }
        public string ShippingZipcode { get; set; }
        public string ShippingCountry { get; set; }
        public List<OrderItemFullFillModel> OrderItems { get; set; }
        public List<ShipmentModel> Shipments { get; set; }
        public string OrderStatus { get; set; }
        public string PaymentStatus { get; set; }
        public string ShippingStatus { get; set; }
    }
}