﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.OrderFullFill
{
    public class ShipmentModel
    {
        public int Id { get; set; }

        public string TrackingNumber { get; set; }

        public string TrackingUrl { get; set; }

        public string TrackingCompany { get; set; }

        public DateTime CreatedOnUtc { get; set; }

        public string Note { get; set; }
    }
}