﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.OrderFullFill
{
    public class EggOrderTrackingModel
    {
        public EggOrderTrackingModel()
        {
            Orders = new List<OrderFullFillModel>();
        }
        public List<OrderFullFillModel> Orders { get; set; }
        public string Key { get; set; }
        public bool Success { get; set; }
    }
}