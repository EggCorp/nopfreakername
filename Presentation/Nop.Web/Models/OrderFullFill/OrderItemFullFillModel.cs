﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.OrderFullFill
{
    public class OrderItemFullFillModel
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public long OrderItemId { get; set; }
        public string Size { get; set; }
        public int Quantity { get; set; }
        public string Type { get; set; }
        public string Color { get; set; }
        public string OriginUrl { get; set; }
        public string ProductUrl { get; set; }
        public string PictureUrl { get; set; }
        public string ProductTiltle { get; set; }
        public bool Deleted { get; set; }

    }
}