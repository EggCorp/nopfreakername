﻿using Nop.Core.Domain.Shipping;
using Nop.Web.Framework.Mvc;
using Nop.Web.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Checkout
{
    public partial class EggCheckoutModel : BaseNopModel
    {
        public EggCheckoutModel()
        {
            
        }

        public CheckoutBillingAddressModel BillingAddress { get; set; }

        public CheckoutPaymentMethodModel CheckoutPaymentMethod { get; set; }

        public List<ShippingMethod> ShippingMethods { get; set; }

        public int SelectedShippingMethodId { get; set; }

    }
}