﻿using Nop.Web.Framework.Mvc;
using Nop.Web.Models.Catalog;
using Nop.Web.Models.Order;
using System.Collections.Generic;

namespace Nop.Web.Models.Checkout
{
    public partial class CheckoutCompletedModel : BaseNopModel
    {
        public int OrderId { get; set; }
        public bool OnePageCheckoutEnabled { get; set; }
        public OrderDetailsModel OrderDetails { get; set; }
        public EggMarketingDataModel RemarketingData { get; set; }
        public string StoreEmail { get; set;
        }
    }
}