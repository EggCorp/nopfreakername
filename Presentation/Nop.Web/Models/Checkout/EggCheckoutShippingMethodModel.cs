﻿using Nop.Web.Framework.Mvc;
using System.Collections.Generic;

namespace Nop.Web.Models.Checkout
{
    public partial class EggCheckoutShippingMethodModel : BaseNopEntityModel
    {
        public EggCheckoutShippingMethodModel()
        {
            ShippingMethods = new List<EggShippingMethodModel>();
        }

        public IList<EggShippingMethodModel> ShippingMethods { get; set; }
        public int ShippingMethod { get; set; }
    }

    public partial class EggShippingMethodModel : BaseNopEntityModel
    {
        public string Name { get; set; }
        public string Description { get; set; }

    }
}