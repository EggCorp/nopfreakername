﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Checkout
{
    public class PaymentMethodJsonModel
    {
        public bool completed { get; set; }
        public string name { get; set; }
        public string redirectUrl { get; set; }
        public string html { get; set; }
        public string message { get; set; }
    }
}