﻿using Nop.Core.Domain.Designs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Data.Mapping.Designs
{
    public partial class DesignProductCustomFieldMap : NopEntityTypeConfiguration<DesignProductCustomFieldMapping>
    {
        public DesignProductCustomFieldMap()
        {
            this.ToTable("Design_Product_CustomField_Mapping");
            this.HasKey(dc => dc.Id);

            this.HasRequired(dc => dc.DesignCustomField)
            .WithMany(p=>p.DesignProductCustomFieldMappings)
            .HasForeignKey(dc => dc.DesignCustomFieldId);

            this.HasRequired(dc => dc.ProductCustomFieldMapping)
            .WithMany(p=>p.DesignProductCustomFieldMappings)
            .HasForeignKey(dc => dc.ProductCustomFieldMappingId);
        }
    }
}
