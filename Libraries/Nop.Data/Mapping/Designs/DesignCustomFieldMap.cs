﻿using Nop.Core.Domain.Designs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Data.Mapping.Designs
{
    public partial class DesignCustomFieldMap : NopEntityTypeConfiguration<DesignCustomField>
    {
        public DesignCustomFieldMap()
        {
            this.ToTable("Design_CustomField");
            this.HasKey(dc => dc.Id);

            this.HasRequired(dc => dc.Design)
            .WithMany(p=>p.DesignCustomFields)
            .HasForeignKey(dc => dc.DesignId);

            this.HasRequired(dc => dc.CustomField)
            .WithMany()
            .HasForeignKey(dc => dc.CustomFieldId);

            this.Ignore(dc => dc.ArtAlignment);
            this.Ignore(dc => dc.ArtEffect);
            this.Ignore(dc => dc.ArtType);
        }
    }
}
