﻿using Nop.Core.Domain.Designs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Data.Mapping.Designs
{
    public partial class DesignMap : NopEntityTypeConfiguration<Design>
    {
        public DesignMap()
        {
            this.ToTable("Design");
            this.HasKey(pa => pa.Id);

            this.HasRequired(pam => pam.Product)
              .WithMany(p => p.ProductDesigns)
              .HasForeignKey(pam => pam.ProductId);
            this.Ignore(p => p.UploadStatus);

        }
    }
}
