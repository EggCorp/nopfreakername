﻿using Nop.Core.Domain.Catalog;

namespace Nop.Data.Mapping.Catalog
{
    partial class ProductTypeInfoMap : NopEntityTypeConfiguration<ProductTypeInfo>
    {
        public ProductTypeInfoMap()
        {
            this.ToTable("ProductTypeInfo");
            this.HasKey(pam => pam.Id);
            this.Property(pa => pa.TypeName).IsRequired();
            this.Property(pa => pa.TypeCode).IsRequired();
            this.Property(pa => pa.IsActive).IsRequired();

            this.HasOptional(pam => pam.ProductInfoTemplate)
              .WithMany(p=>p.ProductTypes)
              .HasForeignKey(pam => pam.ProductInfoTemplateId);

            this.HasOptional(pam => pam.ProductShippingInfoTemplate)
             .WithMany(p => p.ProductTypes)
            .HasForeignKey(pam => pam.ProductShippingInfoTemplateId);

            this.HasOptional(pam => pam.ProductSizingInfoTemplate)
            .WithMany(p => p.ProductTypes)
            .HasForeignKey(pam => pam.ProductSizingInfoTemplateId);
        }
    }
}
