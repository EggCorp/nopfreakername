﻿using Nop.Core.Domain.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Data.Mapping.Catalog
{
    public partial class ProductSizingInfoTemplateMap : NopEntityTypeConfiguration<ProductSizingInfoTemplate>
    {
        public ProductSizingInfoTemplateMap()
        {
            this.ToTable("ProductSizingInfoTemplate");
            this.HasKey(pa => pa.Id);
            this.HasMany(p => p.ProductTypes).WithOptional();
        }
    }
}
