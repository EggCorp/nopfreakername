﻿
using Nop.Core.Domain.Catalog;

namespace Nop.Data.Mapping.Catalog
{
    public partial class ProductReviewPictureMap : NopEntityTypeConfiguration<ProductReviewPicture>
    {
        public ProductReviewPictureMap()
        {
            this.ToTable("ProductReview_Picture_Mapping");
            this.HasKey(pp => pp.Id);

            this.HasRequired(pp => pp.Picture)
                .WithMany()
                .HasForeignKey(pp => pp.PictureId);

            this.HasRequired(pp => pp.ProductReview)
                .WithMany(p => p.ProductReviewPictures)
                .HasForeignKey(pp => pp.ProductReviewId);
        }
    }
}