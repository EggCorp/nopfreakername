﻿using Nop.Core.Domain.Catalog;

namespace Nop.Data.Mapping.Catalog
{
    partial class ShippingByCountryMap : NopEntityTypeConfiguration<ShippingByCountry>
    {
        public ShippingByCountryMap()
        {
            this.ToTable("ShippingByCountry");
            this.HasKey(pam => pam.Id);
        }
    }
}
