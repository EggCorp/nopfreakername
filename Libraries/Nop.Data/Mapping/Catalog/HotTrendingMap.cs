﻿using Nop.Core.Domain.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Data.Mapping.Catalog
{
    public partial class HotTrendingMap : NopEntityTypeConfiguration<HotTrending>
    {
        public HotTrendingMap()
        {
            this.ToTable("HotTrending");
            this.HasKey(pp => pp.Id);

            this.HasRequired(pp => pp.Picture)
                .WithMany()
                .HasForeignKey(pp => pp.PictureId);
        }
    }
}