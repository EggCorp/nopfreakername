﻿using Nop.Core.Domain.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Data.Mapping.Orders
{
    public class OrderCustomFieldValueMap : NopEntityTypeConfiguration<OrderCustomFieldValue>
    {
        public OrderCustomFieldValueMap()
        {
            this.ToTable("OrderCustomFieldValue");
            this.HasKey(on => on.Id);
        }
    }
}
