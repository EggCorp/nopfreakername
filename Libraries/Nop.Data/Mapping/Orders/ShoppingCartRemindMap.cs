﻿using Nop.Core.Domain.Orders;

namespace Nop.Data.Mapping.Orders
{
    public partial class ShoppingCartRemindMap : NopEntityTypeConfiguration<ShoppingCartRemind>
    {
        public ShoppingCartRemindMap()
        {
            this.ToTable("ShoppingCartRemind");
            this.HasKey(sci => sci.Id);


        }
    }
}
