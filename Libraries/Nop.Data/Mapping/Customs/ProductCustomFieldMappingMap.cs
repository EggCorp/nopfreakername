﻿using Nop.Core.Domain.Customs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Data.Mapping.Customs
{
    partial class ProductCustomFieldMappingMap : NopEntityTypeConfiguration<ProductCustomFieldMapping>
    {
        public ProductCustomFieldMappingMap()
        {
            this.ToTable("Product_CustomField_Mapping");
            this.HasKey(pam => pam.Id);

            this.HasRequired(pam => pam.Product)
                .WithMany(p => p.ProductCustomFieldMappings)
                .HasForeignKey(pam => pam.ProductId);

            this.HasRequired(pam => pam.CustomField)
              .WithMany()
              .HasForeignKey(pam => pam.CustomFieldId);
        }
    }
}
