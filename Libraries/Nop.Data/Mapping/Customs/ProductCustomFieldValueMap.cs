﻿using Nop.Core.Domain.Customs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Data.Mapping.Customs
{
    public partial class ProductCustomFieldValueMap : NopEntityTypeConfiguration<ProductCustomFieldValue>
    {
        public ProductCustomFieldValueMap()
        {
            this.ToTable("ProductCustomFieldValue");
            this.HasKey(pa => pa.Id);

            this.HasRequired(pam => pam.ProductCustomFieldMapping)
                .WithMany(p=>p.ProductCustomFieldValues)
                .HasForeignKey(pam => pam.ProductCustomFieldMappingId);

            this.HasRequired(pam => pam.ProductAttributeValue)
            .WithMany()
            .HasForeignKey(pam => pam.ProductAttributeValueId);
        }
    }
}
