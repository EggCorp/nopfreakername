﻿using Nop.Core.Domain.Customs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Data.Mapping.Customs
{
    public partial class CustomFieldMap : NopEntityTypeConfiguration<CustomField>
    {
        public CustomFieldMap()
        {
            this.ToTable("CustomField");
            this.HasKey(pa => pa.Id);
            this.Property(pa => pa.Name).IsRequired();
            this.Ignore(pav => pav.Type);
            this.Ignore(pav => pav.TypedValue);
        }
    }
}
