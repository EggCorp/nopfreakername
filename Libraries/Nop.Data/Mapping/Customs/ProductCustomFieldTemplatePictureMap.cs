﻿using Nop.Core.Domain.Customs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Data.Mapping.Customs
{
    public partial class ProductCustomFieldTemplatePictureMap : NopEntityTypeConfiguration<ProductCustomFieldTemplatePicture>
    {
        public ProductCustomFieldTemplatePictureMap()
        {
            this.ToTable("ProductCustomFieldTemplatePicture");
            this.HasKey(pam => pam.Id);

            this.HasRequired(pam => pam.Picture)
                .WithMany()
                .HasForeignKey(pam => pam.PictureId);

            this.HasRequired(pam => pam.ProductAttributeValue)          
              .WithMany(pam=>pam.ProductCustomFieldTemplatePictures)
              .HasForeignKey(pam => pam.ProductAttributeValueId);
        }
    }
}
