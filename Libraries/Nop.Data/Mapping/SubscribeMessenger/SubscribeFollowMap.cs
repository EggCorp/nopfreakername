﻿using Nop.Core.Domain.SubscribeMessenger;

namespace Nop.Data.Mapping.SubscribeMessenger
{
    public partial class SubscribeFollowMap : NopEntityTypeConfiguration<SubscribeFollow>
    {
        public SubscribeFollowMap()
        {
            this.ToTable("SubscribeFollow");

            this.Property(u => u.Name).HasMaxLength(400);
            this.Property(u => u.Note).HasMaxLength(400);
        }
    }
}
