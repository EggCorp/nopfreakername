﻿using Nop.Core.Domain.SubscribeMessenger;

namespace Nop.Data.Mapping.SubscribeMessenger
{
    public partial class CampaignMessengerMap : NopEntityTypeConfiguration<CampaignMessenger>
    {
        public CampaignMessengerMap()
        {
            this.ToTable("CampaignMessenger");
            this.Property(u => u.AttachmentUrl).HasMaxLength(400);
            this.Property(u => u.AttachmentType).HasMaxLength(400);
            this.Property(u => u.TextMessage).HasMaxLength(400);
            this.Property(u => u.Link).HasMaxLength(400);
        }
    }
}
