﻿using Nop.Core.Domain.SubscribeMessenger;

namespace Nop.Data.Mapping.SubscribeMessenger
{
    public partial class CampaignInterestMap: NopEntityTypeConfiguration<CampaignInterest>
    {
        public CampaignInterestMap()
        {
            this.ToTable("CampaignInterest");
            this.HasKey(p => p.Id);
            this.HasRequired(p => p.CampaignMessenger)
                .WithMany()
                .HasForeignKey(p => p.CampaignMessengerId);
        }
    }
}
