﻿using Nop.Core.Domain.SubscribeMessenger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Data.Mapping.SubscribeMessenger
{
    public partial class CampaignEntityMap : NopEntityTypeConfiguration<CampaignEntity>
    {
        public CampaignEntityMap()
        {
            this.ToTable("Campaign_Entity_Mapping");
            this.HasKey(p => p.Id);
            this.HasRequired(p => p.CampaignMessenger)
                .WithMany()
                .HasForeignKey(p => p.CampaignMessengerId);
        }
    }
}
