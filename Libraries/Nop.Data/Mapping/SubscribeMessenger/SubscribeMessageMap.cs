﻿using Nop.Core.Domain.SubscribeMessenger;

namespace Nop.Data.Mapping.SubscribeMessenger
{
    public partial class SubscribeMessageMap : NopEntityTypeConfiguration<SubscribeMessage>
    {
        public SubscribeMessageMap()
        {
            this.ToTable("SubscribeMessage");
            this.HasKey(p => p.Id);
            this.HasOptional(u => u.SubscribeCustomer)
                .WithMany()
                .HasForeignKey(u => u.SubscribeCustomerId);
            this.HasOptional(u => u.SubscribeFollow)
                .WithMany()
                .HasForeignKey(u => u.SubscribeFollowId);
        }
    }
}
