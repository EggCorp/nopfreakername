﻿using Nop.Core.Domain.SubscribeMessenger;

namespace Nop.Data.Mapping.SubscribeMessenger
{
    public partial class CampaignSubscribeCustomerMap : NopEntityTypeConfiguration<CampaignSubscribeCustomer>
    {
        public CampaignSubscribeCustomerMap()
        {
            this.ToTable("Campaign_SubscribeCustomer_Mapping");
            this.HasKey(p => p.Id);
            this.HasRequired(p => p.CampaignMessenger)
                .WithMany()
                .HasForeignKey(p => p.CampaignMessengerId);

            this.HasRequired(p => p.SubscribeCustomer)
                .WithMany()
                .HasForeignKey(p => p.SubscribeCustomerId);
        }
        
    }
}
