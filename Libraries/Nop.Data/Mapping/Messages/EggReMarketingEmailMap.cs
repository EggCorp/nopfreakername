﻿using Nop.Core.Domain.Messages;

namespace Nop.Data.Mapping.Messages
{
    public partial class EggReMarketingEmailMap: NopEntityTypeConfiguration<EggReMarketingEmail>
    {
        public EggReMarketingEmailMap()
        {
            this.ToTable("EggReMarketingEmail");
            this.HasKey(mt => mt.Id);
            this.Property(mt => mt.EmailAddress).IsRequired();
            this.Property(mt => mt.EmailType).IsRequired();
            this.Property(mt => mt.MaxStep).IsRequired();
            this.Property(mt => mt.InStep).IsRequired();
            this.Property(mt => mt.SentOnUtc).IsRequired();
            this.Property(mt => mt.ResentDate).IsRequired();
            this.Property(mt => mt.CreatedOnUtc);
            this.Property(mt => mt.OrderId);
            this.Property(mt => mt.NameRegister);
            this.Property(mt => mt.CategoryId);

        }
    }
}
