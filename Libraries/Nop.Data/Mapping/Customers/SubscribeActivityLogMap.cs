﻿using Nop.Core.Domain.Customers;

namespace Nop.Data.Mapping.Customers
{
    public partial class SubscribeActivityLogMap : NopEntityTypeConfiguration<SubscribeActivityLog>
    {
        public SubscribeActivityLogMap()
        {
            this.ToTable("SubscribeActivityLog");
            this.HasKey(c => c.Id);
            this.Property(u => u.EntityName).HasMaxLength(1000);
            this.Property(u => u.Comment).HasMaxLength(1000);
            this.HasOptional(u => u.ActivityLogType)
                .WithMany()
                .HasForeignKey(u => u.ActivityLogTypeId);
            this.HasOptional(u => u.SubscribeCustomer)
                .WithMany()
                .HasForeignKey(u => u.SubscribeCustomerId);
        }
    }
}
