﻿using Nop.Core.Domain.Customers;

namespace Nop.Data.Mapping.Customers
{
    public partial class SubscribeCustomerMap : NopEntityTypeConfiguration<SubscribeCustomer>
    {
        public SubscribeCustomerMap()
        {
            this.ToTable("SubscribeCustomer");
            this.HasKey(c => c.Id);
            this.Property(u => u.Token).HasMaxLength(1000);
            this.Property(u => u.UserRef).HasMaxLength(1000);
            this.HasOptional(u => u.Product)
                .WithMany()
                .HasForeignKey(u => u.ProductId);
            this.HasOptional(u => u.Customer)
                .WithMany()
                .HasForeignKey(u => u.CustomerId);
        }
    }
}
