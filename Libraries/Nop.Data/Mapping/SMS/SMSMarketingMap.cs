﻿using Nop.Core.Domain.SMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Data.Mapping.SMS
{
    public partial class SMSMarketingMap : NopEntityTypeConfiguration<SMSMarketing>
    {
        public SMSMarketingMap()
        {
            this.ToTable("SMSMarketing");
            this.HasKey(mt => mt.Id);
            this.Property(mt => mt.OrderId).IsRequired();
            this.Property(mt => mt.SMSType).IsRequired();
            this.Property(mt => mt.To).IsRequired();
            this.Property(mt => mt.Body).IsRequired();
        }
    }
}
