﻿using Nop.Core.Domain.SMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Data.Mapping.SMS
{
    public partial class SMSTemplateMap : NopEntityTypeConfiguration<SMSTemplate>
    {
        public SMSTemplateMap()
        {
            this.ToTable("SMSTemplate");
            this.HasKey(mt => mt.Id);
            this.Property(mt => mt.SMSType).IsRequired();
            this.Property(mt => mt.IsDeleted).IsRequired();
            this.Property(mt => mt.TemplateBody).IsRequired();
        }
    }
}
