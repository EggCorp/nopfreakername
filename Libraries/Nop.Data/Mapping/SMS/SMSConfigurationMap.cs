﻿using Nop.Core.Domain.SMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Data.Mapping.SMS
{
    class SMSConfigurationMap : NopEntityTypeConfiguration<SMSConfiguration>
    {
        public SMSConfigurationMap()
        {
            this.ToTable("SMSConfiguration");
            this.HasKey(mt => mt.Id);
            this.Property(mt => mt.AccountSid).IsRequired();
            this.Property(mt => mt.AuthToken).IsRequired();
            this.Property(mt => mt.PhoneNumber).IsRequired();
        }
    }
}
