﻿using Nop.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Data.Mapping.Common
{
    public partial class ReviewMap : NopEntityTypeConfiguration<Review>
    {
        public ReviewMap()
        {
            this.ToTable("Review");
            this.HasKey(st => st.Id);
            this.Property(st => st.CreatedOnUtc);
            this.Property(st => st.Rating).IsRequired();
        }
    }
}
