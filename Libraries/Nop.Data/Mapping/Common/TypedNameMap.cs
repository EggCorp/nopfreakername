﻿using Nop.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Data.Mapping.Common
{
    public partial class TypedNameMap : NopEntityTypeConfiguration<TypedName>
    {
        public TypedNameMap()
        {
            this.ToTable("TypedName");
            this.HasKey(st => st.Id);
        }
    }
}
