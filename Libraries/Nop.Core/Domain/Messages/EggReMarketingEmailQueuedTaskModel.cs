﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Messages
{
    public class EggReMarketingEmailQueuedTaskModel
    {
        public IList<QueuedEmail> QueuedEmails { get; set; }
        public IList<EggReMarketingEmail> EggReMarketingEmails { get; set; }
    }
}
