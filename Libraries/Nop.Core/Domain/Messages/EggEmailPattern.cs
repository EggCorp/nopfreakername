﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Messages
{
    public class EggEmailPattern
    {
        public const string CustomerNamePartten = "%Customer.Name%";
        public const string DataTablePartten = "%ProductInName.DataTable%";
        public const string OrderGuid = "%Order.OrderGuide%";
        public const string OrderProductTable = "%Order.Product(s)%";
        public const string CustomerOldCartUrl = "%Customer.CartUrl%";
        public const string StoreName = "%Store.Name%";
        public const string StoreUrl = "%Store.Url%";
        public const string StoreEmail = "%Store.Email%";
        public const string OrderTrackingUrl = "%Order.TrackingUrl%";
        public const string OrderTrackingNumber = "%Order.TrackingNumber%";
        public const string OrderTrackingCompany = "%Order.TrackingCompany%";

    }
}
