﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Messages
{
    public partial class EggReMarketingEmail : BaseEntity
    {
        public string EmailAddress { get; set; }

        public string EmailType { get; set; }
        
        public int MaxStep { get; set; }

        public int InStep { get; set; }

        public bool IsCompleted { get; set; }

        public DateTime CreatedOnUtc { get; set; }

        public DateTime SentOnUtc { get; set; }

        public DateTime ResentDate { get; set; }

        public string NameRegister { get; set; }

        public int OrderId { get; set; }

        public int CategoryId { get; set; }


    }
}
