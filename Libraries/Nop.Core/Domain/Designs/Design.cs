﻿using Nop.Core.Domain.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Designs
{
    public partial class Design : BaseEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string FileDesign { get; set; }

        public string TemplateUpload { get; set; }

        public int? ProductId { get; set; }

        public string UploadLink { get; set; }

        public int? UploadStatusId { get; set; }

        public UploadStatus UploadStatus
        {
            get
            {
                return (UploadStatus)this.UploadStatusId;
            }
            set
            {
                this.UploadStatusId = (int)value;
            }
        }

        public virtual Product Product { get; set; }

        private ICollection<DesignCustomField> _designCustomFields;

        public virtual ICollection<DesignCustomField> DesignCustomFields
        {
            get { return _designCustomFields ?? (_designCustomFields = new List<DesignCustomField>()); }
            protected set { _designCustomFields = value; }
        }
    }
}
