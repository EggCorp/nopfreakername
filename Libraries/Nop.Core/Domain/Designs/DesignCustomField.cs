﻿using Nop.Core.Domain.Customs;
using Nop.Core.Domain.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Designs
{
    public partial class DesignCustomField : BaseEntity
    {
        public int CustomFieldId { get; set; }

        public int DesignId { get; set; }

        public string Font { get; set; }

        public string FontColor { get; set; }

        public int FontSize { get; set; }

        public double Width { get; set; }

        public double Height { get; set; }

        public double? Bend { get; set; }

        public double? Rotate { get; set; }

        public int ArtTypeId { get; set; } = 0;
        public ArtType ArtType
        {
            get
            {
                return (ArtType)this.ArtTypeId;
            }
            set
            {
                this.ArtTypeId = (int)value;
            }
        }

        public int ArtEffectId { get; set; } = 0;
        public ArtEffect ArtEffect
        {
            get
            {
                return (ArtEffect)this.ArtEffectId;
            }
            set
            {
                this.ArtEffectId = (int)value;
            }
        }

        public int ArtAlignmentId { get; set; } = 0;
        public ArtAlignment ArtAlignment
        {
            get
            {
                return (ArtAlignment)this.ArtAlignmentId;
            }
            set
            {
                this.ArtAlignmentId = (int)value;
            }
        }

        public string EffectColor { get; set; }

        public double? EffectSize { get; set; }

        public double? LeftScale { get; set; }

        public double? RightScale { get; set; }

        public double? Tilt { get; set; }

        public double? X { get; set; }

        public double? Y { get; set; }

        public string DefaultText {get;set;}

        public Nullable<bool> FullWidthRequire { get; set; }

        public Nullable<bool> FullBoxSizeRequire { get; set; }

        public virtual CustomField CustomField { get; set; }

        public virtual Design Design { get; set; }

        private ICollection<DesignProductCustomFieldMapping> _designProductCustomFieldMappings;

        public virtual ICollection<DesignProductCustomFieldMapping> DesignProductCustomFieldMappings
        {
            get { return _designProductCustomFieldMappings ?? (_designProductCustomFieldMappings = new List<DesignProductCustomFieldMapping>()); }
            protected set { _designProductCustomFieldMappings = value; }
        }

    }
}
