﻿using Nop.Core.Domain.Customs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Designs
{
    public partial class DesignProductCustomFieldMapping:BaseEntity
    {
        public int DesignCustomFieldId { get; set; }

        public int ProductCustomFieldMappingId { get; set; }

        public DesignCustomField DesignCustomField { get; set; }

        public ProductCustomFieldMapping ProductCustomFieldMapping { get; set; }
    }
}
