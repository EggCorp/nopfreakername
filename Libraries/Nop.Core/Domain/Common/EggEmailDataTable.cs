﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Common
{
    public class EggEmailDataTable
    {
        public int Id { get; set; }
        public string ProductLink { get; set; }
        public string ImageUrl { get; set; }
        public string Name { get; set; }
        public string Desciption { get; set; }
    }
}
