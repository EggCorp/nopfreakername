﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Common
{
    public partial class TypedName : BaseEntity
    {
        public string Name { get; set; }

        public int CustomerId { get; set; }

        public int ProductId { get; set; }

        public int Count { get; set; }

        public DateTime CreatedOnTime { get; set; }
    }
}
