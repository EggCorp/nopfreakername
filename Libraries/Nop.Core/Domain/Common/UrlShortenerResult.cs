﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Common
{
    public partial class UrlShortenerResult
    {
        public string kind { get; set; }

        public string id { get; set; }

        public string longUrl { get; set; }    
    }
}
