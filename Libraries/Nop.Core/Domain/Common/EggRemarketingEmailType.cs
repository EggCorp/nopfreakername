﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Common
{
    public enum EggRemarketingEmailType
    {
        RegisterName= 1,
        GetCoupon =2,
        CheckoutUnsuccess =3,
        CheckoutSuccess =4,
        OrderShipped = 5,
        OrderProcessing=6,
        RemindShoppingCart= 7
    }
}
