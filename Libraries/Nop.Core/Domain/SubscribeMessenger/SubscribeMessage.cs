﻿using Nop.Core.Domain.Customers;
using System;

namespace Nop.Core.Domain.SubscribeMessenger
{
    public partial class SubscribeMessage : BaseEntity
    {
        public int? SubscribeCustomerId { get; set; }
        public int? SubscribeFollowId { get; set; }
        public string MessagesJson { get; set; }
        public int? SendingStatusId { get; set; }
        public string Note { get; set; }
        public int? Step { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public DateTime SendDateUtc { get; set; }
        public virtual SubscribeCustomer SubscribeCustomer { get;set;}
        public virtual SubscribeFollow SubscribeFollow { get; set; }
        public virtual bool? IsCompletedFollow { get; set; }

    }

    public enum SubscribeMesseageSendingStatus
    {
        New = 10,
        Error = 20,
        Completed = 30
    }
    
}
