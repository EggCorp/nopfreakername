﻿using Newtonsoft.Json;

namespace Nop.Core.Domain.SubscribeMessenger
{
    public class FbSendMessage
    {
        public FbSendMessage()
        {
            Recipient = new FbRecipient();
            Message = new FbMessage();
        }

        [JsonProperty("messaging_type")]
        public string MessagingType { get; set; } = "RESPONSE";


        [JsonProperty("recipient")]
        public FbRecipient Recipient { get; set; }

        [JsonProperty("message")]
        public FbMessage Message { get; set; }
    }

    public class FbReponseSendMessage
    {
        [JsonProperty("message_id")]
        public string MessageId { get; set; }

        [JsonProperty("error", NullValueHandling = NullValueHandling.Ignore)]
        public FbReponseError Error { get; set; }
    }

    public class FbReponseError
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("error_subcode")]
        public string ErrorSubcode { get; set; }
        [JsonProperty("fbtrace_id")]
        public string FbtraceId { get; set; }
    }

}
