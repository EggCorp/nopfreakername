﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Nop.Core.Domain.SubscribeMessenger
{
    public class FbMessage
    {
        public FbMessage()
        {
            Attachment = new FbAttachment();
        }
        [JsonProperty("attachment")]
        public FbAttachment Attachment { get; set; }
    }

    public class FbAttachment
    {
        public FbAttachment()
        {
            Payload = new FbPayload();
        }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("payload")]
        public FbPayload Payload { get; set; }
    }

    public class FbPayload
    {
        public FbPayload()
        {
            
        }

        [JsonProperty("template_type", NullValueHandling = NullValueHandling.Ignore)]
        public string TemplateType { get; set; }

        [JsonProperty("text", NullValueHandling = NullValueHandling.Ignore)]
        public string Text { get; set; }

        [JsonProperty("buttons", NullValueHandling = NullValueHandling.Ignore)]
        public List<FbButton> Buttons { get; set; }

        [JsonProperty("url", NullValueHandling = NullValueHandling.Ignore)]
        public string Url { get; set; }

        [JsonProperty("is_reusable", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsReusable { get; set; }

    }

    public class FbButton
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }
    }
}
