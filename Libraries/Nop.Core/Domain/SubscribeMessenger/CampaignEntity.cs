﻿namespace Nop.Core.Domain.SubscribeMessenger
{
    public partial class CampaignEntity : BaseEntity
    {
        public string EntityName { get; set; }
        public int EntityId { get; set; }
        public int CampaignMessengerId { get; set; }
        public int StatusId { get; set; }

        public virtual CampaignMessenger CampaignMessenger { get; set; }
    }
}
