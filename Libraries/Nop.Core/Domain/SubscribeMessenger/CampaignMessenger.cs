﻿using System;

namespace Nop.Core.Domain.SubscribeMessenger
{
    public partial class CampaignMessenger : BaseEntity
    {
        public string Name { get; set; }
        public string AttachmentUrl { get; set; }
        public string AttachmentType { get; set; }
        public string TextMessage { get; set; }
        public string Link { get; set; }
        public string CallToAction { get; set; }
        public int PageView { get; set; }
        public string ActionType { get; set; }
        public DateTime CreatedDateUTC { get; set; }
        public int StatusId { get; set; }
        public int TotalAudience { get; set; }
    }

    public enum CampaignStatus
    {
        New = 10,
        Processing = 20,
        ProcessingInterest = 25,
        ProcessingAudience = 26,
        Sending = 30,
        Completed = 40
    }

    public enum CampaignInterestType
    {
        Category,
        Keyword
    }

    public enum CampaignEntityType
    {
        Category,
        Product
    }

    public enum CampaignActionType
    {
        View,
        Add2Cart,
        Purchage
    }
}
