﻿namespace Nop.Core.Domain.SubscribeMessenger
{
    public partial class SubscribeFollow : BaseEntity
    {
        public string Name { get; set; }
        public string Note { get; set; }
        public int Priority { get; set; }
        public int NumberStep { get; set; }
    }

    public enum SubscribeFollowType
    {
        RemindViewProduct = 1,
        RemindAdd2Cart = 2,
        RemindPendingOrder = 3
    }
}
