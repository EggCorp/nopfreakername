﻿using Nop.Core.Domain.Customers;

namespace Nop.Core.Domain.SubscribeMessenger
{
    public partial class CampaignSubscribeCustomer : BaseEntity
    {
        public int CampaignMessengerId { get; set; }
        public int SubscribeCustomerId { get; set; }
        public int StatusId { get; set; }
        public virtual CampaignMessenger CampaignMessenger { get; set; }
        public virtual SubscribeCustomer SubscribeCustomer { get; set; }
    }
}
