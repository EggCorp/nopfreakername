﻿using System;

namespace Nop.Core.Domain.SubscribeMessenger
{
    public partial class CampaignInterest : BaseEntity
    {
        public string Name { get; set; }
        public int? CateogryId { get; set; }
        public string Keyword { get; set; }
        public int CampaignMessengerId { get; set; }
        public string Type { get; set; }
        public DateTime CreatedDateUTC { get; set; }
        public virtual CampaignMessenger CampaignMessenger { get; set; }
    }
}
