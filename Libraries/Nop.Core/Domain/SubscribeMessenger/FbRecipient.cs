﻿using Newtonsoft.Json;

namespace Nop.Core.Domain.SubscribeMessenger
{
    public class FbRecipient
    {
        [JsonProperty("user_ref")]
        public string UserRef { get; set; }
    }

    public class FbAttachmentType
    {
        public const string Image = "image";
        public const string Template = "template";
    }

    public class FbPayloadTemplateType
    {
        public const string Button = "button";
    }

    public class FbButtonType
    {
        public const string WebUrl = "web_url";
    }
}
