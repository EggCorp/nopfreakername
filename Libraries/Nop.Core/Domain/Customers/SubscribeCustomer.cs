﻿using Nop.Core.Domain.Catalog;
using System;

namespace Nop.Core.Domain.Customers
{
    public partial class SubscribeCustomer : BaseEntity
    {
        /// <summary>
        /// Gets or sets the customer identifier
        /// </summary>
        public int? CustomerId { get; set; }
        /// <summary>
        /// Gets or sets the product identifier
        /// </summary>
        public int? ProductId { get; set; }
        public string UserRef { get; set; }
        public string Token { get; set; }
        public DateTime? CreatedDate { get; set; } = DateTime.UtcNow;
        public DateTime? LatestSendDate { get; set; }
        public int? NumberMessage { get; set; } = 0;

        public virtual Customer Customer { get; set; }
        /// <summary>
        /// Gets the product
        /// </summary>
        public virtual Product Product { get; set; }
    }
}
