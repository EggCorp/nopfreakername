﻿using Nop.Core.Domain.Logging;
using System;

namespace Nop.Core.Domain.Customers
{
    public partial class SubscribeActivityLog : BaseEntity
    {
        public int? ActivityLogTypeId { get; set; }
        public int? SubscribeCustomerId { get; set; }
        public DateTime? CreatedOnUtc { get; set; } = DateTime.UtcNow;
        public string EntityName { get; set; }
        public int? EntityId { get; set; }
        public string Comment { get; set; }
        public virtual ActivityLogType ActivityLogType { get; set;}
        public virtual SubscribeCustomer SubscribeCustomer { get; set; }
    }
}
