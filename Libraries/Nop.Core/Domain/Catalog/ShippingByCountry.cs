﻿namespace Nop.Core.Domain.Catalog
{
    public partial class ShippingByCountry : BaseEntity
    {
        public int StoreId { get; set; }
        public string ProductType { get; set; }
        public string ProductCode { get; set; }
        public decimal USShippingPrice { get; set; }
        public decimal InternationalShippingPrice { get; set; }
        public decimal USAddedItemPrice { get; set; }
        public decimal InternationalAddedItemPrice { get; set; }
        public int Order { get; set; }
    }
}
