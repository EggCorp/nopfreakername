﻿using Nop.Core.Domain.Media;

namespace Nop.Core.Domain.Catalog
{
    public partial class HotTrending: BaseEntity
    {
        public string Name { get; set; }

        public string RedirectUrl { get; set; }

        public string InnerText { get; set; }

        public int DisplayOrder { get; set; }

        public bool IsLargeBanner { get; set; }

        public int PictureId { get; set; }

        public Picture Picture { get; set; }

        public bool IsSellOffBanner { get; set; }

    }
}
