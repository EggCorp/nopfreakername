﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Catalog
{
    public partial class ProductContentForAttributeValue
    {
        public ProductTypeInfo TypeInformation { get; set; }
        public int ProductAttributeValueId { get; set; }
        public bool IsPreselected { get; set; }
    }
}
