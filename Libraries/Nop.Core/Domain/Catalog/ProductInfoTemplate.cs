﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Catalog
{
    public partial class ProductInfoTemplate : BaseEntity
    {
        public string Name { get; set; }
        public string Content { get; set; }

        private ICollection<ProductTypeInfo> _productTypeInfos;

        public virtual ICollection<ProductTypeInfo> ProductTypes
        {
            get { return _productTypeInfos ?? (_productTypeInfos = new List<ProductTypeInfo>()); }
            protected set { _productTypeInfos = value; }
        }
    }
}
