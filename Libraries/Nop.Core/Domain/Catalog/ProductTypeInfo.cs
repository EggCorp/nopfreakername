﻿namespace Nop.Core.Domain.Catalog
{
    public partial class ProductTypeInfo : BaseEntity
    {
        public string TypeName { get; set; }
        public string TypeCode { get; set; }
        public string Description { get; set; }
        public int? ProductInfoTemplateId { get; set; }
        public int? ProductSizingInfoTemplateId { get; set; }
        public int? ProductShippingInfoTemplateId { get; set; }
        public bool IsActive { get; set; }
        public ProductSizingInfoTemplate ProductSizingInfoTemplate { get; set; }
        public ProductShippingInfoTemplate ProductShippingInfoTemplate { get; set; }
        public ProductInfoTemplate ProductInfoTemplate { get; set; } 

    }
}
