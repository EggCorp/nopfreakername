﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Customs
{
    public enum CustomFieldType
    {
        text=10,
        number=20,
        date=30,
        email = 40,
        tel =50
    }
}