﻿using Nop.Core.Domain.Designs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Customs
{
    public class CustomField : BaseEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public int TypeId { get; set; }

        public CustomFieldType Type
        {
            get
            {
                return (CustomFieldType)this.TypeId;
            }
            set
            {
                this.TypeId = (int)value;
            }
        }

        public virtual string TypedValue { get; set; }

    }
}
