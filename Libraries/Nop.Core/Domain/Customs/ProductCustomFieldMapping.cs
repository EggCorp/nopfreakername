﻿using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Designs;
using Nop.Core.Domain.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Customs
{
    public class ProductCustomFieldMapping : BaseEntity
    {
        private ICollection<ProductCustomFieldValue> _productCustomFieldValues;

        public int ProductId { get; set; }

        public int CustomFieldId { get; set; }

        public int Group { get; set; }

        public double Width { get; set; }

        public double Height { get; set; }

        public double FontSize { get; set; }

        public virtual Product Product { get; set; } 

        public virtual CustomField CustomField { get; set; }

        public virtual ICollection<ProductCustomFieldValue> ProductCustomFieldValues
        {
            get { return _productCustomFieldValues ?? (_productCustomFieldValues = new List<ProductCustomFieldValue>()); }
            protected set { _productCustomFieldValues = value; }
        }

        private ICollection<DesignProductCustomFieldMapping> _designProductCustomFieldMappings;

        public virtual ICollection<DesignProductCustomFieldMapping> DesignProductCustomFieldMappings
        {
            get { return _designProductCustomFieldMappings ?? (_designProductCustomFieldMappings = new List<DesignProductCustomFieldMapping>()); }
            protected set { _designProductCustomFieldMappings = value; }
        }

    }
}
