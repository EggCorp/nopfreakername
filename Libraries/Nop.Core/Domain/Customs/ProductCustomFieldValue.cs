﻿using Nop.Core.Domain.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Customs
{
    public class ProductCustomFieldValue : BaseEntity
    {
        public int ProductCustomFieldMappingId { get; set; }

        public int ProductAttributeValueId { get; set; }

        public int PositionX { get; set; }

        public int PositionY { get; set; }

        public virtual ProductCustomFieldMapping ProductCustomFieldMapping { get; set; }

        public virtual ProductAttributeValue ProductAttributeValue { get; set; }
    }
}
