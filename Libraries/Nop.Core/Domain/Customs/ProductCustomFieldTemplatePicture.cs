﻿using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Customs
{
    public class ProductCustomFieldTemplatePicture : BaseEntity
    {
        public int ProductAttributeValueId { get; set; }

        public int PictureId { get; set; }

        public virtual Picture Picture { get; set; }

        public virtual ProductAttributeValue ProductAttributeValue { get; set; }

    }
}
