﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Orders
{
    public class SunfrogTShirtInfo
    {
        public string ProductType { get; set; }

        public string ProductTypeLink { get; set; }
        
        public List<EggColor> ColorsOfType { get; set; }
    }

    public class EggColor
    {
        public string ColorUrl { get; set; }
        public string ColorImageUrl { get; set; }
        public string ColorName { get; set; }
    }
}
