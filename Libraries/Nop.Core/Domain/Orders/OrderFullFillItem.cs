﻿using Nop.Core.Domain.Common;

namespace Nop.Core.Domain.Orders
{
    public class OrderFullFillItem
    {
        public string ImageUrl { get; set; }

        public string OriginUrl { get; set; }

        public string ProductSKU { get; set; }

        public string Color { get; set; }

        public string ProductType { get; set; }

        public int PictureId { get; set; }

        public string ProductSize { get; set; }

        public int Quantity { get; set; }

        public string NameOnShirt { get; set; }

        public string NumberOnShirt { get; set; }

        public string TeamJersey { get; set; }

        public string VendorName { get; set; }

        public int ProductId { get; set; }

    }
}
