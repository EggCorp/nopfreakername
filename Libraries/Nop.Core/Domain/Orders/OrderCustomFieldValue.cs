﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Orders
{
    public class OrderCustomFieldValue: BaseEntity
    {
        public int OrderItemId { get; set; }
        public string FullFillSKU { get; set; }
        public string FullFillProductUrl { get; set; }
    }
}
