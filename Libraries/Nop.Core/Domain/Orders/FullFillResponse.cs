﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Orders
{
    public class FullFillResponse
    {
        public byte[] Data { get; set; }
        public List<int> SuccessIds { get; set; }

    }
}
