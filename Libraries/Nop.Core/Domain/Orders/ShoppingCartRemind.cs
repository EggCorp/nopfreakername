﻿using System;

namespace Nop.Core.Domain.Orders
{
    public partial class ShoppingCartRemind : BaseEntity
    {
        /// <summary>
        /// Gets or sets the customer identifier
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Get or set number email message was sent
        /// </summary>
        public int NumberMessage { get; set; }

        /// <summary>
        /// Get or set status of remind record
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// Get or set Create Date UTC
        /// </summary>
        public DateTime CreatedOnUtc { get; set; }
    }
}
