namespace Nop.Core.Domain.Orders
{
    /// <summary>
    /// Represents an order status enumeration
    /// </summary>
    public enum OrderStatus
    {
        /// <summary>
        /// Pending
        /// </summary>
        Pending = 10,
        /// <summary>
        /// Processing
        /// </summary>
        Processing = 20,
        /// <summary>
        /// waiting uploading art to sunfrog
        /// </summary>
        Uploading = 21,
        /// <summary>
        /// Uploaded art to sunfrog. Waiting get Sunfrog SKU for order
        /// </summary>
        Uploaded = 22,
        /// <summary>
        /// Order has been export full fill
        /// </summary>
        ExportFullFill = 23,
        /// <summary>
        /// Order has been full filled
        /// </summary>
        FullFilled = 24,
        /// Order has multi vendor items
        /// </summary>
        MultiVendor = 25,
        /// <summary>
        /// Complete
        /// </summary>
        Complete = 30,
        /// <summary>
        /// Cancelled
        /// </summary>
        Cancelled = 40
    }
}
