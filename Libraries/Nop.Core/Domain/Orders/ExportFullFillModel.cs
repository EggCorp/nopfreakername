﻿using Nop.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Orders
{
    public class GroupedExportFullFullModel
    {
        public GroupedExportFullFullModel()
        {
            ExportFullFillModels = new List<ExportFullFillModel>();
        }
        public Address GroupedAddress { get; set; }

        public bool IsValidAddress { get; set; }
        public List<ExportFullFillModel> ExportFullFillModels { get; set; }
    }
    public class ExportFullFillModel
    {
        public ExportFullFillModel()
        {
            Items = new List<OrderFullFillItem>();
        }
        public  int OrderId { get; set; }
        public  List<OrderFullFillItem> Items { get; set; }
        public Address ShippingAddress { get; set; }
        public decimal OrderShippingPrice { get; set; }
    }
}
