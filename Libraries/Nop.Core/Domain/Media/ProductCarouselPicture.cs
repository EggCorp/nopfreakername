﻿namespace Nop.Core.Domain.Media
{
    public class ProductCarouselPicture
    {
        public string ImageUrl { get; set; }

        public string ImageThumbUrl { get; set; }

        public string ImageSizeMDUrl { get; set; }

        public bool IsSeclected { get; set; }

        public int DisplayIndex { get; set; }

        public int AttributeMappingId { get; set; }

        public int AttributeValueId { get; set; }

        public string ProductType { get; set; }
    }
}
