﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Media
{
    //
    // Summary:
    //     Specifies text alignment (left, center, or right for horizontal text; top, center,
    //     or bottom for vertical text).
    //
    // Remarks:
    //     Alignment of each text is performed relatively to its location.
    public enum ArtAlignment
    {
        //
        // Summary:
        //     Aligns horizontal text to the left side.
        Left = 0,
        //
        // Summary:
        //     Aligns vertical text to the top.
        Top = 1,
        //
        // Summary:
        //     Aligns horizontal or vertical text to the center.
        Center = 2,
        //
        // Summary:
        //     Aligns horizontal text to the right side.
        Right = 3,
        //
        // Summary:
        //     Aligns vertical text to the bottom.
        Bottom = 4,
        //
        // Summary:
        //     Distributes horizontal or vertical text along the text area.
        Justification = 5
    }
}
