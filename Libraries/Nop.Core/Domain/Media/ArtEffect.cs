﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Media
{
    public enum ArtEffect
    {
        None = 0,
        Glow = 1,
        Shadow = 2
    }
}
