﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Media
{
    public enum ArtType
    {
        None = 0,
        BridgeText = 1,
        BulgeText = 2,
        PinchText = 3,
        RoofText = 4,
        ValleyText = 5,
        WedgeText = 6,
        RoundText = 7,
       
    }
}
