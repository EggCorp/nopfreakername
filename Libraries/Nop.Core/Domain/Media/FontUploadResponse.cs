﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Media
{
    public partial class FontUploadResponse
    {
        public bool Success { get; set; }

        public string Message { get; set; }

        public string FontName { get; set; }
    }
}
