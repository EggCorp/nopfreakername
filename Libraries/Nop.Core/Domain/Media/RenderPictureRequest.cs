﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Media
{
    public partial class RenderPictureRequest
    {
        public RenderPictureRequest()
        {
            Data = new List<CustomData>();
        }

        public int AttrValueId { get; set; }

        public List<CustomData> Data { get; set; }

        public int Scale { get; set; }

        public partial class CustomData
        {
            public string Text { get; set; }

            public int Id { get; set; }

            public int ValueId { get; set; }
        }
    }
}
