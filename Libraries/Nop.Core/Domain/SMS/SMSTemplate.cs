﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.SMS
{
    public partial class SMSTemplate : BaseEntity
    {
        public string SMSType { get; set; }

        public string TemplateBody { get; set; }

        public bool IsDeleted { get; set; }
    }
}
