﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.SMS
{
    public partial class SMSMarketing : BaseEntity
    {
        public int OrderId { get; set; }

        public string SMSType { get; set; }

        public bool IsCompleted { get; set; }

        public DateTime? SentOnUtc { get; set; }

        public string Body { get; set; }

        public string From { get; set; }

        public string To { get; set; }

    }
}
