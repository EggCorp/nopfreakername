﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.SMS
{
    public partial class SMSConfiguration : BaseEntity
    {
        public string AccountSid { get; set; }

        public string AuthToken { get; set; }

        public string PhoneNumber { get; set; }

        public int Qouta { get; set; }

        public int SendCount { get; set; }

        public bool IsDisable { get; set; }

    }
}
