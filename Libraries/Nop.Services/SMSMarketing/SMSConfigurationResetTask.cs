﻿using Nop.Services.Orders;
using Nop.Services.Catalog;
using Nop.Services.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Services.Logging;
using Nop.Services.SMS;

namespace Nop.Services.SMS
{
    public partial class SMSConfigurationResetTask : ITask
    {
        private readonly ISMSMarketingService _smsMarketingService;
        private readonly ILogger _logger;
        public SMSConfigurationResetTask(
            ISMSMarketingService smsMarketingService,
            ILogger logger)
        {
            this._smsMarketingService = smsMarketingService;
            this._logger = logger;
        }

        /// <summary>
        /// Executes a task
        /// </summary>
        public virtual void Execute()
        {
            try
            {
                this._smsMarketingService.ResetSMSConfiguration();
            }
            catch (Exception ex)
            {
                _logger.Error(string.Format("Error run task reset sms config: {0}", ex.Message));
            }

        }
    }
}
