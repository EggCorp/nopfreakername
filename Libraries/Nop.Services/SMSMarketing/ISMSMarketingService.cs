﻿using Nop.Core;
using Nop.Core.Domain.SMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.SMS
{
    public partial interface ISMSMarketingService
    {
        void InsertSMSMarketing(SMSMarketing SMSMarketing);

        /// <summary>
        /// Updates a queued email
        /// </summary>
        /// <param name="SMSMarketing">Queued email</param>
        void UpdateSMSMarketing(SMSMarketing SMSMarketing);

        /// <summary>
        /// Deleted a queued email
        /// </summary>
        /// <param name="SMSMarketing">Queued email</param>
        void DeleteSMSMarketing(SMSMarketing SMSMarketing);

        /// <summary>
        /// Deleted a queued emails
        /// </summary>
        /// <param name="SMSMarketings">Queued emails</param>
        void DeleteSMSMarketings(IList<SMSMarketing> SMSMarketings);

        /// <summary>
        /// Gets a queued email by identifier
        /// </summary>
        /// <param name="SMSMarketingId">Queued email identifier</param>
        /// <returns>Queued email</returns>
        SMSMarketing GetSMSMarketingById(int SMSMarketingId);

        /// <summary>
        /// Get queued emails by identifiers
        /// </summary>
        /// <param name="SMSMarketingIds">queued email identifiers</param>
        /// <returns>Queued emails</returns>
        IList<SMSMarketing> GetSMSMarketingsByIds(int[] SMSMarketingIds);
        IList<SMSMarketing> SearchSMSMarketings(SMSType? type, string to, bool showCompleted, int orderId =0);
        void DeleteCompleted();

        void CreateSMSTemplate(SMSTemplate template);

        void UpdateSMSTemplate(SMSTemplate template);

        void DeleteSMSTemplate(SMSTemplate template);

        List<SMSTemplate> GetSMSTemplates();

        SMSTemplate GetSMSTemplateById(int id);

        SMSTemplate GetTemplateForAction(SMSType type);

        void OnSendSMSMarketing(int orderId, SMSType type);

        void SendSMSMarketing(string to, string message);

        void SendCustomizationSMS(string to,string name, SMSType type);
        void SendCustomizationSMS(string to, string message);

        void CreateSMSConfiguration(SMSConfiguration template);

        void UpdateSMSConfiguration(SMSConfiguration template);

        void DeleteSMSConfiguration(SMSConfiguration template);

        List<SMSConfiguration> GetSMSConfigurations();

        SMSConfiguration GetSMSConfigurationById(int id);

        SMSConfiguration GetAvailableSMSConfiguration();

        void ResetSMSConfiguration();

    }
}
