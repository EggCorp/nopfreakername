﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Common;
using Nop.Data;
using Nop.Services.Events;
using Nop.Services.Orders;
using Nop.Services.Catalog;
using Nop.Services.Localization;
using Nop.Services.Helpers;
using Nop.Services.Directory;
using Nop.Services.Media;
using Nop.Services.Payments;
using Nop.Services.Common;
using Nop.Services.Stores;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Tax;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Shipping;
using Nop.Core.Domain;
using Nop.Core.Domain.Orders;
using System.Text;
using System.Web;
using Nop.Services.Seo;
using Nop.Services.Shipping;
using Nop.Services.Messages;
using Twilio;
using Twilio.Types;
using Twilio.Rest.Api.V2010.Account;
using Nop.Core.Domain.SMS;
using Nop.Core.Domain.Messages;
using Nop.Services.Logging;

namespace Nop.Services.SMS
{
    public partial class SMSMarketingService : ISMSMarketingService
    {
        private readonly IRepository<SMSMarketing> _sMSMarketingRepository;
        private readonly IRepository<SMSTemplate> _smsTemplateRepository;
        private readonly IDbContext _dbContext;
        private readonly IDataProvider _dataProvider;
        private readonly CommonSettings _commonSettings;
        private readonly IEventPublisher _eventPublisher;
        private readonly IOrderService _orderService;
        private readonly IProductService _productService;
        private readonly ILanguageService _languageService;
        private readonly ILocalizationService _localizationService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IPriceFormatter _priceFormatter;
        private readonly ICurrencyService _currencyService;
        private readonly IWorkContext _workContext;
        private readonly IDownloadService _downloadService;
        private readonly IPaymentService _paymentService;
        private readonly IProductAttributeParser _productAttributeParser;
        private readonly IAddressAttributeFormatter _addressAttributeFormatter;
        private readonly IPictureService _pictureService;
        private readonly IStoreService _storeService;
        private readonly IStoreContext _storeContext;
        private readonly ITokenizer _tokenizer;
        private readonly IShipmentService _shipmentService;
        private readonly CatalogSettings _catalogSettings;
        private readonly TaxSettings _taxSettings;
        private readonly CurrencySettings _currencySettings;
        private readonly ShippingSettings _shippingSettings;
        private readonly StoreInformationSettings _storeInformationSettings;
        private readonly ILogger _logger;
        private readonly IUrlShortenerHelper _urlShortenerHelper;
        private readonly IRepository<SMSConfiguration> _smsConfigurationRepository;

        public SMSMarketingService(
            IEventPublisher eventPublisher,
            IDbContext dbContext,
            IDataProvider dataProvider,
            CommonSettings commonSettings,
            IOrderService orderService,
            IProductService productService,
            ILanguageService languageService,
            ILocalizationService localizationService,
            IDateTimeHelper dateTimeHelper,
            IPriceFormatter priceFormatter,
            ICurrencyService currencyService,
            IWorkContext workContext,
            IDownloadService downloadService,
            IPaymentService paymentService,
            IPictureService pictureService,
            IStoreService storeService,
            IStoreContext storeContext,
            IProductAttributeParser productAttributeParser,
            IAddressAttributeFormatter addressAttributeFormatter,
            IShipmentService shipmentService,
            ITokenizer tokenizer,
            CatalogSettings catalogSettings,
            TaxSettings taxSettings,
            CurrencySettings currencySettings,
            ShippingSettings shippingSettings,
            StoreInformationSettings storeInformationSettings,
            IRepository<SMSMarketing> sMSMarketingRepository,
            IRepository<SMSTemplate> smsTemplateRepository,
            ILogger logger,
            IUrlShortenerHelper urlShortenerHelper,
            IRepository<SMSConfiguration> smsConfigurationRepository
            )
        {
            _eventPublisher = eventPublisher;
            this._dbContext = dbContext;
            this._dataProvider = dataProvider;
            this._commonSettings = commonSettings;
            this._orderService = orderService;
            this._productService = productService;
            this._languageService = languageService;
            this._localizationService = localizationService;
            this._dateTimeHelper = dateTimeHelper;
            this._priceFormatter = priceFormatter;
            this._currencyService = currencyService;
            this._workContext = workContext;
            this._downloadService = downloadService;
            this._paymentService = paymentService;
            this._productAttributeParser = productAttributeParser;
            this._addressAttributeFormatter = addressAttributeFormatter;
            this._storeService = storeService;
            this._storeContext = storeContext;
            this._pictureService = pictureService;
            this._tokenizer = tokenizer;
            this._shipmentService = shipmentService;

            this._catalogSettings = catalogSettings;
            this._taxSettings = taxSettings;
            this._currencySettings = currencySettings;
            this._shippingSettings = shippingSettings;
            this._storeInformationSettings = storeInformationSettings;
            this._sMSMarketingRepository = sMSMarketingRepository;
            this._smsTemplateRepository = smsTemplateRepository;
            this._logger = logger;
            this._urlShortenerHelper = urlShortenerHelper;
            this._smsConfigurationRepository = smsConfigurationRepository;
        }

        protected virtual void SMSManagerInit(string accountSid, string authToken)
        {
            try
            {
                TwilioClient.Init(accountSid, authToken);
            }
            catch (Exception ex)
            {
                _logger.Error(string.Format("Error Init SMS account: {0}", ex.Message));
            }
        }

        /// <summary>
        /// Inserts a smsMarketing
        /// </summary>
        /// <param name="queuedEmail">smsMarketing</param>        
        public virtual void InsertSMSMarketing(SMSMarketing smsMarketing)
        {
            if (smsMarketing == null)
                throw new ArgumentNullException("smsMarketing");

            _sMSMarketingRepository.Insert(smsMarketing);

            //event notification
            _eventPublisher.EntityInserted(smsMarketing);
        }

        /// <summary>
        /// Updates a smsMarketing email
        /// </summary>
        /// <param name="smsMarketing">Queued email</param>
        public virtual void UpdateSMSMarketing(SMSMarketing smsMarketing)
        {
            if (smsMarketing == null)
                throw new ArgumentNullException("smsMarketing");

            _sMSMarketingRepository.Update(smsMarketing);

            //event notification
            _eventPublisher.EntityUpdated(smsMarketing);
        }

        /// <summary>
        /// Deleted a smsMarketing email
        /// </summary>
        /// <param name="smsMarketing">Queued email</param>
        public virtual void DeleteSMSMarketing(SMSMarketing smsMarketing)
        {
            if (smsMarketing == null)
                throw new ArgumentNullException("smsMarketing");

            _sMSMarketingRepository.Delete(smsMarketing);

            //event notification
            _eventPublisher.EntityDeleted(smsMarketing);
        }

        /// <summary>
        /// Deleted a smsMarketing emails
        /// </summary>
        /// <param name="smsMarketings">Queued emails</param>
        public virtual void DeleteSMSMarketings(IList<SMSMarketing> smsMarketings)
        {
            if (smsMarketings == null)
                throw new ArgumentNullException("smsMarketings");

            _sMSMarketingRepository.Delete(smsMarketings);

        }

        /// <summary>
        /// Gets a smsMarketing email by identifier
        /// </summary>
        /// <param name="smsMarketingId">Queued email identifier</param>
        /// <returns>Queued email</returns>
        public virtual SMSMarketing GetSMSMarketingById(int smsMarketingId)
        {
            if (smsMarketingId == 0)
                return null;

            return _sMSMarketingRepository.GetById(smsMarketingId);

        }

        /// <summary>
        /// Get smsMarketing emails by identifiers
        /// </summary>
        /// <param name="smsMarketingIds">smsMarketing email identifiers</param>
        /// <returns>Queued emails</returns>
        public virtual IList<SMSMarketing> GetSMSMarketingsByIds(int[] smsMarketingIds)
        {
            if (smsMarketingIds == null || smsMarketingIds.Length == 0)
                return new List<SMSMarketing>();

            var query = from qe in _sMSMarketingRepository.Table
                        where smsMarketingIds.Contains(qe.Id)
                        select qe;
            var smsMarketings = query.ToList();
            //sort by passed identifiers
            var sortedSMSMarketings = new List<SMSMarketing>();
            foreach (int id in smsMarketingIds)
            {
                var smsMarketing = smsMarketings.Find(x => x.Id == id);
                if (smsMarketing != null)
                    sortedSMSMarketings.Add(smsMarketing);
            }
            return sortedSMSMarketings;
        }

        public virtual IList<SMSMarketing> SearchSMSMarketings(SMSType? type, string to, bool showCompleted, int orderId = 0)
        {
            var query = _sMSMarketingRepository.Table;
            query = query.OrderByDescending(p => p.SentOnUtc);

            if (!showCompleted)
                query = query.Where(p => !p.IsCompleted);
            if (type.HasValue)
                query = query.Where(p => p.SMSType.Equals(type.ToString()));
            if (orderId > 0)
                query = query.Where(p => p.OrderId == orderId);
            if (!String.IsNullOrEmpty(to))
                query = query.Where(p => p.To.Contains(to));
            var result = query.ToList();
            return result;
        }

        public virtual void DeleteCompleted()
        {
            try
            {
                var smsRemarketings = _sMSMarketingRepository.Table.Where(p => p.IsCompleted).ToList();
                DeleteSMSMarketings(smsRemarketings);
            }
            catch (Exception ex)
            { }
        }

        public virtual void CreateSMSTemplate(SMSTemplate template)
        {
            if (template == null)
                throw new ArgumentNullException("SMSTemplate");
            template.IsDeleted = false;
            _smsTemplateRepository.Insert(template);
        }

        public virtual void UpdateSMSTemplate(SMSTemplate template)
        {
            if (template == null)
                throw new ArgumentNullException("SMSTemplate");
            _smsTemplateRepository.Update(template);
        }
        public virtual void DeleteSMSTemplate(SMSTemplate template)
        {
            if (template == null)
                throw new ArgumentNullException("SMSTemplate");
            template.IsDeleted = true;
            _smsTemplateRepository.Update(template);
        }

        public virtual List<SMSTemplate> GetSMSTemplates()
        {
            var query = _smsTemplateRepository.Table.Where(t => !t.IsDeleted);
            return query.ToList();
        }
        public virtual SMSTemplate GetSMSTemplateById(int id)
        {
            var query = _smsTemplateRepository.Table.Where(t => !t.IsDeleted && t.Id == id);
            return query.FirstOrDefault();
        }

        public virtual SMSTemplate GetTemplateForAction(SMSType type)
        {
            var query = _smsTemplateRepository.Table.Where(t => !t.IsDeleted && t.SMSType.Equals(type.ToString()));
            return query.FirstOrDefault();
        }
        protected string Replace(string original, string pattern, string replacement)
        {
            return original.Replace(pattern, replacement);
        }

        public virtual void OnSendSMSMarketing(int orderId, SMSType type)
        {
            try
            {
                var order = _orderService.GetOrderById(orderId);
                if (order != null)
                {
                    if (order.BillingAddress != null)
                    {
                        if (!string.IsNullOrEmpty(order.BillingAddress.PhoneNumber))
                        {
                            var template = GetTemplateForAction(type);
                            if (template != null)
                            {
                                var smsBody = template.TemplateBody;
                                smsBody = Replace(smsBody, EggEmailPattern.CustomerNamePartten, order.BillingAddress.FirstName);

                                string storeUrl = _storeContext.CurrentStore.Url;
                                storeUrl = storeUrl.Replace("https://", string.Empty);
                                storeUrl = storeUrl.Replace("http://", string.Empty);
                                storeUrl = storeUrl.Replace("/", string.Empty);
                                smsBody = Replace(smsBody, EggEmailPattern.StoreUrl, storeUrl);
                                if (type == SMSType.Checkout)
                                {
                                    string cartUrl = string.Format("{0}yourcart?cartcode={1}", _storeContext.CurrentStore.Url, order.OrderGuid);
                                    string shortCartUrl = _urlShortenerHelper.UrlShorter(cartUrl);
                                    smsBody = Replace(smsBody, EggEmailPattern.CustomerOldCartUrl, shortCartUrl);
                                }
                                var customerPhoneNumber = order.BillingAddress.PhoneNumber;
                                SMSMarketing smsMarketing = new SMSMarketing
                                {
                                    From = _storeContext.CurrentStore.Name,
                                    To = customerPhoneNumber,
                                    Body = smsBody,
                                    SMSType = type.ToString(),
                                    IsCompleted = false,
                                    OrderId = order.Id,
                                };
                                _sMSMarketingRepository.Insert(smsMarketing);
                                SendSMSMarketing(customerPhoneNumber, smsBody);
                                smsMarketing.IsCompleted = true;
                                smsMarketing.SentOnUtc = DateTime.UtcNow;
                                UpdateSMSMarketing(smsMarketing);
                            }
                        }
                        else
                        {
                            _logger.Warning(string.Format("Order #{0} dont have a phonenumber", order.Id));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(string.Format("Error send SMS : {0}", ex.Message));
            }
        }

        public virtual void SendSMSMarketing(string to, string message)
        {
            try
            {
                //TwilioClient.Init(_storeInformationSettings.SMSAccountSid, _storeInformationSettings.SMSAuthToken);
                var smsConfig = GetAvailableSMSConfiguration();
                if (smsConfig != null)
                {
                    SMSManagerInit(smsConfig.AccountSid, smsConfig.AuthToken);
                    var toPhoneNumber = new PhoneNumber(to);
                    var from = new PhoneNumber(smsConfig.PhoneNumber);
                    var body = MessageResource.Create(to, from: from, body: message);
                    IncreaseSendCount(smsConfig.Id, message);
                    _logger.Information(string.Format("Sent SMS to : {0}", to));
                }
                else
                    _logger.Warning("SMS phonenumber is out of qouta. Update new phonenumber");
            }
            catch (Exception ex)
            {
                _logger.Error(string.Format("Error send SMS : {0}", ex.Message));
            }
        }

        public virtual void SendCustomizationSMS(string to, string name, SMSType type)
        {
            try
            {
                var template = GetTemplateForAction(type);
                if (template != null)
                {
                    var smsBody = template.TemplateBody;
                    smsBody = Replace(smsBody, EggEmailPattern.CustomerNamePartten, name);
                    string storeUrl = _storeContext.CurrentStore.Url;
                    storeUrl = storeUrl.Replace("https://", string.Empty);
                    storeUrl = storeUrl.Replace("http://", string.Empty);
                    storeUrl = storeUrl.Replace("/", string.Empty);
                    smsBody = Replace(smsBody, EggEmailPattern.StoreUrl, storeUrl);
                    SMSMarketing smsMarketing = new SMSMarketing
                    {
                        From = _storeContext.CurrentStore.Name,
                        To = to,
                        Body = smsBody,
                        SMSType = type.ToString(),
                        IsCompleted = false,
                        OrderId = 0,
                        SentOnUtc = null
                    };
                    _sMSMarketingRepository.Insert(smsMarketing);
                    //sending
                    SendSMSMarketing(to, smsBody);
                    smsMarketing.IsCompleted = true;
                    smsMarketing.SentOnUtc = DateTime.UtcNow;
                    UpdateSMSMarketing(smsMarketing);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(string.Format("Error send SMS : {0}", ex.Message));
            }
        }
        public virtual void SendCustomizationSMS(string to, string message)
        {
            try
            {
                var smsConfig = GetAvailableSMSConfiguration();
                if (smsConfig != null)
                {
                    SMSManagerInit(smsConfig.AccountSid, smsConfig.AuthToken);
                    var toPhoneNumber = new PhoneNumber(to);
                    var from = new PhoneNumber(smsConfig.PhoneNumber);
                    var body = MessageResource.Create(to, from: from, body: message);
                    IncreaseSendCount(smsConfig.Id, message);
                    _logger.Information(string.Format("Sent SMS to : {0}", to));
                    SMSMarketing smsMarketing = new SMSMarketing
                    {
                        From = _storeContext.CurrentStore.Name,
                        To = to,
                        Body = message,
                        SMSType = SMSType.Custom.ToString(),
                        IsCompleted = false,
                        OrderId = 0,
                    };
                    _sMSMarketingRepository.Insert(smsMarketing);
                }
                else
                    _logger.Warning("SMS phonenumber is out of qouta. Update new phonenumber");
            }
            catch (Exception ex)
            {
                _logger.Error(string.Format("Error send SMS : {0}", ex.Message));
            }
        }

        public virtual void CreateSMSConfiguration(SMSConfiguration conf)
        {
            if (conf == null)
                throw new ArgumentNullException("SMSConfiguration");
            conf.IsDisable = false;
            conf.SendCount = 0;
            _smsConfigurationRepository.Insert(conf);
        }

        public virtual void UpdateSMSConfiguration(SMSConfiguration conf)
        {
            if (conf == null)
                throw new ArgumentNullException("SMSConfiguration");
            _smsConfigurationRepository.Update(conf);
        }
        public virtual void DeleteSMSConfiguration(SMSConfiguration conf)
        {
            if (conf == null)
                throw new ArgumentNullException("SMSConfiguration");
            _smsConfigurationRepository.Delete(conf);
        }

        public virtual List<SMSConfiguration> GetSMSConfigurations()
        {
            var query = _smsConfigurationRepository.Table.OrderBy(p => p.SendCount);
            return query.ToList();
        }

        public virtual SMSConfiguration GetSMSConfigurationById(int id)
        {
            var query = _smsConfigurationRepository.Table.Where(t => !t.IsDisable && t.Id == id);
            return query.FirstOrDefault();
        }

        public virtual SMSConfiguration GetAvailableSMSConfiguration()
        {
            try
            {
                var query = _smsConfigurationRepository.Table.Where(t => !t.IsDisable);
                query = query.Where(p => p.SendCount <= p.Qouta).OrderByDescending(p => p.SendCount);
                return query.FirstOrDefault();
            }
            catch (Exception ex)
            {
                _logger.Error(string.Format("Error get sms config: {0}", ex.Message));
            }
            return null;
        }

        protected virtual void IncreaseSendCount(int id, string message)
        {
            try
            {
                var config = GetSMSConfigurationById(id);
                if (config != null)
                {
                    int smsLength = CalculateSmsLength(message);
                    config.SendCount = config.SendCount + smsLength;
                    if (config.SendCount >= config.Qouta)
                        config.IsDisable = true;
                }
                UpdateSMSConfiguration(config);
            }
            catch (Exception ex)
            { }
        }

        protected virtual int CalculateSmsLength(string text)
        {
            if (string.IsNullOrEmpty(text))
                return 0;
            try
            {
                text = text.Trim();
                return text.Length <= 160 ? 1 : Convert.ToInt32(Math.Ceiling(Convert.ToDouble(text.Length) / 160));
            }
            catch
            {
                return 1;
            }
        }

        public virtual void ResetSMSConfiguration()
        {
            try
            {
                var smsConfigs = _smsConfigurationRepository.Table
                    .Where(p => p.IsDisable || p.SendCount > 0).ToList();
                foreach (var config in smsConfigs)
                {
                    config.SendCount = 0;
                    config.IsDisable = false;
                    this.UpdateSMSConfiguration(config);
                }
                _logger.Information(string.Format("Reset sms configs - : {0}", DateTime.Now));
            }
            catch (Exception ex)
            {
                _logger.Error(string.Format("Error reset sms config: {0}", ex.Message));
            }
        }
    }
}
