﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Messages;
using Nop.Data;
using Nop.Services.Events;
using Nop.Services.Orders;
using Nop.Services.Catalog;
using Nop.Services.Localization;
using Nop.Services.Helpers;
using Nop.Services.Directory;
using Nop.Services.Media;
using Nop.Services.Payments;
using Nop.Services.Common;
using Nop.Services.Stores;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Tax;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Shipping;
using Nop.Core.Domain;
using Nop.Core.Domain.Orders;
using System.Text;
using System.Web;
using Nop.Services.Seo;
using Nop.Services.Shipping;
using Nop.Services.Logging;
using Nop.Services.SMS;
using Nop.Core.Domain.SMS;
using Nop.Core.Domain.Customers;
using Nop.Services.Customers;

namespace Nop.Services.Messages
{
    public partial class EggReMarketingEmailService : IEggReMarketingEmailService
    {
        private readonly IRepository<EggReMarketingEmail> _eggReMarketingEmaillRepository;
        private readonly IDbContext _dbContext;
        private readonly IDataProvider _dataProvider;
        private readonly CommonSettings _commonSettings;
        private readonly IEventPublisher _eventPublisher;
        private readonly IRepository<MessageTemplate> _messageTemplateRepository;
        private readonly IOrderService _orderService;
        private readonly IProductService _productService;
        private readonly IEmailAccountService _emailAccountService;
        private readonly ILanguageService _languageService;
        private readonly ILocalizationService _localizationService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IPriceFormatter _priceFormatter;
        private readonly ICurrencyService _currencyService;
        private readonly IWorkContext _workContext;
        private readonly IDownloadService _downloadService;
        private readonly IPaymentService _paymentService;
        private readonly IProductAttributeParser _productAttributeParser;
        private readonly IAddressAttributeFormatter _addressAttributeFormatter;
        private readonly IPictureService _pictureService;
        private readonly IStoreService _storeService;
        private readonly IStoreContext _storeContext;
        private readonly ITokenizer _tokenizer;
        private readonly IShipmentService _shipmentService;
        private readonly IMessageTokenProvider _messageTokenProvider;
        private readonly MessageTemplatesSettings _templatesSettings;
        private readonly CatalogSettings _catalogSettings;
        private readonly TaxSettings _taxSettings;
        private readonly CurrencySettings _currencySettings;
        private readonly ShippingSettings _shippingSettings;
        private readonly StoreInformationSettings _storeInformationSettings;
        private readonly EmailAccountSettings _emailAccountSettings;
        private readonly IQueuedEmailService _queuedEmailService;
        private readonly ILogger _logger;
        private readonly ISMSMarketingService _smsMarketingService;
        private readonly ICustomerService _customerService;
        private readonly IPriceCalculationService _priceCalculationService;
        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="queuedEmailRepository">Queued email repository</param>
        /// <param name="eventPublisher">Event published</param>
        /// <param name="dbContext">DB context</param>
        /// <param name="dataProvider">WeData provider</param>
        /// <param name="commonSettings">Common settings</param>
        public EggReMarketingEmailService(IRepository<EggReMarketingEmail> eggReMarketingEmaillRepository,
            IEventPublisher eventPublisher,
            IDbContext dbContext,
            IDataProvider dataProvider,
            CommonSettings commonSettings,
            IRepository<MessageTemplate> messageTemplateRepository,
            IOrderService orderService,
            IProductService productService,
            IEmailAccountService emailAccountService,
            EmailAccountSettings emailAccountSettings,
            ILanguageService languageService,
            ILocalizationService localizationService,
            IDateTimeHelper dateTimeHelper,
            IPriceFormatter priceFormatter,
            ICurrencyService currencyService,
            IWorkContext workContext,
            IDownloadService downloadService,
            IPaymentService paymentService,
            IPictureService pictureService,
            IStoreService storeService,
            IStoreContext storeContext,
            IProductAttributeParser productAttributeParser,
            IAddressAttributeFormatter addressAttributeFormatter,
            IShipmentService shipmentService,
            ITokenizer tokenizer,
            IMessageTokenProvider messageTokenProvider,
            MessageTemplatesSettings templatesSettings,
            CatalogSettings catalogSettings,
            TaxSettings taxSettings,
            CurrencySettings currencySettings,
            ShippingSettings shippingSettings,
            StoreInformationSettings storeInformationSettings,
            IQueuedEmailService queuedEmailService,
            ILogger logger,
            ISMSMarketingService smsMarketingService,
            ICustomerService customerService,
            IPriceCalculationService priceCalculationService)
        {
            _eggReMarketingEmaillRepository = eggReMarketingEmaillRepository;
            _eventPublisher = eventPublisher;
            this._dbContext = dbContext;
            this._dataProvider = dataProvider;
            this._commonSettings = commonSettings;
            this._messageTemplateRepository = messageTemplateRepository;
            this._orderService = orderService;
            this._productService = productService;
            this._emailAccountService = emailAccountService;
            this._emailAccountSettings = emailAccountSettings;
            this._languageService = languageService;
            this._localizationService = localizationService;
            this._dateTimeHelper = dateTimeHelper;
            this._priceFormatter = priceFormatter;
            this._currencyService = currencyService;
            this._workContext = workContext;
            this._downloadService = downloadService;
            this._paymentService = paymentService;
            this._productAttributeParser = productAttributeParser;
            this._addressAttributeFormatter = addressAttributeFormatter;
            this._storeService = storeService;
            this._storeContext = storeContext;
            this._pictureService = pictureService;
            this._tokenizer = tokenizer;
            this._messageTokenProvider = messageTokenProvider;
            this._shipmentService = shipmentService;
            this._queuedEmailService = queuedEmailService;

            this._templatesSettings = templatesSettings;
            this._catalogSettings = catalogSettings;
            this._taxSettings = taxSettings;
            this._currencySettings = currencySettings;
            this._shippingSettings = shippingSettings;
            this._storeInformationSettings = storeInformationSettings;
            this._logger = logger;
            this._smsMarketingService = smsMarketingService;

            this._customerService = customerService;
            this._priceCalculationService = priceCalculationService;
        }

        /// <summary>
        /// Inserts a eggReMarketingEmail
        /// </summary>
        /// <param name="queuedEmail">eggReMarketingEmail</param>        
        public virtual void InsertEggReMarketingEmail(EggReMarketingEmail eggReMarketingEmail)
        {
            if (eggReMarketingEmail == null)
                throw new ArgumentNullException("eggReMarketingEmail");

            _eggReMarketingEmaillRepository.Insert(eggReMarketingEmail);

            //event notification
            _eventPublisher.EntityInserted(eggReMarketingEmail);
        }

        /// <summary>
        /// Updates a eggReMarketingEmail email
        /// </summary>
        /// <param name="eggReMarketingEmail">Queued email</param>
        public virtual void UpdateEggReMarketingEmail(EggReMarketingEmail eggReMarketingEmail)
        {
            if (eggReMarketingEmail == null)
                throw new ArgumentNullException("eggReMarketingEmail");

            _eggReMarketingEmaillRepository.Update(eggReMarketingEmail);

            //event notification
            _eventPublisher.EntityUpdated(eggReMarketingEmail);
        }

        /// <summary>
        /// Deleted a eggReMarketingEmail email
        /// </summary>
        /// <param name="eggReMarketingEmail">Queued email</param>
        public virtual void DeleteEggReMarketingEmail(EggReMarketingEmail eggReMarketingEmail)
        {
            if (eggReMarketingEmail == null)
                throw new ArgumentNullException("eggReMarketingEmail");

            _eggReMarketingEmaillRepository.Delete(eggReMarketingEmail);

            //event notification
            _eventPublisher.EntityDeleted(eggReMarketingEmail);
        }

        /// <summary>
        /// Deleted a eggReMarketingEmail emails
        /// </summary>
        /// <param name="eggReMarketingEmails">Queued emails</param>
        public virtual void DeleteEggReMarketingEmails(IList<EggReMarketingEmail> eggReMarketingEmails)
        {
            if (eggReMarketingEmails == null)
                throw new ArgumentNullException("eggReMarketingEmails");

            _eggReMarketingEmaillRepository.Delete(eggReMarketingEmails);

        }

        /// <summary>
        /// Gets a eggReMarketingEmail email by identifier
        /// </summary>
        /// <param name="eggReMarketingEmailId">Queued email identifier</param>
        /// <returns>Queued email</returns>
        public virtual EggReMarketingEmail GetEggReMarketingEmailById(int eggReMarketingEmailId)
        {
            if (eggReMarketingEmailId == 0)
                return null;

            return _eggReMarketingEmaillRepository.GetById(eggReMarketingEmailId);

        }

        /// <summary>
        /// Get eggReMarketingEmail emails by identifiers
        /// </summary>
        /// <param name="eggReMarketingEmailIds">eggReMarketingEmail email identifiers</param>
        /// <returns>Queued emails</returns>
        public virtual IList<EggReMarketingEmail> GetEggReMarketingEmailsByIds(int[] eggReMarketingEmailIds)
        {
            if (eggReMarketingEmailIds == null || eggReMarketingEmailIds.Length == 0)
                return new List<EggReMarketingEmail>();

            var query = from qe in _eggReMarketingEmaillRepository.Table
                        where eggReMarketingEmailIds.Contains(qe.Id)
                        select qe;
            var eggReMarketingEmails = query.ToList();
            //sort by passed identifiers
            var sortedEggReMarketingEmails = new List<EggReMarketingEmail>();
            foreach (int id in eggReMarketingEmailIds)
            {
                var eggReMarketingEmail = eggReMarketingEmails.Find(x => x.Id == id);
                if (eggReMarketingEmail != null)
                    sortedEggReMarketingEmails.Add(eggReMarketingEmail);
            }
            return sortedEggReMarketingEmails;
        }

        /// <summary>
        /// Gets all eggReMarketingEmail emails
        /// </summary>
        /// <param name="fromEmail">From Email</param>
        /// <param name="toEmail">To Email</param>
        /// <param name="createdFromUtc">Created date from (UTC); null to load all records</param>
        /// <param name="createdToUtc">Created date to (UTC); null to load all records</param>
        /// <param name="loadNotSentItemsOnly">A value indicating whether to load only not sent emails</param>
        /// <param name="loadOnlyItemsToBeSent">A value indicating whether to load only emails for ready to be sent</param>
        /// <param name="maxSendTries">Maximum send tries</param>
        /// <param name="loadNewest">A value indicating whether we should sort eggReMarketingEmail email descending; otherwise, ascending.</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Email item list</returns>
        public virtual IPagedList<EggReMarketingEmail> SearchEmails(string fromEmail,
            string toEmail, DateTime? createdFromUtc, DateTime? createdToUtc,
            bool loadNotSentItemsOnly, bool loadOnlyItemsToBeSent, int maxSendTries,
            bool loadNewest, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            fromEmail = (fromEmail ?? String.Empty).Trim();
            toEmail = (toEmail ?? String.Empty).Trim();

            var query = _eggReMarketingEmaillRepository.Table;
            //load the newest records
            var eggReMarketingEmails = new PagedList<EggReMarketingEmail>(query, pageIndex, pageSize);
            return eggReMarketingEmails;
        }

        /// <summary>
        /// Delete all eggReMarketingEmail emails
        /// </summary>
        public virtual void DeleteAllEmails()
        {
            if (_commonSettings.UseStoredProceduresIfSupported && _dataProvider.StoredProceduredSupported)
            {
                //although it's not a stored procedure we use it to ensure that a database supports them
                //we cannot wait until EF team has it implemented - http://data.uservoice.com/forums/72025-entity-framework-feature-suggestions/suggestions/1015357-batch-cud-support


                //do all databases support "Truncate command"?
                string eggReMarketingEmailTableName = _dbContext.GetTableName<EggReMarketingEmail>();
                _dbContext.ExecuteSqlCommand(String.Format("TRUNCATE TABLE [{0}]", eggReMarketingEmailTableName));
            }
            else
            {
                var eggReMarketingEmails = _eggReMarketingEmaillRepository.Table.ToList();
                foreach (var qe in eggReMarketingEmails)
                    _eggReMarketingEmaillRepository.Delete(qe);
            }
        }

        public virtual MessageTemplate GetMessageTemplateForStep(string type, int step)
        {
            var query = _messageTemplateRepository.Table.Where(mt => mt.EmailType.Equals(type) && mt.EmailStep == step);
            var result = query.FirstOrDefault();
            return result;
        }
        public virtual MessageTemplate GetMessageTemplateByEggReMarketingEmailId(int eggReMarketingEmaillId)
        {
            if (eggReMarketingEmaillId == 0)
                return null;
            var eggReMarketingEmail = _eggReMarketingEmaillRepository.GetById(eggReMarketingEmaillId);
            if (eggReMarketingEmail != null)
            {
                var query = _messageTemplateRepository.Table.Where(mt => mt.EmailType.Equals(eggReMarketingEmail.EmailType) && mt.EmailStep == eggReMarketingEmail.InStep);
                var result = query.FirstOrDefault();
                return result;
            }
            else
                return null;
        }
        public virtual IList<EggReMarketingEmail> GetInProgressEggReMarketingEmail()
        {
            var query = _eggReMarketingEmaillRepository.Table.Where(p => !p.IsCompleted);
            query = query.Where(p => p.ResentDate <= DateTime.UtcNow);
            return query.ToList();
        }

        public virtual EggReMarketingEmailQueuedTaskModel PrepareEggReMarketingQueuedEmails()
        {
            RefreshOrderStatusForEggReMarketingEmail();
            var eggReMarketingEmails = GetInProgressEggReMarketingEmail();
            var currentStore = _storeContext.CurrentStore;
            if (eggReMarketingEmails.Any())
            {
                EggReMarketingEmailQueuedTaskModel eggReMarketingEmailQueuedTaskModel = new EggReMarketingEmailQueuedTaskModel
                {
                    EggReMarketingEmails = new List<EggReMarketingEmail>(),
                    QueuedEmails = new List<QueuedEmail>()
                };
                var emailAccount = _emailAccountService.GetEmailAccountById(_emailAccountSettings.DefaultEmailAccountId);
                if (emailAccount == null)
                    emailAccount = _emailAccountService.GetAllEmailAccounts().FirstOrDefault();
                if (emailAccount == null)
                    throw new Exception("No email account could be loaded");
                foreach (var eggEmail in eggReMarketingEmails)
                {
                    var eggQueuedEmail = new QueuedEmail();
                    var emailTemplate = new MessageTemplate();
                    string from = emailAccount.Email;
                    //string fromName = emailAccount.DisplayName;
                    string fromName = currentStore.Name;
                    switch (eggEmail.EmailType)
                    {
                        case "GetCoupon":
                            {
                                if (eggEmail.InStep <= eggEmail.MaxStep)
                                {
                                    emailTemplate = GetMessageTemplateForStep(eggEmail.EmailType, eggEmail.InStep);
                                    string subject = emailTemplate.Subject;
                                    subject = Replace(subject, EggEmailPattern.StoreName, currentStore.Name);
                                    string body = emailTemplate.Body;
                                    body = Replace(body, EggEmailPattern.StoreName, currentStore.Name);
                                    body = Replace(body, EggEmailPattern.StoreUrl, currentStore.Url);
                                    body = Replace(body, EggEmailPattern.StoreEmail, currentStore.EmailAccountDisplayed);
                                    //required for some SMTP servers
                                    from = emailAccount.Email;
                                    fromName = emailAccount.DisplayName;
                                    eggQueuedEmail.From = from;
                                    eggQueuedEmail.FromName = fromName;
                                    eggQueuedEmail.To = eggEmail.EmailAddress;
                                    eggQueuedEmail.ToName = emailAccount.DisplayName;
                                    eggQueuedEmail.ReplyTo = from;
                                    eggQueuedEmail.ReplyToName = fromName;
                                    eggQueuedEmail.Priority = QueuedEmailPriority.High;
                                    eggQueuedEmail.Subject = subject;
                                    eggQueuedEmail.Body = body;
                                    eggQueuedEmail.CreatedOnUtc = DateTime.UtcNow;
                                    eggQueuedEmail.EmailAccountId = emailAccount.Id;
                                    eggReMarketingEmailQueuedTaskModel.QueuedEmails.Add(eggQueuedEmail);
                                    eggReMarketingEmailQueuedTaskModel.EggReMarketingEmails.Add(eggEmail);
                                }
                            }
                            break;
                        case "CheckoutUnsuccess":
                            {
                                if (eggEmail.InStep < eggEmail.MaxStep)
                                {
                                    var order = _orderService.GetOrderById(eggEmail.OrderId);

                                    if (eggEmail.InStep == 1)
                                    {
                                        //send sms 
                                        _smsMarketingService.OnSendSMSMarketing(order.Id, SMSType.Checkout);
                                    }

                                    emailTemplate = GetMessageTemplateForStep(eggEmail.EmailType, eggEmail.InStep);
                                    var emailTemplateReplaced = PrepareDataForTemplate(order, emailTemplate.Body, emailTemplate.Subject);
                                    string subject = emailTemplateReplaced.Subject;
                                    string body = emailTemplateReplaced.Body;
                                    //required for some SMTP servers
                                    from = emailAccount.Email;
                                    fromName = emailAccount.DisplayName;
                                    eggQueuedEmail.From = from;
                                    eggQueuedEmail.FromName = fromName;
                                    eggQueuedEmail.To = eggEmail.EmailAddress;
                                    eggQueuedEmail.ToName = emailAccount.DisplayName;
                                    eggQueuedEmail.ReplyTo = from;
                                    eggQueuedEmail.ReplyToName = fromName;
                                    eggQueuedEmail.Priority = QueuedEmailPriority.High;
                                    eggQueuedEmail.Subject = subject;
                                    eggQueuedEmail.Body = body;
                                    eggQueuedEmail.CreatedOnUtc = DateTime.UtcNow;
                                    eggQueuedEmail.EmailAccountId = emailAccount.Id;
                                    eggReMarketingEmailQueuedTaskModel.QueuedEmails.Add(eggQueuedEmail);
                                    eggReMarketingEmailQueuedTaskModel.EggReMarketingEmails.Add(eggEmail);
                                }
                            }
                            break;
                        case "CheckoutSuccess":
                            {
                                if (eggEmail.InStep < eggEmail.MaxStep)
                                {
                                    var order = _orderService.GetOrderById(eggEmail.OrderId);
                                    if (eggEmail.InStep == 2)
                                    {
                                        TimeSpan span = DateTime.UtcNow.Subtract(eggEmail.SentOnUtc);
                                        if (span.TotalDays > 12)
                                        {
                                            eggEmail.InStep++;
                                            UpdateEggReMarketingEmail(eggEmail);
                                            emailTemplate = GetMessageTemplateForStep(eggEmail.EmailType, eggEmail.InStep);
                                            var emailTemplateReplaced = PrepareDataForTemplate(order, emailTemplate.Body, emailTemplate.Subject);
                                            string subject = emailTemplateReplaced.Subject;
                                            string body = emailTemplateReplaced.Body;
                                            //required for some SMTP servers
                                            from = emailAccount.Email;
                                            fromName = emailAccount.DisplayName;
                                            eggQueuedEmail.From = from;
                                            eggQueuedEmail.FromName = fromName;
                                            eggQueuedEmail.To = eggEmail.EmailAddress;
                                            eggQueuedEmail.ToName = emailAccount.DisplayName;
                                            eggQueuedEmail.ReplyTo = from;
                                            eggQueuedEmail.ReplyToName = fromName;
                                            eggQueuedEmail.Priority = QueuedEmailPriority.High;
                                            eggQueuedEmail.Subject = subject;
                                            eggQueuedEmail.Body = body;
                                            eggQueuedEmail.CreatedOnUtc = DateTime.UtcNow;
                                            eggQueuedEmail.EmailAccountId = emailAccount.Id;
                                            eggReMarketingEmailQueuedTaskModel.QueuedEmails.Add(eggQueuedEmail);
                                            eggReMarketingEmailQueuedTaskModel.EggReMarketingEmails.Add(eggEmail);
                                            this.NextStepOrChangeStatus(eggEmail);
                                        }
                                    }
                                    else
                                    {
                                        emailTemplate = GetMessageTemplateForStep(eggEmail.EmailType, eggEmail.InStep);
                                        var emailTemplateReplaced = PrepareDataForTemplate(order, emailTemplate.Body, emailTemplate.Subject);
                                        string subject = emailTemplateReplaced.Subject;
                                        string body = emailTemplateReplaced.Body;
                                        //required for some SMTP servers
                                        from = emailAccount.Email;
                                        fromName = emailAccount.DisplayName;
                                        eggQueuedEmail.From = from;
                                        eggQueuedEmail.FromName = fromName;
                                        eggQueuedEmail.To = eggEmail.EmailAddress;
                                        eggQueuedEmail.ToName = emailAccount.DisplayName;
                                        eggQueuedEmail.ReplyTo = from;
                                        eggQueuedEmail.ReplyToName = fromName;
                                        eggQueuedEmail.Priority = QueuedEmailPriority.High;
                                        eggQueuedEmail.Subject = subject;
                                        eggQueuedEmail.Body = body;
                                        eggQueuedEmail.CreatedOnUtc = DateTime.UtcNow;
                                        eggQueuedEmail.EmailAccountId = emailAccount.Id;
                                        eggReMarketingEmailQueuedTaskModel.QueuedEmails.Add(eggQueuedEmail);
                                        eggReMarketingEmailQueuedTaskModel.EggReMarketingEmails.Add(eggEmail);
                                    }
                                }
                            }
                            break;
                        case "OrderShipped":
                            {
                                Shipment orderShipment = _shipmentService.GetOrderTrackingNumberForOrder(eggEmail.OrderId);

                                if (orderShipment != null)
                                {
                                    if (!string.IsNullOrEmpty(orderShipment.TrackingNumber))
                                    {
                                        var order = _orderService.GetOrderById(eggEmail.OrderId);
                                        emailTemplate = GetMessageTemplateForStep(eggEmail.EmailType, eggEmail.InStep);
                                        var emailTemplateReplaced = PrepareDataForTemplate(order, emailTemplate.Body, emailTemplate.Subject);
                                        string trackingUrl = string.Format("{0}{1}&loc=en_us", "https://wwwapps.ups.com/WebTracking/track?track=yes&trackNums=", orderShipment.TrackingNumber);
                                        emailTemplateReplaced.Body = Replace(emailTemplateReplaced.Body, EggEmailPattern.OrderTrackingUrl, trackingUrl);
                                        emailTemplateReplaced.Body = Replace(emailTemplateReplaced.Body, EggEmailPattern.OrderTrackingNumber, orderShipment.TrackingNumber);
                                        string subject = emailTemplateReplaced.Subject;
                                        string body = emailTemplateReplaced.Body;
                                        //required for some SMTP servers
                                        from = emailAccount.Email;
                                        fromName = emailAccount.DisplayName;
                                        eggQueuedEmail.From = from;
                                        eggQueuedEmail.FromName = fromName;
                                        eggQueuedEmail.To = eggEmail.EmailAddress;
                                        eggQueuedEmail.ToName = emailAccount.DisplayName;
                                        eggQueuedEmail.ReplyTo = from;
                                        eggQueuedEmail.ReplyToName = fromName;
                                        eggQueuedEmail.Priority = QueuedEmailPriority.High;
                                        eggQueuedEmail.Subject = subject;
                                        eggQueuedEmail.Body = body;
                                        eggQueuedEmail.CreatedOnUtc = DateTime.UtcNow;
                                        eggQueuedEmail.EmailAccountId = emailAccount.Id;
                                        eggReMarketingEmailQueuedTaskModel.QueuedEmails.Add(eggQueuedEmail);
                                        eggReMarketingEmailQueuedTaskModel.EggReMarketingEmails.Add(eggEmail);

                                        //update shipment comment 
                                        orderShipment.AdminComment = "USP";
                                        _shipmentService.UpdateShipment(orderShipment);
                                    }
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
                return eggReMarketingEmailQueuedTaskModel;
            }
            return null;
        }

        public string Replace(string original, string pattern, string replacement)
        {
            return original.Replace(pattern, replacement);
        }
        public virtual void NextStepOrChangeStatus(EggReMarketingEmail eggEmail)
        {
            switch (eggEmail.EmailType)
            {
                case "GetCoupon":
                    {
                        switch (eggEmail.InStep)
                        {
                            case 1:
                                eggEmail.InStep++;
                                eggEmail.SentOnUtc = DateTime.UtcNow;
                                eggEmail.ResentDate = eggEmail.SentOnUtc.AddHours(72);
                                break;
                            case 2:
                                eggEmail.IsCompleted = true;
                                eggEmail.SentOnUtc = DateTime.UtcNow;
                                break;
                        }
                    }
                    break;
                case "CheckoutUnsuccess":
                    {
                        switch (eggEmail.InStep)
                        {
                            case 1:
                                eggEmail.InStep++;
                                eggEmail.SentOnUtc = DateTime.UtcNow;
                                eggEmail.ResentDate = eggEmail.SentOnUtc.AddHours(24);
                                break;
                            case 2:
                                eggEmail.InStep++;
                                eggEmail.SentOnUtc = DateTime.UtcNow;
                                eggEmail.ResentDate = eggEmail.SentOnUtc.AddHours(48);
                                break;
                            case 3:
                                eggEmail.InStep++;
                                eggEmail.SentOnUtc = DateTime.UtcNow;
                                eggEmail.ResentDate = eggEmail.SentOnUtc.AddHours(48);
                                break;
                            case 4:
                                eggEmail.IsCompleted = true;
                                eggEmail.SentOnUtc = DateTime.UtcNow;
                                break;
                        }
                    }
                    break;
                case "CheckoutSuccess":
                    {
                        switch (eggEmail.InStep)
                        {
                            case 1:
                                eggEmail.InStep++;
                                eggEmail.SentOnUtc = DateTime.UtcNow;
                                eggEmail.ResentDate = eggEmail.SentOnUtc.AddHours(48);
                                break;
                            case 2:
                                eggEmail.InStep++;
                                eggEmail.SentOnUtc = DateTime.UtcNow;
                                eggEmail.ResentDate = eggEmail.SentOnUtc.AddDays(8);
                                break;
                            case 3:
                                eggEmail.IsCompleted = true;
                                eggEmail.SentOnUtc = DateTime.UtcNow;
                                break;
                        }
                    }
                    break;
                case "OrderShipped":
                    {
                        eggEmail.IsCompleted = true;
                        eggEmail.SentOnUtc = DateTime.UtcNow;
                    }
                    break;
                default:
                    break;
            }
            UpdateEggReMarketingEmail(eggEmail);
        }
        public virtual void RefreshOrderStatusForEggReMarketingEmail()
        {
            var query = _eggReMarketingEmaillRepository.Table.Where(p => !p.IsCompleted);
            query = query.Where(p => p.EmailType.Equals(EggRemarketingEmailType.CheckoutUnsuccess.ToString()));
            var eggReMarketingEmails = query.ToList();
            if (eggReMarketingEmails.Any())
            {
                foreach (var item in eggReMarketingEmails)
                {
                    var order = _orderService.GetOrderById(item.OrderId);
                    if (order != null)
                    {
                        if (order.OrderStatus == OrderStatus.Complete || order.OrderStatus == OrderStatus.Processing)
                        {
                            //Update status for Email type checkout unsuccess
                            item.IsCompleted = true;
                            UpdateEggReMarketingEmail(item);
                            //Email for order was paid
                            /*EggReMarketingEmail checkoutSuccessEmail = new EggReMarketingEmail
                            {
                                EmailType = EggRemarketingEmailType.CheckoutSuccess.ToString(),
                                EmailAddress = order.BillingAddress.Email,
                                InStep = 1,
                                MaxStep = 3,
                                IsCompleted = false,
                                CreatedOnUtc = DateTime.UtcNow,
                                SentOnUtc = DateTime.UtcNow,
                                ResentDate = DateTime.UtcNow,
                                OrderId = order.Id
                            };
                            InsertEggReMarketingEmail(checkoutSuccessEmail);*/

                            //Email for shipping information
                            //EggReMarketingEmail shippingInfoEmail = new EggReMarketingEmail
                            //{
                            //    EmailType = EggRemarketingEmailType.OrderShipped.ToString(),
                            //    EmailAddress = order.BillingAddress.Email,
                            //    InStep = 1,
                            //    MaxStep = 2,
                            //    IsCompleted = false,
                            //    CreatedOnUtc = DateTime.UtcNow,
                            //    SentOnUtc = DateTime.UtcNow,
                            //    ResentDate = DateTime.UtcNow,
                            //    OrderId = order.Id
                            //};
                            //InsertEggReMarketingEmail(shippingInfoEmail);
                        }
                    }
                }
            }
        }
        private MessageTemplate PrepareDataForTemplate(Order order, string body, string subject)
        {
            var store = _storeService.GetStoreById(order.StoreId) ?? _storeContext.CurrentStore;
            //tokens
            var tokens = new List<Token>();
            _messageTokenProvider.AddOrderTokens(tokens, order, 1);
            _messageTokenProvider.AddCustomerTokens(tokens, order.Customer);

            //Replace subject and body tokens 
            var subjectReplaced = _tokenizer.Replace(subject, tokens, false);
            subjectReplaced = Replace(subjectReplaced, EggEmailPattern.StoreName, store.Name);
            var bodyReplaced = _tokenizer.Replace(body, tokens, false);
            string cartUrl = string.Format("{0}yourcart?cartcode={1}&utm_source=Email.Remind&utm_medium=free&utm_campaign=Email.Remind.Trends&utm_term=Email.Remind.Trends&utm_content=Email.Remind.Trends", store.Url, order.OrderGuid);
            bodyReplaced = Replace(bodyReplaced, EggEmailPattern.CustomerOldCartUrl, cartUrl);
            bodyReplaced = Replace(bodyReplaced, EggEmailPattern.StoreName, store.Name);
            bodyReplaced = Replace(bodyReplaced, EggEmailPattern.StoreUrl, store.Url);
            bodyReplaced = Replace(bodyReplaced, EggEmailPattern.StoreEmail, store.EmailAccountDisplayed);
            MessageTemplate result = new MessageTemplate();
            result.Body = bodyReplaced;
            result.Subject = subjectReplaced;
            return result;
        }

        private MessageTemplate PrepareDataForRemindShoppingCart(Customer customer, string body, string subject)
        {
           
            var store = _storeService.GetStoreById(customer.ShoppingCartItems.FirstOrDefault().StoreId) ?? _storeContext.CurrentStore;
            //tokens
            var tokens = new List<Token>();
            Order tmpOrder = new Order();
            tmpOrder.BillingAddress = customer.BillingAddress;
            tmpOrder.ShippingAddress = customer.BillingAddress;
            tmpOrder.CustomerCurrencyCode = "USD";
            tmpOrder.Customer = customer;
            tmpOrder.CurrencyRate = 1;
            foreach(ShoppingCartItem cartItem in customer.ShoppingCartItems)
            {
                OrderItem orderItem = new OrderItem();
                orderItem.Product = cartItem.Product;
                orderItem.Quantity = cartItem.Quantity;
                orderItem.AttributesXml = cartItem.AttributesXml;
                
                orderItem.UnitPriceInclTax = _priceCalculationService.GetUnitPrice(cartItem);
                orderItem.PriceInclTax = orderItem.Quantity * orderItem.UnitPriceInclTax;
                tmpOrder.OrderSubtotalInclTax += orderItem.PriceInclTax;
                tmpOrder.OrderItems.Add(orderItem);
            }
            tmpOrder.OrderTotal = tmpOrder.OrderSubtotalInclTax;

            _messageTokenProvider.AddOrderTokens(tokens, tmpOrder, 1);
            _messageTokenProvider.AddCustomerTokens(tokens, customer);

            //Replace subject and body tokens 
            var subjectReplaced = _tokenizer.Replace(subject, tokens, false);
            subjectReplaced = Replace(subjectReplaced, EggEmailPattern.StoreName, store.Name);
            var bodyReplaced = _tokenizer.Replace(body, tokens, false);
            string cartUrl = string.Format("{0}restorecart?customerId={1}&coupon=SUMMER10", store.Url, customer.Id);
            bodyReplaced = Replace(bodyReplaced, EggEmailPattern.CustomerOldCartUrl, cartUrl);
            bodyReplaced = Replace(bodyReplaced, EggEmailPattern.StoreName, store.Name);
            bodyReplaced = Replace(bodyReplaced, EggEmailPattern.StoreUrl, store.Url);
            bodyReplaced = Replace(bodyReplaced, EggEmailPattern.StoreEmail, store.EmailAccountDisplayed);
            MessageTemplate result = new MessageTemplate();
            result.Body = bodyReplaced;
            result.Subject = subjectReplaced;
            return result;
        }

        public virtual void SetCompletedForEggReMarketingEmailTypeGetCoupon(string email)
        {
            var query = _eggReMarketingEmaillRepository.Table.Where(p => !p.IsCompleted);
            var eggEmailGotCouponList = query.Where(e => e.EmailAddress.Equals(email) && e.EmailType.Equals(EggRemarketingEmailType.GetCoupon.ToString())).ToList();
            if (eggEmailGotCouponList.Any())
            {
                foreach (var item in eggEmailGotCouponList)
                {
                    item.IsCompleted = true;
                    UpdateEggReMarketingEmail(item);
                }
            }

        }

        public virtual IList<EggReMarketingEmail> SearchEggReMarketingEmails(EggRemarketingEmailType? type, int inStep = 0, string nameRegister = "", string email = "", bool showCompleted = false)
        {
            var query = _eggReMarketingEmaillRepository.Table;
            query = query.OrderByDescending(p => p.CreatedOnUtc);

            if (!showCompleted)
                query = query.Where(p => !p.IsCompleted);
            if (type.HasValue)
                query = query.Where(p => p.EmailType.Equals(type.ToString()));
            if (inStep > 0)
                query = query.Where(p => p.InStep == inStep);
            if (!String.IsNullOrEmpty(nameRegister))
                query = query.Where(p => p.NameRegister.Equals(nameRegister));
            if (!String.IsNullOrEmpty(email))
                query = query.Where(p => p.EmailAddress.Contains(email));

            var result = query.ToList();
            return result;
        }

        public virtual void DeleteCompletedEggReMarketingEmails()
        {
            var eggCompletedEmails = _eggReMarketingEmaillRepository.Table.Where(p => p.IsCompleted).ToList();
            _eggReMarketingEmaillRepository.Delete(eggCompletedEmails);
        }
        public virtual bool UnSubscribeReMarketingEmail(string email)
        {
            bool isUnSubscribed = false;

            try
            {
                var emails = _eggReMarketingEmaillRepository.Table.Where(p => !p.IsCompleted && p.EmailAddress.Equals(email)).ToList();
                if (emails.Any())
                {
                    foreach (var item in emails)
                    {
                        item.IsCompleted = true;
                        _eggReMarketingEmaillRepository.Update(item);
                    }
                    isUnSubscribed = true;
                }
            }
            catch (Exception ex)
            {
            }

            return isUnSubscribed;
        }

        public virtual bool CheckValidTimetoRegetCoupon(string email)
        {
            bool isApprovedGetCoupon = false;
            var query = _eggReMarketingEmaillRepository.Table.Where(p => p.EmailType.Equals(EggRemarketingEmailType.GetCoupon.ToString()));
            var emailHadGotCoupon = query.Where(p => p.EmailAddress.Equals(email)).OrderByDescending(e => e.CreatedOnUtc).FirstOrDefault();
            if (emailHadGotCoupon == null)
                isApprovedGetCoupon = true;
            else
            {
                if (emailHadGotCoupon.CreatedOnUtc.AddMinutes(30) > DateTime.UtcNow)
                    isApprovedGetCoupon = false;
                else
                    isApprovedGetCoupon = true;
            }
            return isApprovedGetCoupon;
        }

        public virtual void SendImmediatelyCheckoutEmail(Order order, EggRemarketingEmailType emailType, int step)
        {
            try
            {
                //send email
                var emailTemplate = new MessageTemplate();
                var emailAccount = _emailAccountService.GetEmailAccountById(_emailAccountSettings.DefaultEmailAccountId);
                if (emailAccount == null)
                    emailAccount = _emailAccountService.GetAllEmailAccounts().FirstOrDefault();
                if (emailAccount == null)
                    throw new Exception("No email account could be loaded");
                string from = emailAccount.Email;
                string fromName = emailAccount.DisplayName;
                emailTemplate = GetMessageTemplateForStep(emailType.ToString(), step);
                var emailTemplateReplaced = PrepareDataForTemplate(order, emailTemplate.Body, emailTemplate.Subject);
                string subject = emailTemplateReplaced.Subject;
                string body = emailTemplateReplaced.Body;
                //required for some SMTP servers 
                from = emailAccount.Email;
                fromName = emailAccount.DisplayName;
                var eggQueuedEmail = new QueuedEmail();
                eggQueuedEmail.From = from;
                eggQueuedEmail.FromName = fromName;
                eggQueuedEmail.To = order.BillingAddress.Email;
                eggQueuedEmail.ToName = emailAccount.DisplayName;
                eggQueuedEmail.ReplyTo = from;
                eggQueuedEmail.ReplyToName = fromName;
                eggQueuedEmail.Priority = QueuedEmailPriority.High;
                eggQueuedEmail.Subject = subject;
                eggQueuedEmail.Body = body;
                eggQueuedEmail.CreatedOnUtc = DateTime.UtcNow;
                eggQueuedEmail.EmailAccountId = emailAccount.Id;
                _queuedEmailService.InsertQueuedEmail(eggQueuedEmail);

                //insert remarketing email
                var eggRemarketingEmailModel = new EggReMarketingEmail();
                eggRemarketingEmailModel.EmailType = emailType.ToString();
                eggRemarketingEmailModel.CreatedOnUtc = DateTime.UtcNow;
                eggRemarketingEmailModel.InStep = step + 1;
                eggRemarketingEmailModel.IsCompleted = false;
                eggRemarketingEmailModel.EmailAddress = order.BillingAddress.Email;
                //eggRemarketingEmailModel.MaxStep = emailType == EggRemarketingEmailType.CheckoutSuccess ? 3 : 4;
                eggRemarketingEmailModel.MaxStep = 3;
                eggRemarketingEmailModel.OrderId = order.Id;
                eggRemarketingEmailModel.NameRegister = string.Empty;
                eggRemarketingEmailModel.SentOnUtc = DateTime.UtcNow;
                int resentInHour = emailType == EggRemarketingEmailType.CheckoutSuccess ? 48 : 24;
                eggRemarketingEmailModel.ResentDate = DateTime.UtcNow.AddHours(resentInHour);
                InsertEggReMarketingEmail(eggRemarketingEmailModel);

                _logger.Information(string.Format("Insert to queue: {0} type {1}", eggRemarketingEmailModel.EmailAddress, eggRemarketingEmailModel.EmailType));
            }
            catch (Exception ex)
            {
                _logger.Error(string.Format("Error sent email: {0}", ex.Message));
            }
        }

        protected virtual void EnsureNotExistDuplicateEmail(string email, int orderId)
        {
            var remarkEmails = _eggReMarketingEmaillRepository.Table
                .Where(p => !p.IsCompleted && p.EmailAddress.Equals(email))
                .OrderByDescending(p => p.CreatedOnUtc).ToList();
        }
        public virtual void SendOrderShippedEmail(int orderId)
        {
            var order = _orderService.GetOrderById(orderId);
            if (order == null)
                throw new Exception("No order could be loaded");

            Shipment orderShipment = _shipmentService.GetOrderTrackingNumberForOrder(order.Id);
            if (orderShipment != null)
            {
                if (!string.IsNullOrEmpty(orderShipment.TrackingNumber))
                {
                    //send email
                    var emailTemplate = new MessageTemplate();
                    var emailAccount = _emailAccountService.GetEmailAccountById(_emailAccountSettings.DefaultEmailAccountId);
                    if (emailAccount == null)
                        emailAccount = _emailAccountService.GetAllEmailAccounts().FirstOrDefault();
                    if (emailAccount == null)
                        throw new Exception("No email account could be loaded");

                    string from = emailAccount.Email;
                    string fromName = emailAccount.DisplayName;
                    emailTemplate = GetMessageTemplateForStep(EggRemarketingEmailType.OrderShipped.ToString(), 1);
                    var emailTemplateReplaced = PrepareDataForTemplate(order, emailTemplate.Body, emailTemplate.Subject);
                    emailTemplateReplaced.Body = Replace(emailTemplateReplaced.Body, EggEmailPattern.OrderTrackingUrl, orderShipment.TrackingNumber);
                    emailTemplateReplaced.Body = Replace(emailTemplateReplaced.Body, EggEmailPattern.OrderTrackingNumber, orderShipment.TrackingNumber);
                    string subject = emailTemplateReplaced.Subject;
                    string body = emailTemplateReplaced.Body;

                    //required for some SMTP servers 
                    from = emailAccount.Email;
                    fromName = emailAccount.DisplayName;
                    var eggQueuedEmail = new QueuedEmail();
                    eggQueuedEmail.From = from;
                    eggQueuedEmail.FromName = fromName;
                    eggQueuedEmail.To = order.BillingAddress.Email;
                    eggQueuedEmail.ToName = emailAccount.DisplayName;
                    eggQueuedEmail.ReplyTo = from;
                    eggQueuedEmail.ReplyToName = fromName;
                    eggQueuedEmail.Priority = QueuedEmailPriority.High;
                    eggQueuedEmail.Subject = subject;
                    eggQueuedEmail.Body = body;
                    eggQueuedEmail.CreatedOnUtc = DateTime.UtcNow;
                    eggQueuedEmail.EmailAccountId = emailAccount.Id;
                    _queuedEmailService.InsertQueuedEmail(eggQueuedEmail);


                    //update shipment comment 
                    orderShipment.AdminComment = "USP";
                    _shipmentService.UpdateShipment(orderShipment);

                    var eggEmailRemarketing = _eggReMarketingEmaillRepository.Table.Where(p => p.OrderId == order.Id && p.EmailType.Equals(EggRemarketingEmailType.CheckoutSuccess.ToString())).FirstOrDefault();

                    if (eggEmailRemarketing == null)
                    {
                        //not exist egg email in system
                        //insert remarketing email
                        var eggRemarketingEmailModel = new EggReMarketingEmail();
                        eggRemarketingEmailModel.EmailType = EggRemarketingEmailType.CheckoutSuccess.ToString();
                        eggRemarketingEmailModel.CreatedOnUtc = DateTime.UtcNow;
                        eggRemarketingEmailModel.InStep = 2;
                        eggRemarketingEmailModel.IsCompleted = false;
                        eggRemarketingEmailModel.EmailAddress = order.BillingAddress.Email;
                        eggRemarketingEmailModel.MaxStep = 3;
                        eggRemarketingEmailModel.OrderId = order.Id;
                        eggRemarketingEmailModel.NameRegister = string.Empty;
                        eggRemarketingEmailModel.SentOnUtc = DateTime.UtcNow;
                        eggRemarketingEmailModel.ResentDate = DateTime.UtcNow;
                        InsertEggReMarketingEmail(eggRemarketingEmailModel);
                        this.NextStepOrChangeStatus(eggRemarketingEmailModel);
                    }
                    else
                    {
                        //update existed egg email
                        if (eggEmailRemarketing.InStep == 2)
                            this.NextStepOrChangeStatus(eggEmailRemarketing);
                    }

                    //Log
                    _logger.Information(string.Format("Insert to queue: {0} type {1}", order.BillingAddress.Email, EggRemarketingEmailType.OrderShipped.ToString()));
                }
            }
        }

        public virtual void SendOrderInProcessingEmail(int orderId)
        {
            var order = _orderService.GetOrderById(orderId);
            if (order == null)
                throw new Exception("No order could be loaded");

            //send email
            var emailTemplate = new MessageTemplate();
            var emailAccount = _emailAccountService.GetEmailAccountById(_emailAccountSettings.DefaultEmailAccountId);
            if (emailAccount == null)
                emailAccount = _emailAccountService.GetAllEmailAccounts().FirstOrDefault();
            if (emailAccount == null)
                throw new Exception("No email account could be loaded");

            string from = emailAccount.Email;
            string fromName = emailAccount.DisplayName;
            emailTemplate = GetMessageTemplateForStep(EggRemarketingEmailType.OrderProcessing.ToString(), 1);
            if (emailTemplate != null)
            {
                var emailTemplateReplaced = PrepareDataForTemplate(order, emailTemplate.Body, emailTemplate.Subject);
                string subject = emailTemplateReplaced.Subject;
                string body = emailTemplateReplaced.Body;

                //required for some SMTP servers 
                from = emailAccount.Email;
                fromName = emailAccount.DisplayName;
                var eggQueuedEmail = new QueuedEmail();
                eggQueuedEmail.From = from;
                eggQueuedEmail.FromName = fromName;
                eggQueuedEmail.To = order.BillingAddress.Email;
                eggQueuedEmail.ToName = emailAccount.DisplayName;
                eggQueuedEmail.ReplyTo = from;
                eggQueuedEmail.ReplyToName = fromName;
                eggQueuedEmail.Priority = QueuedEmailPriority.High;
                eggQueuedEmail.Subject = subject;
                eggQueuedEmail.Body = body;
                eggQueuedEmail.CreatedOnUtc = DateTime.UtcNow;
                eggQueuedEmail.EmailAccountId = emailAccount.Id;
                _queuedEmailService.InsertQueuedEmail(eggQueuedEmail);

                //insert remarketing email
                var eggRemarketingEmailModel = new EggReMarketingEmail();
                eggRemarketingEmailModel.EmailType = EggRemarketingEmailType.OrderProcessing.ToString();
                eggRemarketingEmailModel.CreatedOnUtc = DateTime.UtcNow;
                eggRemarketingEmailModel.InStep = 1;
                eggRemarketingEmailModel.IsCompleted = true;
                eggRemarketingEmailModel.EmailAddress = order.BillingAddress.Email;
                eggRemarketingEmailModel.MaxStep = 1;
                eggRemarketingEmailModel.OrderId = order.Id;
                eggRemarketingEmailModel.NameRegister = string.Empty;
                eggRemarketingEmailModel.SentOnUtc = DateTime.UtcNow;
                eggRemarketingEmailModel.ResentDate = DateTime.UtcNow;
                InsertEggReMarketingEmail(eggRemarketingEmailModel);
            }
            else
                throw new Exception("No email template could be loaded");
        }


        public virtual void SendCustomEmailForOrder(int orderId, string body, string subject)
        {
            var order = _orderService.GetOrderById(orderId);
            if (order == null)
                throw new Exception("No order could be loaded");

            //send email
            var emailAccount = _emailAccountService.GetEmailAccountById(_emailAccountSettings.DefaultEmailAccountId);
            if (emailAccount == null)
                emailAccount = _emailAccountService.GetAllEmailAccounts().FirstOrDefault();
            if (emailAccount == null)
                throw new Exception("No email account could be loaded");
            var store = _storeContext.CurrentStore;

            string from = emailAccount.Email;
            string fromName = emailAccount.DisplayName;

            StringBuilder _emailBody = new StringBuilder();

            //generate email template
            _emailBody.AppendLine("<div style=\"color: #414244; margin: 0 auto; width: 600px;\">");
            _emailBody.AppendLine(string.Format("<div style=\"margin - bottom: 30px; \"><a style=\"color: #414244; font-size: 26px; font-weight: bold;\" href=\"{0}\" target=\"_blank\">{1} T- SHIRTS</a></div>", store.Url, store.Name));
            _emailBody.AppendLine("<div style=\"background: #fff; border-color: #02daaf #d8d8d8 #d8d8d8; border-style: solid; border-width: 4px 1px 1px; padding: 30px 0;\">");
            _emailBody.AppendLine("<div style=\"padding: 0 30px; \">");

            //email body
            _emailBody.AppendLine(body);
            //end of body
            _emailBody.AppendLine(string.Format("<p>This is automatic reply. If you want to contact us directly, please email at: {0}</p>", store.EmailAccountDisplayed));
            _emailBody.AppendLine("</div></div></div>");


            var eggQueuedEmail = new QueuedEmail();
            eggQueuedEmail.From = from;
            eggQueuedEmail.FromName = fromName;
            eggQueuedEmail.To = order.BillingAddress.Email;
            eggQueuedEmail.ToName = emailAccount.DisplayName;
            eggQueuedEmail.ReplyTo = from;
            eggQueuedEmail.ReplyToName = fromName;
            eggQueuedEmail.Priority = QueuedEmailPriority.High;
            eggQueuedEmail.Subject = subject;
            eggQueuedEmail.Body = _emailBody.ToString();
            eggQueuedEmail.CreatedOnUtc = DateTime.UtcNow;
            eggQueuedEmail.EmailAccountId = emailAccount.Id;
            _queuedEmailService.InsertQueuedEmail(eggQueuedEmail);
        }

        public virtual void SendCustomEmailToCustomer(string email, string body, string subject)
        {
            if (string.IsNullOrEmpty(email))
                throw new Exception("Invalid email");

            //send email
            var emailAccount = _emailAccountService.GetEmailAccountById(_emailAccountSettings.DefaultEmailAccountId);
            if (emailAccount == null)
                emailAccount = _emailAccountService.GetAllEmailAccounts().FirstOrDefault();
            if (emailAccount == null)
                throw new Exception("No email account could be loaded");
            var store = _storeContext.CurrentStore;

            string from = emailAccount.Email;
            string fromName = emailAccount.DisplayName;

            StringBuilder _emailBody = new StringBuilder();

            //generate email template
            _emailBody.AppendLine("<div style=\"color: #414244; margin: 0 auto; width: 600px;\">");
            _emailBody.AppendLine(string.Format("<div style=\"margin - bottom: 30px; \"><a style=\"color: #414244; font-size: 26px; font-weight: bold;\" href=\"{0}\" target=\"_blank\">{1} T- SHIRTS</a></div>", store.Url, store.Name));
            _emailBody.AppendLine("<div style=\"background: #fff; border-color: #02daaf #d8d8d8 #d8d8d8; border-style: solid; border-width: 4px 1px 1px; padding: 30px 0;\">");
            _emailBody.AppendLine("<div style=\"padding: 0 30px; text-align: justify;\">");

            //email body
            _emailBody.AppendLine(body);
            //end of body
            _emailBody.AppendLine(string.Format("<p>This is automatic reply. If you want to contact us directly, please email at: {0}</p>", store.EmailAccountDisplayed));
            _emailBody.AppendLine("</div></div></div>");


            var eggQueuedEmail = new QueuedEmail();
            eggQueuedEmail.From = from;
            eggQueuedEmail.FromName = fromName;
            eggQueuedEmail.To = email;
            eggQueuedEmail.ToName = emailAccount.DisplayName;
            eggQueuedEmail.ReplyTo = from;
            eggQueuedEmail.ReplyToName = fromName;
            eggQueuedEmail.Priority = QueuedEmailPriority.High;
            eggQueuedEmail.Subject = subject;
            eggQueuedEmail.Body = _emailBody.ToString();
            eggQueuedEmail.CreatedOnUtc = DateTime.UtcNow;
            eggQueuedEmail.EmailAccountId = emailAccount.Id;
            _queuedEmailService.InsertQueuedEmail(eggQueuedEmail);
        }

        public void SendRemindShoppingCartEmail(int customerId)
        {
            Customer customer = _customerService.GetCustomerById(customerId);
            if (customer == null)
                throw new Exception(string.Format("Customer not found - {0}", customerId));

            //send email
            var emailTemplate = new MessageTemplate();
            var emailAccount = _emailAccountService.GetEmailAccountById(_emailAccountSettings.DefaultEmailAccountId);
            if (emailAccount == null)
                emailAccount = _emailAccountService.GetAllEmailAccounts().FirstOrDefault();
            if (emailAccount == null)
                throw new Exception("No email account could be loaded");

            string from = emailAccount.Email;
            string fromName = emailAccount.DisplayName;
            emailTemplate = GetMessageTemplateForStep(EggRemarketingEmailType.RemindShoppingCart.ToString(), 1);
            if (emailTemplate != null)
            {
                var emailTemplateReplaced = PrepareDataForRemindShoppingCart(customer, emailTemplate.Body, emailTemplate.Subject);
                string subject = emailTemplateReplaced.Subject;
                string body = emailTemplateReplaced.Body;

                //required for some SMTP servers 
                from = emailAccount.Email;
                fromName = emailAccount.DisplayName;
                var eggQueuedEmail = new QueuedEmail();
                eggQueuedEmail.From = from;
                eggQueuedEmail.FromName = fromName;
                eggQueuedEmail.To = customer.BillingAddress.Email;
                eggQueuedEmail.ToName = emailAccount.DisplayName;
                eggQueuedEmail.ReplyTo = from;
                eggQueuedEmail.ReplyToName = fromName;
                eggQueuedEmail.Priority = QueuedEmailPriority.High;
                eggQueuedEmail.Subject = subject;
                eggQueuedEmail.Body = body;
                eggQueuedEmail.CreatedOnUtc = DateTime.UtcNow;
                eggQueuedEmail.EmailAccountId = emailAccount.Id;
                _queuedEmailService.InsertQueuedEmail(eggQueuedEmail);

            }
            else
                throw new Exception("No email template could be loaded");
        }


    }
}
