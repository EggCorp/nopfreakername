﻿using System;
using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.Messages;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Orders;

namespace Nop.Services.Messages
{
    public partial interface IEggReMarketingEmailService
    {
        /// <summary>
        /// Inserts a queued email
        /// </summary>
        /// <param name="EggReMarketingEmail">Queued email</param>
        void InsertEggReMarketingEmail(EggReMarketingEmail EggReMarketingEmail);

        /// <summary>
        /// Updates a queued email
        /// </summary>
        /// <param name="EggReMarketingEmail">Queued email</param>
        void UpdateEggReMarketingEmail(EggReMarketingEmail EggReMarketingEmail);

        /// <summary>
        /// Deleted a queued email
        /// </summary>
        /// <param name="EggReMarketingEmail">Queued email</param>
        void DeleteEggReMarketingEmail(EggReMarketingEmail EggReMarketingEmail);

        /// <summary>
        /// Deleted a queued emails
        /// </summary>
        /// <param name="EggReMarketingEmails">Queued emails</param>
        void DeleteEggReMarketingEmails(IList<EggReMarketingEmail> EggReMarketingEmails);

        /// <summary>
        /// Gets a queued email by identifier
        /// </summary>
        /// <param name="EggReMarketingEmailId">Queued email identifier</param>
        /// <returns>Queued email</returns>
        EggReMarketingEmail GetEggReMarketingEmailById(int EggReMarketingEmailId);

        /// <summary>
        /// Get queued emails by identifiers
        /// </summary>
        /// <param name="EggReMarketingEmailIds">queued email identifiers</param>
        /// <returns>Queued emails</returns>
        IList<EggReMarketingEmail> GetEggReMarketingEmailsByIds(int[] EggReMarketingEmailIds);

        /// <summary>
        /// Search queued emails
        /// </summary>
        /// <param name="fromEmail">From Email</param>
        /// <param name="toEmail">To Email</param>
        /// <param name="createdFromUtc">Created date from (UTC); null to load all records</param>
        /// <param name="createdToUtc">Created date to (UTC); null to load all records</param>
        /// <param name="loadNotSentItemsOnly">A value indicating whether to load only not sent emails</param>
        /// <param name="loadOnlyItemsToBeSent">A value indicating whether to load only emails for ready to be sent</param>
        /// <param name="maxSendTries">Maximum send tries</param>
        /// <param name="loadNewest">A value indicating whether we should sort queued email descending; otherwise, ascending.</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Queued emails</returns>
        IPagedList<EggReMarketingEmail> SearchEmails(string fromEmail,
            string toEmail, DateTime? createdFromUtc, DateTime? createdToUtc,
            bool loadNotSentItemsOnly, bool loadOnlyItemsToBeSent, int maxSendTries,
            bool loadNewest, int pageIndex = 0, int pageSize = int.MaxValue);

        /// <summary>
        /// Delete all queued emails
        /// </summary>
        void DeleteAllEmails();

        MessageTemplate GetMessageTemplateForStep(string type, int step);

        MessageTemplate GetMessageTemplateByEggReMarketingEmailId(int id);

        IList<EggReMarketingEmail> GetInProgressEggReMarketingEmail();

        EggReMarketingEmailQueuedTaskModel PrepareEggReMarketingQueuedEmails();

        void NextStepOrChangeStatus(EggReMarketingEmail eggEmail);
        void RefreshOrderStatusForEggReMarketingEmail();
        void SetCompletedForEggReMarketingEmailTypeGetCoupon(string email);
        IList<EggReMarketingEmail> SearchEggReMarketingEmails(EggRemarketingEmailType? type, int inStep, string nameRegister, string email, bool showCompleted);
        void DeleteCompletedEggReMarketingEmails();
        string Replace(string original, string pattern, string replacement);
        bool UnSubscribeReMarketingEmail(string email);
        bool CheckValidTimetoRegetCoupon(string email);
        void SendImmediatelyCheckoutEmail(Order order, EggRemarketingEmailType emailType, int step);
        void SendOrderShippedEmail(int orderId);
        void SendOrderInProcessingEmail(int orderId);
        void SendRemindShoppingCartEmail(int customerId);
        void SendCustomEmailToCustomer(string email, string body, string subject);
        void SendCustomEmailForOrder(int orderId, string body, string subject);
    }
}
