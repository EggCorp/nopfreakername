﻿using Nop.Services.Orders;
using Nop.Services.Catalog;
using Nop.Services.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Services.Logging;

namespace Nop.Services.Messages
{
    public partial class EggReMarketingEmailSendTask : ITask
    {
        private readonly IQueuedEmailService _queuedEmailService;
        private readonly IEmailSender _emailSender;
        private readonly ILogger _logger;
        private readonly IEggReMarketingEmailService _eggReMarketingEmailService;
        public EggReMarketingEmailSendTask(
            IQueuedEmailService queuedEmailService,
            IEmailSender emailSender, 
            ILogger logger,
            IEggReMarketingEmailService eggReMarketingEmailService)
        {
            this._queuedEmailService = queuedEmailService;
            this._emailSender = emailSender;
            this._logger = logger;
            this._eggReMarketingEmailService = eggReMarketingEmailService;
        }

        /// <summary>
        /// Executes a task
        /// </summary>
        public virtual void Execute()
        {
            var eggReMarketingEmailModel = _eggReMarketingEmailService.PrepareEggReMarketingQueuedEmails();
            if(eggReMarketingEmailModel != null)
            {
                if(eggReMarketingEmailModel.QueuedEmails.Any() && eggReMarketingEmailModel.EggReMarketingEmails.Any())
                {
                    for(int index =0; index<eggReMarketingEmailModel.QueuedEmails.Count(); index++)
                    {
                        try
                        {
                            _queuedEmailService.InsertQueuedEmail(eggReMarketingEmailModel.QueuedEmails[index]);
                            _eggReMarketingEmailService.NextStepOrChangeStatus(eggReMarketingEmailModel.EggReMarketingEmails[index]);

                        }
                        catch (Exception exc)
                        {
                            _logger.Error(string.Format("Error insert into QueuedEmail: {0}", exc.Message), exc);
                        }
                        finally
                        {
                            _logger.Information(string.Format("Insert into QueuedEmail successfully: To {0}-{1}, Type: {2}.", eggReMarketingEmailModel.QueuedEmails[index].To, eggReMarketingEmailModel.QueuedEmails[index].ToName, eggReMarketingEmailModel.EggReMarketingEmails[index].EmailType));
                        }
                    }
                }
            }
          
        }
    }
}
