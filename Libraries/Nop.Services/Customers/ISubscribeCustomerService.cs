﻿using Nop.Core.Domain.Customers;
using System.Collections.Generic;

namespace Nop.Services.Customers
{
    public partial interface ISubscribeCustomerService
    {
        /// <summary>
        /// Insert a SubscribeCustomer
        /// </summary>
        /// <param name="SubscribeCustomer">SubscribeCustomer</param>
        SubscribeCustomer InsertSubscribeCustomer(SubscribeCustomer scCustomer);
        IList<SubscribeCustomer> Search();
        SubscribeCustomer GetByCustomer(int customerId);
        SubscribeCustomer GetById(int id);
        void UpdateSubscribeCustomer(SubscribeCustomer subscribeCustomer);
    }
}
