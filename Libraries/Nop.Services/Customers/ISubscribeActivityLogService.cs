﻿using Nop.Core.Domain.Customers;
using Nop.Core.Domain.SubscribeMessenger;
using System.Collections.Generic;

namespace Nop.Services.Customers
{
    public partial interface ISubscribeActivityLogService
    {
        /// <summary>
        /// Insert a SubscribeCustomer
        /// </summary>
        /// <param name="SubscribeActivityLog">SubscribeActivityLog</param>
        SubscribeActivityLog InsertSubscribeActivityLogViewProduct(int entityId, string comment);
        SubscribeActivityLog InsertSubscribeActivityLogViewCategory(int entityId, string comment);
        SubscribeActivityLog InsertSubscribeActivityAddToCart(string comment);
        SubscribeActivityLog InsertSubscribeActivityPlaceOrder(int entityId, string comment);
        SubscribeActivityLog InsertSubscribeActivityLog(Customer customer, string systemKeyword, string entityName, int entityId, string comment);
        SubscribeActivityLog GetActivityLogPlaceOrder(int orderId);
        List<SubscribeActivityLog> GetActivityLogBySubscribeCustomerId(int subscribeCustomerId);

        List<SubscribeActivityLog> GetViewProductLogsOfSubscribeCustomer(int subscribeCustomerId);
        List<SubscribeActivityLog> GetAddToCartLogsOfSubscribeCustomer(int subscribeCustomerId);
        List<SubscribeActivityLog> GetPlaceOrderLogsOfSubscribeCustomer(int subscribeCustomerId);
        List<int> GetSubscribeCustomersIdByCampignEntity(CampaignEntity campainEntity);
        bool CheckSubscribeCustomerByCampaignMessenger(int subscribeCustomerId, CampaignMessenger campaignMessenger);
    }
}
