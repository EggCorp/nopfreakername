﻿using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.SubscribeMessenger;
using Nop.Services.Logging;
using Nop.Services.SubscribeMessenger;
using System;
using System.Collections.Generic;
using System.Linq;
using static Nop.Services.Logging.CustomerActivityService;

namespace Nop.Services.Customers
{
    public partial class SubscribeActivityLogService : ISubscribeActivityLogService
    {
        #region Fields
        private readonly ICacheManager _cacheManager;
        private readonly IWorkContext _workContext;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly ISubscribeCustomerService _subscribeCustomerService;
        private readonly ILogger _logger;
        private readonly IRepository<SubscribeActivityLog> _subscribeActivityLogRepository;
        private readonly ICampaignMessengerService _campaignMessengerService;
        #endregion

        #region Constant
        /// <summary>
        /// Key for caching
        /// </summary>
        private const string ACTIVITYTYPE_ALL_KEY = "Nop.activitytype.all";
        /// <summary>
        /// Key pattern to clear cache
        /// </summary>
        private const string ACTIVITYTYPE_PATTERN_KEY = "Nop.activitytype.";

        private const string ACTIVITYTYPE_VIEW_PRODUCT = "PublicStore.ViewProduct";
        private const string ACTIVITYTYPE_VIEW_CATEGORY = "PublicStore.ViewCategory";
        private const string ACTIVITYTYPE_ADD_TO_SHOPPING_CART = "PublicStore.AddToShoppingCart";
        private const string ACTIVITYTYPE_PLACE_ORDER = "PublicStore.PlaceOrder";

        private const string ENTITY_PRODUCT = "Product";
        private const string ENTITY_CATEGORY = "Category";
        private const string ENTITY_ORDER = "Order";
        #endregion

        #region Ctor
        public SubscribeActivityLogService(ICacheManager cacheManager, IWorkContext workContext, 
            ICustomerActivityService customerActivityService, ISubscribeCustomerService subscribeCustomerService,
            ILogger logger, IRepository<SubscribeActivityLog> subscribeActivityLogRepository,
            ICampaignMessengerService campaignMessengerService)
        {
            _cacheManager = cacheManager;
            _workContext = workContext;
            _customerActivityService = customerActivityService;
            _subscribeCustomerService = subscribeCustomerService;
            _logger = logger;
            _subscribeActivityLogRepository = subscribeActivityLogRepository;
            _campaignMessengerService = campaignMessengerService;
        }
        #endregion


        #region Utilities

        public virtual SubscribeActivityLog InsertSubscribeActivityLogViewProduct(int entityId, string comment)
        {
            return InsertSubscribeActivityLog(_workContext.CurrentCustomer, ACTIVITYTYPE_VIEW_PRODUCT, ENTITY_PRODUCT, entityId, comment);
        }

        /// <summary>
        /// Gets all activity log types (class for caching)
        /// </summary>
        /// <returns>Activity log types</returns>
        protected virtual IList<ActivityLogTypeForCaching> GetAllActivityTypesCached()
        {
            //cache
            string key = string.Format(ACTIVITYTYPE_ALL_KEY);
            return _cacheManager.Get(key, () =>
            {
                var result = new List<ActivityLogTypeForCaching>();
                var activityLogTypes = _customerActivityService.GetAllActivityTypes();
                foreach (var alt in activityLogTypes)
                {
                    var altForCaching = new ActivityLogTypeForCaching
                    {
                        Id = alt.Id,
                        SystemKeyword = alt.SystemKeyword,
                        Name = alt.Name,
                        Enabled = alt.Enabled
                    };
                    result.Add(altForCaching);
                }
                return result;
            });
        }

        public SubscribeActivityLog InsertSubscribeActivityLogViewCategory(int entityId, string comment)
        {
            return InsertSubscribeActivityLog(_workContext.CurrentCustomer, ACTIVITYTYPE_VIEW_CATEGORY, ENTITY_CATEGORY, entityId, comment);
        }

        public SubscribeActivityLog InsertSubscribeActivityAddToCart(string comment)
        {
            return InsertSubscribeActivityLog(_workContext.CurrentCustomer, ACTIVITYTYPE_ADD_TO_SHOPPING_CART, string.Empty, 0, comment);
        }

        public SubscribeActivityLog InsertSubscribeActivityPlaceOrder(int entityId, string comment)
        {
            return InsertSubscribeActivityLog(_workContext.CurrentCustomer, ACTIVITYTYPE_PLACE_ORDER, ENTITY_ORDER, entityId, comment);
        }

        public virtual SubscribeActivityLog InsertSubscribeActivityLog(Customer customer, string systemKeyword, string entityName, int entityId, string comment)
        {
            try
            {
                if (customer == null)
                    return null;

                var subscribeCustomer = _subscribeCustomerService.GetByCustomer(customer.Id);
                if (subscribeCustomer == null)
                    return null;

                var activityTypes = GetAllActivityTypesCached();
                var activityType = activityTypes.ToList().Find(at => at.SystemKeyword == systemKeyword);
                if (activityType == null)
                    throw new Exception("Cannot Find Activity Type!");

                comment = CommonHelper.EnsureMaximumLength(comment, 4000);

                var subscribeActivityLog = new SubscribeActivityLog();
                subscribeActivityLog.ActivityLogTypeId = activityType.Id;
                subscribeActivityLog.SubscribeCustomer = subscribeCustomer;
                subscribeActivityLog.Comment = comment;
                subscribeActivityLog.EntityName = entityName;
                subscribeActivityLog.EntityId = entityId;
                subscribeActivityLog.CreatedOnUtc = DateTime.UtcNow;


                _subscribeActivityLogRepository.Insert(subscribeActivityLog);

                return subscribeActivityLog;
            }
            catch(Exception ex)
            {
                _logger.Error("Subscribe Log Activity", ex, customer);
                return null;
            }
        }

        public virtual SubscribeActivityLog GetActivityLogPlaceOrder(int orderId)
        {
            var activityTypes = GetAllActivityTypesCached();
            var activityTypePlaceOrder = activityTypes.ToList().Find(at => at.SystemKeyword == ACTIVITYTYPE_PLACE_ORDER);
            if (activityTypePlaceOrder == null)
                return null;
            var result = _subscribeActivityLogRepository.Table.Where(scal => scal.ActivityLogTypeId == activityTypePlaceOrder.Id && scal.EntityName == ENTITY_ORDER && scal.EntityId == orderId).FirstOrDefault();
            return result;
        }

        /// <summary>
        /// Get All Activity Log of SubscribeCustomerID
        /// </summary>
        /// <param name="subscribeCustomerId"></param>
        /// <returns></returns>
        public List<SubscribeActivityLog> GetActivityLogBySubscribeCustomerId(int subscribeCustomerId)
        {
            return _subscribeActivityLogRepository.Table.Where(scal => scal.SubscribeCustomerId == subscribeCustomerId).ToList();
        }

        public List<SubscribeActivityLog> GetViewProductLogsOfSubscribeCustomer(int subscribeCustomerId)
        {
            var activityTypes = GetAllActivityTypesCached();
            var activityTypeViewProduct = activityTypes.ToList().Find(at => at.SystemKeyword == ACTIVITYTYPE_VIEW_PRODUCT);
            if (activityTypeViewProduct == null)
                return null;
            var result = _subscribeActivityLogRepository.Table.Where(scal => scal.ActivityLogTypeId == activityTypeViewProduct.Id && scal.EntityName == ENTITY_PRODUCT && scal.SubscribeCustomerId == subscribeCustomerId).ToList();

            return result;
        }

        public List<SubscribeActivityLog> GetAddToCartLogsOfSubscribeCustomer(int subscribeCustomerId)
        {
            var activityTypes = GetAllActivityTypesCached();
            var activityTypeAdd2Cart = activityTypes.ToList().Find(at => at.SystemKeyword == ACTIVITYTYPE_ADD_TO_SHOPPING_CART);
            if (activityTypeAdd2Cart == null)
                return null;
            var result = _subscribeActivityLogRepository.Table.Where(scal => scal.ActivityLogTypeId == activityTypeAdd2Cart.Id && scal.SubscribeCustomerId == subscribeCustomerId).ToList();

            return result;
        }

        public List<SubscribeActivityLog> GetPlaceOrderLogsOfSubscribeCustomer(int subscribeCustomerId)
        {
            var activityTypes = GetAllActivityTypesCached();
            var activityTypePlaceOrder = activityTypes.ToList().Find(at => at.SystemKeyword == ACTIVITYTYPE_PLACE_ORDER);
            if (activityTypePlaceOrder == null)
                return null;
            var result = _subscribeActivityLogRepository.Table.Where(scal => scal.ActivityLogTypeId == activityTypePlaceOrder.Id && scal.EntityName == ENTITY_ORDER && scal.SubscribeCustomerId == subscribeCustomerId).ToList();

            return result;
        }

        public List<int> GetSubscribeCustomersIdByCampignEntity(CampaignEntity campainEntity)
        {
            var activityTypes = GetAllActivityTypesCached();

            List<int> subscribeCustomerIds = new List<int>();

            // get subscribe customer interest campaign entity
            if (campainEntity.EntityName.ToLower().Equals(CampaignEntityType.Category.ToString().ToLower()))
            {
                var activityTypeViewCategory = activityTypes.ToList().Find(at => at.SystemKeyword == ACTIVITYTYPE_VIEW_CATEGORY);
                subscribeCustomerIds = _subscribeActivityLogRepository.Table.Where(sca => (sca.ActivityLogTypeId == activityTypeViewCategory.Id)
                                                                                    && (sca.EntityName.Equals(ENTITY_CATEGORY)) 
                                                                                    && (sca.EntityId.Value == campainEntity.EntityId))
                                                                       .Select(sca => sca.SubscribeCustomerId.Value).Distinct().ToList();
            }
            else if (campainEntity.EntityName.ToLower().Equals(CampaignEntityType.Product.ToString().ToLower()))
            {
                var activityTypeViewProduct = activityTypes.ToList().Find(at => at.SystemKeyword == ACTIVITYTYPE_VIEW_PRODUCT);
                subscribeCustomerIds = _subscribeActivityLogRepository.Table.Where(sca => (sca.ActivityLogTypeId == activityTypeViewProduct.Id) 
                                                                                          && (sca.EntityName.Equals(ENTITY_PRODUCT)) 
                                                                                          && (sca.EntityId.Value == campainEntity.EntityId))
                                                                      .Select(sca => sca.SubscribeCustomerId.Value).Distinct().ToList();
            }

            // filter condition campaign messenger
            return subscribeCustomerIds;
        }

        /// <summary>
        /// Filter condition of Subscribe Customer by CampaignMessenger
        /// Page View
        /// Campaign Type: View, Add2Cart, Checkout
        /// </summary>
        /// <param name="subscribeCustomerId"></param>
        /// <param name="campaignMessenger"></param>
        /// <returns>
        /// Return true if subscribeCustomer match with campaign messenger condition
        /// </returns>
        public bool CheckSubscribeCustomerByCampaignMessenger(int subscribeCustomerId, CampaignMessenger campaignMessenger)
        {
            bool result = false;

            // check action type
            if (campaignMessenger.ActionType.ToLower().Equals(CampaignActionType.View.ToString().ToLower()))
            {
                result = true;
            }
            else if (campaignMessenger.ActionType.ToLower().Equals(CampaignActionType.Add2Cart.ToString().ToLower()))
            {
                var add2CartActivity = GetAddToCartLogsOfSubscribeCustomer(subscribeCustomerId);
                if (add2CartActivity.Count() > 0)
                    result = true;
            }
            else if (campaignMessenger.ActionType.ToLower().Equals(CampaignActionType.Purchage.ToString().ToLower()))
            {
                var placeOrderActivity = GetPlaceOrderLogsOfSubscribeCustomer(subscribeCustomerId);
                if (placeOrderActivity.Count() > 0)
                    result = true;
            }

            // check number page view
            if (result)
            {
                int countActivityLogs = _subscribeActivityLogRepository.Table.Where(acl => acl.SubscribeCustomerId == subscribeCustomerId).Count();
                if (countActivityLogs >= campaignMessenger.PageView)
                    result = true;
            }

            return result;
        }
        #endregion
    }
}
