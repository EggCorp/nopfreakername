﻿using Nop.Core.Data;
using Nop.Core.Domain.Customers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nop.Services.Customers
{
    public partial class SubscribeCustomerService : ISubscribeCustomerService
    {
        private readonly IRepository<SubscribeCustomer> _subscribeCustomerRepository;

        public SubscribeCustomerService(IRepository<SubscribeCustomer> subscribeCustomerRepository)
        {
            this._subscribeCustomerRepository = subscribeCustomerRepository;
        }

        public SubscribeCustomer GetByCustomer(int customerId)
        {
            return _subscribeCustomerRepository.Table.Where(sc => sc.CustomerId == customerId).FirstOrDefault();
        }

        public SubscribeCustomer GetById(int id)
        {
            return _subscribeCustomerRepository.GetById(id);
        }

        public virtual SubscribeCustomer InsertSubscribeCustomer(SubscribeCustomer scCustomer)
        {
            if (scCustomer == null)
                throw new ArgumentNullException("ScCustomer");

            _subscribeCustomerRepository.Insert(scCustomer);
            return scCustomer;
        }

        public IList<SubscribeCustomer> Search()
        {
            try
            {
                var query = _subscribeCustomerRepository.Table.OrderByDescending(sc=> sc.CreatedDate);
                
                return query.ToList();
            }
            catch
            {
                // do nothing
            }
            return null;
        }

        public void UpdateSubscribeCustomer(SubscribeCustomer subscribeCustomer)
        {
            _subscribeCustomerRepository.Update(subscribeCustomer);
        }
    }
}
