﻿using Nop.Core;
using Nop.Core.Domain.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Catalog
{
    public partial interface IHotTrendingService
    {
        void DeleteHotTrending(HotTrending hotTrending);
        HotTrending GetHotTrendingById(int hotTrendingId);
        IPagedList<HotTrending> GetHotTrendings(int pageIndex = 0, int pageSize = int.MaxValue);
        void InsertHotTrending(HotTrending hotTrending);
        void UpdateHotTrending(HotTrending hotTrending);
        IList<HotTrending> GetHotTrendingForHomePage();
    }
}
