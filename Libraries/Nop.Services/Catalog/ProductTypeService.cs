﻿using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Services.Events;
using Nop.Services.Seo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Catalog
{
    public class ProductTypeService: IProductTypeService
    {
        #region Fields

        private readonly IRepository<ProductTypeInfo> _productTypeInfoRepository;
        private readonly IRepository<ProductShippingInfoTemplate> _productShippingInfoTemplateRepository;
        private readonly IRepository<ProductInfoTemplate> _productInfoTemplateRepository;
        private readonly IRepository<ProductSizingInfoTemplate> _productSizingInfoTemplateRepository;
        private readonly IRepository<ShippingByCountry> _shippingByCountryRepository;
        private readonly IEventPublisher _eventPublisher;
        private readonly ICacheManager _cacheManager;
        private readonly IProductService _productService;
        private readonly IProductAttributeService _productAttributeService;

        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="cacheManager">Cache manager</param>
        /// <param name="productCustomFieldRepository">Product attribute repository</param>
        /// <param name="productCustomFieldMappingRepository">Product attribute mapping repository</param>
        /// <param name="productCustomFieldCombinationRepository">Product attribute combination repository</param>
        /// <param name="productCustomFieldValueRepository">Product attribute value repository</param>
        /// <param name="predefinedProductCustomFieldValueRepository">Predefined product attribute value repository</param>
        /// <param name="eventPublisher">Event published</param>
        public ProductTypeService(ICacheManager cacheManager,
            IRepository<ProductTypeInfo> productTypeInfoRepository,
            IRepository<ProductShippingInfoTemplate> productShippingInfoTemplateRepository,
            IRepository<ProductInfoTemplate> productInfoTemplateRepository,
            IRepository<ProductSizingInfoTemplate> productSizingInfoTemplateRepository,
            IRepository<ShippingByCountry> shippingByCountryRepository,
            IEventPublisher eventPublisher,
            IProductService productService,
            IProductAttributeService productAttributeService)
        {
            this._cacheManager = cacheManager;
           
            this._eventPublisher = eventPublisher;
            this._productAttributeService = productAttributeService;
            this._productService = productService;
            this._productInfoTemplateRepository = productInfoTemplateRepository;
            this._productShippingInfoTemplateRepository = productShippingInfoTemplateRepository;
            this._productSizingInfoTemplateRepository = productSizingInfoTemplateRepository;
            this._productTypeInfoRepository = productTypeInfoRepository;
            this._shippingByCountryRepository = shippingByCountryRepository;
        }

        #endregion

        #region Methods

        #region Product type info

        /// <summary>
        /// Deletes
        /// </summary>
        public virtual void DeleteProductType(ProductTypeInfo model)
        {
            if (model == null)
                throw new ArgumentNullException("productType");

            _productTypeInfoRepository.Delete(model);

        }

        /// <summary>
        /// Gets all
        /// </summary>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        public virtual IPagedList<ProductTypeInfo> GetAllProductTypes(int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = from pa in _productTypeInfoRepository.Table
                        orderby pa.TypeName
                        select pa;
            var result = new PagedList<ProductTypeInfo>(query, pageIndex, pageSize);
            return result;
        }

        /// <summary>
        /// Gets 
        /// </summary>
        public virtual ProductTypeInfo GetProductTypeById(int id)
        {
            if (id == 0)
                return null;
            return _productTypeInfoRepository.Table.FirstOrDefault(p=>p.Id==id);
        }

        /// <summary>
        /// Inserts 
        /// </summary>
        public virtual void InsertProductTypeInfo(ProductTypeInfo model)
        {
            if (model == null)
                throw new ArgumentNullException("productType");

            _productTypeInfoRepository.Insert(model);

        }

        /// <summary>
        /// Updates 
        /// </summary>
        public virtual void UpdateProductTypeInfo(ProductTypeInfo model)
        {
            if (model == null)
                throw new ArgumentNullException("productType");

            _productTypeInfoRepository.Update(model);

        }
        public virtual IList<ProductTypeInfo> GetAvialableTypes()
        {
            var query = from pa in _productTypeInfoRepository.Table
                        where pa.IsActive
                        orderby pa.TypeName
                        select pa;
            return query.ToList();
        }

        #endregion

        # region Product Sizing Info Template

        /// <summary>
        /// Deletes
        /// </summary>
        public virtual void DeleteProductSizingInfoTemplate(ProductSizingInfoTemplate model)
        {
            if (model == null)
                throw new ArgumentNullException("ProductSizingInfoTemplate");

            _productSizingInfoTemplateRepository.Delete(model);

        }

        /// <summary>
        /// Gets all
        /// </summary>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        public virtual IPagedList<ProductSizingInfoTemplate> GetAllProductSizingInfoTemplate(int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = from pa in _productSizingInfoTemplateRepository.Table
                        orderby pa.Name
                        select pa;
            var result = new PagedList<ProductSizingInfoTemplate>(query, pageIndex, pageSize);
            return result;
        }

        /// <summary>
        /// Gets 
        /// </summary>
        public virtual ProductSizingInfoTemplate GetProductSizingInfoTemplateById(int id)
        {
            if (id == 0)
                return null;
            return _productSizingInfoTemplateRepository.GetById(id);
        }

        /// <summary>
        /// Inserts 
        /// </summary>
        public virtual void InsertProductSizingInfoTemplate(ProductSizingInfoTemplate model)
        {
            if (model == null)
                throw new ArgumentNullException("ProductSizingInfoTemplate");

            _productSizingInfoTemplateRepository.Insert(model);

        }

        /// <summary>
        /// Updates 
        /// </summary>
        public virtual void UpdateProductSizingInfoTemplate(ProductSizingInfoTemplate model)
        {
            if (model == null)
                throw new ArgumentNullException("ProductSizingInfoTemplate");

            _productSizingInfoTemplateRepository.Update(model);

        }

        #endregion

        #region Product Shipping Info Template

        /// <summary>
        /// Deletes
        /// </summary>
        public virtual void DeleteProductShippingInfoTemplate(ProductShippingInfoTemplate model)
        {
            if (model == null)
                throw new ArgumentNullException("ProductShippingInfoTemplate");

            _productShippingInfoTemplateRepository.Delete(model);

        }

        /// <summary>
        /// Gets all
        /// </summary>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        public virtual IPagedList<ProductShippingInfoTemplate> GetAllProductShippingInfoTemplate(int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = from pa in _productShippingInfoTemplateRepository
                        .Table
                        orderby pa.Name
                        select pa;
            var result = new PagedList<ProductShippingInfoTemplate>(query, pageIndex, pageSize);
            return result;
        }

        /// <summary>
        /// Gets 
        /// </summary>
        public virtual ProductShippingInfoTemplate GetProductShippingInfoTemplateById(int id)
        {
            if (id == 0)
                return null;
            return _productShippingInfoTemplateRepository.GetById(id);
        }

        /// <summary>
        /// Inserts 
        /// </summary>
        public virtual void InsertProductShippingInfoTemplate(ProductShippingInfoTemplate model)
        {
            if (model == null)
                throw new ArgumentNullException("ProductShippingInfoTemplate");

            _productShippingInfoTemplateRepository.Insert(model);

        }

        /// <summary>
        /// Updates 
        /// </summary>
        public virtual void UpdateProductShippingInfoTemplate(ProductShippingInfoTemplate model)
        {
            if (model == null)
                throw new ArgumentNullException("productType");

            _productShippingInfoTemplateRepository.Update(model);

        }

        #endregion

        #region Product Basic Info Template

        /// <summary>
        /// Deletes
        /// </summary>
        public virtual void DeleteProductInfoTemplateTemplate(ProductInfoTemplate model)
        {
            if (model == null)
                throw new ArgumentNullException("ProductInfoTemplate");

            _productInfoTemplateRepository.Delete(model);

        }

        /// <summary>
        /// Gets all
        /// </summary>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        public virtual IPagedList<ProductInfoTemplate> GetAllProductInfoTemplate(int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = from pa in _productInfoTemplateRepository.Table
                        orderby pa.Name
                        select pa;
            var result = new PagedList<ProductInfoTemplate>(query, pageIndex, pageSize);
            return result;
        }

        /// <summary>
        /// Gets 
        /// </summary>
        public virtual ProductInfoTemplate GetProductInfoTemplateById(int id)
        {
            if (id == 0)
                return null;
            return _productInfoTemplateRepository.GetById(id);
        }

        /// <summary>
        /// Inserts 
        /// </summary>
        public virtual void InsertProductInfoTemplate(ProductInfoTemplate model)
        {
            if (model == null)
                throw new ArgumentNullException("ProductInfoTemplate");

            _productInfoTemplateRepository.Insert(model);

        }

        /// <summary>
        /// Updates 
        /// </summary>
        public virtual void UpdateProductInfoTemplate(ProductInfoTemplate model)
        {
            if (model == null)
                throw new ArgumentNullException("ProductInfoTemplate");

            _productInfoTemplateRepository.Update(model);

        }

        #endregion

        #region type for product
        public virtual IList<ProductContentForAttributeValue> GetProductContentForAttributeValues(int productId,int selectedId =0)
        {
            try
            {
                var product = _productService.GetProductById(productId);
                if (product == null)
                    return null;
                var allPam = product.ProductAttributeMappings;
                var pamStyle = allPam.FirstOrDefault(p => p.ProductAttribute.Name.Equals("Available Products"));
                if(pamStyle!=null &&pamStyle.ShouldHaveValues())
                {
                    var result = new List<ProductContentForAttributeValue>();
                    var availableTypes = this.GetAllProductTypes();
                   foreach(var style in pamStyle.ProductAttributeValues)
                    {
                        var convertedName = SeoExtensions.GetSeName(style.Name);
                        var matchedType = availableTypes.FirstOrDefault(p => p.TypeCode.Equals(convertedName));
                        if(matchedType!=null)
                        {
                            var model = new ProductContentForAttributeValue();
                            matchedType.ProductInfoTemplate = this.GetProductInfoTemplateById(matchedType.ProductInfoTemplateId.Value);
                            matchedType.ProductShippingInfoTemplate = this.GetProductShippingInfoTemplateById(matchedType.ProductShippingInfoTemplateId.Value);
                            matchedType.ProductSizingInfoTemplate = this.GetProductSizingInfoTemplateById(matchedType.ProductSizingInfoTemplateId.Value);

                            model.TypeInformation = matchedType;
                            model.ProductAttributeValueId = style.Id;
                            model.IsPreselected= selectedId == 0 ? style.IsPreSelected : (style.Id == selectedId);
                            result.Add(model);
                        }
                    }
                    return result;
                }
            }
            catch(Exception ex)
            { }
            return null;
        }

        public ShippingByCountry GetShippingByCountryByProductTypeName(string productTypeName)
        {
            return _shippingByCountryRepository.Table.Where(sbc => sbc.ProductType.ToLower().Equals(productTypeName.Trim().ToLower())).FirstOrDefault();
        }
        #endregion
        #endregion
    }
}
