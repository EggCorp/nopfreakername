﻿using Nop.Core;
using Nop.Core.Domain.Customs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Catalog
{
    public partial interface IProductCustomFieldService
    {
        #region Product customFields

        /// <summary>
        /// Deletes a product customField
        /// </summary>
        /// <param name="productCustomField">Product attribute</param>
        void DeleteProductCustomField(CustomField productCustomField);

        /// <summary>
        /// Gets all product attributes
        /// </summary>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Product customFields</returns>
        IPagedList<CustomField> GetAllProductCustomFields(int pageIndex = 0, int pageSize = int.MaxValue);

        /// <summary>
        /// Gets a product customField 
        /// </summary>
        /// <param name="productCustomFieldId">Product attribute identifier</param>
        /// <returns>Product attribute </returns>
        CustomField GetProductCustomFieldById(int productCustomFieldId);

        /// <summary>
        /// Inserts a product customField
        /// </summary>
        /// <param name="productCustomField">Product attribute</param>
        void InsertProductCustomField(CustomField productCustomField);

        /// <summary>
        /// Updates the product customField
        /// </summary>
        /// <param name="productCustomField">Product attribute</param>
        void UpdateProductCustomField(CustomField productCustomField);

        /// <summary>
        /// Returns a list of IDs of not existing attributes
        /// </summary>
        /// <param name="attributeId">The IDs of the attributes to check</param>
        /// <returns>List of IDs not existing attributes</returns>
        int[] GetNotExistingAttributes(int[] attributeId);

        #endregion

        #region Product customFields mappings

        /// <summary>
        /// Deletes a product customField mapping
        /// </summary>
        /// <param name="productCustomFieldMapping">Product attribute mapping</param>
        void DeleteProductCustomFieldMapping(ProductCustomFieldMapping productCustomFieldMapping);

        /// <summary>
        /// Gets product customField mappings by product identifier
        /// </summary>
        /// <param name="productId">The product identifier</param>
        /// <returns>Product attribute mapping collection</returns>
        IList<ProductCustomFieldMapping> GetProductCustomFieldMappingsByProductId(int productId);

        /// <summary>
        /// Gets a product customField mapping
        /// </summary>
        /// <param name="productCustomFieldMappingId">Product attribute mapping identifier</param>
        /// <returns>Product attribute mapping</returns>
        ProductCustomFieldMapping GetProductCustomFieldMappingById(int productCustomFieldMappingId);

        /// <summary>
        /// Inserts a product customField mapping
        /// </summary>
        /// <param name="productCustomFieldMapping">The product customField mapping</param>
        void InsertProductCustomFieldMapping(ProductCustomFieldMapping productCustomFieldMapping);

        /// <summary>
        /// Updates the product customField mapping
        /// </summary>
        /// <param name="productCustomFieldMapping">The product customField mapping</param>
        void UpdateProductCustomFieldMapping(ProductCustomFieldMapping productCustomFieldMapping);

        #endregion

        #region Product attribute values

        /// <summary>
        /// Deletes a product customField value
        /// </summary>
        /// <param name="productCustomFieldValue">Product attribute value</param>
        void DeleteProductCustomFieldValue(ProductCustomFieldValue productCustomFieldValue);

        /// <summary>
        /// Gets product customField values by product customField mapping identifier
        /// </summary>
        /// <param name="productCustomFieldMappingId">The product customField mapping identifier</param>
        /// <returns>Product attribute values</returns>
        IList<ProductCustomFieldValue> GetProductCustomFieldValues(int productCustomFieldMappingId);

        /// <summary>
        /// Gets a product customField value
        /// </summary>
        /// <param name="productCustomFieldValueId">Product attribute value identifier</param>
        /// <returns>Product attribute value</returns>
        ProductCustomFieldValue GetProductCustomFieldValueById(int productCustomFieldValueId);

        /// <summary>
        /// Inserts a product customField value
        /// </summary>
        /// <param name="productCustomFieldValue">The product customField value</param>
        void InsertProductCustomFieldValue(ProductCustomFieldValue productCustomFieldValue);

        /// <summary>
        /// Updates the product customField value
        /// </summary>
        /// <param name="productCustomFieldValue">The product customField value</param>
        void UpdateProductCustomFieldValue(ProductCustomFieldValue productCustomFieldValue);

        IList<ProductCustomFieldValue> GenerateProductCustomFieldValueForVariant(int productId, int productCustomFieldMappingId);


        /// <summary>
        /// Deletes a product customField
        /// </summary>
        /// <param name="productCustomField">Product attribute</param>
        void DeleteProductCustomFieldTemplatePicture(ProductCustomFieldTemplatePicture productCustomFieldTemplatePicture);


        /// <summary>
        /// Gets a product customField 
        /// </summary>
        /// <param name="productCustomFieldId">Product attribute identifier</param>
        /// <returns>Product attribute </returns>
        ProductCustomFieldTemplatePicture GetProductCustomFieldTemplatePictureById(int id);

        /// <summary>
        /// Inserts a product customField
        /// </summary>
        /// <param name="productCustomField">Product attribute</param>
        void InsertProductCustomFieldTemplatePicture(ProductCustomFieldTemplatePicture productCustomFieldTemplatePicture);

        /// <summary>
        /// Updates the product customField
        /// </summary>
        /// <param name="productCustomField">Product attribute</param>
        void UpdateProductCustomFieldTemplatePicture(ProductCustomFieldTemplatePicture productCustomFieldTemplatePicture);

        ProductCustomFieldTemplatePicture GetCustomPictureForProductAttributeValue(int attrId);

        #endregion

    }
}
