﻿using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Customs;
using Nop.Services.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Catalog
{
    public class ProductCustomFieldService: IProductCustomFieldService
    {
        #region Constants
        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : page index
        /// {1} : page size
        /// </remarks>
        private const string PRODUCTCUSTOMFIELDS_ALL_KEY = "Nop.productcustomfield.all-{0}-{1}";
        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : product attribute ID
        /// </remarks>
        private const string PRODUCTCUSTOMFIELDS_BY_ID_KEY = "Nop.productcustomfield.id-{0}";
        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : product ID
        /// </remarks>
        private const string PRODUCTCUSTOMFIELDMAPPINGS_ALL_KEY = "Nop.productcustomfieldmapping.all-{0}";
        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : product attribute mapping ID
        /// </remarks>
        private const string PRODUCTCUSTOMFIELDMAPPINGS_BY_ID_KEY = "Nop.productcustomfieldmapping.id-{0}";
        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : product attribute mapping ID
        /// </remarks>
        private const string PRODUCTCUSTOMFIELDVALUES_ALL_KEY = "Nop.productcustomfieldvalue.all-{0}";
        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : product attribute value ID
        /// </remarks>
        private const string PRODUCTCUSTOMFIELDVALUES_BY_ID_KEY = "Nop.productcustomfieldvalue.id-{0}";
        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : product ID
        /// </remarks>
        private const string PRODUCTCUSTOMFIELDCOMBINATIONS_ALL_KEY = "Nop.productcustomfieldcombination.all-{0}";
        /// <summary>
        /// Key pattern to clear cache
        /// </summary>
        private const string PRODUCTCUSTOMFIELDS_PATTERN_KEY = "Nop.productcustomfield.";
        /// <summary>
        /// Key pattern to clear cache
        /// </summary>
        private const string PRODUCTCUSTOMFIELDMAPPINGS_PATTERN_KEY = "Nop.productcustomfieldmapping.";
        /// <summary>
        /// Key pattern to clear cache
        /// </summary>
        private const string PRODUCTCUSTOMFIELDVALUES_PATTERN_KEY = "Nop.productcustomfieldvalue.";
        /// <summary>
        /// Key pattern to clear cache
        /// </summary>
        private const string PRODUCTCUSTOMFIELDCOMBINATIONS_PATTERN_KEY = "Nop.productcustomfieldcombination.";

        private const int DefaultCustomfieldPositionX = 320;
        private const int DefaultCustomfieldPositionY = 500;

        #endregion

        #region Fields

        private readonly IRepository<CustomField> _productCustomFieldRepository;
        private readonly IRepository<ProductCustomFieldMapping> _productCustomFieldMappingRepository;
        private readonly IRepository<ProductCustomFieldValue> _productCustomFieldValueRepository;
        private readonly IEventPublisher _eventPublisher;
        private readonly ICacheManager _cacheManager;
        private readonly IProductService _productService;
        private readonly IProductAttributeService _productAttributeService;
        private readonly IRepository<ProductCustomFieldTemplatePicture> _productCustomFieldTemplatePicture;
        private readonly IProductDesignService _productDesignService;

        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="cacheManager">Cache manager</param>
        /// <param name="productCustomFieldRepository">Product attribute repository</param>
        /// <param name="productCustomFieldMappingRepository">Product attribute mapping repository</param>
        /// <param name="productCustomFieldCombinationRepository">Product attribute combination repository</param>
        /// <param name="productCustomFieldValueRepository">Product attribute value repository</param>
        /// <param name="predefinedProductCustomFieldValueRepository">Predefined product attribute value repository</param>
        /// <param name="eventPublisher">Event published</param>
        public ProductCustomFieldService(ICacheManager cacheManager,
            IRepository<CustomField> productCustomFieldRepository,
            IRepository<ProductCustomFieldMapping> productCustomFieldMappingRepository,
            IRepository<ProductCustomFieldValue> productCustomFieldValueRepository,
            IEventPublisher eventPublisher,
            IProductService productService,
            IProductAttributeService productAttributeService,
            IRepository<ProductCustomFieldTemplatePicture> productCustomFieldTemplatePicture,
            IProductDesignService productDesignService)
        {
            this._cacheManager = cacheManager;
            this._productCustomFieldRepository = productCustomFieldRepository;
            this._productCustomFieldMappingRepository = productCustomFieldMappingRepository;
            this._productCustomFieldValueRepository = productCustomFieldValueRepository;
            this._eventPublisher = eventPublisher;
            this._productAttributeService = productAttributeService;
            this._productService = productService;
            this._productCustomFieldTemplatePicture = productCustomFieldTemplatePicture;
            this._productDesignService = productDesignService;
        }

        #endregion

        #region Methods

        #region Product attributes

        /// <summary>
        /// Deletes a product attribute
        /// </summary>
        /// <param name="productCustomField">Product attribute</param>
        public virtual void DeleteProductCustomField(CustomField productCustomField)
        {
            if (productCustomField == null)
                throw new ArgumentNullException("productCustomField");

            _productCustomFieldRepository.Delete(productCustomField);

            //cache
            _cacheManager.RemoveByPattern(PRODUCTCUSTOMFIELDS_PATTERN_KEY);
            _cacheManager.RemoveByPattern(PRODUCTCUSTOMFIELDMAPPINGS_PATTERN_KEY);
            _cacheManager.RemoveByPattern(PRODUCTCUSTOMFIELDVALUES_PATTERN_KEY);
            _cacheManager.RemoveByPattern(PRODUCTCUSTOMFIELDCOMBINATIONS_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityDeleted(productCustomField);
        }

        /// <summary>
        /// Gets all product attributes
        /// </summary>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Product attributes</returns>
        public virtual IPagedList<CustomField> GetAllProductCustomFields(int pageIndex = 0, int pageSize = int.MaxValue)
        {
            string key = string.Format(PRODUCTCUSTOMFIELDS_ALL_KEY, pageIndex, pageSize);
            return _cacheManager.Get(key, () =>
            {
                var query = from pa in _productCustomFieldRepository.Table
                            orderby pa.Name
                            select pa;
                var productCustomFields = new PagedList<CustomField>(query, pageIndex, pageSize);
                return productCustomFields;
            });
        }

        /// <summary>
        /// Gets a product attribute 
        /// </summary>
        /// <param name="productCustomFieldId">Product attribute identifier</param>
        /// <returns>Product attribute </returns>
        public virtual CustomField GetProductCustomFieldById(int productCustomFieldId)
        {
            if (productCustomFieldId == 0)
                return null;
            string key = string.Format(PRODUCTCUSTOMFIELDS_BY_ID_KEY, productCustomFieldId);
            return _cacheManager.Get(key, () => _productCustomFieldRepository.GetById(productCustomFieldId));
        }

        /// <summary>
        /// Inserts a product attribute
        /// </summary>
        /// <param name="productCustomField">Product attribute</param>
        public virtual void InsertProductCustomField(CustomField productCustomField)
        {
            if (productCustomField == null)
                throw new ArgumentNullException("productCustomField");

            _productCustomFieldRepository.Insert(productCustomField);

            //cache
            _cacheManager.RemoveByPattern(PRODUCTCUSTOMFIELDS_PATTERN_KEY);
            _cacheManager.RemoveByPattern(PRODUCTCUSTOMFIELDMAPPINGS_PATTERN_KEY);
            _cacheManager.RemoveByPattern(PRODUCTCUSTOMFIELDVALUES_PATTERN_KEY);
            _cacheManager.RemoveByPattern(PRODUCTCUSTOMFIELDCOMBINATIONS_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityInserted(productCustomField);
        }

        /// <summary>
        /// Updates the product attribute
        /// </summary>
        /// <param name="productCustomField">Product attribute</param>
        public virtual void UpdateProductCustomField(CustomField productCustomField)
        {
            if (productCustomField == null)
                throw new ArgumentNullException("productCustomField");

            _productCustomFieldRepository.Update(productCustomField);

            //cache
            _cacheManager.RemoveByPattern(PRODUCTCUSTOMFIELDS_PATTERN_KEY);
            _cacheManager.RemoveByPattern(PRODUCTCUSTOMFIELDMAPPINGS_PATTERN_KEY);
            _cacheManager.RemoveByPattern(PRODUCTCUSTOMFIELDVALUES_PATTERN_KEY);
            _cacheManager.RemoveByPattern(PRODUCTCUSTOMFIELDCOMBINATIONS_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityUpdated(productCustomField);
        }

        /// <summary>
        /// Returns a list of IDs of not existing attributes
        /// </summary>
        /// <param name="attributeId">The IDs of the attributes to check</param>
        /// <returns>List of IDs not existing attributes</returns>
        public virtual int[] GetNotExistingAttributes(int[] attributeId)
        {
            if (attributeId == null)
                throw new ArgumentNullException("attributeId");

            var query = _productCustomFieldRepository.Table;
            var queryFilter = attributeId.Distinct().ToArray();
            var filter = query.Select(a => a.Id).Where(m => queryFilter.Contains(m)).ToList();
            return queryFilter.Except(filter).ToArray();
        }

        #endregion

        #region Product attributes mappings

        /// <summary>
        /// Deletes a product attribute mapping
        /// </summary>
        /// <param name="productCustomFieldMapping">Product attribute mapping</param>
        public virtual void DeleteProductCustomFieldMapping(ProductCustomFieldMapping productCustomFieldMapping)
        {
            if (productCustomFieldMapping == null)
                throw new ArgumentNullException("productCustomFieldMapping");

            _productCustomFieldMappingRepository.Delete(productCustomFieldMapping);

            //cache
            _cacheManager.RemoveByPattern(PRODUCTCUSTOMFIELDS_PATTERN_KEY);
            _cacheManager.RemoveByPattern(PRODUCTCUSTOMFIELDMAPPINGS_PATTERN_KEY);
            _cacheManager.RemoveByPattern(PRODUCTCUSTOMFIELDVALUES_PATTERN_KEY);
            _cacheManager.RemoveByPattern(PRODUCTCUSTOMFIELDCOMBINATIONS_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityDeleted(productCustomFieldMapping);
        }

        /// <summary>
        /// Gets product attribute mappings by product identifier
        /// </summary>
        /// <param name="productId">The product identifier</param>
        /// <returns>Product attribute mapping collection</returns>
        public virtual IList<ProductCustomFieldMapping> GetProductCustomFieldMappingsByProductId(int productId)
        {
            string key = string.Format(PRODUCTCUSTOMFIELDMAPPINGS_ALL_KEY, productId);

            return _cacheManager.Get(key, () =>
            {
                var query = from pam in _productCustomFieldMappingRepository.Table
                            where pam.ProductId == productId
                            select pam;
                var productCustomFieldMappings = query.ToList();
                return productCustomFieldMappings;
            });
        }

        /// <summary>
        /// Gets a product attribute mapping
        /// </summary>
        /// <param name="productCustomFieldMappingId">Product attribute mapping identifier</param>
        /// <returns>Product attribute mapping</returns>
        public virtual ProductCustomFieldMapping GetProductCustomFieldMappingById(int productCustomFieldMappingId)
        {
            if (productCustomFieldMappingId == 0)
                return null;
            return _productCustomFieldMappingRepository.GetById(productCustomFieldMappingId);
        }

        /// <summary>
        /// Inserts a product attribute mapping
        /// </summary>
        /// <param name="productCustomFieldMapping">The product attribute mapping</param>
        public virtual void InsertProductCustomFieldMapping(ProductCustomFieldMapping productCustomFieldMapping)
        {
            if (productCustomFieldMapping == null)
                throw new ArgumentNullException("productCustomFieldMapping");

            _productCustomFieldMappingRepository.Insert(productCustomFieldMapping);

            //cache
            _cacheManager.RemoveByPattern(PRODUCTCUSTOMFIELDS_PATTERN_KEY);
            _cacheManager.RemoveByPattern(PRODUCTCUSTOMFIELDMAPPINGS_PATTERN_KEY);
            _cacheManager.RemoveByPattern(PRODUCTCUSTOMFIELDVALUES_PATTERN_KEY);
            _cacheManager.RemoveByPattern(PRODUCTCUSTOMFIELDCOMBINATIONS_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityInserted(productCustomFieldMapping);
        }

        /// <summary>
        /// Updates the product attribute mapping
        /// </summary>
        /// <param name="productCustomFieldMapping">The product attribute mapping</param>
        public virtual void UpdateProductCustomFieldMapping(ProductCustomFieldMapping productCustomFieldMapping)
        {
            if (productCustomFieldMapping == null)
                throw new ArgumentNullException("productCustomFieldMapping");

            _productCustomFieldMappingRepository.Update(productCustomFieldMapping);

            //cache
            _cacheManager.RemoveByPattern(PRODUCTCUSTOMFIELDS_PATTERN_KEY);
            _cacheManager.RemoveByPattern(PRODUCTCUSTOMFIELDMAPPINGS_PATTERN_KEY);
            _cacheManager.RemoveByPattern(PRODUCTCUSTOMFIELDVALUES_PATTERN_KEY);
            _cacheManager.RemoveByPattern(PRODUCTCUSTOMFIELDCOMBINATIONS_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityUpdated(productCustomFieldMapping);
        }

        #endregion

        #region Product attribute values

        /// <summary>
        /// Deletes a product attribute value
        /// </summary>
        /// <param name="productCustomFieldValue">Product attribute value</param>
        public virtual void DeleteProductCustomFieldValue(ProductCustomFieldValue productCustomFieldValue)
        {
            if (productCustomFieldValue == null)
                throw new ArgumentNullException("productCustomFieldValue");

            _productCustomFieldValueRepository.Delete(productCustomFieldValue);

            //cache
            _cacheManager.RemoveByPattern(PRODUCTCUSTOMFIELDS_PATTERN_KEY);
            _cacheManager.RemoveByPattern(PRODUCTCUSTOMFIELDMAPPINGS_PATTERN_KEY);
            _cacheManager.RemoveByPattern(PRODUCTCUSTOMFIELDVALUES_PATTERN_KEY);
            _cacheManager.RemoveByPattern(PRODUCTCUSTOMFIELDCOMBINATIONS_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityDeleted(productCustomFieldValue);
        }

        /// <summary>
        /// Gets product attribute values by product attribute mapping identifier
        /// </summary>
        /// <param name="productCustomFieldMappingId">The product attribute mapping identifier</param>
        /// <returns>Product attribute mapping collection</returns>
        public virtual IList<ProductCustomFieldValue> GetProductCustomFieldValues(int productCustomFieldMappingId)
        {
            string key = string.Format(PRODUCTCUSTOMFIELDVALUES_ALL_KEY, productCustomFieldMappingId);
            return _cacheManager.Get(key, () =>
            {
                var query = from pav in _productCustomFieldValueRepository.Table
                            where pav.ProductCustomFieldMappingId == productCustomFieldMappingId
                            select pav;
                var productCustomFieldValues = query.ToList();
                return productCustomFieldValues;
            });
        }

        /// <summary>
        /// Gets a product attribute value
        /// </summary>
        /// <param name="productCustomFieldValueId">Product attribute value identifier</param>
        /// <returns>Product attribute value</returns>
        public virtual ProductCustomFieldValue GetProductCustomFieldValueById(int productCustomFieldValueId)
        {
            if (productCustomFieldValueId == 0)
                return null;

            string key = string.Format(PRODUCTCUSTOMFIELDVALUES_BY_ID_KEY, productCustomFieldValueId);
            return _cacheManager.Get(key, () => _productCustomFieldValueRepository.GetById(productCustomFieldValueId));
        }

        /// <summary>
        /// Inserts a product attribute value
        /// </summary>
        /// <param name="productCustomFieldValue">The product attribute value</param>
        public virtual void InsertProductCustomFieldValue(ProductCustomFieldValue productCustomFieldValue)
        {
            if (productCustomFieldValue == null)
                throw new ArgumentNullException("productCustomFieldValue");

            _productCustomFieldValueRepository.Insert(productCustomFieldValue);

            //cache
            _cacheManager.RemoveByPattern(PRODUCTCUSTOMFIELDS_PATTERN_KEY);
            _cacheManager.RemoveByPattern(PRODUCTCUSTOMFIELDMAPPINGS_PATTERN_KEY);
            _cacheManager.RemoveByPattern(PRODUCTCUSTOMFIELDVALUES_PATTERN_KEY);
            _cacheManager.RemoveByPattern(PRODUCTCUSTOMFIELDCOMBINATIONS_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityInserted(productCustomFieldValue);
        }

        /// <summary>
        /// Updates the product attribute value
        /// </summary>
        /// <param name="productCustomFieldValue">The product attribute value</param>
        public virtual void UpdateProductCustomFieldValue(ProductCustomFieldValue productCustomFieldValue)
        {
            if (productCustomFieldValue == null)
                throw new ArgumentNullException("productCustomFieldValue");

            _productCustomFieldValueRepository.Update(productCustomFieldValue);

            //cache
            _cacheManager.RemoveByPattern(PRODUCTCUSTOMFIELDS_PATTERN_KEY);
            _cacheManager.RemoveByPattern(PRODUCTCUSTOMFIELDMAPPINGS_PATTERN_KEY);
            _cacheManager.RemoveByPattern(PRODUCTCUSTOMFIELDVALUES_PATTERN_KEY);
            _cacheManager.RemoveByPattern(PRODUCTCUSTOMFIELDCOMBINATIONS_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityUpdated(productCustomFieldValue);
        }

        public virtual IList<ProductCustomFieldValue> GenerateProductCustomFieldValueForVariant(int productId, int productCustomFieldMappingId)
        {
            try
            {
                var product = _productService.GetProductById(productId);
                if(product!= null)
                {
                    var result = new List<ProductCustomFieldValue>();
                    var productAvailableAttrValues = product.ProductAttributeMappings
                        .Where(p => p.ProductAttributeId == 1)
                        .Select(p => p.ProductAttributeValues).FirstOrDefault();
                    var productCustomfieldMapping = product.ProductCustomFieldMappings.Where(p => p.Id == productCustomFieldMappingId).FirstOrDefault();
                    if(productCustomfieldMapping!=null)
                    {
                        if(productCustomfieldMapping.ProductCustomFieldValues.Any())
                        {
                            //only insert new variant for product custom field
                            var existedIds = productCustomfieldMapping.ProductCustomFieldValues.Select(p => p.ProductAttributeValueId).ToList();
                            productAvailableAttrValues = productAvailableAttrValues.Where(p => !existedIds.Contains(p.Id)).ToList();
                        }
                        foreach (var attrValue in productAvailableAttrValues)
                        {
                            var tempProductCustomFieldValue = new ProductCustomFieldValue();
                            tempProductCustomFieldValue.PositionX = DefaultCustomfieldPositionX;
                            tempProductCustomFieldValue.PositionY = DefaultCustomfieldPositionY;
                            tempProductCustomFieldValue.ProductAttributeValueId = attrValue.Id;
                            tempProductCustomFieldValue.ProductCustomFieldMappingId = productCustomFieldMappingId;
                            _productCustomFieldValueRepository.Insert(tempProductCustomFieldValue);
                        }
                    }
                    result = this.GetProductCustomFieldMappingById(productCustomFieldMappingId).ProductCustomFieldValues.ToList();
                    return result;
                }

            }
            catch(Exception ex)
            {
            }
            return null;
        }

        #endregion

        #region Product CustomField Template Picture

        /// <summary>
        /// Deletes a product attribute
        /// </summary>
        /// <param name="productCustomField">Product attribute</param>
        public virtual void DeleteProductCustomFieldTemplatePicture(ProductCustomFieldTemplatePicture id)
        {
            if (id == null)
                throw new ArgumentNullException("productCustomFieldTemplatePicture");

            _productCustomFieldTemplatePicture.Delete(id);
        }
        /// <summary>
        /// Gets a product attribute 
        /// </summary>
        /// <param name="productCustomFieldId">Product attribute identifier</param>
        /// <returns>Product attribute </returns>
        public virtual ProductCustomFieldTemplatePicture GetProductCustomFieldTemplatePictureById(int id)
        {
            if (id == 0)
                return null;
            return _productCustomFieldTemplatePicture.GetById(id);
        }

        /// <summary>
        /// Inserts a product attribute
        /// </summary>
        /// <param name="productCustomField">Product attribute</param>
        public virtual void InsertProductCustomFieldTemplatePicture(ProductCustomFieldTemplatePicture productCustomFieldTemplatePicture)
        {
            if (productCustomFieldTemplatePicture == null)
                throw new ArgumentNullException("productCustomFieldTemplatePicture");

            _productCustomFieldTemplatePicture.Insert(productCustomFieldTemplatePicture);
        }

        /// <summary>
        /// Updates the product attribute
        /// </summary>
        /// <param name="ProductCustomFieldTemplatePicture">Product attribute</param>
        public virtual void UpdateProductCustomFieldTemplatePicture(ProductCustomFieldTemplatePicture productCustomFieldTemplatePicture)
        {
            if (productCustomFieldTemplatePicture == null)
                throw new ArgumentNullException("productCustomFieldTemplatePicture");
            _productCustomFieldTemplatePicture.Update(productCustomFieldTemplatePicture);
        }

        public virtual ProductCustomFieldTemplatePicture GetCustomPictureForProductAttributeValue(int attrId)
        {
            var attributeValue = _productAttributeService.GetProductAttributeValueById(attrId);
            if (attributeValue == null)
                throw new ArgumentNullException("productCustomFieldTemplatePicture");

            return  _productCustomFieldTemplatePicture.Table.FirstOrDefault(p => p.ProductAttributeValueId == attributeValue.Id);
        }

        #endregion

        #endregion
    }
}
