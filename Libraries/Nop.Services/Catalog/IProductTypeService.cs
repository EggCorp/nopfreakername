﻿using Nop.Core;
using Nop.Core.Domain.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Catalog
{
    public partial interface IProductTypeService
    {
        #region Product type info

        /// <summary>
        /// Deletes
        /// </summary>
        void DeleteProductType(ProductTypeInfo model);
        /// <summary>
        /// Gets all
        /// </summary>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        IPagedList<ProductTypeInfo> GetAllProductTypes(int pageIndex = 0, int pageSize = int.MaxValue);

        /// <summary>
        /// Gets 
        /// </summary>
        ProductTypeInfo GetProductTypeById(int id);

        /// <summary>
        /// Inserts 
        /// </summary>
        void InsertProductTypeInfo(ProductTypeInfo model);
        /// <summary>
        /// Updates 
        /// </summary>
        void UpdateProductTypeInfo(ProductTypeInfo model);

        IList<ProductTypeInfo> GetAvialableTypes();
        #endregion

        # region Product Sizing Info Template

        /// <summary>
        /// Deletes
        /// </summary>
        void DeleteProductSizingInfoTemplate(ProductSizingInfoTemplate model);

        /// <summary>
        /// Gets all
        /// </summary>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        IPagedList<ProductSizingInfoTemplate> GetAllProductSizingInfoTemplate(int pageIndex = 0, int pageSize = int.MaxValue);

        /// <summary>
        /// Gets 
        /// </summary>
        ProductSizingInfoTemplate GetProductSizingInfoTemplateById(int id);

        /// <summary>
        /// Inserts 
        /// </summary>
         void InsertProductSizingInfoTemplate(ProductSizingInfoTemplate model);

        /// <summary>
        /// Updates 
        /// </summary>
        void UpdateProductSizingInfoTemplate(ProductSizingInfoTemplate model);

        #endregion

        #region Product Shipping Info Template

        /// <summary>
        /// Deletes
        /// </summary>
       void DeleteProductShippingInfoTemplate(ProductShippingInfoTemplate model);

        /// <summary>
        /// Gets all
        /// </summary>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        IPagedList<ProductShippingInfoTemplate> GetAllProductShippingInfoTemplate(int pageIndex = 0, int pageSize = int.MaxValue);

        /// <summary>
        /// Gets 
        /// </summary>
        ProductShippingInfoTemplate GetProductShippingInfoTemplateById(int id);

        /// <summary>
        /// Inserts 
        /// </summary>
        void InsertProductShippingInfoTemplate(ProductShippingInfoTemplate model);

        /// <summary>
        /// Updates 
        /// </summary>
        void UpdateProductShippingInfoTemplate(ProductShippingInfoTemplate model);

        #endregion

        #region Product Shipping Info Template

        /// <summary>
        /// Deletes
        /// </summary>
        void DeleteProductInfoTemplateTemplate(ProductInfoTemplate model);

        /// <summary>
        /// Gets all
        /// </summary>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        IPagedList<ProductInfoTemplate> GetAllProductInfoTemplate(int pageIndex = 0, int pageSize = int.MaxValue);
        
        /// <summary>
        /// Gets 
        /// </summary>
        ProductInfoTemplate GetProductInfoTemplateById(int id);

        /// <summary>
        /// Inserts 
        /// </summary>
        void InsertProductInfoTemplate(ProductInfoTemplate model);

        /// <summary>
        /// Updates 
        /// </summary>
        void UpdateProductInfoTemplate(ProductInfoTemplate model);

        #endregion

        #region type for product

        IList<ProductContentForAttributeValue> GetProductContentForAttributeValues(int productId, int selectedId = 0);
        #endregion

        ShippingByCountry GetShippingByCountryByProductTypeName(string productTypeName);
    }
}
