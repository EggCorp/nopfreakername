﻿using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Customs;
using Nop.Core.Domain.Designs;
using Nop.Services.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Catalog
{
    public partial class ProductDesignService: IProductDesignService
    {

        #region Fields

        private readonly IRepository<CustomField> _productCustomFieldRepository;
        private readonly IRepository<ProductCustomFieldMapping> _productCustomFieldMappingRepository;
        private readonly IRepository<ProductCustomFieldValue> _productCustomFieldValueRepository;
        private readonly IEventPublisher _eventPublisher;
        private readonly ICacheManager _cacheManager;
        private readonly IProductService _productService;
        private readonly IRepository<ProductCustomFieldTemplatePicture> _productCustomFieldTemplatePicture;
        private readonly IRepository<Design> _designRepository;
        private readonly IRepository<DesignCustomField> _designCustomFieldRepository;
        private readonly IRepository<DesignProductCustomFieldMapping> _designProductCustomFieldMappingRepository;
        #endregion

        #region Ctor

        public ProductDesignService(ICacheManager cacheManager,
            IRepository<CustomField> productCustomFieldRepository,
            IRepository<ProductCustomFieldMapping> productCustomFieldMappingRepository,
            IRepository<ProductCustomFieldValue> productCustomFieldValueRepository,
            IEventPublisher eventPublisher,
            IProductService productService,
            IRepository<ProductCustomFieldTemplatePicture> productCustomFieldTemplatePicture,
            IRepository<Design> designRepository,
            IRepository<DesignCustomField> designCustomFieldRepository,
            IRepository<DesignProductCustomFieldMapping> designProductCustomFieldMappingRepository
            )
        {
            this._cacheManager = cacheManager;
            this._productCustomFieldRepository = productCustomFieldRepository;
            this._productCustomFieldMappingRepository = productCustomFieldMappingRepository;
            this._productCustomFieldValueRepository = productCustomFieldValueRepository;
            this._eventPublisher = eventPublisher;
            this._productService = productService;
            this._productCustomFieldTemplatePicture = productCustomFieldTemplatePicture;
            this._designRepository = designRepository;
            this._designCustomFieldRepository = designCustomFieldRepository;
            this._designProductCustomFieldMappingRepository = designProductCustomFieldMappingRepository;
        }

        #endregion

        #region Method

        #region Design
        /// <summary>
        /// Deletes a search term record
        /// </summary>
        /// <param name="searchTerm">Search term</param>
        public virtual void DeleteDesign(Design design)
        {
            if (design == null)
                throw new ArgumentNullException("searchTerm");

            _designRepository.Delete(design);

            //event notification
            _eventPublisher.EntityDeleted(design);
        }

        /// <summary>
        /// Gets a search term record by identifier
        /// </summary>
        /// <param name="searchTermId">Search term identifier</param>
        /// <returns>Search term</returns>
        public virtual Design GetDesignById(int designId)
        {
            if (designId == 0)
                return null;

            return _designRepository.GetById(designId);
        }

        /// <summary>
        /// Gets a search term statistics
        /// </summary>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>A list search term report lines</returns>
        public virtual IPagedList<Design> GetDesigns(int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = _designRepository.Table;
            query = query.OrderByDescending(r => r.Id);
            var result = new PagedList<Design>(query, pageIndex, pageSize);
            return result;
        }

        /// <summary>
        /// Inserts a search term record
        /// </summary>
        /// <param name="searchTerm">Search term</param>
        public virtual void InsertDesign(Design design)
        {
            if (design == null)
                throw new ArgumentNullException("searchTerm");
            _designRepository.Insert(design);

            //event notification
            _eventPublisher.EntityInserted(design);
        }

        /// <summary>
        /// Updates the search term record
        /// </summary>
        /// <param name="searchTerm">Search term</param>
        public virtual void UpdateDesign(Design design)
        {
            if (design == null)
                throw new ArgumentNullException("design");

            _designRepository.Update(design);

            //event notification
            _eventPublisher.EntityUpdated(design);
        }

        public virtual IList<Design> SearchDesign(string name="")
        {
            try
            {
                var query = _designRepository.Table;
                query = query.OrderByDescending(p => p.Id);
                if (!string.IsNullOrEmpty(name))
                {
                    query = query.Where(p => p.Name.Contains(name));
                    query = query.OrderBy(p => p.Name.Equals(name));
                }
                return query.ToList();
            }
            catch(Exception ex)
            { }
            return null;
        }


        #endregion


        #region Design DesignCustomField
        /// <summary>
        /// Deletes a product customField
        /// </summary>
        /// <param name="designCustomField">Product attribute</param>
        public virtual void DeleteDesignCustomField(DesignCustomField designCustomField)
        {
            if (designCustomField == null)
                throw new ArgumentNullException("designCustomField");

            _designCustomFieldRepository.Delete(designCustomField);
        }

        /// <summary>
        /// Gets all product attributes
        /// </summary>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Product customFields</returns>
        public virtual IPagedList<DesignCustomField> GetAllDesignCustomFields(int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = from pa in _designCustomFieldRepository.Table
                        select pa;
            var productCustomFields = new PagedList<DesignCustomField>(query, pageIndex, pageSize);
            return productCustomFields;
        }

        /// <summary>
        /// Gets a product customField 
        /// </summary>
        /// <param name="productCustomFieldId">Product attribute identifier</param>
        /// <returns>Product attribute </returns>
        public virtual DesignCustomField GetDesignCustomFieldById(int id)
        {
            return _designCustomFieldRepository.GetById(id);
        }

        /// <summary>
        /// Inserts a product customField
        /// </summary>
        /// <param name="designCustomField">Product attribute</param>
        public virtual void InsertDesignCustomField(DesignCustomField designCustomField)
        {
            if (designCustomField == null)
                throw new ArgumentNullException("designCustomField");

            _designCustomFieldRepository.Insert(designCustomField);
        }

        /// <summary>
        /// Updates the product customField
        /// </summary>
        /// <param name="designCustomField">Product attribute</param>
        public virtual void UpdateDesignCustomField(DesignCustomField designCustomField)
        {
            if (designCustomField == null)
                throw new ArgumentNullException("designCustomField");

            _designCustomFieldRepository.Update(designCustomField);
        }
        #endregion

        #region Design ProdcuctCustomField Mapping
        /// <summary>
        /// Deletes a product customField mapping
        /// </summary>
        /// <param name="designProductCustomFieldMapping">Product attribute mapping</param>
        public virtual void DeleteDesignProductCustomFieldMapping(DesignProductCustomFieldMapping designProductCustomFieldMapping)
        {
            if (designProductCustomFieldMapping == null)
                throw new ArgumentNullException("designProductCustomFieldMapping");

            _designProductCustomFieldMappingRepository.Delete(designProductCustomFieldMapping);
        }


        /// <summary>
        /// Gets a product customField mapping
        /// </summary>
        /// <param name="designProductCustomFieldMappingId">Product attribute mapping identifier</param>
        /// <returns>Product attribute mapping</returns>
        public virtual DesignProductCustomFieldMapping GetDesignProductCustomFieldMappingById(int designProductCustomFieldMappingId)
        {
            return _designProductCustomFieldMappingRepository.GetById(designProductCustomFieldMappingId);
        }

        /// <summary>
        /// Inserts a product customField mapping
        /// </summary>
        /// <param name="designProductCustomFieldMapping">The product customField mapping</param>
        public virtual void InsertDesignProductCustomFieldMapping(DesignProductCustomFieldMapping designProductCustomFieldMapping)
        {
            if (designProductCustomFieldMapping == null)
                throw new ArgumentNullException("designProductCustomFieldMapping");

            _designProductCustomFieldMappingRepository.Insert(designProductCustomFieldMapping);
        }

        /// <summary>
        /// Updates the product customField mapping
        /// </summary>
        /// <param name="designProductCustomFieldMapping">The product customField mapping</param>
        public virtual void UpdateDesignProductCustomFieldMapping(DesignProductCustomFieldMapping designProductCustomFieldMapping)
        {
            if (designProductCustomFieldMapping == null)
                throw new ArgumentNullException("designProductCustomFieldMapping");

            _designProductCustomFieldMappingRepository.Update(designProductCustomFieldMapping);
        }
        #endregion

        public virtual void SyncProductDesignCustomField(int productId, int designId)
        {
            var product = _productService.GetProductById(productId);
            if (product == null)
                throw new ArgumentNullException("No product found");
            var design = this.GetDesignById(designId);
            if (design == null)
                throw new ArgumentNullException("No design found");
            //on sync
            var prdcustomfieldMaps = _productCustomFieldMappingRepository.Table.Where(p=>p.ProductId==product.Id).ToList();
            if (!prdcustomfieldMaps.Any())
                throw new ArgumentNullException("No product customfield found");
            var dscustomfields = _designCustomFieldRepository.Table.Where(p => p.DesignId == design.Id).ToList();
            if (!dscustomfields.Any())
                throw new ArgumentNullException("No design customfield found");
            var query = _designProductCustomFieldMappingRepository.Table;
            bool syncsucces = false;
            foreach(var prcusmap in prdcustomfieldMaps)
            { 
                foreach (var dscus in dscustomfields)
                {
                    //check match custom field
                    if (prcusmap.CustomFieldId == dscus.CustomFieldId)
                    {
                        //check product design customfield mapping
                        var designProductCustomFieldMappings = query.Where(prc => prc.ProductCustomFieldMappingId == prcusmap.Id|| prc.DesignCustomFieldId == dscus.Id).ToList();
                        //on sync product design update or create new...
                        if(designProductCustomFieldMappings.Any())
                        {
                            //check is existed mapping item
                            DesignProductCustomFieldMapping pdcMap = designProductCustomFieldMappings.FirstOrDefault(prc => prc.ProductCustomFieldMappingId == prcusmap.Id && prc.DesignCustomFieldId == dscus.Id);
                            if (pdcMap != null)
                            {
                                //update mapping item if that is existed
                                pdcMap.DesignCustomFieldId = dscus.Id;
                                pdcMap.ProductCustomFieldMappingId = prcusmap.Id;
                                _designProductCustomFieldMappingRepository.Update(pdcMap);
                            }
                            else
                            {
                                //check if product has any custom field mapping. for update desgn or insert new
                                var currentPrdCusMap = designProductCustomFieldMappings.FirstOrDefault(p => p.ProductCustomFieldMappingId == prcusmap.Id);
                                if (currentPrdCusMap != null)
                                {
                                    //update if is existed
                                    currentPrdCusMap.DesignCustomFieldId = dscus.Id;
                                    _designProductCustomFieldMappingRepository.Update(currentPrdCusMap);
                                }
                                else
                                {
                                    //create new
                                    DesignProductCustomFieldMapping newpdcMap = new DesignProductCustomFieldMapping();
                                    newpdcMap.DesignCustomFieldId = dscus.Id;
                                    newpdcMap.ProductCustomFieldMappingId = prcusmap.Id;
                                    _designProductCustomFieldMappingRepository.Insert(newpdcMap);
                                }
                            }
                        }
                        else
                        {
                            //create new
                            DesignProductCustomFieldMapping newpdcMap = new DesignProductCustomFieldMapping();
                            newpdcMap.DesignCustomFieldId = dscus.Id;
                            newpdcMap.ProductCustomFieldMappingId = prcusmap.Id;
                            _designProductCustomFieldMappingRepository.Insert(newpdcMap);
                        }

                        //
                        syncsucces = true;
                    }
                   
                }
            }

            //update product design if sync success
            if(syncsucces)
            {
                //remove product from another design.
                var oldDesignWithinProduct = _designRepository.Table.Where(p => p.ProductId == product.Id).ToList();
                foreach(var oldDesign in oldDesignWithinProduct)
                {
                    oldDesign.ProductId = null;
                    _designRepository.Update(design);
                }

                //set new product for design
                design.ProductId = product.Id;
                _designRepository.Update(design);
            }
        }

        public virtual IList<Design> SearchAvailableDesignsForSyncProduct(string name = "")
        {
            try
            {
                var query = _designRepository.Table;
                if (!string.IsNullOrEmpty(name))
                {
                    query = query.Where(p => p.Name.Contains(name));
                    query = query.OrderBy(p => p.Name.Equals(name));
                }
                var result = query.ToList();
                result = result.Where(p=>!p.ProductId.HasValue).OrderByDescending(p => p.Id).ToList();
                return result;
            }
            catch (Exception ex)
            { }
            return null;
        }

        public virtual Design GetProductDesign(int productId)
        {
            if (productId == 0)
                return null;

            return _designRepository.Table.FirstOrDefault(p=>p.ProductId==productId);
        }
        #endregion
    }
}
