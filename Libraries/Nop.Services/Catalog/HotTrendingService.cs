﻿using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Services.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Catalog
{
    public partial class HotTrendingService: IHotTrendingService
    {
        #region Fields

        private readonly IRepository<HotTrending> _hotTrendingRepository;
        private readonly IEventPublisher _eventPublisher;

        #endregion

        #region Ctor

        public HotTrendingService(IRepository<HotTrending> hotTrendingRepository,
            IEventPublisher eventPublisher)
        {
            this._hotTrendingRepository = hotTrendingRepository;
            this._eventPublisher = eventPublisher;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Deletes a search term record
        /// </summary>
        /// <param name="searchTerm">Search term</param>
        public virtual void DeleteHotTrending(HotTrending hotTrending)
        {
            if (hotTrending == null)
                throw new ArgumentNullException("searchTerm");

            _hotTrendingRepository.Delete(hotTrending);

            //event notification
            _eventPublisher.EntityDeleted(hotTrending);
        }

        /// <summary>
        /// Gets a search term record by identifier
        /// </summary>
        /// <param name="searchTermId">Search term identifier</param>
        /// <returns>Search term</returns>
        public virtual HotTrending GetHotTrendingById(int hotTrendingId)
        {
            if (hotTrendingId == 0)
                return null;

            return _hotTrendingRepository.GetById(hotTrendingId);
        }

        /// <summary>
        /// Gets a search term statistics
        /// </summary>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>A list search term report lines</returns>
        public virtual IPagedList<HotTrending> GetHotTrendings(int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = _hotTrendingRepository.Table;
            query = query.OrderBy(r => r.DisplayOrder);
            var result = new PagedList<HotTrending>(query, pageIndex, pageSize);
            return result;
        }

        /// <summary>
        /// Inserts a search term record
        /// </summary>
        /// <param name="searchTerm">Search term</param>
        public virtual void InsertHotTrending(HotTrending hotTrending)
        {
            if (hotTrending == null)
                throw new ArgumentNullException("searchTerm");
            _hotTrendingRepository.Insert(hotTrending);

            //event notification
            _eventPublisher.EntityInserted(hotTrending);
        }

        /// <summary>
        /// Updates the search term record
        /// </summary>
        /// <param name="searchTerm">Search term</param>
        public virtual void UpdateHotTrending(HotTrending hotTrending)
        {
            if (hotTrending == null)
                throw new ArgumentNullException("hotTrending");

            _hotTrendingRepository.Update(hotTrending);

            //event notification
            _eventPublisher.EntityUpdated(hotTrending);
        }

        public virtual IList<HotTrending> GetHotTrendingForHomePage()
        {
            return _hotTrendingRepository.Table.OrderBy(p => p.DisplayOrder).ToList();
        }
        #endregion
    }
}
