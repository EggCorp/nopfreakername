﻿using Nop.Core;
using Nop.Core.Domain.Designs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Catalog
{
    public partial interface IProductDesignService
    {
        #region Design
        void DeleteDesign(Design design);
        Design GetDesignById(int id);
        IPagedList<Design> GetDesigns(int pageIndex = 0, int pageSize = int.MaxValue);
        void InsertDesign(Design design);
        void UpdateDesign(Design design);
        IList<Design> SearchDesign(string name="");

        #endregion

        #region Design DesignCustomField
        /// <summary>
        /// Deletes a product customField
        /// </summary>
        /// <param name="designCustomField">Product attribute</param>
        void DeleteDesignCustomField(DesignCustomField designCustomField);

        /// <summary>
        /// Gets all product attributes
        /// </summary>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Product customFields</returns>
        IPagedList<DesignCustomField> GetAllDesignCustomFields(int pageIndex = 0, int pageSize = int.MaxValue);

        /// <summary>
        /// Gets a product customField 
        /// </summary>
        /// <param name="productCustomFieldId">Product attribute identifier</param>
        /// <returns>Product attribute </returns>
        DesignCustomField GetDesignCustomFieldById(int productCustomFieldId);

        /// <summary>
        /// Inserts a product customField
        /// </summary>
        /// <param name="designCustomField">Product attribute</param>
        void InsertDesignCustomField(DesignCustomField designCustomField);

        /// <summary>
        /// Updates the product customField
        /// </summary>
        /// <param name="designCustomField">Product attribute</param>
        void UpdateDesignCustomField(DesignCustomField designCustomField);
        #endregion

        #region Design ProdcuctCustomField Mapping
        /// <summary>
        /// Deletes a product customField mapping
        /// </summary>
        /// <param name="designProductCustomFieldMapping">Product attribute mapping</param>
        void DeleteDesignProductCustomFieldMapping(DesignProductCustomFieldMapping designProductCustomFieldMapping);

        /// <summary>
        /// Gets a product customField mapping
        /// </summary>
        /// <param name="designProductCustomFieldMappingId">Product attribute mapping identifier</param>
        /// <returns>Product attribute mapping</returns>
        DesignProductCustomFieldMapping GetDesignProductCustomFieldMappingById(int designProductCustomFieldMappingId);

        /// <summary>
        /// Inserts a product customField mapping
        /// </summary>
        /// <param name="designProductCustomFieldMapping">The product customField mapping</param>
        void InsertDesignProductCustomFieldMapping(DesignProductCustomFieldMapping designProductCustomFieldMapping);

        /// <summary>
        /// Updates the product customField mapping
        /// </summary>
        /// <param name="designProductCustomFieldMapping">The product customField mapping</param>
        void UpdateDesignProductCustomFieldMapping(DesignProductCustomFieldMapping designProductCustomFieldMapping);

        IList<Design> SearchAvailableDesignsForSyncProduct(string name = "");
        void SyncProductDesignCustomField(int productId, int designId);

        Design GetProductDesign(int productId);
        #endregion
    }
}
