﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
using Nop.Core.Domain.Common;
using System.Web.Script.Serialization;

namespace Nop.Services.Helpers
{
    public class UrlShortenerHelper: IUrlShortenerHelper
    {
        private const string key = "AIzaSyAykAkiRybQj-z7vLIHv0bq4t_iO6y7lw4";
        public UrlShortenerHelper()
        {

        }

        public virtual string UrlShorter(string longUrl)
        {
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://www.googleapis.com/urlshortener/v1/url?key=" + key);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = "{\"longUrl\":\""+ longUrl+ "\"}";
                    streamWriter.Write(json);
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var responseText = streamReader.ReadToEnd();
                    var result = new JavaScriptSerializer().Deserialize<UrlShortenerResult>(responseText);
                    return result.id;
                }
            }
            catch(Exception ex)
            {
                return longUrl;
            }
        }
    }
}
