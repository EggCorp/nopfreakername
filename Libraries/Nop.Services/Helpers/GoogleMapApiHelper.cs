﻿using Newtonsoft.Json;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Nop.Services.Logging;

namespace Nop.Services.Helpers
{
    public class GoogleMapApiHelper: IGoogleMapApiHelper
    {
        public const string GMapApiKey = "AIzaSyDSON9uu203YuqTGWSazIYb3bZCUsTo5DY";
        private readonly ILogger _logger;
        public GoogleMapApiHelper(ILogger logger)
        {
            this._logger = logger;
        }

        public virtual GoogleGeoCodeAddress ValidateAddress(GoogleGeoCodeAddress geoAddress)
        {
            //Create the url string to send our request to googs.
            string url = string.Format("https://maps.googleapis.com/maps/api/geocode/json?address={0} +{1} +{2} +{3} +{4}&sensor=false&language=en&key={5}", geoAddress.Line1, geoAddress.City + ", ", geoAddress.State, geoAddress.Zip, geoAddress.Country, GMapApiKey);
            try
            {
                //Create a web request client.
                WebClient wc = new WebClient();
                //retrieve our result and put it in a streamreader
                var jsonResponse = wc.DownloadString(url);
                GoogleGeoCodeResponse geoCodeResponse = JsonConvert.DeserializeObject<GoogleGeoCodeResponse>(jsonResponse);
                if (geoCodeResponse != null)
                {
                    if (geoCodeResponse.status.Equals("OK"))
                    {
                        if(geoCodeResponse.results.Any())
                        {
                            var addressResponse = geoCodeResponse.results.FirstOrDefault();
                            geoAddress.Latitude = addressResponse.geometry.location.lat;
                            geoAddress.Longitude = addressResponse.geometry.location.lng;
                            geoAddress.IsValid = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                geoAddress.IsValid = false;
                _logger.Debug("An error occured while retrieving GeoCoded results from Google, the error was: " + ex.Message, ex);
            }
            return geoAddress;
        }
    }
}
