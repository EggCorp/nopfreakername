﻿using Nop.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Helpers
{
    public partial interface IGoogleMapApiHelper
    {
        GoogleGeoCodeAddress ValidateAddress(GoogleGeoCodeAddress geoAddress);
    }
}
