﻿using Nop.Core;
using Nop.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Common
{
    public partial interface IReviewService
    {
        void DeleteReview(Review review);
        Review GetReviewById(int reviewId);
        IPagedList<Review> GetReviews(int pageIndex = 0, int pageSize = int.MaxValue);
        void InsertReview(Review review);
        void UpdateReview(Review review);
        void RefreshReviewData();
    }
}
