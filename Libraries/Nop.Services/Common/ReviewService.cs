﻿using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Common;
using Nop.Services.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Common
{
     public partial class ReviewService: IReviewService
    {
        #region Fields

        private readonly IRepository<Review> _reviewRepository;
        private readonly IEventPublisher _eventPublisher;

        #endregion

        #region Ctor

        public ReviewService(IRepository<Review> reviewRepository,
            IEventPublisher eventPublisher)
        {
            this._reviewRepository = reviewRepository;
            this._eventPublisher = eventPublisher;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Deletes a search term record
        /// </summary>
        /// <param name="searchTerm">Search term</param>
        public virtual void DeleteReview(Review review)
        {
            if (review == null)
                throw new ArgumentNullException("searchTerm");

            _reviewRepository.Delete(review);

            //event notification
            _eventPublisher.EntityDeleted(review);
        }

        /// <summary>
        /// Gets a search term record by identifier
        /// </summary>
        /// <param name="searchTermId">Search term identifier</param>
        /// <returns>Search term</returns>
        public virtual Review GetReviewById(int reviewId)
        {
            if (reviewId == 0)
                return null;

            return _reviewRepository.GetById(reviewId);
        }

        /// <summary>
        /// Gets a search term statistics
        /// </summary>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>A list search term report lines</returns>
        public virtual IPagedList<Review> GetReviews(int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = _reviewRepository.Table;
            query = query.OrderByDescending(r=>r.CreatedOnUtc);
            var result = new PagedList<Review>(query, pageIndex, pageSize);
            return result;
        }

        /// <summary>
        /// Inserts a search term record
        /// </summary>
        /// <param name="searchTerm">Search term</param>
        public virtual void InsertReview(Review review)
        {
            if (review == null)
                throw new ArgumentNullException("searchTerm");
            _reviewRepository.Insert(review);

            //event notification
            _eventPublisher.EntityInserted(review);
        }

        /// <summary>
        /// Updates the search term record
        /// </summary>
        /// <param name="searchTerm">Search term</param>
        public virtual void UpdateReview(Review review)
        {
            if (review == null)
                throw new ArgumentNullException("review");

            _reviewRepository.Update(review);

            //event notification
            _eventPublisher.EntityUpdated(review);
        }

        public virtual void RefreshReviewData()
        {
            try
            {
                var allReviews = _reviewRepository.Table
                    .OrderByDescending(p => p.CreatedOnUtc).Take(100).ToList();
                foreach(var review in allReviews)
                {
                    Random rnd = new Random();
                    int randomDay = rnd.Next(-15,-1);
                    //int randomStar = rnd.Next(4, 5);

                    //update
                    review.CreatedOnUtc = DateTime.UtcNow.AddDays(randomDay);
                    review.Rating  = review.Id%2==0? 4:5;
                    _reviewRepository.Update(review);
                }
                    
            }
            catch(Exception ex)
            { }
        }
        #endregion
    }
}
