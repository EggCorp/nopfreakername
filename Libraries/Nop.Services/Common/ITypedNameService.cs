﻿using Nop.Core;
using Nop.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Common
{
    public partial interface ITypedNameService
    {
        /// <summary>
        /// Deletes a search term record
        /// </summary>
        /// <param name="typedName">Search term</param>
        void Delete(TypedName typedName);

        /// <summary>
        /// Gets a search term record by identifier
        /// </summary>
        /// <param name="searchTermId">Search term identifier</param>
        /// <returns>Search term</returns>
        TypedName GetById(int id);

        /// <summary>
        /// Gets a search term record by keyword
        /// </summary>
        /// <param name="keyword">Search term keyword</param>
        /// <param name="storeId">Store identifier</param>
        /// <returns>Search term</returns>
        TypedName GetByName(string name, int customerId,int productId);

        /// <summary>
        /// Gets a search sterm statistics
        /// </summary>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>A list search term report lines</returns>
        IList<TypedName> GetStats();

        /// <summary>
        /// Inserts a search term record
        /// </summary>
        /// <param name="typedName">Search term</param>
        void Insert(TypedName typedName);

        /// <summary>
        /// Updates the search term record
        /// </summary>
        /// <param name="typedName">Search term</param>
        void Update(TypedName typedName);

        IList<TypedName> Searchs(bool stats = false, bool newest = false, string name = "", int productId = 0);
    }
}
