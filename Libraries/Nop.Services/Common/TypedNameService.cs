﻿using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Common;
using Nop.Services.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Common
{
    public partial class TypedNameService : ITypedNameService
    {
        #region Fields

        private readonly IRepository<TypedName> _typedNameRepository;
        private readonly IEventPublisher _eventPublisher;

        #endregion

        #region Ctor

        public TypedNameService(IRepository<TypedName> typedNameRepository,
            IEventPublisher eventPublisher)
        {
            this._typedNameRepository = typedNameRepository;
            this._eventPublisher = eventPublisher;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Deletes a search term record
        /// </summary>
        /// <param name="typedName">Search term</param>
        public virtual void Delete(TypedName typedName)
        {
            if (typedName == null)
                throw new ArgumentNullException("typedName");

            _typedNameRepository.Delete(typedName);

            //event notification
            _eventPublisher.EntityDeleted(typedName);
        }

        /// <summary>
        /// Gets a search term record by identifier
        /// </summary>
        /// <param name="typedNameId">Search term identifier</param>
        /// <returns>Search term</returns>
        public virtual TypedName GetById(int typedNameId)
        {
            if (typedNameId == 0)
                return null;

            return _typedNameRepository.GetById(typedNameId);
        }

        /// <summary>
        /// Gets a search term record by keyword
        /// </summary>
        /// <param name="keyword">Search term keyword</param>
        /// <param name="storeId">Store identifier</param>
        /// <returns>Search term</returns>
        public virtual TypedName GetByName(string name, int customerId,int productId)
        {
            if (String.IsNullOrEmpty(name))
                return null;

            var query = from st in _typedNameRepository.Table
                        where st.Name == name && st.CustomerId == customerId&&st.ProductId==productId
                        orderby st.Id
                        select st;
            var typedName = query.FirstOrDefault();
            return typedName;
        }

        /// <summary>
        /// Gets a search term statistics
        /// </summary>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>A list search term report lines</returns>
        public virtual IList<TypedName> GetStats()
        {
            try
            {
               
                var query = _typedNameRepository.Table
                         .OrderByDescending(m => m.Count).ThenByDescending(p=>p.CreatedOnTime).ToList(); ;
                return query.ToList();
            }
            catch(Exception ex)
            { }
            return null;
        }

        /// <summary>
        /// Inserts a search term record
        /// </summary>
        /// <param name="typedName">Search term</param>
        public virtual void Insert(TypedName typedName)
        {
            if (typedName == null)
                throw new ArgumentNullException("typedName");

            _typedNameRepository.Insert(typedName);

            //event notification
            _eventPublisher.EntityInserted(typedName);
        }

        /// <summary>
        /// Updates the search term record
        /// </summary>
        /// <param name="typedName">Search term</param>
        public virtual void Update(TypedName typedName)
        {
            if (typedName == null)
                throw new ArgumentNullException("typedName");

            _typedNameRepository.Update(typedName);

            //event notification
            _eventPublisher.EntityUpdated(typedName);
        }
        public virtual IList<TypedName> Searchs(bool stats = false, bool newest = false, string name = "", int productId = 0)
        {
            try
            {
                var query = _typedNameRepository.Table;
                if (newest)
                    query = query.OrderByDescending(p => p.CreatedOnTime);
                if (stats)
                    query = query.OrderByDescending(p => p.Count);
                if (!string.IsNullOrEmpty(name))
                    query = query.Where(p => p.Name.Contains(name));
                if (productId > 0)
                    query = query.Where(p => p.ProductId == productId);
                return query.ToList();
            }
            catch(Exception ex)
            { }
            return null;
        }
        #endregion
    }
}
