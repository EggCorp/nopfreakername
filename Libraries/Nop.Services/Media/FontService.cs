﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Media;
using Nop.Data;
using Nop.Services.Configuration;
using Nop.Services.Events;
using Nop.Services.Logging;
using Nop.Services.Seo;

namespace Nop.Services.Media
{
    public partial class FontService: IFontService
    {
        private readonly ISettingService _settingService;
        private readonly IWebHelper _webHelper;
        private readonly ILogger _logger;
        private readonly IDbContext _dbContext;
        private readonly IEventPublisher _eventPublisher;
        private readonly MediaSettings _mediaSettings;
        private readonly IDataProvider _dataProvider;
        public FontService(
           ISettingService settingService,
           IWebHelper webHelper,
           ILogger logger,
           IDbContext dbContext,
           IEventPublisher eventPublisher,
           MediaSettings mediaSettings,
           IDataProvider dataProvider)
        {
            this._settingService = settingService;
            this._webHelper = webHelper;
            this._logger = logger;
            this._dbContext = dbContext;
            this._eventPublisher = eventPublisher;
            this._mediaSettings = mediaSettings;
            this._dataProvider = dataProvider;
        }

        protected virtual string GetFontLocalPath(string fileName)
        {
            return Path.Combine(CommonHelper.MapPath("~/content/fonts/"), fileName);
        }
        protected virtual bool GeneratedFontExists(string fontFilePath, string fontbFileName)
        {
            return File.Exists(fontFilePath);
        }
        protected virtual void SaveFont(string fontFilePath, string fontFileName, byte[] binary)
        {
            File.WriteAllBytes(fontFilePath, binary);
        }

        public virtual FontUploadResponse UploadFont(byte[] fontBinary, string fontName, string mimeType)
        {
            FontUploadResponse result = new FontUploadResponse();
            try
            {
                string fontPath = this.GetFontLocalPath(fontName);
                if (!this.GeneratedFontExists(fontPath, fontName))
                {
                    this.SaveFont(fontPath, fontName, fontBinary);
                    result.Message = "Upload new font was successfully!";
                }
                else
                    result.Message = "This font already exist in fonts folder!";
                result.Success = true;
                result.FontName = fontName;
            }
            catch(Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
                _logger.Error(string.Format("Error upload font: {0}", ex.Message), ex);
            }
            return result;
        }
    }
}
