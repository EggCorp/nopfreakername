﻿using Aurigma.GraphicsMill;
using Aurigma.GraphicsMill.AdvancedDrawing;
using Aurigma.GraphicsMill.AdvancedDrawing.Effects;
using Nop.Core.Domain.Media;

namespace Nop.Services.Media
{
    public class DrawingArtTypeSetting
    {
        public ArtType ArtType { get; set; }

        public string Text { get; set; }

        public Font Font { get; set; }

        public Color Color { get; set; }

        public Effect Effect { get; set; }

        public float Bend { get; set; }

        public TextAlignment Alignment { get; set; }

        public string EffectColor { get; set; }

        public float EffectSize { get; set; }

        public float LeftScale { get; set; }

        public float RightScale { get; set; }

        public float Tilt { get; set; }

        public bool FullWidthRequire { get; set; }

        public bool FullBoxSizeRequire { get; set; }

    }
}
