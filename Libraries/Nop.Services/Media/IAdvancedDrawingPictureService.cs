﻿using Aurigma.GraphicsMill.AdvancedDrawing;
using Nop.Core.Domain.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Media
{
    public partial interface IAdvancedDrawingPictureService
    {
        byte[] DrawingCustomProductPicture(RenderPictureRequest request);
    }
}
