﻿using Nop.Core;
using Nop.Core.Domain.Media;
using Nop.Services.Catalog;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Media
{
    public partial class RenderPictureService: IRenderPictureService
    {
        private readonly IProductAttributeService _productAttributeService;
        private readonly IProductCustomFieldService _productCustomFieldService;
        private readonly IPictureService _pictureService;
        private readonly IProductAttributeParser _productAttributeParser;
        private readonly IAdvancedDrawingPictureService _advancedDrawing;
        public RenderPictureService(IProductAttributeService productAttributeService, 
            IProductCustomFieldService productCustomFieldService,
            IPictureService pictureService,
            IProductAttributeParser productAttributeParser,
            IAdvancedDrawingPictureService advancedDrawing)
        {
            this._productAttributeService = productAttributeService;
            this._productCustomFieldService = productCustomFieldService;
            this._pictureService = pictureService;
            this._productAttributeParser = productAttributeParser;
            this._advancedDrawing = advancedDrawing;
        }
        public virtual byte[] RendingCustomProductPicture(RenderPictureRequest request)
        {
            try
            {
                var productAttributeValue = _productAttributeService.GetProductAttributeValueById(request.AttrValueId);
                if (productAttributeValue != null)
                {
                    //image path
                    string picturePath = string.Empty;

                    if (productAttributeValue.PictureId>0)
                    {
                        if (productAttributeValue.ProductCustomFieldTemplatePictures.Any())
                        {
                            var productCustomFieldTemplatePicture = productAttributeValue.ProductCustomFieldTemplatePictures.FirstOrDefault();
                            var picture = _pictureService.GetPictureById(productCustomFieldTemplatePicture.PictureId);
                            if (picture != null)
                                picturePath = _pictureService.GetThumbLocalPath(picture);
                        }
                        else
                        {
                            var pictures = _pictureService.GetPicturesByProductId(productAttributeValue.ProductAttributeMapping.ProductId);
                            var selectedPicture = pictures.FirstOrDefault();
                            string pictureUrl = _pictureService.GetPictureUrl(selectedPicture.Id, 1024);
                            var fileName = string.Format("{0}_{1}.{2}", productAttributeValue.ProductAttributeMapping.ProductId, selectedPicture.Id, this.GetFileExtensionFromMimeType(selectedPicture.MimeType));
                            picturePath = this.DownloadPicture(pictureUrl, fileName);
                        }
                    }
                    else
                    {
                        // select ispreselect product
                        var productAttributeMapping = productAttributeValue.ProductAttributeMapping;
                        var attributes = _productAttributeService.GetProductAttributeMappingsByProductId(productAttributeMapping.ProductId);
                        var colorAttrs = attributes.Where(p => p.AttributeControlTypeId == 40);
                        foreach (var attr in colorAttrs)
                        {
                            var valuesThatShouldBeSelected = _productAttributeParser.ParseConditionValue(attr.ConditionAttributeXml, productAttributeMapping.Id, productAttributeValue.Id).ToList();
                            if (valuesThatShouldBeSelected.Any())
                            {
                                var preselectedAttrValue = attr.ProductAttributeValues.Where(p => p.IsPreSelected == true).FirstOrDefault();

                                var productCustomFieldTemplatePicture = preselectedAttrValue.ProductCustomFieldTemplatePictures.FirstOrDefault();
                                var picture = _pictureService.GetPictureById(productCustomFieldTemplatePicture.PictureId);
                                if (picture != null)
                                    picturePath = _pictureService.GetThumbLocalPath(picture);
                            }
                        }
                    }
                   
                    if (!string.IsNullOrEmpty(picturePath))
                    {
                        using (Bitmap bitmap = (Bitmap)Image.FromFile(picturePath, false))    //create a bitmap for selected 
                        {
                            using (Graphics graphics = Graphics.FromImage(bitmap))
                            {
                                foreach (var req in request.Data)
                                {
                                    var productCustomFieldSelectingValue = _productCustomFieldService.GetProductCustomFieldValueById(req.ValueId);
                                    if (productCustomFieldSelectingValue != null)
                                    {
                                        var productCustomFieldSelecting = productCustomFieldSelectingValue.ProductCustomFieldMapping;
                                        var designCustomFieldMapping = productCustomFieldSelecting.DesignProductCustomFieldMappings.FirstOrDefault();
                                        var selectedDesignCustomField = designCustomFieldMapping.DesignCustomField;

                                        var fontFamily = this.GetFontByImportedFileName(selectedDesignCustomField.Font);
                                        using (Font drawFont = new Font(fontFamily, (int)selectedDesignCustomField.FontSize, GraphicsUnit.Pixel))
                                        {
                                            int _width = (int)selectedDesignCustomField.Width;
                                            int _height = (int)selectedDesignCustomField.Height;
                                            int _xPos = productCustomFieldSelectingValue.PositionX;
                                            int _yPos = productCustomFieldSelectingValue.PositionY;

                                            Color _color = System.Drawing.ColorTranslator.FromHtml(selectedDesignCustomField.FontColor);
                                            graphics.DrawImage(this.DrawText(req.Text, drawFont, _color, _width, _height), _xPos, _yPos);
                                        }
                                    }
                                }
                            }
                            Bitmap resized = new Bitmap(bitmap, new Size(request.Scale, request.Scale));
                            resized.SetResolution(72, 72);
                            System.IO.MemoryStream stream = new System.IO.MemoryStream();
                            resized.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
                            byte[] imageBytes = stream.ToArray();
                            return imageBytes;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }
        protected virtual Image DrawText(String text, Font font, Color textColor, int width, int height)
        {
            //first, create a dummy bitmap just to get a graphics object
            Image img = new Bitmap(1, 1);
            Graphics drawing = Graphics.FromImage(img);
            drawing.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
            //Set the alignment based on the coordinates      
            StringFormat stringformat = new StringFormat(StringFormat.GenericTypographic);
            stringformat.LineAlignment = StringAlignment.Near;
            stringformat.Alignment = StringAlignment.Near;
            stringformat.FormatFlags = StringFormatFlags.MeasureTrailingSpaces;
            //measure the string to see how big the image needs to be
            SizeF textSize = drawing.MeasureString(text, font,int.MaxValue, stringformat);

            //free up the dummy image and old graphics object
            img.Dispose();
            drawing.Dispose();

            //create a new image of the right size
            img = new Bitmap((int)textSize.Width, (int)textSize.Height);
            drawing = Graphics.FromImage(img);
            //create a brush for the text
            Brush textBrush = new SolidBrush(textColor);
            // Draw string to screen.
            Rectangle rectangle = new Rectangle(0, 0, (int)textSize.Width, (int)textSize.Height);
            drawing.DrawRectangle(new Pen(Color.Red, 5),rectangle);
            //Font bestFont = FindFont(drawing, text, rectangle.Size,font);
            drawing.DrawString(text, font, textBrush,0,0,stringformat);

            var center = new Point((int)textSize.Width / 2, (int)textSize.Height / 2);

            //this.DrawCurvedText(drawing, text, center, 100, 100, curvedfont, new SolidBrush(Color.Red));
            //this.SaveImage(img);
            drawing.Save();
            textBrush.Dispose();
            drawing.Dispose();
            
            Bitmap resizedImg = new Bitmap(img, new Size(width, height));
            return resizedImg;
        }

        protected virtual Font FindFont(System.Drawing.Graphics g, string longString, Size Room, Font PreferedFont)
        {
            //you should perform some scale functions!!!
            SizeF RealSize = g.MeasureString(longString, PreferedFont);
            float HeightScaleRatio = Room.Height / RealSize.Height;
            float WidthScaleRatio = Room.Width / RealSize.Width;
            float ScaleRatio = (HeightScaleRatio < WidthScaleRatio) ? ScaleRatio = HeightScaleRatio : ScaleRatio = WidthScaleRatio;
            float ScaleFontSize = PreferedFont.Size * ScaleRatio;
            return new Font(PreferedFont.FontFamily, ScaleFontSize);
        }

        protected virtual string DownloadPicture(string url,string fileName)
        {
           try
            {
                var downloadPath = this.GetPictureLocalPath(fileName);
                using (WebClient client = new WebClient())
                {
                    client.DownloadFile(new Uri(url), downloadPath);
                    return downloadPath;
                }
            }
            catch(Exception ex)
            { }
            return null;
        }

        protected virtual string GetPictureLocalPath(string fileName)
        {
            return Path.Combine(CommonHelper.MapPath("~/content/images/"), fileName);
        }

        protected virtual string GetFileExtensionFromMimeType(string mimeType)
        {
            if (mimeType == null)
                return null;

            //also see System.Web.MimeMapping for more mime types

            string[] parts = mimeType.Split('/');
            string lastPart = parts[parts.Length - 1];
            switch (lastPart)
            {
                case "pjpeg":
                    lastPart = "jpg";
                    break;
                case "x-png":
                    lastPart = "png";
                    break;
                case "x-icon":
                    lastPart = "ico";
                    break;
            }
            return lastPart;
        }
        protected virtual void SaveImage(Image img)
        {
            try
            {
                var name = "image-" + Guid.NewGuid().ToString() + ".jpg";
                img.Save(@"D:\Images\" + name);
            }
            catch (Exception exp)
            {
            }
        }
        protected virtual string GetFontPathFolder(string fontName)
        {
            try
            {
                var fontDirectoryPath = CommonHelper.MapPath("~/content/fonts");
                var fontFilePath = Path.Combine(fontDirectoryPath, fontName);
                return fontFilePath;
            }
            catch(Exception ex)
            {
            }
            return string.Empty;
        }

        protected virtual FontFamily GetFontByImportedFileName(string fileName)
        {
            try
            {
                System.Drawing.Text.PrivateFontCollection privateFonts = new System.Drawing.Text.PrivateFontCollection();
                string fontFilePath = this.GetFontPathFolder(fileName);
                privateFonts.AddFontFile(fontFilePath);
                var font = privateFonts.Families[0];
                return font;
            }
            catch(Exception ex)
            {
                return FontFamily.GenericSerif;
            }
        }

        protected void DrawCurvedText(Graphics graphics, string text, Point centre, float distanceFromCentreToBaseOfText, float radiansToTextCentre, Font font, Brush brush)
        {
            // Circumference for use later
            var circleCircumference = (float)(Math.PI * 2 * distanceFromCentreToBaseOfText);

            // Get the width of each character
            var characterWidths = GetCharacterWidths(graphics, text, font).ToArray();

            // The overall height of the string
            var characterHeight = graphics.MeasureString(text, font).Height;

            var textLength = characterWidths.Sum();

            // The string length above is the arc length we'll use for rendering the string. Work out the starting angle required to 
            // centre the text across the radiansToTextCentre.
            float fractionOfCircumference = textLength / circleCircumference;

            float currentCharacterRadians = radiansToTextCentre + (float)(Math.PI * fractionOfCircumference);

            for (int characterIndex = 0; characterIndex < text.Length; characterIndex++)
            {
                char @char = text[characterIndex];

                // Polar to cartesian
                float x = (float)(distanceFromCentreToBaseOfText * Math.Sin(currentCharacterRadians));
                float y = -(float)(distanceFromCentreToBaseOfText * Math.Cos(currentCharacterRadians));

                using (GraphicsPath characterPath = new GraphicsPath())
                {
                    characterPath.AddString(@char.ToString(), font.FontFamily, (int)font.Style, font.Size, Point.Empty,
                                            StringFormat.GenericTypographic);

                    var pathBounds = characterPath.GetBounds();

                    // Transformation matrix to move the character to the correct location. 
                    // Note that all actions on the Matrix class are prepended, so we apply them in reverse.
                    var transform = new Matrix();

                    // Translate to the final position
                    transform.Translate(centre.X + x, centre.Y + y);

                    // Rotate the character
                    var rotationAngleDegrees = currentCharacterRadians * 180F / (float)Math.PI - 180F;
                    transform.Rotate(rotationAngleDegrees);

                    // Translate the character so the centre of its base is over the origin
                    transform.Translate(-pathBounds.Width / 2F, -characterHeight);

                    characterPath.Transform(transform);

                    // Draw the character
                    graphics.FillPath(brush, characterPath);
                }

                if (characterIndex != text.Length - 1)
                {
                    // Move "currentCharacterRadians" on to the next character
                    var distanceToNextChar = (characterWidths[characterIndex] + characterWidths[characterIndex + 1]) / 2F;
                    float charFractionOfCircumference = distanceToNextChar / circleCircumference;
                    currentCharacterRadians -= charFractionOfCircumference * (float)(2F * Math.PI);
                }

            }
        }

        protected IEnumerable<float> GetCharacterWidths(Graphics graphics, string text, Font font)
        {
            // The length of a space. Necessary because a space measured using StringFormat.GenericTypographic has no width.
            // We can't use StringFormat.GenericDefault for the characters themselves, as it adds unwanted spacing.
            var spaceLength = graphics.MeasureString(" ", font, Point.Empty, StringFormat.GenericDefault).Width;

            return text.Select(c => c == ' ' ? spaceLength : graphics.MeasureString(c.ToString(), font, Point.Empty, StringFormat.GenericTypographic).Width);
        }

    }
}
