﻿using Aurigma.GraphicsMill;
using Aurigma.GraphicsMill.AdvancedDrawing;
using Aurigma.GraphicsMill.AdvancedDrawing.Art;
using Aurigma.GraphicsMill.AdvancedDrawing.Effects;
using Aurigma.GraphicsMill.Transforms;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Media;
using Nop.Services.Catalog;
using Nop.Services.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Services.Logging;

namespace Nop.Services.Media
{
    public partial class AdvancedDrawingPictureService: IAdvancedDrawingPictureService
    {
        private readonly IProductAttributeService _productAttributeService;
        private readonly IProductCustomFieldService _productCustomFieldService;
        private readonly IPictureService _pictureService;
        private readonly IProductAttributeParser _productAttributeParser;
        private readonly IProductDesignService _productDesignService;
        private readonly ITypedNameService _typedNameService;
        private readonly IWorkContext _workContext;
        private readonly ILogger _logger;
        public AdvancedDrawingPictureService(IProductAttributeService productAttributeService,
            IProductCustomFieldService productCustomFieldService,
            IPictureService pictureService,
            IProductAttributeParser productAttributeParser,
            IProductDesignService productDesignService,
            ITypedNameService typedNameService,
             ILogger logger,
             IWorkContext workContext)
        {
            this._productAttributeService = productAttributeService;
            this._productCustomFieldService = productCustomFieldService;
            this._pictureService = pictureService;
            this._productAttributeParser = productAttributeParser;
            this._productDesignService = productDesignService;
            this._typedNameService = typedNameService;
            this._workContext = workContext;
            this._logger = logger;
        }

        const float dpi = 72f;
        const int _bestDrawingStringLength = 6;

        public virtual byte[] DrawingCustomProductPicture(RenderPictureRequest request)
        {
            try
            {
                var productAttributeValue = _productAttributeService.GetProductAttributeValueById(request.AttrValueId);
                if (productAttributeValue != null)
                {
                    //image path
                    string picturePath = string.Empty;

                    if (productAttributeValue.ProductAttributeMapping.AttributeControlType == AttributeControlType.ColorSquares)
                    {
                        if (productAttributeValue.ProductCustomFieldTemplatePictures.Any())
                        {
                            var productCustomFieldTemplatePicture = productAttributeValue.ProductCustomFieldTemplatePictures.FirstOrDefault();
                            var picture = _pictureService.GetPictureById(productCustomFieldTemplatePicture.PictureId);
                            if (picture != null)
                                picturePath = _pictureService.GetThumbLocalPath(picture);
                        }
                    }
                    else
                    {
                        // select ispreselect product
                        var productAttributeMapping = productAttributeValue.ProductAttributeMapping;
                        var attributes = _productAttributeService.GetProductAttributeMappingsByProductId(productAttributeMapping.ProductId);
                        var colorAttrs = attributes.Where(p => p.AttributeControlType == AttributeControlType.ColorSquares);
                        foreach (var attr in colorAttrs)
                        {
                            var valuesThatShouldBeSelected = _productAttributeParser.ParseConditionValue(attr.ConditionAttributeXml, productAttributeMapping.Id, productAttributeValue.Id).ToList();
                            if (valuesThatShouldBeSelected.Any())
                            {
                                var preselectedAttrValue = attr.ProductAttributeValues.Where(p => p.IsPreSelected == true).FirstOrDefault();

                                if(preselectedAttrValue==null)
                                    attr.ProductAttributeValues.FirstOrDefault();

                                var productCustomFieldTemplatePicture = _productCustomFieldService.GetCustomPictureForProductAttributeValue(preselectedAttrValue.Id);
                                if(productCustomFieldTemplatePicture!=null)
                                {
                                    var picture = _pictureService.GetPictureById(productCustomFieldTemplatePicture.PictureId);
                                    if (picture != null)
                                        picturePath = _pictureService.GetThumbLocalPath(picture);
                                }
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(picturePath))
                    {
                        using (var bitmap = new Bitmap(picturePath))
                        using (var graphics = bitmap.GetAdvancedGraphics())
                        {
                            float _dpiX = graphics.DpiX;
                            float _dpiY = graphics.DpiY;
                            foreach (var req in request.Data)
                            {
                                var productCustomFieldSelectingValue = _productCustomFieldService.GetProductCustomFieldValueById(req.ValueId);
                                if (productCustomFieldSelectingValue != null)
                                {
                                    this.InsertTypedNameLog(req.Text,productCustomFieldSelectingValue.ProductCustomFieldMapping.ProductId);
                                    var productCustomFieldSelecting = productCustomFieldSelectingValue.ProductCustomFieldMapping;
                                    if (productCustomFieldSelecting.DesignProductCustomFieldMappings.Any())
                                    {
                                        var designCustomFieldMapping = productCustomFieldSelecting.DesignProductCustomFieldMappings.FirstOrDefault();
                                        var selectedDesignCustomField = _productDesignService.GetDesignCustomFieldById(designCustomFieldMapping.DesignCustomFieldId);

                                        var fontRegistry = new CustomFontRegistry();
                                        // prepare drawing art setting.
                                        string fontFilePath = this.GetFontPathFolder(selectedDesignCustomField.Font);
                                        var postscriptName = fontRegistry.Add(fontFilePath);
                                        var font = fontRegistry.CreateFont(postscriptName, (float)productCustomFieldSelecting.FontSize, _dpiX, _dpiY);
                                        var color = System.Drawing.ColorTranslator.FromHtml(selectedDesignCustomField.FontColor);
                                        int _width = (int)productCustomFieldSelecting.Width;
                                        int _height = (int)productCustomFieldSelecting.Height;
                                        float _xPos = productCustomFieldSelectingValue.PositionX;
                                        float _yPos = productCustomFieldSelectingValue.PositionY;
                                        float _rotate = (float)selectedDesignCustomField.Rotate;
                                        float _bend = (float)selectedDesignCustomField.Bend;
                                        ArtEffect _effect = selectedDesignCustomField.ArtEffect;
                                        ArtType _artType = selectedDesignCustomField.ArtType;
                                        int _aglignment = selectedDesignCustomField.ArtAlignmentId;
                                        Color _effectColor = System.Drawing.ColorTranslator.FromHtml(selectedDesignCustomField.EffectColor);
                                        float _effectSize = (float)selectedDesignCustomField.EffectSize;
                                        float _leftScale = (float)selectedDesignCustomField.LeftScale;
                                        float _rightScale = (float)selectedDesignCustomField.RightScale;
                                        float _tilt = (float)selectedDesignCustomField.Tilt;
                                        bool isFullWidthRequire = selectedDesignCustomField.FullWidthRequire.HasValue ? selectedDesignCustomField.FullWidthRequire.Value:false;
                                        bool isFullBoxSizeRequire = selectedDesignCustomField.FullBoxSizeRequire.HasValue ? selectedDesignCustomField.FullBoxSizeRequire.Value : false;

                                        var artSetting = new DrawingArtTypeSetting();
                                        artSetting.ArtType = _artType;
                                        artSetting.Color = color;
                                        artSetting.Font = font;
                                        artSetting.Bend = _bend;
                                        artSetting.Text = req.Text;
                                        artSetting.Effect = this.GetArtEffect(_effect, _effectColor, _effectSize);
                                        artSetting.Alignment = (TextAlignment)_aglignment;
                                        artSetting.LeftScale = _leftScale;
                                        artSetting.RightScale = _rightScale;
                                        artSetting.Tilt = _tilt;
                                        artSetting.FullWidthRequire = isFullWidthRequire;
                                        artSetting.FullBoxSizeRequire = isFullBoxSizeRequire;

                                        Bitmap img = null;
                                        if (artSetting.FullBoxSizeRequire)
                                            img = this.OnDrawingImage(artSetting, _width, _height, _rotate, _dpiX, _dpiY);
                                        else
                                            img = this.OnDrawingImageNoFullSizeBox(artSetting, _width, _height, _rotate, _dpiX, _dpiY);
                                        
                                        
                                        graphics.DrawImage(img, _xPos, _yPos);
                                       
                                    }
                                    else
                                    {
                                        return this.DrawingTextImage(string.Format("Error: {0}", "This product does not have any design custom field. Please create design customfield"), RgbColor.Red);
                                    }
                                }
                                else
                                {
                                    return this.DrawingTextImage(string.Format("Error: {0}", "This product does not have any customfiled variant. Please generate customfield value for variant"), RgbColor.Red);
                                }
                            }
                            var gdiBitmap = bitmap.ToGdiPlusBitmap();
                            System.Drawing.Bitmap resized = new System.Drawing.Bitmap(gdiBitmap, new System.Drawing.Size(request.Scale, request.Scale));
                            resized.SetResolution(dpi, dpi);
                            System.IO.MemoryStream stream = new System.IO.MemoryStream();
                            resized.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
                            byte[] imageBytes = stream.ToArray();
                            graphics.Dispose();
                            return imageBytes;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this._logger.Warning(string.Format("Error rend image: {0}", ex.Message), ex, _workContext.CurrentCustomer);
                return this.DrawingProductDefaultPicture(request);
            }
            return null;
        }

        protected virtual byte[] DrawingProductDefaultPicture(RenderPictureRequest request)
        {
            try
            {
                var productAttributeValue = _productAttributeService.GetProductAttributeValueById(request.AttrValueId);
                if (productAttributeValue != null)
                {
                    //image path
                    string picturePath = string.Empty;

                    if (productAttributeValue.PictureId > 0)
                    {
                        var picture = _pictureService.GetPictureById(productAttributeValue.PictureId);
                        if (picture != null)
                            picturePath = _pictureService.GetThumbLocalPath(picture);
                    }
                    else
                    {
                        // select ispreselect product
                        var productAttributeMapping = productAttributeValue.ProductAttributeMapping;
                        var product = productAttributeMapping.Product;
                        var productPicture = product.ProductPictures.OrderBy(p=>p.DisplayOrder).FirstOrDefault();
                        if (productPicture != null)
                        {
                            var picture = _pictureService.GetPictureById(productPicture.PictureId);
                            if (picture != null)
                                picturePath = _pictureService.GetThumbLocalPath(picture);
                        }
                    }

                    if (!string.IsNullOrEmpty(picturePath))
                    {
                        using (var bitmap = new Bitmap(picturePath))
                        {
                            var gdiBitmap = bitmap.ToGdiPlusBitmap();
                            System.Drawing.Bitmap resized = new System.Drawing.Bitmap(gdiBitmap, new System.Drawing.Size(request.Scale, request.Scale));
                            resized.SetResolution(dpi, dpi);
                            System.IO.MemoryStream stream = new System.IO.MemoryStream();
                            resized.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
                            byte[] imageBytes = stream.ToArray();
                            return imageBytes;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this._logger.Warning(string.Format("Error rend product default image: {0}", ex.Message), ex, _workContext.CurrentCustomer);
                return this.DrawingTextImage(string.Format("Error: {0}", ex.Message), RgbColor.Red);
            }
            return null;
        }


        protected virtual Bitmap OnDrawingImage(DrawingArtTypeSetting setting,int width, int height, float rotate, float dpiX, float dpiY)
        {
            var artText = this.GetArtText(setting);
            var blackBox = artText.GetBlackBox();
            int _boxWidth = (int)blackBox.Width;
            int _boxHeight = (int)(blackBox.Height) != 0 ? (int)(blackBox.Height) : height;
            //resize box for short name;
            if (!setting.FullWidthRequire && setting.ArtType == ArtType.None)
                _boxWidth = _boxWidth < width ? width : _boxWidth;

            Color background = new RgbColor(255, 255, 255, 0);
          
            var bitmap = new Bitmap(_boxWidth, _boxHeight, PixelFormat.Format64bppArgb, background);
            bitmap.DpiX = dpiX;
            bitmap.DpiY = dpiY;
            var graphics = bitmap.GetAdvancedGraphics();

            //prepare best font for special art effect 
            if (setting.ArtType != ArtType.None && !setting.FullWidthRequire)
            {
                if (artText.String.Length < _bestDrawingStringLength)
                {
                    var scaleFont = this.FindBetsFont(graphics, artText.String, new System.Drawing.SizeF(blackBox.Width, blackBox.Height), artText.Font);
                    artText.Font = scaleFont;
                }
            }
            artText.Center = new System.Drawing.PointF((float)_boxWidth / 2, (float)_boxHeight / 2);
            graphics.DrawText(artText);
            bitmap.Transforms.Resize(width, height);
            bitmap.Transforms.Rotate(rotate, background, InterpolationMode.High);
            graphics.Dispose();
            return bitmap;
        }

        protected virtual Bitmap OnDrawingImageNoFullSizeBox(DrawingArtTypeSetting setting, int width, int height, float rotate, float dpiX, float dpiY)
        {
            var artText = this.GetArtText(setting);
            var blackBox = artText.GetBlackBox();
            
            int _boxWidth = (int)blackBox.Width;
            int _boxHeight = (int)(blackBox.Height) != 0 ? (int)(blackBox.Height) : height;

            Color background = new RgbColor(255, 255, 255, 0);

            var bitmap = new Bitmap(_boxWidth, _boxHeight, PixelFormat.Format64bppArgb, background);
            bitmap.DpiX = dpiX;
            bitmap.DpiY = dpiY;
            var graphics = bitmap.GetAdvancedGraphics();

            artText.Center = new System.Drawing.PointF((float)_boxWidth / 2, (float)_boxHeight / 2);
            graphics.DrawText(artText);
           
            int newWidth = _boxWidth;
            int newHeight = _boxHeight;
            if (_boxWidth > width)
                newWidth = width;
            if (_boxHeight > height)
                newHeight = height;

            if ((newWidth != _boxWidth) || (newHeight != _boxHeight))
            {
                bitmap.Transforms.Resize(newWidth, newHeight);
            }
            
            bitmap.Transforms.Rotate(rotate, background, InterpolationMode.High);

            graphics.Dispose();

            if ((newWidth < width) || (newHeight < height ))
            {
                var coverBitmap = new Bitmap(width, height, PixelFormat.Format64bppArgb, background);

                coverBitmap.DpiX = dpiX;
                coverBitmap.DpiY = dpiY;

                using (var graphicCoverBitmap = coverBitmap.GetAdvancedGraphics())
                {
                    int posx = (width / 2) - (newWidth / 2);
                    graphicCoverBitmap.DrawImage(bitmap, posx, 0);
                }
                
                return coverBitmap;
            }
            else
                return bitmap;
        }

        protected virtual byte[] DrawingTextImage(string text, Color color)
        {
            try
            {
                using (var bitmap = new Bitmap(1000, 1000, PixelFormat.Format24bppRgb, new RgbColor(255, 255, 255, 255)))
                using (var graphics = bitmap.GetAdvancedGraphics())
                using (var fontRegistry = new CustomFontRegistry())
                {
                    var font = graphics.CreateFont("Arial", 30);
                    var boundedText = new BoundedText(text, font, new SolidBrush(color))
                    {
                        Rectangle = new System.Drawing.RectangleF(0, 450, 1000, 1000),
                        Alignment= TextAlignment.Center,
                    };
                    graphics.DrawText(boundedText);
                    var gdiBitmap = bitmap.ToGdiPlusBitmap();
                    System.IO.MemoryStream stream = new System.IO.MemoryStream();
                    gdiBitmap.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
                    byte[] imageBytes = stream.ToArray();
                    graphics.Dispose();
                    return imageBytes;
                }
            }
            catch(Exception ex)
            { }
            return null;

        }

        protected virtual string GetFontPathFolder(string fontName)
        {
            try
            {
                var fontDirectoryPath = CommonHelper.MapPath("~/content/fonts");
                var fontFilePath = System.IO.Path.Combine(fontDirectoryPath, fontName);
                return fontFilePath;
            }
            catch (Exception ex)
            {
            }
            return string.Empty;
        }

        protected virtual Font GetFontByImportedFileName(string fileName,float size)
        {
            try
            {
                using (var fontRegistry = new CustomFontRegistry())
                {
                    var dpi = 72f;
                    // Load custom OpenType font
                    string fontFilePath = this.GetFontPathFolder(fileName);

                    var postscriptName = fontRegistry.Add(fontFilePath);
                    fontRegistry.FallbackFonts.Add("Arial Unicode MS");

                    var font = fontRegistry.CreateFont(postscriptName, size, dpi, dpi);
                    return font;
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        protected virtual ArtText GetArtText(DrawingArtTypeSetting setting)
        {
            try
             {
                var texts = new List<ArtText>();
                var brush = new SolidBrush(setting.Color);

                texts.Add(new BridgeText(setting.Text, setting.Font, brush) {
                    Alignment = setting.Alignment,
                    Effect= setting.Effect,
                    Bend = 0
                });

                texts.Add(new BridgeText(setting.Text, setting.Font, brush)
                {
                    Effect = setting.Effect,
                    Bend = setting.Bend,
                    Alignment = setting.Alignment
                });
                texts.Add(new BulgeText(setting.Text, setting.Font, brush)
                {
                    Effect = setting.Effect,
                    Bend = setting.Bend,
                    Alignment = setting.Alignment
                });
                texts.Add(new PinchText(setting.Text, setting.Font, brush)
                {
                    Effect = setting.Effect,
                    Bend = setting.Bend,
                    Alignment = setting.Alignment
                });
                texts.Add(new RoofText(setting.Text, setting.Font, brush)
                {
                    Effect = setting.Effect,
                    Bend = setting.Bend,
                    Alignment = setting.Alignment
                });
                texts.Add(new ValleyText(setting.Text, setting.Font, brush)
                {
                    Effect = setting.Effect,
                    Bend = setting.Bend,
                    Alignment = setting.Alignment
                });
                texts.Add(new WedgeText(setting.Text, setting.Font, brush)
                {
                    Effect = setting.Effect,
                    Alignment = setting.Alignment,
                    RightScale = setting.RightScale,
                    LeftScale = setting.LeftScale,
                    Tilt = setting.Tilt
                });
                texts.Add(new RoundText(setting.Text, setting.Font, brush)
                {
                    Effect = setting.Effect,
                    Bend = setting.Bend,
                    Alignment = setting.Alignment,
                });
                var result = texts[(int)setting.ArtType] != null ? texts[(int)setting.ArtType] : texts.FirstOrDefault();
                return result;
            }

            catch (Exception ex)
            {
            }
            return null;
        }

        protected virtual Effect GetArtEffect(ArtEffect effect,Color color, float size)
        {
            try
            {
                var effects = new List<Effect>();
                effects.Add(null);
                effects.Add(new Glow(new RgbColor(color), size));
                effects.Add(new Aurigma.GraphicsMill.AdvancedDrawing.Effects.Shadow(new RgbColor(color), size));
                return effects[(int)effect];
            }
            catch(Exception ex)
            {
            }
            return null;
        }
        protected virtual string FindBestStringToDrawing(string text)
        {
            try
            {
                text = text.Trim();
                string whiteSpace = "\t";
                int length = text.Length;
                string bestString = string.Empty;
                if (length < 8)
                {
                    int misssingChar = 8 - length; ;
                    switch (misssingChar)
                    {
                        case 1:
                        case 2:
                            bestString = whiteSpace + text + whiteSpace;
                            break;
                        case 3:
                        case 4:
                            bestString = whiteSpace + whiteSpace + text + whiteSpace + whiteSpace;
                            break;
                        case 5:
                        case 6:
                            bestString = whiteSpace + whiteSpace + whiteSpace + text + whiteSpace + whiteSpace + whiteSpace;
                            break;
                        default: bestString = text; break;
                    }
                    return bestString;
                }
                else return text;
            }
            catch(Exception ex)
            {
                return text;
            }
        }

        protected virtual string PadBoth(string source, int length)
        {
            int spaces = length - source.Length;
            int padLeft = spaces / 2 + source.Length;
            return source.PadLeft(padLeft).PadRight(length);

        }
        protected virtual Font FindBetsFont(Graphics g, string longString, System.Drawing.SizeF Room, Font PreferedFont)
        {
            //you should perform some scale functions!!!
            var RealSize = PreferedFont.MeasureString(longString);
            float HeightScaleRatio = Room.Height / RealSize.Height;
            float WidthScaleRatio = Room.Width / RealSize.Width;
            float ScaleRatio = (HeightScaleRatio < WidthScaleRatio) ? ScaleRatio = HeightScaleRatio : ScaleRatio = WidthScaleRatio;
            float ScaleFontSize = PreferedFont.Size * ScaleRatio;
            var fontRegistry = new CustomFontRegistry();
            var font = g.CreateFont(PreferedFont.PostscriptName, ScaleFontSize);
            return font;
        }

        protected virtual void InsertTypedNameLog(string name, int productId)
        {
            try
            {
                if(!string.IsNullOrEmpty(name))
                {
                    var customer = _workContext.CurrentCustomer;
                    if(customer!=null)
                    {
                        var isAdmin = customer.CustomerRoles.Where(p => p.Active && p.Name.Equals("Administrators")).FirstOrDefault()!=null?true:false;
                        if (!isAdmin)
                        {
                            var typedName = _typedNameService.GetByName(name, customer.Id, productId);
                            if (typedName != null)
                            {
                                typedName.Count++;
                                typedName.CreatedOnTime = DateTime.UtcNow;
                                _typedNameService.Update(typedName);
                            }
                            else
                            {
                                typedName = new TypedName
                                {
                                    Name = name,
                                    CustomerId = customer.Id,
                                    Count = 1,
                                    ProductId = productId,
                                    CreatedOnTime= DateTime.UtcNow 
                                };
                                _typedNameService.Insert(typedName);
                            }
                        }
                    }
                }
            }
            catch(Exception ex) { }
        }
    }
}
