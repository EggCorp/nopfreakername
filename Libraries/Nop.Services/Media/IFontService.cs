﻿using Nop.Core.Domain.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Media
{
    public partial interface IFontService
    {
        FontUploadResponse UploadFont(byte[] fontBinary,string fontName, string mimeType);
    }
}
