﻿using Newtonsoft.Json;
using Nop.Core;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.SubscribeMessenger;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Tasks;
using RestSharp;
using System;
using System.Net;

namespace Nop.Services.SubscribeMessenger
{
    public partial class SendSubcribeMessageTask : ITask
    {
        #region Field
        private readonly ILogger _logger;
        private ISubscribeMessageService _subscribeMessageService;
        private readonly IStoreContext _storeContext;
        private readonly IPictureService _pictureService;
        #endregion

        #region Const
        private const string EndPointSendMessage = "/v2.6/me/messages";
        private const string ApiFacebook = "https://graph.facebook.com";
        #endregion


        public SendSubcribeMessageTask(
            ISubscribeMessageService subscribeMessageService,
            ILogger logger, IStoreContext storeContext, IPictureService pictureService)
        {
            this._subscribeMessageService = subscribeMessageService;
            this._logger = logger;
            _storeContext = storeContext;
            _pictureService = pictureService;
        }
        public void Execute()
        {
            var messages = _subscribeMessageService.GetNotSendMessageByDate(DateTime.UtcNow);
            foreach (SubscribeMessage mess in messages)
            {
                try
                {
                    FbSendMessage fbSendMessage = JsonConvert.DeserializeObject<FbSendMessage>(mess.MessagesJson);
                    fbSendMessage.Recipient.UserRef = mess.SubscribeCustomer.UserRef;

                    RestClient restClient = new RestClient(ApiFacebook);
                    RestRequest request = new RestRequest(string.Format("{0}?access_token={1}", EndPointSendMessage, mess.SubscribeCustomer.Token), Method.POST);
                    // check image server
                    if (fbSendMessage.Message.Attachment.Type.Equals(FbAttachmentType.Image))
                    {
                        fbSendMessage.Message.Attachment.Payload.Url = ChangeImageUrl(fbSendMessage.Message.Attachment.Payload.Url);
                    }

                    request.AddParameter("Application/Json", JsonConvert.SerializeObject(fbSendMessage), ParameterType.RequestBody);


                    //request.AddJsonBody(fbSendMessage);
                    request.RequestFormat = DataFormat.Json;

                    var res = restClient.Execute(request);

                    var responResult = JsonConvert.DeserializeObject<FbReponseSendMessage>(res.Content);
                    if (!string.IsNullOrEmpty(responResult.MessageId))
                    {
                        // update send success
                        mess.SendingStatusId = (int)SubscribeMesseageSendingStatus.Completed;
                        _subscribeMessageService.UpdateSubscribeMessage(mess);
                    }
                    else
                    {
                        // sending error
                        mess.SendingStatusId = (int)SubscribeMesseageSendingStatus.Error;
                        mess.Note = JsonConvert.SerializeObject(responResult);
                        _subscribeMessageService.UpdateSubscribeMessage(mess);
                    }

                }
                catch (Exception ex)
                {
                    mess.SendingStatusId = (int)SubscribeMesseageSendingStatus.Error;
                    mess.Note = ex.Message;
                    _subscribeMessageService.UpdateSubscribeMessage(mess);
                }
            }
        }

        private string ChangeImageUrl(string url)
        {
            if (!url.Contains(_storeContext.CurrentStore.Url))
            {
                // download image
                var webClient = new WebClient();
                byte[] imageBytes = new byte[0];
                imageBytes = webClient.DownloadData(url);
                Picture picture = _pictureService.InsertPicture(imageBytes, "image/jpg", string.Empty);
                return _pictureService.GetPictureUrl(picture.Id, 943);
            }
            else
                return url;
        }
    }
}
