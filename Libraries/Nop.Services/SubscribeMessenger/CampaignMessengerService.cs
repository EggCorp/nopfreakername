﻿using Nop.Core.Data;
using Nop.Core.Domain.SubscribeMessenger;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nop.Services.SubscribeMessenger
{
    public partial class CampaignMessengerService : ICampaignMessengerService
    {
        #region Field
        private readonly IRepository<CampaignMessenger> _campaignMessengerRepository;
        private readonly IRepository<CampaignInterest> _campaignInterestRepository;
        private readonly IRepository<CampaignEntity> _campaignEntityRepository;
        private readonly IRepository<CampaignSubscribeCustomer> _campaignSubscribeCustomerRepository;
        #endregion

        #region Constructor
        public CampaignMessengerService(IRepository<CampaignMessenger> campaignMessengerRepository, 
                                        IRepository<CampaignInterest> campaignInterestRepository,
                                        IRepository<CampaignEntity> campaignEntityRepository,
                                        IRepository<CampaignSubscribeCustomer> campaignSubscribeCustomerRepository)
        {
            _campaignMessengerRepository = campaignMessengerRepository;
            _campaignInterestRepository = campaignInterestRepository;
            _campaignEntityRepository = campaignEntityRepository;
            _campaignSubscribeCustomerRepository = campaignSubscribeCustomerRepository;
        }

        #endregion

        #region Method
        public IList<CampaignMessenger> Search()
        {
            try
            {
                var query = _campaignMessengerRepository.Table.OrderByDescending(sc => sc.Id);

                return query.ToList();
            }
            catch
            {
                // do nothing
            }
            return null;
        }


        public void Insert(CampaignMessenger model)
        {
            _campaignMessengerRepository.Insert(model);
        }

        public CampaignMessenger GetCampaignMessengerById(int id)
        {
            return _campaignMessengerRepository.GetById(id);
        }

        public void UpdateCampaignMessenger(CampaignMessenger model)
        {
            _campaignMessengerRepository.Update(model);
        }

        public void InsertCampaignInterest(CampaignInterest model)
        {
            _campaignInterestRepository.Insert(model);
        }

        public void UpdateCampaignInterest(CampaignInterest model)
        {
            _campaignInterestRepository.Update(model);
        }

        public CampaignInterest GetCampaignInterestById(int id)
        {
            return _campaignInterestRepository.GetById(id);
        }

        public IList<CampaignInterest> GetCampaignInterestByCampaignMessengerId(int campaignMessengerId)
        {
            try
            {
                var query = _campaignInterestRepository.Table.Where(ci => ci.CampaignMessengerId == campaignMessengerId);

                return query.ToList();
            }
            catch (Exception ex)
            {
                // do nothing
            }
            return null;
        }

        public void DeleteCampaignInterest(CampaignInterest campaignInterest)
        {
            _campaignInterestRepository.Delete(campaignInterest);
        }

        public void GenerateFbSendMessageFromCampaignMessengerId(int campaignMessengerId, out FbSendMessage attachmentFbMessage, out FbSendMessage textFbMessage)
        {
            CampaignMessenger campaignMessenger = _campaignMessengerRepository.GetById(campaignMessengerId);

            attachmentFbMessage = new FbSendMessage();
            attachmentFbMessage.Message.Attachment.Type = campaignMessenger.AttachmentType;
            attachmentFbMessage.Message.Attachment.Payload.Url = campaignMessenger.AttachmentUrl;
            attachmentFbMessage.Message.Attachment.Payload.IsReusable = true;

            textFbMessage = new FbSendMessage();
            textFbMessage.Message.Attachment.Type = FbAttachmentType.Template;
            textFbMessage.Message.Attachment.Payload.TemplateType = FbPayloadTemplateType.Button;
            textFbMessage.Message.Attachment.Payload.Text = campaignMessenger.TextMessage;
            textFbMessage.Message.Attachment.Payload.Buttons = new List<FbButton>();
            textFbMessage.Message.Attachment.Payload.Buttons.Add(new FbButton
            {
                Title = campaignMessenger.CallToAction,
                Type = FbButtonType.WebUrl,
                Url = campaignMessenger.Link
            });
        }

        public void UpdateStatusCampaignMessenger(int campaignId, CampaignStatus status)
        {
            CampaignMessenger campaignMessenger = _campaignMessengerRepository.GetById(campaignId);
            campaignMessenger.StatusId = (int)status;
            _campaignMessengerRepository.Update(campaignMessenger);
        }

        public CampaignMessenger GetFirstCampaignMessengerByStatus(int statusId)
        {
            return _campaignMessengerRepository.Table.Where(cpm => cpm.StatusId == statusId).FirstOrDefault();
        }

        public void InsertCampaignEntity(CampaignEntity campaignEntity)
        {
            _campaignEntityRepository.Insert(campaignEntity);
        }

        public bool ExitCampaignEntity(CampaignEntity campaignEntity)
        {
            var tmpCampaignEntity = _campaignEntityRepository.Table.Where(ce => ce.EntityName.Equals(campaignEntity.EntityName) && (ce.EntityId == campaignEntity.EntityId) && (ce.CampaignMessengerId == ce.CampaignMessengerId)).FirstOrDefault();
            return tmpCampaignEntity != null;
        }

        public IList<CampaignEntity> GetCampaignEntityByCampaignMessengerId(int campaignMessengerId, int campaignEntityStatus, int limit = 100)
        {
            return _campaignEntityRepository.Table.Where(ce => ce.CampaignMessengerId == campaignMessengerId && ce.StatusId == campaignEntityStatus).Take(limit).ToList();
        }

        public void AddCampaignSubscribeCustomer(CampaignSubscribeCustomer campaignSubscribeCustomer)
        {
            _campaignSubscribeCustomerRepository.Insert(campaignSubscribeCustomer);
        }

        public bool IsExitCampaignSubscribeCustomer(int campaignMessengerId, int subscribeCustomerId)
        {
            return _campaignSubscribeCustomerRepository.Table.Where(cscb => cscb.CampaignMessengerId == campaignMessengerId && cscb.SubscribeCustomerId == subscribeCustomerId).Count() > 0;
        }

        public void UpdateCampaignEntityStatus(int campaignEntityId, int status)
        {
            CampaignEntity campaignEntity = _campaignEntityRepository.GetById(campaignEntityId);
            campaignEntity.StatusId = status;
            _campaignEntityRepository.Update(campaignEntity);
        }

        public IList<CampaignSubscribeCustomer> GetCampaignSubscribeCustomersByCampaignMessengerId(int campaignMessengerId, int status, int limit = 100)
        {
            return _campaignSubscribeCustomerRepository.Table.Where(csc => csc.CampaignMessengerId == campaignMessengerId && csc.StatusId == status).Take(limit).ToList();
        }

        public void UpdateCampaignSubscribeCustomerStatus(int campaignSubscribeCustomerId, int status)
        {
            CampaignSubscribeCustomer campaignSubscribeCustomer = _campaignSubscribeCustomerRepository.GetById(campaignSubscribeCustomerId);
            campaignSubscribeCustomer.StatusId = status;
            _campaignSubscribeCustomerRepository.Update(campaignSubscribeCustomer);
        }

        public int GetTotalAudienceOfCampaignMessenger(int campaignMessengerId)
        {
            return _campaignSubscribeCustomerRepository.Table.Where(csc => csc.CampaignMessengerId == campaignMessengerId).Count();
        }
        #endregion
    }
}
