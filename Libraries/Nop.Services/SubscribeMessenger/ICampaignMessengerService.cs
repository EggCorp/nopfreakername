﻿using Nop.Core.Domain.SubscribeMessenger;
using System.Collections.Generic;

namespace Nop.Services.SubscribeMessenger
{
    public partial interface ICampaignMessengerService
    {
        IList<CampaignMessenger> Search();

        void Insert(CampaignMessenger model);

        CampaignMessenger GetCampaignMessengerById(int id);

        void UpdateCampaignMessenger(CampaignMessenger model);
        void UpdateStatusCampaignMessenger(int campaignId, CampaignStatus status);

        IList<CampaignInterest> GetCampaignInterestByCampaignMessengerId(int campaignMessengerId);
        CampaignInterest GetCampaignInterestById(int id);
        void InsertCampaignInterest(CampaignInterest model);
        void UpdateCampaignInterest(CampaignInterest model);
        void DeleteCampaignInterest(CampaignInterest campaignInterest);
        void GenerateFbSendMessageFromCampaignMessengerId(int campaignMessengerId, out FbSendMessage attachmentFbMessage, out FbSendMessage textFbMessage);
        CampaignMessenger GetFirstCampaignMessengerByStatus(int statusId);

        void InsertCampaignEntity(CampaignEntity campaignEntity);
        bool ExitCampaignEntity(CampaignEntity campaignEntity);
        void UpdateCampaignEntityStatus(int campaignEntityId, int status);
        IList<CampaignEntity> GetCampaignEntityByCampaignMessengerId(int campaignMessengerId, int campaignEntityStatus, int limit = 100);

        void AddCampaignSubscribeCustomer(CampaignSubscribeCustomer campaignSubscribeCustomer);
        bool IsExitCampaignSubscribeCustomer(int campaignMessengerId, int subscribeCustomerId);
        IList<CampaignSubscribeCustomer> GetCampaignSubscribeCustomersByCampaignMessengerId(int campaignMessengerId, int status, int limit = 100);
        void UpdateCampaignSubscribeCustomerStatus(int campaignSubscribeCustomerId, int status);

        int GetTotalAudienceOfCampaignMessenger(int campaignMessengerId);
    }
}
