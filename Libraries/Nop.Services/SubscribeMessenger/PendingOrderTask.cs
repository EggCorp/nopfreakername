﻿using Nop.Services.Logging;
using Nop.Core.Domain.Customers;
using Nop.Services.Tasks;

namespace Nop.Services.SubscribeMessenger
{

    public partial class PendingOrderTask : ITask
    {
        #region Field
        private readonly ILogger _logger;
        private ISubscribeMessageService _subscribeMessageService;
        #endregion

        public PendingOrderTask(ISubscribeMessageService subscribeMessageService,
            ILogger logger)
        {
            _logger = logger;
            _subscribeMessageService = subscribeMessageService;
        }

        public void Execute()
        {
            SubscribeCustomer sc = new SubscribeCustomer();
            sc.Id = 1;
            sc.ProductId = 4491;
            //_subscribeMessageService.TrackingOrderMessage(1177, "Https://wwww.google.com");
            _subscribeMessageService.RemindPendingOrderMessage(336, "82DFF1DE-CE4F-4511-B727-6E9A2B930A9A", 1);
        }
    }
}
