﻿using Nop.Core.Domain.Customers;
using Nop.Core.Domain.SubscribeMessenger;
using System;
using System.Collections.Generic;

namespace Nop.Services.SubscribeMessenger
{
    public partial interface ISubscribeMessageService
    {
        List<SubscribeMessage> GetNotSendMessageByDate(DateTime date);
        void UpdateSubscribeMessage(SubscribeMessage message);
        void Add(SubscribeMessage message);
        void RemindViewProductFirstMessage(SubscribeCustomer subscribeCustomer);
        void RemindViewProduct(int subscribeId, int productId, int step);
        void ProcessingOrderMessage(int orderId);
        void TrackingOrderMessage(int orderId, string linkTracking);
        void RemindPendingOrderMessage(int subscribeCustomerId, string orderGuid, int step);
        void RemindAdd2CartMessage(int subscribeCustomerId, int customerId, int step);
        List<SubscribeMessage> GetMessageNotCompletedByFollow(SubscribeFollowType followType);
        List<SubscribeMessage> GetMessageNotCompletedBySubscribeCustomerId(int subscribeCustomerId);
        
    }
}
