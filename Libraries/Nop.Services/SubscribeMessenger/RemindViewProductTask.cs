﻿using Nop.Services.Logging;
using Nop.Services.Tasks;
using Nop.Services.Customers;
using Nop.Core.Domain.SubscribeMessenger;
using System.Collections.Generic;
using System.Linq;
using Nop.Core.Domain.Customers;
using System;
using Nop.Core.Domain.Orders;
using Nop.Services.Orders;

namespace Nop.Services.SubscribeMessenger
{
    public partial class RemindViewProductTask : ITask
    {
        #region Field
        private readonly ILogger _logger;
        private ISubscribeMessageService _subscribeMessageService;
        private ISubscribeActivityLogService _subscribeActivityLogService;
        private ISubscribeCustomerService _subscribeCustomerService;
        private IOrderService _orderService;
        #endregion

        public RemindViewProductTask(ISubscribeMessageService subscribeMessageService,
            ILogger logger, ISubscribeActivityLogService subscribeActivityLogService,
            IOrderService orderService, ISubscribeCustomerService subscribeCustomerService)
        {
            _logger = logger;
            _subscribeMessageService = subscribeMessageService;
            _subscribeActivityLogService = subscribeActivityLogService;
            _orderService = orderService;
            _subscribeCustomerService = subscribeCustomerService;
        }

        public void Execute()
        {
            // get all SubscribeMessage which have follow in step 1 and not completed
            List<SubscribeMessage> allMessageStepViewProducts = _subscribeMessageService.GetMessageNotCompletedByFollow(SubscribeFollowType.RemindViewProduct);

            var groupSubscribeCustomers = allMessageStepViewProducts.GroupBy(ms => ms.SubscribeCustomerId).Select(ms => ms);

            foreach(var itemSubscribeCustomer in groupSubscribeCustomers)
            {
                try
                {
                    // all message of customer
                    List<SubscribeMessage> subMessageOfCustomer = _subscribeMessageService.GetMessageNotCompletedBySubscribeCustomerId(itemSubscribeCustomer.Key.Value);


                    // get subscribe message of customer
                    List<SubscribeMessage> subscribeMessageViewProductOfCustomer = new List<SubscribeMessage>();
                    foreach (SubscribeMessage sc in itemSubscribeCustomer)
                    {
                        subscribeMessageViewProductOfCustomer.Add(sc);
                    }
                    DateTime minDateTimeSubscribeMessage = subscribeMessageViewProductOfCustomer.Min(sc => sc.CreatedOnUtc);

                    // check customer has activity PLACE ORDER
                    List<SubscribeActivityLog> placeOrderActivityLogs = _subscribeActivityLogService.GetPlaceOrderLogsOfSubscribeCustomer(itemSubscribeCustomer.Key.Value).Where(scl => scl.CreatedOnUtc > minDateTimeSubscribeMessage && scl.CreatedOnUtc <= DateTime.UtcNow.AddMinutes(-30)).ToList();

                    bool continueNextFollow = false;

                    if (placeOrderActivityLogs.Count() > 0) // subscribeCustomer has activity place order
                    {
                        string orderGuidPending = GetPendingOrderGuidId(placeOrderActivityLogs);
                        if (!string.IsNullOrEmpty(orderGuidPending))
                        {
                            // send pending message order
                            _subscribeMessageService.RemindPendingOrderMessage(itemSubscribeCustomer.Key.Value, orderGuidPending, 1);
                        }
                        else // subscribeCustomer has completed order
                        {
                            // insert completed subscribe message follow pending order
                            SubscribeMessage scCompletedPendingOrder = new SubscribeMessage();
                            scCompletedPendingOrder.CreatedOnUtc = DateTime.UtcNow;
                            scCompletedPendingOrder.SendingStatusId = (int)SubscribeMesseageSendingStatus.Completed;
                            scCompletedPendingOrder.SendDateUtc = DateTime.UtcNow;
                            scCompletedPendingOrder.Step = 2;
                            scCompletedPendingOrder.SubscribeCustomerId = itemSubscribeCustomer.Key.Value;
                            scCompletedPendingOrder.SubscribeFollowId = (int)SubscribeFollowType.RemindPendingOrder;
                            scCompletedPendingOrder.Note = "Completed subscribe message follow pending order";
                            _subscribeMessageService.Add(scCompletedPendingOrder);
                        }

                        // update all previous step sending message completed
                        foreach (SubscribeMessage sc in subMessageOfCustomer)
                        {
                            sc.SendingStatusId = (int)SubscribeMesseageSendingStatus.Completed;
                            sc.IsCompletedFollow = true;
                            _subscribeMessageService.UpdateSubscribeMessage(sc);
                        }

                        continueNextFollow = false;

                        _logger.Information(string.Format("Task Remind View Product: SubscribeCustomerID: {0} - Place OrderGUID: {1}", itemSubscribeCustomer.Key.Value, orderGuidPending));
                    }
                    else // subscribeCustomer has no activity place order
                    {
                        // continue check add2Cart Activity
                        continueNextFollow = true;
                    }

                    // check Add2Cart Activity
                    if (continueNextFollow)
                    {
                        List<SubscribeActivityLog> add2CartActivityLogs = _subscribeActivityLogService.GetAddToCartLogsOfSubscribeCustomer(itemSubscribeCustomer.Key.Value).Where(scl => scl.CreatedOnUtc > minDateTimeSubscribeMessage).ToList();
                        if (add2CartActivityLogs.Count() > 0)
                        {
                            SubscribeCustomer subscribeCustomer = _subscribeCustomerService.GetById(add2CartActivityLogs[0].SubscribeCustomerId.Value);
                            if (subscribeCustomer.CustomerId.HasValue)
                            {
                                // send remind add2cart message
                                _subscribeMessageService.RemindAdd2CartMessage(subscribeCustomer.Id, subscribeCustomer.CustomerId.Value, 1);

                                // update all previous step sending message completed
                                foreach (SubscribeMessage sc in subMessageOfCustomer)
                                {
                                    sc.SendingStatusId = (int)SubscribeMesseageSendingStatus.Completed;
                                    sc.IsCompletedFollow = true;
                                    _subscribeMessageService.UpdateSubscribeMessage(sc);
                                }

                                continueNextFollow = false;

                                _logger.Information(string.Format("Task Remind View Product: SubscribeCustomerID:{0} -  Add2Cart", itemSubscribeCustomer.Key.Value));
                            }
                            else
                            {
                                continueNextFollow = true;
                            }
                        }
                        else
                        {
                            continueNextFollow = true;
                        }
                    }

                    // check ViewProdcut Activity
                    if (continueNextFollow)
                    {
                        List<SubscribeActivityLog> viewProductActivityLogs = _subscribeActivityLogService.GetViewProductLogsOfSubscribeCustomer(itemSubscribeCustomer.Key.Value).Where(scl => scl.CreatedOnUtc > minDateTimeSubscribeMessage).ToList();
                        List<int> productIdsViewed = viewProductActivityLogs.Select(activity => activity.EntityId.Value).Distinct().ToList();

                        SubscribeMessage subMessageMaxStep = subscribeMessageViewProductOfCustomer.Where(ms=> ms.SubscribeFollowId == (int)SubscribeFollowType.RemindViewProduct).OrderByDescending(ms => ms.Step).First();

                        if (subMessageMaxStep.SendingStatusId == (int)SubscribeMesseageSendingStatus.Completed)
                        {
                            if (subMessageMaxStep.Step == 3) // max step follow view product
                            {
                                // update all completed all message view product
                                foreach (SubscribeMessage sc in subscribeMessageViewProductOfCustomer)
                                {
                                    sc.SendingStatusId = (int)SubscribeMesseageSendingStatus.Completed;
                                    sc.IsCompletedFollow = true;
                                    _subscribeMessageService.UpdateSubscribeMessage(sc);
                                }

                                _logger.Information(string.Format("Task Remind View Product: SubscribeCustomerID: {0} - Finish Step Remind View Product", itemSubscribeCustomer.Key.Value));
                            }
                            else
                            {
                                if (productIdsViewed.Count() >= (subMessageMaxStep.Step + 1))
                                {
                                    _subscribeMessageService.RemindViewProduct(itemSubscribeCustomer.Key.Value, productIdsViewed[subMessageMaxStep.Step.Value], subMessageMaxStep.Step.Value + 1);
                                }
                                else
                                {
                                    _subscribeMessageService.RemindViewProduct(itemSubscribeCustomer.Key.Value, productIdsViewed[0], subMessageMaxStep.Step.Value + 1);
                                }

                                _logger.Information(string.Format("Task Remind View Product: SubscribeCustomerID: {0} - Step {1}", itemSubscribeCustomer.Key.Value, subMessageMaxStep.Step.Value + 1));
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error("Task Remind View Product", ex);
                }
            }
        }

        private string GetPendingOrderGuidId(List<SubscribeActivityLog> placeOrderActivityLogs)
        {
            List<Order> orders = new List<Order>();
            foreach(var orderLog in placeOrderActivityLogs)
            {
                orders.Add(_orderService.GetOrderById(orderLog.EntityId.Value));
            }

            var pendingOrders = orders.Where(ord => ord.OrderStatusId == (int)OrderStatus.Pending).ToList();
            var completedOrders = orders.Where(ord => (ord.OrderStatusId != (int)OrderStatus.Pending) && (ord.OrderStatusId != (int)OrderStatus.Cancelled)).ToList();

            if (pendingOrders.Count() > 0 && completedOrders.Count() == 0)
            {
                return pendingOrders[0].OrderGuid.ToString();
            }
            else
                return string.Empty;
        }
    }
}
