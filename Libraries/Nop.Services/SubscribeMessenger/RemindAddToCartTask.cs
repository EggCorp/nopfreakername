﻿using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.SubscribeMessenger;
using Nop.Services.Customers;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nop.Services.SubscribeMessenger
{
    public partial class RemindAddToCartTask : ITask
    {
        #region Field
        private readonly ILogger _logger;
        private ISubscribeMessageService _subscribeMessageService;
        private ISubscribeActivityLogService _subscribeActivityLogService;
        private ISubscribeCustomerService _subscribeCustomerService;
        private IOrderService _orderService;
        #endregion

        public RemindAddToCartTask(ISubscribeMessageService subscribeMessageService,
            ILogger logger, ISubscribeActivityLogService subscribeActivityLogService,
            IOrderService orderService, ISubscribeCustomerService subscribeCustomerService)
        {
            _logger = logger;
            _subscribeMessageService = subscribeMessageService;
            _subscribeActivityLogService = subscribeActivityLogService;
            _orderService = orderService;
            _subscribeCustomerService = subscribeCustomerService;
        }


        public void Execute()
        {
            // get all SubscribeMessage which have follow in step 2 and not completed
            List<SubscribeMessage> messageStepAdd2Cart = _subscribeMessageService.GetMessageNotCompletedByFollow(SubscribeFollowType.RemindAdd2Cart);

            var groupSubscribeCustomers = messageStepAdd2Cart.GroupBy(ms => ms.SubscribeCustomerId).Select(ms => ms);
            foreach (var itemSubscribeCustomer in groupSubscribeCustomers)
            {
                try
                {
                    // all message of customer
                    List<SubscribeMessage> subMessageOfCustomer = _subscribeMessageService.GetMessageNotCompletedBySubscribeCustomerId(itemSubscribeCustomer.Key.Value);


                    // get subscribe message of customer
                    List<SubscribeMessage> subscribeMessageAdd2CartOfCustomer = new List<SubscribeMessage>();

                    foreach (SubscribeMessage sc in itemSubscribeCustomer)
                    {
                        subscribeMessageAdd2CartOfCustomer.Add(sc);
                    }
                    DateTime minDateTimeSubscribeMessageAdd2Cart = subscribeMessageAdd2CartOfCustomer.Min(sc => sc.CreatedOnUtc);

                    // check customer has activity PLACE ORDER
                    List<SubscribeActivityLog> placeOrderActivityLogs = _subscribeActivityLogService.GetPlaceOrderLogsOfSubscribeCustomer(itemSubscribeCustomer.Key.Value).Where(scl => scl.CreatedOnUtc > minDateTimeSubscribeMessageAdd2Cart && scl.CreatedOnUtc <= DateTime.UtcNow.AddMinutes(-30)).ToList();

                    bool continueNextFollow = false;

                    if (placeOrderActivityLogs.Count() > 0) // subscribeCustomer has activity place order
                    {
                        string orderGuidPending = GetPendingOrderGuidId(placeOrderActivityLogs);
                        if (!string.IsNullOrEmpty(orderGuidPending))
                        {
                            // send pending message order
                            _subscribeMessageService.RemindPendingOrderMessage(itemSubscribeCustomer.Key.Value, orderGuidPending, 1);
                        }
                        else // subscribeCustomer has completed order
                        {
                            // insert completed subscribe message follow pending order
                            SubscribeMessage scCompletedPendingOrder = new SubscribeMessage();
                            scCompletedPendingOrder.CreatedOnUtc = DateTime.UtcNow;
                            scCompletedPendingOrder.SendingStatusId = (int)SubscribeMesseageSendingStatus.Completed;
                            scCompletedPendingOrder.SendDateUtc = DateTime.UtcNow;
                            scCompletedPendingOrder.Step = 2;
                            scCompletedPendingOrder.SubscribeCustomerId = itemSubscribeCustomer.Key.Value;
                            scCompletedPendingOrder.SubscribeFollowId = (int)SubscribeFollowType.RemindPendingOrder;
                            scCompletedPendingOrder.Note = "Completed subscribe message follow pending order";
                            _subscribeMessageService.Add(scCompletedPendingOrder);
                        }

                        // update all previous step sending message completed
                        foreach (SubscribeMessage sc in subMessageOfCustomer)
                        {
                            sc.SendingStatusId = (int)SubscribeMesseageSendingStatus.Completed;
                            sc.IsCompletedFollow = true;
                            _subscribeMessageService.UpdateSubscribeMessage(sc);
                        }

                        continueNextFollow = false;

                        _logger.Information(string.Format("Task Remind Add2Cart: SubscribeCustomerID: {0} - Place OrderGUID: {1}", itemSubscribeCustomer.Key.Value, orderGuidPending));
                    }
                    else // subscribeCustomer has no activity place order
                    {
                        // continue check add2Cart Activity
                        continueNextFollow = true;
                    }

                    // check Add2Cart Activity
                    if (continueNextFollow)
                    {
                        SubscribeMessage subMessageMaxStep = subscribeMessageAdd2CartOfCustomer.Where(ms => ms.SubscribeFollowId == (int)SubscribeFollowType.RemindAdd2Cart).OrderByDescending(ms => ms.Step).First();

                        if (subMessageMaxStep.SendingStatusId == (int)SubscribeMesseageSendingStatus.Completed)
                        {
                            if (subMessageMaxStep.Step == 2) // max step follow view product
                            {
                                // update all completed all message view product
                                foreach (SubscribeMessage sc in subscribeMessageAdd2CartOfCustomer)
                                {
                                    sc.SendingStatusId = (int)SubscribeMesseageSendingStatus.Completed;
                                    sc.IsCompletedFollow = true;
                                    _subscribeMessageService.UpdateSubscribeMessage(sc);
                                }

                                _logger.Information(string.Format("Task Remind Add2Cart: SubscribeCustomerID: {0} - Finish Step", itemSubscribeCustomer.Key.Value));
                            }
                            else
                            {
                                SubscribeMessage subMessageAdd2CartNextStep = subMessageMaxStep;
                                subMessageAdd2CartNextStep.CreatedOnUtc = DateTime.UtcNow;
                                subMessageAdd2CartNextStep.SendDateUtc = DateTime.UtcNow.AddHours(11);
                                subMessageAdd2CartNextStep.SendingStatusId = (int)SubscribeMesseageSendingStatus.New;
                                subMessageAdd2CartNextStep.Step = subMessageMaxStep.Step + 1;
                                _subscribeMessageService.Add(subMessageMaxStep);

                                _logger.Information(string.Format("Task Remind Add2Cart: SubscribeCustomerID: {0} - Step {1}", itemSubscribeCustomer.Key.Value, subMessageMaxStep.Step.Value + 1));
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error("Task Remind Add2Cart", ex);
                }
            }
        }

        private string GetPendingOrderGuidId(List<SubscribeActivityLog> placeOrderActivityLogs)
        {
            List<Order> orders = new List<Order>();
            foreach (var orderLog in placeOrderActivityLogs)
            {
                orders.Add(_orderService.GetOrderById(orderLog.EntityId.Value));
            }

            var pendingOrders = orders.Where(ord => ord.OrderStatusId == (int)OrderStatus.Pending).ToList();
            var completedOrders = orders.Where(ord => (ord.OrderStatusId != (int)OrderStatus.Pending) && (ord.OrderStatusId != (int)OrderStatus.Cancelled)).ToList();

            if (pendingOrders.Count() > 0 && completedOrders.Count() == 0)
            {
                return pendingOrders[0].OrderGuid.ToString();
            }
            else
                return string.Empty;
        }
    }
}
