﻿using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.SubscribeMessenger;
using Nop.Services.Catalog;
using Nop.Services.Logging;
using Nop.Services.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nop.Services.SubscribeMessenger
{
    public partial class ProcessCampaignMessengerTask : ITask
    {
        #region
        private readonly ILogger _logger;
        private readonly ICampaignMessengerService _campaignMessengerService;
        private readonly ICategoryService _categoryService;
        private readonly IProductService _productService;
        #endregion

        public ProcessCampaignMessengerTask(ILogger logger, ICampaignMessengerService campaignMessengerService, 
                                            ICategoryService categoryService, IProductService productService)
        {
            _logger = logger;
            _campaignMessengerService = campaignMessengerService;
            _categoryService = categoryService;
            _productService = productService;
        }
        public void Execute()
        {
            try
            {
                CampaignMessenger campaignMessenger = _campaignMessengerService.GetFirstCampaignMessengerByStatus((int)CampaignStatus.Processing);

                if (campaignMessenger != null)
                {
                    int numberEntity = 0;

                    List<CampaignInterest> campaignInterests = _campaignMessengerService.GetCampaignInterestByCampaignMessengerId(campaignMessenger.Id).ToList();
                    foreach(CampaignInterest interest in campaignInterests)
                    {
                        if (interest.Type.ToLower().Equals(CampaignInterestType.Category.ToString().ToLower()))
                        {
                            var subCategories = _categoryService.GetAllCategoriesByParentCategoryId(interest.CateogryId.Value);

                            // add this category to list
                            subCategories.Add(_categoryService.GetCategoryById(interest.CateogryId.Value));

                            foreach(Category cat in subCategories)
                            {
                                // add Campaign Entity Category
                                CampaignEntity tmpCampaignEntity = new CampaignEntity();
                                tmpCampaignEntity.EntityName = CampaignEntityType.Category.ToString();
                                tmpCampaignEntity.StatusId = (int)CampaignStatus.New;
                                tmpCampaignEntity.EntityId = cat.Id;
                                tmpCampaignEntity.CampaignMessengerId = campaignMessenger.Id;

                                if (!_campaignMessengerService.ExitCampaignEntity(tmpCampaignEntity))
                                {
                                    _campaignMessengerService.InsertCampaignEntity(tmpCampaignEntity);
                                    numberEntity++;
                                }
                                    

                                // add Campaign Entity Product
                                var productsOfCategory = _categoryService.GetProductCategoriesByCategoryId(cat.Id);
                                foreach(var productCat in productsOfCategory)
                                {
                                    CampaignEntity tmpCampaignEntityProdduct = new CampaignEntity();

                                    tmpCampaignEntityProdduct.EntityName = CampaignEntityType.Product.ToString();
                                    tmpCampaignEntityProdduct.StatusId = (int)CampaignStatus.New;
                                    tmpCampaignEntityProdduct.EntityId = productCat.ProductId;
                                    tmpCampaignEntityProdduct.CampaignMessengerId = campaignMessenger.Id;

                                    if (!_campaignMessengerService.ExitCampaignEntity(tmpCampaignEntityProdduct))
                                    {
                                        _campaignMessengerService.InsertCampaignEntity(tmpCampaignEntityProdduct);
                                        numberEntity++;
                                    }
                                        
                                }
                            }
                        }
                        else if (interest.Type.ToLower().Equals(CampaignInterestType.Keyword.ToString().ToLower()))
                        {
                            string[] keywords = interest.Keyword.Trim().Split(',');
                            foreach(string kw in keywords)
                            {
                                var products = _productService.SearchProducts(keywords: kw);
                                foreach(var product in products)
                                {
                                    CampaignEntity tmpCampaignEntityProdduct = new CampaignEntity();

                                    tmpCampaignEntityProdduct.EntityName = CampaignEntityType.Product.ToString();
                                    tmpCampaignEntityProdduct.StatusId = (int)CampaignStatus.New;
                                    tmpCampaignEntityProdduct.EntityId = product.Id;
                                    tmpCampaignEntityProdduct.CampaignMessengerId = campaignMessenger.Id;

                                    if (!_campaignMessengerService.ExitCampaignEntity(tmpCampaignEntityProdduct))
                                    {
                                        _campaignMessengerService.InsertCampaignEntity(tmpCampaignEntityProdduct);
                                        numberEntity++;
                                    }
                                        
                                }
                            }
                        }
                    }

                    _campaignMessengerService.UpdateStatusCampaignMessenger(campaignMessenger.Id, CampaignStatus.ProcessingInterest);

                    _logger.Information(string.Format("Success Process Campaign Messenger - {0} - Entity {1}", campaignMessenger.Name, numberEntity));
                }
                else
                {
                    _logger.Information("Not Found Campaign Messenger");
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Process CampaignMessenger Task", ex);
            }
            
        }
    }
}
