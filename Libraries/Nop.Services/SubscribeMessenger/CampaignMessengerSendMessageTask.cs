﻿using Newtonsoft.Json;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.SubscribeMessenger;
using Nop.Services.Customers;
using Nop.Services.Logging;
using Nop.Services.Tasks;
using System;
using System.Linq;

namespace Nop.Services.SubscribeMessenger
{
    public partial class CampaignMessengerSendMessageTask : ITask
    {
        #region
        private ICampaignMessengerService _campaignMessengerService;
        private ISubscribeCustomerService _subscribeCustomerService;
        private ISubscribeMessageService _subscribeMessageService;
        private readonly ILogger _logger;
        #endregion
        
        public CampaignMessengerSendMessageTask(ICampaignMessengerService campaignMessengerService,
                                                ISubscribeCustomerService subscribeCustomerService,
                                                ISubscribeMessageService subscribeMessageService,
                                                ILogger logger)
        {
            _campaignMessengerService = campaignMessengerService;
            _subscribeCustomerService = subscribeCustomerService;
            _subscribeMessageService = subscribeMessageService;
            _logger = logger;
        }

        public void Execute()
        {
            try
            {
                CampaignMessenger campaignMessenger = _campaignMessengerService.GetFirstCampaignMessengerByStatus((int)CampaignStatus.Sending);
                if (campaignMessenger != null)
                {
                    var campaignSubscribeCustomers = _campaignMessengerService.GetCampaignSubscribeCustomersByCampaignMessengerId(campaignMessenger.Id, (int)CampaignStatus.New, 100);
                    if (campaignSubscribeCustomers.Count() > 0)
                    {
                        foreach(CampaignSubscribeCustomer camSubCus in campaignSubscribeCustomers)
                        {
                            SubscribeCustomer subscribeCustomer = _subscribeCustomerService.GetById(camSubCus.SubscribeCustomerId);
                            if (subscribeCustomer != null)
                            {
                                FbSendMessage fbImageMessage = new FbSendMessage();
                                FbSendMessage fbTextMessage = new FbSendMessage();

                                _campaignMessengerService.GenerateFbSendMessageFromCampaignMessengerId(camSubCus.CampaignMessengerId, out fbImageMessage, out fbTextMessage);
                                fbImageMessage.Recipient.UserRef = subscribeCustomer.UserRef;
                                fbTextMessage.Recipient.UserRef = subscribeCustomer.UserRef;

                                // add subscribe message

                                SubscribeMessage subscribeAttachmentMessage = new SubscribeMessage();
                                subscribeAttachmentMessage.SubscribeCustomerId = subscribeCustomer.Id;
                                subscribeAttachmentMessage.CreatedOnUtc = DateTime.UtcNow;
                                subscribeAttachmentMessage.SendDateUtc = DateTime.UtcNow;
                                subscribeAttachmentMessage.SendingStatusId = (int)SubscribeMesseageSendingStatus.New;
                                subscribeAttachmentMessage.MessagesJson = JsonConvert.SerializeObject(fbImageMessage);
                                _subscribeMessageService.Add(subscribeAttachmentMessage);

                                SubscribeMessage subscribeTextMessage = new SubscribeMessage();
                                subscribeTextMessage.SubscribeCustomerId = subscribeCustomer.Id;
                                subscribeTextMessage.CreatedOnUtc = DateTime.UtcNow;
                                subscribeTextMessage.SendDateUtc = DateTime.UtcNow;
                                subscribeTextMessage.SendingStatusId = (int)SubscribeMesseageSendingStatus.New;
                                subscribeTextMessage.MessagesJson = JsonConvert.SerializeObject(fbTextMessage);
                                _subscribeMessageService.Add(subscribeTextMessage);

                            }

                            _campaignMessengerService.UpdateCampaignSubscribeCustomerStatus(camSubCus.Id, (int)CampaignStatus.Completed);
                        }

                        _logger.Information("CampaignMessengerSendMessageTask - Send Subscribe Customer:  " + campaignSubscribeCustomers.Count());
                    }
                    else
                    {
                        // update campaign messenger status
                        campaignMessenger.StatusId = (int)CampaignStatus.Completed;
                        campaignMessenger.TotalAudience = _campaignMessengerService.GetTotalAudienceOfCampaignMessenger(campaignMessenger.Id);
                        _campaignMessengerService.UpdateCampaignMessenger(campaignMessenger);
                        _logger.Information("CampaignMessengerSendMessageTask - Completed Sending - CampaignMessenger: " + campaignMessenger.Name);
                    }
                }   
                else
                {
                    _logger.Information("CampaignMessengerSendMessageTask - Not Found Campaign Messenger");
                }
            }
            catch (Exception ex)
            {
                _logger.Error("CampaignMessengerSendMessageTask", ex);
            }
        }
    }
}
