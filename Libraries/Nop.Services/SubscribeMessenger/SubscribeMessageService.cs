﻿using Newtonsoft.Json;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.SubscribeMessenger;
using Nop.Services.Catalog;
using Nop.Services.Customers;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Seo;
using Nop.Services.Shipping;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nop.Services.SubscribeMessenger
{
    public partial class SubscribeMessageService : ISubscribeMessageService
    {
        #region Field
        private readonly IRepository<SubscribeMessage> _subscribeMessageRepository;
        private readonly IProductService _productService;
        private readonly IPictureService _pictureService;
        private readonly IStoreContext _storeContext;
        private readonly ISubscribeActivityLogService _subscribeActivityLogService;
        private readonly ISubscribeCustomerService _subscribeCustomerService;
        private readonly IShipmentService _shipmentService;
        private readonly ILogger _logger;
        #endregion

        #region Constructor
        public SubscribeMessageService(IRepository<SubscribeMessage> subscribeMessageRepository, 
                                       IProductService productService, IPictureService pictureSerice,
                                       IStoreContext storeContext, ILogger logger,
                                       ISubscribeActivityLogService subscribeActivityLogService,
                                       ISubscribeCustomerService subscribeCustomerService,
                                       IShipmentService shipmentService)
        {
            _subscribeMessageRepository = subscribeMessageRepository;
            _productService = productService;
            _pictureService = pictureSerice;
            _storeContext = storeContext;
            _logger = logger;
            _subscribeActivityLogService = subscribeActivityLogService;
            _subscribeCustomerService = subscribeCustomerService;
            _shipmentService = shipmentService;
        }

        public SubscribeMessageService()
        {
        }
        #endregion

        #region Method
        public List<SubscribeMessage> GetNotSendMessageByDate(DateTime date)
        {
            return _subscribeMessageRepository.Table.Where(sm => (sm.SendingStatusId == (int)SubscribeMesseageSendingStatus.New) && sm.SendDateUtc <= date).ToList();
        }

        /// <summary>
        /// Init first subscribe message when user subscribe in product detail
        /// Message will be send after 24h
        /// </summary>
        /// <param name="subscribeCustomer"></param>
        public void RemindViewProductFirstMessage(SubscribeCustomer subscribeCustomer)
        {
            if (subscribeCustomer.ProductId.HasValue)
            {
                RemindViewProduct(subscribeCustomer.Id, subscribeCustomer.ProductId.Value, 1);
            }
        }

        /// <summary>
        /// Sending Remind Message when customer checkout unsuccess
        /// </summary>
        /// <param name="orderId"></param>
        public void RemindPendingOrderMessage(int subscribeCustomerId, string orderGuid, int step)
        {
            
            // Send First Message
            SubscribeMessage subscribeTextMessage = new SubscribeMessage();
            subscribeTextMessage.SubscribeCustomerId = subscribeCustomerId;
            subscribeTextMessage.CreatedOnUtc = DateTime.UtcNow;
            subscribeTextMessage.SendDateUtc = DateTime.UtcNow.AddHours(6);
            subscribeTextMessage.SendingStatusId = (int)SubscribeMesseageSendingStatus.New;
            subscribeTextMessage.Step = step;
            subscribeTextMessage.SubscribeFollowId = (int)SubscribeFollowType.RemindPendingOrder;

            FbSendMessage fbTextMessage = new FbSendMessage();
            fbTextMessage.Message.Attachment.Type = FbAttachmentType.Template;
            fbTextMessage.Message.Attachment.Payload.TemplateType = FbPayloadTemplateType.Button;
            fbTextMessage.Message.Attachment.Payload.Text = "Hello,\nHow are you today?\nI’m checking our system and I realize your order was not sucessful.\nCould you tell us what happened?\nJust let me know if you have any problems!";
            fbTextMessage.Message.Attachment.Payload.Buttons = new List<FbButton>();
            fbTextMessage.Message.Attachment.Payload.Buttons.Add(new FbButton
            {
                Title = "Checkout (15% Off)",
                Type = FbButtonType.WebUrl,
                Url = string.Format("{0}yourcart?cartcode={1}&coupon=FRIEND15", _storeContext.CurrentStore.Url, orderGuid)
            });
            subscribeTextMessage.MessagesJson = JsonConvert.SerializeObject(fbTextMessage);
            _subscribeMessageRepository.Insert(subscribeTextMessage);
        }

        /// <summary>
        /// Remind Add2Cart Message
        /// </summary>
        /// <param name="subscribeCustomerId"></param>
        /// <param name="customerId"></param>
        /// <param name="step"></param>
        public void RemindAdd2CartMessage(int subscribeCustomerId, int customerId, int step)
        {
            // Send First Message
            SubscribeMessage subscribeTextMessage = new SubscribeMessage();
            subscribeTextMessage.SubscribeCustomerId = subscribeCustomerId;
            subscribeTextMessage.CreatedOnUtc = DateTime.UtcNow;
            subscribeTextMessage.SendDateUtc = DateTime.UtcNow.AddDays(1);
            subscribeTextMessage.SendingStatusId = (int)SubscribeMesseageSendingStatus.New;
            subscribeTextMessage.Step = step;
            subscribeTextMessage.SubscribeFollowId = (int)SubscribeFollowType.RemindAdd2Cart;

            FbSendMessage fbTextMessage = new FbSendMessage();
            fbTextMessage.Message.Attachment.Type = FbAttachmentType.Template;
            fbTextMessage.Message.Attachment.Payload.TemplateType = FbPayloadTemplateType.Button;
            fbTextMessage.Message.Attachment.Payload.Text = "Don't miss out!\nYour Cart expires soon\n@Freakers.name!";
            fbTextMessage.Message.Attachment.Payload.Buttons = new List<FbButton>();
            fbTextMessage.Message.Attachment.Payload.Buttons.Add(new FbButton
            {
                Title = "Checkout (15% Off)",
                Type = FbButtonType.WebUrl,
                Url = string.Format("{0}restorecart?customerId={1}&coupon=FRIEND15", _storeContext.CurrentStore.Url, customerId)
            });
            subscribeTextMessage.MessagesJson = JsonConvert.SerializeObject(fbTextMessage);
            _subscribeMessageRepository.Insert(subscribeTextMessage);
        }

        public void UpdateSubscribeMessage(SubscribeMessage message)
        {
            _subscribeMessageRepository.Update(message);
        }

        public void Add(SubscribeMessage message)
        {
            _subscribeMessageRepository.Insert(message);
        }

        public void ProcessingOrderMessage(int orderId)
        {
            SubscribeActivityLog subscribeActivityLog = _subscribeActivityLogService.GetActivityLogPlaceOrder(orderId);
            if (subscribeActivityLog != null)
            {
                SubscribeCustomer subscribeCustomer = _subscribeCustomerService.GetById(subscribeActivityLog.SubscribeCustomerId.Value);

                SubscribeMessage subscribeTextMessage = new SubscribeMessage();
                subscribeTextMessage.SubscribeCustomerId = subscribeCustomer.Id;
                subscribeTextMessage.CreatedOnUtc = DateTime.UtcNow;
                subscribeTextMessage.SendDateUtc = DateTime.UtcNow;
                subscribeTextMessage.SendingStatusId = (int)SubscribeMesseageSendingStatus.New;

                FbSendMessage fbTextMessage = new FbSendMessage();
                var product = _productService.GetProductById(subscribeCustomer.ProductId.Value);
                fbTextMessage.Message.Attachment.Type = FbAttachmentType.Template;
                fbTextMessage.Message.Attachment.Payload.TemplateType = FbPayloadTemplateType.Button;
                fbTextMessage.Message.Attachment.Payload.Text = "Dear ,\nWe'd like to inform that your order is in processing. It will be shipped out soonest.\nWe will update tracking info and send it to you asap.\nThank you !";
                fbTextMessage.Message.Attachment.Payload.Buttons = new List<FbButton>();
                fbTextMessage.Message.Attachment.Payload.Buttons.Add(new FbButton
                {
                    Title = "Shop Now!",
                    Type = FbButtonType.WebUrl,
                    Url = _storeContext.CurrentStore.Url
                });

                subscribeTextMessage.MessagesJson = JsonConvert.SerializeObject(fbTextMessage);
                _subscribeMessageRepository.Insert(subscribeTextMessage);
            }
        }

        public void TrackingOrderMessage(int orderId, string linkTracking)
        {
            SubscribeActivityLog subscribeActivityLog = _subscribeActivityLogService.GetActivityLogPlaceOrder(orderId);
            if (subscribeActivityLog != null)
            {
                SubscribeCustomer subscribeCustomer = _subscribeCustomerService.GetById(subscribeActivityLog.SubscribeCustomerId.Value);
              
                if (!string.IsNullOrEmpty(linkTracking))
                {
                    SubscribeMessage subscribeTextMessage = new SubscribeMessage();
                    subscribeTextMessage.SubscribeCustomerId = subscribeCustomer.Id;
                    subscribeTextMessage.CreatedOnUtc = DateTime.UtcNow;
                    subscribeTextMessage.SendDateUtc = DateTime.UtcNow;
                    subscribeTextMessage.SendingStatusId = (int)SubscribeMesseageSendingStatus.New;

                    FbSendMessage fbTextMessage = new FbSendMessage();
                    var product = _productService.GetProductById(subscribeCustomer.ProductId.Value);
                    fbTextMessage.Message.Attachment.Type = FbAttachmentType.Template;
                    fbTextMessage.Message.Attachment.Payload.TemplateType = FbPayloadTemplateType.Button;
                    fbTextMessage.Message.Attachment.Payload.Text = "Hello,\nYour order has been shipped.\nOnce again thank you so much and have a great day.\nPlease click to the link below and tracking your order, it would be updated day by day.";
                    fbTextMessage.Message.Attachment.Payload.Buttons = new List<FbButton>();
                    fbTextMessage.Message.Attachment.Payload.Buttons.Add(new FbButton
                    {
                        Title = "Track Here!",
                        Type = FbButtonType.WebUrl,
                        Url = linkTracking
                    });

                    subscribeTextMessage.MessagesJson = JsonConvert.SerializeObject(fbTextMessage);
                    _subscribeMessageRepository.Insert(subscribeTextMessage);
                }        
            }
        }

        public List<SubscribeMessage> GetMessageNotCompletedByFollow(SubscribeFollowType followType)
        {
            return _subscribeMessageRepository.Table.Where(sm=> sm.SubscribeFollowId == (int)followType && !(sm.IsCompletedFollow == true)).ToList();
        }

        public void RemindViewProduct(int subscribeId, int productId, int step)
        {
            var productPictures = _productService.GetProductPicturesByProductId(productId);
            if (productPictures.Count() > 0)
            {
                SubscribeMessage subscribeImageMessage = new SubscribeMessage();
                subscribeImageMessage.SubscribeCustomerId = subscribeId;
                subscribeImageMessage.CreatedOnUtc = DateTime.UtcNow;
                subscribeImageMessage.SendDateUtc = DateTime.UtcNow.AddDays(1);
                subscribeImageMessage.SendingStatusId = (int)SubscribeMesseageSendingStatus.New;
                subscribeImageMessage.Step = step;
                subscribeImageMessage.SubscribeFollowId = (int)SubscribeFollowType.RemindViewProduct;

                FbSendMessage fbImageMessage = new FbSendMessage();
                fbImageMessage.Message.Attachment.Type = FbAttachmentType.Image;
                fbImageMessage.Message.Attachment.Payload.Url = _pictureService.GetPictureUrl(productPictures[0].PictureId, 1000);
                fbImageMessage.Message.Attachment.Payload.IsReusable = true;


                subscribeImageMessage.MessagesJson = JsonConvert.SerializeObject(fbImageMessage);
                _subscribeMessageRepository.Insert(subscribeImageMessage);

            }

            // Send First Message
            SubscribeMessage subscribeTextMessage = new SubscribeMessage();
            subscribeTextMessage.SubscribeCustomerId = subscribeId;
            subscribeTextMessage.CreatedOnUtc = DateTime.UtcNow;
            subscribeTextMessage.SendDateUtc = DateTime.UtcNow.AddDays(1);
            subscribeTextMessage.SendingStatusId = (int)SubscribeMesseageSendingStatus.New;
            subscribeTextMessage.Step = step;
            subscribeTextMessage.SubscribeFollowId = (int)SubscribeFollowType.RemindViewProduct;

            FbSendMessage fbTextMessage = new FbSendMessage();
            var product = _productService.GetProductById(productId);
            fbTextMessage.Message.Attachment.Type = FbAttachmentType.Template;
            fbTextMessage.Message.Attachment.Payload.TemplateType = FbPayloadTemplateType.Button;
            fbTextMessage.Message.Attachment.Payload.Text = "ATTENTION!!! \nLAST CHANCE TO BUY!! \nCode: FRIEND15 15% OFF Sale on Sale ";
            fbTextMessage.Message.Attachment.Payload.Buttons = new List<FbButton>();
            fbTextMessage.Message.Attachment.Payload.Buttons.Add(new FbButton
            {
                Title = "Shop Now",
                Type = FbButtonType.WebUrl,
                Url = string.Format("{0}/{1}?coupon=FRIEND15", _storeContext.CurrentStore.Url, product.GetSeName())
            });
            subscribeTextMessage.MessagesJson = JsonConvert.SerializeObject(fbTextMessage);
            _subscribeMessageRepository.Insert(subscribeTextMessage);
        }

        public List<SubscribeMessage> GetMessageNotCompletedBySubscribeCustomerId(int subscribeCustomerId)
        {
            return _subscribeMessageRepository.Table.Where(sm => sm.SubscribeFollowId.HasValue && !(sm.IsCompletedFollow == true) && sm.SubscribeCustomerId == subscribeCustomerId).ToList();
        }
        #endregion
    }
}
