﻿using Nop.Core.Domain.SubscribeMessenger;
using Nop.Services.Customers;
using Nop.Services.Logging;
using Nop.Services.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nop.Services.SubscribeMessenger
{
    public partial class CampaignMessengerProcessAudienceTask : ITask
    {
        #region field
        private readonly ILogger _logger;
        private readonly ICampaignMessengerService _campaignMessengerService;
        private readonly ISubscribeActivityLogService _subscribeActivityLogService;
        #endregion

        public CampaignMessengerProcessAudienceTask(ILogger logger, 
                                                    ICampaignMessengerService campaignMessengerService,
                                                    ISubscribeActivityLogService subscribeActivityLogService)
        {
            _logger = logger;
            _campaignMessengerService = campaignMessengerService;
            _subscribeActivityLogService = subscribeActivityLogService;
        }
        public void Execute()
        {
            try
            {
                CampaignMessenger campaignMessenger = _campaignMessengerService.GetFirstCampaignMessengerByStatus((int)CampaignStatus.ProcessingInterest);
                if (campaignMessenger != null)
                {
                    var campaignEntitys =  _campaignMessengerService.GetCampaignEntityByCampaignMessengerId(campaignMessenger.Id, (int)CampaignStatus.New, 10);
                    if (campaignEntitys.Count() > 0)
                    {
                        List<int> audienceCampaignSubscribesId = new List<int>();

                        foreach(CampaignEntity camEntity in campaignEntitys)
                        {
                            audienceCampaignSubscribesId.AddRange(_subscribeActivityLogService.GetSubscribeCustomersIdByCampignEntity(camEntity));
                            audienceCampaignSubscribesId = audienceCampaignSubscribesId.Distinct().ToList();
                        }

                        //Add Campaign SubscribeCustomer
                        foreach(int subCusId in audienceCampaignSubscribesId)
                        {
                            if (!_campaignMessengerService.IsExitCampaignSubscribeCustomer(campaignMessenger.Id, subCusId) && _subscribeActivityLogService.CheckSubscribeCustomerByCampaignMessenger(subCusId, campaignMessenger))
                                _campaignMessengerService.AddCampaignSubscribeCustomer(new CampaignSubscribeCustomer
                                {
                                    CampaignMessengerId = campaignMessenger.Id,
                                    SubscribeCustomerId = subCusId,
                                    StatusId = (int)CampaignStatus.New
                                });
                        }

                        // update campaign entity status completed
                        foreach (CampaignEntity camEntity in campaignEntitys)
                        {
                            _campaignMessengerService.UpdateCampaignEntityStatus(camEntity.Id, (int)CampaignStatus.Completed);
                        }

                        _logger.Information("CampaignMessengerProcessAudienceTask - Processing Audience - Number Entity: " + campaignEntitys.Count());
                    }
                    else
                    {
                        // update status campaign messenger sending
                        _campaignMessengerService.UpdateStatusCampaignMessenger(campaignMessenger.Id, CampaignStatus.Sending);
                        _logger.Information("CampaignMessengerProcessAudienceTask - Completed Processing Audience - CampaignMessenger: " + campaignMessenger.Name);
                    }
                }
                else
                    _logger.Information("CampaignMessengerProcessAudienceTask - Not Found Campaign Messenger");
            }
            catch (Exception ex)
            {
                _logger.Error("Process CampaignMessenger Audience Task", ex);
            }
        }
    }
}