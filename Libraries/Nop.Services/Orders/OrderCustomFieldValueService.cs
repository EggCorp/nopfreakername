﻿using Nop.Core.Data;
using Nop.Core.Domain.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Orders
{
    public partial class OrderCustomFieldValueService: IOrderCustomFieldValueService
    {
        private readonly IRepository<OrderCustomFieldValue> _orderCustomFiledValueRepository;
        public OrderCustomFieldValueService(IRepository<OrderCustomFieldValue> orderCustomFiledValueRepository)
        {
            this._orderCustomFiledValueRepository = orderCustomFiledValueRepository;
        }

        public virtual void Insert(OrderCustomFieldValue item)
        {
            if (item != null)
                _orderCustomFiledValueRepository.Insert(item);

        }
        public virtual void Update(OrderCustomFieldValue item)
        {
            if (item != null)
                _orderCustomFiledValueRepository.Update(item);
        }
        public virtual void Delete(OrderCustomFieldValue item)
        {
            if (item != null)
                _orderCustomFiledValueRepository.Delete(item);
        }
        public virtual OrderCustomFieldValue GetById(int id)
        {
            return _orderCustomFiledValueRepository.GetById(id);
        }
        public virtual IList<OrderCustomFieldValue> GetAll()
        {
            return _orderCustomFiledValueRepository.Table.ToList();
        }
        public virtual OrderCustomFieldValue GetByOrderItemId(int id)
        {
            return _orderCustomFiledValueRepository.Table.Where(p => p.OrderItemId == id && !string.IsNullOrEmpty(p.FullFillSKU)).FirstOrDefault();
        }
    }
}
