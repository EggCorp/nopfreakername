﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Services.Events;
using Nop.Core.Domain.Common;
using System.Net;
using HtmlAgilityPack;
using Nop.Services.Media;
using Nop.Services.Catalog;
using System.Text.RegularExpressions;
using Nop.Services.Vendors;
using System.Xml;
using System.Globalization;
using Nop.Services.Common;
using Nop.Services.Helpers;

namespace Nop.Services.Orders
{
    public partial class OrderFullFilService :IOrderFullFillService
    {
        #region Fields

        private readonly IRepository<Order> _orderRepository;
        private readonly IRepository<OrderItem> _orderItemRepository;
        private readonly IRepository<OrderNote> _orderNoteRepository;
        private readonly IRepository<Product> _productRepository;
        private readonly IRepository<RecurringPayment> _recurringPaymentRepository;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IEventPublisher _eventPublisher;
        private readonly IRepository<Address> _addressRepository;
        private readonly IPictureService _pictureService;
        private readonly IProductAttributeParser _productAttributeParser;
        private readonly IProductAttributeFormatter _productAttributeFormatter;
        private readonly IOrderCustomFieldValueService _orderCustomFiledValueService;
        private readonly IProductCustomFieldService _productCustomFieldService;
        private readonly IVendorService _vendorService;
        private readonly IGoogleMapApiHelper _googleMapApiHelper;

        public const string LinkSunfrog = "https://www.sunfrog.com";

        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="orderRepository">Order repository</param>
        /// <param name="orderItemRepository">Order item repository</param>
        /// <param name="orderNoteRepository">Order note repository</param>
        /// <param name="productRepository">Product repository</param>
        /// <param name="recurringPaymentRepository">Recurring payment repository</param>
        /// <param name="customerRepository">Customer repository</param>
        /// <param name="eventPublisher">Event published</param>
        public OrderFullFilService(IRepository<Order> orderRepository,
            IRepository<OrderItem> orderItemRepository,
            IRepository<OrderNote> orderNoteRepository,
            IRepository<Product> productRepository,
            IRepository<RecurringPayment> recurringPaymentRepository,
            IRepository<Customer> customerRepository,
            IEventPublisher eventPublisher,
            IRepository<Address> addressReposity,
            IPictureService pictureService,
            IProductAttributeParser productAttributeParser,
            IProductAttributeFormatter productAttributeFormatter,
            IOrderCustomFieldValueService orderCustomFiledValueService,
            IProductCustomFieldService productCustomFieldService,
            IVendorService vendorService,
            IGoogleMapApiHelper googleMapApiHelper
             )
        {
            this._orderRepository = orderRepository;
            this._orderItemRepository = orderItemRepository;
            this._orderNoteRepository = orderNoteRepository;
            this._productRepository = productRepository;
            this._recurringPaymentRepository = recurringPaymentRepository;
            this._customerRepository = customerRepository;
            this._eventPublisher = eventPublisher;
            this._addressRepository = addressReposity;
            this._pictureService = pictureService;
            this._productAttributeParser = productAttributeParser;
            this._productAttributeFormatter = productAttributeFormatter;
            this._orderCustomFiledValueService = orderCustomFiledValueService;
            this._productCustomFieldService = productCustomFieldService;
            this._vendorService = vendorService;
            this._googleMapApiHelper = googleMapApiHelper;

        }

        #endregion

        #region Method
        public static string GetImageOfSunfrogShirt(string link)
        {
            string result = string.Empty;

            try
            {
                HtmlAgilityPack.HtmlDocument htmlDoc = new HtmlAgilityPack.HtmlDocument();
                htmlDoc.OptionFixNestedTags = true;
                HttpWebRequest request = HttpWebRequest.Create(link) as HttpWebRequest;
                request.Method = "GET";

                /* Sart browser signature */
                request.UserAgent = "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:31.0) Gecko/20100101 Firefox/31.0";
                request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
                request.Headers.Add(HttpRequestHeader.AcceptLanguage, "en-us,en;q=0.5");
                /* Sart browser signature */


                WebResponse response = request.GetResponse();
                HtmlNode.ElementsFlags.Remove("option");
                htmlDoc.Load(response.GetResponseStream(), true);


                if (htmlDoc.DocumentNode != null)
                {
                    HtmlNode priceValElement = htmlDoc.DocumentNode.SelectSingleNode("//*[@id=\"shirtTypes\"]/option[@selected]");
                    HtmlNode imageElement = htmlDoc.DocumentNode.SelectSingleNode("//*[@id=\"MainImgShow\"]");
                    result = imageElement.GetAttributeValue("src", string.Empty);

                    result = string.Format("https:{0}", result);
                }
                else
                    throw new Exception("Can not load html document!");
            }
            catch (Exception ex)
            {
                result = string.Empty;
            }

            return result;
        }

        public static string GetSkuOfSunfrogShirt(string link)
        {
            string result = string.Empty;

            try
            {
                HtmlAgilityPack.HtmlDocument htmlDoc = new HtmlAgilityPack.HtmlDocument();
                htmlDoc.OptionFixNestedTags = true;
                HttpWebRequest request = HttpWebRequest.Create(link) as HttpWebRequest;
                request.Method = "GET";

                /* Sart browser signature */
                request.UserAgent = "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:31.0) Gecko/20100101 Firefox/31.0";
                request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
                request.Headers.Add(HttpRequestHeader.AcceptLanguage, "en-us,en;q=0.5");
                /* Sart browser signature */


                WebResponse response = request.GetResponse();
                HtmlNode.ElementsFlags.Remove("option");
                htmlDoc.Load(response.GetResponseStream(), true);


                if (htmlDoc.DocumentNode != null)
                {
                    HtmlNode shirtSKUElement = htmlDoc.DocumentNode.SelectSingleNode("/html/body/div[6]/div[2]/div[2]/div[1]/div/div/small");
                    if(!string.IsNullOrEmpty(shirtSKUElement.InnerText))
                        result = RemoveHtmlTags(shirtSKUElement.InnerText).Replace("Product SKU:", string.Empty).Trim();

                }
                else
                    throw new Exception("Can not load html document!");
            }
            catch (Exception ex)
            {
                result = string.Empty;
            }

            return result;
        }

        public static string RemoveHtmlTags(string strHtml)
        {
            string strText = Regex.Replace(strHtml, "<(.|\n)*?>", String.Empty);
            strText = Regex.Replace(strText, @"\s+", " ");
            return strText;
        }

        public static string SunfrogConvertShirtType(string s)
        {
            string result = string.Empty;
            var index = s.IndexOf('$');
            if (index > 2)
                result = s.Substring(0, index - 1);
            return result;
        }

        protected string CheckIsMatchProductOrGetProductSku(OrderFullFillItem orderFullFillItem)
        {
            var result = string.Empty;
            string currentImageUrl = GetImageOfSunfrogShirt(orderFullFillItem.OriginUrl);
            if (currentImageUrl.Equals(orderFullFillItem.ImageUrl))
                return GetSkuOfSunfrogShirt(orderFullFillItem.OriginUrl);


            else
                return CompareProductImageUrl(orderFullFillItem);
        }

        protected string CompareProductImageUrl(OrderFullFillItem orderFullFillItem)
        {
            string result = string.Empty;
            try
            {
                HtmlAgilityPack.HtmlDocument htmlDoc = new HtmlAgilityPack.HtmlDocument();
                htmlDoc.OptionFixNestedTags = true;
                HttpWebRequest request = HttpWebRequest.Create(orderFullFillItem.OriginUrl) as HttpWebRequest;
                request.Method = "GET";

                /* Sart browser signature */
                request.UserAgent = "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:31.0) Gecko/20100101 Firefox/31.0";
                request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
                request.Headers.Add(HttpRequestHeader.AcceptLanguage, "en-us,en;q=0.5");
                /* Sart browser signature */


                WebResponse response = request.GetResponse();
                HtmlNode.ElementsFlags.Remove("option");
                htmlDoc.Load(response.GetResponseStream(), true);


                if (htmlDoc.DocumentNode != null)
                {
                    // get shirt type
                    HtmlNodeCollection shirtTypeNodes = htmlDoc.DocumentNode.SelectNodes("//*[@id=\"shirtTypes\"]/option");
                    List<SunfrogTShirtInfo> listShirt = new List<SunfrogTShirtInfo>();
                    foreach (var shirtType in shirtTypeNodes)
                    {
                        string tmpType = SunfrogConvertShirtType(shirtType.InnerText);
                        string tmpLink = shirtType.GetAttributeValue("value", string.Empty);
                        if (!string.IsNullOrEmpty(tmpLink))
                        {
                            if (tmpLink.IndexOf('?') > 0)
                                tmpLink = tmpLink.Substring(0, tmpLink.IndexOf('?'));
                            string typeLink = string.Format("{0}{1}", LinkSunfrog, tmpLink);

                            //get color of type
                            var shirtInfo = GetAllColorOfProductType(typeLink);
                            shirtInfo.ProductTypeLink = typeLink;
                            shirtInfo.ProductType = tmpType;
                            listShirt.Add(shirtInfo);
                        }
                    }

                    var matchProductWithColorAndType = listShirt.Where(p => p.ProductType.Equals(orderFullFillItem.ProductType)).FirstOrDefault();
                    if(matchProductWithColorAndType!=null)
                    {
                        var tmpShirtInfoWithMatchIamgeLink = matchProductWithColorAndType.ColorsOfType.Where(p => p.ColorImageUrl.Equals(orderFullFillItem.ImageUrl)).FirstOrDefault();
                        if (tmpShirtInfoWithMatchIamgeLink != null)
                        {
                            result = GetSkuOfSunfrogShirt(tmpShirtInfoWithMatchIamgeLink.ColorUrl);
                            return result;
                        }
                        //foreach (var colorItem in matchProductWithColorAndType.ColorsOfType)
                        //{
                        //    if(colorItem.ColorName.Equals(orderFullFillItem.Color))
                        //    {
                        //        result = GetSkuOfSunfrogShirt(colorItem.ColorUrl);
                        //        return result;
                        //    }
                        //}
                    }
                    else
                    {
                        foreach (var shirt in listShirt)
                        {
                            foreach (var color in shirt.ColorsOfType)
                            {
                                //var colorImageUrl = GetImageOfSunfrogShirt(color.ColorUrl);
                                if (color.ColorImageUrl.Equals(orderFullFillItem.ImageUrl))
                                {
                                    result = GetSkuOfSunfrogShirt(color.ColorUrl);
                                    break;
                                }
                            }
                        }
                    }
                   
                }
                else
                    throw new Exception("Can not load html document!");
            }
            catch (Exception ex)
            {
                
            }

            return result;
        }

        public static SunfrogTShirtInfo GetAllColorOfProductType(string linkShirtType)
        {
            SunfrogTShirtInfo result = new SunfrogTShirtInfo();
            result.ColorsOfType = new List<EggColor>();
            try
            {
                HtmlAgilityPack.HtmlDocument htmlDoc = new HtmlAgilityPack.HtmlDocument();
                htmlDoc.OptionFixNestedTags = true;
                HttpWebRequest request = HttpWebRequest.Create(linkShirtType) as HttpWebRequest;
                request.Method = "GET";

                /* Sart browser signature */
                request.UserAgent = "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:31.0) Gecko/20100101 Firefox/31.0";
                request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
                request.Headers.Add(HttpRequestHeader.AcceptLanguage, "en-us,en;q=0.5");
                /* Sart browser signature */

                Console.WriteLine(request.RequestUri.AbsoluteUri);
                WebResponse response = request.GetResponse();
                HtmlNode.ElementsFlags.Remove("option");
                htmlDoc.Load(response.GetResponseStream(), true);

                if (htmlDoc.DocumentNode != null)
                {
                    var btnGroupNodes = htmlDoc.DocumentNode.SelectSingleNode("//div[@class=\"btn-group\"]");
                    if (btnGroupNodes != null)
                    {
                        HtmlNodeCollection shirtColorNodes = btnGroupNodes.SelectNodes("label");
                        foreach (var shirtColor in shirtColorNodes)
                        {

                            string tmpLink = shirtColor.GetAttributeValue("onclick", string.Empty).ToString();
                            tmpLink = tmpLink.Replace("window.location='", string.Empty);
                            tmpLink = tmpLink.Replace("';", string.Empty);
                            if (tmpLink.IndexOf('?') > 0)
                                tmpLink = tmpLink.Substring(0, tmpLink.IndexOf('?'));
                            tmpLink = string.Format("{0}/{1}", LinkSunfrog, tmpLink);

                            string tmpColor = shirtColor.GetAttributeValue("title", string.Empty).ToString();
                          
                            result.ColorsOfType.Add(new EggColor { ColorName = tmpColor, ColorImageUrl = GetImageOfSunfrogShirt(tmpLink) ,ColorUrl=tmpLink});
                        }
                    }
                    else
                    {
                        // only 1 color
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return result;
        }
        ////
        public virtual IList<GroupedExportFullFullModel> PrepareExportFullFillData(IList<Order> orders)
        {
            List<ExportFullFillModel> result = new List<ExportFullFillModel>();
            var allCustomField = _productCustomFieldService.GetAllProductCustomFields();
            foreach( var order in orders)
            {
                ExportFullFillModel exportItem = new ExportFullFillModel();
                exportItem.OrderId = order.Id;
                exportItem.ShippingAddress = order.ShippingAddress;
                exportItem.OrderShippingPrice = order.OrderShippingExclTax;

                foreach (var item in order.OrderItems)
                {
                    var orderFullFillItem = _productAttributeParser.ParseOrderFullFillItem(item.AttributesXml, allCustomField.ToList());
                    orderFullFillItem.Quantity = item.Quantity;
                    var orderCustomfieldValue = _orderCustomFiledValueService.GetByOrderItemId(item.Id);
                    if (orderCustomfieldValue != null)
                    {
                        orderFullFillItem.ProductSKU = orderCustomfieldValue.FullFillSKU;
                        orderFullFillItem.OriginUrl = orderCustomfieldValue.FullFillProductUrl;
                    }
                    var vendor = _vendorService.GetVendorById(item.Product.VendorId);
                    if (vendor != null)
                        orderFullFillItem.VendorName = vendor.Name;

                    exportItem.Items.Add(orderFullFillItem);
                }
                result.Add(exportItem);
            }
            return GroupFullFillDataByShippingAddress(result);
        }

        protected virtual IList<GroupedExportFullFullModel> GroupFullFillDataByShippingAddress(IList<ExportFullFillModel> exports)
        {
            var result = new List<GroupedExportFullFullModel>();
            if (exports.Any())
            {
                for (int index = 0; index < exports.Count; index++)
                {
                    var item = exports[index];
                    GroupedExportFullFullModel model = new GroupedExportFullFullModel();
                    if(!result.Any())
                    {
                        model.GroupedAddress = item.ShippingAddress;
                        model.ExportFullFillModels.Add(item);
                        result.Add(model);
                    }
                    else
                    {
                        //check same shipping address
                        try
                        {

                            var checkItems = result.Where(p => p.GroupedAddress.LastName.Equals(item.ShippingAddress.LastName));
                            checkItems = checkItems.Where(p => p.GroupedAddress.FirstName.Equals(item.ShippingAddress.FirstName));
                            checkItems = checkItems.Where(p => p.GroupedAddress.ZipPostalCode.Equals(item.ShippingAddress.ZipPostalCode));
                            checkItems = checkItems.Where(p => p.GroupedAddress.City.Equals(item.ShippingAddress.City));
                            checkItems = checkItems.Where(p => p.GroupedAddress.Address1.Equals(item.ShippingAddress.Address1));
                            checkItems = checkItems.Where(p => p.GroupedAddress.Email.Equals(item.ShippingAddress.Email));
                            checkItems = checkItems.Where(p => p.GroupedAddress.CountryId == item.ShippingAddress.CountryId);

                            var itemExisted = checkItems.FirstOrDefault();
                            if (itemExisted != null)
                            {
                                var currentIndex = result.IndexOf(itemExisted);
                                result[currentIndex].ExportFullFillModels.Add(item);
                            }
                            else
                            {
                                model.GroupedAddress = item.ShippingAddress;
                                model.ExportFullFillModels.Add(item);
                                result.Add(model);
                            }
                        }
                        catch(Exception ex)
                        {

                        }
                    }
                }
            }
            return ValidateFullFillDataAddress(result);
        }

        protected virtual IList<GroupedExportFullFullModel> ValidateFullFillDataAddress(IList<GroupedExportFullFullModel> datas)
        {
            foreach (var item in datas)
            {
                try
                {
                    Address orderAddress = item.GroupedAddress;
                    GoogleGeoCodeAddress gmapAddress = new GoogleGeoCodeAddress
                    {
                        City = orderAddress.City,
                        Country = orderAddress.Country != null? orderAddress.Country.Name:"",
                        Line1 = orderAddress.Address1,
                        Line2 = orderAddress.Address2,
                        State = orderAddress.StateProvince!=null? orderAddress.StateProvince.Name:"",
                        Zip = orderAddress.ZipPostalCode
                    };
                    //validate address
                    gmapAddress = _googleMapApiHelper.ValidateAddress(gmapAddress);
                    if (gmapAddress.IsValid)
                    {
                        item.IsValidAddress = true;
                    }
                }
                catch (Exception ex)
                {
                }
            }
            return datas;
        }
        #endregion
    }
}
