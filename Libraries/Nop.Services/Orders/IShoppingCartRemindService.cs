﻿using Nop.Core.Domain.Orders;
using System.Collections.Generic;

namespace Nop.Services.Orders
{
    public partial interface IShoppingCartRemindService
    {
        void Add(ShoppingCartRemind model);

        void Update(ShoppingCartRemind model);

        /// <summary>
        /// Get remind message not completed
        /// </summary>
        /// <param name="numberMessage"></param>
        /// <returns></returns>
        List<ShoppingCartRemind> GetByNumberMessage(int numberMessage);

        void AddFirstShoppingCartRemind(int customerId);
    }
}
