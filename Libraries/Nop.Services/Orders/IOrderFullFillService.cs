﻿using Nop.Core.Domain.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Orders
{
    public partial interface IOrderFullFillService
    {

        #region Orders Full Fill

        IList<GroupedExportFullFullModel> PrepareExportFullFillData(IList<Order> orders);
      
        #endregion
    }
}
