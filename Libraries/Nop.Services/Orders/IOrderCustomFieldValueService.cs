﻿using Nop.Core.Domain.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Orders
{
    public partial interface IOrderCustomFieldValueService
    {
        void Insert(OrderCustomFieldValue item);
        void Update(OrderCustomFieldValue item);
        void Delete(OrderCustomFieldValue item);
        OrderCustomFieldValue GetById(int id);
        IList<OrderCustomFieldValue> GetAll();
        OrderCustomFieldValue GetByOrderItemId(int id);

    }
}
