﻿using Nop.Core.Data;
using Nop.Core.Domain.Orders;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nop.Services.Orders
{
    public partial class ShoppingCartRemindService : IShoppingCartRemindService
    {

        private IRepository<ShoppingCartRemind> _shoppingCartRemindRepository;

        public ShoppingCartRemindService(IRepository<ShoppingCartRemind> shoppingCartRemindRepository)
        {
            _shoppingCartRemindRepository = shoppingCartRemindRepository;
        }


        public void Add(ShoppingCartRemind model)
        {
            model.CreatedOnUtc = DateTime.UtcNow;
            _shoppingCartRemindRepository.Insert(model);
        }

        public void AddFirstShoppingCartRemind(int customerId)
        {
            DateTime compairDateTime = DateTime.UtcNow.AddMinutes(-30);
            var exitRemindShoppingCart = _shoppingCartRemindRepository.Table.Where(scr => scr.CustomerId == customerId && scr.CreatedOnUtc >= compairDateTime).FirstOrDefault();
            if (exitRemindShoppingCart == null)
            {
                ShoppingCartRemind remindShoppingCart = new ShoppingCartRemind();
                remindShoppingCart.CustomerId = customerId;
                remindShoppingCart.NumberMessage = 0;
                remindShoppingCart.Status = (int) OrderStatus.Processing;
                remindShoppingCart.CreatedOnUtc = DateTime.UtcNow;
                _shoppingCartRemindRepository.Insert(remindShoppingCart);
            }
        }

        public List<ShoppingCartRemind> GetByNumberMessage(int numberMessage)
        {
            return _shoppingCartRemindRepository.Table.Where(sc => sc.NumberMessage == 0 && sc.Status == (int)OrderStatus.Processing ).ToList();
        }

        public void Update(ShoppingCartRemind model)
        {
            _shoppingCartRemindRepository.Update(model);
        }
    }
}
