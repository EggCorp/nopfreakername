﻿using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Services.Customers;
using Nop.Services.Logging;
using Nop.Services.Messages;
using Nop.Services.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nop.Services.Orders
{
    public partial class RemindShoppingCartFirstTimeTask : ITask
    {
        #region Field
        private readonly IShoppingCartRemindService _shoppingCartRemindService;
        private readonly ICustomerService _customerService;
        private readonly ILogger _logger;
        private readonly IEggReMarketingEmailService _eggReMarketingEmailService;
        private readonly IOrderService _orderService;
        #endregion
        public RemindShoppingCartFirstTimeTask(IShoppingCartRemindService shoppingCartRemindService, ICustomerService customerService, 
                                               ILogger logger, IEggReMarketingEmailService eggReMarketingEmailService, IOrderService orderService)
        {
            _shoppingCartRemindService = shoppingCartRemindService;
            _customerService = customerService;
            _logger = logger;
            _eggReMarketingEmailService = eggReMarketingEmailService;
            _orderService = orderService;
        }
        public void Execute()
        {
            DateTime compairTime = DateTime.UtcNow.AddMinutes(-15);

            List<ShoppingCartRemind> shoppingCartReminds = _shoppingCartRemindService.GetByNumberMessage(0).Where(scr => scr.CreatedOnUtc <= compairTime).ToList();

            foreach(ShoppingCartRemind shoppingCartRemind in shoppingCartReminds)
            {
                try
                {
                    Customer customer = _customerService.GetCustomerById(shoppingCartRemind.CustomerId);
                    var orders = _orderService.GetOrdersByEmail(customer.BillingAddress.Email);
                    List<Order> validatedOrder = new List<Order>();
                    if (orders.Count() > 0)
                    {
                        validatedOrder = orders.Where(ord => ord.CreatedOnUtc > shoppingCartRemind.CreatedOnUtc).ToList();
                    }

                    if (validatedOrder.Count() > 0) // not send remind message
                    {
                        shoppingCartRemind.Status = (int)OrderStatus.Complete;
                        _shoppingCartRemindService.Update(shoppingCartRemind);
                        _logger.Information("Remind Shopping Cart: Completed Order - Customer Email: " + customer.BillingAddress.Email);
                    }
                    else
                    {
                        _eggReMarketingEmailService.SendRemindShoppingCartEmail(shoppingCartRemind.CustomerId);
                        shoppingCartRemind.NumberMessage = 1;
                        _shoppingCartRemindService.Update(shoppingCartRemind);
                        _logger.Information("Remind Shopping Cart: First Email - Customer Email: " + customer.BillingAddress.Email);
                    }
                }
                catch(Exception ex)
                {
                    _logger.Error("Remind Shopping Cart - CustomerID: " + shoppingCartRemind.CustomerId, ex);
                }
                    
            }
            
        }
    }
}
