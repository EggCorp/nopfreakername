using Autofac;
using Autofac.Core;
using Nop.Core.Configuration;
using Nop.Core.Data;
using Nop.Core.Infrastructure;
using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Data;
using Nop.Plugin.Shipping.ByCountry.Data;
using Nop.Plugin.Shipping.ByCountry.Domain;
using Nop.Plugin.Shipping.ByCountry.Services;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.Shipping.ByCountry
{
    /// <summary>
    /// Dependency registrar
    /// </summary>
    public class DependencyRegistrar : IDependencyRegistrar
    {
        /// <summary>
        /// Register services and interfaces
        /// </summary>
        /// <param name="builder">Container builder</param>
        /// <param name="typeFinder">Type finder</param>
        /// <param name="config">Config</param>
        public virtual void Register(ContainerBuilder builder, ITypeFinder typeFinder, NopConfig config)
        {
            builder.RegisterType<ShippingByCountryService>().As<IShippingByCountryService>().InstancePerLifetimeScope();

            //data context
            this.RegisterPluginDataContext<ShippingByContryObjectContext>(builder, "nop_object_context_shipping_country_zip");

            //override required repository with our custom context
            builder.RegisterType<EfRepository<ShippingByCountryRecord>>()
                .As<IRepository<ShippingByCountryRecord>>()
                .WithParameter(ResolvedParameter.ForNamed<IDbContext>("nop_object_context_shipping_country_zip"))
                .InstancePerLifetimeScope();

            builder.RegisterType<EfRepository<ShippingByCountryMethod>>()
                .As<IRepository<ShippingByCountryMethod>>()
                .WithParameter(ResolvedParameter.ForNamed<IDbContext>("nop_object_context_shipping_country_zip"))
                .InstancePerLifetimeScope();

            builder.RegisterType<EfRepository<ShippingByCountrySpecial>>()
                .As<IRepository<ShippingByCountrySpecial>>()
                .WithParameter(ResolvedParameter.ForNamed<IDbContext>("nop_object_context_shipping_country_zip"))
                .InstancePerLifetimeScope();
        }

        /// <summary>
        /// Order of this dependency registrar implementation
        /// </summary>
        public int Order
        {
            get { return 1; }
        }
    }
}
