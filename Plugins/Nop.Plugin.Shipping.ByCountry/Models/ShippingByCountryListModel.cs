﻿using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.Shipping.ByCountry.Models
{
    public class ShippingByCountryListModel : BaseNopModel
    {
        [NopResourceDisplayName("Plugins.Shipping.ByCountry.Fields.LimitMethodsToCreated")]
        public bool LimitMethodsToCreated { get; set; }
    }
}