﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Shipping.ByCountry.Models
{
    public class ProductTypes
    {
        public const string BellaFlowyTank = "Bella Flowy Tank";
        public const string GildanSleevelessTee = "Gildan Sleeveless Tee";
        public const string GuysTee = "Guys Tee";
        public const string GuysVNeckTee = "Guys V-Neck";
        public const string Hoodie = "Hoodie";
        public const string InfantOnesie = "Infant Onesie";
        public const string LadiesTee = "Ladies Tee";
        public const string LadiesVNeckTee = "Ladies V-Neck";
        public const string SweatShirt = "Sweat Shirt";
        public const string UnisexLongSleeve = "Unisex Long Sleeve";
        public const string UnisexTank = "Unisex Tank Top";
        public const string YouthTee = "Youth Tee";
    }
}
