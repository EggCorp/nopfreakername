﻿using System.Collections.Generic;
using System.Web.Mvc;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.Shipping.ByCountry.Models
{
    public class ShippingByCountryModel : BaseNopEntityModel
    {
        public ShippingByCountryModel()
        {
            AvailableCountries = new List<SelectListItem>();
            AvailableStates = new List<SelectListItem>();
            AvailableShippingMethods = new List<SelectListItem>();
            AvailableStores = new List<SelectListItem>();
            AvailableWarehouses = new List<SelectListItem>();
        }

        [NopResourceDisplayName("Plugins.Shipping.ByCountry.Fields.Store")]
        public int StoreId { get; set; }
        [NopResourceDisplayName("Plugins.Shipping.ByCountry.Fields.Store")]
        public string StoreName { get; set; }

        [NopResourceDisplayName("Product Type")]
        public string ProductType { get; set; }

        [NopResourceDisplayName("Product Code")]
        public string ProductCode { get; set; }

        [NopResourceDisplayName("USShipping Price")]
        public decimal USShippingPrice { get; set; }

        [NopResourceDisplayName("InternationalShipping Price")]
        public decimal InternationalShippingPrice { get; set; }

        [NopResourceDisplayName("USAddedItem Price")]
        public decimal USAddedItemPrice { get; set; }

        [NopResourceDisplayName("InternationalAddedItem Price")]
        public decimal InternationalAddedItemPrice { get; set; }
        [NopResourceDisplayName("Plugins.Shipping.ByCountry.Fields.Order")]
        public int Order { get; set; }

        [NopResourceDisplayName("Plugins.Shipping.ByCountry.Fields.DataHtml")]
        public string DataHtml { get; set; }

        public string PrimaryStoreCurrencyCode { get; set; }



        public IList<SelectListItem> AvailableCountries { get; set; }
        public IList<SelectListItem> AvailableStates { get; set; }
        public IList<SelectListItem> AvailableShippingMethods { get; set; }
        public IList<SelectListItem> AvailableStores { get; set; }
        public IList<SelectListItem> AvailableWarehouses { get; set; }
    }
}