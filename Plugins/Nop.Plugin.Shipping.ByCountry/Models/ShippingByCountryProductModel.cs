﻿namespace Nop.Plugin.Shipping.ByCountry.Models
{
    public class ShippingByCountryProductModel
    {
        public string ProductType { get; set; }
        public int ProductCount { get; set; }
        public decimal USShippingPrice { get; set; }
        public decimal InternationalShippingPrice { get; set; }
        public decimal USAddedItemPrice { get; set; }
        public decimal InternationalAddedItemPrice { get; set; }
        public int MaxItemPerBox { get; set; } = 999;
        public int CategoryId { get; set; }
    }
}
