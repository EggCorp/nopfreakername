﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Shipping.ByCountry.Models
{
    public class ProductAttributeValueModel
    {
        public string ProductLink { get; set; }
        public string ProductType { get; set; }
        public int ColorId { get; set; }
        public int SizeId { get; set; }
    }
}
