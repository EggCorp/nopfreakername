﻿using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Nop.Plugin.Shipping.ByCountry.Models
{
    public class ShippingByCountryMethodModel : BaseNopEntityModel
    {
        public ShippingByCountryMethodModel()
        {
            AvailableShippingMethods = new List<SelectListItem>();
            AvailableShippingRecords = new List<SelectListItem>();
        }
        public int ShippingMethodId { get; set; }
        public string ShippingMethodName { get; set; }
        public int ShippingByCountryId { get; set; }
        public string ShippingByCountryName { get; set; }
        public decimal ShippingFee { get; set; }
        public int MinShipDate { get; set; }
        public int MaxShipDate { get; set; }
        public string Description { get; set; }
        public int MaxItemsPerBox { get; set; }
        public decimal AddedItemPrice { get; set; }

        public IList<SelectListItem> AvailableShippingMethods { get; set; }
        public IList<SelectListItem> AvailableShippingRecords { get; set; }
    }
}
