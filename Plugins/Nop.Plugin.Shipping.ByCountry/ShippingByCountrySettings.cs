﻿
using Nop.Core.Configuration;

namespace Nop.Plugin.Shipping.ByCountry
{
    public class ShippingByCountrySettings : ISettings
    {
        public bool LimitMethodsToCreated { get; set; }
    }
}