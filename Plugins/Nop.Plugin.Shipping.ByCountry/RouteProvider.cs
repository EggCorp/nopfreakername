﻿using System.Web.Mvc;
using System.Web.Routing;
using Nop.Web.Framework.Mvc.Routes;

namespace Nop.Plugin.Shipping.ByCountry
{
    public partial class RouteProvider : IRouteProvider
    {
        public void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute("Plugin.Shipping.ByCountry.SaveGeneralSettings",
                 "Plugins/ShippingByCountry/SaveGeneralSettings",
                 new { controller = "ShippingByCountry", action = "SaveGeneralSettings", },
                 new[] { "Nop.Plugin.Shipping.ByCountry.Controllers" }
            );

            routes.MapRoute("Plugin.Shipping.ByCountry.AddPopup",
                 "Plugins/ShippingByCountry/AddPopup",
                 new { controller = "ShippingByCountry", action = "AddPopup" },
                 new[] { "Nop.Plugin.Shipping.ByCountry.Controllers" }
            );
            routes.MapRoute("Plugin.Shipping.ByCountry.EditPopup",
                 "Plugins/ShippingByCountry/EditPopup",
                 new { controller = "ShippingByCountry", action = "EditPopup" },
                 new[] { "Nop.Plugin.Shipping.ByCountry.Controllers" }
            );
            routes.MapRoute("Plugin.Shipping.ByCountry.AddMethodConfiguration",
               "Plugins/ShippingByCountry/AddMethodConfiguration",
               new { controller = "ShippingByCountry", action = "AddMethodConfiguration" },
               new[] { "Nop.Plugin.Shipping.ByCountry.Controllers" }
          );
            routes.MapRoute("Plugin.Shipping.ByCountry.EditMethodConfiguration",
             "Plugins/ShippingByCountry/EditMethodConfiguration",
             new { controller = "ShippingByCountry", action = "EditMethodConfiguration" },
             new[] { "Nop.Plugin.Shipping.ByCountry.Controllers" }
        );
            routes.MapRoute("Plugin.Shipping.ByCountry.ShippingByCountryMethodConfiguration",
                           "Plugins/ShippingByCountry/MethodConfiguration",
                           new { controller = "ShippingByCountry", action = "ShippingByCountryMethodConfiguration" },
                           new[] { "Nop.Plugin.Shipping.ByCountry.Controllers" }
            );
            routes.MapRoute("Plugin.Shipping.ByCountry.DeleteMethodConfiguration",
                          "Plugins/ShippingByCountry/DeleteMethodConfiguration",
                          new { controller = "ShippingByCountry", action = "DeleteMethodConfiguration" },
                          new[] { "Nop.Plugin.Shipping.ByCountry.Controllers" }
           );
        }
        public int Priority
        {
            get
            {
                return 0;
            }
        }
    }
}
