﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Domain.Directory;
using Nop.Plugin.Shipping.ByCountry.Domain;
using Nop.Plugin.Shipping.ByCountry.Models;
using Nop.Plugin.Shipping.ByCountry.Services;
using Nop.Services.Configuration;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Services.Security;
using Nop.Services.Shipping;
using Nop.Services.Stores;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Kendoui;
using Nop.Web.Framework.Mvc;
using Nop.Web.Framework.Security;

namespace Nop.Plugin.Shipping.ByCountry.Controllers
{
    [AdminAuthorize]
    public class ShippingByCountryController : BasePluginController
    {
        private readonly IShippingService _shippingService;
        private readonly IStoreService _storeService;
        private readonly ICountryService _countryService;
        private readonly IStateProvinceService _stateProvinceService;
        private readonly ShippingByCountrySettings _shippingByCountrySettings;
        private readonly IShippingByCountryService _shippingByCountryService;
        private readonly ISettingService _settingService;
        private readonly ILocalizationService _localizationService;
        private readonly IPermissionService _permissionService;

        private readonly ICurrencyService _currencyService;
        private readonly CurrencySettings _currencySettings;
        private readonly IMeasureService _measureService;
        private readonly MeasureSettings _measureSettings;

        public ShippingByCountryController(IShippingService shippingService,
            IStoreService storeService,
            ICountryService countryService,
            IStateProvinceService stateProvinceService,
            ShippingByCountrySettings shippingByCountrySettings,
            IShippingByCountryService shippingByCountryService,
            ISettingService settingService,
            ILocalizationService localizationService,
            IPermissionService permissionService,
            ICurrencyService currencyService,
            CurrencySettings currencySettings,
            IMeasureService measureService,
            MeasureSettings measureSettings)
        {
            this._shippingService = shippingService;
            this._storeService = storeService;
            this._countryService = countryService;
            this._stateProvinceService = stateProvinceService;
            this._shippingByCountrySettings = shippingByCountrySettings;
            this._shippingByCountryService = shippingByCountryService;
            this._settingService = settingService;
            this._localizationService = localizationService;
            this._permissionService = permissionService;
            this._currencyService = currencyService;
            this._currencySettings = currencySettings;
            this._measureService = measureService;
            this._measureSettings = measureSettings;
        }
        
        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            //little hack here
            //always set culture to 'en-US' (Telerik has a bug related to editing decimal values in other cultures). Like currently it's done for admin area in Global.asax.cs
            CommonHelper.SetTelerikCulture();

            base.Initialize(requestContext);
        }

        [ChildActionOnly]
        public ActionResult Configure()
        {
            var model = new ShippingByCountryListModel();
            //other settings
            model.LimitMethodsToCreated = _shippingByCountrySettings.LimitMethodsToCreated;

            return View("~/Plugins/Shipping.ByCountry/Views/ShippingByCountry/Configure.cshtml", model);
        }

        [HttpPost]
        [AdminAntiForgery]
        public ActionResult SaveGeneralSettings(ShippingByCountryListModel model)
        {
            //save settings
            _shippingByCountrySettings.LimitMethodsToCreated = model.LimitMethodsToCreated;
            _settingService.SaveSetting(_shippingByCountrySettings);

            return Json(new { Result = true });
        }

        [HttpPost]
        [AdminAntiForgery]
        public ActionResult RatesList(DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageShippingSettings))
                return Content("Access denied");

            var records = _shippingByCountryService.GetAll(command.Page - 1, command.PageSize);
            var sbwModel = records.Select(x =>
                {
                    var m = new ShippingByCountryModel
                    {
                        Id = x.Id,
                        StoreId = x.StoreId,
                        ProductType = x.ProductType,
                        ProductCode = x.ProductCode,
                        USShippingPrice = x.USShippingPrice,
                        InternationalShippingPrice = x.InternationalShippingPrice,
                        USAddedItemPrice = x.USAddedItemPrice,
                        InternationalAddedItemPrice = x.InternationalAddedItemPrice
                    };
                
                    //store
                    var store = _storeService.GetStoreById(x.StoreId);
                    m.StoreName = (store != null) ? store.Name : "*";
                
                    ////country
                    //var c = _countryService.GetCountryById(x.CountryId);
                    //m.CountryName = (c != null) ? c.Name : "*";

                    return m;
                })
                .ToList();
            var gridModel = new DataSourceResult
            {
                Data = sbwModel,
                Total = records.TotalCount
            };

            return Json(gridModel);
        }

        [HttpPost]
        [AdminAntiForgery]
        public ActionResult RateDelete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageShippingSettings))
                return Content("Access denied");

            var sbw = _shippingByCountryService.GetById(id);
            if (sbw != null)
                _shippingByCountryService.DeleteShippingByCountryRecord(sbw);

            return new NullJsonResult();
        }

        //add
        public ActionResult AddPopup()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageShippingSettings))
                return Content("Access denied");

            var model = new ShippingByCountryModel();
            model.PrimaryStoreCurrencyCode = _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId).CurrencyCode;
          
            //stores
            model.AvailableStores.Add(new SelectListItem { Text = "*", Value = "0" });
            foreach (var store in _storeService.GetAllStores())
                model.AvailableStores.Add(new SelectListItem { Text = store.Name, Value = store.Id.ToString() });

            //countries
            model.AvailableCountries.Add(new SelectListItem { Text = "*", Value = "0" });
            var countries = _countryService.GetAllCountries(showHidden: true);
            foreach (var c in countries)
                model.AvailableCountries.Add(new SelectListItem { Text = c.Name, Value = c.Id.ToString() });
            //states
            model.AvailableStates.Add(new SelectListItem { Text = "*", Value = "0" });

            return View("~/Plugins/Shipping.ByCountry/Views/ShippingByCountry/AddPopup.cshtml", model);
        }
        [HttpPost]
        [AdminAntiForgery]
        public ActionResult AddPopup(string btnId, string formId, ShippingByCountryModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageShippingSettings))
                return Content("Access denied");

            var sbw = new ShippingByCountryRecord
            {
                StoreId = model.StoreId,
                Order = model.Order,
                ProductType = model.ProductType,
                ProductCode = model.ProductCode,
                USShippingPrice = model.USShippingPrice,
                InternationalShippingPrice = model.InternationalShippingPrice,
                USAddedItemPrice = model.USAddedItemPrice,
                InternationalAddedItemPrice = model.InternationalAddedItemPrice
            };
            _shippingByCountryService.InsertShippingByCountryRecord(sbw);

            ViewBag.RefreshPage = true;
            ViewBag.btnId = btnId;
            ViewBag.formId = formId;

            return View("~/Plugins/Shipping.ByCountry/Views/ShippingByCountry/AddPopup.cshtml", model);
        }

        //edit
        public ActionResult EditPopup(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageShippingSettings))
                return Content("Access denied");

            var sbw = _shippingByCountryService.GetById(id);
            if (sbw == null)
                //No record found with the specified id
                return RedirectToAction("Configure");

            var model = new ShippingByCountryModel
            {
                Id = sbw.Id,
                StoreId = sbw.StoreId,
                ProductType = sbw.ProductType,
                ProductCode = sbw.ProductCode,
                Order = sbw.Order,
                USShippingPrice = sbw.USShippingPrice,
                InternationalShippingPrice = sbw.InternationalShippingPrice,
                USAddedItemPrice = sbw.USAddedItemPrice,
                InternationalAddedItemPrice = sbw.InternationalAddedItemPrice,
                PrimaryStoreCurrencyCode = _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId).CurrencyCode,
            };


            var selectedStore = _storeService.GetStoreById(sbw.StoreId);
            
            //var selectedCountry = _countryService.GetCountryById(sbw.CountryId);
            
            //stores
            model.AvailableStores.Add(new SelectListItem { Text = "*", Value = "0" });
            foreach (var store in _storeService.GetAllStores())
                model.AvailableStores.Add(new SelectListItem { Text = store.Name, Value = store.Id.ToString(), Selected = (selectedStore != null && store.Id == selectedStore.Id) });
            
            ////countries
            //model.AvailableCountries.Add(new SelectListItem { Text = "*", Value = "0" });
            //var countries = _countryService.GetAllCountries(showHidden: true);
            //foreach (var c in countries)
            //    model.AvailableCountries.Add(new SelectListItem { Text = c.Name, Value = c.Id.ToString(), Selected = (selectedCountry != null && c.Id == selectedCountry.Id) });
            

            return View("~/Plugins/Shipping.ByCountry/Views/ShippingByCountry/EditPopup.cshtml", model);
        }
        [HttpPost]
        [AdminAntiForgery]
        public ActionResult EditPopup(string btnId, string formId, ShippingByCountryModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageShippingSettings))
                return Content("Access denied");

            var sbw = _shippingByCountryService.GetById(model.Id);
            if (sbw == null)
                //No record found with the specified id
                return RedirectToAction("Configure");

            sbw.StoreId = model.StoreId;
            sbw.ProductType = model.ProductType;
            sbw.ProductCode = model.ProductCode;
            sbw.Order = model.Order;
            sbw.USShippingPrice = model.USShippingPrice;
            sbw.InternationalShippingPrice = model.InternationalShippingPrice;
            sbw.USAddedItemPrice = model.USAddedItemPrice;
            sbw.InternationalAddedItemPrice = model.InternationalAddedItemPrice;

            _shippingByCountryService.UpdateShippingByCountryRecord(sbw);

            ViewBag.RefreshPage = true;
            ViewBag.btnId = btnId;
            ViewBag.formId = formId;

            return View("~/Plugins/Shipping.ByCountry/Views/ShippingByCountry/EditPopup.cshtml", model);
        }

        #region Shipping by Country Method Mapping

        public ActionResult ShippingByCountryMethodConfiguration()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageShippingSettings))
                return Content("Access denied");

            return View("~/Plugins/Shipping.ByCountry/Views/ShippingByCountry/ShippingByCountryMethodConfiguration.cshtml");
        }

        [HttpPost]
        [AdminAntiForgery]
        public ActionResult ShippingByCountryMethodConfigList(DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageShippingSettings))
                return Content("Access denied");

            var records = _shippingByCountryService.GetAllShippingByCountryMethod(command.Page - 1, command.PageSize);
            var model = records.Select(x =>
            {
                var m = new ShippingByCountryMethodModel
                {
                    Id = x.Id,
                    Description = x.Description,
                    AddedItemPrice = x.AddedItemPrice,
                    MaxItemsPerBox = x.MaxItemsPerBox,
                    MaxShipDate = x.MaxShipDate,
                    MinShipDate = x.MinShipDate,
                    ShippingByCountryId = x.ShippingByCountryId,
                    ShippingMethodId = x.ShippingMethodId,
                    ShippingFee = x.ShippingFee,
                };
                var shippingMethod = _shippingService.GetShippingMethodById(m.ShippingMethodId);
                m.ShippingMethodName = shippingMethod != null ? shippingMethod.Name : "Not set";
                var shippingRecord = _shippingByCountryService.GetById(m.ShippingByCountryId);
                m.ShippingByCountryName = shippingRecord != null ? shippingRecord.ProductType : "Not set";
                return m;
            })
                .ToList();
            var gridModel = new DataSourceResult
            {
                Data = model,
                Total = records.TotalCount
            };

            return Json(gridModel);
        }

        //create
        public ActionResult AddMethodConfiguration()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageShippingSettings))
                return Content("Access denied");

            ShippingByCountryMethodModel model = new ShippingByCountryMethodModel();
            var allShippingRecord = _shippingByCountryService.GetAll();
            model.AvailableShippingRecords.Add(new SelectListItem
            {
                Text = "Not Set",
                Value = "0",
            });
            foreach(var item in allShippingRecord)
            {
                model.AvailableShippingRecords.Add(new SelectListItem
                {
                    Text = item.ProductType,
                    Value = item.Id.ToString(),
                });
            }
            var allShippingMethod = _shippingService.GetAllShippingMethods();
            model.AvailableShippingMethods.Add(new SelectListItem
            {
                Text = "Not Set",
                Value = "0",
            });
            foreach (var item in allShippingMethod)
            {
                model.AvailableShippingMethods.Add(new SelectListItem
                {
                    Text = item.Name,
                    Value = item.Id.ToString(),
                });
            }
            return View("~/Plugins/Shipping.ByCountry/Views/ShippingByCountry/AddMethodConfiguration.cshtml", model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult AddMethodConfiguration(ShippingByCountryMethodModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageShippingSettings))
                return Content("Access denied");

            if (ModelState.IsValid)
            {
                bool isValid;
                string message;
                 _shippingByCountryService.ValidateShippingByCountryMethod(model.ShippingByCountryId, model.ShippingMethodId, out isValid,out message);

                if(isValid)
                {
                    var methodConf = new ShippingByCountryMethod
                    {
                        ShippingByCountryId = model.ShippingByCountryId,
                        ShippingMethodId = model.ShippingMethodId,
                        AddedItemPrice = model.AddedItemPrice,
                        Description = model.Description,
                        MaxItemsPerBox = model.MaxItemsPerBox,
                        MaxShipDate = model.MaxShipDate,
                        MinShipDate = model.MinShipDate,
                        ShippingFee = model.ShippingFee
                    };
                    _shippingByCountryService.InsertShippingByCountryMethod(methodConf);

                    SuccessNotification("Added successfully");
                    if (continueEditing)
                    {

                        return RedirectToAction("EditMethodConfiguration", new { id = methodConf.Id });
                    }
                    return RedirectToAction("ShippingByCountryMethodConfiguration");
                }
                else
                {
                    ErrorNotification(message);
                    var allShippingRecord = _shippingByCountryService.GetAll();
                    model.AvailableShippingRecords.Add(new SelectListItem
                    {
                        Text = "Not Set",
                        Value = "0",
                    });
                    foreach (var item in allShippingRecord)
                    {
                        model.AvailableShippingRecords.Add(new SelectListItem
                        {
                            Text = item.ProductType,
                            Value = item.Id.ToString(),
                        });
                    }
                    var allShippingMethod = _shippingService.GetAllShippingMethods();
                    model.AvailableShippingMethods.Add(new SelectListItem
                    {
                        Text = "Not Set",
                        Value = "0",
                    });
                    foreach (var item in allShippingMethod)
                    {
                        model.AvailableShippingMethods.Add(new SelectListItem
                        {
                            Text = item.Name,
                            Value = item.Id.ToString(),
                        });
                    }
                }
            }
            //If we got this far, something failed, redisplay form
            return View("~/Plugins/Shipping.ByCountry/Views/ShippingByCountry/AddMethodConfiguration.cshtml", model);
        }

        //edit
        public ActionResult EditMethodConfiguration(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageShippingSettings))
                return Content("Access denied");

            var methodCfg = _shippingByCountryService.GetShippingByCountryMethodById(id);
            if (methodCfg == null)
                //No product attribute found with the specified id
                return RedirectToAction("List");

            var model = new ShippingByCountryMethodModel {
                Id= methodCfg.Id,
                ShippingByCountryId = methodCfg.ShippingByCountryId,
                ShippingMethodId= methodCfg.ShippingMethodId,
                AddedItemPrice= methodCfg.AddedItemPrice,
                Description= methodCfg.Description,
                MaxItemsPerBox= methodCfg.MaxItemsPerBox,
                MaxShipDate= methodCfg.MaxShipDate,
                MinShipDate= methodCfg.MinShipDate,
                ShippingFee= methodCfg.ShippingFee
            };
            var allShippingRecord = _shippingByCountryService.GetAll();
            foreach (var item in allShippingRecord)
            {
                model.AvailableShippingRecords.Add(new SelectListItem
                {
                    Text = item.ProductType,
                    Value = item.Id.ToString(),
                    Selected = model.ShippingByCountryId==item.Id
                });
            }

            var allShippingMethod = _shippingService.GetAllShippingMethods();
            foreach (var item in allShippingMethod)
            {
                model.AvailableShippingMethods.Add(new SelectListItem
                {
                    Text = item.Name,
                    Value = item.Id.ToString(),
                    Selected = model.ShippingMethodId == item.Id
                });
            }
            return View("~/Plugins/Shipping.ByCountry/Views/ShippingByCountry/EditMethodConfiguration.cshtml", model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult EditMethodConfiguration(ShippingByCountryMethodModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageShippingSettings))
                return Content("Access denied");

            var methodCfg = _shippingByCountryService.GetShippingByCountryMethodById(model.Id);
            if (methodCfg == null)
                //No product attribute found with the specified id
                return RedirectToAction("ShippingByCountryMethodConfiguration");

            if (ModelState.IsValid)
            {
                bool isValid;
                string message;
                _shippingByCountryService.ValidateShippingByCountryMethod(model.ShippingByCountryId, model.ShippingMethodId, out isValid, out message);

                //allowed update if config require field is not changed
                if (methodCfg.ShippingMethodId == model.ShippingMethodId && methodCfg.ShippingByCountryId == model.ShippingByCountryId)
                    isValid = true;

                if(isValid)
                {
                    methodCfg.ShippingByCountryId = model.ShippingByCountryId;
                    methodCfg.ShippingMethodId = model.ShippingMethodId;
                    methodCfg.AddedItemPrice = model.AddedItemPrice;
                    methodCfg.Description = model.Description;
                    methodCfg.MaxItemsPerBox = model.MaxItemsPerBox;
                    methodCfg.MaxShipDate = model.MaxShipDate;
                    methodCfg.MinShipDate = model.MinShipDate;
                    methodCfg.ShippingFee = model.ShippingFee;
                    _shippingByCountryService.UpdateShippingByCountryMethod(methodCfg);

                    SuccessNotification("Updated was succesfully");
                    if (continueEditing)
                    {
                        return RedirectToAction("EditMethodConfiguration", new { id = methodCfg.Id });
                    }
                    return RedirectToAction("ShippingByCountryMethodConfiguration");
                }
                else
                {
                    ErrorNotification(message);
                    var allShippingRecord = _shippingByCountryService.GetAll();
                    foreach (var item in allShippingRecord)
                    {
                        model.AvailableShippingRecords.Add(new SelectListItem
                        {
                            Text = item.ProductType,
                            Value = item.Id.ToString(),
                            Selected = model.ShippingByCountryId == item.Id
                        });
                    }

                    var allShippingMethod = _shippingService.GetAllShippingMethods();
                    foreach (var item in allShippingMethod)
                    {
                        model.AvailableShippingMethods.Add(new SelectListItem
                        {
                            Text = item.Name,
                            Value = item.Id.ToString(),
                            Selected = model.ShippingMethodId == item.Id
                        });
                    }
                }
            }

            //If we got this far, something failed, redisplay form
            return View("~/Plugins/Shipping.ByCountry/Views/ShippingByCountry/EditMethodConfiguration.cshtml", model);
        }

        //delete
        [HttpPost]
        public ActionResult DeleteMethodConfiguration(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageShippingSettings))
                return Content("Access denied");

            var methodCfg = _shippingByCountryService.GetShippingByCountryMethodById(id);
            if (methodCfg == null)
                //No product attribute found with the specified id
                return RedirectToAction("ShippingByCountryMethodConfiguration");

            _shippingByCountryService.DeleteShippingByCountryMethod(methodCfg);

            SuccessNotification(_localizationService.GetResource("Admin.Catalog.Attributes.ProductAttributes.Deleted"));
            return RedirectToAction("ShippingByCountryMethodConfiguration");
        }
        #endregion
    }
}
