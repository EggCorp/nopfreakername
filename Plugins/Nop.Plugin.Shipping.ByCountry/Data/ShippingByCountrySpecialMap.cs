﻿using Nop.Data.Mapping;
using Nop.Plugin.Shipping.ByCountry.Domain;

namespace Nop.Plugin.Shipping.ByCountry.Data
{
    public partial class ShippingByCountrySpecialMap : NopEntityTypeConfiguration<ShippingByCountrySpecial>
    {
        public ShippingByCountrySpecialMap()
        {
            this.ToTable("ShippingByCountrySpecial");
            this.HasKey(x => x.Id);

            //this.HasRequired(u => u.ShippingByCountryMethod)
            //    .WithMany()
            //    .HasForeignKey(u => u.ShippingByCountryMethodId);

            //this.HasRequired(u => u.Country)
            //    .WithMany()
            //    .HasForeignKey(u => u.CountryId);
        }
    }
}
