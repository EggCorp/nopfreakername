using Nop.Data.Mapping;
using Nop.Plugin.Shipping.ByCountry.Domain;

namespace Nop.Plugin.Shipping.ByCountry.Data
{
    public partial class ShippingByCountryRecordMap : NopEntityTypeConfiguration<ShippingByCountryRecord>
    {
        public ShippingByCountryRecordMap()
        {
            this.ToTable("ShippingByCountry");
            this.HasKey(x => x.Id);

        }
    }
}