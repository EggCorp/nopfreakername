﻿using Nop.Data.Mapping;
using Nop.Plugin.Shipping.ByCountry.Domain;

namespace Nop.Plugin.Shipping.ByCountry.Data
{
    public class ShippingByCountryMethodMap : NopEntityTypeConfiguration<ShippingByCountryMethod>
    {
        public ShippingByCountryMethodMap()
        {
            this.ToTable("ShippingByCountryMethod");
            this.HasKey(x => x.Id);

            //this.HasRequired(u=> u.ShippingByCountry)
            //    .WithMany()
            //    .HasForeignKey(u => u.ShippingByCountryId);

            //this.HasRequired(u => u.ShippingMethod)
            //    .WithMany()
            //    .HasForeignKey(u => u.ShippingMethodId);


        }
    }
}
