using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Plugin.Shipping.ByCountry.Domain;
using Nop.Core.Domain.Catalog;
using Nop.Services.Catalog;
using Nop.Services.Orders;
using Nop.Services.Common;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Shipping;

namespace Nop.Plugin.Shipping.ByCountry.Services
{
    public partial class ShippingByCountryService : IShippingByCountryService
    {
        #region Constants
        private const string SHIPPINGByCountry_ALL_KEY = "Nop.shippingByCountry.all-{0}-{1}";
        private const string SHIPPINGByCountry_PATTERN_KEY = "Nop.shippingByCountry.";
        #endregion

        #region Fields

        private readonly IRepository<ShippingByCountryRecord> _sbwRepository;
        private readonly IRepository<ShippingByCountryMethod> _shippingByCountryMethodRepository;
        private readonly IRepository<ShippingByCountrySpecial> _shippingByCountrySpecialRepository;
        private readonly ICacheManager _cacheManager;
        private readonly IRepository<ProductAttributeValue> _productAttributeValue;
        private readonly IRepository<ShippingMethod> _shippingMethodRepository;
        private readonly IProductService _productService;
        private readonly IProductAttributeParser _productAttributeParser;
        private readonly ICheckoutAttributeParser _checkoutAttributeParser;
        private readonly IGenericAttributeService _genericAttributeService;

        #endregion

        #region Ctor

        public ShippingByCountryService(ICacheManager cacheManager,
            IRepository<ShippingByCountryRecord> sbwRepository, 
            IRepository<ProductAttributeValue> productAttributeValue,
            IProductService productService,
            IProductAttributeParser productAttributeParser,
            ICheckoutAttributeParser checkoutAttributeParser,
            IGenericAttributeService genericAttributeService,
            IRepository<ShippingByCountryMethod> shippingByCountryMethodRepository,
            IRepository<ShippingByCountrySpecial> shippingByCountrySpecialRepository,
            IRepository<ShippingMethod> shippingMethodRepository)
        {
            this._cacheManager = cacheManager;
            this._sbwRepository = sbwRepository;
            this._productAttributeValue = productAttributeValue;
            this._productService = productService;
            this._productAttributeParser = productAttributeParser;
            this._checkoutAttributeParser = checkoutAttributeParser;
            this._genericAttributeService = genericAttributeService;
            this._shippingByCountryMethodRepository = shippingByCountryMethodRepository;
            this._shippingByCountrySpecialRepository = shippingByCountrySpecialRepository;
            this._shippingMethodRepository = shippingMethodRepository;
        }

        #endregion

        #region Methods

        public virtual void DeleteShippingByCountryRecord(ShippingByCountryRecord shippingByCountryRecord)
        {
            if (shippingByCountryRecord == null)
                throw new ArgumentNullException("shippingByCountryRecord");

            _sbwRepository.Delete(shippingByCountryRecord);

            _cacheManager.RemoveByPattern(SHIPPINGByCountry_PATTERN_KEY);
        }

        public virtual IPagedList<ShippingByCountryRecord> GetAll(int pageIndex = 0, int pageSize = int.MaxValue)
        {
            string key = string.Format(SHIPPINGByCountry_ALL_KEY, pageIndex, pageSize);
            return _cacheManager.Get(key, () =>
            {
                var query = from sbw in _sbwRepository.Table
                            orderby sbw.StoreId, sbw.ProductType, sbw.Order
                            select sbw;

                var records = new PagedList<ShippingByCountryRecord>(query, pageIndex, pageSize);
                return records;
            });
        }

        public virtual ShippingByCountryRecord FindRecord(int storeId,
            string productCode, int order)
        {

            //filter by country and shipping method
            var existingRates = GetAll().ToList();

            //filter by store
            var matchedByStore = new List<ShippingByCountryRecord>();
            foreach (var sbw in existingRates)
                if (storeId == sbw.StoreId)
                    matchedByStore.Add(sbw);
            if (!matchedByStore.Any())
                foreach (var sbw in existingRates)
                    if (sbw.StoreId == 0)
                        matchedByStore.Add(sbw);


            //filter by product code
            var matchedByCode = new List<ShippingByCountryRecord>();
            foreach (var sbw in matchedByStore)
                if (productCode == sbw.ProductCode)
                    matchedByCode.Add(sbw);
            if (!matchedByCode.Any())
                foreach (var sbw in matchedByStore)
                    if (string.IsNullOrEmpty(sbw.ProductCode))
                        matchedByCode.Add(sbw);

            //filter by order
            var matchedByOrder = new List<ShippingByCountryRecord>();
            foreach (var sbw in matchedByCode)
                if (order == sbw.Order)
                    matchedByOrder.Add(sbw);
            if (!matchedByOrder.Any())
                foreach (var sbw in matchedByCode)
                    if (sbw.Order == 0)
                        matchedByOrder.Add(sbw);

            return matchedByOrder.FirstOrDefault();
        }

        public virtual ShippingByCountryRecord GetById(int shippingByCountryRecordId)
        {
            if (shippingByCountryRecordId == 0)
                return null;

            return _sbwRepository.GetById(shippingByCountryRecordId);
        }

        public virtual void InsertShippingByCountryRecord(ShippingByCountryRecord shippingByCountryRecord)
        {
            if (shippingByCountryRecord == null)
                throw new ArgumentNullException("shippingByCountryRecord");

            _sbwRepository.Insert(shippingByCountryRecord);

            _cacheManager.RemoveByPattern(SHIPPINGByCountry_PATTERN_KEY);
        }

        public virtual void UpdateShippingByCountryRecord(ShippingByCountryRecord shippingByCountryRecord)
        {
            if (shippingByCountryRecord == null)
                throw new ArgumentNullException("shippingByCountryRecord");

            _sbwRepository.Update(shippingByCountryRecord);

            _cacheManager.RemoveByPattern(SHIPPINGByCountry_PATTERN_KEY);
        }
        public virtual string GetProductTypeByAttributeValueId(int id)
        {
            var query = _productAttributeValue.Table.Where(p => p.Id == id);
            var result = query.Select(p => p.Name).FirstOrDefault();

            return result;
        }
        public virtual ProductAttributeValue GetProductAttributeValue(string attributesXml, List<ShippingByCountryRecord> sbcRecords)
        {
            var productAttrValues = _productAttributeParser.ParseProductAttributeValues(attributesXml);
            if(productAttrValues.Any())
            {
                foreach (var item in sbcRecords)
                {
                    foreach (var attr in productAttrValues)
                    {
                        if (item.ProductType.Equals(attr.Name))
                            return attr;
                    }
                }
            }
            return productAttrValues.FirstOrDefault();
        }

        public ShippingByCountryMethod GetShippingCountryMethod(int shippingByCountryId, int shippingMethodId, Address address)
        {
            ShippingByCountryMethod result = _shippingByCountryMethodRepository.Table.Where(sbcm => sbcm.ShippingByCountryId == shippingByCountryId && sbcm.ShippingMethodId == shippingMethodId).FirstOrDefault();
            if (result == null)
                return null;
            else
            {
                //check shipping Country Special
                ShippingByCountrySpecial shippingByCountrySpecial = _shippingByCountrySpecialRepository.Table.Where(sbcs => sbcs.ShippingByCountryMethodId == result.Id && sbcs.CountryId == address.CountryId).FirstOrDefault();
                if (shippingByCountrySpecial != null)
                {
                    result.ShippingFee = shippingByCountrySpecial.ShippingFee;
                    result.AddedItemPrice = shippingByCountrySpecial.AddedItemPrice;
                }

                return result;
            }
        }
        
        public virtual IPagedList<ShippingByCountryMethod> GetAllShippingByCountryMethod(int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = _shippingByCountryMethodRepository.Table.OrderByDescending(p=>p.Id);
            var records = new PagedList<ShippingByCountryMethod>(query, pageIndex, pageSize);
            return records;
        }


        public virtual void InsertShippingByCountryMethod(ShippingByCountryMethod shippingByCountryMethod)
        {
            if (shippingByCountryMethod == null)
                throw new ArgumentNullException("shippingByCountryMethod");

            _shippingByCountryMethodRepository.Insert(shippingByCountryMethod);
        }

        public virtual void UpdateShippingByCountryMethod(ShippingByCountryMethod shippingByCountryMethod)
        {
            if (shippingByCountryMethod == null)
                throw new ArgumentNullException("shippingByCountryMethod");

            _shippingByCountryMethodRepository.Update(shippingByCountryMethod);
        }
        public virtual ShippingByCountryMethod GetShippingByCountryMethodById(int id)
        {
            return _shippingByCountryMethodRepository.GetById(id);
        }
        public virtual void DeleteShippingByCountryMethod(ShippingByCountryMethod shippingByCountryMethod)
        {
            if (shippingByCountryMethod == null)
                throw new ArgumentNullException("shippingByCountryMethod");

            _shippingByCountryMethodRepository.Delete(shippingByCountryMethod);
        }

        public virtual void ValidateShippingByCountryMethod(int recordId,int methodId, out bool isValid,out string message)
        {
            isValid = false;
            message = string.Empty;
            var shippingRecord = this.GetById(recordId);
            var shippingMethod = _shippingMethodRepository.GetById(methodId);
            if (shippingRecord == null)
                message = "Shipping by country record is not exist";
            if (shippingMethod == null)
                message = "Shipping method is not exist";

            if (shippingMethod != null && shippingRecord != null)
            {
                var shippingByCountryMethod = _shippingByCountryMethodRepository.Table.Where(p => p.ShippingByCountryId == shippingRecord.Id && p.ShippingMethodId == shippingMethod.Id).FirstOrDefault();
                if (shippingByCountryMethod != null)
                    message = string.Format("Shipping by country record-{0} was already matched with Method {1}. Please try again.", shippingRecord.ProductType, shippingMethod.Name);
                else
                {
                    isValid = true;
                    message = "Valid shipping record and method";
                }
            }
            
        }
        #endregion
    }
}
