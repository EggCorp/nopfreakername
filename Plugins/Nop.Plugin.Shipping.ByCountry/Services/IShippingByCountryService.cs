using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Plugin.Shipping.ByCountry.Domain;
using System.Collections.Generic;

namespace Nop.Plugin.Shipping.ByCountry.Services
{
    public partial interface IShippingByCountryService
    {
        void DeleteShippingByCountryRecord(ShippingByCountryRecord shippingByCountryRecord);

        IPagedList<ShippingByCountryRecord> GetAll(int pageIndex = 0, int pageSize = int.MaxValue);

        ShippingByCountryRecord FindRecord(int storeId,
            string productCode, int order);

        ShippingByCountryRecord GetById(int shippingByCountryRecordId);

        void InsertShippingByCountryRecord(ShippingByCountryRecord shippingByCountryRecord);

        void UpdateShippingByCountryRecord(ShippingByCountryRecord shippingByCountryRecord);

        string GetProductTypeByAttributeValueId(int id);
        ProductAttributeValue GetProductAttributeValue(string attributesXml, List<ShippingByCountryRecord> sbcRecords);

        ShippingByCountryMethod GetShippingCountryMethod(int shippingByCountryId, int shippingMethodId, Address address);

        IPagedList<ShippingByCountryMethod> GetAllShippingByCountryMethod(int pageIndex = 0, int pageSize = int.MaxValue);

        void InsertShippingByCountryMethod(ShippingByCountryMethod shippingByCountryMethod);
        void UpdateShippingByCountryMethod(ShippingByCountryMethod shippingByCountryMethod);
        ShippingByCountryMethod GetShippingByCountryMethodById(int id);
        void DeleteShippingByCountryMethod(ShippingByCountryMethod shippingByCountryMethod);
        void ValidateShippingByCountryMethod(int recordId, int methodId, out bool isValid, out string message);
    }
}
