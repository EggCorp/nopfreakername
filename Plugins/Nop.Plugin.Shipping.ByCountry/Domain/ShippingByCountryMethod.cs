﻿using Nop.Core;
using Nop.Core.Domain.Shipping;
using Nop.Plugin.Shipping.ByCountry.Domain;

namespace Nop.Plugin.Shipping.ByCountry.Domain
{
    public class ShippingByCountryMethod : BaseEntity
    {
        public int ShippingMethodId { get; set; }
        public int ShippingByCountryId { get; set; }
        public decimal ShippingFee { get; set; }
        public int MinShipDate { get; set; }
        public int MaxShipDate { get; set; }
        public string Description { get; set; }
        public int MaxItemsPerBox { get; set; }
        public decimal AddedItemPrice { get; set; }

        //public virtual ShippingMethod ShippingMethod { get; set; }
        //public virtual ShippingByCountryRecord ShippingByCountry { get; set; }
    }
}
