using Nop.Core;

namespace Nop.Plugin.Shipping.ByCountry.Domain
{
    /// <summary>
    /// Represents a shipping by country record
    /// </summary>
    public partial class ShippingByCountryRecord : BaseEntity
    {
        /// <summary>
        /// Gets or sets the store identifier
        /// </summary>
        public int StoreId { get; set; }

        /// <summary>
        /// Gets or sets the country identifier
        /// </summary>
        public string ProductType { get; set; }
        public string ProductCode { get; set; }
        public decimal USShippingPrice { get; set; }
        public decimal InternationalShippingPrice { get; set; }
        public decimal USAddedItemPrice { get; set; }
        public decimal InternationalAddedItemPrice { get; set; }


        public int Order { get; set; }
    }
}