﻿using Nop.Core;
using Nop.Core.Domain.Directory;


namespace Nop.Plugin.Shipping.ByCountry.Domain
{
    public class ShippingByCountrySpecial : BaseEntity
    {
        public int CountryId { get; set; }
        public decimal ShippingFee { get; set; }
        public int ShippingByCountryMethodId { get; set; }
        public int MaxItemsPerBox { get; set; }
        public decimal AddedItemPrice { get; set; }
        //public virtual Country Country { get; set; }
        //public virtual ShippingByCountryMethod ShippingByCountryMethod { get; set; }
    }
}
