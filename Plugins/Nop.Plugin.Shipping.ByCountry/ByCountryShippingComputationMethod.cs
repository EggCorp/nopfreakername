﻿using System;
using System.Linq;
using System.Web.Routing;
using Nop.Core;
using Nop.Core.Domain.Shipping;
using Nop.Core.Plugins;
using Nop.Plugin.Shipping.ByCountry.Data;
using Nop.Plugin.Shipping.ByCountry.Services;
using Nop.Services.Catalog;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Shipping;
using Nop.Services.Shipping.Tracking;
using System.Xml;
using System.Collections.Generic;
using Nop.Plugin.Shipping.ByCountry.Models;
using System.Text.RegularExpressions;
using Nop.Plugin.Shipping.ByCountry.Domain;
using Nop.Services.Common;
using Nop.Core.Domain.Customers;

namespace Nop.Plugin.Shipping.ByCountry
{
    public class ByCountryShippingComputationMethod : BasePlugin, IShippingRateComputationMethod
    {
        #region Fields

        private readonly IShippingService _shippingService;
        private readonly IStoreContext _storeContext;
        private readonly IShippingByCountryService _shippingByCountryService;
        private readonly IPriceCalculationService _priceCalculationService;
        private readonly ShippingByCountrySettings _shippingByCountrySettings;
        private readonly ShippingByContryObjectContext _objectContext;
        private readonly ISettingService _settingService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IWorkContext _workContext;

        #endregion

        #region Ctor
        public ByCountryShippingComputationMethod(IShippingService shippingService,
            IStoreContext storeContext,
            IShippingByCountryService shippingByCountryService,
            IPriceCalculationService priceCalculationService, 
            ShippingByCountrySettings shippingByCountrySettings,
            ShippingByContryObjectContext objectContext,
            ISettingService settingService,
            IGenericAttributeService genericAttributeService,
            IWorkContext workContext)
        {
            this._shippingService = shippingService;
            this._storeContext = storeContext;
            this._shippingByCountryService = shippingByCountryService;
            this._priceCalculationService = priceCalculationService;
            this._shippingByCountrySettings = shippingByCountrySettings;
            this._objectContext = objectContext;
            this._settingService = settingService;
            this._genericAttributeService = genericAttributeService;
            this._workContext = workContext;
        }
        #endregion

        #region Utilities
        
        private decimal? GetRate(int storeId, string productCode)
        {
            var shippingByCountryRecord = _shippingByCountryService.FindRecord(storeId, productCode, 0);
            if (shippingByCountryRecord == null)
            {
                if (_shippingByCountrySettings.LimitMethodsToCreated)
                    return null;
                
                return decimal.Zero;
            }

            //additional fixed cost
            decimal shippingTotal = shippingByCountryRecord.USShippingPrice;
            

            if (shippingTotal < decimal.Zero)
                shippingTotal = decimal.Zero;
            return shippingTotal;
        }
        
        #endregion

        #region Methods

        /// <summary>
        ///  Gets available shipping options
        /// </summary>
        /// <param name="getShippingOptionRequest">A request for getting shipping options</param>
        /// <returns>Represents a response of getting shipping rate options</returns>
        public GetShippingOptionResponse GetShippingOptions(GetShippingOptionRequest getShippingOptionRequest)
        {
            ShippingMethod shippingMethod = null;

            string selectedShippingMethod = _workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.SelectedShippingMethod);
            if (!string.IsNullOrEmpty(selectedShippingMethod))
            {
                int shippingMethodId;
                Int32.TryParse(selectedShippingMethod, out shippingMethodId);
                shippingMethod = _shippingService.GetShippingMethodById(shippingMethodId);
            }
                

            if (getShippingOptionRequest == null)
                throw new ArgumentNullException("getShippingOptionRequest");

            var response = new GetShippingOptionResponse();

            if (getShippingOptionRequest.Items == null || !getShippingOptionRequest.Items.Any())
            {
                response.AddError("No shipment items");
                return response;
            }
            if (getShippingOptionRequest.ShippingAddress == null)
            {
                response.AddError("Shipping address is not set");
                return response;
            }

            var storeId = getShippingOptionRequest.StoreId;
            if (storeId == 0)
                storeId = _storeContext.CurrentStore.Id;
            int countryId = getShippingOptionRequest.ShippingAddress.CountryId.HasValue ? getShippingOptionRequest.ShippingAddress.CountryId.Value : 0;

            int totalItem = 0;
            var shippingCountryOptions = _shippingByCountryService.GetAll().OrderByDescending(sbc => sbc.USShippingPrice);
            List<ShippingByCountryProductModel> productModels = new List<ShippingByCountryProductModel>();
            foreach (var packageItem in getShippingOptionRequest.Items)
            {
                if (packageItem.ShoppingCartItem.IsFreeShipping)
                    continue;
                if(!string.IsNullOrEmpty(packageItem.ShoppingCartItem.AttributesXml))
                {
                    //Get attr value
                    var attribute = _shippingByCountryService.GetProductAttributeValue(packageItem.ShoppingCartItem.AttributesXml, shippingCountryOptions.ToList());
                    //var attrModel= GetAttributeValue(packageItem.ShoppingCartItem.AttributesXml, shippingCountryOptions.ToList());
                    //string productType = _shippingByCountryService.GetProductTypeByAttributeValueId(attrModel.ProductTypeId);
                    if(attribute !=null)
                    {
                        if (!string.IsNullOrEmpty(attribute.Name))
                        {
                            var model = new ShippingByCountryProductModel();
                            model.ProductType = attribute.Name;
                            model.ProductCount = packageItem.ShoppingCartItem.Quantity;
                            model.CategoryId = packageItem.ShoppingCartItem.Product.ProductCategories.Select(p => p.CategoryId).FirstOrDefault();
                            productModels.Add(model);
                        }
                    }

                }
                //TODO we should use getShippingOptionRequest.Items.GetQuantity() method to get subtotal
                totalItem += packageItem.ShoppingCartItem.Quantity;
            }
            var allItems = productModels.GroupBy(p => new { p.ProductType })
               .Select(pr => new ShippingByCountryProductModel
               {
                   ProductType = pr.Select(p => p.ProductType).FirstOrDefault(),
                   CategoryId = pr.Select(p => p.CategoryId).FirstOrDefault(),
                   ProductCount = pr.Sum(c => c.ProductCount)
               }).ToList();

            /// Calculate Shipping price for product type
            foreach (var item in allItems)
            {
                var normalizeProductType = Regex.Replace(item.ProductType.ToLower(), "[^0-9a-zA-Z]+", string.Empty);

                var shippingModel = shippingCountryOptions
                    .Where(p => normalizeProductType
                    .Equals(Regex.Replace(p.ProductType.ToLower(), "[^0-9a-zA-Z]+", string.Empty)))
                    .FirstOrDefault();
                if(shippingModel !=null)
                {
                    // shipping fee for stander shipping method
                    item.USAddedItemPrice = shippingModel.USAddedItemPrice;
                    item.USShippingPrice = shippingModel.USShippingPrice;
                    item.InternationalAddedItemPrice = shippingModel.InternationalAddedItemPrice;
                    item.InternationalShippingPrice = shippingModel.InternationalShippingPrice;

                    // check shipping method
                    if (shippingMethod != null)
                    {
                        ShippingByCountryMethod shippingByCountryMethod = _shippingByCountryService.GetShippingCountryMethod(shippingModel.Id, shippingMethod.Id, getShippingOptionRequest.ShippingAddress);
                        if (shippingByCountryMethod != null)
                        {
                            item.USAddedItemPrice = shippingByCountryMethod.AddedItemPrice;
                            item.USShippingPrice = shippingByCountryMethod.ShippingFee;
                            item.InternationalAddedItemPrice = shippingByCountryMethod.AddedItemPrice;
                            item.InternationalShippingPrice = shippingByCountryMethod.ShippingFee;
                            item.MaxItemPerBox = shippingByCountryMethod.MaxItemsPerBox;
                        }
                    }
                }
                else
                {
                    var defaultShippingModel = shippingCountryOptions
                   .Where(p => Regex.Replace(p.ProductType.ToLower(), "[^0-9a-zA-Z]+", string.Empty).Equals("guystee"))
                   .FirstOrDefault();
                    //default shipping model price

                   if(defaultShippingModel!=null)
                    {

                        item.USAddedItemPrice = defaultShippingModel.USAddedItemPrice;
                        item.USShippingPrice = defaultShippingModel.USShippingPrice;
                        item.InternationalAddedItemPrice = defaultShippingModel.InternationalAddedItemPrice;
                        item.InternationalShippingPrice = defaultShippingModel.InternationalShippingPrice;
                    }
                    else
                    {
                        item.USAddedItemPrice = (decimal)3.5;
                        item.USShippingPrice = (decimal)4.99;
                        item.InternationalAddedItemPrice = (decimal)3.5;
                        item.InternationalShippingPrice = (decimal)4.99;
                    }
                }
            }
            allItems = allItems.OrderByDescending(p => p.USShippingPrice).ToList();
            decimal totalRate = decimal.Zero;
            foreach(var item in allItems)
            {
               int orderCount = item.ProductCount;
               if(countryId==1)
                {
                    //USA shipping Price
                    if (orderCount == 1)
                    {
                        totalRate = totalRate + item.USShippingPrice;
                    }
                    else
                    {
                        if (item.MaxItemPerBox >= orderCount)
                            totalRate = totalRate + item.USShippingPrice + (orderCount - 1) * item.USAddedItemPrice;
                        else
                        {
                            totalRate = totalRate + (orderCount / item.MaxItemPerBox) * item.USShippingPrice;
                            if ((orderCount % item.MaxItemPerBox) > 0)
                                totalRate = totalRate + item.USShippingPrice;
                        }
                    }
                }
               else
                {
                    //International shipping Price
                    if (orderCount == 1)
                    {
                        totalRate = totalRate + item.InternationalShippingPrice;
                    }
                    else
                    {
                        if (item.MaxItemPerBox >= orderCount)
                            totalRate = totalRate + item.InternationalShippingPrice + (orderCount - 1) * item.InternationalAddedItemPrice;
                        else
                        {
                            totalRate = totalRate + (orderCount / item.MaxItemPerBox) * item.InternationalShippingPrice;
                            if ((orderCount % item.MaxItemPerBox) > 0)
                                totalRate = totalRate + item.InternationalShippingPrice;
                        }
                    }
                }
            }
            
            // return response shipping
            var shippingOption = new ShippingOption();
            shippingOption.Name = "Shipping" ;
            shippingOption.Description = "Shipping";
            shippingOption.Rate = totalRate;
            response.ShippingOptions.Add(shippingOption);
            

            return response;
        }

        public ProductAttributeValueModel GetAttributeValue(string xmlProductAttribute, List<ShippingByCountryRecord> sbcRecords)
        {
            XmlDocument XMLDoc = new XmlDocument();
            XMLDoc.LoadXml(xmlProductAttribute);
            var DocElement = XMLDoc.DocumentElement;
            var attr = DocElement.GetElementsByTagName("ProductAttribute");
            var attrValues = DocElement.GetElementsByTagName("ProductAttributeValue");
            ProductAttributeValueModel model = new ProductAttributeValueModel();
            var valueCount = attrValues.Count;
            if(valueCount!=0)
            {
                var regex = new Regex("^[0-9]*$");
                int index = 0;

                for(int i=0; i<valueCount; i++)
                {
                    string attrInnerText = attrValues[i].InnerText;
                    if (regex.IsMatch(attrInnerText))
                    {
                        //true
                        int attrID = int.Parse(attrInnerText);
                        var productAttributeValue = _shippingByCountryService.GetProductTypeByAttributeValueId(attrID);
                        foreach(var item in sbcRecords)
                        {
                            if(item.ProductType.Equals(productAttributeValue))
                            {
                                model.ProductType = productAttributeValue; break;
                            }
                        }
                        
                    }
                    index++;
                }
            }
            else
            {
                //No result
            }

            return model;
        }

        /// <summary>
        /// Gets fixed shipping rate (if shipping rate computation method allows it and the rate can be calculated before checkout).
        /// </summary>
        /// <param name="getShippingOptionRequest">A request for getting shipping options</param>
        /// <returns>Fixed shipping rate; or null in case there's no fixed shipping rate</returns>
        public decimal? GetFixedRate(GetShippingOptionRequest getShippingOptionRequest)
        {
            return null;
        }

        /// <summary>
        /// Gets a route for provider configuration
        /// </summary>
        /// <param name="actionName">Action name</param>
        /// <param name="controllerName">Controller name</param>
        /// <param name="routeValues">Route values</param>
        public void GetConfigurationRoute(out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        {
            actionName = "Configure";
            controllerName = "ShippingByCountry";
            routeValues = new RouteValueDictionary { { "Namespaces", "Nop.Plugin.Shipping.ByCountry.Controllers" }, { "area", null } };
        }
        
        /// <summary>
        /// Install plugin
        /// </summary>
        public override void Install()
        {
            //settings
            var settings = new ShippingByCountrySettings
            {
                LimitMethodsToCreated = false,
            };
            _settingService.SaveSetting(settings);


            //database objects
            _objectContext.Install();

            //locales
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.ByCountry.Fields.Store", "Store");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.ByCountry.Fields.Store.Hint", "If an asterisk is selected, then this shipping rate will apply to all stores.");
            
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.ByCountry.Fields.Country", "Country");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.ByCountry.Fields.Country.Hint", "If an asterisk is selected, then this shipping rate will apply to all customers, regardless of the country.");

            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.ByCountry.Fields.Order", "Order");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.ByCountry.Fields.Order.Hint", "Order.");

            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.ByCountry.Fields.PriceInCountry", "PriceInCountry");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.ByCountry.Fields.PriceInCountry.PriceInCountry", "PriceInCountry.");

            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.ByCountry.Fields.PriceOutCountry", "PriceOutCountry");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.ByCountry.Fields.PriceOutCountry.Hint", "PriceOutCountry.");

            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.ByCountry.Fields.DataHtml", "Data");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.ByCountry.AddRecord", "Add record");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.ByCountry.Formula", "Formula to calculate rates");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.ByCountry.Formula.Value", "[additional fixed cost] + ([order total weight] - [lower weight limit]) * [rate per weight unit] + [order subtotal] * [charge percentage]");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.ByCountry.Fields.LimitMethodsToCreated", "Limit shipping methods to configured ones");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.ByCountry.Fields.LimitMethodsToCreated.Hint", "If you check this option, then your customers will be limited to shipping options configured here. Otherwise, they'll be able to choose any existing shipping options even they've not configured here (zero shipping fee in this case).");
            base.Install();
        }

        /// <summary>
        /// Uninstall plugin
        /// </summary>
        public override void Uninstall()
        {
            //settings
            _settingService.DeleteSetting<ShippingByCountrySettings>();

            //database objects
            _objectContext.Uninstall();

            //locales
            this.DeletePluginLocaleResource("Plugins.Shipping.ByCountry.Fields.Store");
            this.DeletePluginLocaleResource("Plugins.Shipping.ByCountry.Fields.Store.Hint");
            this.DeletePluginLocaleResource("Plugins.Shipping.ByCountry.Fields.Country");
            this.DeletePluginLocaleResource("Plugins.Shipping.ByCountry.Fields.Country.Hint");
            this.DeletePluginLocaleResource("Plugins.Shipping.ByCountry.Fields.Order.Hint");
            this.DeletePluginLocaleResource("Plugins.Shipping.ByCountry.Fields.Order");
            this.DeletePluginLocaleResource("Plugins.Shipping.ByCountry.Fields.PriceInCountry");
            this.DeletePluginLocaleResource("Plugins.Shipping.ByCountry.Fields.PriceInCountry.Hint");
            this.DeletePluginLocaleResource("Plugins.Shipping.ByCountry.Fields.PriceOutCountry");
            this.DeletePluginLocaleResource("Plugins.Shipping.ByCountry.Fields.PriceOutCountry.Hint");

            this.DeletePluginLocaleResource("Plugins.Shipping.ByCountry.Fields.LimitMethodsToCreated");
            this.DeletePluginLocaleResource("Plugins.Shipping.ByCountry.Fields.LimitMethodsToCreated.Hint");
            this.DeletePluginLocaleResource("Plugins.Shipping.ByCountry.Fields.DataHtml");
            this.DeletePluginLocaleResource("Plugins.Shipping.ByCountry.AddRecord");
            this.DeletePluginLocaleResource("Plugins.Shipping.ByCountry.Formula");
            this.DeletePluginLocaleResource("Plugins.Shipping.ByCountry.Formula.Value");
            
            base.Uninstall();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets a shipping rate computation method type
        /// </summary>
        public ShippingRateComputationMethodType ShippingRateComputationMethodType
        {
            get
            {
                return ShippingRateComputationMethodType.Offline;
            }
        }


        /// <summary>
        /// Gets a shipment tracker
        /// </summary>
        public IShipmentTracker ShipmentTracker
        {
            get
            {
                //uncomment a line below to return a general shipment tracker (finds an appropriate tracker by tracking number)
                //return new GeneralShipmentTracker(EngineContext.Current.Resolve<ITypeFinder>());
                return null; 
            }
        }

        #endregion
    }
}
