﻿using Nop.Core;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Web.Framework.Controllers;
using Nop.Services.Logging;
using Nop.Services.Payments;
using Nop.Core.Domain.Payments;
using Nop.Services.Orders;
using Nop.Core.Domain.Orders;
using Nop.Services.Common;
using Nop.Core.Domain.Common;
using System.Web.Mvc;
using Nop.Web.Framework.Security;
using System;
using Nop.Plugin.Payments.Stripe.Models;
using System.Collections.Generic;
using Nop.Core.Domain.Logging;
using System.Net;
using System.Linq;
using Nop.Plugin.Payments.Stripe.Validators;
using Nop.Services;
using Nop.Services.Directory;
using Nop.Core.Domain.Directory;
using Stripe;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Catalog;

namespace Nop.Plugin.Payments.Stripe.Controllers
{
    public class PaymentStripeController : BasePaymentController
    {
        #region Fields

        private readonly ILanguageService _languageService;
        private readonly ILocalizationService _localizationService;
        private readonly IPermissionService _permissionService;
        private readonly ISettingService _settingService;
        private readonly IStoreService _storeService;
        private readonly IWorkContext _workContext;
        private readonly ILogger _logger;
        private readonly IPaymentService _paymentService;
        private readonly PaymentSettings _paymentSettings;
        private readonly IWebHelper _webHelper;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly OrderSettings _orderSettings;
        private readonly IStoreContext _storeContext;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IAddressAttributeParser _addressAttributeParser;
        private readonly IAddressAttributeService _addressAttributeService;
        private readonly AddressSettings _addressSettings;
        private readonly IOrderService _orderService;
        private readonly ICurrencyService _currencyService;
        private readonly CurrencySettings _currencySettings;

        public static readonly string[] ZeroDecimal = { "BIF", "CLP", "DJF", "GNF", "JPY", "KMF", "KRW", "MGA", "PYG", "RWF", "UGX", "VND", "VUV", "XAF", "XOF", "XPF" };

        #endregion

        #region Ctor

        public PaymentStripeController(ILanguageService languageService,
            ILocalizationService localizationService,
            IPermissionService permissionService,
            ISettingService settingService,
            IStoreService storeService,
            IWorkContext workContext,
            ILogger logger,
            IPaymentService paymentService,
            PaymentSettings paymentSettings,
            IWebHelper webHelper,
            IOrderProcessingService orderProcessingService,
            OrderSettings orderSettings,
            IStoreContext storeContext,
            IShoppingCartService shoppingCartService,
            IGenericAttributeService genericAttributeService,
            IAddressAttributeParser addressAttributeParser,
            IAddressAttributeService addressAttributeService,
            AddressSettings addressSettings,
            IOrderService orderService,
            ICurrencyService currencyService,
            CurrencySettings currencySettings
            )
        {
            this._languageService = languageService;
            this._localizationService = localizationService;
            this._permissionService = permissionService;
            this._settingService = settingService;
            this._storeService = storeService;
            this._workContext = workContext;
            this._logger = logger;
            this._paymentService = paymentService;
            this._paymentSettings = paymentSettings;
            this._webHelper = webHelper;
            this._orderProcessingService = orderProcessingService;
            this._orderSettings = orderSettings;
            this._storeContext = storeContext;
            this._shoppingCartService = shoppingCartService;
            this._genericAttributeService = genericAttributeService;
            this._addressAttributeParser = addressAttributeParser;
            this._addressAttributeService = addressAttributeService;
            this._addressSettings = addressSettings;
            this._orderService = orderService;
            this._currencyService = currencyService;
            this._currencySettings = currencySettings;
        }

        #endregion

        #region Methods

        [ChildActionOnly]
        [AdminAntiForgery]
        public ActionResult Configure()
        {
            //load settings for a chosen store scope
            var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var stripePaymentSettings = _settingService.LoadSetting<StripePaymentSettings>(storeScope);
           
            var model = new ConfigurationModel();
            model.TransactModeId = (int)stripePaymentSettings.TransactMode;
            model.AdditionalFee = stripePaymentSettings.AdditionalFee;
            model.AdditionalFeePercentage = stripePaymentSettings.AdditionalFeePercentage;
            model.Enable3DSecure = stripePaymentSettings.Enable3DSecure;
            model.TransactModeValues = stripePaymentSettings.TransactMode.ToSelectList();
            model.SecretApiKey = stripePaymentSettings.SecretApiKey;
            model.ApiKey = stripePaymentSettings.ApiKey;

            model.ActiveStoreScopeConfiguration = storeScope;
            if (storeScope > 0)
            {
                model.TransactModeId_OverrideForStore = _settingService.SettingExists(stripePaymentSettings, x => x.TransactMode, storeScope);
                model.AdditionalFee_OverrideForStore = _settingService.SettingExists(stripePaymentSettings, x => x.AdditionalFee, storeScope);
                model.AdditionalFeePercentage_OverrideForStore = _settingService.SettingExists(stripePaymentSettings, x => x.AdditionalFeePercentage, storeScope);
                model.Enable3DSecure_OverrideForStore = _settingService.SettingExists(stripePaymentSettings, x => x.Enable3DSecure, storeScope);
                model.ApiKey_OverrideForStore = _settingService.SettingExists(stripePaymentSettings, x => x.ApiKey, storeScope);
                model.SecretApiKey_OverrideForStore = _settingService.SettingExists(stripePaymentSettings, x => x.SecretApiKey, storeScope);
            }
            
            return View("~/Plugins/Payments.Stripe/Views/Configure.cshtml", model);
        }

        [HttpPost]
        [AdminAntiForgery]
        public ActionResult Configure(ConfigurationModel model)
        {
            if (!ModelState.IsValid)
                return Configure();

            //load settings for a chosen store scope
            var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var stripePaymentSettings = _settingService.LoadSetting<StripePaymentSettings>(storeScope);

            //save settings
            stripePaymentSettings.TransactMode = (TransactMode)model.TransactModeId;
            stripePaymentSettings.AdditionalFee = model.AdditionalFee;
            stripePaymentSettings.Enable3DSecure = model.Enable3DSecure;
            stripePaymentSettings.AdditionalFeePercentage = model.AdditionalFeePercentage;
            stripePaymentSettings.ApiKey = model.ApiKey;
            stripePaymentSettings.SecretApiKey = model.SecretApiKey;

            /* We do not clear cache after each setting update.
             * This behavior can increase performance because cached settings will not be cleared 
             * and loaded from database after each update */

            if (model.TransactModeId_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(stripePaymentSettings, x => x.TransactMode, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(stripePaymentSettings, x => x.TransactMode, storeScope);

            if (model.AdditionalFee_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(stripePaymentSettings, x => x.AdditionalFee, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(stripePaymentSettings, x => x.AdditionalFee, storeScope);

            if (model.Enable3DSecure_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(stripePaymentSettings, x => x.Enable3DSecure, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(stripePaymentSettings, x => x.Enable3DSecure, storeScope);

            if (model.AdditionalFeePercentage_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(stripePaymentSettings, x => x.AdditionalFeePercentage, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(stripePaymentSettings, x => x.AdditionalFeePercentage, storeScope);

            if (model.ApiKey_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(stripePaymentSettings, x => x.ApiKey, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(stripePaymentSettings, x => x.ApiKey, storeScope);

            if (model.SecretApiKey_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(stripePaymentSettings, x => x.SecretApiKey, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(stripePaymentSettings, x => x.SecretApiKey, storeScope);

            //now clear settings cache
            _settingService.ClearCache();

            SuccessNotification(_localizationService.GetResource("Admin.Plugins.Saved"));

            return Configure();
        }

        [ChildActionOnly]
        public ActionResult PaymentInfo()
        {
            var model = new PaymentInfoModel
            {
                CreditCardTypes = new List<SelectListItem>
                {
                    new SelectListItem { Text = "Visa", Value = "visa" },
                    new SelectListItem { Text = "Master card", Value = "MasterCard" },
                    new SelectListItem { Text = "Amex", Value = "Amex" },
                     new SelectListItem { Text = "American Express", Value = "AmericanExpress" },
                    new SelectListItem { Text = "Discover", Value = "Discover" },
                    new SelectListItem { Text = "Diners Club", Value = "DinersClub" },
                    new SelectListItem { Text = "JCB", Value = "JCB" },
                    new SelectListItem { Text = "UnionPay", Value = "UnionPay" },
                }
            };

            //years
            for (var i = 0; i < 15; i++)
            {
                var year = (DateTime.Now.Year + i).ToString();
                model.ExpireYears.Add(new SelectListItem { Text = year, Value = year, });
            }

            //months
            for (var i = 1; i <= 12; i++)
            {
                model.ExpireMonths.Add(new SelectListItem { Text = i.ToString("D2"), Value = i.ToString(), });
            }

            //set postback values (we cannot access "Form" with "GET" requests)
            if (this.Request.HttpMethod != WebRequestMethods.Http.Get)
            {
                var form = this.Request.Form;
                model.CardholderName = form["CardholderName"];
                model.CardNumber = form["CardNumber"];
                model.CardCode = form["CardCode"];
                var selectedCcType = model.CreditCardTypes.FirstOrDefault(x => x.Value.Equals(form["CreditCardType"], StringComparison.InvariantCultureIgnoreCase));
                if (selectedCcType != null)
                    selectedCcType.Selected = true;
                var selectedMonth = model.ExpireMonths.FirstOrDefault(x => x.Value.Equals(form["ExpireMonth"], StringComparison.InvariantCultureIgnoreCase));
                if (selectedMonth != null)
                    selectedMonth.Selected = true;
                var selectedYear = model.ExpireYears.FirstOrDefault(x => x.Value.Equals(form["ExpireYear"], StringComparison.InvariantCultureIgnoreCase));
                if (selectedYear != null)
                    selectedYear.Selected = true;
            }

            if (_orderSettings.OnePageCheckoutEnabled)
                return View("~/Plugins/Payments.Stripe/Views/PaymentInfoOpc.cshtml", model);
            else
                return View("~/Plugins/Payments.Stripe/Views/PaymentInfo.cshtml", model);
        }

        [NonAction]
        public override IList<string> ValidatePaymentForm(FormCollection form)
        {
            var warnings = new List<string>();

            //validate
            var validator = new PaymentInfoValidator(_localizationService);
            var model = new PaymentInfoModel
            {
                CardholderName = form["CardholderName"],
                CardNumber = form["CardNumber"],
                CardCode = form["CardCode"],
                ExpireMonth = form["ExpireMonth"],
                ExpireYear = form["ExpireYear"]
            };
            var validationResult = validator.Validate(model);
            if (!validationResult.IsValid)
                warnings.AddRange(validationResult.Errors.Select(error => error.ErrorMessage));

            return warnings;
        }

        [NonAction]
        public override ProcessPaymentRequest GetPaymentInfo(FormCollection form)
        {
            var paymentInfo = new ProcessPaymentRequest();
            paymentInfo.CreditCardType = form["CreditCardType"];
            paymentInfo.CreditCardName = form["CardholderName"];
            paymentInfo.CreditCardNumber = form["CardNumber"];
            paymentInfo.CreditCardExpireMonth = int.Parse(form["ExpireMonth"]);
            paymentInfo.CreditCardExpireYear = int.Parse(form["ExpireYear"]);
            paymentInfo.CreditCardCvv2 = form["CardCode"];
            return paymentInfo;
        }

        public ActionResult StripeHandler(string client_secret, bool livemode, string source)
        {
            //load settings for a chosen store scope
            var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var stripePaymentSettings = _settingService.LoadSetting<StripePaymentSettings>(storeScope);

            var sourceService = new StripeSourceService(stripePaymentSettings.SecretApiKey);
            StripeSource requestSource = sourceService.Get(source);

            var orderNumber = string.Empty;
            requestSource.Metadata.TryGetValue("OrderGuid", out orderNumber);
            var orderNumberGuid = Guid.Empty;
            try
            {
                orderNumberGuid = new Guid(orderNumber);
            }
            catch { }
            var order = _orderService.GetOrderByGuid(orderNumberGuid);
            if (order != null)
            {
                if (requestSource.ClientSecret == client_secret && requestSource.Status == "chargeable")
                {
                    if (order.OrderItems.Select(p => p.Product).Any(p => p.IsRecurring))
                    {
                        StripeChargeCreateOptions chargeOptions = new StripeChargeCreateOptions()
                        {
                            Amount = requestSource.Amount,
                            Currency = requestSource.Currency,
                            SourceTokenOrExistingSourceId = requestSource.Id,
                            CustomerId = requestSource.ThreeDSecure.CustomerId,
                            Description = $"Invoice-{order.Id}"
                        };

                        var chargeService = new StripeChargeService();
                        chargeService.ApiKey = stripePaymentSettings.SecretApiKey;
                        StripeCharge stripeCharge = chargeService.Create(chargeOptions);
                        foreach (var item in order.OrderItems)
                        {
                            StripePlan stripePlan;
                            try
                            {
                                var planService = new StripePlanService();
                                stripePlan = planService.Get(_workContext.WorkingCurrency.Name + "_" + item.ProductId.ToString());
                            }
                            catch (Exception ex)
                            {
                                var Product = new StripeProductCreateOptions()
                                {
                                    Name = item.Product.Name,
                                    Id = "prod_" + item.ProductId,
                                    Active = true,
                                    Type = "service"
                                };

                                var productService = new StripeProductService(stripePaymentSettings.SecretApiKey);
                                StripeProduct product = productService.Create(Product);

                                var currencyTmp = _currencyService.GetCurrencyById(
                                    order.Customer.GetAttribute<int>(SystemCustomerAttributeNames.CurrencyId, order.StoreId));

                                var customerCurrency = (currencyTmp != null && currencyTmp.Published) ? currencyTmp : _workContext.WorkingCurrency;
                                var primaryStoreCurrency = _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId);
                                var CustomerCurrencyCode = customerCurrency.CurrencyCode;
                                var CustomerCurrencyRate = customerCurrency.Rate / primaryStoreCurrency.Rate;


                                var allcurrency = _currencyService.GetAllCurrencies(storeId: _storeContext.CurrentStore.Id);
                                StripePlan currentPlan = new StripePlan();
                                foreach (var currency in allcurrency)
                                {
                                    int paymentAmount = 0;
                                    if (Array.Exists(ZeroDecimal, z => z == CustomerCurrencyCode))
                                        paymentAmount = (int)Math.Ceiling(_currencyService.ConvertCurrency(item.Product.Price, currency.Rate / primaryStoreCurrency.Rate));
                                    else
                                        paymentAmount = (int)Math.Ceiling(_currencyService.ConvertCurrency(item.Product.Price, currency.Rate / primaryStoreCurrency.Rate) * 100);

                                    var planOptions = new StripePlanCreateOptions()
                                    {
                                        Id = currency.Name + "_" + item.ProductId,
                                        Amount = paymentAmount,
                                        Currency = currency.CurrencyCode,
                                        Interval = item.Product.RecurringCyclePeriod.ToString().TrimEnd('s').ToLower(),
                                        IntervalCount = item.Product.RecurringCycleLength,
                                        ProductId = product.Id
                                    };

                                    var planService = new StripePlanService();
                                    var stripePlanCurrency = planService.Create(planOptions);

                                    if (currency.CurrencyCode == CustomerCurrencyCode)
                                        currentPlan = stripePlanCurrency;
                                }
                                stripePlan = currentPlan;
                            }

                            var items = new List<StripeSubscriptionItemOption> {
                                            new StripeSubscriptionItemOption {
                                                PlanId = stripePlan.Id, Quantity = item.Quantity
                                            },

                                        };

                            var customerService = new StripeCustomerService(stripePaymentSettings.SecretApiKey);
                            StripeCustomer customer = customerService.Get(stripeCharge.CustomerId);

                            var trialEnd = DateTime.UtcNow;
                            switch (item.Product.RecurringCyclePeriod)
                            {
                                case RecurringProductCyclePeriod.Days:
                                    trialEnd = trialEnd.AddDays(item.Product.RecurringCycleLength);
                                    break;
                                case RecurringProductCyclePeriod.Months:
                                    trialEnd = trialEnd.AddMonths(item.Product.RecurringCycleLength);
                                    break;
                                case RecurringProductCyclePeriod.Weeks:
                                    trialEnd = trialEnd.AddDays(item.Product.RecurringCycleLength * 7);
                                    break;
                                case RecurringProductCyclePeriod.Years:
                                    trialEnd = trialEnd.AddYears(item.Product.RecurringCycleLength);
                                    break;
                                default:
                                    break;
                            }

                            decimal? additionalFee = 0;
                            if (stripePaymentSettings.AdditionalFeePercentage)
                                additionalFee = stripePaymentSettings.AdditionalFee;
                            else
                                additionalFee = ((decimal)stripePaymentSettings.AdditionalFee / order.OrderTotal) * 100;

                            var options = new StripeSubscriptionCreateOptions
                            {
                                Items = items,
                                TrialEnd = trialEnd,
                                TaxPercent = additionalFee
                            };
                            try
                            {
                                var service = new StripeSubscriptionService(stripePaymentSettings.SecretApiKey);
                                StripeSubscription subscription = service.Create(stripeCharge.CustomerId, options);

                                //mark order as paid
                                if (subscription.Status == "trialing")
                                {
                                    if (_orderProcessingService.CanMarkOrderAsPaid(order))
                                    {
                                        order.SubscriptionTransactionId = subscription.Id;

                                        _orderService.UpdateOrder(order);

                                        _orderProcessingService.MarkOrderAsPaid(order);

                                        var rp = _orderService.SearchRecurringPayments(initialOrderId: order.Id).FirstOrDefault();
                                        rp.RecurringPaymentHistory.Add(new RecurringPaymentHistory
                                        {
                                            RecurringPayment = rp,
                                            CreatedOnUtc = DateTime.UtcNow,
                                            OrderId = order.Id,
                                        });
                                        _orderService.UpdateRecurringPayment(rp);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                order.OrderNotes.Add(new OrderNote
                                {
                                    Note = ex.Message.ToString(),
                                    DisplayToCustomer = false,
                                    CreatedOnUtc = DateTime.UtcNow
                                });

                                _orderService.UpdateOrder(order);
                            }
                        }
                    }
                    else
                    {
                        StripeChargeCreateOptions chargeOptions = new StripeChargeCreateOptions()
                        {
                            Amount = requestSource.Amount,
                            Currency = requestSource.Currency,
                            SourceTokenOrExistingSourceId = requestSource.Id,
                            Description = $"Invoice-{order.Id}"
                        };

                        var chargeService = new StripeChargeService();
                        chargeService.ApiKey = stripePaymentSettings.SecretApiKey;
                        StripeCharge stripeCharge = chargeService.Create(chargeOptions);

                        if (stripeCharge.Status == "succeeded")
                        {
                            //order note
                            order.OrderNotes.Add(new OrderNote
                            {
                                Note = stripeCharge.StripeResponse.ResponseJson.ToString(),
                                DisplayToCustomer = false,
                                CreatedOnUtc = DateTime.UtcNow
                            });
                            _orderService.UpdateOrder(order);

                            //mark order as paid
                            if (stripeCharge.Paid)
                            {
                                if (_orderProcessingService.CanMarkOrderAsPaid(order))
                                {
                                    order.CaptureTransactionId = stripeCharge.Id;
                                    order.CaptureTransactionResult = stripeCharge.Status;

                                    _orderService.UpdateOrder(order);

                                    _orderProcessingService.MarkOrderAsPaid(order);
                                }
                            }
                        }
                        else
                        {
                            order.OrderNotes.Add(new OrderNote
                            {
                                Note = stripeCharge.StripeResponse.ResponseJson.ToString(),
                                DisplayToCustomer = false,
                                CreatedOnUtc = DateTime.UtcNow
                            });

                            _orderService.UpdateOrder(order);
                        }
                    }

                    return RedirectToRoute("CheckoutCompleted", new { orderId = order.Id });
                }
                else
                {
                    var customerOrder = _orderService.SearchOrders(storeId: _storeContext.CurrentStore.Id,
                        customerId: _workContext.CurrentCustomer.Id, pageSize: 1).FirstOrDefault();
                    if (customerOrder != null)
                    {
                        if (customerOrder.PaymentStatus == PaymentStatus.Authorized)
                            return RedirectToRoute("CheckoutCompleted", new { orderId = customerOrder.Id });
                        else
                            return RedirectToRoute("OrderDetails", new { orderId = order.Id });
                    }

                    return RedirectToRoute("HomePage");
                }
            }
            return RedirectToRoute("HomePage");
        }

        #endregion
    }
}
