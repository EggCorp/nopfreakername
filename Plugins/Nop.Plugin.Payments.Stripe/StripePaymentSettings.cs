﻿using Nop.Core.Configuration;

namespace Nop.Plugin.Payments.Stripe
{
    public class StripePaymentSettings : ISettings
    {
        public TransactMode TransactMode { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether to "additional fee" is specified as percentage. true - percentage, false - fixed value.
        /// </summary>
        public bool AdditionalFeePercentage { get; set; }
        /// <summary>
        /// Additional fee
        /// </summary>
        public decimal AdditionalFee { get; set; }

        /// <summary>
        /// Enable 3D Secure
        /// </summary>
        public bool Enable3DSecure { get; set; }

        /// <summary>
        /// Secret Api Key
        /// </summary>
        public string SecretApiKey { get; set; }

        /// <summary>
        /// Api Key
        /// </summary>
        public string ApiKey { get; set; }

        /// <summary>
        /// License Key
        /// </summary>
        public bool IsValid()
        {
            bool valid = true;
            bool flag = string.IsNullOrEmpty(this.ApiKey);
            if (flag)
            {
                valid = false;
            }
            return valid;
        }
    }
}
