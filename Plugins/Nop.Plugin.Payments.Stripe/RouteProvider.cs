﻿using Nop.Web.Framework.Mvc.Routes;
using System.Web.Mvc;
using System.Web.Routing;

namespace Nop.Plugin.Payments.Stripe
{
    public partial class RouteProvider : IRouteProvider
    {
        public void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute("Nop.Plugin.Payment.Stripe.Configure", "Admin/Plugins/Payment/Stripe/Configure",
                      new { controller = "PaymentStripe", action = "Configure" },
                      new[] { "Nop.Plugin.Payments.Stripe.Controllers" }
                      ).DataTokens.Add("area", "admin");

        }
        public int Priority
        {
            get
            {
                return 0;
            }
        }
    }
}
