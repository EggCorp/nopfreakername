﻿using Nop.Core;
using Nop.Core.Domain.Directory;
using Nop.Core.Plugins;
using Nop.Services.Configuration;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Stores;
using Nop.Services.Logging;
using Stripe;
using System.Net;
using Nop.Core.Domain.Payments;
using System;
using System.Collections.Generic;
using Nop.Core.Domain.Orders;
using System.Web.Routing;
using Nop.Plugin.Payments.Stripe.Controllers;
using Nop.Services.Customers;
using System.Linq;
using System.Web;
using Nop.Services.Security;
using Nop.Services.Common;
using Nop.Core.Domain.Customers;

namespace Nop.Plugin.Payments.Stripe
{
    public class StripeProcessor : BasePlugin, IPaymentMethod, IPlugin
    {
        #region Fields

        private readonly CurrencySettings _currencySettings;
        private readonly StripePaymentSettings _stripePaymentSettings;
        private readonly ISettingService _settingService;
        private readonly IOrderTotalCalculationService _orderTotalCalculationService;
        private readonly ICurrencyService _currencyService;
        private readonly IWebHelper _webHelper;
        private readonly ILocalizationService _localizationService;
        private readonly IWorkContext _workContext;
        private readonly StripeChargeService _chargeService;
        private readonly StripeTokenService _tokenService;
        private readonly StripeRefundService _refundService;
        private readonly IStoreService _storeService;
        private readonly IStoreContext _storeContext;
        private readonly ILogger _logger;
        private readonly ICustomerService _customerService;
        private readonly IOrderService _orderService;
        private readonly HttpContextBase _httpContext;
        private readonly IEncryptionService _encryptionService;
        private readonly IOrderProcessingService _orderProcessingService;

        public static readonly string[] ZeroDecimal = { "BIF", "CLP", "DJF", "GNF", "JPY", "KMF", "KRW", "MGA", "PYG", "RWF", "UGX", "VND", "VUV", "XAF", "XOF", "XPF" };

        #endregion

        #region Ctor

        public StripeProcessor(CurrencySettings currencySettings,
            StripePaymentSettings stripePaymentSettings,
            ISettingService settingService, IOrderTotalCalculationService orderTotalCalculationService,
            ICurrencyService currencyService,
            IWebHelper webHelper,
            ILocalizationService localizationService,
            IWorkContext workContext,
            IStoreService storeService,
            IStoreContext storeContext,
            ILogger logger,
            ICustomerService customerService,
            IOrderService orderService,
            HttpContextBase httpContext,
            IEncryptionService encryptionService,
            IOrderProcessingService orderProcessingService)
        {
            this._currencySettings = currencySettings;
            this._stripePaymentSettings = stripePaymentSettings;
            this._settingService = settingService;
            this._orderTotalCalculationService = orderTotalCalculationService;
            this._currencyService = currencyService;
            this._webHelper = webHelper;
            this._localizationService = localizationService;
            this._workContext = workContext;
            this._storeService = storeService;
            this._storeContext = storeContext;
            this._logger = logger;
            this._customerService = customerService;
            this._orderService = orderService;
            this._httpContext = httpContext;
            this._encryptionService = encryptionService;
            this._orderProcessingService = orderProcessingService;

            bool flag = !stripePaymentSettings.IsValid();
            if (!flag)
            {
                StripeConfiguration.SetApiKey(stripePaymentSettings.SecretApiKey);
                this._chargeService = new StripeChargeService(null);
                this._tokenService = new StripeTokenService(null);
                this._refundService = new StripeRefundService(null);
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Process a payment
        /// </summary>
        /// <param name="processPaymentRequest">Payment info required for an order processing</param>
        /// <returns>Process payment result</returns>
        public ProcessPaymentResult ProcessPayment(ProcessPaymentRequest processPaymentRequest)
        {
            ProcessPaymentResult processPaymentResult = new ProcessPaymentResult();
            try
            {
                return !(_stripePaymentSettings.TransactMode == TransactMode.Authorize) || !SupportCapture ? AuthorizeAndCapture(processPaymentRequest, true) : AuthorizeAndCapture(processPaymentRequest, false);
            }
            catch (StripeException ex)
            {
                if (!string.IsNullOrEmpty(ex.StripeError.Code))
                    processPaymentResult.AddError(ex.StripeError.Message + " Error code: " + ex.StripeError.Code);
                else
                    processPaymentResult.AddError(ex.StripeError.Message);
                return processPaymentResult;
            }
        }

        internal ProcessPaymentResult AuthorizeAndCapture(ProcessPaymentRequest processPaymentRequest, bool capture)
        {
            ProcessPaymentResult result = new ProcessPaymentResult();
            result.AllowStoringCreditCardNumber = false;

            if (_stripePaymentSettings.Enable3DSecure)
                return result;

            StripeCreditCardOptions cardOptions = new StripeCreditCardOptions()
            {
                Number = processPaymentRequest.CreditCardNumber,
                ExpirationYear = processPaymentRequest.CreditCardExpireYear,
                ExpirationMonth = processPaymentRequest.CreditCardExpireMonth,
                Cvc = processPaymentRequest.CreditCardCvv2,

            };

            var customer = _customerService.GetCustomerById(processPaymentRequest.CustomerId);
            if (customer == null)
                throw new Exception("Customer cannot be loaded");

            cardOptions.AddressLine1 = customer.BillingAddress.Address1 ?? string.Empty;
            cardOptions.AddressLine2 = customer.BillingAddress.Address2 ?? string.Empty;
            cardOptions.AddressCity = customer.BillingAddress.City ?? string.Empty;
            cardOptions.AddressState = customer.BillingAddress.StateProvince != null ? customer.BillingAddress.StateProvince.Name : string.Empty;
            cardOptions.AddressCountry = customer.BillingAddress.Country != null ? customer.BillingAddress.Country.Name : string.Empty;
            cardOptions.AddressZip = customer.BillingAddress.ZipPostalCode ?? string.Empty;
            cardOptions.Name = processPaymentRequest.CreditCardName;


            var token = CreateToken(cardOptions);

            var currencyTmp = _currencyService.GetCurrencyById(
                customer.GetAttribute<int>(SystemCustomerAttributeNames.CurrencyId, processPaymentRequest.StoreId));

            var customerCurrency = (currencyTmp != null && currencyTmp.Published) ? currencyTmp : _workContext.WorkingCurrency;
            var primaryStoreCurrency = _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId);
            var CustomerCurrencyCode = customerCurrency.CurrencyCode;
            var CustomerCurrencyRate = customerCurrency.Rate / primaryStoreCurrency.Rate;

            int paymentAmount = 0;
            if (Array.Exists(ZeroDecimal, z => z == CustomerCurrencyCode))
                paymentAmount = (int)Math.Ceiling(_currencyService.ConvertCurrency(processPaymentRequest.OrderTotal, CustomerCurrencyRate));
            else
                paymentAmount = (int)Math.Ceiling(_currencyService.ConvertCurrency(processPaymentRequest.OrderTotal, CustomerCurrencyRate) * 100);

            StripeChargeCreateOptions chargeOptions = new StripeChargeCreateOptions()
            {
                Amount = paymentAmount,
                Currency = CustomerCurrencyCode,
                Description = processPaymentRequest.OrderGuid.ToString(),
                Capture = capture,
                SourceTokenOrExistingSourceId = token,
                ReceiptEmail = customer.BillingAddress.Email != null ? customer.BillingAddress.Email : string.Empty
            };

            var chargeService = new StripeChargeService();
            chargeService.ApiKey = _stripePaymentSettings.SecretApiKey;
            StripeCharge stripeCharge = chargeService.Create(chargeOptions);

            if (stripeCharge.Status != "succeeded" && (stripeCharge.Source.Card.AddressLine1Check == "fail" || stripeCharge.Source.Card.AddressZipCheck == "fail"))
            {
                result.AddError("Address verification failed, please correct your address information. If this error persists, contact us.");
            }
            else if (stripeCharge.Status != "succeeded" && stripeCharge.Source.Card.CvcCheck == "fail")
            {
                result.AddError("Cvc verification failed, please correct your information. If this error persists, contact us.");
            }
            else if (capture)
            {
                result.NewPaymentStatus = PaymentStatus.Paid;
                result.CaptureTransactionResult = stripeCharge.Status;
                result.CaptureTransactionId = stripeCharge.Id;
            }
            else
            {
                result.NewPaymentStatus = PaymentStatus.Authorized;
                result.AuthorizationTransactionResult = stripeCharge.Status;
                result.AuthorizationTransactionId = stripeCharge.Id;
            }
            if (stripeCharge.Status == "succeeded" && stripeCharge.Source.Card.AddressLine1Check == "fail")
                _logger.Warning("Stripe charge is successful but AddressLine1 Check has failed. OrderGuid: " + processPaymentRequest.OrderGuid + ". Stripe ChargeId: " + stripeCharge.Id, null, null);
            if (stripeCharge.Status == "succeeded" && stripeCharge.Source.Card.AddressZipCheck == "fail")
                _logger.Warning("Stripe charge is successful but Address Zip Check has failed. OrderGuid: " + processPaymentRequest.OrderGuid + ". Stripe ChargeId: " + stripeCharge.Id, null, null);
            if (stripeCharge.Status == "succeeded" && stripeCharge.Source.Card.CvcCheck == "fail")
                _logger.Warning("Stripe charge is successful but Cvc Check has failed. OrderGuid: " + processPaymentRequest.OrderGuid + ". Stripe ChargeId: " + stripeCharge.Id, null, null);

            return result;
        }

        private string CreateToken(StripeCreditCardOptions card)
        {
            var myToken = new StripeTokenCreateOptions
            {
                Card = card
            };

            var tokenService = new StripeTokenService();
            tokenService.ApiKey = _stripePaymentSettings.ApiKey;
            var stripeToken = tokenService.Create(myToken);
            return stripeToken.Id;
        }

        private StripeSource CreateSource(StripeCreditCardOptions card, StripeSourceOwner owner)
        {
            var cardOptions = new StripeSourceCreateOptions()
            {
                Card = card,
                Type = "card",
                Owner = owner
            };

            var service = new StripeSourceService(_stripePaymentSettings.ApiKey);
            StripeSource SourceCard = service.Create(cardOptions);

            return SourceCard;
        }

        /// <summary>
        /// Post process payment (used by payment gateways that require redirecting to a third-party URL)
        /// </summary>
        /// <param name="postProcessPaymentRequest">Payment info required for an order processing</param>
        public void PostProcessPayment(PostProcessPaymentRequest postProcessPaymentRequest)
        {
            if (!_stripePaymentSettings.Enable3DSecure)
                return;

            StripeCreditCardOptions cardOptions = new StripeCreditCardOptions()
            {
                Number = _encryptionService.DecryptText(postProcessPaymentRequest.Order.CardNumber),
                ExpirationYear = Convert.ToInt32(_encryptionService.DecryptText(postProcessPaymentRequest.Order.CardExpirationYear)),
                ExpirationMonth = Convert.ToInt32(_encryptionService.DecryptText(postProcessPaymentRequest.Order.CardExpirationMonth)),
                Cvc = _encryptionService.DecryptText(postProcessPaymentRequest.Order.CardCvv2),
            };

            StripeSourceOwner owner = new StripeSourceOwner();

            if (postProcessPaymentRequest.Order.BillingAddress != null)
            {
                owner.Line1 = postProcessPaymentRequest.Order.BillingAddress.Address1 ?? string.Empty;
                owner.Line2 = postProcessPaymentRequest.Order.BillingAddress.Address2 ?? string.Empty;
                owner.CityOrTown = postProcessPaymentRequest.Order.BillingAddress.City ?? string.Empty;
                owner.State = postProcessPaymentRequest.Order.BillingAddress.StateProvince != null ? postProcessPaymentRequest.Order.BillingAddress.StateProvince.Name : string.Empty;
                owner.Country = postProcessPaymentRequest.Order.BillingAddress.Country != null ? postProcessPaymentRequest.Order.BillingAddress.Country.Name : string.Empty;
                owner.PostalCode = postProcessPaymentRequest.Order.BillingAddress.ZipPostalCode ?? string.Empty;
                owner.Email = postProcessPaymentRequest.Order.BillingAddress.Email != null ? postProcessPaymentRequest.Order.BillingAddress.Email : postProcessPaymentRequest.Order.BillingAddress.Email ?? string.Empty;
                owner.Name = _encryptionService.DecryptText(postProcessPaymentRequest.Order.CardName);
            }

            int paymentAmount = 0;
            if (Array.Exists(ZeroDecimal, z => z == postProcessPaymentRequest.Order.CustomerCurrencyCode))
                paymentAmount = (int)Math.Ceiling(_currencyService.ConvertCurrency(postProcessPaymentRequest.Order.OrderTotal, postProcessPaymentRequest.Order.CurrencyRate));
            else
                paymentAmount = (int)Math.Ceiling(_currencyService.ConvertCurrency(postProcessPaymentRequest.Order.OrderTotal, postProcessPaymentRequest.Order.CurrencyRate) * 100);

            var token = string.Empty;
            StripeSource sourceCard = new StripeSource();

            sourceCard = CreateSource(cardOptions, owner);

            if (sourceCard.Card.ThreeDSecure == "not_supported")
            {
                if (postProcessPaymentRequest.Order.OrderItems.Select(p => p.Product).Any(p => p.IsRecurring))
                {
                    try
                    {
                        var customer = _customerService.GetCustomerById(postProcessPaymentRequest.Order.CustomerId);
                        var order = postProcessPaymentRequest.Order;

                        var StripeRequestOptions = new StripeRequestOptions();
                        StripeRequestOptions.ApiKey = _stripePaymentSettings.ApiKey;

                        cardOptions.Name = _encryptionService.DecryptText(postProcessPaymentRequest.Order.CardName);
                        cardOptions.AddressLine1 = customer.BillingAddress.Address1 ?? string.Empty;
                        cardOptions.AddressLine2 = customer.BillingAddress.Address2 ?? string.Empty;
                        cardOptions.AddressCity = customer.BillingAddress.City ?? string.Empty;
                        cardOptions.AddressState = customer.BillingAddress.StateProvince != null ? customer.BillingAddress.StateProvince.Name : string.Empty;
                        cardOptions.AddressCountry = customer.BillingAddress.Country != null ? customer.BillingAddress.Country.Name : string.Empty;
                        cardOptions.AddressZip = customer.BillingAddress.ZipPostalCode ?? string.Empty;

                        var tokan = CreateToken(cardOptions);
                        var customerEmail = customer.Email != null ? customer.Email : customer.BillingAddress.Email;
                        var customerOptions = new StripeCustomerCreateOptions()
                        {
                            Email = customerEmail,
                            Description = "Customer for " + customerEmail,
                            SourceToken = tokan
                        };

                        var customerService = new StripeCustomerService();
                        StripeCustomer stripeCustomer = customerService.Create(customerOptions);

                        foreach (var item in order.OrderItems)
                        {
                            StripePlan stripePlan;
                            try
                            {
                                var planService = new StripePlanService();
                                stripePlan = planService.Get(postProcessPaymentRequest.Order.CustomerCurrencyCode + "_" + item.ProductId.ToString());
                            }
                            catch (Exception ex)
                            {
                                var Product = new StripeProductCreateOptions()
                                {
                                    Name = item.Product.Name,
                                    Id = "prod_" + item.ProductId,
                                    Active = true,
                                    Type = "service"
                                };

                                var productService = new StripeProductService(_stripePaymentSettings.SecretApiKey);
                                StripeProduct product = productService.Create(Product);

                                var primaryStoreCurrency = _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId);

                                var allcurrency = _currencyService.GetAllCurrencies(storeId: _storeContext.CurrentStore.Id);
                                StripePlan currentPlan = new StripePlan();
                                foreach (var currency in allcurrency)
                                {
                                    int productAmount = 0;
                                    if (Array.Exists(ZeroDecimal, z => z == currency.CurrencyCode))
                                        productAmount = (int)Math.Ceiling(_currencyService.ConvertCurrency(item.Product.Price, currency.Rate / primaryStoreCurrency.Rate));
                                    else
                                        productAmount = (int)Math.Ceiling(_currencyService.ConvertCurrency(item.Product.Price, currency.Rate / primaryStoreCurrency.Rate) * 100);

                                    var planOptions = new StripePlanCreateOptions()
                                    {
                                        Id = currency.Name + "_" + item.ProductId,
                                        Amount = productAmount,
                                        Currency = currency.CurrencyCode,
                                        Interval = item.Product.RecurringCyclePeriod.ToString().TrimEnd('s').ToLower(),
                                        IntervalCount = item.Product.RecurringCycleLength,
                                        ProductId = product.Id
                                    };

                                    var planService = new StripePlanService();
                                    var stripePlanCurrency = planService.Create(planOptions);

                                    if (currency.CurrencyCode == postProcessPaymentRequest.Order.CustomerCurrencyCode)
                                        currentPlan = stripePlanCurrency;
                                }
                                stripePlan = currentPlan;
                            }
                            var items = new List<StripeSubscriptionItemOption> {
                                            new StripeSubscriptionItemOption {
                                                PlanId = stripePlan.Id, Quantity = item.Quantity,
                                            }
                                        };

                            decimal? additionalFee = 0;
                            if (_stripePaymentSettings.AdditionalFeePercentage)
                                additionalFee = _stripePaymentSettings.AdditionalFee;
                            else
                                additionalFee = ((decimal)_stripePaymentSettings.AdditionalFee / order.OrderTotal) * 100;

                            var options = new StripeSubscriptionCreateOptions
                            {
                                Items = items,
                                TaxPercent = additionalFee,
                            };
                            var subscriptionService = new StripeSubscriptionService();
                            StripeSubscription subscription = subscriptionService.Create(stripeCustomer.Id, options);

                            if (subscription.Status == "active")
                            {
                                postProcessPaymentRequest.Order.SubscriptionTransactionId = subscription.Id;
                                _orderService.UpdateOrder(postProcessPaymentRequest.Order);

                                _orderProcessingService.MarkOrderAsPaid(postProcessPaymentRequest.Order);

                                var rp = _orderService.SearchRecurringPayments(initialOrderId: order.Id).FirstOrDefault();
                                rp.RecurringPaymentHistory.Add(new RecurringPaymentHistory
                                {
                                    RecurringPayment = rp,
                                    CreatedOnUtc = DateTime.UtcNow,
                                    OrderId = order.Id,
                                });
                                _orderService.UpdateRecurringPayment(rp);

                                var eurl = $"{ _webHelper.GetStoreLocation() }Checkout/Completed/{postProcessPaymentRequest.Order.Id}";
                                _httpContext.Response.Redirect(eurl);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        postProcessPaymentRequest.Order.OrderNotes.Add(new OrderNote
                        {
                            Note = ex.Message.ToString(),
                            DisplayToCustomer = false,
                            CreatedOnUtc = DateTime.UtcNow
                        });
                        _orderService.UpdateOrder(postProcessPaymentRequest.Order);

                        var eurl = $"{ _webHelper.GetStoreLocation() }Checkout/Completed/{postProcessPaymentRequest.Order.Id}";
                        _httpContext.Response.Redirect(eurl);
                    }
                }
                else
                {
                    var customer = _customerService.GetCustomerById(postProcessPaymentRequest.Order.CustomerId);

                    cardOptions.Name = _encryptionService.DecryptText(postProcessPaymentRequest.Order.CardName);
                    cardOptions.AddressLine1 = customer.BillingAddress.Address1 ?? string.Empty;
                    cardOptions.AddressLine2 = customer.BillingAddress.Address2 ?? string.Empty;
                    cardOptions.AddressCity = customer.BillingAddress.City ?? string.Empty;
                    cardOptions.AddressState = customer.BillingAddress.StateProvince != null ? customer.BillingAddress.StateProvince.Name : string.Empty;
                    cardOptions.AddressCountry = customer.BillingAddress.Country != null ? customer.BillingAddress.Country.Name : string.Empty;
                    cardOptions.AddressZip = customer.BillingAddress.ZipPostalCode ?? string.Empty;

                    token = CreateToken(cardOptions);
                    StripeChargeCreateOptions chargeOptions = new StripeChargeCreateOptions()
                    {
                        Amount = paymentAmount,
                        Currency = postProcessPaymentRequest.Order.CustomerCurrencyCode,
                        Description = $"Invoice-{postProcessPaymentRequest.Order.Id}",
                        Capture = !(_stripePaymentSettings.TransactMode == TransactMode.Authorize) || !SupportCapture ? true : false,
                        SourceTokenOrExistingSourceId = token
                    };

                    var chargeService = new StripeChargeService();
                    chargeService.ApiKey = _stripePaymentSettings.SecretApiKey;

                    StripeCharge stripeCharge = new StripeCharge();
                    try
                    {
                        stripeCharge = chargeService.Create(chargeOptions);
                    }
                    catch (Exception ex)
                    {
                        postProcessPaymentRequest.Order.OrderNotes.Add(new OrderNote
                        {
                            Note = ex.Message.ToString(),
                            DisplayToCustomer = false,
                            CreatedOnUtc = DateTime.UtcNow
                        });
                        _orderService.UpdateOrder(postProcessPaymentRequest.Order);

                        var eurl = $"{ _webHelper.GetStoreLocation() }Checkout/Completed/{postProcessPaymentRequest.Order.Id}";
                        _httpContext.Response.Redirect(eurl);
                    }

                    IList<string> Warrning = new List<string>();
                    if (stripeCharge.Status != "succeeded" && (stripeCharge.Source.Card.AddressLine1Check == "fail" || stripeCharge.Source.Card.AddressZipCheck == "fail"))
                    {
                        _logger.Error("Address verification failed, please correct your address information. If this error persists, contact us.");
                        Warrning.Add("Address verification failed, please correct your address information. If this error persists, contact us.");
                        postProcessPaymentRequest.Order.OrderNotes.Add(new OrderNote
                        {
                            Note = "Address verification failed, please correct your address information. If this error persists, contact us.",
                            DisplayToCustomer = false,
                            CreatedOnUtc = DateTime.UtcNow
                        });
                        _orderService.UpdateOrder(postProcessPaymentRequest.Order);

                    }
                    else if (stripeCharge.Status != "succeeded" && stripeCharge.Source.Card.CvcCheck == "fail")
                    {
                        _logger.Error("Cvc verification failed, please correct your information. If this error persists, contact us.");
                        Warrning.Add("Cvc verification failed, please correct your information. If this error persists, contact us.");
                        postProcessPaymentRequest.Order.OrderNotes.Add(new OrderNote
                        {
                            Note = "Cvc verification failed, please correct your information. If this error persists, contact us.",
                            DisplayToCustomer = false,
                            CreatedOnUtc = DateTime.UtcNow
                        });

                        _orderService.UpdateOrder(postProcessPaymentRequest.Order);
                    }
                    else if (!Warrning.Any())
                    {
                        if (!(_stripePaymentSettings.TransactMode == TransactMode.Authorize) || !SupportCapture)
                        {
                            //mark order as paid
                            if (_orderProcessingService.CanMarkOrderAsPaid(postProcessPaymentRequest.Order))
                            {
                                postProcessPaymentRequest.Order.CaptureTransactionResult = stripeCharge.Status;
                                postProcessPaymentRequest.Order.CaptureTransactionId = stripeCharge.Id;

                                _orderService.UpdateOrder(postProcessPaymentRequest.Order);

                                _orderProcessingService.MarkOrderAsPaid(postProcessPaymentRequest.Order);
                            }
                        }
                        else
                        {
                            postProcessPaymentRequest.Order.PaymentStatus = PaymentStatus.Authorized;
                            postProcessPaymentRequest.Order.AuthorizationTransactionResult = stripeCharge.Status;
                            postProcessPaymentRequest.Order.AuthorizationTransactionId = stripeCharge.Id;

                            _orderService.UpdateOrder(postProcessPaymentRequest.Order);

                            _orderProcessingService.CheckOrderStatus(postProcessPaymentRequest.Order);
                        }
                    }

                    if (stripeCharge.Status == "succeeded" && stripeCharge.Source.Card.AddressLine1Check == "fail")
                        _logger.Warning("Stripe charge is successful but AddressLine1 Check has failed. OrderGuid: " + postProcessPaymentRequest.Order.OrderGuid + ". Stripe ChargeId: " + stripeCharge.Id, null, null);
                    if (stripeCharge.Status == "succeeded" && stripeCharge.Source.Card.AddressZipCheck == "fail")
                        _logger.Warning("Stripe charge is successful but Address Zip Check has failed. OrderGuid: " + postProcessPaymentRequest.Order.OrderGuid + ". Stripe ChargeId: " + stripeCharge.Id, null, null);
                    if (stripeCharge.Status == "succeeded" && stripeCharge.Source.Card.CvcCheck == "fail")
                        _logger.Warning("Stripe charge is successful but Cvc Check has failed. OrderGuid: " + postProcessPaymentRequest.Order.OrderGuid + ". Stripe ChargeId: " + stripeCharge.Id, null, null);

                    var url = $"{ _webHelper.GetStoreLocation() }Checkout/Completed/{postProcessPaymentRequest.Order.Id}";
                    _httpContext.Response.Redirect(url);
                }
            }
            else
            {
                token = sourceCard.Id;
            }

            var orderDetail = new Dictionary<string, string>();
            orderDetail.Add("OrderGuid", postProcessPaymentRequest.Order.OrderGuid.ToString());

            var threedOptions = new StripeSourceCreateOptions()
            {
                Type = "three_d_secure",
                Amount = paymentAmount,
                Currency = postProcessPaymentRequest.Order.CustomerCurrencyCode,
                ThreeDSecureCardOrSourceId = sourceCard.Id,
                Metadata = orderDetail,
                RedirectReturnUrl = $"{ _webHelper.GetStoreLocation() }PaymentStripe/StripeHandler"
            };

            if (postProcessPaymentRequest.Order.OrderItems.Select(p => p.Product).Any(p => p.IsRecurring))
            {
                var customerEmail = postProcessPaymentRequest.Order.Customer.Email != null ? postProcessPaymentRequest.Order.Customer.Email : postProcessPaymentRequest.Order.BillingAddress.Email;
                var customerOptions = new StripeCustomerCreateOptions()
                {
                    Email = customerEmail,
                    Description = "Customer for " + customerEmail,
                    SourceToken = sourceCard.Id
                };
                var customerService = new StripeCustomerService();
                StripeCustomer stripeCustomer = customerService.Create(customerOptions);

                threedOptions.ThreeDSecureCustomer = stripeCustomer.Id;
            }

            try
            {
                var service = new StripeSourceService(_stripePaymentSettings.ApiKey);
                StripeSource Sourcethreed = service.Create(threedOptions);
                _httpContext.Response.Redirect(Sourcethreed.Redirect.Url);
            }
            catch (Exception ex)
            {
                postProcessPaymentRequest.Order.OrderNotes.Add(new OrderNote
                {
                    Note = ex.Message.ToString(),
                    DisplayToCustomer = false,
                    CreatedOnUtc = DateTime.UtcNow
                });
                _orderService.UpdateOrder(postProcessPaymentRequest.Order);

                var eurl = $"{ _webHelper.GetStoreLocation() }Checkout/Completed/{postProcessPaymentRequest.Order.Id}";
                _httpContext.Response.Redirect(eurl);
            }
        }

        /// <summary>
        /// Returns a value indicating whether payment method should be hidden during checkout
        /// </summary>
        /// <param name="cart">Shoping cart</param>
        /// <returns>true - hide; false - display.</returns>
        public bool HidePaymentMethod(IList<ShoppingCartItem> cart)
        {

            var IsHide = false;
            //you can put any logic here
            //for example, hide this payment method if all products in the cart are downloadable
            //or hide this payment method if current customer is from certain country
            //load settings for a chosen store scope
            return IsHide;
        }

        /// <summary>
        /// Gets additional handling fee
        /// </summary>
        /// <returns>Additional handling fee</returns>
        public decimal GetAdditionalHandlingFee(IList<ShoppingCartItem> cart)
        {
            var result = this.CalculateAdditionalFee(_orderTotalCalculationService, cart,
                _stripePaymentSettings.AdditionalFee, _stripePaymentSettings.AdditionalFeePercentage);
            return result;
        }

        /// <summary>
        /// Captures payment
        /// </summary>
        /// <param name="capturePaymentRequest">Capture payment request</param>
        /// <returns>Capture payment result</returns>
        public CapturePaymentResult Capture(CapturePaymentRequest capturePaymentRequest)
        {
            CapturePaymentResult result = new CapturePaymentResult();
            CapturePaymentResult result2;
            //result.AddError("Capture method not supported");
            //return result;
            try
            {
                StripeCharge stripeCharge = this._chargeService.Capture(capturePaymentRequest.Order.AuthorizationTransactionId, null, null);
                result.NewPaymentStatus = PaymentStatus.Paid;
                result.CaptureTransactionResult = stripeCharge.Status;
                result.CaptureTransactionId = stripeCharge.Id;
                result2 = result;
            }
            catch (StripeException exception)
            {
                bool flag = !string.IsNullOrEmpty(exception.StripeError.Code);
                if (flag)
                {
                    result.AddError(exception.StripeError.Message + " Error code: " + exception.StripeError.Code);
                }
                else
                {
                    result.AddError(exception.StripeError.Message);
                }
                result2 = result;
            }
            return result2;
        }


        /// <summary>
        /// Refunds a payment
        /// </summary>
        /// <param name="refundPaymentRequest">Request</param>
        /// <returns>Result</returns>
        /// <summary>
        /// Refunds a payment
        /// </summary>
        /// <param name="refundPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public RefundPaymentResult Refund(RefundPaymentRequest refundPaymentRequest)
        {
            var result = new RefundPaymentResult();
            RefundPaymentResult result2;
            //result.AddError("Refund method not supported");
            //return result;
            try
            {
                StripeRefundCreateOptions options = new StripeRefundCreateOptions();
                StripeCharge charge = _chargeService.Get(refundPaymentRequest.Order.CaptureTransactionId);
                int maximumRefund = charge.Amount - charge.AmountRefunded;
                options.Amount = new int?((int)Math.Ceiling(refundPaymentRequest.AmountToRefund * 100m));
                bool flag = maximumRefund == 0 && !refundPaymentRequest.IsPartialRefund;
                if (flag)
                {
                    result.NewPaymentStatus = PaymentStatus.Refunded;
                    result2 = result;
                }
                else
                {
                    bool flag2 = options.Amount > maximumRefund;
                    if (flag2)
                    {
                        bool flag3 = maximumRefund > 0;
                        if (flag3)
                        {
                            result.AddError("This charge has already been partially refunded. Maximum refund amount is: " + maximumRefund / 100m);
                        }
                        else
                        {
                            result.AddError("This charge has already been fully refunded.");
                        }
                        result2 = result;
                    }
                    else
                    {
                        refundPaymentRequest.IsPartialRefund = options.Amount != maximumRefund;
                        this._refundService.Create(refundPaymentRequest.Order.CaptureTransactionId, options, null);
                        result.NewPaymentStatus = refundPaymentRequest.IsPartialRefund ? PaymentStatus.PartiallyRefunded : PaymentStatus.Refunded;
                        result2 = result;
                    }
                }
            }
            catch (StripeException exception)
            {
                bool flag4 = !string.IsNullOrEmpty(exception.StripeError.Code);
                if (flag4)
                {
                    result.AddError(exception.StripeError.Message + " Error code: " + exception.StripeError.Code);
                }
                else
                {
                    result.AddError(exception.StripeError.Message);
                }
                result2 = result;
            }
            return result2;
        }

        /// <summary>
        /// Voids a payment
        /// </summary>
        /// <param name="voidPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public VoidPaymentResult Void(VoidPaymentRequest voidPaymentRequest)
        {
            VoidPaymentResult result = new VoidPaymentResult();
            VoidPaymentResult result2;
            //result.AddError("Void method not supported");
            //return result;
            try
            {
                this._refundService.Create(voidPaymentRequest.Order.AuthorizationTransactionId, null, null);
                result.NewPaymentStatus = PaymentStatus.Voided;
                result2 = result;
            }
            catch (StripeException exception)
            {
                result.AddError((!string.IsNullOrEmpty(exception.StripeError.Code)) ? string.Format("{0} Error code: {1}", exception.StripeError.Message, exception.StripeError.Code) : exception.StripeError.Message);
                result2 = result;
            }
            return result2;
        }

        /// <summary>
        /// Process recurring payment
        /// </summary>
        /// <param name="processPaymentRequest">Payment info required for an order processing</param>
        /// <returns>Process payment result</returns>
        public ProcessPaymentResult ProcessRecurringPayment(ProcessPaymentRequest processPaymentRequest)
        {
            var result = new ProcessPaymentResult();
            result.AllowStoringCreditCardNumber = true;

            if (_stripePaymentSettings.Enable3DSecure)
                return result;
            try
            {
                var customer = _customerService.GetCustomerById(processPaymentRequest.CustomerId);

                var shoppingCart = customer.ShoppingCartItems
                    .Where(shoppingCartItem => shoppingCartItem.ShoppingCartType == ShoppingCartType.ShoppingCart)
                    .LimitPerStore(processPaymentRequest.StoreId).ToList();

                var StripeRequestOptions = new StripeRequestOptions();
                StripeRequestOptions.ApiKey = _stripePaymentSettings.ApiKey;

                StripeCreditCardOptions cardOptions = new StripeCreditCardOptions()
                {
                    Number = processPaymentRequest.CreditCardNumber,
                    ExpirationYear = processPaymentRequest.CreditCardExpireYear,
                    ExpirationMonth = processPaymentRequest.CreditCardExpireMonth,
                    Cvc = processPaymentRequest.CreditCardCvv2,
                    Name = processPaymentRequest.CreditCardName
                };

                cardOptions.AddressLine1 = customer.BillingAddress.Address1 ?? string.Empty;
                cardOptions.AddressLine2 = customer.BillingAddress.Address2 ?? string.Empty;
                cardOptions.AddressCity = customer.BillingAddress.City ?? string.Empty;
                cardOptions.AddressState = customer.BillingAddress.StateProvince != null ? customer.BillingAddress.StateProvince.Name : string.Empty;
                cardOptions.AddressCountry = customer.BillingAddress.Country != null ? customer.BillingAddress.Country.Name : string.Empty;
                cardOptions.AddressZip = customer.BillingAddress.ZipPostalCode ?? string.Empty;

                var tokan = CreateToken(cardOptions);

                var customerEmail = customer.Email != null ? customer.Email : customer.BillingAddress.Email;
                var customerOptions = new StripeCustomerCreateOptions()
                {
                    Email = customerEmail,
                    Description = "Customer for " + customerEmail,
                    SourceToken = tokan
                };

                var customerService = new StripeCustomerService();
                StripeCustomer stripeCustomer = customerService.Create(customerOptions);

                foreach (var item in shoppingCart)
                {
                    StripePlan stripePlan;
                    try
                    {
                        var planService = new StripePlanService();
                        stripePlan = planService.Get(_workContext.WorkingCurrency.CurrencyCode + "_" + item.ProductId.ToString());
                    }
                    catch (Exception ex)
                    {
                        var Product = new StripeProductCreateOptions()
                        {
                            Name = item.Product.Name,
                            Id = _workContext.WorkingCurrency.CurrencyCode + "_" + item.ProductId,
                            Active = true,
                            Type = "service"
                        };

                        var productService = new StripeProductService(_stripePaymentSettings.SecretApiKey);
                        StripeProduct product = productService.Create(Product);
                        var currencyTmp = _currencyService.GetCurrencyById(
                            customer.GetAttribute<int>(SystemCustomerAttributeNames.CurrencyId, processPaymentRequest.StoreId));

                        var customerCurrency = (currencyTmp != null && currencyTmp.Published) ? currencyTmp : _workContext.WorkingCurrency;
                        var primaryStoreCurrency = _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId);
                        var CustomerCurrencyCode = customerCurrency.CurrencyCode;
                        var CustomerCurrencyRate = customerCurrency.Rate / primaryStoreCurrency.Rate;

                        var allcurrency = _currencyService.GetAllCurrencies(storeId: _storeContext.CurrentStore.Id);
                        StripePlan currentPlan = new StripePlan();
                        foreach (var currency in allcurrency)
                        {
                            int productAmount = 0;
                            if (Array.Exists(ZeroDecimal, z => z == currency.CurrencyCode))
                                productAmount = (int)Math.Ceiling(_currencyService.ConvertCurrency(item.Product.Price, currency.Rate / primaryStoreCurrency.Rate));
                            else
                                productAmount = (int)Math.Ceiling(_currencyService.ConvertCurrency(item.Product.Price, currency.Rate / primaryStoreCurrency.Rate) * 100);

                            var planOptions = new StripePlanCreateOptions()
                            {
                                Id = currency.CurrencyCode + "_" + item.ProductId,
                                Amount = productAmount,
                                Currency = currency.CurrencyCode,
                                Interval = processPaymentRequest.RecurringCyclePeriod.ToString().TrimEnd('s').ToLower(),
                                IntervalCount = item.Product.RecurringCycleLength,
                                ProductId = product.Id
                            };

                            var planService = new StripePlanService();
                            var stripePlanCurrency = planService.Create(planOptions);

                            if (currency == customerCurrency)
                                currentPlan = stripePlanCurrency;
                        }
                        stripePlan = currentPlan;
                    }
                    if (stripePlan == null)
                    {
                        result.AddError("Couldn't find a matching plan");
                        return result;
                    }
                    var items = new List<StripeSubscriptionItemOption> {
                                            new StripeSubscriptionItemOption {
                                                PlanId = stripePlan.Id, Quantity = item.Quantity
                                            }
                                        };

                    decimal? additionalFee = 0;
                    if (_stripePaymentSettings.AdditionalFeePercentage)
                        additionalFee = _stripePaymentSettings.AdditionalFee;
                    else
                        additionalFee = ((decimal)_stripePaymentSettings.AdditionalFee / processPaymentRequest.OrderTotal) * 100;

                    var options = new StripeSubscriptionCreateOptions
                    {
                        Items = items,
                        TaxPercent = additionalFee
                    };
                    var subscriptionService = new StripeSubscriptionService();
                    StripeSubscription subscription = subscriptionService.Create(stripeCustomer.Id, options);

                    if (subscription.Status == "active")
                    {
                        result.SubscriptionTransactionId = subscription.Id;
                        result.NewPaymentStatus = PaymentStatus.Paid;
                    }

                }

                if (!shoppingCart.Any())
                {
                    var intialOrder = _orderService.GetOrderById(processPaymentRequest.InitialOrderId);
                    if (intialOrder != null)
                    {
                        var subscriptionService = new StripeSubscriptionService();
                        StripeSubscription subscription = subscriptionService.Get(intialOrder.SubscriptionTransactionId);

                        var options = new StripeInvoiceItemCreateOptions
                        {
                            Amount = (int)Math.Ceiling(intialOrder.OrderTotal * 100),
                            Currency = intialOrder.CustomerCurrencyCode,
                            CustomerId = subscription.CustomerId,
                            SubscriptionId = subscription.Id
                        };
                        var service = new StripeInvoiceItemService(_stripePaymentSettings.SecretApiKey);
                        StripeInvoiceLineItem invoiceItem = service.Create(options);

                        var invoiceOptions = new StripeInvoiceCreateOptions()
                        {
                            SubscriptionId = subscription.Id,
                        };
                        var invoiceService = new StripeInvoiceService(_stripePaymentSettings.SecretApiKey);
                        StripeInvoice invoice = invoiceService.Create(subscription.CustomerId, invoiceOptions);

                        //var invoiceService = new StripeInvoiceService();
                        //StripeInvoice invoice = invoiceService.Pay("in_1Cb1ONB9E5ZLJfqErsGMEudQ");
                    }
                }
            }
            catch (Exception ex)
            {
                result.AddError(ex.Message);
            }
            return result;

        }

        /// <summary>
        /// Cancels a recurring payment
        /// </summary>
        /// <param name="cancelPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public CancelRecurringPaymentResult CancelRecurringPayment(CancelRecurringPaymentRequest cancelPaymentRequest)
        {
            CancelRecurringPaymentResult recurringPaymentResult = new CancelRecurringPaymentResult();
            var order = cancelPaymentRequest.Order;
            try
            {
                var subscriptionService = new StripeSubscriptionService(_stripePaymentSettings.SecretApiKey);
                subscriptionService.Cancel(order.SubscriptionTransactionId);
            }
            catch (Exception ex)
            {
                recurringPaymentResult.AddError(ex.Message);
            }
            return recurringPaymentResult;
        }

        /// <summary>
        /// Gets a value indicating whether customers can complete a payment after order is placed but not completed (for redirection payment methods)
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>Result</returns>
        public bool CanRePostProcessPayment(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            //it's not a redirection payment method. So we always return false
            if (!_stripePaymentSettings.Enable3DSecure)
                return false;
            else
            {
                //let's ensure that at least 5 seconds passed after order is placed
                //P.S. there's no any particular reason for that. we just do it
                if ((DateTime.UtcNow - order.CreatedOnUtc).TotalSeconds < 5)
                    return false;

                return true;
            }
        }

        /// <summary>
        /// Gets a route for provider configuration
        /// </summary>
        /// <param name="actionName">Action name</param>
        /// <param name="controllerName">Controller name</param>
        /// <param name="routeValues">Route values</param>
        public void GetConfigurationRoute(out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        {
            actionName = "Configure";
            controllerName = "PaymentStripe";
            routeValues = new RouteValueDictionary { { "Namespaces", "Nop.Plugin.Payments.Stripe.Controllers" }, { "area", null } };
        }

        /// <summary>
        /// Gets a route for payment info
        /// </summary>
        /// <param name="actionName">Action name</param>
        /// <param name="controllerName">Controller name</param>
        /// <param name="routeValues">Route values</param>
        public void GetPaymentInfoRoute(out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        {
            actionName = "PaymentInfo";
            controllerName = "PaymentStripe";
            routeValues = new RouteValueDictionary { { "Namespaces", "Nop.Plugin.Payments.Stripe.Controllers" }, { "area", null } };
        }

        public Type GetControllerType()
        {
            return typeof(PaymentStripeController);
        }

        public override void Install()
        {
            //settings
            var settings = new StripePaymentSettings
            {
                TransactMode = TransactMode.Authorize

            };
            _settingService.SaveSetting(settings);

            //locales
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Stripe.Fields.AdditionalFee", "Additional fee");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Stripe.Fields.AdditionalFee.Hint", "Enter additional fee to charge your customers.");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Stripe.Fields.AdditionalFeePercentage", "Additional fee. Use percentage");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Stripe.Fields.AdditionalFeePercentage.Hint", "Determines whether to apply a percentage additional fee to the order total. If not enabled, a fixed value is used.");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Stripe.Fields.Enable3DSecure", "Enable 3D-secure payment");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Stripe.Fields.Enable3DSecure.Hint", "Enable 3D-secure payment");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Stripe.Fields.TransactMode", "Transaction mode");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Stripe.Fields.TransactMode.Hint", "Choose transaction mode.");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Stripe.Fields.SecretApiKey", "Secret API Key");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Stripe.Fields.SecretApiKey.Hint", "Enter secret api key");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Stripe.Fields.ApiKey", "Public Api Key");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Stripe.Fields.ApiKey.Hint", "Enter public api key");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Stripe.PaymentMethodDescription", "Pay by credit / debit card");

            base.Install();
        }

        public override void Uninstall()
        {
            //settings
            _settingService.DeleteSetting<StripePaymentSettings>();

            //locales
            this.DeletePluginLocaleResource("Plugins.Payments.Stripe.Fields.AdditionalFee");
            this.DeletePluginLocaleResource("Plugins.Payments.Stripe.Fields.AdditionalFee.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.Stripe.Fields.AdditionalFeePercentage");
            this.DeletePluginLocaleResource("Plugins.Payments.Stripe.Fields.AdditionalFeePercentage.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.Stripe.Fields.Enable3DSecure");
            this.DeletePluginLocaleResource("Plugins.Payments.Stripe.Fields.Enable3DSecure.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.Stripe.Fields.TransactMode");
            this.DeletePluginLocaleResource("Plugins.Payments.Stripe.Fields.TransactMode.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.Stripe.Fields.ApiKey");
            this.DeletePluginLocaleResource("Plugins.Payments.Stripe.Fields.ApiKey.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.Stripe.Fields.SecretApiKey");
            this.DeletePluginLocaleResource("Plugins.Payments.Stripe.Fields.SecretApiKey.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.Stripe.PaymentMethodDescription");
            this.DeletePluginLocaleResource("Plugins.Payments.Stripe.Title");

            base.Uninstall();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets a value indicating whether capture is supported
        /// </summary>
        public bool SupportCapture
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets a value indicating whether partial refund is supported
        /// </summary>
        public bool SupportPartiallyRefund
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets a value indicating whether refund is supported
        /// </summary>
        public bool SupportRefund
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets a value indicating whether void is supported
        /// </summary>
        public bool SupportVoid
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets a recurring payment type of payment method
        /// </summary>
        public RecurringPaymentType RecurringPaymentType
        {
            get
            {
                if (_stripePaymentSettings.Enable3DSecure)
                    return RecurringPaymentType.Automatic;
                else
                    return RecurringPaymentType.Manual;
            }
        }

        /// <summary>
        /// Gets a payment method type
        /// </summary>
        public PaymentMethodType PaymentMethodType
        {
            get
            {
                if (_stripePaymentSettings.Enable3DSecure)
                    return PaymentMethodType.Redirection;
                else
                    return PaymentMethodType.Standard;
            }
        }

        /// <summary>
        /// Gets a value indicating whether we should display a payment information page for this plugin
        /// </summary>
        public bool SkipPaymentInfo
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets a payment method description that will be displayed on checkout pages in the public store
        /// </summary>
        public string PaymentMethodDescription
        {
            //return description of this payment method to be display on "payment method" checkout step. good practice is to make it localizable
            //for example, for a redirection payment method, description may be like this: "You will be redirected to PayPal site to complete the payment"
            get { return _localizationService.GetResource("Plugins.Payments.Stripe.PaymentMethodDescription"); }
        }

        #endregion
    }
}
