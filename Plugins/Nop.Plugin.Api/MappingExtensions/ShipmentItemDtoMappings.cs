﻿using Nop.Core.Domain.Shipping;
using Nop.Plugin.Api.AutoMapper;
using Nop.Plugin.Api.DTOs.ShipmentItems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.MappingExtensions
{
    public static class ShipmentItemDtoMappings
    {
        public static ShipmentItemDto ToDto(this ShipmentItem shipmentItem)
        {
            return shipmentItem.MapTo<ShipmentItem, ShipmentItemDto>();
        }
    }
}
