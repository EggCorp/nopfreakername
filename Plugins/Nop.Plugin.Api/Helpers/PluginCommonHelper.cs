﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Helpers
{
    public static class PluginCommonHelper
    {
        public static string GetImageExtentionByMimeType(string mimeType)
        {
            string result = "jpg";
            switch (mimeType)
            {
                case "image/jpeg":
                    result = "jpeg";
                    break;
                case "image/pjpeg":
                    result = "pjpeg";
                    break;
                case "image/jpg":
                    result = "jpg";
                    break;
                case "image/png":
                    result = "png";
                    break;
                default:
                    result = "jpg";
                    break;
            }
            return result;
        }
        public static string ParseImageName(int pictureId, string seoName)
        {
            int maxLengthPictureId = 7;
            string result = pictureId.ToString();
            for (int i = 0; i < (maxLengthPictureId - pictureId.ToString().Length); i++)
            {
                result = string.Format("0{0}", result);
            }

            return result;
        }
    }
}
