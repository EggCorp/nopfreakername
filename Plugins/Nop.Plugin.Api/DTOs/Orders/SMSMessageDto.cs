﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.DTOs.Orders
{
    [JsonObject(Title = "sms_message")]
    public class SMSMessageDto
    {
        [JsonProperty("to")]
        public string To { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }
      
    }
}
