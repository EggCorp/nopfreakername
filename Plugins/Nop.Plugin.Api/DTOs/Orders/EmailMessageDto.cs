﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.DTOs.Orders
{
   
    [JsonObject(Title = "email_message")]
    public class EmailMessageDto
    {
        [JsonProperty("email")]
        public string Email { get; set; }
      
        [JsonProperty("body")]
        public string Body { get; set; }

        [JsonProperty("subject")]
        public string Subject { get; set; }
    }
}
