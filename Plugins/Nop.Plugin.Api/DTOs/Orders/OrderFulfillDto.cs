﻿using Newtonsoft.Json;
using Nop.Plugin.Api.DTOs.OrderItems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.DTOs.Orders
{
    [JsonObject(Title = "orderFulfill")]
    public class OrderFulfillDto
    {
        public OrderFulfillDto()
        {
            OrderItemFulfills = new List<OrderItemFulfillDto>();
        }

        [JsonProperty("orderId")]
        public int? OrderId { get; set; }
        [JsonProperty("orderTotal")]
        public decimal? OrderTotal { get; set; }
        [JsonProperty("orderShipTotal")]
        public decimal? OrderShipTotal { get; set; }
        [JsonProperty("shipTotal")]
        public decimal? ShipTotal { get; set; }
        [JsonProperty("paymentFee")]
        public decimal? PaymentFee { get; set; }
        [JsonProperty("totalProfit")]
        public decimal? TotalProfit { get; set; }
        [JsonProperty("createdDate")]
        public DateTime? CreatedDate { get; set; }
        [JsonProperty("quantity")]
        public int Quantity { get; set; }
        [JsonProperty("shippingName")]
        public string ShippingName { get; set; }
        [JsonProperty("shippingAddress")]
        public string ShippingAddress { get; set; }
        [JsonProperty("shippingCity")]
        public string ShippingCity { get; set; }
        [JsonProperty("shippingState")]
        public string ShippingState { get; set; }
        [JsonProperty("shippingZipcode")]
        public string ShippingZipcode { get; set; }
        [JsonProperty("shippingCountry")]
        public string ShippingCountry { get; set; }
        [JsonProperty("shippingPhone")]
        public string ShippingPhone { get; set; }
        [JsonProperty("shippingEmail")]
        public string ShippingEmail { get; set; }
        [JsonProperty("orderStatusId")]
        public int OrderStatusId { get; set; }
        [JsonProperty("paymentStatusId")]
        public int PaymentStatusId { get; set; }
        [JsonProperty("deleted")]
        public bool Deleted { get; set; }
        [JsonProperty("shippingStatusId")]
        public int? ShippingStatusId { get; set; }
        [JsonProperty("transactionAuthorization")]
        public string TransactionAuthorization { get; set; }
        [JsonProperty("transactionGateway")]
        public string TransactionGateway { get; set; }
        [JsonProperty("paymentBusinessEmail")]
        public string PaymentBusinessEmail { get; set; }

        [JsonProperty("orderItemFulfills")]
        public ICollection<OrderItemFulfillDto> OrderItemFulfills { get; set; }
    }
}
