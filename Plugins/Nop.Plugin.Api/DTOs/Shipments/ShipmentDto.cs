﻿using FluentValidation.Attributes;
using Newtonsoft.Json;
using Nop.Plugin.Api.DTOs.ShipmentItems;
using Nop.Plugin.Api.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.DTOs.Shipments
{
    [JsonObject(Title = "shipment")]
    public class ShipmentDto
    {

        private ICollection<ShipmentItemDto> _shipmentItemDtos;

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("order_id")]
        public string OrderId { get; set; }

        [JsonProperty("tracking_number")]
        public string TrackingNumber { get; set; }

        [JsonProperty("admin_comment")]
        public string AdminComment { get; set; }

        [JsonProperty("total_weight")]
        public decimal? TotalWeight { get; set; }

        [JsonProperty("shipped_date_utc")]
        public DateTime? ShippedDateUtc { get; set; }

        [JsonProperty("delivery_date_utc")]
        public DateTime? DeliveryDateUtc { get; set; }

        [JsonProperty("created_on_utc")]
        public DateTime? CreatedOnUtc { get; set; }

        [JsonProperty("shipment_items")]
        public ICollection<ShipmentItemDto> ShipmentItemDtos
        {
            get { return _shipmentItemDtos; }
            set { _shipmentItemDtos = value; }
        }
    }
}
