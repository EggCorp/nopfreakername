﻿using System;
using FluentValidation.Attributes;
using Newtonsoft.Json;
using Nop.Plugin.Api.Validators;

namespace Nop.Plugin.Api.DTOs.ShipmentItems
{
    [JsonObject(Title = "shipment_item")]
    public class ShipmentItemDto
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("shipment_id")]
        public string ShipmentId { get; set; }

        [JsonProperty("order_item_Id")]
        public string OrderItemId { get; set; }

        [JsonProperty("quantity")]
        public int Quantity { get; set; }
    }
}
