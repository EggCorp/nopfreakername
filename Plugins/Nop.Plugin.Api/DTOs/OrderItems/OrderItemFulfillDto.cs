﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.DTOs.OrderItems
{
    [JsonObject(Title = "orderItemFulfill")]
    public class OrderItemFulfillDto
    {
        [JsonProperty("orderId")]
        public int OrderId { get; set; }
        [JsonProperty("orderItemId")]
        public long? OrderItemId { get; set; }
        [JsonProperty("sKU")]
        public string SKU { get; set; }
        [JsonProperty("size")]
        public string Size { get; set; }
        [JsonProperty("quantity")]
        public int Quantity { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("color")]
        public string Color { get; set; }
        [JsonProperty("originUrl")]
        public string OriginUrl { get; set; }
        [JsonProperty("productUrl")]
        public string ProductUrl { get; set; }
        [JsonProperty("pictureUrl")]
        public string PictureUrl { get; set; }
        [JsonProperty("productTiltle")]
        public string ProductTiltle { get; set; }
        [JsonProperty("deleted")]
        public bool Deleted { get; set; }
        [JsonProperty("createdOnUtc")]
        public DateTime CreatedOnUtc { get; set; }
        [JsonProperty("customName")]
        public string CustomName { get; set; }
        [JsonProperty("customNumber")]
        public string CustomNumber { get; set; }
    }
}
