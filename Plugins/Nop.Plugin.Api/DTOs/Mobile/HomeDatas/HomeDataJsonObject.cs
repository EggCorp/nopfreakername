﻿using Newtonsoft.Json;
using Nop.Plugin.Api.DTOs.Mobile.Categories;
using Nop.Plugin.Api.DTOs.Mobile.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.DTOs.Mobile.HomeDatas
{
    [JsonObject(Title = "homeData")]
    public class HomeDataJsonObject
    {
        public HomeDataJsonObject()
        {
            this.TopCategories = new List<CategoryJsonObject>();
            this.NewestProducts = new List<ProductJsonObject>();
            this.TrendingProducts = new List<ProductJsonObject>();
        }
        [JsonProperty("banner")]
        public CategoryJsonObject Banner { get; set; }
        [JsonProperty("topCategories")]
        public IList<CategoryJsonObject> TopCategories { get; set; }
        [JsonProperty("trendingProducts")]
        public IList<ProductJsonObject> TrendingProducts { get; set; }
        [JsonProperty("newestProducts")]
        public IList<ProductJsonObject> NewestProducts { get; set; }
        [JsonProperty("midBanner")]
        public string MidBanner { get; set; }
        [JsonProperty("midBannerPhotoId")]
        public int MidBannerPhotoId { get; set; }
        [JsonProperty("midBannerWidth")]
        public int MidBannerWidth { get; set; }
        [JsonProperty("midBannerHeight")]
        public int MidBannerHeight { get; set; }
    }
}
