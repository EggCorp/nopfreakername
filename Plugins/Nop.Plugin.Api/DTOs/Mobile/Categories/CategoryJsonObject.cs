﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.DTOs.Mobile.Categories
{
    [JsonObject(Title = "category")]
    public class CategoryJsonObject
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("seName")]
        public string SeName { get; set; }
        [JsonProperty("product")]
        public int Product { get; set; }
        [JsonProperty("photoUrl")]
        public string PhotoUrl { get; set; }
        [JsonProperty("photoId")]
        public int PhotoId { get; set; }
        [JsonProperty("hasSubs")]
        public bool HasSubs { get; set; }
        [JsonProperty("photoWidth")]
        public int PhotoWidth { get; set; }
        [JsonProperty("photoHeight")]
        public int PhotoHeight { get; set; }
    }
}
