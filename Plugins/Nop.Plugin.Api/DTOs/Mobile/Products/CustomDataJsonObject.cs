﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.DTOs.Mobile.Products
{
    [JsonObject(Title = "productDetail")]
    public class CustomDataJsonObject
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("text")]
        public string Text { get; set; }
        [JsonProperty("valueId")]
        public int ValueId { get; set; }
    }
}
