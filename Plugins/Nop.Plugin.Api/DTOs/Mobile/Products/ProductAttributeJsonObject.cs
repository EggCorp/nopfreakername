﻿using Newtonsoft.Json;
namespace Nop.Plugin.Api.DTOs.Mobile.Products
{
    [JsonObject(Title = "productAttribute")]
    public class ProductAttributeJsonObject
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("photoUrl")]
        public string PhotoUrl { get; set; }
        [JsonProperty("photoId")]
        public int PhotoId { get; set; }
        [JsonProperty("price")]
        public decimal Price { get; set; }
        [JsonProperty("code")]
        public string Code { get; set; }
        [JsonProperty("isPreSelected")]
        public bool IsPreSelected { get; set; }
        [JsonProperty("photoWidth")]
        public int PhotoWidth { get; set; }
        [JsonProperty("photoHeight")]
        public int PhotoHeight { get; set; }
    }
}
