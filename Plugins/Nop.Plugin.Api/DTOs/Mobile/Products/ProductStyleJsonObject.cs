﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.DTOs.Mobile.Products
{
    [JsonObject(Title = "style")]
    public class ProductStyleJsonObject
    {
        public ProductStyleJsonObject()
        {
            this.Colors = new List<ProductAttributeJsonObject>();
            this.Sizes = new List<ProductAttributeJsonObject>();
            this.CustomData = new List<CustomDataJsonObject>();
        }
        [JsonProperty("type")]
        public ProductAttributeJsonObject Type { get; set; }
        [JsonProperty("colors")]
        public IList<ProductAttributeJsonObject> Colors { get; set; }
        [JsonProperty("sizes")]
        public IList<ProductAttributeJsonObject> Sizes { get; set; }
        [JsonProperty("customData")]
        public IList<CustomDataJsonObject> CustomData { get; set; }
    }
}
