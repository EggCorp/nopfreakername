﻿using Newtonsoft.Json;
using Nop.Plugin.Api.DTOs.Mobile.Reviews;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.DTOs.Mobile.Products
{
    [JsonObject(Title = "product")]
    public class ProductJsonObject
    {
        public ProductJsonObject()
        {
        }
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("seName")]
        public string SeName { get; set; }
        [JsonProperty("photoUrl")]
        public string PhotoUrl { get; set; }
        [JsonProperty("photoId")]
        public int PhotoId { get; set; }
        [JsonProperty("newPrice")]
        public decimal NewPrice { get; set; }
        [JsonProperty("oldPrice")]
        public decimal OldPrice { get; set; }
        [JsonProperty("customable")]
        public bool Customable { get; set; }
        [JsonProperty("photoWidth")]
        public int PhotoWidth { get; set; }
        [JsonProperty("photoHeight")]
        public int PhotoHeight { get; set; }
    }
}
