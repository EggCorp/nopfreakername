﻿using Newtonsoft.Json;
using Nop.Plugin.Api.DTOs.Mobile.Reviews;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.DTOs.Mobile.Products
{
    [JsonObject(Title = "productDetail")]
    public class ProductDetailJsonObject: ProductJsonObject
    {
        public ProductDetailJsonObject()
        {
            this.Styles = new List<ProductStyleJsonObject>();
            this.Reviews = new List<ReviewJsonObject>();
            this.Photos = new List<string>();
            this.PhotoIds = new List<int>();
            this.Customfields = new List<CustomFieldJsonObject>();
        }
        [JsonProperty("detail")]
        public string Detail { get; set; }
        [JsonProperty("rating")]
        public int Rating { get; set; }
        [JsonProperty("totalReviews")]
        public int TotalReview { get; set; }
        [JsonProperty("onSale")]
        public bool OnSale { get; set; }
        [JsonProperty("saleEndAt")]
        public string SaleEndAt { get; set; }
        [JsonProperty("styles")]
        public IList<ProductStyleJsonObject> Styles { get; set; }
        [JsonProperty("photos")]
        public IList<string> Photos { get; set; }
        [JsonProperty("photoIds")]
        public List<int> PhotoIds { get; set; }
        [JsonProperty("reviews")]
        public IList<ReviewJsonObject> Reviews { get; set; }
        [JsonProperty("customfields")]
        public IList<CustomFieldJsonObject> Customfields { get; set; }
    }
}
