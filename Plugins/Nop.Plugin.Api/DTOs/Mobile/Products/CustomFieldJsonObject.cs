﻿using Newtonsoft.Json;

namespace Nop.Plugin.Api.DTOs.Mobile.Products
{
    [JsonObject(Title = "customfield")]
    public class CustomFieldJsonObject
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("typeId")]
        public int TypeId { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("group")]
        public int Group { get; set; }
    }
}
