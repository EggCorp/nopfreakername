﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.DTOs.Mobile.ShoppingCarts
{
    [JsonObject(Title = "shoppingCart")]
    public class ShoppingCartJsonObject
    {
        public ShoppingCartJsonObject()
        {
            this.Items = new List<ShoppingCartItemJsonObject>();
        }
        [JsonProperty("items")]
        public List<ShoppingCartItemJsonObject> Items { get; set; }
        [JsonProperty("freeShippingOverXValue")]
        public decimal FreeShippingOverXValue { get; set; }
        [JsonProperty("freeShippingOverXEnabled")]
        public bool FreeShippingOverXEnabled { get; set; }
        [JsonProperty("customerId")]
        public int? CustomerId { get; set; }
        [JsonProperty("orderTotal")]
        public OrderTotalsJsonObject OrderTotal { get; set; }
    }
}
