﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.DTOs.Mobile.ShoppingCarts
{
    [JsonObject(Title = "cartItem")]
    public class AddToCartItemJsonObject
    {
        [JsonProperty("productId")]
        public int ProductId { get; set; }
        [JsonProperty("colorId")]
        public int ColorId { get; set; }
        [JsonProperty("sizeId")]
        public int SizeId { get; set; }
        [JsonProperty("typeId")]
        public int TypeId { get; set; }
        [JsonProperty("quantity")]
        public int Quantity { get; set; }
        [JsonProperty("custom")]
        public string Custom { get; set; }
    }
}
