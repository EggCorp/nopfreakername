﻿using Newtonsoft.Json;
using Nop.Plugin.Api.DTOs.Mobile.Products;

namespace Nop.Plugin.Api.DTOs.Mobile.ShoppingCarts
{
    [JsonObject(Title = "shoppingCartItem")]
    public class ShoppingCartItemJsonObject
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("quantity")]
        public int? Quantity { get; set; }
        [JsonProperty("createdOnUtc")]
        public string CreatedOnUtc { get; set; }
        [JsonProperty("productId")]
        public int? ProductId { get; set; }
        [JsonProperty("product")]
        public ProductJsonObject Product { get; set; }
        [JsonProperty("type")]
        public ProductAttributeJsonObject Type { get; set; }
        [JsonProperty("color")]
        public ProductAttributeJsonObject Color { get; set; }
        [JsonProperty("size")]
        public ProductAttributeJsonObject Size { get; set; }
        [JsonProperty("subTotal")]
        public string SubTotal { get; set; }
        [JsonProperty("photoUrl")]
        public string PhotoUrl { get; set; }
        [JsonProperty("photoId")]
        public int PhotoId { get; set; }
    }
}
