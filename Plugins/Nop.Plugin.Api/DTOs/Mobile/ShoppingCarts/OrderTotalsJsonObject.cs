﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.DTOs.Mobile.ShoppingCarts
{
    [JsonObject(Title = "orderTotal")]
    public class OrderTotalsJsonObject
    {
        [JsonProperty("subTotal")]
        public string SubTotal { get; set; }
        [JsonProperty("subTotalDiscount")]
        public string SubTotalDiscount { get; set; }
        [JsonProperty("shipping")]
        public string Shipping { get; set; }
        [JsonProperty("orderTotalDiscount")]
        public string OrderTotalDiscount { get; set; }
        [JsonProperty("orderTotal")]
        public string OrderTotal { get; set; }
        [JsonProperty("isFreeShipping")]
        public bool IsFreeShipping { get; set; }
        [JsonProperty("selectedShippingMethod")]
        public string SelectedShippingMethod { get; set; }
    }
}
