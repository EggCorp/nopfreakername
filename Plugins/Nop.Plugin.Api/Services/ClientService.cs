﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Customers;
using Nop.Core.Infrastructure;
using Nop.Plugin.Api.Domain;
using Nop.Services.Common;

namespace Nop.Plugin.Api.Services
{
    public class ClientService : IClientService
    {
        private readonly IRepository<Client> _clientRepository;
        private readonly ICustomerApiService _customerApiService;
        public ClientService(IRepository<Client> clientRepository, ICustomerApiService customerApiService)
        {
            _clientRepository = clientRepository;
            _customerApiService = customerApiService;
        }

        public bool ValidateClient(string clientId,  string refreshToken)
        {
            return _clientRepository.Table.Any(client => client.ClientId == clientId && client.RefreshToken == refreshToken);
        }

        public Client GetClient(string clientId)
        {
            return _clientRepository.Table.FirstOrDefault(client => client.ClientId == clientId);
        }

        public bool ValidateClientById(string clientId)
        {
            return _clientRepository.Table.Any(client => client.ClientId == clientId);
        }

        public IList<Client> GetAllClients()
        {
            return _clientRepository.Table.ToList();
        }

        public Client GetClientById(int id)
        {
            return _clientRepository.GetById(id);
        }

        public Client GetClientByClientId(string clientId)
        {
            return _clientRepository.Table.FirstOrDefault(client => client.ClientId == clientId);
        }

        public void InsertClient(Client client)
        {
            if (client == null)
            {
                throw new ArgumentNullException("client");
            }
            var mobileCustomer = this._customerApiService.InsertMobileCustomer(client.ClientId);
            if (mobileCustomer == null)
            {
                throw new ArgumentNullException("mobileCustomer");
            }
            _clientRepository.Insert(client);
        }

        public void UpdateClient(Client client)
        {
            if (client == null)
            {
                throw new ArgumentNullException("client");
            }

            _clientRepository.Update(client);
        }

        public void DeleteClient(Client client)
        {
            if (client == null)
                throw new ArgumentNullException("client");

            _clientRepository.Delete(client);
        }
        public CustomerLoginResults ValidateCustomer(string usernameOrEmail, string password)
        {
            return this._customerApiService.ValidateCustomer(usernameOrEmail, password);
        }
        public virtual Client GetLoggedInClient(string usernameOrEmail)
        {
            var customer = this._customerApiService.GetLoggedInCustomer(usernameOrEmail); ;
            if (customer != null)
            {
                var client = this.GetClientByClientId(customer.CustomerGuid.ToString());
                return client;
            }
            else
                return null;
        }
        public virtual Client CreateLoggedInClient(string usernameOrEmail)
        {
            var customer = this._customerApiService.GetLoggedInCustomer(usernameOrEmail); ;
            if (customer != null)
            {
                var client = this.GetClientByClientId(customer.CustomerGuid.ToString());
                if(client==null)
                {
                    client = new Client
                    {
                        ClientId = customer.CustomerGuid.ToString(),
                        CreatedOnUtc = DateTime.UtcNow,
                        IsActive = true
                    };
                    _clientRepository.Insert(client);
                }
                return client;
            }
            else
                return null;
        }

        public virtual Client GetClientByToken(string token)
        {
             return _clientRepository.Table.FirstOrDefault(cl => cl.AccessToken.Equals(token));
        }
        public bool IsValidClientToken(string token)
        {
            var client = _clientRepository.Table.FirstOrDefault(cl => cl.AccessToken.Equals(token));
            return client != null;
        }
        public void UpdateClientActivity(string clientId, string request)
        {
            try
            {
                var _cGuid = new Guid(clientId);
                var customer = _customerApiService.GetCustomerByGuid(_cGuid);
                if (customer != null)
                {
                   
                    //update
                    customer.LastActivityDateUtc = DateTime.UtcNow;
                    customer.LastIpAddress = "via.api";
                    _customerApiService.UpdateCustomer(customer);
                    var genericAttributeService = EngineContext.Current.Resolve<IGenericAttributeService>();
                    var previousPageUrl = customer.GetAttribute<string>(SystemCustomerAttributeNames.LastVisitedPage);
                    if (!request.Equals(previousPageUrl))
                    {
                        genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.LastVisitedPage, request);
                    }
                }
            }
            catch(Exception ex)
            {
               
            }
        }
    }
}