﻿using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using Nop.Core.Infrastructure;
using Nop.Plugin.Api.Domain;
using Nop.Plugin.Api.Helpers;
using Nop.Plugin.Api.Services;

namespace Nop.Plugin.Api.OAuth.Attributes
{
    class MobileActiveTokenAttribute : AuthorizeAttribute
    {
        private readonly IAuthorizationHelper _authorizationHelper = EngineContext.Current.Resolve<IAuthorizationHelper>();

        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
          
            var queryString = actionContext.Request.GetQueryNameValuePairs().ToDictionary(x => x.Key, x => x.Value);
            string _token;
            bool isHasTokenQueryString = queryString.TryGetValue("token", out _token);
            if (isHasTokenQueryString)
            {
                string _requestUrl = actionContext.Request.RequestUri.AbsolutePath;
                IClientService clientService = EngineContext.Current.Resolve<IClientService>();
                var client = clientService.GetClientByToken(_token);
                if (client!=null)
                {
                    //update client activity
                    clientService.UpdateClientActivity(client.ClientId, _requestUrl);
                }

            }

            return true;
        }
    }
}