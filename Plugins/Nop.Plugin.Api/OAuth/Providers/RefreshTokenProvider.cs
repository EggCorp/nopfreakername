﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Owin.Security.Infrastructure;
using Nop.Core.Infrastructure;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.Services;

namespace Nop.Plugin.Api.Owin.OAuth.Providers
{
    public class RefreshTokenProvider : IAuthenticationTokenProvider
    {
        public void Create(AuthenticationTokenCreateContext context)
        {
            context.SetToken(context.SerializeTicket());
        }

        public async Task CreateAsync(AuthenticationTokenCreateContext context)
        {
            context.Ticket.Properties.IssuedUtc = DateTime.UtcNow;
            context.Ticket.Properties.ExpiresUtc = DateTime.UtcNow.AddMinutes(Configurations.RefreshTokenExpirationMinutes);
            System.Security.Claims.Claim clientClaim = context.Ticket.Identity.Claims.FirstOrDefault(x => x.Type == "client_id");

            if(clientClaim!=null)
            {
                IClientService clientService = EngineContext.Current.Resolve<IClientService>();
                var client = clientService.GetClientByClientId(clientClaim.Value);
                if (client != null)
                {
                    var refreshToken = context.SerializeTicket();
                    client.RefreshToken = refreshToken;
                    client.ExpiresIn = DateTime.UtcNow.AddMinutes(Configurations.AccessTokenExpirationMinutes);
                    client.RenewCount = client.RenewCount == null ? 0 : client.RenewCount+1;
                    clientService.UpdateClient(client);
                    context.SetToken(refreshToken);
                }
            }
        }

        public void Receive(AuthenticationTokenReceiveContext context)
        {
            context.DeserializeTicket(context.Token);
        }

        public Task ReceiveAsync(AuthenticationTokenReceiveContext context)
        {
            context.DeserializeTicket(context.Token);

            return Task.FromResult<object>(null);
        }
    }
}
