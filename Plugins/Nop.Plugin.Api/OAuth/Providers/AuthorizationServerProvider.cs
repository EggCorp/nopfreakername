﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Nop.Core.Domain.Customers;
using Nop.Core.Infrastructure;
using Nop.Plugin.Api.Domain;
using Nop.Plugin.Api.Services;

namespace Nop.Plugin.Api.Owin.OAuth.Providers
{
    public class AuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            ApiSettings settings = EngineContext.Current.Resolve<ApiSettings>();

            if (!settings.EnableApi)
            {
                context.SetError("invalid_call", "Could not access private resources on this server!");
                context.Rejected();
            }
            else
            {

                string grantType = context.Parameters.Get("grant_type");
                string clientId = context.Parameters.Get("client_id"); 
                IClientService clientService = EngineContext.Current.Resolve<IClientService>();

                bool valid = false;
                switch (grantType)
                {
                    case "refresh_token": {
                            /*if (!string.IsNullOrEmpty(refreshToken))
                            {
                                if (!string.IsNullOrEmpty(clientId))
                                {
                                    valid = clientService.ValidateClient(clientId, refreshToken);
                                }
                                else if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
                                {
                                    //check login
                                    var validCustomer = clientService.ValidateCustomer(username, password);
                                    if (validCustomer == CustomerLoginResults.Successful)
                                    {
                                        var currentClient = clientService.GetLoggedInClient(username);
                                        if (currentClient != null)
                                        {
                                            clientId = currentClient.ClientId;
                                            valid = clientService.ValidateClient(currentClient.ClientId, refreshToken);
                                        }

                                    }
                                    else
                                    {
                                        context.SetError("invalid_user", validCustomer.ToString());
                                        context.Rejected();
                                    }
                                }
                            }*/
                            string refreshToken = context.Parameters.Get("refresh_token");
                            valid = clientService.ValidateClient(clientId, refreshToken);
                            if (!valid)
                            {
                                context.SetError("invalid_user_refresh_token", "Could not refresh token");
                                context.Rejected();
                                return;
                            }
                        } break;
                    case "password": {
                            string username = context.Parameters.Get("username");
                            string password = context.Parameters.Get("password");
                            if (!string.IsNullOrEmpty(clientId))
                            {
                                valid = clientService.ValidateClientById(clientId);
                            }
                            else if (!string.IsNullOrEmpty(username)&&!string.IsNullOrEmpty(password))
                            {
                                //check login
                                var validCustomer = clientService.ValidateCustomer(username, password);
                                if (validCustomer == CustomerLoginResults.Successful)
                                {
                                    var currentClient = clientService.CreateLoggedInClient(username);
                                    if (currentClient != null)
                                        clientId = currentClient.ClientId;
                                    valid = currentClient != null;
                                }
                                else
                                {
                                    context.SetError("invalid_user", validCustomer.ToString());
                                    context.Rejected();
                                }

                            }
                        } break;
                    case "authorization_code": break;
                    default: break;


                }
                Client client=null;
                if (valid)
                    client = clientService.GetClient(clientId);
                else
                {
                    client = new Client
                    {
                        ClientId = clientId,
                        CreatedOnUtc = DateTime.UtcNow,
                        IsActive = true,
                    };
                    clientService.InsertClient(client);
                }
                if (client!=null)
                {
                    context.OwinContext.Set("oauth:client", client);
                    context.Validated();
                }
                else
                {
                    context.SetError("invalid_user", "User not active or invalid!");
                    context.Rejected();
                }
            }
        }

        // We need this method because the granting of the credentials can't work withouth Identity and AuthenticationTicket,
        // so we need to connect our Client with the Identity and AuthenicationTicket.
        public override Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            Client client = context.OwinContext.Get<Client>("oauth:client");
            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            //Think if we will need the callback url because all the data is contained in the token end point response.
            identity.AddClaim(new Claim("client_id", client.ClientId));

            var ticket = new AuthenticationTicket(identity, new AuthenticationProperties());
            context.Validated(ticket);

            return base.GrantResourceOwnerCredentials(context);
        }
        public override Task  TokenEndpointResponse(OAuthTokenEndpointResponseContext context)
        {
            var accessToken = context.AccessToken;
            Claim clientClaim = context.Identity.Claims.FirstOrDefault(x => x.Type == "client_id");

            if (clientClaim != null)
            {
                IClientService clientService = EngineContext.Current.Resolve<IClientService>();
                var client = clientService.GetClientByClientId(clientClaim.Value);
                if (client != null)
                {
                    client.AccessToken = accessToken;
                    client.TokenType = context.Options.AuthenticationType;
                    clientService.UpdateClient(client);
                }
            }
            return base.TokenEndpointResponse(context);
        }
    }
}