﻿using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Api.Attributes;
using Nop.Plugin.Api.Delta;
using Nop.Plugin.Api.DTOs.Orders;
using Nop.Plugin.Api.Factories;
using Nop.Plugin.Api.Helpers;
using Nop.Plugin.Api.ModelBinders;
using Nop.Plugin.Api.OAuth.Attributes;
using Nop.Plugin.Api.Serializers;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Security;
using Nop.Services.SMS;
using Nop.Services.Stores;
using Nop.Services.SubscribeMessenger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;

namespace Nop.Plugin.Api.Controllers
{
    public class CRMPassingDataController : BaseApiController
    {
        private readonly IDTOHelper _dtoHelper;
        private readonly IStoreContext _storeContext;
        private readonly IFactory<Order> _factory;
        private readonly Nop.Services.Messages.IEggReMarketingEmailService _eggReMarketingEmailService;
        private readonly ISubscribeMessageService _subscribeMessageService;
        private readonly ISMSMarketingService _smsService;


        public CRMPassingDataController(
            IJsonFieldsSerializer jsonFieldsSerializer,
            IAclService aclService,
            ICustomerService customerService,
            IStoreMappingService storeMappingService,
            IStoreService storeService,
            IDiscountService discountService,
            ICustomerActivityService customerActivityService,
            ILocalizationService localizationService,
            IFactory<Order> factory,
            IStoreContext storeContext,
            IPictureService pictureService,
            IDTOHelper dtoHelper,
            IEggReMarketingEmailService eggReMarketingEmailService,
            ISMSMarketingService smsService,
            ISubscribeMessageService subscribeMessageService)
            : base(jsonFieldsSerializer, aclService, customerService, storeMappingService,
                 storeService, discountService, customerActivityService, localizationService, pictureService)
        {
            _factory = factory;
            _storeContext = storeContext;
            _dtoHelper = dtoHelper;
            _eggReMarketingEmailService = eggReMarketingEmailService;
            _subscribeMessageService = subscribeMessageService;
            _smsService = smsService;
        }


        [HttpPost]
        public IHttpActionResult SendEmailToCustomer([ModelBinder(typeof(JsonModelBinder<EmailMessageDto>))] Delta<EmailMessageDto> emailDelta)
        {
            try
            {
                string _to = emailDelta.Dto.Email;
                string _subject = emailDelta.Dto.Subject;
                string _body = emailDelta.Dto.Body;

                if (string.IsNullOrEmpty(_to) || string.IsNullOrEmpty(_body))
                    return Error(HttpStatusCode.BadRequest, "send_email", "invalid data");

                this._eggReMarketingEmailService.SendCustomEmailToCustomer(_to,_body,_subject);
                return Ok();
            }
            catch (Exception ex)
            {
                return Error(HttpStatusCode.NotFound, "send_email", ex.Message);
            }
        }

        [HttpPost]
        public IHttpActionResult SendSMSToCustomer([ModelBinder(typeof(JsonModelBinder<SMSMessageDto>))] Delta<SMSMessageDto> smsDelta)
        {
            try
            {
                string _to = smsDelta.Dto.To;
                string _message = smsDelta.Dto.Message;

                if (string.IsNullOrEmpty(_to) || string.IsNullOrEmpty(_message))
                    return Error(HttpStatusCode.BadRequest, "send_sms", "invalid data");

                this._smsService.SendCustomizationSMS(_to, _message);
                return Ok();
            }
            catch (Exception ex)
            {
                return Error(HttpStatusCode.NotFound, "send_sms", ex.Message);
            }
        }
    }
}