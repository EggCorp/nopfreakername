﻿using Nop.Core.Domain.Catalog;
using Nop.Plugin.Api.Serializers;
using Nop.Plugin.Api.Services;
using Nop.Services.Catalog;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Seo;
using Nop.Plugin.Api.Factories;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.Media;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Plugin.Api.Helpers;
using Nop.Plugin.Api.OAuth.Attributes;

namespace Nop.Plugin.Api.Controllers.Mobile
{
    [MobileActiveToken]
    public class MobileCategoriesController: BaseApiController
    {
        private readonly ICategoryApiService _categoryApiService;
        private readonly ICategoryService _categoryService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly IPictureService _pictureService;
        private readonly IFactory<Category> _factory;
        private readonly IDTOHelper _dtoHelper;
        public MobileCategoriesController(ICategoryApiService categoryApiService,
           IJsonFieldsSerializer jsonFieldsSerializer,
           ICategoryService categoryService,
           IUrlRecordService urlRecordService,
           ICustomerActivityService customerActivityService,
           ILocalizationService localizationService,
           IPictureService pictureService,
           IStoreMappingService storeMappingService,
           IStoreService storeService,
           IDiscountService discountService,
           IAclService aclService,
           ICustomerService customerService,
           IFactory<Category> factory,
           IDTOHelper dtoHelper) : base(jsonFieldsSerializer, aclService, customerService, storeMappingService, storeService, discountService, customerActivityService, localizationService, pictureService)
        {
            _categoryApiService = categoryApiService;
            _categoryService = categoryService;
            _urlRecordService = urlRecordService;
            _factory = factory;
            _pictureService = pictureService;
            _dtoHelper = dtoHelper;
        }
    }
}
