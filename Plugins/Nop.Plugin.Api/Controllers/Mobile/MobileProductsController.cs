﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Discounts;
using Nop.Core.Domain.Media;
using Nop.Plugin.Api.Attributes;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.Delta;
using Nop.Plugin.Api.DTOs.Images;
using Nop.Plugin.Api.DTOs.Products;
using Nop.Plugin.Api.Factories;
using Nop.Plugin.Api.JSON.ActionResults;
using Nop.Plugin.Api.ModelBinders;
using Nop.Plugin.Api.Models.ProductsParameters;
using Nop.Plugin.Api.Serializers;
using Nop.Plugin.Api.Services;
using Nop.Services.Catalog;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Stores;
using Nop.Plugin.Api.Helpers;
using Nop.Plugin.Api.DTOs.Mobile.Products;
using Newtonsoft.Json.Linq;
using System.Web;
using System.Globalization;
using Nop.Plugin.Api.OAuth.Attributes;

namespace Nop.Plugin.Api.Controllers.Mobile
{
    [MobileActiveToken]
    public class MobileProductsController : BaseApiController
    {
        private readonly IStoreContext _storeContext;
        private readonly IProductApiService _productApiService;
        private readonly IProductService _productService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly IPictureService _pictureService;
        private readonly IManufacturerService _manufacturerService;
        private readonly IFactory<Product> _factory;
        private readonly IProductTagService _productTagService;
        private readonly IProductAttributeService _productAttributeService;
        private readonly IDTOHelper _dtoHelper;
        private readonly ICategoryService _categoryService;
        private readonly ICategoryApiService _categoryApiService;
        private readonly CatalogSettings _catalogSettings;
        private readonly IProductAttributeParser _productAttributeParser;
        public MobileProductsController(IProductApiService productApiService,
                                IJsonFieldsSerializer jsonFieldsSerializer,
                                IProductService productService,
                                IUrlRecordService urlRecordService,
                                ICustomerActivityService customerActivityService,
                                ILocalizationService localizationService,
                                IFactory<Product> factory,
                                IAclService aclService,
                                IStoreMappingService storeMappingService,
                                IStoreService storeService,
                                ICustomerService customerService,
                                IDiscountService discountService,
                                IPictureService pictureService,
                                IManufacturerService manufacturerService,
                                IProductTagService productTagService,
                                IProductAttributeService productAttributeService,
                                IDTOHelper dtoHelper, 
                                IStoreContext storeContext,
                                ICategoryApiService categoryApiService,
                                ICategoryService categoryService,
                                CatalogSettings catalogSettings,
                                IProductAttributeParser productAttributeParser) : base
            (jsonFieldsSerializer, aclService, customerService, storeMappingService, storeService, discountService, customerActivityService, localizationService, pictureService)
        {
            _productApiService = productApiService;
            _factory = factory;
            _manufacturerService = manufacturerService;
            _productTagService = productTagService;
            _urlRecordService = urlRecordService;
            _productService = productService;
            _productAttributeService = productAttributeService;
            _dtoHelper = dtoHelper;
            _storeContext = storeContext;
            _pictureService = pictureService;
            _categoryApiService = categoryApiService;
            _categoryService = categoryService;
            _catalogSettings = catalogSettings;
            _productAttributeParser = productAttributeParser;
        }

        /// <summary>
        /// Receive a list of all products
        /// </summary>
        /// <response code="200">OK</response>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [ResponseType(typeof(List<ProductJsonObject>))]
        [GetRequestsErrorInterceptorActionFilter]
        public IHttpActionResult GetProducts(ProductsParametersModel parameters)
        {
            if (parameters.Limit < Configurations.MinLimit || parameters.Limit > Configurations.MaxLimit)
            {
                return Error(HttpStatusCode.BadRequest, "limit", "invalid limit parameter");
            }

            if (parameters.Page < Configurations.DefaultPageValue)
            {
                return Error(HttpStatusCode.BadRequest, "page", "invalid page parameter");
            }

            var allProducts = _productApiService.GetProducts(parameters.Ids, parameters.CreatedAtMin, parameters.CreatedAtMax, parameters.UpdatedAtMin,
                                                                        parameters.UpdatedAtMax, parameters.Limit, parameters.Page, parameters.SinceId, parameters.CategoryId,
                                                                        parameters.VendorName, parameters.PublishedStatus)
                                                .Where(p => _storeMappingService.Authorize(p));

            IList<ProductJsonObject> productsAsJsonObjects = allProducts.Select(product =>
            {
                return _dtoHelper.PrepareProductJsonObject(product);

            }).ToList();

            JToken jToken = JToken.FromObject(productsAsJsonObjects);
            string jTokenResult = jToken.ToString();
            return new RawJsonActionResult(jTokenResult);
        }

        /// <summary>
        /// Retrieve product by spcified id
        /// </summary>
        /// <param name="id">Id of the product</param>
        /// <param name="fields">Fields from the product you want your json to contain</param>
        /// <response code="200">OK</response>
        /// <response code="404">Not Found</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [ResponseType(typeof(ProductDetailJsonObject))]
        [GetRequestsErrorInterceptorActionFilter]
        public IHttpActionResult GetProductById(int id)
        {
            if (id <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "id", "invalid id");
            }

            Product product = _productApiService.GetProductById(id);

            if (product == null)
            {
                return Error(HttpStatusCode.NotFound, "product", "not found");
            }

            ProductDetailJsonObject productDetailJsonObject = new ProductDetailJsonObject();
            this.PrepareProductDetailJsonObject(productDetailJsonObject, product);
            JToken jToken = JToken.FromObject(productDetailJsonObject);
            string jTokenResult = jToken.ToString();
            return new RawJsonActionResult(jTokenResult);
        }
       
        protected void PrepareProductDetailJsonObject(ProductDetailJsonObject model, Product product)
        {
            model.Id = product.Id;
            model.Name = product.Name;
            model.SeName = product.GetSeName();
            model.Detail = product.FullDescription;
            var customFieldMappings = product.ProductCustomFieldMappings;
            foreach(var item in customFieldMappings)
            {
                if (item.CustomField != null)
                {
                    CustomFieldJsonObject customfield = new CustomFieldJsonObject
                    {
                        Id = item.CustomField.Id,
                        Name= item.CustomField.Name,
                        TypeId= item.CustomField.TypeId,
                        Type = item.CustomField.Type.ToString(),
                        Group = item.Group,
                        Description = item.CustomField.Description
                    };
                    model.Customfields.Add(customfield);
                }
            }
            model.Customable = model.Customfields.Any();
            var allCustomfieldValues = customFieldMappings.Select(p => p.ProductCustomFieldValues).ToList();
            //product images
            foreach (var productPicture in product.ProductPictures.OrderBy(p=>p.DisplayOrder).ToList())
            {
                var pictureUrl = _pictureService.GetApiPictureUrl(productPicture.PictureId);
                model.Photos.Add(pictureUrl);
                model.PhotoIds.Add(productPicture.PictureId);
            }
            model.PhotoUrl = model.Photos.FirstOrDefault();
            model.PhotoId = model.PhotoIds.FirstOrDefault();

            // all type
            var availableProductsAttrMap = product.ProductAttributeMappings.Where(p => p.ProductAttribute.Name.Equals(Common.ProductTypeAttributeValue)).FirstOrDefault();
            //all color
            var colorsAttrMap = product.ProductAttributeMappings.Where(p => p.AttributeControlType == AttributeControlType.ColorSquares).ToList();
            //all size
            var sizingAttrMap = product.ProductAttributeMappings.Where(p => p.ProductAttribute.Name.Substring(0, 5).Equals("size_")).ToList();

            if (availableProductsAttrMap != null)
            {
               foreach(var attributeValue in availableProductsAttrMap.ProductAttributeValues)
                {
                    //Type
                    ProductStyleJsonObject productStyle = new ProductStyleJsonObject();
                    productStyle.Type = new ProductAttributeJsonObject
                    {
                        Id = attributeValue.Id,
                        Name = attributeValue.Name,
                        IsPreSelected = attributeValue.IsPreSelected,
                        Price = attributeValue.PriceAdjustment,
                        PhotoId = attributeValue.PictureId,
                        PhotoUrl =  _pictureService.GetApiPictureUrl(attributeValue.PictureId),
                    };
                    //Color
                    if (colorsAttrMap != null)
                    {
                        for (int i = 0; i < colorsAttrMap.Count; i++)
                        {
                            var attr = colorsAttrMap[i];
                            var valuesThatShouldBeSelected = _productAttributeParser.ParseConditionValue(attr.ConditionAttributeXml, attributeValue.ProductAttributeMappingId, attributeValue.Id).ToList();
                            if (valuesThatShouldBeSelected.Any())
                            {
                                //parse color
                                foreach (var colorValue in attr.ProductAttributeValues)
                                {
                                    var colorJson = new ProductAttributeJsonObject
                                    {
                                        Id = colorValue.Id,
                                        Name = colorValue.Name,
                                        IsPreSelected = colorValue.IsPreSelected,
                                        Price = colorValue.PriceAdjustment,
                                        PhotoUrl = _pictureService.GetApiPictureUrl(colorValue.PictureId),
                                        Code = colorValue.ColorSquaresRgb,
                                        PhotoId = colorValue.PictureId
                                    };
                                    productStyle.Colors.Add(colorJson);
                                }
                                colorsAttrMap.Remove(attr);
                                break;
                            }
                        }
                    }

                    //Sizing
                    if(sizingAttrMap!=null)
                    {
                        for (int i = 0; i < sizingAttrMap.Count; i++)
                        {
                            var attr = sizingAttrMap[i];
                            var valuesThatShouldBeSelected = _productAttributeParser.ParseConditionValue(attr.ConditionAttributeXml, attributeValue.ProductAttributeMappingId, attributeValue.Id).ToList();
                            if (valuesThatShouldBeSelected.Any())
                            {
                                //parse color
                                foreach (var sizeValue in attr.ProductAttributeValues)
                                {
                                    var sizeJson = new ProductAttributeJsonObject
                                    {
                                        Id = sizeValue.Id,
                                        Name = sizeValue.Name,
                                        IsPreSelected = sizeValue.IsPreSelected,
                                        Price = sizeValue.PriceAdjustment,
                                    };
                                    productStyle.Sizes.Add(sizeJson);
                                }
                                sizingAttrMap.Remove(attr);
                                break;
                            }
                        }
                        model.Styles.Add(productStyle);
                    }

                    //custom data
                    if (model.Customable)
                    {
                        foreach(var customfieldMapping in customFieldMappings)
                        {
                            var selectedValue = customfieldMapping.ProductCustomFieldValues.FirstOrDefault(p => p.ProductAttributeValueId == productStyle.Type.Id);
                            if(selectedValue!=null)
                            {
                                var custom = new CustomDataJsonObject();
                                custom.Id = customfieldMapping.CustomFieldId;
                                custom.Text = string.Empty;
                                custom.ValueId = selectedValue.Id;
                                productStyle.CustomData.Add(custom);
                            }
                        }
                    }
                }
            }

            //product price
            var preselectStyle = availableProductsAttrMap.ProductAttributeValues.FirstOrDefault(p => p.IsPreSelected);
            if(preselectStyle==null)
                preselectStyle =availableProductsAttrMap.ProductAttributeValues.FirstOrDefault();
            model.NewPrice = preselectStyle.PriceAdjustment;
            model.OldPrice = preselectStyle.PriceAdjustment + _catalogSettings.SaveAmount;

            ///onsela
            ///
            model.OnSale = product.DisableBuyButton || product.Published || product.Deleted;
            model.SaleEndAt = this.GetSellOffExpiresDay();
        }
        protected virtual string GetSellOffExpiresDay()
        {
            try
            {
                string cookieName = "MobileSellOffExpiresDayCookie";
                HttpCookie cookie = HttpContext.Current.Response.Cookies[cookieName];
                if (cookie != null)
                {
                    string sellOffExpiresDayCookie = HttpUtility.HtmlEncode(cookie.Value);
                    if (string.IsNullOrEmpty(sellOffExpiresDayCookie))
                    {
                        HttpContext.Current.Response.Cookies[cookieName].Value = DateTime.UtcNow.AddDays(1).ToString("o");
                    }
                    else
                    {
                        DateTime sellOffExpiresDay = DateTime.Parse(sellOffExpiresDayCookie, new CultureInfo("en-CA"));
                        if (sellOffExpiresDay < DateTime.Now)
                        {
                            HttpContext.Current.Response.Cookies[cookieName].Value = DateTime.UtcNow.AddDays(1).ToString("o");
                        }
                        else
                        {
                        }
                    }
                }
                else
                {
                    cookie = new HttpCookie(cookieName);
                    cookie.Value = DateTime.UtcNow.AddDays(1).ToString("o");
                    HttpContext.Current.Response.Cookies.Add(cookie);
                }
                string result = HttpUtility.HtmlEncode(HttpContext.Current.Response.Cookies[cookieName].Value);
                return result;
            }
            catch (Exception ex)
            { }
            return string.Empty;
        }
    }
}
