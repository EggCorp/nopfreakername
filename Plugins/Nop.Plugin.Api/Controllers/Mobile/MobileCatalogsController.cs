﻿using System;
using System.Linq;
using System.Web.Http;
using Nop.Core.Domain.Catalog;
using Nop.Plugin.Api.Serializers;
using Nop.Plugin.Api.Services;
using Nop.Services.Catalog;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Seo;
using Nop.Plugin.Api.Factories;
using Nop.Plugin.Api.JSON.ActionResults;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.Media;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Plugin.Api.Helpers;
using Nop.Plugin.Api.DTOs.Mobile.HomeDatas;
using Newtonsoft.Json.Linq;
using Nop.Plugin.Api.DTOs.Mobile.Categories;
using Nop.Services.Configuration;
using Nop.Plugin.Api.DTOs;
using System.Net;
using Nop.Plugin.Api.OAuth.Attributes;
using System.Web.Http.Description;
using System.Collections.Generic;
using Nop.Plugin.Api.DTOs.Mobile.Products;
using Nop.Plugin.Api.Constants;

namespace Nop.Plugin.Api.Controllers.Mobile
{
    [MobileActiveToken]
    public class MobileCatalogsController : BaseApiController
    {
        private readonly ICategoryApiService _categoryApiService;
        private readonly ICategoryService _categoryService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly IPictureService _pictureService;
        private readonly IFactory<Category> _factory;
        private readonly IDTOHelper _dtoHelper;
        private readonly IHotTrendingService _hotTrendingService;
        private readonly ISettingService _settingService;
        public MobileCatalogsController(ICategoryApiService categoryApiService,
            IJsonFieldsSerializer jsonFieldsSerializer,
            ICategoryService categoryService,
            IUrlRecordService urlRecordService,
            ICustomerActivityService customerActivityService,
            ILocalizationService localizationService,
            IPictureService pictureService,
            IStoreMappingService storeMappingService,
            IStoreService storeService,
            IDiscountService discountService,
            IAclService aclService,
            ICustomerService customerService,
            IFactory<Category> factory,
            IHotTrendingService hotTrendingService,
            IDTOHelper dtoHelper,
            ISettingService settingService) : base(jsonFieldsSerializer, aclService, customerService, storeMappingService, storeService, discountService, customerActivityService, localizationService, pictureService)
        {
            _categoryApiService = categoryApiService;
            _categoryService = categoryService;
            _urlRecordService = urlRecordService;
            _factory = factory;
            _pictureService = pictureService;
            _dtoHelper = dtoHelper;
            _hotTrendingService = hotTrendingService;
            _settingService = settingService;
        }

        [HttpGet]
        [ResponseType(typeof(HomeDataJsonObject))]
        public IHttpActionResult GetHomePageData()
        {
            try
            {
                var result = new HomeDataJsonObject();
                var newestProducts = this._categoryService.GetStoreNewestProducts();
                var allCategories = _categoryService.GetAllCategories();
                var bestsellingProducts = this._categoryService.GetTopProductBestsellers();
                var allHottrending = _hotTrendingService.GetHotTrendingForHomePage();
                var hotTrendings = allHottrending.Where(p => !p.IsSellOffBanner).OrderBy(p => p.DisplayOrder);
                //banner
                var nivoSliderSettings = _settingService.LoadSetting<NivoSliderSettings>();
                if (nivoSliderSettings != null)
                {
                    string bannerLink = string.Empty;
                    int bannerPictureId = 0;
                    if(nivoSliderSettings.Picture1Id>0)
                    {
                        bannerPictureId = nivoSliderSettings.Picture1Id;
                        bannerLink = nivoSliderSettings.Link1;
                    }
                    else if (nivoSliderSettings.Picture2Id > 0)
                    {
                        bannerPictureId = nivoSliderSettings.Picture1Id;
                        bannerLink = nivoSliderSettings.Link1;
                    }
                    else if (nivoSliderSettings.Picture3Id > 0)
                    {
                        bannerPictureId = nivoSliderSettings.Picture3Id;
                        bannerLink = nivoSliderSettings.Link3;
                    }
                    if(!string.IsNullOrEmpty(bannerLink)&& bannerPictureId > 0)
                    {
                        bannerLink = bannerLink.Replace("/", "");
                        bannerLink = bannerLink.ToLower();
                        var bannerCat = allCategories.Where(p => bannerLink.Equals(p.GetSeName())).FirstOrDefault();
                        result.Banner = new CategoryJsonObject();
                        result.Banner.Id = bannerCat.Id;
                        result.Banner.Name = bannerCat.Name;
                        var picture = _pictureService.GetPictureById(bannerPictureId);
                        result.Banner.PhotoId = picture == null ? 0 : picture.Id;
                        result.Banner.PhotoUrl = _pictureService.GetApiPictureUrl(result.Banner.PhotoId);
                        var imgSize = _pictureService.GetPictureOriginalSize(result.Banner.PhotoId);
                        result.Banner.PhotoHeight = imgSize.Height;
                        result.Banner.PhotoWidth = imgSize.Width;
                        result.Banner.SeName = bannerCat.GetSeName();
                        result.Banner.HasSubs = _categoryService.GetAllCategoriesByParentCategoryId(bannerCat.Id).Any();
                    }

                }
                //Top categories - HotTrend
                foreach (var item in hotTrendings)
                {
                    if (!string.IsNullOrEmpty(item.RedirectUrl))
                    {
                        string trendUrl = item.RedirectUrl.ToLower();
                        var trendCat = allCategories.Where(p => trendUrl.Equals(p.GetSeName())).FirstOrDefault();
                        if (trendCat != null)
                        {
                            var catJson = new CategoryJsonObject();
                            catJson.Id = trendCat.Id;
                            catJson.Name = item.Name;
                            var picture = _pictureService.GetPictureById(item.PictureId);
                            catJson.PhotoId = picture==null? 0: picture.Id;
                            catJson.PhotoUrl = _pictureService.GetApiPictureUrl(catJson.PhotoId);
                            var imgSize = _pictureService.GetPictureOriginalSize(catJson.PhotoId);
                            catJson.PhotoWidth = imgSize.Width;
                            catJson.PhotoHeight = imgSize.Height;
                            catJson.SeName = trendCat.GetSeName();
                            catJson.HasSubs = _categoryService.GetAllCategoriesByParentCategoryId(trendCat.Id).Any();
                            result.TopCategories.Add(catJson);
                        }
                    } 
                }
                //Mid banner
                var midBanner = allHottrending.Where(p => p.IsSellOffBanner).FirstOrDefault();
                if (midBanner != null)
                {
                    var bannerPicture = _pictureService.GetPictureById(midBanner.PictureId);
                    result.MidBannerPhotoId = bannerPicture == null ? 0 : bannerPicture.Id;
                    result.MidBanner = _pictureService.GetApiPictureUrl(result.MidBannerPhotoId);
                    var imgSize = _pictureService.GetPictureOriginalSize(result.MidBannerPhotoId);
                    result.MidBannerWidth = imgSize.Width;
                    result.MidBannerHeight = imgSize.Height;
                }

                //newest garment
                result.NewestProducts = newestProducts.Select(prd =>
                {
                    return _dtoHelper.PrepareProductJsonObject(prd);

                }).ToList();

                //Trending garment
                result.TrendingProducts = bestsellingProducts.Select(prd =>
                {
                    return _dtoHelper.PrepareProductJsonObject(prd);

                }).ToList();

                JToken jToken = JToken.FromObject(result);
                string jTokenResult = jToken.ToString();
                return new RawJsonActionResult(jTokenResult);
            }
            catch(Exception ex)
            {
                return Error(HttpStatusCode.BadRequest, "catalog", ex.Message);
            }
        }

        [HttpGet]
        [ResponseType(typeof(List<CategoryJsonObject>))]
        public IHttpActionResult GetSubCategoiesByParentId(int id)
        {
            try
            {
                var parentCategory = _categoryService.GetCategoryById(id);
                if(parentCategory==null)
                    return Error(HttpStatusCode.BadRequest, "id", "invalid id");

                var subCategories = _categoryService.GetAllCategoriesByParentCategoryId(parentCategory.Id);
                var result = subCategories.Select(cat =>
                {
                    return _dtoHelper.PrepareCategoryJsonObject(cat);

                }).ToList();
                JToken jToken = JToken.FromObject(result);
                string jTokenResult = jToken.ToString();
                return new RawJsonActionResult(jTokenResult);
            }
            catch (Exception ex)
            {
                return Error(HttpStatusCode.BadRequest, "catalog", ex.Message);
            }
        }
        [HttpGet]
        [ResponseType(typeof(List<ProductJsonObject>))]
        public IHttpActionResult GetNewestProducts(int page = Configurations.DefaultPageValue, int limit = Configurations.DefaultLimit)
        {
            if (limit < Configurations.MinLimit || limit > Configurations.MaxLimit)
            {
                return Error(HttpStatusCode.BadRequest, "limit", "invalid limit parameter");
            }

            if (page < Configurations.DefaultPageValue)
            {
                return Error(HttpStatusCode.BadRequest, "page", "invalid page parameter");
            }

            var trendingProducts = _categoryService.GetNewestGarment(page-1, limit);

            IList<ProductJsonObject> productsAsJsonObjects = trendingProducts.Select(product =>
            {
                return _dtoHelper.PrepareProductJsonObject(product);

            }).ToList();

            JToken jToken = JToken.FromObject(productsAsJsonObjects);
            string jTokenResult = jToken.ToString();
            return new RawJsonActionResult(jTokenResult);
        }

        [HttpGet]
        [ResponseType(typeof(List<ProductJsonObject>))]
        public IHttpActionResult GetTrendingProducts(int page = Configurations.DefaultPageValue, int limit = Configurations.DefaultLimit)
        {
            if (limit < Configurations.MinLimit || limit > Configurations.MaxLimit)
            {
                return Error(HttpStatusCode.BadRequest, "limit", "invalid limit parameter");
            }

            if (page < Configurations.DefaultPageValue)
            {
                return Error(HttpStatusCode.BadRequest, "page", "invalid page parameter");
            }

            var trendingProducts = _categoryService.GetStoreBestsellerProducts(page-1, limit);

            IList<ProductJsonObject> productsAsJsonObjects = trendingProducts.Select(product =>
            {
                return _dtoHelper.PrepareProductJsonObject(product);

            }).ToList();

            JToken jToken = JToken.FromObject(productsAsJsonObjects);
            string jTokenResult = jToken.ToString();
            return new RawJsonActionResult(jTokenResult);
        }

        [HttpGet]
        [ResponseType(typeof(List<CategoryJsonObject>))]
        public IHttpActionResult GetTopMenu()
        {
            try
            {
                var allCategories = _categoryService.GetAllCategories();
                var topMenuCategories = allCategories.Where(p => p.ParentCategoryId == 0).ToList();
                topMenuCategories = topMenuCategories.OrderBy(p => p.DisplayOrder).ToList();
                var result = topMenuCategories.Select(cat =>
                {
                    return _dtoHelper.PrepareCategoryJsonObject(cat);

                }).ToList();
                JToken jToken = JToken.FromObject(result);
                string jTokenResult = jToken.ToString();
                return new RawJsonActionResult(jTokenResult);
            }
            catch (Exception ex)
            {
                return Error(HttpStatusCode.BadRequest, "catalog", ex.Message);
            }
        }
    }
}
