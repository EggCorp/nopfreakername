﻿using Nop.Core.Domain.Catalog;
using Nop.Plugin.Api.Serializers;
using Nop.Plugin.Api.Services;
using Nop.Services.Catalog;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Seo;
using Nop.Plugin.Api.Factories;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.Media;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Plugin.Api.Helpers;
using Nop.Plugin.Api.OAuth.Attributes;
using System.Web.Http;
using System.Web.Http.Description;
using Nop.Plugin.Api.Attributes;
using Newtonsoft.Json.Linq;
using Nop.Plugin.Api.JSON.ActionResults;
using Nop.Plugin.Api.DTOs.Mobile.ShoppingCarts;
using Nop.Core.Domain.Orders;
using System.Collections.Generic;
using Nop.Services.Orders;
using System;
using System.Net;
using System.Linq;
using Nop.Plugin.Api.DTOs.Mobile.Products;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Discounts;
using Nop.Core;
using Nop.Services.Tax;
using Nop.Services.Directory;
using Nop.Core.Domain.Tax;
using Nop.Services.Common;
using Nop.Core.Domain.Shipping;
using Nop.Core.Domain.Customers;
using System.Web.Http.ModelBinding;
using Nop.Plugin.Api.ModelBinders;
using Nop.Plugin.Api.Delta;
using Newtonsoft.Json;

namespace Nop.Plugin.Api.Controllers.Mobile
{
    [BearerTokenAuthorize]
    public class MobileShoppingCartController : BaseApiController
    {
        private readonly ICategoryApiService _categoryApiService;
        private readonly ICategoryService _categoryService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly IPictureService _pictureService;
        private readonly IFactory<Category> _factory;
        private readonly IDTOHelper _dtoHelper;
        private readonly IShoppingCartItemApiService _shoppingCartItemApiService;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly IProductService _productService;
        private readonly ICustomerApiService _customerApiService;
        private readonly IProductAttributeParser _productAttributeParser;
        private readonly ShoppingCartSettings _shoppingCartSettings;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly ITaxService _taxService;
        private readonly ICurrencyService _currencyService;
        private readonly IPriceCalculationService _priceCalculationService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IOrderTotalCalculationService _orderTotalCalculationService;
        private readonly ShippingSettings _shippingSettings;
        private readonly IProductAttributeService _productAttributeService;
        private readonly IAuthorizationHelper _authorizationHelper;
        public MobileShoppingCartController(ICategoryApiService categoryApiService,
           IJsonFieldsSerializer jsonFieldsSerializer,
           ICategoryService categoryService,
           IUrlRecordService urlRecordService,
           ICustomerActivityService customerActivityService,
           ILocalizationService localizationService,
           IPictureService pictureService,
           IStoreMappingService storeMappingService,
           IStoreService storeService,
           IDiscountService discountService,
           IAclService aclService,
           ICustomerService customerService,
           IFactory<Category> factory,
           IDTOHelper dtoHelper,
           IShoppingCartService shoppingCartService,
           IShoppingCartItemApiService shoppingCartItemApiService,
           IProductService productService,
           ShoppingCartSettings shoppingCartSettings,
           IProductAttributeParser productAttributeParser,
           IStoreContext storeContext,
           IWorkContext workContext,
           ITaxService taxService, ICurrencyService currencyService,
           IPriceCalculationService priceCalculationService,
           IPriceFormatter priceFormatter,
           IOrderTotalCalculationService orderTotalCalculationService,
           ShippingSettings shippingSettings,
           IProductAttributeService productAttributeService,
           IAuthorizationHelper authorizationHelper,
           ICustomerApiService  customerApiService) : base(jsonFieldsSerializer, aclService, customerService, storeMappingService, storeService, discountService, customerActivityService, localizationService, pictureService)
        {
            _categoryApiService = categoryApiService;
            _categoryService = categoryService;
            _urlRecordService = urlRecordService;
            _factory = factory;
            _pictureService = pictureService;
            _dtoHelper = dtoHelper;
            _shoppingCartItemApiService = shoppingCartItemApiService;
            _shoppingCartService = shoppingCartService;
            _productService = productService;
            _customerApiService = customerApiService;
            this._productAttributeParser = productAttributeParser;
            this._shoppingCartSettings = shoppingCartSettings;
            this._workContext = workContext;
            this._storeContext = storeContext;
            this._taxService = taxService;
            this._currencyService = currencyService;
            this._priceCalculationService = priceCalculationService;
            this._priceFormatter = priceFormatter;
            this._orderTotalCalculationService = orderTotalCalculationService;
            this._shippingSettings = shippingSettings;
            this._productAttributeService = productAttributeService;
            this._authorizationHelper = authorizationHelper;
        }
        [HttpGet]
        [GetRequestsErrorInterceptorActionFilter]
        [ResponseType(typeof(ShoppingCartJsonObject))]
        public IHttpActionResult GetCustomerShoppingCart()
        {
            var client = _authorizationHelper.GetCurrentClientFromClaims();
            if(client==null)
                return Error(HttpStatusCode.BadRequest, "client_id", "client Unauthorize");
            var _cGuid = new Guid(client.ClientId);
            var customer = _customerApiService.GetCustomerByGuid(_cGuid);
            if(customer==null)
                return Error(HttpStatusCode.BadRequest, "client_id", "invalid client_id");

            List<ShoppingCartItem> cart = this._shoppingCartItemApiService.GetShoppingCartItems(customer.Id);
            if (cart == null)
                return Error(HttpStatusCode.BadRequest, "cart", "cart null");
            if(!cart.Any())
                return Ok("cart is empty");

            ShoppingCartJsonObject model = new ShoppingCartJsonObject();
            model.CustomerId = customer.Id;
            this.PrepareMobileShoppingCartJsonObject(model,cart);
            JToken jToken = JToken.FromObject(model);
            string jTokenResult = jToken.ToString();
            return new RawJsonActionResult(jTokenResult);
        }

        [HttpPost]
        public IHttpActionResult AddToCart([ModelBinder(typeof(JsonModelBinder<AddToCartItemJsonObject>))] Delta<AddToCartItemJsonObject> cartDelta)
        {
            // Here we display the errors if the validation has failed at some point.
            if (!ModelState.IsValid)
            {
                return Error();
            }
            var client = _authorizationHelper.GetCurrentClientFromClaims();
            if (client == null)
                return Error(HttpStatusCode.BadRequest, "client_id", "client Unauthorize");
            AddToCartItemJsonObject model = new AddToCartItemJsonObject();
            cartDelta.Merge(model, true);
            var _cGuid = new Guid(client.ClientId);
            var customer = _customerApiService.GetCustomerByGuid(_cGuid);
            if (customer == null)
                return Error(HttpStatusCode.BadRequest, "client_id", "invalid client_id");

            var product = _productService.GetProductById(model.ProductId);
            if (product == null)
                return Error(HttpStatusCode.BadRequest, "product_id", "invalid product_id");
            //var _type = _productAttributeService.GetProductAttributeById(model.TypeId);
            //var _color = _productAttributeService.GetProductAttributeById(model.ColorId);
            //var _size = _productAttributeService.GetProductAttributeById(model.SizeId);
            int _quantity = model.Quantity > 0 ? model.Quantity : 1;
            //add to the cart
            var addToCartWarnings = new List<string>();
            string attributes = this.ParseProductXmlAttributes(product, model);
            addToCartWarnings.AddRange(_shoppingCartService.AddToCart(customer,
                product, ShoppingCartType.ShoppingCart, _storeContext.CurrentStore.Id,
                attributes, 0,
                null, null, _quantity, true));
            if (addToCartWarnings.Any())
            {
                JToken jToken = JToken.FromObject(addToCartWarnings);
                string jTokenResult = jToken.ToString();
                return Error(HttpStatusCode.NotAcceptable, "add_to_cart",jTokenResult);
            }
            return Ok("This product was added to cart successfull!");
        }


        [HttpDelete]
        public IHttpActionResult DeleteCartItem(int id)
        {
            var client = _authorizationHelper.GetCurrentClientFromClaims();
            if (client == null)
                return Error(HttpStatusCode.BadRequest, "client_id", "client Unauthorize");
            var _cGuid = new Guid(client.ClientId);
            var customer = _customerApiService.GetCustomerByGuid(_cGuid);
            if (customer == null)
                return Error(HttpStatusCode.BadRequest, "client_id", "invalid client_id");

            List<ShoppingCartItem> cart = this._shoppingCartItemApiService.GetShoppingCartItems(customer.Id);
            if (cart == null)
                return Error(HttpStatusCode.NoContent, "cart", "cart null");
            if (!cart.Any())
                return Ok("cart is empty");
            var selectedCartItem = cart.FirstOrDefault(p => p.Id == id);
            if (selectedCartItem != null)
            {
                _shoppingCartService.DeleteShoppingCartItem(_shoppingCartItemApiService.GetShoppingCartItem(selectedCartItem.Id), ensureOnlyActiveCheckoutAttributes: true);
            }
            return Ok(string.Format("Deleted cart item - {0}",id));
        }
        [HttpPut]
        public IHttpActionResult UpdateCartItem(int id, [ModelBinder(typeof(JsonModelBinder<AddToCartItemJsonObject>))] Delta<AddToCartItemJsonObject> cartDelta)
        {
            if (!ModelState.IsValid)
            {
                return Error();
            }
            var client = _authorizationHelper.GetCurrentClientFromClaims();
            if (client == null)
                return Error(HttpStatusCode.BadRequest, "client_id", "client Unauthorize");
            AddToCartItemJsonObject model = new AddToCartItemJsonObject();
            cartDelta.Merge(model, true);
            var _cGuid = new Guid(client.ClientId);
            var customer = _customerApiService.GetCustomerByGuid(_cGuid);
            if (customer == null)
                return Error(HttpStatusCode.BadRequest, "client_id", "invalid client_id");

            List<ShoppingCartItem> cart = this._shoppingCartItemApiService.GetShoppingCartItems(customer.Id);
            if (cart == null)
                return Error(HttpStatusCode.NoContent, "cart", "cart null");
            if (!cart.Any())
                return Ok("cart is empty");
            var selectedCartItem = cart.FirstOrDefault(p => p.Id == id);
            if(selectedCartItem==null)
                return Error(HttpStatusCode.NotFound, "cart_item", "invalid id");
            var innerWarnings = new Dictionary<int, IList<string>>();
            var itemAttributeMappings = selectedCartItem.Product.ProductAttributeMappings;
            var itemAttributeValues = _productAttributeParser.ParseProductAttributeValues(selectedCartItem.AttributesXml);
            var itemSelectedAttributeMappings = _productAttributeParser.ParseProductAttributeMappings(selectedCartItem.AttributesXml);

            ProductAttributeValue selectedSize = null;
            ProductAttributeValue selectedColor = null;
            ProductAttributeValue selectedStyle = null;

            //Parse Cart item style
            var allStyle = itemAttributeMappings.Where(p => p.ProductAttribute.Name.Equals(Common.ProductTypeAttributeValue))
               .Select(iam => iam.ProductAttributeValues).FirstOrDefault();
            selectedStyle = itemAttributeValues.Where(p => p.ProductAttributeMappingId == allStyle.FirstOrDefault().ProductAttributeMappingId).FirstOrDefault();

            //parse cart item sizing
            var selectedSizingAttrMapping = itemSelectedAttributeMappings.Where(p => p.ProductAttribute.Name.Substring(0, 5).Equals(Common.PrefixSizing)).FirstOrDefault();
            if (selectedSizingAttrMapping != null)
            {
                foreach (var cartAttrValue in itemAttributeValues)
                {
                    foreach (var itemSizeAttrValue in selectedSizingAttrMapping.ProductAttributeValues)
                    {
                        if (cartAttrValue.Id == itemSizeAttrValue.Id)
                        {
                            selectedSize = cartAttrValue;
                            break;
                        }
                    }
                }
            }

            //parse cart item color

            var allColor = itemAttributeMappings.Where(p => p.AttributeControlType == AttributeControlType.ColorSquares).ToList();
            selectedColor = itemAttributeValues.Where(p => p.ProductAttributeMapping.AttributeControlType == AttributeControlType.ColorSquares).FirstOrDefault();
            var allSizing = itemSelectedAttributeMappings.Where(p => p.ProductAttribute.Name.Substring(0, 5).Equals(Common.PrefixSizing))
                .Select(iam => iam.ProductAttributeValues).FirstOrDefault();
            string attributesXml = selectedCartItem.AttributesXml;

            if (selectedSize.Id != model.SizeId)
            {
                var newSize = allSizing.FirstOrDefault(p => p.Id == model.SizeId);
                if (newSize != null)
                {
                    attributesXml = "";
                    foreach (var attr in itemSelectedAttributeMappings)
                    {
                        if (attr.ProductAttribute.Name.Equals(Common.ProductTypeAttributeValue))
                        {
                            if (selectedStyle != null)
                            {
                                attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                                                     selectedStyle.ProductAttributeMapping, selectedStyle.Id.ToString());
                            }
                        }
                        else if (attr.ProductAttribute.Name.Substring(0, 5).Equals(Common.PrefixSizing))
                        {
                            attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                                                     newSize.ProductAttributeMapping, newSize.Id.ToString());
                        }
                        else if (attr.ProductAttribute.Name.Equals(Common.CustomFieldAttributeValue))
                        {
                            var parseData = _productAttributeParser.ParserCartItemCustomData(selectedCartItem.AttributesXml).ToList();
                            JToken jToken = JToken.FromObject(parseData);
                            string jTokenResult = jToken.ToString();
                            attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                                                      attr, jTokenResult);

                        }
                        else if (attr.AttributeControlType == AttributeControlType.ColorSquares)
                        {
                            if (selectedColor != null)
                            {
                                attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                                                     selectedColor.ProductAttributeMapping, selectedColor.Id.ToString());
                            }
                        }
                    }
                   
                }
            }
            var currSciWarnings = _shoppingCartService.UpdateShoppingCartItem(customer,
                    selectedCartItem.Id, attributesXml, selectedCartItem.CustomerEnteredPrice,
                    selectedCartItem.RentalStartDateUtc, selectedCartItem.RentalEndDateUtc,
                     model.Quantity, true);
            if (currSciWarnings.Count>0)
            {
                JToken jToken = JToken.FromObject(currSciWarnings);
                string jTokenResult = jToken.ToString();
                return Error(HttpStatusCode.NotAcceptable, "update_cart", jTokenResult);
            }
            return Ok(string.Format("Cart item {0} was updated successfully", id));
        }

        protected void PrepareMobileShoppingCartJsonObject(ShoppingCartJsonObject model, IList<ShoppingCartItem> cart)
        {
            //Shopping cart items
           foreach(var sci in cart)
            {
                var cartItemJsonObject = new ShoppingCartItemJsonObject
                {
                    Id = sci.Id,
                    ProductId = sci.ProductId,
                    Quantity = sci.Quantity,
                    CreatedOnUtc = sci.CreatedOnUtc.ToString("o"),
                    Product = _dtoHelper.PrepareProductJsonObject(sci.Product)
                };
                //Selected variant
                var itemAttributeMappings = sci.Product.ProductAttributeMappings;
                var itemAttributeValues = _productAttributeParser.ParseProductAttributeValues(sci.AttributesXml);
                var itemSelectedAttributeMappings = _productAttributeParser.ParseProductAttributeMappings(sci.AttributesXml);

                ProductAttributeValue selectedSize = null;
                ProductAttributeValue selectedColor = null;
                ProductAttributeValue selectedStyle = null;
                //Parse Cart item style
                var allStyle = itemAttributeMappings.Where(p => p.ProductAttribute.Name.Equals(Common.ProductTypeAttributeValue))
                   .Select(iam => iam.ProductAttributeValues).FirstOrDefault();
                selectedStyle = itemAttributeValues.Where(p => p.ProductAttributeMappingId == allStyle.FirstOrDefault().ProductAttributeMappingId).FirstOrDefault();

                //parse cart item sizing
                var selectedSizingAttrMapping = itemSelectedAttributeMappings.Where(p => p.ProductAttribute.Name.Substring(0, 5).Equals(Common.PrefixSizing)).FirstOrDefault();
                if (selectedSizingAttrMapping != null)
                {
                    foreach (var cartAttrValue in itemAttributeValues)
                    {
                        foreach (var itemSizeAttrValue in selectedSizingAttrMapping.ProductAttributeValues)
                        {
                            if (cartAttrValue.Id == itemSizeAttrValue.Id)
                            {
                                selectedSize = cartAttrValue;
                                break;
                            }
                        }
                    }
                }

                //parse cart item color

                var allColor = itemAttributeMappings.Where(p => p.AttributeControlType == AttributeControlType.ColorSquares).ToList();
                selectedColor = itemAttributeValues.Where(p => p.ProductAttributeMapping.AttributeControlType == AttributeControlType.ColorSquares).FirstOrDefault();

                if (selectedColor!=null)
                {
                    cartItemJsonObject.Color = new ProductAttributeJsonObject
                    {
                        Id = selectedColor.Id,
                        Name = selectedColor.Name,
                        Code = selectedColor.ColorSquaresRgb,
                        IsPreSelected = true,
                        Price = selectedColor.PriceAdjustment,
                    };

                }
                if (selectedStyle != null)
                {
                    cartItemJsonObject.Type = new ProductAttributeJsonObject
                    {
                        Id = selectedStyle.Id,
                        Name = selectedStyle.Name,
                        Code = selectedStyle.ColorSquaresRgb,
                        IsPreSelected = true,
                        Price = selectedStyle.PriceAdjustment,
                    };

                }
                if (selectedSize != null)
                {
                    cartItemJsonObject.Size = new ProductAttributeJsonObject
                    {
                        Id = selectedSize.Id,
                        Name = selectedSize.Name,
                        Code = selectedSize.ColorSquaresRgb,
                        IsPreSelected = true,
                        Price = selectedSize.PriceAdjustment,
                    };

                }

                //Image
                //picture
                if (_shoppingCartSettings.ShowProductImagesOnShoppingCart)
                {

                    var renderPictureRequest = new RenderPictureRequest();
                    renderPictureRequest.Data = _productAttributeParser.ParserCartItemCustomData(sci.AttributesXml).ToList();
                    renderPictureRequest.Scale = 512;
                    if (renderPictureRequest.Data.Any())
                    {
                        renderPictureRequest.AttrValueId = selectedColor.Id;
                        cartItemJsonObject.PhotoUrl = this.GetPictureRenderUrl(renderPictureRequest);
                    }
                    else
                    {
                        var sciPicture = sci.Product.GetProductPicture(sci.AttributesXml, _pictureService, _productAttributeParser);
                        cartItemJsonObject.PhotoUrl = _pictureService.GetApiPictureUrl(sciPicture.Id);
                        cartItemJsonObject.PhotoId = sciPicture.Id;
                    }
                    //sub total
                    List<Discount> scDiscounts;
                    decimal shoppingCartItemDiscountBase;
                    decimal taxRate;
                    decimal shoppingCartItemSubTotalWithDiscountBase = _taxService.GetProductPrice(sci.Product, _priceCalculationService.GetSubTotal(sci, true, out shoppingCartItemDiscountBase, out scDiscounts), out taxRate);
                    decimal shoppingCartItemSubTotalWithDiscount = _currencyService.ConvertFromPrimaryStoreCurrency(shoppingCartItemSubTotalWithDiscountBase, _workContext.WorkingCurrency);
                    cartItemJsonObject.SubTotal = _priceFormatter.FormatPrice(shoppingCartItemSubTotalWithDiscount);
                }
                model.Items.Add(cartItemJsonObject);
            }
            //Shopping cart total
            model.OrderTotal = this.PrepareOrderTotalsModel(cart);
            model.FreeShippingOverXEnabled = _shippingSettings.FreeShippingOverXEnabled;
            if (model.FreeShippingOverXEnabled)
                model.FreeShippingOverXValue = _shippingSettings.FreeShippingOverXValue;
        }
        protected virtual string GetPictureRenderUrl(RenderPictureRequest request)
        {
            string route = _storeContext.CurrentStore.Url + "/images";
            var url = string.Empty;
            try
            {
                url = route + "?";
                url += "AttrValueId=" + request.AttrValueId + '&';
                for (int index = 0; index < request.Data.Count; index++)
                {
                    url += string.Format("Data[{0}].Text={1}&", index, request.Data[index].Text);
                    url += string.Format("Data[{0}].ValueId={1}&", index, request.Data[index].ValueId);
                }
                url += "Scale=" + request.Scale;
            }
            catch (Exception ex)
            {
            }
            return url;
        }
        protected virtual OrderTotalsJsonObject PrepareOrderTotalsModel(IList<ShoppingCartItem> cart)
        {
            var model = new OrderTotalsJsonObject();

            if (cart.Any())
            {
                //subtotal
                decimal orderSubTotalDiscountAmountBase;
                List<Discount> orderSubTotalAppliedDiscounts;
                decimal subTotalWithoutDiscountBase;
                decimal subTotalWithDiscountBase;
                var subTotalIncludingTax = _workContext.TaxDisplayType == TaxDisplayType.IncludingTax;
                _orderTotalCalculationService.GetShoppingCartSubTotal(cart, subTotalIncludingTax,
                    out orderSubTotalDiscountAmountBase, out orderSubTotalAppliedDiscounts,
                    out subTotalWithoutDiscountBase, out subTotalWithDiscountBase);
                decimal subtotalBase = subTotalWithoutDiscountBase;
                decimal subtotal = _currencyService.ConvertFromPrimaryStoreCurrency(subtotalBase, _workContext.WorkingCurrency);
                model.SubTotal = _priceFormatter.FormatPrice(subtotal, true, _workContext.WorkingCurrency, _workContext.WorkingLanguage, subTotalIncludingTax);

                if (orderSubTotalDiscountAmountBase > decimal.Zero)
                {
                    decimal orderSubTotalDiscountAmount = _currencyService.ConvertFromPrimaryStoreCurrency(orderSubTotalDiscountAmountBase, _workContext.WorkingCurrency);
                    model.SubTotalDiscount = _priceFormatter.FormatPrice(-orderSubTotalDiscountAmount, true, _workContext.WorkingCurrency, _workContext.WorkingLanguage, subTotalIncludingTax);
                }

                //shipping info
                decimal eggShoppingCartShippingBase;
                decimal? shoppingCartShippingBase = _orderTotalCalculationService.GetShoppingCartShippingTotal(cart);
                model.IsFreeShipping = false;
                if (shoppingCartShippingBase.HasValue)
                {
                    eggShoppingCartShippingBase = shoppingCartShippingBase ?? decimal.Zero;
                    model.IsFreeShipping = shoppingCartShippingBase == decimal.Zero;
                    decimal shoppingCartShipping = _currencyService.ConvertFromPrimaryStoreCurrency(shoppingCartShippingBase.Value, _workContext.WorkingCurrency);
                    model.Shipping = _priceFormatter.FormatShippingPrice(shoppingCartShipping, true);

                    //selected shipping method
                    var shippingOption = _workContext.CurrentCustomer.GetAttribute<ShippingOption>(SystemCustomerAttributeNames.SelectedShippingOption, _storeContext.CurrentStore.Id);
                    if (shippingOption != null)
                        model.SelectedShippingMethod = shippingOption.Name;
                }

                //total
                decimal orderTotalDiscountAmountBase;
                List<Discount> orderTotalAppliedDiscounts;
                List<AppliedGiftCard> appliedGiftCards;
                int redeemedRewardPoints;
                decimal redeemedRewardPointsAmount;
                decimal? shoppingCartTotalBase = _orderTotalCalculationService.GetShoppingCartTotal(cart,
                    out orderTotalDiscountAmountBase, out orderTotalAppliedDiscounts,
                    out appliedGiftCards, out redeemedRewardPoints, out redeemedRewardPointsAmount);
                if (shoppingCartTotalBase.HasValue)
                {
                    decimal shoppingCartTotal = _currencyService.ConvertFromPrimaryStoreCurrency(shoppingCartTotalBase.Value, _workContext.WorkingCurrency);
                    model.OrderTotal = _priceFormatter.FormatPrice(shoppingCartTotal, true, false);
                }
                //discount
                if (orderTotalDiscountAmountBase > decimal.Zero)
                {
                    decimal orderTotalDiscountAmount = _currencyService.ConvertFromPrimaryStoreCurrency(orderTotalDiscountAmountBase, _workContext.WorkingCurrency);
                    model.OrderTotalDiscount = _priceFormatter.FormatPrice(-orderTotalDiscountAmount, true, false);
                }
            }

            return model;
        }
        protected virtual string ParseProductXmlAttributes(Product product, AddToCartItemJsonObject cartItem)
        {
            string attributesXml = "";
            var _productAttributeMappings = product.ProductAttributeMappings;
            var _type = _productAttributeService.GetProductAttributeValueById(cartItem.TypeId);
            var _color = _productAttributeService.GetProductAttributeValueById(cartItem.ColorId);
            var _size = _productAttributeService.GetProductAttributeValueById(cartItem.SizeId);

            // CustomFieldValue
            var pamCustomFieldValue = _productAttributeMappings.Where(ppam => ppam.ProductAttribute.Name.Equals((Common.CustomFieldAttributeValue))).FirstOrDefault();

            if(pamCustomFieldValue!=null && !string.IsNullOrEmpty(cartItem.Custom))
            {
                attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                                 pamCustomFieldValue, cartItem.Custom);
            }
         
            // FullFillSKU
            var pamFullFillSKU = _productAttributeMappings.Where(ppam => ppam.ProductAttributeId == Common.FullFillSKUProductAttributeId).FirstOrDefault();
            attributesXml = _productAttributeParser.AddProductAttribute(attributesXml, pamFullFillSKU, string.Empty);

            //Type
            if (_type != null)
            {
                attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                                             _type.ProductAttributeMapping, _type.Id.ToString());
            }
            //Color
            if (_color != null)
            {
                attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                                             _color.ProductAttributeMapping, _color.Id.ToString());
            }
            //Size
            if (_size != null)
            {
                attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                                             _size.ProductAttributeMapping, _size.Id.ToString());
            }
            return attributesXml;
        }
    }
}
