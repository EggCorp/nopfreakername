﻿using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Shipping;
using Nop.Plugin.Api.Attributes;
using Nop.Plugin.Api.Delta;
using Nop.Plugin.Api.DTOs.Shipments;
using Nop.Plugin.Api.JSON.ActionResults;
using Nop.Plugin.Api.MappingExtensions;
using Nop.Plugin.Api.ModelBinders;
using Nop.Plugin.Api.Serializers;
using Nop.Plugin.Api.Services;
using Nop.Services.Catalog;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Services.Shipping;
using Nop.Services.SMS;
using Nop.Services.Stores;
using Nop.Services.Tax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;

namespace Nop.Plugin.Api.Controllers
{
    [BearerTokenAuthorize]
    //[ApiExplorerSettings(IgnoreApi = true)]
    public class ShipmentsController : BaseApiController
    {
        private readonly IOrderItemApiService _shipmentApiService;
        private readonly IOrderApiService _orderApiService;
        private readonly IOrderService _orderService;
        private readonly IProductApiService _productApiService;
        private readonly IPriceCalculationService _priceCalculationService;
        private readonly ITaxService _taxService;
        private readonly IShipmentService _shipmentService;
        private readonly IEggReMarketingEmailService _eggReMarketingEmailService;
        private readonly ISMSMarketingService _smsService;

        public ShipmentsController(IJsonFieldsSerializer jsonFieldsSerializer,
            IAclService aclService,
            ICustomerService customerService,
            IStoreMappingService storeMappingService,
            IStoreService storeService,
            IDiscountService discountService,
            ICustomerActivityService customerActivityService,
            ILocalizationService localizationService,
            IOrderItemApiService shipmentApiService,
            IOrderApiService orderApiService,
            IOrderService orderService,
            IProductApiService productApiService,
            IPriceCalculationService priceCalculationService,
            ITaxService taxService,
            IShipmentService shipmentService,
            IPictureService pictureService,
            ISMSMarketingService smsService,
            IEggReMarketingEmailService eggReMarketingEmailService)
            : base(jsonFieldsSerializer,
                  aclService,
                  customerService,
                  storeMappingService,
                  storeService,
                  discountService,
                  customerActivityService,
                  localizationService,
                  pictureService)
        {
            _shipmentApiService = shipmentApiService;
            _orderApiService = orderApiService;
            _orderService = orderService;
            _productApiService = productApiService;
            _priceCalculationService = priceCalculationService;
            _taxService = taxService;
            _shipmentService = shipmentService;
            _eggReMarketingEmailService = eggReMarketingEmailService;
            _smsService = smsService;
        }

        [HttpGet]
        [ResponseType(typeof(ShipmentsRootObject))]
        [GetRequestsErrorInterceptorActionFilter]
        public IHttpActionResult GetShipmentByIdForOrder(int orderId, string fields = "")
        {
            Order order = _orderApiService.GetOrderById(orderId);

            if (order == null)
            {
                return Error(HttpStatusCode.NotFound, "order", "not found");
            }

            IList<Shipment> shipments = order.Shipments.ToList();

            if (shipments == null || shipments.Any())
            {
                return Error(HttpStatusCode.NotFound, "order_shipment", "not found");
            }

            var shipmentDtos = new List<ShipmentDto>();
            shipmentDtos = shipments.Select(p => p.ToDto()).ToList();

            var shipmentsRootObject = new ShipmentsRootObject()
            {
                Shipments = shipmentDtos
            };

            var json = _jsonFieldsSerializer.Serialize(shipmentsRootObject, fields);

            return new RawJsonActionResult(json);
        }

        [HttpPost]
        [ResponseType(typeof(ShipmentsRootObject))]
        public IHttpActionResult CreateOrderShipment(int orderId,
            [ModelBinder(typeof(JsonModelBinder<ShipmentDto>))] Delta<ShipmentDto> shipmentDelta)
        {
            try
            {
                // Here we display the errors if the validation has failed at some point.
                if (!ModelState.IsValid)
                {
                    return Error();
                }

                Order order = _orderApiService.GetOrderById(orderId);

                if (order == null)
                {
                    return Error(HttpStatusCode.NotFound, "order", "not found");
                }

                Shipment orderShipment = _shipmentService.GetOrderTrackingNumberForOrder(order.Id);

                if (orderShipment == null)
                {
                    Shipment newShipment = new Shipment();
                    shipmentDelta.Merge(newShipment);
                    newShipment.CreatedOnUtc = DateTime.UtcNow;
                    newShipment.OrderId = orderId;
                    if (!string.IsNullOrEmpty(newShipment.TrackingNumber))
                        newShipment.ShippedDateUtc = DateTime.UtcNow;
                    order.Shipments.Add(newShipment);
                    order.ShippingStatus = ShippingStatus.Shipped;
                    _orderService.UpdateOrder(order);
                    foreach (var orderitem in order.OrderItems)
                    {
                        var shipmentItem = new ShipmentItem
                        {
                            OrderItemId = orderitem.Id,
                            Quantity = orderitem.Quantity,
                            ShipmentId = newShipment.Id,
                        };
                        _shipmentService.InsertShipmentItem(shipmentItem);
                    }

                    _customerActivityService.InsertActivity("AddNewShipment",
                       _localizationService.GetResource("ActivityLog.AddNewShipment"), newShipment.Id);

                    var shipmentsRootObject = new ShipmentsRootObject();

                    shipmentsRootObject.Shipments.Add(newShipment.ToDto());

                    this._eggReMarketingEmailService.SendOrderShippedEmail(order.Id);

                    if (!string.IsNullOrEmpty(order.BillingAddress.PhoneNumber))
                    {
                        string _smsMessage = string.Format("Dear Sir/Madam, your tracking number is {0} Please access the link {1} to track your order. Thank you!", newShipment.TrackingNumber, newShipment.TrackingUrl);
                        this._smsService.SendCustomizationSMS(order.BillingAddress.PhoneNumber, _smsMessage);
                    }

                    var json = _jsonFieldsSerializer.Serialize(shipmentsRootObject, string.Empty);

                    return new RawJsonActionResult(json);
                }
                else
                {
                    return Error(HttpStatusCode.NotAcceptable, "order", "has been sent tracking number!");
                }
            }
            catch (Exception ex)
            {
                return Error(HttpStatusCode.NotFound, "order", ex.Message);
            }
        }

        [HttpPost]
        public IHttpActionResult SendOrderProcessingEmail(int orderId)
        {
            try
            {
                Order order = _orderApiService.GetOrderById(orderId);
                if (order == null)
                {
                    return Error(HttpStatusCode.NotFound, "order", "not found");
                }
                this._eggReMarketingEmailService.SendOrderInProcessingEmail(order.Id);
                _orderService.UpdateOrderStatus(new List<int> { order.Id }, OrderStatus.FullFilled);
                return Ok();
            }
            catch (Exception ex)
            {
                return Error(HttpStatusCode.NotFound, "order", ex.Message);
            }
        }

        [HttpPut]
        [ResponseType(typeof(ShipmentsRootObject))]
        public IHttpActionResult UpdateOrderShipment(int orderId, int shipmentId,
          [ModelBinder(typeof(JsonModelBinder<ShipmentDto>))] Delta<ShipmentDto> shipmentDelta)
        {
            // Here we display the errors if the validation has failed at some point.
            if (!ModelState.IsValid)
            {
                return Error();
            }

            Shipment shipmentToUpdate = _shipmentService.GetShipmentById(shipmentId);

            if (shipmentToUpdate == null)
            {
                return Error(HttpStatusCode.NotFound, "order_item", "not found");
            }

            Order order = _orderApiService.GetOrderById(orderId);

            if (order == null)
            {
                return Error(HttpStatusCode.NotFound, "order", "not found");
            }

            // This is needed because those fields shouldn't be updatable. That is why we save them and after the merge set them back.
            shipmentDelta.Merge(shipmentToUpdate);
            _orderService.UpdateOrder(order);

            _customerActivityService.InsertActivity("UpdateShipment",
               _localizationService.GetResource("ActivityLog.UpdateShipment"), shipmentToUpdate.Id);

            var shipmentsRootObject = new ShipmentsRootObject();

            shipmentsRootObject.Shipments.Add(shipmentToUpdate.ToDto());

            var json = _jsonFieldsSerializer.Serialize(shipmentsRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        [HttpDelete]
        [GetRequestsErrorInterceptorActionFilter]
        public IHttpActionResult DeleteShipmentById(int orderId, int shipmentId)
        {
            Order order = _orderApiService.GetOrderById(orderId);

            if (order == null)
            {
                return Error(HttpStatusCode.NotFound, "order", "not found");
            }

            Shipment shipment = _shipmentService.GetShipmentById(shipmentId);
            _shipmentService.DeleteShipment(shipment);

            return new RawJsonActionResult("{}");
        }

        [HttpDelete]
        [GetRequestsErrorInterceptorActionFilter]
        public IHttpActionResult DeleteAllShipmentsForOrder(int orderId)
        {
            Order order = _orderApiService.GetOrderById(orderId);

            if (order == null)
            {
                return Error(HttpStatusCode.NotFound, "order", "not found");
            }

            var shipmentsList = order.Shipments.ToList();

            for (int i = 0; i < shipmentsList.Count; i++)
            {
                _shipmentService.DeleteShipment(shipmentsList[i]);
            }

            return new RawJsonActionResult("{}");
        }
    }
}
