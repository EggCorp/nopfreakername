﻿using FluentValidation;
using Nop.Plugin.Api.Models;
using Nop.Services.Localization;

namespace Nop.Plugin.Api.Validators
{
    public class ClientValidator : AbstractValidator<ClientModel>
    {
        public ClientValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.UserId).NotEmpty().WithMessage(localizationService.GetResource("Plugins.Api.Admin.Entities.Client.FieldValidationMessages.Name"));
            RuleFor(x => x.ClientId).NotEmpty().WithMessage(localizationService.GetResource("Plugins.Api.Admin.Entities.Client.FieldValidationMessages.ClientId"));
        }
    }
}