﻿using FluentValidation.Attributes;
using Nop.Plugin.Api.Validators;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using System;

namespace Nop.Plugin.Api.Models
{
    [Validator(typeof(ClientValidator))]
    public class ClientModel : BaseNopEntityModel
    {
        [NopResourceDisplayName("Plugins.Api.Admin.Client.UserId")]
        public int UserId { get; set; }
        [NopResourceDisplayName("Plugins.Api.Admin.Client.ClientId")]
        public string ClientId { get; set; }
        [NopResourceDisplayName("Plugins.Api.Admin.Client.IsActive")]
        public bool IsActive { get; set; }
        [NopResourceDisplayName("Plugins.Api.Admin.Client.AccessToken")]
        public string AccessToken { get; set; }
        [NopResourceDisplayName("Plugins.Api.Admin.Client.TokenType")]
        public string TokenType { get; set; }
        [NopResourceDisplayName("Plugins.Api.Admin.Client.ExpiresIn")]
        public DateTime ExpiresIn { get; set; }
        [NopResourceDisplayName("Plugins.Api.Admin.Client.RefreshToken")]
        public string RefreshToken { get; set; }
        [NopResourceDisplayName("Plugins.Api.Admin.Client.RenewCount")]
        public int RenewCount { get; set; }
        [NopResourceDisplayName("Plugins.Api.Admin.Client.CreatedOnUtc")]
        public DateTime CreatedOnUtc { get; set; }
    }
}