﻿using Nop.Core;
using System;

namespace Nop.Plugin.Api.Domain
{
    public class Client : BaseEntity
    {
        public string ClientId { get; set; }
        public bool IsActive { get; set; }

        public string AccessToken { get; set; }
        public string TokenType { get; set; }
        public DateTime? ExpiresIn { get; set; }
        public string RefreshToken { get; set; }

        public int? RenewCount { get; set; }
        public DateTime CreatedOnUtc { get; set; }
    }
}