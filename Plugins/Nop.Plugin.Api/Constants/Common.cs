﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api
{
    public class Common
    {
        //Product Attribute
        public const string ProductTypeAttributeValue = "Available Products";
        public const string CustomFieldAttributeValue = "CustomFieldValue";

        public const int CustomFieldValueProductAttributeId = 4;

        public const int ProductTypeProductAttributeId = 1;

        public const int FullFillSKUProductAttributeId = 5;

        public const string PrefixCustomField = "customField_";

        public const string PrefixAttribute = "product_attribute_";

        public const string RenderQueryStringKey = "custom";

        public const string ApplyCouponQueryStringKey = "coupon";
        public const string PrefixSizing = "size_";
        public const string PrefixColorAttributeValue = "Color";
        //payment method name
        public const string StripePaymentMethodName = "Payments.Stripe";
        public const string PaypalPaymentMethodName = "Payments.PayPalStandard";
    }
}
